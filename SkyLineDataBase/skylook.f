C skylook.f
C
      implicit none
      integer*4 sc,ec,sr,er,getpos
      integer*4 nc,nr,narg,inhead
      integer*4 i,row,expos,obs,nsky,lc
      character*115200 header
      character*80 arg(9),skyfile,sldb
      real*4 a(200000),ech,xd
      real*8 fhead,cent,GetSpimWave,GetSpimPix,pix,wave,bwv(99),rwv(99)
      real*8 disp,r8,skyline(9000),peak,fwhm
      logical ok

C Sky line list.
      call getenv('SKYLINES',sldb)

C Get command line parameters.
      call arguments(arg,narg,9)
      if (narg.ne.1) then
        print *,'Syntax: skylook (Wave. Calib. Sky FITS file)'
        print *,'   Measure position of sky lines.  The environment variable'
        print *,'   SKYLINES should contain the filename of a file with a list'
        print *,'   of approximate sky line positions.'
        print '(2a)',' Sky line database: ',sldb(1:lc(sldb))
        call exit(0)
      endif
      skyfile = arg(1)
      call AddFitsExt(skyfile)

C Read fits file...
      call readfits(skyfile,a,header,ok)
      nc = inhead('NAXIS1',header)
      nr = inhead('NAXIS2',header)
      sc = inhead('CRVAL1',header)
      sr = inhead('CRVAL2',header)
      ec = sc + nc - 1
      er = sr + nr - 1
      
C Other things.
      print '(2a)','File= ',skyfile(1:40)
      ech= sngl(fhead('ECHANGL',header))
      xd = sngl(fhead('XDANGL',header))
      print '(a,2f9.4)','ECHA,XDA= ',ech,xd
      expos= inhead('EXPOSURE',header)
      obs  = inhead('OBSNUM',header)
     
C Read in sky lines.
      print '(2a)',' Sky line database: ',sldb(1:lc(sldb))
      open(1,file=sldb,status='old')
      nsky=0
11    read(1,*,end=55) r8
      nsky=nsky+1
      skyline(nsky) = r8
      goto 11
55    close(1)

C Initialize wavelength = func(pixel) function.
      if (GetSpimWave(header,0.d0,0).lt.0.) then
        print *,'Error getting wavelength scale.'
        stop
      endif
C Initialize pixel = func(wavelength) function.
      if (GetSpimPix(header,0.d0,0).lt.0.) then
        print *,'Error creating inverse wavelength scale.'
        stop
      endif
C Must be air, non-heliocentric.
      if ( (getpos('HELIOCNT',header).ne.-1).or.
     .     (getpos('VACUUM',header).ne.-1) ) then
        print *,'Error- heliocentric or vacuum correction has been applied.'
        stop
      endif

C Load wavelength range for each row.
      do row=sr,er
        bwv(row) = GetSpimWave(header,dfloat(sc),row)
        rwv(row) = GetSpimWave(header,dfloat(ec),row)
      enddo

C Do each row.
      DO row=sr,er
C Look for each sky line.
      do i=1,nsky
C Is line within this row?
        if ((skyline(i).gt.bwv(row)).and.(skyline(i).lt.rwv(row))) then
C What is the pixel position?
          pix = GetSpimPix(header,skyline(i),row)
C Is it well within this row?
          if ((nint(pix).gt.sc+7).and.(nint(pix).lt.ec-7)) then
C Dispersion at this position.
            disp = ( GetSpimWave(header,pix+5.d0,row)
     .             - GetSpimWave(header,pix-5.d0,row) ) / 10.d0
C Centroid the line.
            call arc_centroid(a,sc,ec,sr,er,row,pix,cent,peak,fwhm)
            if (fwhm.gt.0.) then
              wave = GetSpimWave(header,cent,row)
              print '(f8.2,f12.5,f9.1,f7.2,f10.3,f10.6,3f6.0)',
     .          skyline(i),wave,peak,fwhm,cent,disp,
     .          float(obs),float(expos),float(row)
            endif
          endif
        endif
      enddo
      ENDDO

      stop
      end

C----------------------------------------------------------------------
      subroutine arc_centroid(a,sc,ec,sr,er,row,pix,cent,peak,fwhm)
      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er)
      real*8 pix,cent,peak,fwhm,last_cent,sum,wsum,r8
      real*4 low,arr(900),median
      integer*4 i,n,iter

C Find background.
      n=0
      do i=max(sc,nint(pix)-40),min(ec,nint(pix)+40)
        n=n+1
        arr(n) = a(i,row)
      enddo
      call find_median(arr,n,median)
      low = arr(n/4)
C Find centroid.
      iter=0
      cent=pix
      last_cent=cent+1.d0
      do while((iter.lt.10).and.(abs(cent-last_cent).gt.0.01))
        iter=iter+1
        last_cent=cent
        sum = 0.d0
        wsum= 0.d0
        do i=max(sc,nint(cent)-5),min(ec,nint(cent)+5)
          r8  = dble(max(0.,a(i,row)-low))
          sum = sum + dfloat(i)*r8
          wsum= wsum+ r8
        enddo
        if (wsum.lt.1.d-30) goto 905
        cent = sum / wsum
      enddo 
C Find peak.
      peak=a(nint(cent),row)
      do i=max(sc,nint(cent)-5),min(ec,nint(cent)+5)
        if (a(i,row).gt.peak) peak=a(i,row)
      enddo 
C Find area.
      sum=0.d0
      do i=max(sc,nint(cent)-5),min(ec,nint(cent)+5)
        sum=sum+dble(max(0.,a(i,row)-low))
      enddo 
C Find FWHM.
      if ((sum.lt.1.d-30).or.(peak.lt.1.d-30)) then
        print *,'Error finding FWHM, sum,peak=',sum,peak
        call exit(1)
      else
        fwhm=0.939437d0*sum/peak
      endif
      return
905   continue
      print *,'ERROR wsum=',wsum
      cent=0.d0
      peak=0.d0
      fwhm=-1.d0
      return
      end
