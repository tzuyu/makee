C skyshift.f   Calculate shift from sky lines.

      implicit none
      character*80 arg(999)
      character header*115200
      real*4 a(100000),minpk
      integer*4 narg,minline
      character*80 skyfile,file,wrd
      integer*4 inhead,sc,ec,sr,er,nc,nr,i,lc
      logical ok,verbose,findarg,plot
      real*8 r8,SkyShift
 
      call arguments(arg,narg,999)
      verbose = findarg(arg,narg,'-verbose',':',wrd,i)
      plot = findarg(arg,narg,'-plot',':',wrd,i)
      if (narg.lt.1) then
        print *,'Syntax:  skyshift (FITS file) [-verbose] [-plot]'
        stop
      endif

      call Get_EE_Home(wrd)
      skyfile = wrd(1:lc(wrd))//'SkyLineDataBase/sky.results'

      file=arg(1)
      call AddFitsExt(file)
      call readfits(file,a,header,ok)

C Dimensions.
      nc = inhead('NAXIS1',header)
      nr = inhead('NAXIS2',header)
      sc = inhead('CRVAL1',header)
      sr = inhead('CRVAL2',header)
      ec = sc + nc - 1
      er = sr + nr - 1

      minpk  =15.
      minline= 5

      r8 = SkyShift(a,sc,ec,sr,er,header,skyfile,minpk,minline,verbose,plot)

C     print '(f9.3)',r8

      stop
      end 

