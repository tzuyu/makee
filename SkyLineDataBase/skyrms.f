C skyrms.f
      implicit none
      real*8 r1,r2,r3,r4,r5,r6,r7,r8,r9
      integer*4 m
      parameter(m=9000)
      real*8 w(m),s(m),f(m),d(m),wgt(m)
      real*8 fwhm,strength,mean,rms,last_r1,merr,disp
C     real*8 median,arr(m)
      real*8 dev,hidev,wsum,siglim
      integer*4 n,i,nrej,hidevpt
C Set rejection limit.
      siglim = 3.0
C Open data file.
      open(1,file='sky.data',status='old')
      read(1,*,end=5) r1,r2,r3,r4,r5,r6,r7,r8,r9
1     continue
      n=1
      w(n)=r2
      s(n)=3600.d0 * r3/r8
      f(n)=r4
      d(n)=r6
      last_r1=r1
2     continue
      read(1,*,end=5) r1,r2,r3,r4,r5,r6,r7,r8,r9
      if (abs(r1-last_r1).lt.0.01) then
        n=n+1
        w(n)=r2
        s(n)=3600.d0 * r3/r8
        f(n)=r4
        d(n)=r6
        last_r1=r1
        goto 2
      endif
C Need enough lines to get decent measure.
      if (n.lt.5) goto 1
C Set number of lines rejected.
      nrej=0
      do i=1,n
        wgt(i)=1.d0
      enddo
3     continue
C Find Median.
C     j=0
C     do i=1,n
C       if (wgt(i).gt.0.) then
C         j=j+1
C         arr(j)=w(i)
C       endif
C     enddo
C     call find_median8(arr,j,median)
C Weight sum.
      wsum=0.d0
      do i=1,n
        wsum=wsum+wgt(i)
      enddo
C Find Mean
      mean=0.d0
      do i=1,n
        mean = mean + (w(i)*wgt(i))
      enddo
      mean=mean/wsum
C Find RMS
      rms=0.d0
      do i=1,n
        rms = rms + (((mean-w(i))*(mean-w(i)))*wgt(i))
      enddo
      rms=sqrt(rms/wsum)
C Mean Dispersion.
      disp=0.d0
      do i=1,n
        disp = disp + (d(i)*wgt(i))
      enddo
      disp=disp/wsum
C Mean FWHM.
      fwhm=0.d0
      do i=1,n
        fwhm = fwhm + (f(i)*wgt(i))
      enddo
      fwhm=fwhm/wsum
C Mean Strength.
      strength=0.d0
      do i=1,n
        strength = strength + (s(i)*wgt(i))
      enddo
      strength=strength/wsum
C Error in mean.
      merr=rms/sqrt(wsum)
C Reject bad points.
      hidev=0.
      hidevpt=0
      do i=1,n
        if (wgt(i).gt.0.) then
          dev = abs(w(i)-mean)/rms
          if (dev.gt.hidev) then
            hidev  =dev
            hidevpt=i
          endif
        endif
      enddo
      if ((hidev.gt.siglim).and.(nrej.lt.(n/5))) then
        nrej=nrej+1
        wgt(hidevpt) = 0.d0
        goto 3
      endif
C Print.
      print '(i4,f11.4,2f8.4,f8.1,f7.2,f8.4,2f6.2)',
     .  n-nrej,mean,rms,merr,strength,fwhm,disp,rms/disp,merr/disp
      goto 1
5     close(1)
      stop
      end
      
