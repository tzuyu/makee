C ------------------------------------------------------------------------------
C flat2pin.f
C
C Collapse a flat field image into a pinhole-type image.
C
      implicit none
      integer*4 maxpt
      parameter(maxpt=5290000)  
      integer*4 sc,ec,sr,er,bsr,ber,bsc,bec,mvrow,i,narg,nc,nr,lc
      real*4 a(maxpt),b(maxpt),valread
      character*115200 aheader,bheader
      character*80 afile,bfile,arg(9),wrd
      logical ok,findarg

      call arguments(arg,narg,9)

      mvrow=0
      if (findarg(arg,narg,'row=',':',wrd,i)) then
         mvrow = max(0,nint(valread(wrd)))
      endif

      if (narg.ne.2) then
        print *,'Syntax:  flat2pin  (FITS file 1)  (FITS file 2)'
        print *,'  ntax:  flat2pin  (FITS file 1)  (FITS file 2)'
        print *,'    Move a row from FITS image 1 to FITS image 2.'
        print *,'    Overwrites original FITS image 2 file.'
        call exit(0)
      endif

      afile = arg(1)
      call AddFitsExt(afile)
      call readfits(afile,a,maxpt,aheader,ok)
      if (.not.ok) goto 801
      call GetDimensions(aheader,sc,ec,sr,er,nc,nr)

      bfile = arg(2)
      call AddFitsExt(bfile)
      call readfits(bfile,b,maxpt,bheader,ok)
      if (.not.ok) goto 801
      call GetDimensions(bheader,bsc,bec,bsr,ber,nc,nr)

      if ((sc.ne.bsc).or.(sr.ne.bsr)) goto 802
      if ((ec.ne.bec).or.(er.ne.ber)) goto 802

      if ((mvrow.lt.sr).or.(mvrow.gt.er)) goto 803

      print *,'Moving row:',mvrow
      call MoveRow(a,b,sc,ec,sr,er,mvrow)

      print '(2a)',' Overwriting: ',bfile(1:lc(bfile))
      call writefits(bfile,b,bheader,ok)
      goto 999

801   print *,'Error reading FITS file.'
      goto 999

802   print *,'Error- dimensions do not match.'
      goto 999

803   print *,'Error- row to be moved out of range.'

999   stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
      subroutine MoveRow(a,b,sc,ec,sr,er,mvrow)
      implicit none
      integer*4 sc,ec,sr,er,mvrow,col
      real*4 a(sc:ec,sr:er)
      real*4 b(sc:ec,sr:er)
      do col=sc,ec
        b(col,mvrow) = a(col,mvrow)
      enddo
      return
      end

