C spim1.f                               tab  1995-1998

C
C (very old) TODO:
C
C  >>> Small shift between two of the same setup.
C
C  >>> Verify the small shifts by comparing centroids from both lamps.
C
C  >>> Verify wavelength scale on full FITS and on parsed FITS files.
C
C  >>> Sum up flat field groups automatically.
C
C  >>> Large auto-shift to auto-scale an arbitrary image.
C
C  >>> final refitting of lines with gaussians.???
C

C
      implicit none
      integer*4 nc,nr,sc,sr,er,ec,narg,inhead,i,j
      integer*4 nc2,nr2,sc2,sr2,row
      character*115200 header(2)
      logical ok
      character*80 arg(9),file(2),wrd,wrd2,idfile,device
      integer*4 maxpt,maxpt2,lastchar
      parameter(maxpt=201000,maxpt2=2*maxpt)
      real*4 a(maxpt2),x(maxpt),valread,c(maxpt)
      logical findarg,desat,EE_QueryHIRES,EE_QueryESI
      logical EE_QueryHIRES2
C
      include 'spim0.inc'
      include 'spim1.inc'
      include 'verbose.inc'
      include 'soun.inc'

C Standard output.
      soun=6

C Get command line parameters.
      arg(2)=' '
      call arguments(arg,narg,9)
      idfile=' '
      if (findarg(arg,narg,'id=',':',wrd,i)) idfile=wrd
      ssf=1.d0
      if (findarg(arg,narg,'ssf=',':',wrd,i)) ssf=dble(valread(wrd))
      row=-1001
      if (findarg(arg,narg,'row=',':',wrd,i)) row=nint(valread(wrd))
      device = '?'
      if (findarg(arg,narg,'device=',':',wrd,i)) device=wrd
      vt100 = findarg(arg,narg,'-vt100',':',wrd,i)
      desat = findarg(arg,narg,'-desat',':',wrd,i)
      verbose = findarg(arg,narg,'-verbose',':',wrd,i)
      if ((narg.lt.1).or.(narg.gt.2)) then
        print *,
     .  'Syntax: spim1 (FITS file) [2nd FITS file] [id=file] [ssf=v]'
        print *,
     .  '                [-vt100]  [row=v]  [-desat]  [-verbose]'
        print *,
     .  '                [device=]'
        print *,' Spectra-in-an-image interactive plotting program.'
        print *,
     . ' This program is used for interactive wavelength calibrations.'
        print *,
     .  '    "ssf=v"   : is the strength scale factor (default is 1.0)'
        print *,'                  to apply to the second image only.'
        print *,'    "row=v"   : starts by plotting row v.'
        print *,
     .  '    "-vt100"  : for plotting on a VT100 with Retrographics.'
        print *,
     . '    "-desat"  : to correct for saturated and bleeding columns.'
        print *,'    "-verbose": no verbosity.'
        print *,
     . '(you can use MAKEE_DIR/00.pgplot_device to specify device)'
        call exit(0)
      endif
      file(1) = arg(1)
      call AddFitsExt(file(1))
      if (narg.gt.1) then
        file(2) = arg(2)
        call AddFitsExt(file(2))
        numim=2
      else
        file(2) = ' '
        numim=1
      endif

C Set device.   Added -tab 23jun2008
      if (device.eq.'?') then
        call GetMakeeHome(wrd)
        wrd2 = wrd(1:lastchar(wrd))//'/00.pgplot_device'
        if (access(wrd2,'r').eq.0) then
          open(1,file=wrd2,status='old')
          read(1,'(a)') device
          close(1)
        else
          device = '/XDISP'
        endif
      endif
      pgplot_device = device
      print *,'NOTE: Device=',pgplot_device(1:lastchar(pgplot_device))

C Read first fits file...
      call readfits(file(1),a,maxpt2,header(1),ok)
      if (.not.ok) then
        print *,'Error reading fits file: ',file(1)
        call exit(1)
      endif
      nc = max(1,inhead('NAXIS1',header(1)))
      nr = max(1,inhead('NAXIS2',header(1)))
      sc = inhead('CRVAL1',header(1))
      sr = inhead('CRVAL2',header(1))
      ec = sc + nc - 1
      er = sr + nr - 1
      if (desat) then
C Desaturate bleeding columns.
        call arcdesat(a,c,sc,ec,sr,er,60000.,i)
        if (i.gt.0) print *,i,
     .     ' bad (saturated and bleeding) pixels found in first image.'
      endif

C Load echelle-order-defining polynomial coeffecients.
      call Load_co(nr,header(1))

C Read second fits file...
      if (file(2).ne.' ') then
        call readfits(file(2),x,maxpt,header(2),ok)
        if (.not.ok) then
          print *,'Error reading fits file: ',file(2)
          call exit(1)
        endif
        nc2= max(1,inhead('NAXIS1',header(2)))
        nr2= max(1,inhead('NAXIS2',header(2)))
        sc2= inhead('CRVAL1',header(2))
        sr2= inhead('CRVAL2',header(2))
        if ((nc.ne.nc2).or.(nr.ne.nr2).or.
     .                 (sc.ne.sc2).or.(sr.ne.sr2)) then
          print *,'ERROR- image dimensions do not match.'
          print *,'sr1,sc1,nc1,nr1=',sr,sc,nc,nr
          print *,'sr2,sc2,nc2,nr2=',sr2,sc2,nc2,nr2
          call exit(1)
        endif
        if (desat) then
          call arcdesat(x,c,sc,ec,sr,er,60000.,i)
          if (i.gt.0) print *,i,
     .  ' bad (saturated and bleeding) pixels found in second image.'
        endif
      else
        header(2)=' '
      endif

C ESI or HIRES?
      spim1_ESI   = EE_QueryESI(header(1))
      spim1_HIRES = EE_QueryHIRES(header(1))
      spim1_HIRES2= EE_QueryHIRES2(header(1))

C Check?
      if ((.not.spim1_ESI).and.(.not.spim1_HIRES)
     .                    .and.(.not.spim1_HIRES2)) then
        print *,'ERROR: Image must be either ESI or HIRES[2].'
        call exit(1)
      endif
      if ((spim1_ESI).and.(spim1_HIRES)) then
        print *,'ERROR: Both ESI and HIRES flagged.'
        call exit(1)
      endif

C Interactive plotting.
      call spim1(a,x,sc,ec,sr,er,header,file,idfile,row,device)

C Write the new images out?
      if (newim) then
        if (desat) print *,
     .  'WARNING: Image was modified due to -desat option.'
        wrd='new-'//file(1)
        print '(2a)',' Writing: ',wrd(1:60)
        call writefits(wrd,a(1),header(1),ok)
        if (file(2).ne.' ') then
          wrd='new-'//file(2)
          print '(2a)',' Writing: ',wrd(1:60)
          j = (nc*nr) + 1
          call writefits(wrd,a(j),header(2),ok)
        endif
      endif

      stop
      end

CENDOFMAIN
