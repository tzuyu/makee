C flatave.f                                   tab Oct98
C
      implicit none
C
      character*80 arg(999),wrd
      integer*4 narg,i,lc,nfi
      logical findarg,ok
      real*4 valread
      logical nogroup,nocontig
C
      include 'makeepipe.inc'
      include 'verbose.inc'
C

C Get arguments from command line.
      call arguments(arg,narg,999)
C
      saturation=64000.
      if (findarg(arg,narg,'sat=',':',wrd,i)) saturation=valread(wrd)
      rawdir='./'
      if (findarg(arg,narg,'raw=',':',wrd,i)) rawdir=wrd
      nogroup = findarg(arg,narg,'-nogroup',':',wrd,i)
      nocontig= findarg(arg,narg,'-nocontig',':',wrd,i)
      verbose = findarg(arg,narg,'-verbose',':',wrd,i).or.
     .          findarg(arg,narg,'-v'      ,':',wrd,i)
      echtol=0.0011
      if (findarg(arg,narg,'echtol=',':',wrd,i)) echtol=valread(wrd)
      xdtol =0.0011
      if (findarg(arg,narg,'xdtol=',':',wrd,i)) xdtol=valread(wrd)
C
      if (narg.eq.0) then
        print *,'Syntax: flatave (list of files) or ("all")'
        print *,'            [sat=]  [raw=]  [-verbose]'
        print *,'            [echtol=] [xdtol=]'
C       print *,'            [-nogroup]  [-nocontig]'
        print *,
     .  '   Average HIRES flat field images.  Exclude saturated'
        print *,
     .  '   flat fields.  If "all" is given then all the files in the'
        print *,
     .  '   raw image directory are used.  Quartz lamp images are'
        print *,
     .  '   grouped which are contiguous in the file list (excluding'
        print *,
     .  '   pinhole, saturated flats, and non-flats), are taken within'
        print *,'   a 2 hour period, have the same echelle'
        print *,
     .  '   and cross disperser angle within a tolerance of 0.0011 and'
        print *,'   have the same decker name.'
        print *,
     .  '   (This assumes all images have the same dimensions.)'
        print *,' '
        print *,'Options:'
        print *,'    sat=  : give saturation level (default is 64000)'
        print *,
     .  '    raw=  : Directory with raw image files (default is "./")'
CCC     print *,' -nogroup : Average all files in list, do not group.'
CCC     print *,
CCC  .  ' -nocontig: Averaged files need not be contiguous in the list.'
        call exit(0)
      endif
C
C Grab list of files from raw directory.
      if (arg(1)(1:3).eq.'all') then
        call system('/usr/bin/ls '//rawdir(1:lc(rawdir))//
     .              '/*.fits >! /tmp/flatave.temp')
        open(1,file='/tmp/flatave.temp',status='old')
        nfi=0
1       read(1,'(a)',end=5) wrd
        nfi=nfi+1
        fi(nfi)=wrd
        hi(nfi)=0.
        goto 1
5       close(1,status='delete')
      else
        do i=1,narg
          fi(i)=arg(i)
          hi(i)=0.
        enddo
        nfi=narg
      endif

C Clear.  (added this 27sep05, don't know why it wasn't here before -tab)
      do i=1,maxpix
        obj(i) = 0.
        flat(i)= 0.
      enddo
       
C Average flats.
      call hires_flatave(nfi,fi,hi,echtol,xdtol,saturation,rawdir,
     .                'flat.list',ok,obj,flat)
      if (.not.ok) then
        print *,'ERROR in flatave program.'
        call exit(1)
      endif

      stop
      end

