C This file is pse.inc

C:::::::::::::::::::::::::
C Semi-fixed parameters.  See "makee.param" for descriptions.
C
C Suggested values:
C     parameter(PSE_SkySpacer=1.0)
C     parameter(PSE_SkyMin=1.0)
C     parameter(PSE_SkyOrder=1)
C     parameter(PSE_SingleColumnSkyFit=.false.)
C     parameter(PSE_SkyRejLimOne=5.5)
C     parameter(PSE_SkyRejLimSat=2.0)
C     parameter(PSE_SkyRejLimTwo=4.0)
C     parameter(PSEO_ProfInc=0.2)
C     parameter(PSEO_ProfIncSlop=0.1)
C     parameter(PSEOM_ProfMdnBox=12)
C     parameter(PSE_ProOrder=-1)
C     parameter(PSEOM_ProRejLim=4.0)
C     parameter(PSEOM_ObjRejLim=5.0)
C     parameter(PSEOM_ObjRejLimBig=(2.0*PSEOM_ObjRejLim))
C     parameter(PSEOM_ObjRejLimSat=2.2)
C
      real*4 PSE_SkySpacer, PSE_SkyMin
      real*4 PSE_SkyRejLimOne, PSE_SkyRejLimSat
      real*4 PSE_SkyRejLimTwo, PSEO_ProfInc, PSEO_ProfIncSlop
      real*4 PSEOM_ProRejLim, PSEOM_ObjRejLim, PSEOM_ObjRejLimBig
      real*4 PSEOM_ObjRejLimSat
      integer*4 PSE_SkyOrder, PSEOM_ProfMdnBox, PSE_ProOrder
      logical PSE_SingleColumnSkyFit
C
      common / PSEPARMBLK / PSE_SkySpacer,PSE_SkyMin,PSE_SkyRejLimOne,
     .    PSE_SkyRejLimSat, PSE_SkyRejLimTwo, 
     .    PSEO_ProfInc, PSEO_ProfIncSlop,
     .    PSEOM_ProRejLim, PSEOM_ObjRejLim, PSEOM_ObjRejLimBig,
     .    PSEOM_ObjRejLimSat, PSE_SkyOrder, 
     .    PSEOM_ProfMdnBox, PSE_ProOrder,
     .    PSE_SingleColumnSkyFit
C:::::::::::::::::::::::::

C ::::::::::::::::::::: VARIABLES ::::::::::::::::::::::::::::
C
C mpixo = Maximum number of points in fits, normally this should be less than
C         the number of columns in image.
C PSE_mpv=Maximum number of "threads" in profile modeling.  This should be the
C         maximum object width divided by the minimum profile increment.
C
      integer*4 mpixo,PSE_mpv,mth
      parameter(mpixo=900,PSE_mpv=5000,mth=900)
      real*8 pff(0:20,mth)
      real*4 PSE_pparm(5),PSE_pval(PSE_mpv)
      real*4 pfcd(mth),rv(mth),pv(mth),PSEMRA
      integer*4 orow(mpixo),ocol(mpixo)
      logical profpoly
C
      common / PSEBLK / pff,PSE_pparm,PSE_pval,pfcd,rv,pv,PSEMRA,
     .                  ocol,orow,profpoly
C
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C Temporary arrays:
C   orow,ocol    = used in SkyMaskTwo when fitting/rejected pixels.
C   pfcd         = profile-centroid-distances used in profile modeling.
C   rv,pv        = row-value and profile-value used in profile modeling.
C   pff          = polynomial coeffecient arrays used in profile modeling.
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C Parameters:
C   PSEMRA       = PSE Sky Rejection Adjustment (initially set at 1).
C..........
C   pval         = profile model values.
C   pparm        = parameters for profile model values (see notes below).
C   neo          = Current number of echelle orders.
C   profpoly     = if .true. write out profile fit plots as PS files.
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C      Other notes:
C   PSE_pparm(1) = number of values in profile model
C   PSE_pparm(2) = starting x value
C   PSE_pparm(3) = delta x value (x increment between array elements)
C   PSE_pparm(4) = centroid.
C   PSE_pparm(5) = background level of profile.
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
