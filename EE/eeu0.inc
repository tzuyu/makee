C This file is eeu0.inc .

C:::::::::::::::::::::::::
C Version of keck processing software.
      character*40 KP_VERSION
      parameter(KP_VERSION='1.3 : November 1996')

C::::::::::::::::::::::::::::::::::
C Maximum number of FITS files is mf, actual number is nf.
      integer*4 mf,nf,nflat
      parameter(mf=900)
      character*80 object(mf),fitsfile(mf),MaskFile,utdate(mf)
      character*80 uttime(mf),flatfile(mf)
      character*4 imtype(mf),lamp(mf),filt(mf),shutter(mf)
      character*4 hatch(mf),deck(mf)
      integer*4 obsnum(mf),expos(mf)
      integer*4 medmax(mf),highmax(mf),lowmax(mf),flatimno(mf)
      real*4 echa(mf),xda(mf)
      common /KECKUTILBLK/ object,fitsfile,MaskFile,utdate,uttime,
     .                     flatfile,imtype,lamp,filt,shutter,hatch,deck,
     .                     obsnum,expos,medmax,highmax,lowmax,flatimno,
     .                     nf,nflat,echa,xda
C::::::::::::::::::::::::::::::::::
