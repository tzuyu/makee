C rmlamp.f     Read FITS header from a file.

      implicit none
      character*80 fn,arg(999),wrd
      character hdr*57600
      integer*4 narg,nfile
      logical dark,darkclos,lhead,listall,listonly
      logical findarg,rmit,ok
      character*80 chead,object,lamp,obj
      character*80 lampname,lfilname,filename,mvdir
      integer*4 ttime,lc,i,frameno,inhead
      real*8 fhead,xdangl,echangl,valread8,minexp,maxexp,r8
      real*8 ech,xd
 
      call arguments(arg,narg,999)
C
      lamp = 'rmlamp-no-lampname given'
      if (findarg(arg,narg,'lamp=',':',wrd,i)) lamp = wrd
      object = 'rmlamp-no-object-name-listed'
      if (findarg(arg,narg,'object=',':',wrd,i)) object = wrd
      echangl = -1.e+30
      if (findarg(arg,narg,'echangl=',':',wrd,i))echangl=valread8(wrd)
      xdangl = -1.e+30
      if (findarg(arg,narg,'xdangl=',':',wrd,i)) xdangl =valread8(wrd)
      minexp = -1.
      if (findarg(arg,narg,'minexp=',':',wrd,i)) minexp =valread8(wrd)
      maxexp = 1.e+30
      if (findarg(arg,narg,'maxexp=',':',wrd,i)) maxexp =valread8(wrd)
C
      dark    = findarg(arg,narg,'-dark',':',wrd,i)
C
      listonly= findarg(arg,narg,'-listonly',':',wrd,i)
      listall = findarg(arg,narg,'-listall',':',wrd,i)
      mvdir = ' '
      if (findarg(arg,narg,'dir=',':',wrd,i)) mvdir = wrd
C
      if (narg.lt.1) then
        print *,'Syntax:  rmlamp (FITS file(s))'
        print *,'    [ lamp= ] [ -listonly ] [ -listall ]'
        print *,'    [ dir= ]  [ object= ]'
        print *,'    [ echangl= ]  [ xdangl= ]'
        print *,'    [ minexp= ] [ maxexp= ]  [-dark]'
        print *,' '
        print *,'  Remove specified Keck raw or reduced data files.'
        print *,' '
        print *,'lamp=    : Remove with this LAMPNAME card.'
        print *,'-dark    : Remove with "DARKCLOS = T".'
        print *,'object=  : Remove with this OBJECT card.'
        print *,'echangl= : Remove with this ECHANGL (within 0.001).'
        print *,'xdangl=  : Remove with this XDANGL (within 0.001).'
        print *,'minexp=  : Remove below this TTIME time.'
        print *,'maxexp=  : Remove above this TTIME time.'
        print *,' '
        print *,'-listonly: Only list the files that would be removed.'
        print *,
     . '-listall : List all files whether they are selected or not.'
        print *,'dir=     : Move to this directory (do not delete.)'
        print *,' '
        stop
      endif

C Check options.
      DO nfile=1,narg

C Read FITS file header.
      call AddFitsExt(arg(nfile))
      fn = arg(nfile)
      call readfitsheader(fn,hdr,ok)
      if (.not.ok) goto 900

C Check for reasons to remove.
      rmit = .false.

C Echelle angle.
      r8 = fhead('ECHANGL',hdr)
      if (abs(r8 - echangl).lt.0.001) rmit = .true.
      r8 = fhead('XDANGL',hdr)
      if (abs(r8 - xdangl).lt.0.001) rmit = .true.
      r8 = fhead('TTIME',hdr)
      if (r8.lt.minexp) rmit = .true.
      if (r8.gt.maxexp) rmit = .true.
      wrd = chead('LAMPNAME',hdr)
      if (index(wrd,lamp(1:lc(lamp))).gt.0) rmit = .true.
      wrd = chead('OBJECT',hdr)
      if (index(wrd,object(1:lc(object))).gt.0) rmit = .true.
      darkclos = lhead('DARKCLOS',hdr) 
      if ((darkclos).and.(dark)) rmit = .true.

C Get values for listing.
      frameno = inhead('FRAMENO',hdr)
      ech     = fhead('ECHANGL',hdr)
      xd      = fhead('XDANGL',hdr)
      lampname= chead('LAMPNAME',hdr)
      lfilname= chead('LFILNAME',hdr)
      obj     = chead('OBJECT',hdr)
      ttime   = inhead('TTIME',hdr)
      darkclos= lhead('DARKCLOS',hdr) 
      filename= arg(nfile)(1:index(arg(nfile),'.fits')-1)

      if (rmit) then

      print '(i4,2x,a8,2f8.4,2x,a,2x,a,i5,2a)',
     .            frameno,filename,ech,xd,
     .            lampname(1:5),lfilname(1:5),ttime,'s  ',obj(1:28)

      if (.not.listonly) then
        if (mvdir.eq.' ') then
          write(wrd,'(2a)') 
     .  '/bin/rm -f ',arg(nfile)(1:lc(arg(nfile)))
          print '(a)',wrd(1:lc(wrd))
          call system(wrd)
        else
          write(wrd,'(4a)') 
     .            '/bin/mv -f ',arg(nfile)(1:lc(arg(nfile))),
     .            ' ',mvdir(1:lc(mvdir))
          print '(a)',wrd(1:lc(wrd))
          call system(wrd)
        endif
      endif

      else

        if (listall) then
          print '(i4,2x,a8,2f8.4,2x,a,2x,a,i5,2a)',
     .            frameno,filename,ech,xd,
     .            lampname(1:5),lfilname(1:5),ttime,'s  ',obj(1:28)
        endif

      endif

      ENDDO

      stop

900   print *,'Error reading FITS header from file : ',fn
      call exit(1)
      end 
