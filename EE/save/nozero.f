C ------------------------------------------------------------------------------
C Interpolate across zero pixels.
C
      implicit none
      integer*4 maxpt
      parameter(maxpt=5290000)  
      integer*4 narg,inhead,lc,sc,sr,nc,nr,ec,er
      real*4 a(maxpt)
      character header*57600
      character*80 file1,file2,arg(9)
      logical ok
C
      call arguments(arg,narg,9)
      if (narg.lt.2) then
        print *,'Syntax:  nozero (FITS file) (Modified FITS file)'
        print *,
     .  '    All the exactly zero data points in the FITS file are'
        print *,'    replaced with interpolated values.'
        call exit(0)
      endif
      file1 = arg(1)
      call AddFitsExt(file1)
      file2 = arg(2)
      call AddFitsExt(file2)
C Read input file.
      call readfits(file1,a,maxpt,header,ok)
      if (.not.ok) goto 801
C Dimensions.
      sc = inhead('CRVAL1',header)
      sr = inhead('CRVAL2',header)
      nc = max(1,inhead('NAXIS1',header))
      nr = max(1,inhead('NAXIS2',header))
      ec = sc + nc - 1
      er = sr + nr - 1
C Interpolate zero points.
      call fits_zero_interp(a,sc,ec,sr,er,10)
C Write out output file.
      print '(2a)','  Writing file : ',file2(1:lc(file2))
      call writefits(file2,a,header,ok)
      stop
801   print *,'Error reading FITS image.'
      stop
      end
