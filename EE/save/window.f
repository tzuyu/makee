C window.f                                                    tab  Feb98
C
C Window a 2-D image given a sub-region.
C
      implicit none
C
      include 'maxpix.inc'
      include 'soun.inc'
C
      real*4 a(maxpix)
      real*4 valread
      integer*4 i,narg,lc,nc,nr,new_sc,new_ec,new_sr,new_er
      integer*4 sc,ec,sr,er
      character*57600 header
      character*80 arg(9),infile,outfile,wrds(9)
      logical ok,findarg

C Standard output unit number.
      soun = 6

C Command line arguments.
      call arguments(arg,narg,9)
      new_sc = 0
      new_ec = 0
      new_sr = 0
      new_er = 0
      if (findarg(arg,narg,'box=',',',wrds,i)) then
        if (i.eq.4) then
          new_sc = nint(valread(wrds(1)))
          new_ec = nint(valread(wrds(2)))
          new_sr = nint(valread(wrds(3)))
          new_er = nint(valread(wrds(4)))
        endif
      endif

C Syntax.
      if ((narg.ne.2).or.(new_ec.eq.0)) then
        print *,'Syntax:  window  (input FITS file)  (output file)'
        print *,'                          box=sc,ec,sr,er'
        print *,'  Window a 2-D FITS file.'
        print *,'    box= : Specify sub-window in image.'
        call exit(0)
      endif

C Read files.
      infile = arg(1)
      call AddFitsExt(infile)
      outfile= arg(2)
      call AddFitsExt(outfile)
      call readfits(infile,a,(maxpix),header,ok)
      if (.not.ok) goto 801
      call GetDimensions(header,sc,ec,sr,er,nc,nr)

      print '(a,4i5)',' Old dimensions (sc,ec,sr,er) : ',sc,ec,sr,er
      print '(a,4i5)',' New dimensions (sc,ec,sr,er) : ',
     .                                  new_sc,new_ec,new_sr,new_er
      if ( (new_sr.lt.sr).or.(new_er.gt.er).or.
     .     (new_sc.lt.sc).or.(new_ec.gt.ec).or.
     .     (new_sr.gt.new_er).or.(new_sc.gt.new_ec) ) then
        print *,'Error specifying new box region.'
        call exit(1)
      endif

C Now window the image.
      call window_fits(a,header,new_sc,new_ec,new_sr,new_er,ok)
      if (.not.ok) goto 802

C Write file.
      print '(2a)',' Writing : ',outfile(1:lc(outfile))
      call writefits(outfile,a,header,ok)
      if (.not.ok) goto 801

      stop
801   print *,'Error writing/reading FITS image.'
      stop
802   print *,'Error windowing image.'
      stop
      end
