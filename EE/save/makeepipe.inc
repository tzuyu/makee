C This file is makeepipe.inc                tab Nov 98
C
C
      include 'makee_big.inc'   ! For scratch images: obj() and scr().
C
      character*80 userbiasfile ! User Bias file.
      character*80 flatlistfile ! Flat list file.
      character*80 pffile       ! Parameter File file (e.g. makee.param)
      character*80 rawdir       ! Raw HIRES image file directory.
      character*80 lpr          ! Printer name.
      character*80 observer     ! New OBSERVER card.
      character*80 pre_fix      ! prefix of RAW FITS filenames.
      character*80 maskfile     ! Filename of bad column masks.
C
      character*8 timeflag      ! time flag ((partial) seconds since 1/1/1970)
C
      real*4 saturation         ! Saturation level in DN.
      real*4 user_eperdn        ! User e-/DN
      real*4 user_ronoise       ! User Readout noise in electrons.
      real*4 echtol             ! Echelle angle tolerance.
      real*4 xdtol              ! Cross disperser angle tolerance.
      real*4 slittol            ! Slit width tolerance.
      real*4 fttol              ! Flat time tolerance in minutes.
C
      logical step(6)           ! Execution switch for all steps.
      logical noobject          ! Do not change object name.
      logical rename            ! Rename Quartz and ThAr object names.
      logical update            ! Only do objects without a Flux-*fits file.
      logical nomedian          ! Do not calculate medians for listings.
      logical testmode          ! Un-advertised test mode.
      logical lesspr            ! Less output to printer.
      logical logit             ! Send makee output to log file.
      logical UseAddArcs        ! Use additional arclamps (arc1add=,arc2add=).
      logical cycle             ! Loop mode?
C
      common / MAKEEPIPEBLK80 / userbiasfile,flatlistfile,
     .                          pffile,rawdir,lpr,
     .                          observer,pre_fix,maskfile

      common / MAKEEPIPEBLK8 / timeflag

      common / MAKEEPIPEBLK4 / saturation,user_eperdn,user_ronoise,
     .                         echtol,xdtol,slittol,fttol

      common /MAKEEPIPEBLK1 / step,noobject,rename,update,
     .                        nomedian,testmode,lesspr,logit,
     .                        UseAddArcs,cycle

C
      integer*4 mf
      parameter(mf=900)           ! Maximum number of files.
C
      character*200 lline(mf)     ! image.List LINE (scratch array).
      character*80 fi(mf)         ! Scratch array for file list.
      character*80 mp_fi(mf)      ! File list.
      character*80 mp_object(mf)  ! Object name.
      character*16 mp_coord(mf)   ! Coordinate name.
      character*4 mp_type(mf)     ! Image type.
      character*4 mp_fil1(mf)     ! Filter#1.
      character*4 mp_lfil(mf)     ! Lamp filter.
      character*4 mp_deck(mf)     ! Decker name.
      real*4 hi(mf)               ! High value scratch array.
      real*4 mp_expos(mf)         ! Exposure time.
      real*4 mp_echa(mf)          ! Echelle angle.
      real*4 mp_xda(mf)           ! Cross disperser angle.
      real*4 mp_slit(mf)          ! Slit width (SLITWID card).
      real*4 mp_mdn(mf)           ! Max. Column median.
      real*4 mp_high(mf)          ! Max. Column high.
      real*4 mp_low(mf)           ! Max. Column low.
      integer*4 mp_obs(mf)        ! Observation number.
      integer*4 mp_xbin(mf)       ! X Binning factor.
      integer*4 mp_ybin(mf)       ! Y Binning factor.
      integer*4 mp_time(mf)       ! Time in seconds since 1/1/1970
C                                 !    (for middle of exposure).
      integer*4 mp_flag(mf)       ! Flag used for various purposes.
      integer*4 mp_nfi            ! Number of files in file list.
      integer*4 start_time        ! Start of cycle (sec. 1/1/70).
      integer*4 stop_time         ! Requested time to stop cycle (sec. 1/1/70).
C
      common / MPBLK / lline,fi,mp_fi,mp_object,mp_coord,
     .                 mp_type,mp_fil1,mp_lfil,mp_deck,
     .                 hi,mp_expos,mp_echa,mp_xda,mp_slit,
     .                 mp_mdn,mp_high,mp_low,
     .                 mp_obs,mp_xbin,mp_ybin,mp_time,mp_flag,mp_nfi,
     .                 start_time,stop_time


