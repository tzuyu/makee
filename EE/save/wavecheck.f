C wavecheck.f                    tab April 1999
C
      implicit none
      integer*4 narg,sc,ec,sr,er,nc,nr1,nr2,pixinc,narr,i,row,lc,nn
      real*8 coef1(0:7),coef2(0:7),polyval,disp,arr1(9000),arr2(9000)
      real*8 dx,dy,hidx,hidy,mdndx,mdndy,meandx,meandisp,meanwave
      real*4 xx(9000),yy(9000),yr,xmin,xmax,ymin,ymax
      character*57600 header1,header2
      character*80 arg(9),file1,file2,wrd,chead,wv0,wv4
      character c7*7
      logical findarg,plotit,verbose,ok
C
      include 'soun.inc'
C
      soun=6

C Get arguments.
      call arguments(arg,narg,9)
      plotit = findarg(arg,narg,'-plot',':',wrd,i)
      verbose= findarg(arg,narg,'-verbose',':',wrd,i)

C Syntax.
      if (narg.ne.2) then
        print *,
     .  'Syntax:  wavecheck (HIRES FITS file #1) (HIRES FITS file #2)'
        print *,'                        [-plot]  [-verbose]'
        print *,' ' 
        print *,' Compare wavelength scales of two FITS files.' 
        print *,
     .  ' The mean pixel differences reported are the wavelength'
        print *,
     .  ' differences (1-2) at a column, divided by the dispersion.'
        print *,' ' 
        print *,
     .  ' For example, if the wavelength at column 100 in file#1 is'
        print *,
     .  ' 5000.0, and the wavelength at column 100 in file#2 is 5000.3'
        print *,
     .  ' and the dispersion in 0.15 Angstroms/pixel,then the reported'
        print *,
     .  ' mean pixel difference is -2 pixels.  This means that file#1'
        print *,' has a bluer wavelength range than file#2.'
        print *,' ' 
        stop
      endif

C Files.
      file1 = arg(1)
      call AddFitsExt(file1)
      file2 = arg(2)
      call AddFitsExt(file2)

C Read headers.
      call readfits_header(file1,header1,ok)
      if (.not.ok) stop
      call GetDimensions(header1,sc,ec,sr,er,nc,nr1)
      call readfits_header(file2,header2,ok)
      if (.not.ok) stop
      call GetDimensions(header2,sc,ec,sr,er,nc,nr2)
      if (nr2.ne.nr1) then
        write(soun,'(a)') 
     .  'ERROR: Number of rows do not match: ',nr1,nr2
        stop
      endif

C Verbose mode.
      if (verbose) then
        pixinc = 100
      else
        pixinc = 100
      endif

C Plotting.
      if (plotit) then
        call PGBEGIN(0,'/XDISP',1,1)
      endif

C Column headers.
      write(soun,'(2a)') 'Row  Max|PixDiff|  Mdn|PixDiff|  Mean',
     .                   '(PixDiff)  Mean(Disp)  Mean(Wave)'

C Row by row...
      DO row=sr,er
C
      if (verbose) write(soun,'(a,i5)') 
     .  '...........................ROW= ',row
C
C Extract coeffecients from first header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,header1)
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) 
     .  coef1(0),coef1(1),coef1(2),coef1(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,header1)
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) 
     .  coef1(4),coef1(5),coef1(6),coef1(7)
      if (verbose) then
        write(soun,'(a,4(1pe15.7))') 'coef1=',(coef1(i),i=0,3)
        write(soun,'(a,4(1pe15.7))') '      ',(coef1(i),i=4,7)
      endif
C Extract coeffecients from second header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,header2)
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) 
     .  coef2(0),coef2(1),coef2(2),coef2(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,header2)
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) 
     .  coef2(4),coef2(5),coef2(6),coef2(7)
      if (verbose) then
        write(soun,'(a,4(1pe15.7))') 'coef2=',(coef2(i),i=0,3)
        write(soun,'(a,4(1pe15.7))') '      ',(coef2(i),i=4,7)
      endif
C
      hidy = -0.0001
      hidx = -0.0001
      ymin = +1.e+30
      ymax = -1.e+30
      meandx = 0.d0
      meandisp = 0.d0
      meanwave = 0.d0
      nn   = 0
      narr = 0
C
      do i=sc,ec,pixinc
C Angstroms per pixel.
        disp=(polyval(7,coef1,dfloat(i+10))-
     .        polyval(7,coef1,dfloat(i-10)))/20.d0
C Angstrom difference.
        dy  = polyval(7,coef1,dfloat(i)) - polyval(7,coef2,dfloat(i))
C Pixel difference.
        dx  = dy / disp
C Maximums
        meandx  = meandx + dx
        meandisp= meandisp + disp
        meanwave= meanwave + polyval(7,coef1,dfloat(i))
C Verbosity.
        if (verbose) then
          write(soun,'(a,i6,3(a,f8.4))') 
     .           'Col=',i,'   Ang=',dy,'   Pix=',dx,'   Disp=',disp
        endif
C Absolute values.
        dx = abs(dx)
        dy = abs(dy)
        if (dy.gt.hidy) hidy = dy
        if (dx.gt.hidx) hidx = dx
C Store.
        narr=narr+1
        arr1(narr) = dx
        arr2(narr) = dy
C Plotting.
        nn=nn+1
        xx(nn)=float(i)
        yy(nn)=sngl( (polyval(7,coef1,dfloat(i))-
     .                polyval(7,coef2,dfloat(i)) ) / disp )
        if (yy(nn).gt.ymax) ymax=yy(nn)
        if (yy(nn).lt.ymin) ymin=yy(nn)
      enddo
C Plot.
      if (plotit) then
        call PGPAGE
        ymin = min(-0.10,ymin)
        ymax = max(+0.10,ymax)
        yr = max(0.00001,ymax-ymin)
        xmin = float(sc-10)
        xmax = float(ec+10)
        call PGWINDOW(xmin,xmax,ymin-(yr*0.07),ymax+(yr*0.07))
        call PGBOX('BCNST',0.,0,'BCNST',0.,0)
        call PGPT(nn,xx,yy,5)
        call PGSLS(4)
        call PG_HLine(xmin,xmax,0.)
        call PGSLS(1)
        write(wrd,'(a,i4)') 'Row=',row
        call PGMTXT('T',0.3,0.5,0.5,wrd(1:lc(wrd)))
        call PGMTXT('L',2.3,0.5,0.5,'Pixel Difference')
        call PGMTXT('B',2.3,0.5,0.5,'Column Number')
      endif

      call find_median8(arr1,narr,mdndx)
      call find_median8(arr2,narr,mdndy)
      write(soun,'(i3,4(f13.4),f13.2)') row,hidx,mdndx,
     .        meandx/dfloat(narr),meandisp/dfloat(narr),
     .        meanwave/dfloat(narr)
C
      ENDDO
C
      if (plotit) call PGEND

      stop
800   continue
      write(soun,'(a)') 
     .  'ERROR: wavecheck:reading or writing wavelength values.'
      stop
      end

