C rotate.f                                                    tab  Feb98
C
C Rotate a 2-D image in increments of 90 degrees.
C
      implicit none
C
      include 'maxpix.inc'
      include 'soun.inc'
C
      real*4 a(maxpix)
      real*4 b(maxpix)
      real*4 valread,angle
      integer*4 i,narg,lc
      character*57600 header
      character*80 arg(9),infile,outfile,wrd
      logical ok,findarg

C Standard output unit number.
      soun = 6

C Command line arguments.
      call arguments(arg,narg,9)
      angle=0.
      if (findarg(arg,narg,'angle=',',',wrd,i)) angle=valread(wrd)

C Syntax.
      if (narg.ne.2) then
        print *,'Syntax:  rotate (input FITS file) (output FITS file)'
        print *,'                          (angle=)'
        print *,'  Rotate a 2-D FITS image file in increments',
     .          ' of 90 degrees.'
        print *,'  angle=  :  Specify angle to rotate, either:'
        print *,'            -270, -180, -90, 0, 90, 180, or 270.'
        call exit(0)
      endif

C Read files.
      infile = arg(1)
      call AddFitsExt(infile)
      outfile= arg(2)
      call AddFitsExt(outfile)
      call readfits(infile,a,maxpix,header,ok)
      if (.not.ok) goto 801

      call rotate_fits(a,b,header,angle,ok)
      if (.not.ok) goto 802

C Write file.
      print '(2a)',' Writing : ',outfile(1:lc(outfile))
      call writefits(outfile,a,header,ok)
      if (.not.ok) goto 801

      stop
801   print *,'Error writing or reading FITS image.'
      stop
802   print *,'Error rotating image.'
      stop
      end

