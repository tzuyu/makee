/* fitpoly.c                  tab  may01 */

#include "ctab.h"
#include "cmisc.h"
#include "fstuff.h"
#include "cufio.h"

/* ----------------------------------------------------------------------
  FOR GALEX calibration test #54..... tab 11jun01
  Special routine for combining two polynomial solutions and
  applying a special wavelength offset.
*/
void special_poly_combine( int galex_order )
{
/**/
char file[100];
char wrd[100];
char xyfile[100];
char fitfile[100];
char polyfile[100];
char c3[3];
/**/
int ii,order1,order2,order,iok,nn;
/**/
double coef1[99],coef2[99];
double xoff1,xoff2,xx;
double coef[99],xoff,high,wrms,wppd,ff;
double x8[9000],y8[9000],w8[9000];
/**/
FILE *infu;
FILE *outfu;
/**/

order = 3;

if (galex_order < 0) { 
  sprintf(c3,"%2d",galex_order);
} else {
  sprintf(c3,"+%1d",galex_order);
}

sprintf(polyfile,"fuv_order%s.poly",c3);
sprintf( fitfile,"fuv_order%s.fit",c3);
sprintf( xyfile, "fuv_order%s.xy",c3);

sprintf(file,"1/order%s.poly",c3);
infu = fopen(file,"r");
ii = fgetline(wrd,infu); order1  = GLV(wrd,1);
ii = fgetline(wrd,infu); xoff1   = GLV(wrd,1);
ii = fgetline(wrd,infu); coef1[0]= GLV(wrd,1);
ii = fgetline(wrd,infu); coef1[1]= GLV(wrd,1);
ii = fgetline(wrd,infu); coef1[2]= GLV(wrd,1);
ii = fgetline(wrd,infu); coef1[3]= GLV(wrd,1);
fclose(infu);

sprintf(file,"2/order%s.poly",c3);
infu = fopen(file,"r");
ii = fgetline(wrd,infu); order2  = GLV(wrd,1);
ii = fgetline(wrd,infu); xoff2   = GLV(wrd,1);
ii = fgetline(wrd,infu); coef2[0]= GLV(wrd,1);
ii = fgetline(wrd,infu); coef2[1]= GLV(wrd,1);
ii = fgetline(wrd,infu); coef2[2]= GLV(wrd,1);
ii = fgetline(wrd,infu); coef2[3]= GLV(wrd,1);
fclose(infu);

/* Load values. */
nn=0;

/* NUV: for (xx=1750.; xx<3001.; xx=xx+10) { */

for (xx=1210.; xx<1851.; xx=xx+10) {
 x8[nn] = xx;  y8[nn] = cpolyval(order1+1,coef1,x8[nn] - xoff1);  w8[nn]=1.;
 ++nn;
 x8[nn] = xx;  y8[nn] = cpolyval(order2+1,coef2,x8[nn] - xoff2);  w8[nn]=1.;
 ++nn;
}

/* Polynomial fit. */
iok  = 0;
f2cpolyfitglls_ ( &nn, x8, y8, w8, &order, coef, &iok );
xoff = 0.;

/* Report residuals. */
f2cpolyfitgjresiduals_ ( &nn, x8, y8, w8, &order, coef, &xoff, &high, &wrms, &wppd );
printf("NOTE: Residuals: high=%9.4f  wrms=%8.4f  wppd=%8.4f\n",high,wrms,wppd);


/* .poly file */
outfu = fopen(polyfile,"w");
printf("Writing: %s\n",polyfile);
fprintf(outfu,"%15d = order\n",order);
fprintf(outfu,"%15.7e = xoff\n",xoff);
for (ii=0; ii<=order; ++ii) { 
  fprintf(outfu,"%15.7e = coef[%2.2d]\n",coef[ii],ii);
}
fprintf(outfu,"%15d = nn\n",nn);
fprintf(outfu,"%15.4f = high (residual)\n",high);
fprintf(outfu,"%15.4f = wrms\n",wrms);
fprintf(outfu,"%15.4f = wppd\n",wppd);
fclose(outfu);


/* .fit file. */
outfu = fopen(fitfile,"w");
for (ii=0; ii<nn; ++ii) {
  ff = cpolyval(order+1,coef,x8[ii] - xoff);
  fprintf(outfu,"%15.7e %15.7e\n",x8[ii],ff);
}
fclose(outfu);


/* .xy file. */
outfu = fopen(xyfile,"w");
for (ii=0; ii<nn; ++ii) {
  fprintf(outfu,"%15.7e %15.7e\n",x8[ii],y8[ii]);
}
fclose(outfu);


return;
}



/* - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Fit a polynomial to a set of points.
  (This was tailored for doing the GALEX dispersion ground testing
   calibration analyses.  -tab 08jun01)
*/
int main(int argc, char *argv[])
{
/**/
char arg[100][100];
char xyfile[100];
char fitfile[100];
char polyfile[100];
char wrd[100];
/**/
int ii,nn,narg,order,glls;
int iok,forcexoff;
/**/
FILE *infu;
FILE *outfu;
/**/
const int maxcol = 9000;
double xx8[maxcol],yy8[maxcol],ww8[maxcol],coef[99];
double xv1,xv2,xv,ff,xoff,high,wrms,wppd,adj_disp;
double front_off,back_off;
/**/
int narr; double arr8[9000],disp,ff2,ff1;
/**/

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Options. */
order = 1;
if (cfindarg(arg,&narg,"ord=",':',wrd) == 1) order = GLV(wrd,1);

adj_disp = 1.0;
if (cfindarg(arg,&narg,"ad=",':',wrd) == 1) adj_disp = GLV(wrd,1);

glls = 0;
if (cfindarg(arg,&narg,"-glls",'s',wrd) == 1) glls = 1;

front_off = -1.e+30;
if (cfindarg(arg,&narg,"front_off=",':',wrd) == 1) front_off = GLV(wrd,1);

back_off = -1.e+30;
if (cfindarg(arg,&narg,"back_off=",':',wrd) == 1) back_off = GLV(wrd,1);

/* Syntax. */
if ( narg != 1 ) {
  printf("Syntax: fitpoly  (.xy file)  [ord=]  [-glls]\n");
  printf("                      [ front_off= ]  [ back_off= ]\n");
  printf("\n");
  printf("  Fit a polynomial to a set of points in a x,y ASCII file.\n");
  printf("  Also creates a .fit and a .poly file.\n");
  printf("\n");
  printf("Options:\n");
  printf("  ord=  : Polynomial order to fit (def: 1).\n");
  printf("  -glls : Use GLLS method (good for orders < 7).\n");
  printf("front_off=v : Adjust y values like this:  (new y) = v - (old_y)\n");
  printf(" back_off=v : Adjust y values like this:  (new y) = (old_y) - v\n");
  printf("\n");
  exit(0);
}

/* Filename. */
strcpy(xyfile,arg[1]);

/* Special. */
if (strcmp(xyfile,"combine") == 0) {
  special_poly_combine( order );
  exit(0);
}

/* .fit (fit points) filename. */
strcpy(fitfile,xyfile);
ii = cindex(fitfile,".xy");
fitfile[ii+1]='f'; fitfile[ii+2]='i'; fitfile[ii+3]='t'; fitfile[ii+4]='\0';

/* .poly (poly coeffecients) filename. */
strcpy(polyfile,xyfile);
ii = cindex(polyfile,".xy");
polyfile[ii+1]='p'; polyfile[ii+2]='o'; polyfile[ii+3]='l';
polyfile[ii+4]='y'; polyfile[ii+5]='\0';

/* Read data from .xy file. */
printf("Reading %s ...\n",xyfile);
infu = fopen(xyfile,"r");
if (infu == NULL) {
  fprintf(stderr,"***ERROR: Opening xyfile for reading: %s\n",xyfile);
  exit(0);
}
nn=0;
while (fgetline(wrd,infu) == 1) {
  xx8[nn] = GLV(wrd,1);
  yy8[nn] = GLV(wrd,2);
  ww8[nn] = GLVQ(wrd,3);
  if (ww8[nn] < 0.) ww8[nn] = 1.0;
  ++nn;
}
fclose(infu);

/* Front off. */
if (front_off > -1.e+29) {
  for (ii=0; ii<nn; ++ii) { yy8[ii] = front_off - yy8[ii]; }
}

/* Back off. */
if (back_off > -1.e+29) {
  for (ii=0; ii<nn; ++ii) { yy8[ii] = yy8[ii] - back_off; }
}

/* Polynomial fit. */
xoff      = 0.;
forcexoff = 0;
iok       = 0;
if (glls == 1) {
  f2cpolyfitglls_ ( &nn, xx8, yy8, ww8, &order, coef, &iok );
  xoff = 0.;
} else {
  f2cpolyfitgj_ ( &nn, xx8, yy8, ww8, &order, coef, &xoff, &forcexoff, &iok );
}


/* Report residuals. */
f2cpolyfitgjresiduals_ ( &nn, xx8, yy8, ww8, &order, coef, &xoff, &high, &wrms, &wppd );
printf("NOTE: Residuals: high=%9.4f  wrms=%8.4f  wppd=%8.4f\n",high,wrms,wppd);


/* .poly file */
outfu = fopen(polyfile,"w");
fprintf(outfu,"%15d = order\n",order);
fprintf(outfu,"%15.7e = xoff\n",xoff);
for (ii=0; ii<=order; ++ii) { 
  fprintf(outfu,"%15.7e = coef[%2.2d]\n",coef[ii],ii);
}
fprintf(outfu,"%15d = nn\n",nn);
fprintf(outfu,"%15.4f = high (residual)\n",high);
fprintf(outfu,"%15.4f = wrms\n",wrms);
fprintf(outfu,"%15.4f = wppd\n",wppd);
fclose(outfu);


/* .fit file. */
outfu = fopen(fitfile,"w");
for (ii=0; ii<nn; ++ii) {
  ff = cpolyval(order+1,coef,xx8[ii] - xoff);
  fprintf(outfu,"%15.7e %15.7e\n",xx8[ii],ff);
}
fclose(outfu);



/* ### */
narr=0;
for (ii=1; ii<(nn-1); ++ii) {
  ff1= cpolyval(order+1,coef,xx8[ii-1] - xoff);
  ff2= cpolyval(order+1,coef,xx8[ii+1] - xoff);
  arr8[narr] = ((ff2 - ff1) / (xx8[ii+1] - xx8[ii-1]) ); 
  ++narr;
}
disp = cfind_median8( narr, arr8 );
printf("NOTE: median disp = %10.4f  (%9.4f )\n",disp,(disp / adj_disp) );
/* ### */


/* .ffit file. */
strcpy(fitfile,xyfile);
ii = cindex(fitfile,".xy");
fitfile[ii+1]='f'; fitfile[ii+2]='f'; fitfile[ii+3]='i'; 
fitfile[ii+4]='t'; fitfile[ii+5]='\0';
outfu = fopen(fitfile,"w");
xv1 = xx8[0]    - ((xx8[nn-1] - xx8[0]) * 0.20);
xv2 = xx8[nn-1] + ((xx8[nn-1] - xx8[0]) * 0.20);
for (xv=xv1; xv<xv2; xv=xv+1.0) {
  ff = cpolyval(order+1,coef,xv - xoff);
  fprintf(outfu,"%15.7e %15.7e\n",xv,ff);
}
fclose(outfu);

/* .nxy file (new .xy). */
strcpy(fitfile,xyfile);
ii = cindex(fitfile,".xy");
fitfile[ii+1]='n'; fitfile[ii+2]='x'; fitfile[ii+3]='y'; fitfile[ii+4]='\0';
outfu = fopen(fitfile,"w");
for (ii=0; ii<nn; ++ii) {
  fprintf(outfu,"%15.7e %15.7e\n",xx8[ii],yy8[ii]);
}
fclose(outfu);


return(0);
}



