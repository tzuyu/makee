

      implicit none
      character*80 arg(9)
      integer*4 narg
      logical ok

      call arguments(arg,narg,9)
 
      call mk_sss(arg,narg,ok)

      stop
      end



C----------------------------------------------------------------------
C sss.f : Same-Setup-Sums                        tab  May 1999
C
      subroutine mk_sss(arg,narg,ok)
C
      implicit none
      character*(*) arg(*)
      integer*4 narg
      logical ok
C
      integer*4 m
      parameter(m=999)
C
      real*8 ra(m),dec(m),echa(m),xda(m),frtime(m),expos(m)
      character*80 ffile(m),efile(m),object(m)
      character*8 deck(m)
      integer*4 nc(m),nr(m),used(m),match(m)
C
      real*8 echtol,xdtol,minexpos
      character*80 file,wrd
      integer*4 nff,i,n,ii,lc,jj
      logical sss_match,logtemp
C
C Defaults.
      echtol  = 0.0005d0
      xdtol   = 0.005d0
      minexpos= 899.d0
      do i=1,m
        used(i)=0
      enddo
C
      call arguments(arg,narg,9)
C
      if (narg.ne.1) then
        print *,'Syntax: sss (file with list of Flux-???.fits files)'
        print *,
     . '    or: sss "all" (for all Flux-???.fits in current directory)'
        print *,' '
        print *,
     .  '  Same setup sums: sums up Flux-???.fits with the same'
        print *,
     .  '    object name, coordinates, setup angles, etc.  Also'
        print *,'    creates error file corresponding to sum.'
        print *,
     .  '    Output:  sss(obs#)-(# summed)_(7 letter RA+DEC)[e].fits '
        ok=.false.
        return
      endif
C
C List files?
      if (arg(1).eq.'all') then
        wrd = '/bin/ls Flux-???.fits >! sss.list.input'
        call fsysp(wrd)
        arg(1)='sss.list.input'
      endif

C Open file with list of files.
      open(1,file=arg(1),status='old')
      nff=0
C Load filenames and parameters.
1     continue
      read(1,'(a)',end=5) file
      call AddFitsExt(file)
      i = index(file,'Flux-')
      if (i.eq.0) then
        print *,
     .  'ERROR: Files must be in the format: Flux-???.fits : ',file
        ok=.false.
        return
      endif
      nff = nff + 1
      ffile(nff) = file
      file = file(1:i-1)//'Err'//file(i+4:lc(file))
      efile(nff) = file
C Load variables.
      call sss_load(ffile(nff),ra(nff),dec(nff),echa(nff),xda(nff),
     . frtime(nff),expos(nff),object(nff),deck(nff),nc(nff),nr(nff),ok)
      if (.not.ok) return
C Exposure time too small?  Then subtract this entry.
      if (expos(nff).lt.minexpos) nff=nff-1
      goto 1
5     continue
      close(1)
C
C Find matching objects.
      ii = 1
      DO WHILE(ii.le.nff)
      IF (used(ii).eq.0) THEN
C
C Find matches.
      n=1
      match(n)= ii
      used(ii) = 1
      do jj=ii+1,nff
        if (used(jj).eq.0) then
Ctemp
CCC     print *,'test: ',ii,jj,' : ',ffile(ii)(1:20),ffile(jj)(1:20)
Ctemp
        logtemp = sss_match(ii,jj,ra,dec,echa,xda,frtime,object,
     .                               deck,nc,nr,echtol,xdtol)
        if (logtemp) then
          n=n+1
          match(n)= jj
          used(jj)= 1
        endif
        endif
      enddo
C Sum up matches.
      call sss_sum_images(n,match,ffile,efile,object,expos,ra,dec)
C
      ENDIF
      ii=ii+1
      ENDDO
C
      ok=.true.
      return
      end


C----------------------------------------------------------------------
C
      subroutine sss_load(file,ra,dec,echa,xda,frtime,expos,
     .                      object,deck,nc,nr,ok)
C
      implicit none
      character*(*) file,object,deck
      real*8 ra,dec,echa,xda,frtime,expos
      integer*4 nc,nr
      logical ok
C
      character*57600 header
      character*80 chead,wrd
      real*8 fhead
      integer*4 lc,sc,ec,sr,er
C
      call readfits_header(file,header,ok)
      if (.not.ok) return
      ra    = fhead('RA',header)
      dec   = fhead('DEC',header)
      echa  = fhead('ECHANGL',header)
      xda   = fhead('XDANGL',header)
      call HIRES_FrameTime(header,frtime)
      expos = fhead('EXPOSURE',header)
      wrd   = chead('OBJECT',header)
      object= wrd(1:lc(wrd))
      wrd   = chead('DECKER',header)
      deck  = wrd(1:lc(wrd))
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
C
      return
      end
        

      
C----------------------------------------------------------------------
C
      subroutine sss_sum_images(n,match,ffile,efile,object,
     .  expos,ra,dec)
C
      implicit none
      integer*4 n
      integer*4 match(*)
      character*(*) ffile(*),efile(*),object(*)
      real*8 expos(*),ra(*),dec(*)
C
      character*80 trg(99),of,ofe
      character*2000 wrd
      character*9 c9
      character*2 c2
      integer*4 i,j,k,lc,ntrg
      logical ok
C
C Echo.
      print *,'...'
      if (n.eq.1) then
        print *,'Found single exposure....'
      else
        print *,'Summing',n,' exposures...'
      endif
C Echo more.
      do i=1,n
        k = match(i)
        print '(i4,a,i4,3a,i5,2a)',
     .           i,'  #= ',k,' : ',ffile(k)(1:20),' : ',
     .           nint(expos(k)),'s   ',object(k)(1:lc(object(k)))
      enddo
C
C Output file.
      k = match(1)
      call write_radec(ra(k),dec(k),c9,2)
      wrd = ffile(k)
      i = index(wrd,'Flux-')
      j = index(wrd,'.fits')
      write(c2,'(i2.2)') min(99,n)
      of = 'sss'//wrd(i+5:j-1)//'-'//c2//'_'//c9(1:7)//'.fits'
      ofe= 'sss'//wrd(i+5:j-1)//'-'//c2//'_'//c9(1:7)//'e.fits'
C
      if (n.eq.1) then
C Single exposure.
        k = match(1)
        wrd = '/bin/cp '//ffile(k)(1:lc(ffile(k)))//' '//of(1:lc(of))
        call csysp(wrd)
        wrd = '/bin/cp '//efile(k)(1:lc(efile(k)))//' '//ofe(1:lc(ofe))
        call csysp(wrd)
C
      else
C Multiple exposures.
C Flux files.
        trg(1) = '-clobber'
        trg(2) = 'add'
        ntrg=2
        wrd = 'opim -clobber add'
        do i=1,n
          k = match(i)
          ntrg=min(99,ntrg+1)
          trg(ntrg)=ffile(k)
          wrd = wrd(1:lc(wrd))//' '//trg(ntrg)(1:lc(trg(ntrg)))
        enddo
        ntrg=min(99,ntrg+1)
        trg(ntrg)=of
        wrd = wrd(1:lc(wrd))//' '//trg(ntrg)(1:lc(trg(ntrg)))
        print '(a)',wrd(1:lc(wrd))
        call opim_main(trg,ntrg,ok)
C Error files.
        trg(1) = '-clobber'
        trg(2) = 'ade'
        ntrg=2
        wrd = 'opim -clobber ade'
        do i=1,n
          k = match(i)
          ntrg=min(99,ntrg+1)
          trg(ntrg)=efile(k)
          wrd = wrd(1:lc(wrd))//' '//trg(ntrg)(1:lc(trg(ntrg)))
        enddo
        ntrg=min(99,ntrg+1)
        trg(ntrg)=ofe
        wrd = wrd(1:lc(wrd))//' '//trg(ntrg)(1:lc(trg(ntrg)))
        print '(a)',wrd(1:lc(wrd))
        call opim_main(trg,ntrg,ok)
      endif
C
C Run spim0 on sss###-##_ file.
      trg(1)=of
      trg(2)='minp=-0.08'
      trg(3)='bin=2'
      i = index(of,'.fits')
      trg(4)='ps='//of(1:i-1)//'.ps'
      ntrg=4
      write(wrd,'(5(a,2x))') 'spim0',trg(1)(1:lc(trg(1))),
     .                               trg(2)(1:lc(trg(2))),
     .                               trg(3)(1:lc(trg(3))),
     .                               trg(4)(1:lc(trg(4)))
      print '(a)',wrd(1:lc(wrd))
      call spim0_main(trg,ntrg,ok)
      print '(2x)'
C
      return
      end

C----------------------------------------------------------------------
C
      logical function sss_match(i,j,ra,dec,echa,xda,frtime,object,
     .                            deck,nc,nr,echtol,xdtol)
C
      implicit none
      integer*4 i,j
      real*8 ra(*),dec(*),echa(*),xda(*),frtime(*),echtol,xdtol
      character*(*) object(*),deck(*)
      integer*4 nc(*),nr(*)
C
      real*8 a1,t1,a2,t2,as,r8
      integer*4 lc
C
C Default.
      sss_match = .false.
C Object name must match.
      if ( object(i)(1:lc(object(i))).ne.
     .                  object(j)(1:lc(object(j))) ) then
C       print *,'Object not matched'
        return
      endif
C Echelle and Cross Dispersers must be within tolerance. 
      if ( abs(echa(i)-echa(j)).gt.echtol ) then
C       print *,'ECHA not matched'
        return
      endif
      if ( abs(xda(i) -xda(j) ).gt.xdtol  ) then
C       print *,'XDA not matched'
        return
      endif
C RA and DEC must be within 12 arcseconds.
      a1 = 360.d0 * (ra(i)/24.d0)
      a2 = 360.d0 * (ra(j)/24.d0)
      t1 =  90.d0 - (dec(i))
      t2 =  90.d0 - (dec(j))
      r8 = as(a1,t1,a2,t2)
      if ( r8 .gt. 0.2 ) then
CCC     print *,
CCC  .  'RA and Dec position difference too large (arcmin): ',r8
        return
      endif
C Frame time must be within 5 days.
      if (abs(frtime(i)-frtime(j)).gt.432000.) then
CCC     print *,'Time difference too large.'
        return
      endif
C Decker name must be the same.
      if ( deck(i)(1:lc(deck(i))).ne.deck(j)(1:lc(deck(j))) ) then
CCC     print *,'Decker not matched.'
        return
      endif
C Number of columns and rows must be the same.
      if ( (nc(i).ne.nc(j)).or.(nr(i).ne.nr(j)) ) then
CCC     print *,'nc or nr not matched.'
        return
      endif
C
C A match!
      sss_match = .true.
      return
      end

