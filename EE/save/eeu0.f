C eeu0.f                               tab  1995-1999

CENDOFMAIN

C----------------------------------------------------------------------
C This routine is called within "makee" to fix FITS header cards (tab 13Jun98).
C
C Look in "observer.dat" file to set new OBSERVER keyword.
C     (Although usually set outside of FixKeckCards...  tab 05apr00)
C
C MUST set "Global_HIRES" and "Global_ESI" in global.inc before calling.
C
      subroutine FixKeckCards(fitsfile,header,eperdn,ronoise)
C
      implicit none
      character*(*) fitsfile  ! FITS filename (input)
      character*(*) header    ! FITS header (input/output)
      real*4 eperdn,ronoise   ! e-/DN and readout noise in e- (input)
C
      include 'makee.inc'
      include 'global.inc'
      include 'soun.inc'
C
      character*80 chead,c80,observer,lfilname,lampname
      character*80 deckname,shutter,wrd
      real*8 fhead,ra,dec,ha,slitsize,slitwidth,r8
      real*4 echa,xda,expos
      integer*4 lc,xbin,ybin,epoch,inhead,obsnum,i,j,access
      character radec*11,c11*11
      logical lhead,card_exist
C
C Globals set?
      call EE_CheckInstrument( header )
C
C Set e-/DN.
      r8 = dble(eperdn)
      write(soun,'(a,f8.4)') ' Setting EPERDN = ',sngl(r8)
      call fheadset('EPERDN',r8,header)
C
C Ronoise.
      r8 = dble(ronoise)
      write(soun,'(a,f8.4)') ' Setting RONOISE = ',sngl(r8)
      call fheadset('RONOISE',r8,header)
C
C Check CCD ID.
      c80 = chead('DETECTOR',header)
      if ( (index(c80,'Tek 2048E').eq.0).and.
     .     (index(c80,'MIT-LL').eq.0) ) then
        write(soun,'(2a)') ' WARNING: Unknown detector: ',c80(1:lc(c80))
      endif
C
C Get binning factor.
      call GetBinning(header,xbin,ybin)
      write(soun,'(a,i3,a,i3)') 
     .  '  Column binning=',xbin,'    Row binning=',ybin
      if ((xbin.ne.1).or.((ybin.ne.2).and.(ybin.ne.1))) then
       write(soun,'(a)') 
     .  'WARNING: Programs not fully tested with this binning.'
      endif
C
C Add arcsec/pixel in dispersion and spatial directions (for Keck HIRES 1998).
      if (Global_HIRES) then
        r8 = 0.29d0 * dfloat(xbin)
        call fheadset('ARCSPPIX',r8,header)
        call fheadset('ASPP_DSP',r8,header)
        r8 = 0.19d0 * dfloat(ybin)
        call fheadset('ASPP_SPA',r8,header)
      endif
C
C Add arcsec/pixel in dispersion and spatial directions (for Keck ESI 2000).
      if (Global_ESI) then
        r8 = 0.153d0 * dfloat(xbin)
        call fheadset('ARCSPPIX',r8,header)
        call fheadset('ASPP_DSP',r8,header)
        r8 = 0.153d0 * dfloat(ybin)
        call fheadset('ASPP_SPA',r8,header)
      endif
C
C Add new observer card.
      if (access('observer.dat','r').eq.0) then
        open(33,file='observer.dat',status='old')
        read(33,'(a)') observer
        close(33)
        c80 = chead('OBSERVER',header)
        if (c80.ne.' ') write(soun,'(2a)')
     .  '  Old observer card =',c80(1:lc(c80))
        c80 = observer
        call cheadset('OBSERVER',c80,header)
        if (c80.ne.' ') write(soun,'(2a)')
     .  '  New observer card =',c80(1:lc(c80))
      endif
C
C Get some values.
      epoch = inhead('EQUINOX',header)
      ra = fhead('RA',header)
      dec= fhead('DEC',header)
      call write_radec(ra,dec,radec,3)
C
C Check and echo various things.
C
      if (Global_HIRES) then
        deckname = chead('DECKNAME',header)
        lampname = chead('LAMPNAME',header)
        lfilname = chead('LFILNAME',header)
        if (lhead('DARKOPEN',header)) then
          shutter='open'
        else
          shutter='closed'
        endif
        write(soun,'(6a,3x,a)') ' Lamp= ',lampname(1:lc(lampname)),
     .    '   Lamp Filter= ',lfilname(1:lc(lfilname)),
     .    '   Shutter= ',shutter(1:lc(shutter)),deckname(1:lc(deckname))
        echa= max(-99.,min(999.,sngl(fhead('ECHANGL',header))))
        xda = max(-99.,min(999.,sngl(fhead('XDANGL',header))))
        i = inhead('FRAMENO',header)
        write(soun,'(a,i5,a,f8.4,a,f8.4,3a)')
     .      ' Frame#=',i,' :  ECHANGL=',echa,'  XDANGL= ',xda,
     .      '   (',fitsfile(1:lc(fitsfile)),')'
      endif
C
      if (Global_ESI) then
        i = inhead('FRAMENO',header)
        write(soun,'(a,i5)') ' Frame#=',i
      endif
C
C Check exposure time.
      j = max(-999,min(99999,inhead('TTIME',header)))
      i = max(-999,min(99999,inhead('ELAPTIME',header)))
      expos = max(-1,min(99999,float(i)))
      if (i.ne.j) then
        write(soun,'(a,i6,a,i6)') 
     .         'NOTE: FixKeckCards:  ELAPTIME=',i,'    TTIME=',j
        write(soun,'(a)') 
     .  'NOTE: Since elapsed time does not match total time,'
        write(soun,'(a)') 
     .  'NOTE:    this program will use ELAPTIME.'
      endif
      wrd = chead('OBJECT',header)
      write(soun,'(2a,i5,2a)
     .  ')  radec,'  Expos=',i,'s   Object=',wrd(1:lc(wrd))
C
C Set exposure time.
      if (.not.card_exist('EXPOSURE',header)) then
        call inheadset('EXPOSURE',nint(expos),header)
      endif
C
C Other RAs and DECs...
      if (epoch.eq.1950) then
        call write_hms(ra,c11,' ') 
        call cheadset('RA1950',c11,header)
        call write_hms(dec,c11,'+') 
        call cheadset('DEC1950',c11,header)
        call precess(1950.d0,2000.d0,ra,dec)
        call write_hms(ra,c11,' ') 
        call cheadset('RA2000',c11,header)
        call write_hms(dec,c11,'+') 
        call cheadset('DEC2000',c11,header)
      elseif (epoch.eq.2000) then
        call write_hms(ra,c11,' ') 
        call cheadset('RA2000',c11,header)
        call write_hms(dec,c11,'+') 
        call cheadset('DEC2000',c11,header)
        call precess(2000.d0,1950.d0,ra,dec)
        call write_hms(ra,c11,' ') 
        call cheadset('RA1950',c11,header)
        call write_hms(dec,c11,'+') 
        call cheadset('DEC1950',c11,header)
      else
        write(soun,'(a,i9)') 'What is this EPOCH? : ',epoch
      endif
C
C Copy observation number.
      obsnum=min(9999,max(-1,inhead('FRAMENO',header)))
      if (.not.card_exist('OBSNUM',header)) then
        call inheadset('OBSNUM',obsnum,header)
      endif
C
C Copy UT DATE.
      if (.not.card_exist('DATE-OBS',header)) then
        c80=chead('DATE',header)
        call cheadset('DATE-OBS',c80,header)
      endif
C
C Copy UT TIME.
      if (.not.card_exist('TIME',header)) then
        c80=chead('UT',header)
        call cheadset('TIME',c80,header)
      endif
C
C Copy CCDID.
      if (.not.card_exist('CCDID',header)) then
        c80=chead('DETECTOR',header)
        call cheadset('CCDID',c80,header)
      endif
C
C Fix Hour Angle.
      ha = fhead('HA',header)
      if (ha.gt.12.) ha = ha - 24.d0
      call write_hms(ha,c11,'+')
      call cheadset('HA',c11,header)
C
C Add CRVAL,CDELT,CRPIX,...
C
C NOTE: Making first column called "0" and first row called "0"- tab Mar2000.
C
      if ( (.not.card_exist('CRVAL1',header)).or.
     .     (.not.card_exist('CRVAL2',header)).or.
     .     (.not.card_exist('CRPIX2',header)) ) then
        call inheadset('CRVAL1',0,header)
        call inheadset('CRVAL2',0,header)
        call inheadset('CDELT1',1,header)
        call inheadset('CDELT2',1,header)
        call inheadset('CRPIX1',1,header)
        call inheadset('CRPIX2',1,header)
C Added these next two lines-- tab Mar2000.
        call cheadset('CTYPE1','PIXEL',header)
        call cheadset('CTYPE2','PIXEL',header)
      endif
C
C Add latitude and longitude (for Mauna Kea).
      if ((Global_HIRES).or.(Global_ESI)) then
        call fheadset('LATITUDE',19.82658656d0,header)
        call fheadset('LONGITUD',155.4722d0,header)
      endif
C
C Add slit size and slit width.
      if (Global_HIRES) then
        call get_slit(header,slitsize,slitwidth)
        call fheadset('SLITSIZE',slitsize,header)
        call fheadset('SLITLENG',slitwidth,header)
C Add shutter state.
        if (shutter.eq.'open') then
          call cheadset('SHUTTER','OPEN',header)
        else
          call cheadset('SHUTTER','CLOSED',header)
        endif
      endif
C
C Extra cards.
      call cheadset('STATUS','PROCESSED',header)
      call inheadset('BITPIX',-32,header)
      call cheadset('DATATYPE','REAL*4',header)
C
      return
      end


C---------------------------------------------------------------------
C Find slit width from header.  For HIRES data only.
C
      subroutine get_slit(header,ss,slitwidth)
C
      implicit none
      character*(*) header
      character*80 c80,chead
      real*8 ss,slitwidth
      c80 = chead('DECKNAME',header)
      call upper_case(c80)
      if (index(C80,'B1').gt.0) then
        ss = 0.574
        slitwidth = 3.5
      elseif (index(C80,'B2').gt.0) then
        ss = 0.574
        slitwidth = 7.0
      elseif (index(C80,'B3').gt.0) then
        ss = 0.574
        slitwidth = 14.0
      elseif (index(C80,'B4').gt.0) then
        ss = 0.574
        slitwidth = 28.0
      elseif (index(C80,'B5').gt.0) then
        ss = 0.861
        slitwidth =  3.5
      elseif (index(C80,'C1').gt.0) then
        ss = 0.861
        slitwidth =  7.0
      elseif (index(C80,'C2').gt.0) then
        ss = 0.861
        slitwidth = 14.0
      elseif (index(C80,'C3').gt.0) then
        ss = 0.861
        slitwidth = 28.0
      elseif (index(C80,'C4').gt.0) then
        ss = 1.148
        slitwidth =  3.5
      elseif (index(C80,'C5').gt.0) then
        ss = 1.148
        slitwidth =  7.0
      elseif (index(C80,'D1').gt.0) then
        ss = 1.148
        slitwidth = 14.0
      elseif (index(C80,'D2').gt.0) then
        ss = 1.148
        slitwidth = 28.0
      elseif (index(C80,'D3').gt.0) then
        ss = 1.722
        slitwidth =  7.0
      elseif (index(C80,'D4').gt.0) then
        ss = 1.722
        slitwidth = 14.0
      elseif (index(C80,'D5').gt.0) then
        ss = 0.179
        slitwidth = 0.119
      endif
      return
      end

C----------------------------------------------------------------------
C Decide what kind of image this is.
C Valid image types: DARK, FLAT, STAR, OBJT, UNKN, ARCL, or SATU.
C NEW Valid image type: PINH (pinhole using D5 decker).
C
C If (med,high,low)max are not given they should be set to -1.
C
      subroutine ImageType(header,medmax,highmax,lowmax,imtype,
     .                     saturation)
C
      implicit none
      character*(*) header           ! FITS header (input).
      real*4 medmax,highmax,lowmax   ! Special median, high, and low 
C                                    !   "median column" values (input).
      real*4 saturation              ! Saturation level in DN (input).
      character*(*) imtype           ! Four character image type (output).
C
      include 'soun.inc'
C
      character*80 lampname,chead,hatch,shutter,object,decker
      integer*4 inhead,obsnum
      logical lhead
      real*4 expos

C Object name.
      object = chead('OBJECT',header)
C Exposure.
      expos = float(inhead('EXPOSURE',header))
C Decker name.
      decker = chead('DECKNAME',header)
      call lower_case(decker)
C Observation number.
      obsnum= inhead('OBSNUM',header) 
C Hatch.
      if (lhead('HATOPEN',header)) then
        hatch='open'
      else
        hatch='closed'
      endif
C Shutter.
      if (lhead('DARKOPEN',header)) then
        shutter='open'
      else
        shutter='closed'
      endif
C Lamp name.
      lampname = chead('LAMPNAME',header)
      call lower_case(lampname)
C Assume an undefined lamp is off.
      if (lampname(1:5).eq.'undef') lampname='none'
C
C Is this a dark?
      if (shutter.eq.'closed') then
C Check to see if lamps are off during dark.
        if (index(lampname,'none').eq.0) then
          write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     . '): A lamp appears to be on during a dark exposure, type=UNKN.'
          imtype = 'UNKN'
          goto 8
        endif
C Check counts.
        if (highmax.gt.100.) then
          write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     . '): High counts (>100 DN) on a dark exposure, type=UNKN.'
          imtype = 'UNKN'
          goto 8
        endif
        imtype = 'DARK'
        goto 8
C They may have left shutter open during dark.
      elseif (hatch.eq.'closed') then
        if ( (expos.lt.2.).and.(index(lampname,'none').gt.0)
     .                    .and.(highmax.lt.100.) ) then
          write(soun,'(a)') 
     . 'WARNING: Hatch closed, but shutter open on dark, type=DARK.'
          imtype = 'DARK'
          goto 8
        endif
C They may have left shutter and hatch open during dark.
      elseif (expos.lt.2.) then
        if ( (index(lampname,'none').gt.0).and.(highmax.lt.100.) ) then
          write(soun,'(a)') 
     . 'WARNING: Hatch open and shutter open on dark, type=DARK.'
          imtype = 'DARK'
          goto 8
        endif
      endif
C
C Is this a pinhole quartz?
      if ((index(lampname,'quart').gt.0).and.
     .    (index(decker,'d5').gt.0)) then
C Check exposure length.
        if (expos.gt.600) then  
          write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     .     '): Long pinhole quartz exposure (>600s), type=UNKN.'
          imtype = 'UNKN'
          goto 8
        endif
C Check total counts.
        if ((medmax.gt.0.).and.(medmax.lt.100.)) then
          write(soun,'(a,i4,2a)') ' WARNING(',obsnum,
     .         '): Median counts too small (<100) o',
     .         'n pinhole quartz lamp, type=UNKN.'
          imtype = 'UNKN'
          goto 8
        endif
        imtype = 'PINH'
        goto 8
      endif
C
C Is this a flat field?
      if (index(lampname,'quart').gt.0) then
C Check exposure length.
        if (expos.gt.300) then  
          write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     .'): Long exposure (>300s) with a quartz lamp on, type=UNKN.'
          imtype = 'UNKN'
          goto 8
        endif
C Check total counts.
        if ((medmax.gt.0.).and.(medmax.lt.1000.)) then
          write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     .'): Median counts too small (<1000) on a quartz lamp, type=UNKN.'
          imtype = 'UNKN'
          goto 8
        endif
C Check for saturation.
        if ((highmax.gt.0.).and.(highmax.gt.saturation)) then
          write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     .       '): Quartz lamp appears to be saturated, imtype=SATU.'
          imtype = 'SATU'
          goto 8
        endif
        imtype = 'FLAT'
        goto 8
      endif
C
C Is this an arc lamp?
      if (index(lampname,'thar').gt.0) then
        if (expos.gt.300) then  
          write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     . '): Long exposure (>300s) with a ThAr lamp on, type=UNKN.'
          imtype = 'UNKN'
          goto 8
        endif
        imtype = 'ARCL'
        goto 8
      endif
C
C Is the hatch closed?
      if (hatch.eq.'closed') then
        write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     .        '): Hatch closed, shutter open, no lamp, type=UNKN.'
        imtype = 'UNKN'
        goto 8
      endif
C
C Is this a (standard) star?
C Case where we have "max" values.
      if (medmax.gt.0.) then
        if ((expos.lt.1201.).and.(lowmax.gt.60.).and.
     .                           (medmax/max(1.,expos).gt.0.4)) then
C Check for saturation.
          if (highmax.gt.saturation) then
            write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     .         '): Star exposure appears to be saturated, imtype=UNKN.'
            imtype = 'UNKN'
            goto 8
          endif
          imtype='STAR'
          goto 8
        endif
C Case where we do not have "max" values.
      else
        if (expos.lt.590.) then
          imtype='STAR'
          goto 8
        endif
      endif
C
C Unknown images.
      if ((medmax.gt.0.).and.(medmax.lt.30.).and.(expos.lt.601.)) then
        write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     . '): Low counts (<30) and short exposure (<601s), type=UNKN.'
        imtype = 'UNKN'
        goto 8
      endif
      if ((medmax.gt.0.).and.(medmax.lt.15.)) then
        write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     . '): Low counts (<15) with hatch and shutter open, type=UNKN.'
        imtype = 'UNKN'
        goto 8
      endif
C
C This must be an object!
C Check for saturation.
      if (highmax.gt.saturation) then
        write(soun,'(a,i4,a)') ' WARNING(',obsnum,
     .     '): Object exposure appears to be saturated, imtype=UNKN.'
        imtype = 'UNKN'
        goto 8
      endif
      imtype='OBJT'

8     continue
C Possible override.
C Valid image types are: DARK, FLAT, STAR, OBJT, UNKN, ARCL, SATU, or PINH.
      if (index(object,'[DARK]').gt.0) then
        write(soun,'(a)') 
     .  'Object name overrides automatic type selection, imtype=DARK.'
        imtype = 'DARK'
      elseif (index(object,'[FLAT]').gt.0) then
        write(soun,'(a)') 
     .  'Object name overrides automatic type selection, imtype=FLAT.'
        imtype = 'FLAT'
      elseif (index(object,'[STAR]').gt.0) then
        write(soun,'(a)') 
     .  'Object name overrides automatic type selection, imtype=STAR.'
        imtype = 'STAR'
      elseif (index(object,'[OBJT]').gt.0) then
        write(soun,'(a)') 
     .  'Object name overrides automatic type selection, imtype=OBJT.'
        imtype = 'OBJT'
      elseif (index(object,'[UNKN]').gt.0) then
        write(soun,'(a)') 
     .  'Object name overrides automatic type selection, imtype=UNKN.'
        imtype = 'UNKN'
      elseif (index(object,'[ARCL]').gt.0) then
        write(soun,'(a)') 
     .  'Object name overrides automatic type selection, imtype=ARCL.'
        imtype = 'ARCL'
      elseif (index(object,'[SATU]').gt.0) then
        write(soun,'(a)') 
     .  'Object name overrides automatic type selection, imtype=SATU.'
        imtype = 'SATU'
      elseif (index(object,'[PINH]').gt.0) then
        write(soun,'(a)') 
     .  'Object name overrides automatic type selection, imtype=PINH.'
        imtype = 'PINH'
      endif
      return
      end

C----------------------------------------------------------------------
C Find "representation" maximum along columns.
C
      subroutine ColumnMaximum(sc,ec,sr,er,a,medmax,highmax,
     .                         lowmax,xbin)
C
      implicit none
      integer*4 sc,ec,sr,er,xbin
      real*4 a(sc:ec,sr:er),arr(9000),high,medmax,highmax,lowmax
      integer*4 col,row,narr,j,blc1,blc2,num
      real*4 bl(9000),sum,r
C Baseline region.
      blc1 = 2261/xbin
      blc2 = ec-1
C Calculate crude baseline array, if not already baselined/windowed.
      if ((blc2-blc1).gt.10) then
        do row=sr,er 
          sum=0.
          num=0
          do col=blc1,blc2
            sum=sum+a(col,row)
            num=num+1
          enddo
          bl(row)=sum/float(max(1,num))
        enddo
      else
        do row=sr,er
          bl(row)=0.
        enddo
      endif
C Do every tenth column near middle of image.
      narr=0
      DO col=sc+300,ec-300,10
C Find maximum in every set of 200 rows.
      row = sr+10
      do while(row.lt.er-10)
        high=0.
        do j=row,min(er,row+200)
          r = a(col,j) - bl(j)
          if (r.gt.high) high=r
        enddo
        narr=narr+1
        arr(narr)=high
        row=row+201
      enddo
      ENDDO
C Median of maximums.
      call find_median(arr,narr,medmax)
      highmax=arr(nint(float(narr)*0.98))
      lowmax =arr(nint(float(narr)*0.05))
      return
      end 

C----------------------------------------------------------------------
C BIW = Baseline, Interpolate bad regions, Window image.
C (Also rotate if necessary.)  
C   Input: a(*) = image, header = old FITS header,
C          maskfile = file containing mask region data.
C  Output: a(*) = corrected and windowed image, header = new FITS header,
C          sc,ec,sr,er = new dimensions.
C
C MUST set "Global_HIRES" and "Global_ESI" in global.inc before calling.
C
      subroutine BIW(aa,bb,header,maskfile,maskmode,sc,ec,sr,er,ok)
C
      implicit none
      real*4 aa(*)            ! data image (input/output)
      real*4 bb(*)            ! scratch image (scratch)
      character*(*) header    ! FITS header (input/output)
      character*(*) maskfile  ! Bad column mask data filename (input)
      integer*4 maskmode      ! 1=mask bad columns, 0=interpolate,
                              !   2=ESI known trace interpolation (input)
      integer*4 sc,ec,sr,er   ! image dimensions (output)
      logical ok              ! Success? (output)
C
      include 'verbose.inc'
      include 'global.inc'
      include 'soun.inc'
C
      integer*4 nc,nr,xbin,ybin,numamps,prepix,postpix,ii
      integer*4 wsc,wec,wsr,wer
      integer*4 wx1,wy1,wx2,wy2,inhead,temp
C
C Check globals.
      call EE_CheckInstrument( header )
C
C Check if image has already been processed.  If so, return now.
      if (inhead('BIW',header).eq.1) then
        if (verbose) then
          write(soun,'(a)') 
     .  'NOTE: BIW: Image has already been processed.'
        endif
        return
      endif

C Dimensions.
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
     
C Get Window.
      call GetWindow(header,wx1,wy1,wx2,wy2)

C Get binning.
      call GetBinning(header,xbin,ybin)
      write(soun,'(a,i4,a,i4)') 
     .  'NOTE: BIW:  X binning:',xbin,'   Y binning:',ybin

C Prepix, Postpix, NumAmps.
      prepix= inhead('PREPIX',header)
      if (prepix.lt.1) prepix= 0
      postpix= inhead('POSTPIX',header)
      if (postpix.lt.1) postpix= 100
      numamps= inhead('NUMAMPS',header)
      if (numamps.lt.1) numamps= 1

C Baseline (overscan) correction.
      if (numamps.eq.1) then
        call BaseLineCorrectionOneAmp(aa,sc,ec,sr,er,postpix,wx2)
      elseif (numamps.eq.2) then
        call BaseLineCorrectionTwoAmp(aa,sc,ec,sr,er,prepix,postpix)
      else
        write(soun,'(a,i8)') 
     .    'ERROR: BIW: Cannot baseline with numamps=',numamps
        call exit(0)
      endif

C Interpolate bad columns.
      if (maskfile.ne.' ') then
        if (maskmode.eq.0) then
          call Interpolate_Bad_Regions(aa,sc,ec,sr,er,maskfile)
        elseif ((maskmode.eq.1).or.(maskmode.eq.2)) then
          call Mask_Bad_Regions(aa,sc,ec,sr,er,maskfile)
        endif
      endif

C New dimensions. Set new window according to prepix and postpix and numamps.
      sc = (sc + (numamps * prepix)) + 1
      ec = (ec - (numamps *postpix)) - 1
      sr = sr + 1
      nc = 1 + ec - sc
      nr = 1 + er - sr

C Save window dimensions.
      wsc = sc
      wec = ec
      wsr = sr
      wer = er

C Window the image.
      call window_fits(aa,header,sc,ec,sr,er,ok)
      if (.not.ok) return

C We want to make blue on the bottom and left in ESI image.
      if (Global_ESI) then
C Rotate the ESI image.
        write(soun,'(a)') 'Rotating ESI image by 90 degrees...'
        call rotate_fits(aa,bb,header,-90.,ok)
        if (.not.ok) return
C Flip the ESI image.
        write(soun,'(a)') 'Flipping ESI image in columns...'
        call flip_fits(aa,header,'LR',ok)
        if (.not.ok) return
C Starting position no longer meaningful, might as well set to 0,0 (ESI only).
        call inheadset('CRVAL1',0,header)
        call inheadset('CRVAL2',0,header)
C New rotated,flipped dimensions.
        temp=ec-sc
        ec  = er-sr
        er  = temp
        sr  = 0
        sc  = 0
        nc  = 1 + ec - sc
        nr  = 1 + er - sr
C Flip binning sense.
        temp = xbin
        xbin = ybin
        ybin = temp
      endif

C Force pixel types.
      call cheadset('CTYPE1','PIXEL',header)
      call cheadset('CTYPE2','PIXEL',header)

C Set header card flag.
      call inheadset('BIW',1,header)

C Echo.
      write(soun,'(a,i5,a,i5)')
     .        'Prepix:',prepix,'   Postpix:',postpix
      write(soun,'(a,i4,i6,a,i4,i6)')
     .        'Original Window:  cols=',wx1,wx2,'  rows=',wy1,wy2
      write(soun,'(a,i4,i6,a,i4,i6)')
     .        'New Clipped Window:  cols=',sc,ec,'  rows=',sr,er

C ESI known trace interpolation.
C .. Trace interpolation is done now since the traces are with respect
C .. to the windowed,rotated, and flipped coordinate system.
C ..
      if ((maskmode.eq.2).and.(maskfile.ne.' ')) then 
        if (.not.Global_ESI) then
          write(soun,'(2a)') 'WARNING: BIW: Cannot do ESI known tr',
     .                       'ace interpolation on non-ESI data.'
          ok=.false. 
          return
        endif
C Copy the real image into the scratch image.
        do ii=1,(nc*nr)
          bb(ii) = aa(ii)
        enddo
C Trace interpolation (bad regions must be masked first.)
        call Interpolate_Bad_Regions_ESI(aa,bb,sc,ec,sr,er,
     .                      wsc,wec,wsr,wer,maskfile,xbin,ybin)
      endif
C
      ok=.true.
      return
      end



C----------------------------------------------------------------------
C Do baseline correction appropriate for 1994-98 Keck HIRES or 2000 ESI CCD
C   with only one readout amplifier.
C
      subroutine BaseLineCorrectionOneAmp(a,sc,ec,sr,er,postpix,wx2)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er)
      integer*4 postpix,wx2
      real*4 baseline,sum
      integer*4 row,col,num,blc1,blc2
C
      include 'global.inc'
      include 'soun.inc'
C
C Column range.
      blc1 = 1 + ec - postpix
      blc2 = ec
C Avoid baseline closest to data with HIRES.
      if (Global_HIRES) blc1 = blc1 + (postpix/2)
C Echo.
      write(soun,'(a,i5,a,i5,a)') 
     .   'Subtracting baseline from image (',blc1,' -->',blc2,' )'
C Check.
      if (blc1.lt.wx2) then
        write(soun,'(2a,i6,a,i6,a)') 'WARNING: BaselineCorrection: ',
     .     ' Apparent start of baseline within data window. (blc1=',
     .     blc1,' wx2=',wx2,')'
        return
      endif
C Ignore this area on HIRES ccd:
C For 1x2 binning: sc,ec,sr,er=2150,2260,413,466.
      if (Global_HIRES) then
        do row=nint(float(er)*0.4033),nint(float(er)*0.4551)
          do col=blc1,blc1+nint(float(blc2-blc1)*0.74)
            a(col,row)=-1.
          enddo
        enddo
      endif
C Compute average baseline for each row.
      do row=sr,er
        num=0
        sum=0.
        do col=blc1,blc2 
          if (a(col,row).gt.0.) then
            sum=sum+a(col,row)
            num=num+1
          endif
        enddo
        baseline = sum / float(max(1,num)) 
C Subtract from each column.
        do col=sc,ec
          a(col,row) = a(col,row) - baseline
        enddo
      enddo
      return
      end


C----------------------------------------------------------------------
C Do baseline correction appropriate for 2000 ESI CCD or HIRES CCD.
C   with two readout amplifiers.
C
      subroutine BaseLineCorrectionTwoAmp(a,sc,ec,sr,er,prepix,postpix)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er)
      integer*4 prepix,postpix
      real*4 baseline,sum
      integer*4 row,col,num,blc1,blc2,dac,dac1,dac2
C
      include 'global.inc'
      include 'soun.inc'
C
C Data Column range.
      dac = ((ec - sc + 1) - (prepix * 2)) - (postpix * 2)

C First baseline range.
      blc1 = 1 + ec - (postpix * 2)
      blc2 = ec - postpix
C Avoid baseline closest to data with HIRES.
      if (Global_HIRES) blc1 = blc1 + (postpix/2)
C Data column range to correct.
      dac1 = sc + (prepix * 2)
      dac2 = dac1 + (dac / 2) - 1
C Echo.
      write(soun,'(a,i5,a,i5,a,a,i5,a,i5,a)') 
     .   'Subtracting baseline (',blc1,' -->',blc2,' )',
     .   ' from data (',dac1,' -->',dac2,' ).'
C Compute average baseline for each row.
      do row=sr,er
        num=0
        sum=0.
        do col=blc1,blc2 
          if (a(col,row).gt.0.) then
            sum=sum+a(col,row)
            num=num+1
          endif
        enddo
        baseline = sum / float(max(1,num)) 
C Subtract from each column.
        do col=dac1,dac2
          a(col,row) = a(col,row) - baseline
        enddo
      enddo

C Second baseline range.
      blc1 = blc2 + 1
      blc2 = ec
C Avoid baseline closest to outer edge with HIRES.
      if (Global_HIRES) blc2 = blc2 - (postpix/2)
C Data column range to correct.
      dac1 = dac2 + 1
      dac2 = ec - (postpix * 2)
C Echo.
      write(soun,'(a,i5,a,i5,a,a,i5,a,i5,a)') 
     .   'Subtracting baseline (',blc1,' -->',blc2,' )',
     .   ' from data (',dac1,' -->',dac2,' ).'
C Compute average baseline for each row.
      do row=sr,er
        num=0
        sum=0.
        do col=blc1,blc2 
          if (a(col,row).gt.0.) then
            sum=sum+a(col,row)
            num=num+1
          endif
        enddo
        baseline = sum / float(max(1,num)) 
C Subtract from each column.
        do col=dac1,dac2
          a(col,row) = a(col,row) - baseline
        enddo
      enddo
C
      return
      end



C----------------------------------------------------------------------
C Interpolate over bad areas on the CCD image using up to 3 columns on
C either side.
C
      subroutine Interpolate_Bad_Regions(a,sc,ec,sr,er,maskfile)
C
      implicit none
      integer*4 sc,ec,sr,er    ! dimensions (input)
      real*4 a(sc:ec,sr:er)    ! Data image array (input/output)
      character*(*) maskfile   ! Masking file filename (input)
C
      include 'soun.inc'
C
      real*4 arr(20),sum
      integer*4 lc,i,bsc,bec,bsr,ber,row,col,num,n
      character*100 line
      real*8 fgetlinevalue
C
      if (maskfile.eq.' ') return                   ! No interpolation mode.
      open(31,file=maskfile,status='old',iostat=i)
      if (i.ne.0) then
        write(soun,'(2a)') 
     .        'WARNING: IBR: Could not find maskfile : ',
     .        maskfile(1:lc(maskfile))
        return
      endif
1     continue
      read(31,'(a)',end=5,err=801) line
      bsc = nint( fgetlinevalue(line,1) )
      bec = nint( fgetlinevalue(line,2) )
      bsr = nint( fgetlinevalue(line,3) )
      ber = nint( fgetlinevalue(line,4) )
      if ((bsc.lt.-1).or.(bec.lt.-1).or.
     .                (bsr.lt.-1).or.(ber.lt.-1)) then
        write(soun,'(2a)')
     .     'ERROR: IBR: Reading line from ',maskfile
        write(soun,'(2a)')
     .     'ERROR: IBR: Bad line: ',line(1:lc(line))
        return
      endif
      if (bsc.eq.-1) bsc=sc     
      if (bec.eq.-1) bec=ec     
      if (bsr.eq.-1) bsr=sr     
      if (ber.eq.-1) ber=er     
      bsc=max(sc,min(ec,bsc)) 
      bec=max(sc,min(ec,bec)) 
      bsr=max(sr,min(er,bsr)) 
      ber=max(sr,min(er,ber)) 
C Interpolate over each row.  Use 3 columns on either side.
      do row=bsr,ber
        n=0
        if (bsc.gt.sc) then
          do col=max(sc,bsc-3),bsc-1
            n=n+1
            arr(n)=a(col,row)
          enddo
        endif
        if (bec.lt.ec) then
          do col=bec+1,min(ec,bec+3)
            n=n+1
            arr(n)=a(col,row)
          enddo
        endif
C Use find_median to sort list.
        call find_median(arr,n,sum)
C Find average of lowest 3 values (to avoid CRs).
        sum=0. 
        num=0
        do i=1,min(3,n)
          sum=sum+arr(i)
          num=num+1
        enddo
        sum = sum/float(max(1,num))
        do col=bsc,bec
          a(col,row)=sum
        enddo
      enddo
      goto 1
5     continue
      close(31) 
      return
801   continue
      write(soun,'(2a)') 
     .  'ERROR reading maskfile : ',maskfile(1:lc(maskfile))
      close(31)
      call exit(1)
      end


C----------------------------------------------------------------------
C Interpolate over bad areas on the ESI CCD image using a guess at the
C the echelle order trace fits.
C
C  ASSUMES BAD REGIONS HAVE BEEN MASKED (bad pixels set -1.e+10).
C
      subroutine Interpolate_Bad_Regions_ESI(aa,bb,sc,ec,sr,er,
     .                        wsc,wec,wsr,wer,maskfile,xbin,ybin)
C
      implicit none
      integer*4 sc,ec,sr,er      ! dimensions (input)
      integer*4 wsc,wec,wsr,wer  ! window dim. (prior to rotation,flip) (input)
      real*4 aa(sc:ec,sr:er)     ! Data image array (input/output)
      real*4 bb(sc:ec,sr:er)     ! Reference image (copy of aa) (input)
      character*(*) maskfile     ! Raw data masking file.
      integer*4 xbin,ybin        ! x,y binning in new (rotated,windowed) frame.
C
      integer*4 bsc,bec          ! Bad region starting and ending column (input)
      integer*4 bsr,ber          ! Bad region starting and ending row (input)
C
      include 'soun.inc'
C
      character*80 line
      integer*4 nn,ii,jj,order,row,col
      integer*4 rsc,rec,rsr,rer,nc,nr,temp
      real*8 col8,row8,xx,yy,polyvaloff,xfac
      real*8 coef(0:99),xoff,yoff,fgetlinevalue
      real*8 leftmean,rightmean,leftdist,rightdist,mean,wsum,wt
C
C Check binning.
      if (ybin.ne.1) then
        write(soun,'(a)')'ERROR: Cannot properly interpolate ESI image'
        write(soun,'(a)')'ERROR:   without column (x) binning of 1.'
        return
      endif
      if ((xbin.ne.1).and.(xbin.ne.2)) then
        write(soun,'(a)')'ERROR: Cannot properly interpolate ESI image'
        write(soun,'(a)')'ERROR:   without row binning (y) of 1 or 2.'
        return
      endif

C Binning Factor.
      xfac = dfloat(xbin)

C Open mask file.
      open(30,file=maskfile,status='old')
111   read(30,'(a)',end=555,err=801) line
C Raw region.
      rsc = nint( fgetlinevalue(line,1) )
      rec = nint( fgetlinevalue(line,2) )
      rsr = nint( fgetlinevalue(line,3) )
      rer = nint( fgetlinevalue(line,4) )

C Windowed, Rotated -90, Flipped in columns.
C Expand.
      if (rsc.eq.-1) rsc = wsc
      if (rec.eq.-1) rec = wec
      if (rsr.eq.-1) rsr = wsr
      if (rer.eq.-1) rer = wer
C Window.
      if (rsc.lt.wsc) rsc=wsc
      if (rec.gt.wec) rec=wec
      if (rsr.lt.wsr) rsr=wsr
      if (rer.gt.wer) rer=wer
C Rotate -90 degrees.
      nr = 1 + wer - wsr
      nc = 1 + wec - wsc
      call rotate_counter_clockwise(rsc,rsr,bsc,bsr,wsc,wsr,0,0,nr)
      call rotate_counter_clockwise(rec,rer,bec,ber,wsc,wsr,0,0,nr)
C New dimensions.
      temp = nr
      nr   = nc
      nc   = temp
      sc = 0
      sr = 0
      ec = nc + sc - 1
      er = nr + sr - 1
C Flip in columns.
      bsc = 1 + nc - bsc
      bec = 1 + nc - bec
C
C
C Interpolate over each pixel using trace fit.
      DO col=bsc,bec
      col8 = dfloat(col)
      DO row=bsr,ber
      row8 = dfloat(row)
C
C Find trace fit.
      call Find_ESI_Polynomial(nint(col*xfac),row,order,coef,xoff,yoff)
C
C Find left value.
      nn = 0
      xx = col8 - 1.
      leftmean=0.
      leftdist=0.
      do while((nint(xx).ge.sc).and.(nn.lt.5))
        yy = polyvaloff(order+1,coef,(xx * xfac),xoff) + yoff
        ii = nint(xx)
        jj = nint(yy)
        if ((jj.ge.sr).and.(jj.le.er)) then
        if (bb(ii,jj).gt.-1.e+9) then
          nn=nn+1
          leftmean = leftmean + bb(ii,jj)
          leftdist = leftdist + abs(xx-col8)
        endif
        endif
        xx = xx - 1.
      enddo
      if (nn.gt.0) then
        leftmean = leftmean / dfloat(nn)
        leftdist = leftdist / dfloat(nn)
      else
        leftdist = -1.
      endif
C
C Find right value.
      nn = 0
      xx = col8 + 1.
      rightmean=0.
      rightdist=0.
      do while((nint(xx).le.ec).and.(nn.lt.5))
        yy = polyvaloff(order+1,coef,(xx * xfac),xoff) + yoff
        ii = nint(xx)
        jj = nint(yy)
        if ((jj.ge.sr).and.(jj.le.er)) then
        if (bb(ii,jj).gt.-1.e+9) then
          nn=nn+1
          rightmean = rightmean + bb(ii,jj)
          rightdist = rightdist + abs(xx-col8)
        endif
        endif
        xx = xx + 1.
      enddo
      if (nn.gt.0) then
        rightmean = rightmean / dfloat(nn)
        rightdist = rightdist / dfloat(nn)
      else
        rightdist = -1.
      endif
C
C Final mean.
      mean = 0.
      wsum = 0.
      if (leftdist.gt.0.) then
        wt   = 1. / leftdist
        mean = mean + (wt * leftmean)
        wsum = wsum + wt
      endif
      if (rightdist.gt.0.) then
        wt   = 1. / rightdist
        mean = mean + (wt * rightmean)
        wsum = wsum + wt
      endif
      mean = mean / wsum
C
C Replace value.
      aa(col,row) = mean
C
      ENDDO
      ENDDO
C
      goto 111
555   continue
      close(30)
C
      return
C
801   continue
      write(soun,'(2a)') 'ERROR: IBRE: Reading mask file: ',maskfile
      return
      end


C----------------------------------------------------------------------
C Rotate a position clockwise 90 degrees.
C
      subroutine rotate_clockwise(col1,row1,col2,row2,
     .                            osc,osr,nsc,nsr,nc)
C
      implicit none
      integer*4 col1,row1   ! original x,y
      integer*4 col2,row2   ! new x,y
      integer*4 osc,osr     ! original starting column,row (input)
      integer*4 nsc,nsr     ! new starting column,row (input)
      integer*4 nc          ! number of columns (original)
      col2 = (row1 - osr)
      row2 = 1 + nc - (col1 - osc)
      col2 = col2 + nsc
      row2 = row2 + nsr
      return
      end
C----------------------------------------------------------------------
C Rotate a position counter-clockwise 90 degrees.
C
      subroutine rotate_counter_clockwise(col1,row1,col2,row2,
     .                                    osc,osr,nsc,nsr,nr)
C
      implicit none
      integer*4 col1,row1   ! original x,y (input)
      integer*4 col2,row2   ! new x,y (output)
      integer*4 osc,osr     ! original starting column,row (input)
      integer*4 nsc,nsr     ! new starting column,row (input)
      integer*4 nr          ! number of rows (original) (input)
      col2 = 1 + nr - (row1 - osr)
      row2 = (col1 - osc)
      col2 = col2 + nsc
      row2 = row2 + nsr
      return
      end
     


C---------------------------------------------------------------------- 
C Read in two flat images.  Baseline/window and then find electrons per digital
C number (eperdn).  a(), b() should be big enough to hold an entire raw image.
C
      subroutine InverseGain(flat1,flat2,maskfile,a,b,header,
     .                       rov,eperdn,ok) 
C
      implicit none 
      character*(*) flat1,flat2   ! FITS filenames (input)
      character*(*) maskfile      ! Bad column mask data filename (input)
      real*4 a(*),b(*)            ! Data images (scratch)
      character*(*) header        ! FITS header (scratch)
      real*4 rov                  ! Readout Variance in DN (input)
      real*4 eperdn               ! Electrons per Digital Number (output)
      logical ok                  ! Success? (output)
C
      include 'maxpix.inc'
      include 'soun.inc'
C
      integer*4 sc,ec,sr,er,ii,fftimeget
      character*80 savefile
C
C Check.
      if (rov.le.0.) then
        write(soun,'(a)') 'ERROR: InverseGain: "rov" must be >= 0.'
        ok=.false.
        return
      endif
C Read image a.
      call readfits(flat1,a,maxpix,header,ok)
      if (.not.ok) return
      call BIW(a,b,header,maskfile,0,sc,ec,sr,er,ok)
      if (.not.ok) return
C Save image a.
      ii = fftimeget()
      write(savefile,'(a,i10.10,a)') 'junk-IG-a_',ii,'_.fits'
      call writefits(savefile,a,header,ok)
      if (.not.ok) return
C Read image b.
      call readfits(flat2,b,maxpix,header,ok)
      if (.not.ok) return
      call BIW(b,a,header,maskfile,0,sc,ec,sr,er,ok)
      if (.not.ok) return
C Recover image a.
      call readfits(savefile,a,maxpix,header,ok)
      if (.not.ok) return
C Find inverse gain.
      call find_eperdn(a,b,sc,ec,sr,er,rov,eperdn)
C Delete temporary file.
      call delete_file(savefile,ok)
C
      ok=.true.
      return
      end

C----------------------------------------------------------------------
C Determines if a small region about the given point col,row is "flat"
C (within) 5%, is above 5000 DN, is not too high (<40000), and is not
C too close to the boundaries of the image.  If "initial" is less than zero,
C then it is set to the data value at col,row, otherwise the pixel at col,row
C is required to be within 10% of "initial".
C
      logical function flatflat(a,sc,ec,sr,er,col,row,initial)
      implicit none
      integer*4 sc,ec,sr,er,col,row
      real*4 a(sc:ec,sr:er),aa,mp,initial
      parameter(mp=0.05)
      aa=a(col,row)
      flatflat= (  (abs(a(col-3,row-3)-aa)/aa.lt.mp).and.
     .             (abs(a(col+3,row-3)-aa)/aa.lt.mp).and.
     .             (abs(a(col-3,row+3)-aa)/aa.lt.mp).and.
     .             (abs(a(col+3,row+3)-aa)/aa.lt.mp).and.
     .             (col.gt.sc+10).and.(col.lt.ec-10).and.
     .             (row.gt.sr+10).and.(row.lt.er-10).and.
     .             (aa.gt.5000.).and.(aa.lt.40000) )
      if (flatflat) then
        if (initial.lt.0.) then
          initial = aa 
        else
          flatflat = (abs(aa-initial)/aa.lt.0.1)
        endif
      endif
      return
      end


C----------------------------------------------------------------------
C Find electrons per digital number given two flat field images.
C Input: a,b,sc,ec,sr,er,rov  Output: eperdn.
C "rov" is the readout variance in DN^2.
C If rov is negative than it is a guess at the ronoise in electrons.
C
      subroutine find_eperdn(fa,fb,sc,ec,sr,er,rov,eperdn)
C
      implicit none
      integer*4 sc,ec,sr,er                   ! dimensions (input)
      real*4 fa(sc:ec,sr:er),fb(sc:ec,sr:er)  ! two flat images (input)
      real*4 rov                              ! readout variance (input)
      real*4 eperdn                           ! eperdn (output)
C
C Need mpix, arr(..), z(..), and zpk(..) from include file.
      include 'makee.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
C These variables use the same memory space allocated to z() and zpk().
      real*4 av(mpix),bv(mpix)
      equivalence (z,av),(zpk,bv)
C
      real*4 mna,mnb,mnr,sum,ratio,rr,rms2,n2
      real*4 AA,BB,CC,initial
      integer*4 num,i,j,buf,tot,narr,np,row,col,j1,j2,ii
      logical flatflat
C
C One tenth of the image size.  Use for buffer from edges of image.
      buf=max(1,min((ec-sc)/10,(er-sr)/10))
C
C Find mean ratio between the two images.  Use middle part of image.
      sum=0.
      num=0
      do i=sc+buf,ec-buf,5
        do j=sr+buf,er-buf,5
          if ((fa(i,j).gt.5000.).and.(fb(i,j).gt.5000.)) then
            sum=sum+(fa(i,j)/fb(i,j))
            num=num+1
          endif
        enddo
      enddo
      if (num.lt.100) then
        write(soun,'(2a)') 
     .           'ERROR: find_eperdn: Not enough pix',
     .           'els above 5000 counts in flats.'
        goto 909
      endif
      ratio = sum/float(num)
C Be sure that all points are within 10% of ratio.
      num=0
      tot=0
      do i=sc+buf,ec-buf,5
        do j=sr+buf,er-buf,5
          if ((fa(i,j).gt.5000.).and.(fb(i,j).gt.5000.)) then
            tot=tot+1
            rr = fa(i,j)/fb(i,j)
            if (abs(rr-ratio)/ratio.lt.0.20) num=num+1
          endif
        enddo
      enddo
      if (float(num)/float(tot).lt.0.80) then
        write(soun,'(a)') 
     .  'ERROR: find_eperdn: Flats appear to inconsistent...'
        goto 909
      endif

C Start at middle column starting row and look for a region where the
C counts are above 5000 and are within 10% of each other.
      narr=0
      col=sc+100
      DO WHILE(col.lt.ec-100)

      row=sr+100
1     continue
      initial=-1.
      do while((row.lt.er-100).and.
     .         (.not.flatflat(fa,sc,ec,sr,er,col,row,initial)))
        row=row+1
      enddo
      if (row.eq.er-100) goto 9
      i=col
C Define lower bound.
      j=row-1
      do while(flatflat(fa,sc,ec,sr,er,i,j,initial))
        j=j-1
      enddo
      j1=j+1
C Define upper bound.
      j=row+1
      do while(flatflat(fa,sc,ec,sr,er,i,j,initial))
        j=j+1
      enddo
      j2=j-1 
      if (j2-j1.lt.3) goto 5
C Do each row between bounds.
      np=0
      j=j1
      do while(j.le.j2)
C Look left.
        i=col
        do while(flatflat(fa,sc,ec,sr,er,i,j,initial))
          np=np+1
          av(np)=fa(i,j)
          bv(np)=fb(i,j)
          if (np.eq.mpix) goto 3
          i=i-1
        enddo
C Look right.
        i=col+1
        do while(flatflat(fa,sc,ec,sr,er,i,j,initial))
          np=np+1
          av(np)=fa(i,j)
          bv(np)=fb(i,j)
          if (np.eq.mpix) goto 3
          i=i+1
        enddo
        j=j+1
      enddo
3     continue
C Use points to calculate an eperdn.
      if (np.gt.500) then
        mna=0.
        mnb=0.
        mnr=0.
        do ii=1,np
          mna=mna+av(ii)
          mnb=mnb+bv(ii)
          mnr=mnr+(av(ii)/bv(ii))
        enddo
        mna=mna/float(np) 
        mnb=mnb/float(np) 
        mnr=mnr/float(np)
C Find rms in ratio.
        sum=0.
        num=0
        do ii=1,np
          rr = av(ii)/bv(ii)
          sum= sum + ((rr-mnr)*(rr-mnr))
        enddo
        rms2 = sum/float(np)
C If rov is less than zero then it is a guess at the ronoise in electrons.
        if (rov.lt.0.) then
          N2=rov*rov
          AA=mnb*mnb*rms2
          BB=-1.*( mna + ((mna*mna)/mnb) )
          CC=-1.*( N2 + ((mna*mna*N2)/(mnb*mnb)) )
          eperdn = ( (-1.*BB) + sqrt((BB*BB)-(4.*AA*CC)) ) / (2.*AA)
        else
          eperdn = ( mna + (mna*mnr) )
     .           / ( (mnb*mnb*rms2) - (rov*(1.0+(mnr*mnr))) )
        endif
        narr=narr+1
        arr(narr)=eperdn
      endif
C Jump 100 rows and find new region. 
5     continue
      row = row + 100
      if (row.lt.er-100) goto 1

C Find median eperdn near a different column.
9     continue

      col=col+100

      ENDDO
      if (narr.eq.0) goto 909
      call find_median(arr,narr,eperdn)
      write(soun,'(a,i6,f9.4)') 'narr,eperdn=',narr,eperdn
      return
909   continue
      eperdn=-1.
      return
      end


C---------------------------------------------------------------------- 
C Read in two dark images.  Baseline/window and then find readout-variance
C in DN (rov).  a() and b() should be big enough to hold an entire raw image.
C
      subroutine ReadoutVariance(dark1,dark2,maskfile,a,b,header,
     .                           rov,ok) 
C
      implicit none 
      character*(*) dark1,dark2   ! FITS filenames (input)
      character*(*) maskfile      ! Bad column mask data filename (input)
      real*4 a(*),b(*)            ! Data images (scratch)
      character*(*) header        ! FITS header (scratch)
      real*4 rov                  ! Readout Variance in DN (output)
      logical ok                  ! Success? (output)
C
      include 'maxpix.inc'
      include 'global.inc'
C
      integer*4 sc,ec,sr,er,ii,fftimeget
      character*80 savefile
C
C FITS Filenames.
      call AddFitsExt(dark1)
      call AddFitsExt(dark2)
C Read in image a.
      call readfits(dark1,a,maxpix,header,ok)
      if (.not.ok) return
C Check globals.
      call EE_CheckInstrument( header )
      call BIW(a,b,header,maskfile,0,sc,ec,sr,er,ok)
      if (.not.ok) return
C Save image a.
      ii = fftimeget()
      write(savefile,'(a,i10.10,a)') 'junk-RV-a_',ii,'_.fits'
      call writefits(savefile,a,header,ok)
      if (.not.ok) return
C Read in image b.
      call readfits(dark2,b,maxpix,header,ok)
      if (.not.ok) return
      call BIW(b,a,header,maskfile,0,sc,ec,sr,er,ok)
      if (.not.ok) return
C Recover image a.
      call readfits(savefile,a,maxpix,header,ok)
      if (.not.ok) return
C Find variance.
      call find_rov(a,b,sc,ec,sr,er,rov)
C Delete temporary file.
      call delete_file(savefile,ok)
C
      return
      end



C----------------------------------------------------------------------
C Find readout-variance in DN (rov) given two short darks.
C
      subroutine find_rov(a,b,sc,ec,sr,er,rov)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er),b(sc:ec,sr:er),rov
      integer*4 col,row,ii,jj,num,colinc,rowinc,narr,i1,i2,i
      real*4 sum,mean,rms,arr(9000),r
C Subtract a from b. 
      do col=sc,ec
        do row=sr,er
          a(col,row) = a(col,row) - b(col,row)
        enddo
      enddo
C Divide image into an 10 by 10 grid and find rms in each grid.
      colinc=(ec-sc)/10
      rowinc=(er-sr)/10
      narr=0
      do ii=sc+colinc,ec-(2*colinc),colinc
        do jj=sr+rowinc,er-(2*rowinc),rowinc
          sum=0.
          num=0
          do col=ii,ii+colinc
            do row=jj,jj+rowinc
              sum = sum + a(col,row)
              num = num + 1
            enddo
          enddo
          mean=sum/float(max(1,num))
          sum=0.
          num=0
          do col=ii,ii+colinc
            do row=jj,jj+rowinc
              r   = (a(col,row)-mean)
              sum = sum + (r*r)
              num = num + 1
            enddo
          enddo
          rms = sqrt(sum/float(max(1,num)))
C Do it again, this time rejecting bad pixels.
          sum =0.
          num =0
          do col=ii,ii+colinc
            do row=jj,jj+rowinc
              r   = (a(col,row)-mean)
              if (abs(r).lt.rms*4.) then
                sum = sum + (r*r)
                num = num + 1
              endif
            enddo
          enddo
          rms = sqrt(sum/float(max(1,num)))
          narr=narr+1
          arr(narr)=rms
        enddo
      enddo
C Regular median.
      call find_median(arr,narr,rms)
C Improved average/median.
      if (narr.gt.9) then
        i1 = max(2,min(narr-1,1+nint(float(narr-1)*0.3)))
        i2 = max(2,min(narr-1,1+nint(float(narr-1)*0.7)))
        sum=0.
        do i=i1,i2
          sum=sum+arr(i)
        enddo
        rms=sum/float(1+i2-i1)
      endif
C Translate to readout variance in DN^2.
      rov = (rms*rms)/2.
      return
      end


C----------------------------------------------------------------------
C Replace each column with the median of that column. Ignores masked regions.
C
      subroutine CB_Mush(a,sc,ec,sr,er,arr,mush)
C
      implicit none
      integer*4 sc,ec,sr,er,n,col,row,ii
      real*4 a(sc:ec,sr:er),arr(*),mush(sc:ec)
C
      include 'soun.inc'
C
C Find median of each column. Ignore masked regions.
      do col=sc,ec
        n=0
        do row=sr,er
          if (a(col,row).gt.-1.e+5) then
            n=n+1
            arr(n)=a(col,row)
          endif
        enddo
        if (n.gt.100) then
          call find_median(arr,n,mush(col))
        else
          mush(col)=-1.e+10
        endif
      enddo
C Try to correct for problems near bad columns which run the length of the CCD.
      do col=sc,ec
        if (mush(col).lt.-1.e+5) then
          n=0
          ii=col-1
          do while((ii.gt.sc).and.(n.lt.10))
            if (mush(ii).gt.-1.e+5) then
              n=n+1
              arr(n)=mush(ii)
            endif
            ii=ii-1
          enddo
          ii=col+1
          do while((ii.lt.ec).and.(n.lt.20))
            if (mush(ii).gt.-1.e+5) then
              n=n+1
              arr(n)=mush(ii)
            endif
            ii=ii+1
          enddo
          if (n.gt.4) then
            call find_median(arr,n,mush(col))
          else
            mush(col)=-1.e+10
          endif
        endif
      enddo
C Check for problems.
      n=0
      do col=sc,ec
        if (mush(col).lt.-1.e+5) then
          mush(col)=0.
          n=n+1
        endif
      enddo
      if (n.gt.0) then
        write(soun,'(a,i5,a)')
     .  'WARNING: Problem with',n,' bad columns in darks.'
      endif
C Replace image with mush.
      do col=sc,ec
        do row=sr,er
          a(col,row)=mush(col)
        enddo
      enddo
      return
      end


C----------------------------------------------------------------------
C Mask out (set to -1.e+10) all bad regions on CCD.
C
      subroutine Mask_Bad_Regions(a,sc,ec,sr,er,maskfile)
C
      implicit none
      integer*4 sc,ec,sr,er           ! input
      real*4 a(sc:ec,sr:er)           ! input/output
      character*(*) maskfile          ! input
C
      include 'soun.inc'
C
      integer*4 bsc,bec,bsr,ber,row,col,lc,i
      character*100 line
      real*8 fgetlinevalue
C
      if (maskfile.eq.' ') return
      open(31,file=maskfile,status='old',iostat=i)
      if (i.ne.0) then
        write(soun,'(2a)') 
     .     'WARNING: MBR: Could not find maskfile : ',
     .     maskfile(1:lc(maskfile))
        return
      endif
1     continue
      read(31,'(a)',end=5,err=801) line
      bsc = nint( fgetlinevalue(line,1) )
      bec = nint( fgetlinevalue(line,2) )
      bsr = nint( fgetlinevalue(line,3) )
      ber = nint( fgetlinevalue(line,4) )
      if ((bsc.lt.-1).or.(bec.lt.-1).or.
     .                (bsr.lt.-1).or.(ber.lt.-1)) then
        write(soun,'(2a)')
     .     'ERROR: MBR: Reading line from ',maskfile
        write(soun,'(2a)')
     .     'ERROR: MBR: Bad line: ',line(1:lc(line))
        return
      endif
      if (bsc.eq.-1) bsc=sc     
      if (bec.eq.-1) bec=ec     
      if (bsr.eq.-1) bsr=sr     
      if (ber.eq.-1) ber=er     
      bsc=max(sc,min(ec,bsc)) 
      bec=max(sc,min(ec,bec)) 
      bsr=max(sr,min(er,bsr)) 
      ber=max(sr,min(er,ber)) 
C Set to -1.e+10 bad region.
      do col=bsc,bec
        do row=bsr,ber
          a(col,row)=-1.e+10
        enddo
      enddo
      goto 1
5     continue
      close(31) 
      return
C
801   continue
      write(soun,'(2a)') 
     .  'ERROR:MBR: reading maskfile : ',maskfile(1:lc(maskfile))
      close(31)
      call exit(1)
      end

C ------------------------------------------------------------------------------
C Operate on FITS images.  "soun.inc" must be set.
C
      subroutine opim_main(arg,narg,ok)
C
      implicit none
      character*(*) arg(*)    ! "Command line" arguments (input)
      integer*4 narg          ! Number of arguments (input)
      logical ok              ! Success? (output)
C
      include 'maxpix.inc'
      include 'soun.inc'
C
      real*4 a(maxpix),b(maxpix)
      real*4 high,low
      integer*4 i,j,inhead,npix,lc,k,nfile,access,expos
      integer*4 col,row,newcol,newrow,sc,ec,sr,er,nc,nr
      real*8 const
      character header*57600, op*3
      character*80 file(19),wrd,resultfile,cc,chead,object
      logical have_const,findarg,clobber,verbose,constfirst
      real*8 mean,rms,echa,xda,fhead,valread8

C Look at arguments.
C Make comment card for header.
      cc = 'opim'
      do i=1,narg
        cc = cc(1:lc(cc))//' '//arg(i)(1:lc(arg(i)))
      enddo
C Get constant option.
      have_const= findarg(arg,narg,'const=',':',wrd,i)
      if (have_const) const=valread8(wrd)
C Other options.
      clobber   = findarg(arg,narg,'-clobber',':',wrd,i)
      verbose   = findarg(arg,narg,'-verbose',':',wrd,i)
      constfirst= findarg(arg,narg,'-constfirst',':',wrd,i)
C Get operation option.
      op = arg(1)(1:3)
C Check to see that we have enough arguments.
      ok=.true.
      if (narg.lt.2) ok=.false.
      if (ok) then
        if ((op.eq.'log').or.(op.eq.'ln ')) then
          if ((narg.ne.3).or.(have_const)) ok=.false.
        elseif ((op.ne.'mea').and.(op.ne.'mdn').and.
     .          (.not.have_const)) then
          if (narg.lt.3) ok=.false.
        endif
      endif
      if (.not.ok) then
        print *,
     .  'Syntax: opim [ add | sub | div | mul | pow | log | ln | mean'
        print *,
     .  '                 | mdn | ade | abs | min | max | rol | ror ]'
C       print *,'                    | ftb | frl | fur | ful ]'
        print *,
     .'   (FITS file 1) (FITS file 2) ... (FITS file n) (result FITS )'
        print *,'  [const=] [-clobber] [-verbose] [-constfirst]'
        print *,' '
        print *,
     .  '      Operates on FITS files.  Valid operations: add, sub,'
        print *,'      div, mul, pow, log, ln, mean, mdn, or ade.'
        print *,' '
        print *,
     .  '    FITS header from file 1 will be used for result image.'
        print *,' '
        print *,
     .  '    const=  : specify a constant to use in the operation'
        print *,
     .  '              (constant will be last in operation sequence.)'
        print *,
     .  '   -clobber : overwrite "result FITS file" with no query.'
        print *,
     .  '   -verbose : give more details (and check HIRES angles.)'
        print *,
     .  '-constfirst : put the constant first in operation sequence.'
        print *,
     .  '              (e.g.  result = constant (operation) file .)'
        print *,
     .  ' '
        print *,
     .  'Operations: add="+", sub="-", div="/", mul="*", pow="**",'
        print *,
     .  '     log="log base 10", ln="log base e", mean="calculate'
        print *,
     .  '     mean value", mdn="calculate median value"'
        print *,
     .  '     ade="add error images in quadrature"'
        print *,
     .  '     abs="take absolute value of data"'
        print *,
     .  '     min="clip data at a minimum value given by const= "'
        print *,
     .  '     max="clip data at a maximum value given by const= "'
        print *,
     .  '     rol="Rotate image 90 degrees left (counter-clockwise).'
        print *,
     .  '     ror="Rotate image 90 degrees right (clockwise).'
        print *,
     .  '       ( "ror" and "rol" work for square images only.)'
C
C       print *,
C    .  '     ftb="Flip tob-to-bottom (horizontal axis).'
C       print *,
C    .  '     frl="Flip right-to-left (vertical axis).'
C       print *,
C    .  '     fur="Flip along diagonal through upper right corner.'
C       print *,
C    .  '     ful="Flip along diagonal through upper right corner.'
C
        ok=.false.
        return
      endif

C Load filenames.
      do i=2,narg
        file(i-1)=arg(i)
        call AddFitsExt(file(i-1))
      enddo
      nfile = narg-1
C Do we need a result file?
      if ((op.ne.'mea').and.(op.ne.'mdn')) then
        resultfile=file(nfile)
        call AddFitsExt(resultfile)
        nfile = nfile-1
        if ((.not.clobber).and.(access(resultfile,'r').eq.0)) then
          if (soun.eq.6) then
            print '(2a)',
     .  ' Result file already exists: ',resultfile(1:lc(resultfile))
            print '(a,$)',' Do you want to overwrite (1/0) ? '
            read(5,*) i
            if (i.eq.0) then
              ok=.true.
              return
            endif
          else
            write(soun,'(2a)') ' Overwriting result file: ',
     .                               resultfile(1:lc(resultfile))
          endif
        endif
      endif

C Read in first file.
      if ((op.ne.'mea').and.(op.ne.'mdn')) then
        call readfits(file(1),a,maxpix,header,ok)
        if (.not.ok) goto 801
        npix = max(1,inhead('NAXIS1',header))*
     .         max(1,inhead('NAXIS2',header))
        if (verbose) write(soun,'(a,i8)') '  Number of pixels:',npix
        call GetDimensions(header,sc,ec,sr,er,nc,nr)
        object= chead('OBJECT',header)
        echa  = fhead('ECHANGL',header)
        xda   = fhead('XDANGL',header)
        expos = inhead('EXPOSURE',header)
        if (verbose) then
          if (echa.gt.-1000.) then
            write(soun,'(a,2f8.4,i6,2a)') 
     .               file(1)(1:14),echa,xda,expos,'s  ',
     .               object(1:min(40,lc(object)))
          else
            write(soun,'(a,a)') 
     .               file(1)(1:14),object(1:min(40,lc(object)))
          endif
        endif
      endif

C Apply option.
      if (op.eq.'mea') then
        do k=1,nfile
          npix=0
          call rdff(file(k),a,npix,verbose,echa,xda,ok)
          if (.not.ok) return
          write(soun,'(3a,i8)') ' File: ',file(k)(1:lc(file(k))),
     .                          '   Number of pixels:',npix
          mean=0.d0
          high=a(1)
          low =a(1)
          do i=1,npix
            mean=mean+dble(a(i))
            if (a(i).gt.high) high=a(i)
            if (a(i).lt.low ) low =a(i)
          enddo
          mean=mean/dfloat(npix)
          rms=0.d0
          do i=1,npix
            rms=rms+((dble(a(i))-mean)*(dble(a(i))-mean))
          enddo
          rms=sqrt(rms/dfloat(npix))
          write(soun,'(a,1pe15.7)') ' Mean=',mean
          write(soun,'(a,1pe15.7)') ' RMS =',rms
          write(soun,'(a,1pe15.7)') ' High=',high
          write(soun,'(a,1pe15.7)') ' Low =',low
        enddo
        ok=.true.
        return

      elseif (op.eq.'mdn') then
        do k=1,nfile
          npix=0
          call rdff(file(k),a,npix,verbose,echa,xda,ok)
          if (.not.ok) return
          write(soun,'(3a,i8)') 'File: ',file(k)(1:lc(file(k))),
     .                          '   Number of pixels:',npix
          call find_median(a,npix,low)
          write(soun,'(a,1pe15.7)') 
     .  ' 01%=',a(max(1,min(npix,nint(float(npix)*0.01))))
          write(soun,'(a,1pe15.7)') 
     .  ' 10%=',a(max(1,min(npix,nint(float(npix)*0.1))))
          write(soun,'(a,1pe15.7)') 
     .  ' 20%=',a(max(1,min(npix,nint(float(npix)*0.2))))
          write(soun,'(a,1pe15.7)') 
     .  ' 30%=',a(max(1,min(npix,nint(float(npix)*0.3))))
          write(soun,'(a,1pe15.7)') 
     .  ' 40%=',a(max(1,min(npix,nint(float(npix)*0.4))))
          write(soun,'(a,1pe15.7)') 
     .  ' 50%=',a(max(1,min(npix,nint(float(npix)*0.5))))
          write(soun,'(a,1pe15.7)') 
     .  ' 60%=',a(max(1,min(npix,nint(float(npix)*0.6))))
          write(soun,'(a,1pe15.7)') 
     .  ' 70%=',a(max(1,min(npix,nint(float(npix)*0.7))))
          write(soun,'(a,1pe15.7)') 
     .  ' 80%=',a(max(1,min(npix,nint(float(npix)*0.8))))
          write(soun,'(a,1pe15.7)') 
     .  ' 90%=',a(max(1,min(npix,nint(float(npix)*0.9))))
          write(soun,'(a,1pe15.7)') 
     .  ' 99%=',a(max(1,min(npix,nint(float(npix)*0.99))))
        enddo
        ok=.true.
        return

      elseif (op.eq.'pow') then
        if (((have_const).and.(nfile.gt.1)).or.(nfile.gt.2)) then
          write(soun,'(2a)') 'ERROR-- You can only specify one ex',
     .            'tra image or one constant with "pow" option.'
          ok=.false.
          return
        endif
        if (nfile.eq.2) then
          call rdff(file(2),b,npix,verbose,echa,xda,ok)
          if (.not.ok) return
          do i=1,npix
            if (b(i).lt.1.) then
              if (a(i).lt.1.e-30) then
                a(i) = 0.
              else
                a(i) = sngl( dble(a(i)) ** dble(b(i)) )
              endif
            else
              a(i) = sngl( dble(a(i)) ** dble(b(i)) )
            endif
          enddo
        elseif (have_const.and.constfirst) then
          do i=1,npix
            a(i) = sngl( const ** dble(a(i)) )
          enddo
        elseif (have_const) then
          if (dfloat(nint(const)).ne.const) then
            do i=1,npix
              if (a(i).lt.1.e-30) then
                a(i) = 0.
              else
                a(i) = sngl( dble(a(i)) ** const )
              endif
            enddo
          else
            do i=1,npix
              a(i) = sngl( dble(a(i)) ** const )
            enddo
          endif
        else
          write(soun,'(a)') 'ERROR with pow option.'
        endif

      elseif (op.eq.'ade') then
        do i=1,npix
          if (a(i).gt.1.e-30) then
            a(i) = a(i)*a(i)
          else
            a(i) = -1.
          endif
        enddo
        if (nfile.gt.1) then
          do k=2,nfile
            call rdff(file(k),b,npix,verbose,echa,xda,ok)
            if (.not.ok) return
            do i=1,npix
              if ((a(i).gt.1.e-30).and.(b(i).gt.1.e-30)) then
                a(i) = a(i) + ( b(i)*b(i) )
              else
                a(i) = -1.
              endif
            enddo
          enddo
        endif
        if (have_const) then
          do i=1,npix
            if ((a(i).gt.1.e-30).and.(const.gt.1.e-30)) then
              a(i) = a(i) + sngl(const*const)
            else
              a(i) = -1.
            endif
          enddo
        endif
        do i=1,npix
          if (a(i).gt.1.e-30) a(i) = sqrt(a(i))
        enddo

      elseif (op.eq.'add') then
        if (nfile.gt.1) then
          do k=2,nfile
            call rdff(file(k),b,npix,verbose,echa,xda,ok)
            if (.not.ok) return
            do i=1,npix
              a(i) = a(i) + b(i)
            enddo
          enddo
        endif
        if (have_const) then
          do i=1,npix
            a(i) = a(i) + sngl(const)
          enddo
        endif

      elseif (op.eq.'sub') then
        if (have_const.and.constfirst) then
          do i=1,npix
            a(i) = sngl(const) - a(i)
          enddo
        endif
        if (nfile.gt.1) then
          do k=2,nfile
            call rdff(file(k),b,npix,verbose,echa,xda,ok)
            if (.not.ok) return
            do i=1,npix
              a(i) = a(i) - b(i)
            enddo
          enddo
        endif
        if (have_const) then
          do i=1,npix
            a(i) = a(i) - const
          enddo
        endif

      elseif (op.eq.'mul') then
        if (nfile.gt.1) then
          do k=2,nfile
            call rdff(file(k),b,npix,verbose,echa,xda,ok)
            if (.not.ok) return
            do i=1,npix
              a(i) = a(i) * b(i)
            enddo
          enddo
        endif
        if (have_const) then
          do i=1,npix
            a(i) = a(i) * const
          enddo
        endif

      elseif (op.eq.'div') then
        ok=.true.
        if (have_const.and.constfirst) then
          do i=1,npix
            if (abs(a(i)).gt.1.e-30) then
              a(i) = sngl( const / dble(a(i)) )
            else
              ok=.false.
              a(i) = 0.
            endif
          enddo
        endif
        if (nfile.gt.1) then
          do k=2,nfile
            call rdff(file(k),b,npix,verbose,echa,xda,ok)
            if (.not.ok) return
            do i=1,npix
              if (abs(b(i)).gt.1.e-30) then
                a(i) = a(i) / b(i)
              else
                ok=.false.
                a(i) = 0.
              endif
            enddo
          enddo
        endif
        if (have_const.and.(.not.constfirst)) then
          if (abs(const).gt.1.e-30) then
            do i=1,npix
              a(i) = a(i) / const
            enddo
          else
            ok=.false.
            do i=1,npix
              a(i) = 0.
            enddo
          endif
        endif
        if (.not.ok) then
          write(soun,'(2a)') 'WARNING: ',
     .  'Found ~0 value pixels in denominator. Setting result to 0.'
        endif

      elseif (op.eq.'log') then
        if (have_const) then
          write(soun,'(a)') 
     .  'ERROR- You cannot specify a constant with "log".'
          ok=.false.
          return
        endif
        if (nfile.gt.1) then
          write(soun,'(a)') 
     .  'ERROR- You can only specify one file with "log".'
          ok=.false.
          return
        endif
        ok=.true.
        do i=1,npix
          if (a(i).gt.1.e-31) then
            a(i) = log10( a(i) )
          else
            ok=.false.
            a(i) = 0.
          endif
        enddo
        if (.not.ok) then
          write(soun,'(a)') 
     .  'WARNING: Found <~0 value pixels. Setting result to 0.'
        endif

C Rotate image counter-clockwise.
      elseif (op.eq.'rol') then
        do i=1,npix
          row= 1 + ((i-1)/nc)
          col= i - ((row-1)*nc)
          newcol = row
          newrow = 1 + nc - col
          j = newcol + (newrow-1)*nc
          b(j) = a(i)
        enddo
        do i=1,npix
          a(i) = b(i)
        enddo

C Rotate image clockwise.
      elseif (op.eq.'ror') then
        do i=1,npix
          row= 1 + ((i-1)/nc)
          col= i - ((row-1)*nc)
          newcol = 1 + nr - row
          newrow = col
          j = newcol + (newrow-1)*nc
          b(j) = a(i)
        enddo
        do i=1,npix
          a(i) = b(i)
        enddo
          
      elseif (op.eq.'abs') then
        if (have_const) then
          write(soun,'(a)') 
     .  'ERROR- You cannot specify a constant with "abs".'
          ok=.false.
          return
        endif
        if (nfile.gt.1) then
          write(soun,'(a)') 
     .  'ERROR- You can only specify one file with "abs".'
          ok=.false.
          return
        endif
        do i=1,npix
          a(i) = abs( a(i) )
        enddo

      elseif (op.eq.'min') then
        if (.not.have_const) then
          write(soun,'(a)') 
     .  'ERROR- You must specify a constant with "min".'
          ok=.false.
          return
        endif
        if (nfile.gt.1) then
          write(soun,'(a)') 
     .  'ERROR- You can only specify one file with "min".'
          ok=.false.
          return
        endif
        do i=1,npix
          if (a(i).lt.const) a(i)=const
        enddo

      elseif (op.eq.'max') then
        if (.not.have_const) then
          write(soun,'(a)') 
     .  'ERROR- You must specify a constant with "max".'
          ok=.false.
          return
        endif
        if (nfile.gt.1) then
          write(soun,'(a)') 
     .  'ERROR- You can only specify one file with "max".'
          ok=.false.
          return
        endif
        do i=1,npix
          if (a(i).gt.const) a(i)=const
        enddo

      elseif (op.eq.'ln ') then
        if (have_const) then
          write(soun,'(a)') 
     .  'ERROR- You cannot specify a constant with "ln".'
          ok=.false.
          return
        endif
        if (nfile.gt.1) then
          write(soun,'(a)') 
     .  'ERROR- You can only specify one file with "ln".'
          ok=.false.
          return
        endif
        ok=.true.
        do i=1,npix
          if (a(i).gt.1.e-31) then
            a(i) = log( a(i) )
          else
            ok=.false.
            a(i) = 0.
          endif
        enddo
        if (.not.ok) then
          write(soun,'(a)') 
     .  'WARNING: Found <~0 value pixels. Setting result to 0.'
        endif

      else
        write(soun,'(a)') 'ERROR - Unknown option: ',op
        ok=.false.
        return

      endif
C Write out result.  Add comment to header.  Add option to OBJECT name.
      call cheadset('COMMENT',cc,header)
      wrd = chead('OBJECT',header)
      write(wrd,'(4a)',err=701) wrd(1:lc(wrd)),' [',op,']'
      call cheadset('OBJECT',wrd,header)
701   write(soun,'(2a)') '  Writing file : ',
     .  resultfile(1:lc(resultfile))
      call writefits(resultfile,a,header,ok)
      ok=.true.
      return
801   write(soun,'(2a)') 
     .  'ERROR: opim: Reading FITS image. File=',file(1)
      ok=.false.
      return
      end

C----------------------------------------------------------------------
C Read FITS file. If npix>0, then check to make sure new npix equals
C old npix.  If verbose then print info and check HIRES angles.
C
      subroutine rdff(file,a,npix,verbose,echa,xda,ok)

      implicit none
      character*(*) file    ! FITS filename (input)
      real*4 a(*)           ! Data image (output)
      integer*4 npix        ! # of pixels to match check and set (input/output)
      logical verbose       ! Verbosity? (input)
      real*8 echa,xda       ! Echelle and XD angles to match check (input)
      logical ok            ! Success? (output)
C
      include 'maxpix.inc'
      include 'soun.inc'
C
      integer*4 inhead,k,expos,lc
      character*57600 header
      character*80 chead,object
      logical checkang,card_exist
      real*8 echa0,xda0,fhead

C Read FITS file.
      call readfits(file,a,maxpix,header,ok)
      if (.not.ok) goto 801
C Print out more info.
      if (verbose) then
C Do we need to check the HIRES angles?
        checkang = ( (card_exist('EE_VERSN',header)).and.
     .                       (inhead('NAXIS',header).eq.2) )
        object= chead('OBJECT',header)
        echa0 = fhead('ECHANGL',header)
        xda0  = fhead('XDANGL',header)
        expos = inhead('EXPOSURE',header)
        if (echa0.gt.-1000.) then
          write(soun,'(a,2f8.4,i6,2a)')
     .      file(1:14),echa0,xda,expos,'s  ',
     .      object(1:min(40,lc(object)))
        else
          write(soun,'(2a)') file(1:14),object(1:min(40,lc(object)))
        endif
        if (checkang) then
          if ((abs(echa0-echa).gt.0.02).or.
     .        (abs(xda0-xda).gt.0.02)) then
            write(soun,'(a)') 
     .  'ERROR- Echelle or Cross Disperser angles differ.'
            ok=.false.
            return
          endif
        endif
      endif
      k= max(1,inhead('NAXIS1',header))*max(1,inhead('NAXIS2',header))
      if (npix.gt.0) then
        if (k.ne.npix) goto 802
      else
        npix = k
      endif
      return
801   write(soun,'(2a)') 'ERROR: rdff: Reading FITS image. File=',file
      ok=.false.
      return
802   write(soun,'(3a)') 
     .  'ERROR: rdff: Number of pixels in FI',
     .  'TS images do not match. File=',file
      ok=.false.
      return
      end


C----------------------------------------------------------------------
C Create new "maskfile" given the default 1x1 mask file.
C
      subroutine mk_select_maskfile(header,maskfile)
C
      implicit none
      character*(*) header    ! FITS header (input)
      character*(*) maskfile  ! filename of new mask file (output)
C
      character*80 defmaskfile,wrd,instr
      character*100 line
      integer*4 lc,i,j,filetime,time,x1,x2,y1,y2,ios
      integer*4 xbin,ybin,numamps,prepix,inhead
      real*8 fgetlinevalue
C
      include 'global.inc'
      include 'soun.inc'
C
C Binning.
      call GetBinning(header,xbin,ybin)
C
C Number of amps and prepix.
      prepix= inhead('PREPIX',header)
      if (prepix.lt.1) prepix= 0
      numamps= inhead('NUMAMPS',header)
      if (numamps.lt.1) numamps= 1
C
C New temporary mask file.
      if (Global_HIRES) then
        instr='HIRES'
      elseif (Global_ESI) then
        instr='ESI'
      else
        instr='Other'
      endif
      write(maskfile,'(3a,i1.1,a,i1.1,a,i1.1,a)') 
     .  'Mask',instr(1:lc(instr)),'_',xbin,'x',ybin,'_',numamps,'.dat'
C
C Check time, return if less than 10 hours old.
      i = filetime(maskfile)
      j = time()
      if (j-i.lt.36000) return
C Open original file.
      call GetMakeeHome(wrd)
      defmaskfile = 
     .    wrd(1:lc(wrd))//'/Mask'//instr(1:lc(instr))//'_1x1.dat'
      open(1,file=defmaskfile,status='old',iostat=ios)
      if (ios.ne.0) then
        print *,'ERROR: mk_select_maskfile: Cannot open file: ',
     .                         defmaskfile(1:lc(defmaskfile))
        maskfile=' '
        return
      endif
C Open new file.
      open(2,file=maskfile,status='unknown')
C Read.
1     continue
      read(1,'(a)',end=5) line
      x1 = nint( fgetlinevalue(line,1) )
      x2 = nint( fgetlinevalue(line,2) )
      y1 = nint( fgetlinevalue(line,3) )
      y2 = nint( fgetlinevalue(line,4) )
      if ((x1.lt.-1).or.(x2.lt.-1).or.
     .                (y1.lt.-1).or.(y2.lt.-1)) then
        write(soun,'(2a)')
     .     'ERROR: MSM: Reading line from ',maskfile
        write(soun,'(2a)')
     .     'ERROR: MSM: Bad line: ',line(1:lc(line))
        return
      endif
C Convert. 
      if ((x1.gt.0).and.(xbin.gt.1)) x1 = x1 / xbin
      if ((x2.gt.0).and.(xbin.gt.1)) 
     .  x2 = int( (float(x2)/float(xbin)) + 1. )
      if ((y1.gt.0).and.(ybin.gt.1)) y1 = y1 / ybin
      if ((y2.gt.0).and.(ybin.gt.1)) 
     .  y2 = int( (float(y2)/float(ybin)) + 1. )
C Adjust for prepix (2 amps only).
      if (numamps.gt.1) then
        x1 = x1 + prepix
        x2 = x2 + prepix
      endif
C Write.
      write(2,'(4i6)') x1,x2,y1,y2
      goto 1
5     close(1)
      close(2)
C
      return
      end


C----------------------------------------------------------------------
C Select which is the best value to use for eperdn and ronoise.
C
C Use in order:    (1) user value.
C                  (2) value in FITS header.
C                  (3) value in local "eperdn*.dat" or "ronoise*.dat" file.
C                  (4) value in default "eperdn*.dat" or "ronoise*.dat" file.
C
      subroutine mk_select_eperdn_ronoise(xb,yb,ue,ur,header,
     .  eperdn,ronoise)
C
      implicit none
C
      integer*4 xb,yb         ! column and row binning factor.
      real*4 ue,ur            ! user eperdn and readout noise.
C                             !             (use these values if > 0.)
      character*(*) header    ! FITS header.
      real*4 eperdn,ronoise   ! selected eperdn and readout noise.
C
      real*4 rr
      character*80 hh,file,wrd,eperdnfile,ronoisefile,chead
      real*8 fhead
      integer*4 lc
      logical card_exist,lhead
C
      include 'global.inc'
      include 'soun.inc'
C
      call GetMakeeHome(hh)
C
      if ((.not.Global_HIRES).and.(.not.Global_ESI)) then
        write(soun,'(2a)') 'ERROR: mk_select_eperdn_ronoise:',
     .                ' Global_HIRES or Global_ESI must be set.'
        call exit(0)
      endif
C
C ..........Electrons per digital number..........
      eperdn=-1.
C User value.
      if (ue.gt.0.) eperdn = ue
C FITS header value.
      if (eperdn.lt.0.) then
        rr = sngl(fhead('EPERDN',header))
        if (rr.gt.0.) eperdn = rr
      endif
C Local .dat file.
      if (eperdn.lt.0.) then
        call mk_read_dat('eperdn.dat',rr)
        if (rr.gt.0.) eperdn=rr
      endif
C Default .dat file.
      if (eperdn.lt.0.) then
        write(eperdnfile,'(a,a)') hh(1:lc(hh)),'/eperdn.dat'
        if (Global_HIRES) then
          if ( (card_exist('CCDGAIN',header)).and.
     .         (.not.lhead('CCDGAIN',header)) ) then
            write(eperdnfile,'(a,a)') 
     .            hh(1:lc(hh)),'/eperdnHIRES_logain.dat'
          else
            write(eperdnfile,'(a,a)') 
     .            hh(1:lc(hh)),'/eperdnHIRES_higain.dat'
          endif
        endif
        if (Global_ESI) then
          wrd = chead('CCDGAIN',header)
          if (index(wrd,'high').gt.0) then
            write(eperdnfile,'(a,a)') 
     .            hh(1:lc(hh)),'/eperdnESI_higain.dat'
          else
            write(eperdnfile,'(a,a)') 
     .            hh(1:lc(hh)),'/eperdnESI_logain.dat'
          endif
        endif
        call mk_read_dat(eperdnfile,rr)
        if (rr.gt.0) eperdn=rr
      endif
C Hard-wired default.
      if (eperdn.lt.0.) then
         eperdn=1.00
         if (Global_HIRES) eperdn=2.45
         if (Global_ESI)   eperdn=1.29
         write(soun,'(a,f9.4)') 
     .              'ERROR: Selecting eperdn, using ',eperdn
      endif
C
C ..............Readout noise in electrons..........
      ronoise=-1.
C User value.
      if (ur.gt.0.) ronoise = ur
C FITS header value.
      if (ronoise.lt.0.) then
        rr = sngl(fhead('RONOISE',header))
        if (rr.gt.0.) ronoise = rr
      endif
C Local .dat file.
      if (ronoise.lt.0.) then
        write(file,'(a,i1,a,i1,a)') 'ronoise_',xb,'x',yb,'.dat'
        call mk_read_dat(file,rr)
        if (rr.gt.0.) ronoise=rr
      endif
C Default .dat file.
      if (ronoise.lt.0.) then
        write(ronoisefile,'(a,a)') hh(1:lc(hh)),'/ronoise.dat'
        if (Global_HIRES) then
          write(ronoisefile,'(a,a)') 
     .          hh(1:lc(hh)),'/ronoiseHIRES.dat'
        endif
        if (Global_ESI) then
          write(ronoisefile,'(a,a)') 
     .          hh(1:lc(hh)),'/ronoiseESI.dat'
        endif
        call mk_read_dat(ronoisefile,rr)
        if (rr.gt.0.) ronoise=rr
      endif
C Hard-wired default.
      if (ronoise.lt.0.) then
        ronoise=6.0
        if (Global_HIRES) ronoise=6.0
        if (Global_ESI)   ronoise=2.7
         write(soun,'(a,f9.4)') 
     .         'ERROR: Selecting readout noise, using ',ronoise
      endif
C
      return
      end


C----------------------------------------------------------------------
C Read a single numeric value out of an ASCII file.
C
      subroutine mk_read_dat(file,rr)
C
      implicit none
      character*(*) file
      real*4 rr
      integer*4 ios
      open(1,file=file,status='old',iostat=ios)
      if (ios.ne.0) goto 9
      read(1,*,end=9,err=9) rr
      close(1)
      return
9     continue
CCC   print *,'NOTE: Cannot find file: ',file(1:lc(file))
      return
      end




C----------------------------------------------------------------------
C Given a row and column, determine the closest echelle order and
C load the appropriate polynomial coeffecients.
C
      subroutine Find_ESI_Polynomial(col,row,order,coef,xoff,yoff)
C
      implicit none
      integer*4 col,row ! position on processed CCD image.
      integer*4 order   ! polynomial order (output).
      real*8 coef(0:99) ! polynomial coeffecients (output).
      real*8 xoff       ! x-offset ( see polyvaloff() ) (output).
      real*8 yoff       ! y-offset ( add to polynomial ) (output).
C
      integer*4 eon,eon0
      real*8 col8,row8,rowdiff,lo,polyvaloff
C
      col8 = dfloat(col)
      row8 = dfloat(row)
      lo   = 1.e+40
      eon0 = -1
      do eon=1,20
        call Load_ESI_Polynomial(eon,order,coef,xoff)
        if (order.gt.-1) then
          rowdiff = abs( row8 - polyvaloff(order+1,coef,col8,xoff) )
          if (rowdiff.lt.lo) then
            eon0 = eon
            lo = rowdiff
          endif
        endif
      enddo
C
      if (eon0.gt.-1) then
        call Load_ESI_Polynomial(eon0,order,coef,xoff)
        yoff = row8 - polyvaloff(order+1,coef,col8,xoff)
      else
        print *,'ERROR: Finding ESI trace fit polynomial.'
      endif
C
      return
      end


C----------------------------------------------------------------------
C Load guesses at ESI echelle polynomial fits:  row = func(col)
C
      subroutine Load_ESI_Polynomial(eon,order,coef,xoff)
C
      implicit none
      integer*4 eon      ! ESI echelle order number (1=bluest on CCD) (input).
      integer*4 order    ! polynomial order (output).
      real*8 coef(0:99)  ! polynomial coeffecients (output).
      real*8 xoff        ! x-offset ( see polyvaloff() ) (output).
C
      order = -1
      xoff  = -1.
C
      if (eon.eq.1) then
        xoff    =  2.16273824498353D+03
        coef( 0)=  2.23223089098589D+02
        coef( 1)= -6.25769759476121D-03
        coef( 2)= -6.85926638514595D-05
        coef( 3)= -6.49822144084756D-09
        coef( 4)= -1.19991021915381D-11
        coef( 5)=  3.39829495292470D-14
        coef( 6)=  2.88126684823123D-17
        coef( 7)= -6.76208214341530D-20
        coef( 8)= -3.34601932326621D-23
        coef( 9)=  6.77345203242829D-26
        coef(10)=  1.92970601601932D-29
        coef(11)= -3.55776708167878D-32
        coef(12)= -5.29676385495613D-36
        coef(13)=  9.29301427273131D-39
        coef(14)=  5.49601525028529D-43
        coef(15)= -9.49143704234084D-46
        order = 15
      endif
C
      if (eon.eq.2) then
        xoff    =  2.06509362449799D+03
        coef( 0)=  4.92520006357957D+02
        coef( 1)= -1.61880216907185D-02
        coef( 2)= -6.67595765372934D-05
        coef( 3)= -4.93383780145796D-10
        coef( 4)=  3.70680636242372D-12
        coef( 5)=  1.56590360382650D-15
        coef( 6)= -8.35733979637050D-18
        coef( 7)=  4.92895061487026D-22
        coef( 8)=  7.40781980162206D-24
        coef( 9)= -1.85287584075767D-27
        coef(10)= -3.09683645157733D-30
        coef(11)=  1.06892561944476D-33
        coef(12)=  6.07554554902574D-37
        coef(13)= -2.42628261035452D-40
        coef(14)= -4.52243768549209D-44
        coef(15)=  1.96189153513226D-47
        order = 15
      endif
C
      if (eon.eq.3) then
        xoff    =  2.05797801648763D+03
        coef( 0)=  7.31625187844863D+02
        coef( 1)= -3.51050719527288D-02
        coef( 2)= -6.38416732831779D-05
        coef( 3)=  4.08078833032636D-09
        coef( 4)=  6.20938011799761D-12
        coef( 5)= -1.22595701708011D-14
        coef( 6)= -1.03605264595059D-17
        coef( 7)=  1.69919138748392D-20
        coef( 8)=  7.49178702628550D-24
        coef( 9)= -1.15381243819915D-26
        coef(10)= -2.66285288202566D-30
        coef(11)=  4.03956712118445D-33
        coef(12)=  4.55074110358086D-37
        coef(13)= -7.00883790072684D-40
        coef(14)= -2.99083772403030D-44
        coef(15)=  4.77496692023721D-47
        order = 15
      endif
C
      if (eon.eq.4) then
        xoff    =  2.07278535480624D+03
        coef( 0)=  9.44020779739155D+02
        coef( 1)= -5.25825164577177D-02
        coef( 2)= -5.90736348374536D-05
        coef( 3)=  1.87138213730434D-10
        coef( 4)= -1.68498104531414D-12
        coef( 5)=  3.65517229992253D-16
        coef( 6)=  2.83851694262828D-18
        coef( 7)= -1.02757354232824D-21
        coef( 8)= -2.57862281540362D-24
        coef( 9)=  1.08530714927174D-27
        coef(10)=  1.14759715656974D-30
        coef(11)= -5.23941770807018D-34
        coef(12)= -2.40758618729729D-37
        coef(13)=  1.15522306880368D-40
        coef(14)=  1.90271825417212D-44
        coef(15)= -9.43833649357727D-48
        order = 15
      endif
C
      if (eon.eq.5) then
        xoff    =  2.08350885627530D+03
        coef( 0)=  1.13452464208007D+03
        coef( 1)= -6.69227304365301D-02
        coef( 2)= -5.69095034846350D-05
        coef( 3)=  1.20316843224671D-10
        coef( 4)=  2.42999271058576D-13
        coef( 5)= -5.45896296827987D-16
        coef( 6)= -8.41104765521690D-19
        coef( 7)=  9.88346833382821D-22
        coef( 8)=  7.35624435395128D-25
        coef( 9)= -8.12008319766166D-28
        coef(10)= -3.23455562064660D-31
        coef(11)=  3.38818213853815D-34
        coef(12)=  6.93239233997418D-38
        coef(13)= -6.97675091936240D-41
        coef(14)= -5.68702115335461D-45
        coef(15)=  5.58952246486647D-48
        order = 15
      endif
C
      if (eon.eq.6) then
        xoff    =  2.06454165617921D+03
        coef( 0)=  1.30877856906208D+03
        coef( 1)= -7.55504618643925D-02
        coef( 2)= -5.46584341182145D-05
        coef( 3)=  1.49668235733047D-10
        coef( 4)= -1.18469921541057D-13
        coef( 5)= -5.10336462256108D-16
        coef( 6)= -1.14184502048131D-19
        coef( 7)=  5.81850410262573D-22
        coef( 8)=  8.75596240571690D-26
        coef( 9)= -3.58601956878975D-28
        coef(10)= -2.84466585588592D-32
        coef(11)=  1.19900563190384D-34
        coef(12)=  3.82155156898701D-39
        coef(13)= -2.02900838488907D-41
        coef(14)= -1.24583080782884D-46
        coef(15)=  1.34920573414512D-48
        order = 15
      endif
C
      if (eon.eq.7) then
        xoff    =  2.05263144779467D+03
        coef( 0)=  1.46804406436324D+03
        coef( 1)= -8.27319065678246D-02
        coef( 2)= -5.28317043236461D-05
        coef( 3)= -2.70184175134144D-10
        coef( 4)=  6.92517487007858D-14
        coef( 5)=  2.68056012298125D-16
        coef( 6)= -3.26453425117199D-19
        coef( 7)= -2.99672100945157D-22
        coef( 8)=  2.40192476385407D-25
        coef( 9)=  1.67994736593780D-28
        coef(10)= -8.82707946850184D-32
        coef(11)= -5.00898735331992D-35
        coef(12)=  1.60501735138257D-38
        coef(13)=  7.58831993236608D-42
        coef(14)= -1.14697356983974D-45
        coef(15)= -4.59530675932862D-49
        order = 15
      endif
C
      if (eon.eq.8) then
        xoff    =  2.04709698329594D+03
        coef( 0)=  1.61640856402928D+03
        coef( 1)= -8.84886311420133D-02
        coef( 2)= -5.11962545045749D-05
        coef( 3)= -1.61161068842744D-10
        coef( 4)=  8.32076988553936D-14
        coef( 5)= -1.33306920137327D-16
        coef( 6)= -2.49263290347074D-19
        coef( 7)=  1.44183626906169D-22
        coef( 8)=  1.62029307261323D-25
        coef( 9)= -1.03179546549671D-28
        coef(10)= -5.39586809696311D-32
        coef(11)=  4.09915869235190D-35
        coef(12)=  9.13960682648165D-39
        coef(13)= -8.06264531191746D-42
        coef(14)= -6.22543559637504D-46
        coef(15)=  6.11279434476874D-49
        order = 15
      endif
C
      if (eon.eq.9) then
        xoff    =  2.03554730238394D+03
        coef( 0)=  1.76075657448897D+03
        coef( 1)= -9.13359294314081D-02
        coef( 2)= -4.98689110665201D-05
        coef( 3)=  5.78176486864455D-10
        coef( 4)=  5.39228772134075D-13
        coef( 5)= -1.91569947195722D-15
        coef( 6)= -7.72145824838041D-19
        coef( 7)=  1.86829041799175D-21
        coef( 8)=  5.06032393279419D-25
        coef( 9)= -9.51874668060264D-28
        coef(10)= -1.75964982061718D-31
        coef(11)=  2.62801238419633D-34
        coef(12)=  3.09392100337565D-38
        coef(13)= -3.72648849683868D-41
        coef(14)= -2.15125726836432D-45
        coef(15)=  2.12709643558991D-48
        order = 15
      endif
C
      if (eon.eq.10) then
        xoff    =  1.83170496453901D+03
        coef( 0)=  1.92581718115113D+03
        coef( 1)= -7.25434322956242D-02
        coef( 2)= -4.80256552030242D-05
        coef( 3)= -2.21136575435076D-09
        coef( 4)= -3.61996163200078D-12
        coef( 5)=  3.79432092818171D-15
        coef( 6)=  6.74361903940799D-18
        coef( 7)= -5.33579214069342D-21
        coef( 8)= -6.23946298168214D-24
        coef( 9)=  3.99676872154821D-27
        coef(10)=  3.00191643895598D-30
        coef(11)= -1.58659880346986D-33
        coef(12)= -7.02871867991745D-37
        coef(13)=  3.26552988347857D-40
        coef(14)=  6.35515606583762D-44
        coef(15)= -2.76030877596227D-47
        order = 15
      endif
C
      return
      end

C----------------------------------------------------------------------
C Return .true. if this is a HIRES image.
C
      logical function EE_QueryHIRES( header )
C
      implicit none
      character*(*) header
      character*80 chead,wrd
      wrd = chead('INSTRUME',header)
      if (index(wrd,'HIRES').gt.0) then
        EE_QueryHIRES = .true.
      else
        EE_QueryHIRES = .false.
      endif
      return
      end

C----------------------------------------------------------------------
C Return .true. if this is a ESI image.
C
      logical function EE_QueryESI( header )
C
      implicit none
      character*(*) header
      character*80 chead,wrd
      wrd = chead('INSTRUME',header)
      if (index(wrd,'ESI').gt.0) then
        EE_QueryESI = .true.
      else
        EE_QueryESI = .false.
      endif
      return
      end

C----------------------------------------------------------------------
C Set global instrument type if not set.
C
      subroutine EE_CheckInstrument( header )
C
      implicit none
      character*(*) header
      logical EE_QueryHIRES,EE_QueryESI
C
      include 'global.inc'
      include 'soun.inc'
C
C Global set?
      if ((.not.Global_HIRES).and.(.not.Global_ESI)) then
        Global_HIRES= EE_QueryHIRES(header)
        Global_ESI  = EE_QueryESI(header)
        if ((Global_HIRES).and.(Global_ESI)) then
          write(soun,'(2a)')
     .     'ERROR: EE_CheckInstrument: ',
     .     'Both HIRES and ESI flags set. This should not happen.'
            call exit(0)
        endif
        if ((.not.Global_HIRES).and.(.not.Global_ESI)) then
          write(soun,'(2a)')
     .     'ERROR: EE_CheckInstrument: ',
     .     'Data must be either Global_HIRES or Global_ESI.'
          call exit(0)
        endif
      endif
C
      return
      end
