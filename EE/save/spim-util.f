C spim-util.f                           tab 1995-1998

CENDOFMAIN

C
C----------------------------------------------------------------------
C Find line centroid.  Input: sc,ec,sr,er,a,x,row,ino,ux1,ux2
C                     Output: cent,strength,fwhm
C Failure is indicated by all output values equal to zero.
C
      subroutine line_center(sc,ec,sr,er,a,x,row,ino,ux1,ux2,
     .  cent,strength,fwhm)

      implicit none
      integer*4 sc,ec,sr,er,row,ino
      real*4 a(sc:ec,sr:er,1:2),x(sc:ec,sr:er),ux1,ux2,cent
      real*4 strength,fwhm
C
      include 'spim1.inc'
      include 'verbose.inc'
C
      real*4 lo,sum,wsum,r,x1,x2,cent_last,hw,hi
      integer*4 i,i1,i2,nc,neari,iter
C
C Initialize.
      i1=0
      i2=0
C No. of columns.
      nc=1+ec-sc
C Correct sense for bounds.
      x1=min(ux1,ux2)
      x2=max(ux1,ux2)
      hw=abs(ux2-ux1)/2.
C Initial point.
      cent = (x1+x2)/2.
      if (NoCentroid) then
        print *,
     .  'WARNING: Not calculating centroid, using initial position.'
        goto 7
      endif
      iter=1
C Try again.
1     continue
      cent_last = cent
C Center x1 and x2 on cent.
      x1 = cent - hw
      x2 = cent + hw
C Find bounds.
      i1= neari(x1,nc,x(sc,row)) + sc - 1
      i2= neari(x2,nc,x(sc,row)) + sc - 1
C Find background.
      lo=a(i1,row,ino)
      do i=max(sc,i1-5),min(ec,i2+5)
        if (a(i,row,ino).lt.lo) lo=a(i,row,ino)
      enddo
C Find (2nd moment) centroid.
      sum=0.
      wsum=0.
      do i=i1,i2
        r = (a(i,row,ino)-lo)*(a(i,row,ino)-lo)
        r = max(0.,r)
        sum = sum + r*x(i,row)
        wsum= wsum+ r
      enddo
C Make sure weight sum is greater than zero.
      if (wsum.lt.1.e-10) goto 8
C Centroid.
      cent = sum / wsum
C Try again.
      if (abs(cent-cent_last).gt.0.01) then
        iter=iter+1
C Check for convergence.
        if ((iter.gt.7).and.verbose) then
          print '(a,f9.2)',
     .    'line_center: Trouble converging on line near pixel',
     .    (ux1+ux2)/2.
          goto 8
        endif
        goto 1
      endif
7     continue
C Find strength of line.
      strength = a(nint(cent),row,ino) 
C Find FWHM.
      i1=max(sc,i1-1)
      i2=min(ec,i2+1)
      hi=a(i1,row,ino)
      lo=a(i1,row,ino)
      do i=i1+1,i2
        if (a(i,row,ino).gt.hi) hi=a(i,row,ino)
        if (a(i,row,ino).lt.lo) lo=a(i,row,ino)
      enddo
      sum=0.
      do i=i1,i2
        sum=sum+max(0.,(a(i,row,ino)-lo))
      enddo
      fwhm=(0.939437)*sum/hi
      return
8     continue
      cent=0.
      strength=0.
      fwhm=0.
      return
      end


C---------------------------------------------------------
C
      subroutine WriteIDs(idfile)
C
      implicit none
      integer*4 i,ii,nn,j,lastchar
      character*(*) idfile
C
      include 'spim1.inc'
      include 'soun.inc'
C
C Write lines to file for later reference.
      write(soun,'(3a)') 'Writing line id file "',
     .          idfile(1:lastchar(idfile)),'" for all rows.'
      nn=0
      open(1,file=idfile,status='unknown',iostat=i)
      if (i.ne.0) then
        write(soun,'(2a)') 'Could not open file : ',idfile
        return
      endif
C
CIm row     pixel    wavel. intensity   wt  resid.  WRMS  ord
C--1234----+----1----+----1----+----112345----+---12345678---
C 1 999 99000.528 99000.391 1234000.1 99.0 -99.000 -99.000 12
C
      do ii=1,2
        do j=1,maxr
          if (nl(j,ii).gt.0) then
            do i=1,nl(j,ii)
              write(1,'(i2,i4,2f10.3,f10.1,f5.1,f8.3,f8.3,i3)',err=9)
     .          ii,j,
     .          min(99999.,max(-9.,lc(i,j,ii))),
     .          min(99999.,max(-9.,lw(i,j,ii))),
     .          min(9999999.,max(-9.,ls(i,j,ii))),
     .          min(99.,max(-9.,lwgt(i,j,ii))),
     .          min(999.,max(-99.,lres(i,j,ii))),
     .          min(999.,max(-99.,lrms(j))),
     .          min(99,max(-9,lord(j)))
              nn=nn+1
            enddo
          endif
        enddo
      enddo
      write(soun,'(a,i5,a)') 'A total of',nn,' lines written out.'
      close(1)
      return
9     write(soun,'(2a)') 'Error writing file : ',idfile
      close(1)
      return
      end


C---------------------------------------------------------
C
      subroutine PrintIDs(j)
C
      implicit none
      integer*4 i,ii,nn,j
C
      include 'spim1.inc'
      include 'soun.inc'
C
      write(soun,'(a)')
     .  'Im row    pixel    wavel.  intensity  wt  resid.   WRMS  ord'
C
CIm row     pixel    wavel. intensity   wt  resid.  WRMS  ord
C--1234----+----1----+----1----+----112345----+---12345678---
C 1 999 99000.528 99000.391 1234000.1 99.0 -99.000 -99.000 12
C
      nn=0
      do ii=1,2
          if (nl(j,ii).gt.0) then
            do i=1,nl(j,ii)
              write(6,'(i2,i4,2f10.3,f10.1,f5.1,f8.3,f8.3,i3)',err=9) 
     .          ii,j,
     .          min(99999.,max(-9.,lc(i,j,ii))),
     .          min(99999.,max(-9.,lw(i,j,ii))),
     .          min(9999999.,max(-9.,ls(i,j,ii))),
     .          min(99.,max(-9.,lwgt(i,j,ii))),
     .          min(999.,max(-99.,lres(i,j,ii))),
     .          min(999.,max(-99.,lrms(j))),
     .          min(99,max(-9,lord(j)))
              nn=nn+1
            enddo
          endif
      enddo
      write(soun,'(a,i5,a)') 'A total of',nn,' lines.'
      return
9     write(soun,'(a)') 'Error printing lines.'
      return
      end


C---------------------------------------------------------
C
      subroutine ReadIDs(idfile)
C
      implicit none
      integer*4 i,j,ii,nn,i1
      character*(*) idfile
      real*8 r1,r2,r3,r4,r5,r6
C
      include 'spim1.inc'
      include 'soun.inc'
C
      nn=0
C Open file.
      open(1,file=idfile,status='old',iostat=i)
      if (i.ne.0) then
        write(soun,'(2a)') 'Error opening file: ',idfile
        return
      endif
C Clear line lists.
      do ii=1,2
        do j=1,maxr
          nl(j,ii)=0
        enddo
      enddo
C Clear fit order and rms for fit for each row.
      do j=1,maxr
        lord(j)=0
        lrms(j)=0.d0
      enddo
C
CIm row     pixel    wavel. intensity   wt  resid.  WRMS  ord
C--1234----+----1----+----1----+----112345----+---12345678---
C 1 999 99000.528 99000.391 1234000.1 99.0 -99.000 -99.000 12
C
1     continue
      read(1,*,end=5,err=9) ii,j,r1,r2,r3,r4,r5,r6,i1
      if (r1.lt.0.) goto 1
      nl(j,ii) = nl(j,ii) + 1
      lc(nl(j,ii),j,ii)  = r1
      lw(nl(j,ii),j,ii)  = r2
      ls(nl(j,ii),j,ii)  = r3
      lwgt(nl(j,ii),j,ii)= r4
      lres(nl(j,ii),j,ii)= r5
      if (i1.ne.0) then
        lrms(j)            = r6
        lord(j)            = i1
      endif
      nn=nn+1
      goto 1
5     continue
      close(1)
      write(soun,'(a,i5,a)') 'A total of',nn,' lines read in.'
C Sort all lines.
      do ii=1,2
        do j=1,maxr
          call SortLines(j,ii)
        enddo
      enddo
      return
9     write(soun,'(2a)') 'Error reading file: ',idfile
      close(1)
      return
      end


C---------------------------------------------------------
C Find the nearest line id to the value "cx" that is within "slop" pixels.
C Only checks lines in primary image. Returns imin=0 if nothing found.
      subroutine NearID(cx,row,ino,imin,slop)
      implicit none
      integer*4 row,ino,imin,i
      real*4 cx,r,rmin,slop
      include 'spim1.inc'
      if (nl(row,ino).lt.1) then
        imin=0
        return
      endif
      imin=1
      rmin=abs(cx-sngl(lc(imin,row,ino)))
      do i=2,nl(row,ino)
        r=abs(cx-sngl(lc(i,row,ino)))
        if (r.lt.rmin) then
          rmin=r
          imin=i
        endif
      enddo
      if (rmin.gt.slop) imin=0
      return
      end


C---------------------------------------------------------
      subroutine MarkIDs(row,ino,sc,ec,sr,er,a,x,CI)
      implicit none
      integer*4 row,ino,sc,ec,sr,er,CI
      real*4 a(sc:ec,sr:er,1:2),x(sc:ec,sr:er)
      real*4 xmin,xmax,ymin,ymax,ch,hi,xx,uu(2),vv(2),uch
      integer*4 nc,i,ic,neari,ii
      character c11*11
      include 'spim1.inc'
      if (.not.pgd) return 
      if (nl(row,ino).eq.0) return
      nc = 1+ec-sc
      call PGQWIN(xmin,xmax,ymin,ymax)
      call PGQCH(ch)
C Color index of 1 indicates a PostScript file.
      if (CI.eq.1) then
        uch = 0.63
      else
        uch = 1.00
      endif
      call PGSCH(uch)
      call PGSCI(CI)
      if (.not.vt100) call PGBBUF
      DO i=1,nl(row,ino)
        xx=sngl(lc(i,row,ino))
        if ((xx.gt.xmin).and.(xx.lt.xmax)) then
          ic = neari(xx,nc,x(sc,row)) + sc - 1
          hi = ymin
          do ii=max(sc,ic-4),min(ec,ic+4)
            if (a(ii,row,ino).gt.hi) hi=a(ii,row,ino)
          enddo
          hi = hi+((ymax-ymin)*0.07)
          if (CI.eq.1) then
            hi = min(hi,ymax)
          else
            hi = min(hi,ymin+(0.965*(ymax-ymin)))
          endif
          uu(1)=xx
          uu(2)=xx
          vv(1)=hi-(abs(ymax-ymin)*0.00)
          vv(2)=hi-(abs(ymax-ymin)*0.04)
          call PGLINE(2,uu,vv)
          if (lw(i,row,ino).gt.0.) then
            write(c11,'(f9.3,2x)') 
     .              min(99999.,max(-9999.,lw(i,row,ino)))
            call PGPTXT(xx+(abs(xmax-xmin)*0.004),hi,90.,0.,c11)
          endif
        endif
      ENDDO
      if (.not.vt100) call PGEBUF
      call PGSCH(ch)
      call PGSCI(1)
      return
      end


C----------------------------------------------------------------------
C
      subroutine AddLine(row,ino,pix,wave,strength)
C
      implicit none
      integer*4 row,ino
      real*8 pix,wave,strength,r8
C
      include 'spim1.inc'
      include 'soun.inc'
C
      integer*4 nn,neari_bs8
C
C Change in the line id list.
      newli=.true.
C See if there is a better value.
      r8 = thar(neari_bs8(wave,nthar,thar))
      if (abs(r8-wave).gt.0.0008) then
        if (abs(r8-wave).lt.0.1) then
          write(soun,'(f10.4,a,f10.4)') wave,' --->',r8
          wave=r8
        else
          write(soun,'(a,f10.4,a)') 
     .  ' WARNING:',wave,' does not match a table line.'
        endif 
      endif 
C Add line.
      nl(row,ino) = nl(row,ino) + 1
      nn = nl(row,ino)
      lc(nn,row,ino) = pix
      lw(nn,row,ino) = wave
      ls(nn,row,ino) = strength
      lwgt(nn,row,ino) = 0.d0
      lres(nn,row,ino) = 0.d0
      if (ino.eq.2) ls(nn,row,ino) = strength * ssf
C Sort lines.
      call SortLines(row,ino)
      return
      end

C----------------------------------------------------------------------
C Eliminate a line id from the list.
      subroutine DeleteLine(row,ino,line_num)
      implicit none
      integer*4 row,ino,line_num,i,n
      include 'spim1.inc'
C Change in the line id list.
      newli=.true.
C Load into temporary array, skipping the line in question.
      n=0
      do i=1,nl(row,ino)
        if (i.ne.line_num) then
          n=n+1
          x8(n) =lc(i,row,ino)
          y8(n) =lw(i,row,ino)
          w8(n) =ls(i,row,ino)
          x8b(n)=lwgt(i,row,ino)
          y8b(n)=lres(i,row,ino)
        endif 
      enddo
      nl(row,ino)=n
      do i=1,nl(row,ino)
        lc(i,row,ino)  =x8(i)
        lw(i,row,ino)  =y8(i)
        ls(i,row,ino)  =w8(i)
        lwgt(i,row,ino)=x8b(i)
        lres(i,row,ino)=y8b(i)
      enddo
      return
      end

C----------------------------------------------------------------------
C Sort line id list.
      subroutine SortLines(row,ino)
      implicit none
      integer*4 row,ino,m
      parameter(m=900)
      real*8 lcs(m),lws(m),lss(m),lwgts(m),lress(m)
      include 'spim1.inc'
      integer*4 key(900),nn,i
      nn=nl(row,ino)
      if (nn.lt.2) return
      do i=1,nn
        key(i)=i
        arr(i)=sngl(lc(i,row,ino))
        lcs(i)  = lc(i,row,ino)
        lws(i)  = lw(i,row,ino)
        lss(i)  = ls(i,row,ino)
        lwgts(i)= lwgt(i,row,ino)
        lress(i)= lres(i,row,ino)
      enddo
      call qcksrtkey(nn,arr,key)
      do i=1,nn
        lc(i,row,ino)  = lcs(key(i))
        lw(i,row,ino)  = lws(key(i))
        ls(i,row,ino)  = lss(key(i))
        lwgt(i,row,ino)= lwgts(key(i))
        lres(i,row,ino)= lress(key(i))
      enddo
      return
      end

C----------------------------------------------------------------------
C Load echelle-order-defining polynomial coeffecients.
C "neo" is the number of echelle orders == number of rows in spectrum image.
C
      subroutine Load_co(neo,header)
      implicit none
      integer*4 neo
      character*(*) header
      include 'spim1.inc'
      integer*4 i,k
      character card*80,chead*80,c7*7
C Zero coeffecient arrays.
      do i=1,neo
        do k=1,mpp
          co(k,i)=0.d0
        enddo
      enddo
C Load arrays from header cards.    
      do i=1,neo
        write(c7,'(a,i1,a,i2.2)') 'CO_',0,'_',i
        card = chead(c7,header)
        read(card,'(4(1pe17.9))',err=9) co(1,i),co(2,i),co(3,i),co(4,i)
        write(c7,'(a,i1,a,i2.2)') 'CO_',4,'_',i
        card = chead(c7,header)
        if (card.eq.' ') goto 5
        read(card,'(4(1pe17.9))',err=9) co(5,i),co(6,i),co(7,i),co(8,i)
        write(c7,'(a,i1,a,i2.2)') 'CO_',8,'_',i
        card = chead(c7,header)
        if (card.eq.' ') goto 5
        read(card,'(4(1pe17.9))',err=9) 
     .  co(9,i),co(10,i),co(11,i),co(12,i)
5       continue
      enddo
      return
9     continue
      print *,
     .  'Error reading echelle-order-defining polynomial coeffecients.'
      return
      end

C----------------------------------------------------------------------
C Identify unmarked lines based on the ThAr table of lines.
C Use IDs from other rows to create fit.
C "soft" is 0 or 1, "1" will soften limits on ID-ing lines.
C
      subroutine IdentifyLinesUsingRows(sc,ec,sr,er,a,x,
     .                                  row,ino,soft,ok)
      implicit none
      integer*4 sc,ec,sr,er,row,ino,soft
      real*4 a(sc:ec,sr:er,1:2),x(sc:ec,sr:er)
      logical ok
C
      include 'spim1.inc'
      include 'verbose.inc'
C
      integer*4 i,np,ord,k,k1,k2,neari_bs8,i1,i2,neari,nn,uord
      real*8 high,wrms,w1,w2,disp,pix,wave,polyval,r82
      real*4 tol,tolpass1,cx,r1,r3,r4,back,offset,fract_limit

C Set tolerance in pixels for first pass.
      tolpass1 = 10.0
C Set tolerance in pixels for final pass.
      tol      =  3.0
C Fraction of lines agreeing to apply an offset.
      fract_limit = 0.61
C Soften.
      if (soft.gt.0) then
        fract_limit= 0.71     ! harder to apply (possibly bad) offset.
        tolpass1   = 10.0     ! leave the same.
        tol        = 10.0     ! allow a lot more lines.
      endif
      if (verbose) print *,'soft,tolpass1,tol,fract_limit=',
     .                      soft,tolpass1,tol,fract_limit
C Wavelength range.
      uord=-1
C Initialize row polynomial coeffecients.
      call QuickFitRows(sc,ec,sr,er,row,dfloat(sc),uord,1,w1,disp,ok)
      if (.not.ok) then
        if (verbose) print *,'ERROR-- Problem fitting rows... abort.'
        return
      endif
      uord=-1
      call QuickFitRows(sc,ec,sr,er,row,dfloat(ec),uord,0,w2,disp,ok)
      if (.not.ok) then
        if (verbose) print *,'ERROR-- Problem fitting rows... abort.'
        return
      endif
C Load array.
      np=0
      do i=sc,ec,20
        np=np+1
        pix=dfloat(i)
        uord=-1
        call QuickFitRows(sc,ec,sr,er,row,pix,uord,0,wave,disp,ok)
        if (.not.ok) then
          if (verbose) print *,'ERROR-- Problem fitting rows... abort.'
          return
        endif
        x8b(np)=wave
        y8b(np)=pix
        w8b(np)=1.d0
      enddo
C Fit function: pix = func(wave) .
      ord=9
2     continue
      call poly_fit_glls(np,x8b,y8b,w8b,ord,coef,ok)
      if (.not.ok) then
        print *,'ERROR-- Problem with inverse fit... abort.'
        return
      endif
C Check residuals. 
      call poly_fit_glls_residuals(np,x8b,y8b,w8b,ord,coef,high,wrms)
      if (verbose) then
        print '(a,f9.3,a,f9.3,a,i3)',
     .        '     ILUR: Residuals in inverse fit:  high=',
     .        high,'  wrms=',wrms,'  ord=',ord
      endif
      if ((wrms.gt.0.5).or.(high.gt.2.0)) then
        if (ord.gt.4) then
          ord=ord-1
          goto 2
        endif
        if (verbose) then
          print *,
     .  '    ILUR: Residuals in inverse fit are too high... abort.'
        endif
        ok=.false.
        return
      endif
C Define range and direction for search.
      k1=neari_bs8(w1,nthar,thar)
      k2=neari_bs8(w2,nthar,thar)
      k1=max(1,min(nthar,k1-1))
      k2=max(1,min(nthar,k2+1))

C On first pass, just look at the residuals to the possible lines.
3     continue
      nn=0
C Go through the table.
      do k=k1,k2
        if ((thar(k).gt.w1).and.(thar(k).lt.w2)) then
C Where does this wavelength fall?
          pix= polyval(ord+1,coef,thar(k))
C Is pixel within this spectrum?
          cx = sngl(pix)
          if ((cx.gt.float(sc+3)).and.(cx.lt.float(ec-3))) then
C Find centroid and strength.
            call line_center(sc,ec,sr,er,a,x,row,ino,
     .                              cx-3.,cx+3.,r1,r3,r4)
C Successful centroid?
            IF (abs(r1+r3+r4).gt.1.e-10) THEN
C Find lowest point near line.
            i1= neari(r1-10.,1+ec-sc,x(sc,row)) + sc - 1
            i2= neari(r1+10.,1+ec-sc,x(sc,row)) + sc - 1
            back=a(i1,row,ino)
            do i=i1+1,i2
              if (a(i,row,ino).lt.back) back=a(i,row,ino)
            enddo
C Find nearest line already ID'ed.
            call NearID(r1,row,ino,i,2.)
C Centroid to far from initial guess.
            if (abs(cx-r1).gt.tolpass1) then
              continue
C Is this near another already identified line?
            elseif (i.ne.0) then
C Is it the same line?
              if (abs(lw(i,row,ino)-thar(k)).lt.0.02d0) then
C Already indentified.
                nn=nn+1
                arr(nn)=cx-r1
C Is there another line within 4 pixels?
              elseif ((abs(lw(i,row,ino)-thar(k))/disp).lt.4.d0) then
                continue
              endif
C Is centroid at a location which is too low to be a usable line?
            elseif ((r3-back).lt.weakline(ino)) then
              continue
C Is line saturated?
            elseif (r3.gt.satuline(ino)) then
              continue
C Is line too wide (i.e. perhaps a non-line)?
            elseif (r4.gt.6.) then
              continue
C Eureka!
            else
              nn=nn+1
              arr(nn)=cx-r1
            endif
            ENDIF
          endif
        endif
      enddo
C Have we found enough lines? (Including those which may have already been IDed)
      if (nn.lt.2) then
        if (tolpass1.gt.10.) then
          if (verbose) then
            print *,
     .  '   ILUR: FAILURE to identify lines using other rows!'
          endif
          ok=.false.
          return
        endif
C Increase tolerance for pass 1 and try again.
        tolpass1 = tolpass1 + 1.0
        if (verbose) then
          print *,
     .  '   ILUR: Increasing tolerance first pass, tolpass1=',tolpass1
        endif
        goto 3
      endif
C Find median offset.
      call find_median(arr,nn,offset)

C Are "fract_limit" of offsets within 2.0px of median offset?
      k=0
      do i=1,nn
        if (abs(arr(i)-offset).lt.2.0) k=k+1
      enddo
C Penalize small "nn".
      r1=float(k)/float(max(5,nn))
      if (r1.gt.fract_limit) then
        if ((abs(offset).gt.0.2).and.(verbose)) then
          print '(a,f8.3,a,f7.3)',
     .  ' Applying offset of',offset,'px.  Fract=',r1
        endif
      else
        if (verbose) print '(a,f7.3)',
     .  ' Not applying offset. Fraction=',r1
        offset=0.
      endif

C On second pass, we account for offset.
C Go through the table.
      do k=k1,k2
        if ((thar(k).gt.w1).and.(thar(k).lt.w2)) then
C Where does this wavelength fall?
          pix= polyval(ord+1,coef,thar(k))
C Is pixel within this spectrum?
          cx = sngl(pix)
          if ((cx.gt.float(sc+3)).and.(cx.lt.float(ec-3))) then
C Find centroid and strength.
            call line_center(sc,ec,sr,er,a,x,row,ino,
     .                           cx-3.,cx+3.,r1,r3,r4)
C Successful centroid?
            IF (abs(r1+r3+r4).gt.1.e-10) THEN
C Find lowest point near line.
            i1= neari(r1-10.,1+ec-sc,x(sc,row)) + sc - 1
            i2= neari(r1+10.,1+ec-sc,x(sc,row)) + sc - 1
            back=a(i1,row,ino)
            do i=i1+1,i2
              if (a(i,row,ino).lt.back) back=a(i,row,ino)
            enddo
C Find nearest line already ID'ed.
            call NearID(r1,row,ino,i,2.)
C See if centroid is close to initial guess.
            if (abs((cx-r1)-offset).gt.tol) then
              continue
              if (verbose) then
                print '(6x,a,2f10.3,3f7.2)',
     .             'Centroid too far from guess :',
     .                     cx,thar(k),cx-r1,(cx-r1)-offset,tol
              endif
C Is this near another already identified line?
            elseif (i.ne.0) then
              continue
C Is centroid at a location which is too low to be a usable line?
            elseif ((r3-back).lt.weakline(ino)) then
              if (verbose) then
                print '(8x,a,3f10.3)',
     .  '           Line too weak :',cx,thar(k),r3
              endif
C Is line saturated?
            elseif (r3.gt.satuline(ino)) then
              if (verbose) then
                print '(8x,a,3f10.3)',
     .  '          Line saturated :',cx,thar(k),r3
              endif
C Is line too wide (i.e. perhaps a non-line)?
            elseif (r4.gt.6.) then
              if (verbose) then
                print '(8x,a,3f10.3)',
     .  '           Line too wide :',cx,thar(k),r4
              endif
C Eureka!
            else
              if (verbose) then
                print '(8x,a,4f10.3)',
     .          '           Found new line :',
     .                cx,thar(k),cx-r1,(cx-r1)-offset
              endif
              r82 = thar(k)
              call AddLine(row,ino,dble(r1),r82,dble(r3))
              if (pgd) call mark_line(r1,3)
            endif
            ENDIF
          endif
        endif
      enddo

      return
      end


C----------------------------------------------------------------------
C Identify unmarked lines based on the ThAr table of lines.
C
      subroutine IdentifyLines(sc,ec,sr,er,a,x,row,ino,mdn_resid,ok)
C
      implicit none
      integer*4 sc,ec,sr,er,row,ino
      real*4 a(sc:ec,sr:er,1:2),x(sc:ec,sr:er)
      real*4 mdn_resid
      logical ok
C
      include 'spim1.inc'
      include 'verbose.inc'
C
      real*8 pix,disp,lwmin,lwmax,sum,sw,ew
      real*4 tol,maxtol,mintol,tolinc
      parameter(maxtol=2.51,mintol=1.0,tolinc=0.5)
      integer*4 ord,i,num,ii

C Start with low tolerance.
      tol=mintol
1     continue
C Check if there are enough lines.
      if ((nl(row,1).gt.1).or.(nl(row,2).gt.1)) then
C Find wavelength limits of lines and weighted center.
        lwmin=+1.d+30
        lwmax=-1.d+30
        sum  = 0.d0
        num  = 0
        do ii=1,2
        do i=1,nl(row,ii)
          if ((lc(i,row,ii).gt.0.).and.(lw(i,row,ii).gt.0.)
     .                            .and.(ls(i,row,ii).gt.0.)) then
            if (lw(i,row,ii).lt.lwmin) lwmin=lw(i,row,ii)
            if (lw(i,row,ii).gt.lwmax) lwmax=lw(i,row,ii)
            sum=sum+lw(i,row,ii)
            num=num+1
          endif
        enddo
        enddo
        sum=sum/dfloat(max(1,num))
C Search center to maximum wavelength.
        call FindLines(sc,ec,sr,er,a,x,row,ino,sum,lwmax,tol,disp,ok)
        if (.not.ok) goto 900
C Search center to minimum wavelength.
        call FindLines(sc,ec,sr,er,a,x,row,ino,sum,lwmin,tol,disp,ok)
        if (.not.ok) goto 900
C Find starting wavelength and ending wavelength.
        pix=dfloat(sc)
        sw = -1.d0
        ord=-1
        call ForceVerbose(0)
        call QuickFit(sc,ec,row,pix,sw,disp,ord,ok,mdn_resid)
        pix=dfloat(ec)
        ew = -1.d0
        ord=-1
        call QuickFit(sc,ec,row,pix,ew,disp,ord,ok,mdn_resid)
        call ForceVerbose(2)
        sw=max(lwmin-100.,min(lwmin-10., sw-10.d0 ))
        ew=max(lwmax+10.,min(lwmax+100., ew+10.d0 ))
C Search maximum wavelength to right edge.
        call FindLines(sc,ec,sr,er,a,x,row,ino,lwmax,ew,tol,disp,ok)
        if (.not.ok) goto 900
C Search minimum wavelength to left edge.
        call FindLines(sc,ec,sr,er,a,x,row,ino,lwmin,sw,tol,disp,ok)
        if (.not.ok) goto 900
      else
        if (verbose) print *,'Not enough known lines.'
        return
      endif
C Repeat with larger tolerance.
      tol=tol+tolinc
      if (tol.lt.maxtol) goto 1 
      goto 999
900   continue
      if (verbose) print *,'Error in FindLines.'
999   continue
C Grab the median residual as a measure of quality.
      pix=dfloat(ec)
      ew=-1.d0
      ord=-1
      call QuickFit(sc,ec,row,pix,ew,disp,ord,ok,mdn_resid)
      return
      end


C----------------------------------------------------------------------
C Find unmarked lines based on the ThAr table of lines between wavelengths
C w1 and w2.  If w1<w2 search in the forward direction, if w1>w2 search in
C the reverse direction.  Give a tolerance (tol) in pixels.
C This routine refits after each line is discovered.
C It crashes if the polynomial fit fails.
C
      subroutine FindLines(sc,ec,sr,er,a,x,row,ino,w1,w2,tol,disp,ok)
C
      implicit none
      integer*4 sc,ec,sr,er,row,ino
      real*4 a(sc:ec,sr:er,1:2),x(sc:ec,sr:er),tol
      real*8 w1,w2,disp
      logical ok
C
      include 'spim1.inc'
      include 'verbose.inc'
      include 'soun.inc'
C
      real*8 pix,wave,r82,v1,v2
      real*4 cx,r1,r3,r4,back,mdn_resid
      integer*4 ord,i,k,k1,k2,k3,neari_bs8,i1,i2,neari
C
      real*8 MaxRange
C
C Maximum wavelength range.
      MaxRange = 100
      if (spim1_ESI) MaxRange = 800
C
C Horrible problems.
      if (abs(w1-w2).gt.MaxRange) then
        write(soun,'(a,f11.4,2i5,2f11.4)') 
     .  'ERROR-- PROBLEM in FindLines:',
     .           sngl(abs(w1-w2)),row,ino,sngl(w1),sngl(w2)
        ok=.false.
        return
      endif
C Default.
      ok=.true.
C Define range and direction for search.
      k1=neari_bs8(w1,nthar,thar)
      k2=neari_bs8(w2,nthar,thar)
      if (w1.lt.w2) then
        k1=max(1,min(nthar,k1-1))
        k2=max(1,min(nthar,k2+1))
        k3=1
      else
        k1=max(1,min(nthar,k1+1))
        k2=max(1,min(nthar,k2-1))
        k3=-1
      endif
      v1=min(w1,w2)
      v2=max(w1,w2)

C Go through the table.
      DO k=k1,k2,k3
      IF ((thar(k).gt.v1).and.(thar(k).lt.v2)) THEN
C Solve for pixel as a function of wavelengths.
      pix =-1.d0
      wave= thar(k)
C This fit will set the coef(0:20) array in spim1.inc.
      ord = -1
      call ForceVerbose(0)
      call QuickFit(sc,ec,row,pix,wave,disp,ord,ok,mdn_resid)
      call ForceVerbose(2)
      if (mdn_resid.gt.0.5) then
        if (verbose) then
          write(soun,'(a,f10.3,a)') 
     .  ' Median residual is too high :',mdn_resid,'px.'
          write(soun,'(a)') ' Try cleaning out bad lines.'
        endif
        ok=.false.
        return
      endif
      if (.not.ok) then
        if (verbose) write(soun,'(a)') 'Error in wavelength scale fit.'
        return
      endif
C Is pixel within this spectrum?
      cx=sngl(pix)
      if ((cx.gt.float(sc+3)).and.(cx.lt.float(ec-3))) then
C Find centroid and strength.
        call line_center(sc,ec,sr,er,a,x,row,ino,cx-3.,cx+3.,r1,r3,r4)
C Successful centroid?
        IF (abs(r1+r3+r4).gt.1.e-10) THEN
C Find lowest point near line.
        i1= neari(r1-10.,1+ec-sc,x(sc,row)) + sc - 1
        i2= neari(r1+10.,1+ec-sc,x(sc,row)) + sc - 1
        back=a(i1,row,ino)
        do i=i1+1,i2
          if (a(i,row,ino).lt.back) back=a(i,row,ino)
        enddo
C Find nearest line already ID'ed.
        call NearID(r1,row,ino,i,3.)
        if (abs(cx-r1).gt.tol) then
          if (verbose) then
            write(soun,'(6x,a,3f10.3)')
     .          'Centroid too far from guess :',cx,thar(k),cx-r1
          endif
C Is this near another already identified line?
        elseif (i.ne.0) then
          continue
C Is centroid at a location which is too low to be a usable line?
        elseif ((r3-back).lt.weakline(ino)) then
          if (verbose) then
            write(soun,'(8x,a,3f10.3)')
     .  '            Line too weak :',cx,thar(k),r3
          endif
C Line saturated?
        elseif (r3.gt.satuline(ino)) then
          if (verbose) then
            write(soun,'(8x,a,3f10.3)')
     .  '           Line saturated :',cx,thar(k),r3
          endif
C Line too wide (i.e. perhaps not a real line)?
        elseif (r4.gt.6.) then
          if (verbose) then
            write(soun,'(8x,a,3f10.3)')
     .  '            Line too wide :',cx,thar(k),r4
          endif
C Eureka!
        else
          if (verbose) then
            write(soun,'(8x,a,3f10.3)')
     .  '           Found new line :',cx,thar(k),cx-r1
          endif
          r82 = thar(k)
          call AddLine(row,ino,dble(r1),r82,dble(r3))
          if (pgd) call mark_line(r1,3)
        endif
        ENDIF
      endif
      ENDIF
      ENDDO

      return
      end


C----------------------------------------------------------------------
      subroutine MarkKnownLines(sc,ec,row,ino) 
      implicit none
      integer*4 sc,ec,row,ino
      real*4 r,mdn_resid
      real*8 pix,wave,disp,polyval
      integer*4 ord,i
      logical ok
      include 'spim1.inc'
C Solve for pixel as a function of wavelengths.
      pix =-1.d0
      wave= 1.d0
C This fit will set the coef(0:20) array in spim1.inc.
      ord=-1
      call ForceVerbose(0)
      call QuickFit(sc,ec,row,pix,wave,disp,ord,ok,mdn_resid)
      call ForceVerbose(2)
      if (ord.lt.2) then
        print *,'Not enough lines to do mark.'
        return
      endif
      if (.not.ok) then
        print *,'Error fitting polynomial.'
        return
      endif
      do i=1,nthar
        r = sngl(polyval(ord+1,coef,thar(i)))
        if (pgd) call mark_line(r,4)
      enddo
      return
      end


C----------------------------------------------------------------------
C Does a quick fit and returns a guess at the wavelength (wave) of a given
C pixel value (pix) or vice versa. Set the value you wish guessed to -1.0 
C initially.  This will determine if the polynomial is a function of pixel or
C wavelength.  If it is solving pix=func(wave), it first finds wave=func(pix)
C and then finds the inverse polynomial based on that.
C If uord > 1 then it forces the order to be uord, otherwise an order is
C choosen based on the number of points and returned in the uord variable.
C
      subroutine QuickFit(sc,ec,row,pix,wave,disp,uord,ok,mdn_resid)
C
      implicit none
      integer*4 sc,ec,row,uord
      real*8 pix,wave,polyval,disp,r8,high,wrms,thres
      integer*4 i,np,ord,ii,nrej,nn
      logical ok,iterate
C
      include 'spim1.inc'
      include 'verbose.inc'
      include 'soun.inc'
C
      real*4 xx(900),yy(900),mdn_resid,rr,valread
      character c1*1,c80*80

C Restart.
77    continue
C Load array for wave=func(pix) fit.
      np=0
      do ii=1,2
        if (nl(row,ii).gt.0) then
          do i=1,nl(row,ii)
            if ((lw(i,row,ii).gt.0.).and.(lc(i,row,ii).gt.0.)
     .                              .and.(ls(i,row,ii).gt.0.)) then
              np=np+1
              x8(np) = lc(i,row,ii)
              y8(np) = lw(i,row,ii)
              w8(np) = 1.d0
            endif
          enddo
        endif
      enddo
C Initialize number of pixels rejected. 
      nrej=0
11    continue
C (Re)choose order.
      if (np-nrej.lt.2) then
        write(soun,'(a)') 
     .  'WARNING: Quickfit: No fit possible, need more good lines.'
        ok=.false.
        return
      elseif (np-nrej.lt.4) then
        ord=1
      elseif (np-nrej.lt.10) then
        ord=2
      elseif (np-nrej.lt.30) then
        ord=3
      else
        ord=4
      endif
C ESI override.
      if (spim1_ESI) then
C (Re)choose order.
        if (np-nrej.lt.2) then
          write(soun,'(a)')
     .    'WARNING: Quickfit: No fit possible, need more good lines.'
          ok=.false.
          return
        elseif (np-nrej.lt.3) then
          ord=1
        elseif (np-nrej.lt.6) then
          ord=2
        elseif (np-nrej.lt.8) then
          ord=3
        elseif (np-nrej.lt.10) then
          ord=4
        elseif (np-nrej.lt.16) then
          ord=5
        elseif (np-nrej.lt.25) then
          ord=6
        else
          ord=7
        endif
      endif
C User override.
      if (uord.gt.0) ord=uord
C Check number of points.
      if (np-nrej.lt.ord+1) then
        write(soun,'(a)') 
     .  'WARNING: Quickfit: No fit possible, need more good lines.'
        ok=.false.
        return
      endif
C Fit polynomial.
      call poly_fit_glls(np,x8,y8,w8,ord,coef,ok)
      if (.not.ok) return
C Residuals.
      call poly_fit_glls_residuals(np,x8,y8,w8,ord,coef,high,wrms)
C Find dispersion.
      disp = ( polyval(ord+1,coef,x8(np)) - 
     .         polyval(ord+1,coef,x8(1)) ) / ( x8(np) - x8(1) )
C Find median residual.
      nn=0
      do i=1,np
        nn=nn+1
        arr(nn) = sngl(abs(y8(i)-polyval(ord+1,coef,x8(i))))
      enddo
      call find_median(arr,nn,mdn_resid)
C Check residuals, throw out bad points, and iterate.
      iterate=.false.
      do i=1,np
        if (w8(i).gt.0.) then
          r8 = polyval(ord+1,coef,x8(i)) - y8(i)
          rr = sngl(abs(r8))
          if ((rr.gt.(7.*mdn_resid)).or.(rr.gt.1.0)) then
            if (verbose) write(soun,'(a,2f10.3,f10.5,2f9.3)')
     .      ' Rejecting :',x8(i),y8(i),rr,r8/disp,rr/mdn_resid
            w8(i)=0.d0
            nrej=nrej+1
            iterate=.true.
          endif
        endif
      enddo
      if (nrej.gt.np-3) then
        if (verbose) write(soun,'(a,i4,a,i4)')
     .                     ' Rejected too many points :',nrej,' of',np
        ok=.false.
        return
      endif
      if (iterate) goto 11
C Convert mdn_resid to pixels.
      mdn_resid = mdn_resid / sngl(disp)
C Show all residuals and the plot the result.
      if (verbose) then
        do i=0,ord
          write(soun,'(a,i2,a,1pe15.7)') 'coef(',i,')=',coef(i)
        enddo
        write(soun,'(a,f10.5)') ' Dispersion (A/px)=',disp
        do i=1,np
          rr= sngl(y8(i)-polyval(ord+1,coef,x8(i)))
          write(soun,'(i4,f10.3,f11.4,f9.4,f8.3,f5.1)')
     .            i,sngl(x8(i)),sngl(y8(i)),rr,rr/sngl(disp),w8(i)
        enddo
        write(soun,'(a,f8.3,a,f8.3,a,f8.3)') ' Residuals (px) : Mdn=',
     .      mdn_resid,'  WRMS=',wrms/disp,'  High=',high/disp
        do i=1,np
          xx(i) = sngl(x8(i))
          yy(i) = sngl((y8(i)-polyval(ord+1,coef,x8(i)))/disp)
        enddo
        xx(np+1)=0.
        xx(np+2)=4100.
        yy(np+1)=0.
        yy(np+2)=0.
        IF (pgd) THEN
        call PG_SimpPlot(np+2,xx,yy,1,'pgdisp')
        call PGQCH(rr)
        call PGSCH(2.0)
        call PGSCI(3)
        call PG_SimpPlot(np,xx,yy,3,'go')
        call PGSCI(1)
        call PGSCH(rr)
        call PGSLS(4)
        call PG_SimpPlot(2,xx(np+1),yy(np+1),0,'go')
        call PGSLS(1)
        print *,'Hit any key in plot window to cont',
     .          'inue (hit C or # [th=0.#] for CleanFit).'
        xx(1)=0.
        yy(1)=0.
        call GetKey(xx(1),yy(1),c1)
        call PG_SimpPlot(np,xx,yy,3,'stop')
        if (c1.eq.'C') then
          print '(a,$)',
     .  ' Residual threshold in pixels (0.5,-1=no saturated) : '
          read(5,'(a)') c80
          if (c80.eq.' ') then
            thres = 0.5d0
          else
            thres = dble(valread(c80))
          endif
          print '(a,f7.3,a)',' Threshold =',thres,'px.'
          call CleanFit(sc,ec,row,thres,.false.,ok)
          if (.not.ok) print *,' Error. Failure in cleaning fit.'
          goto 77
        elseif ((ichar(c1).ge.49).and.(ichar(c1).le.57)) then
          thres = float(ichar(c1)-48)/10.
          print '(a,f7.3,a)',' Threshold =',thres,'px.'
          call CleanFit(sc,ec,row,thres,.false.,ok)
          if (.not.ok) print *,' Error. Failure in cleaning fit.'
          goto 77
        endif
        ENDIF
      endif
C Warn about high median residual (in pixels).
      if ((mdn_resid.gt.0.5).and.verbose) then
        print '(a,f6.2,a)',
     .  '   WARNING: Median residual in fit is',
     .                     min(999.,mdn_resid),'px.'
      endif
C Solve for wavelength.
      if (abs(wave+1.d0).lt.1.e-10) then
        wave = polyval(ord+1,coef,pix)
        goto 800
      endif
C Invert the polynomial by fitting pixels as a function of wavelength.
C Load array.
      np=0
      do i=sc,ec,10
        np=np+1
        x8(np)=polyval(ord+1,coef,dfloat(i))
        y8(np)=dfloat(i)
        w8(np)=1.d0
      enddo
      ord=9
C Fit pix=func(wave).
88    continue
      call poly_fit_glls(np,x8,y8,w8,ord,coef,ok)
      if ((ord.gt.6).and.(.not.ok)) then
        ord=ord-1
        goto 88
      endif
      if (.not.ok) then
        print *,' Inverse fit failed.'
        return
      endif
C Check residuals.
      call poly_fit_glls_residuals(np,x8,y8,w8,ord,coef,high,wrms)
      if ((wrms.gt.0.4).or.(high.gt.2.0)) then
        if (ord.gt.4) then
          ord=ord-1
          goto 88
        endif
        if (verbose) then
          print '(a,2f10.3)',
     .  ' Residuals in inverse fit too high:',high,wrms,ord
        endif
        ok=.false.
        return
      endif
C Solve for pixel.
      pix = polyval(ord+1,coef,wave)
C Send back order used.
800   continue
      uord = ord
      return
      end


C----------------------------------------------------------------------
C Estimate a wavelength for a given pixel position in a given row using
C Bob Goodrich's ESI guessing polynomial fits.
C
      subroutine FindWave_ESI(row,pix,wave,disp)
C
      implicit none
      integer*4 row   ! Row number: TrueOrder = 16 - row (input).
      real*8 pix      ! Pixel centroid (column position) (input).
      real*8 wave     ! Guess at wavelength for this column position (output).
      real*8 disp     ! Dispersion in Angstroms per pixel near line (output).
C
      integer*4 teo   ! true echelle order
C
      real*8 coef(4,15),p,wave2
      data coef /
     .                               0., 0., 0., 0.,     ! True echelle order 1
     .                               0., 0., 0., 0.,     ! 2
     .                               0., 0., 0., 0.,     ! 3
     .                               0., 0., 0., 0.,     ! 4
     .                               0., 0., 0., 0.,     ! 5
     .   10156.00, 0.39,      -4.25e6,      0.,          ! 6
     .    8007.59, 0.334496,  -3.6815e-6,  -2.58e-10,    ! 7
     .    7621.60, 0.29266,   -3.203e-6,   -2.77e-10,    ! 8
     .    6776.99, 0.259847,  -2.826e-6,   -1.90e-10,    ! 9
     .    6101.46, 0.233675,  -2.593e-6,   -1.105e-10,   ! 10
     .    5549.09, 0.212052,  -2.365e-6,   -1.23e-10,    ! 11
     .    5088.55, 0.194456,  -2.140e-6,    4.00e-11,    ! 12
     .    4699.50, 0.179043,  -1.912e-6,   -8.44e-11,    ! 13
     .    4366.24, 0.165128,  -2.0205e-6,   5.71e-10,    ! 14
     .    4077.46, 0.154482,  -1.140e-6,   -3.106e-10  / ! 15
C
C True Echelle order.
      teo = 16 - row
C
      if ((teo.lt.6).or.(teo.gt.15)) then
        print *,
     .   'ERROR: FindWave_ESI: Bad true echelle order:',teo,
     .   '   row=',row
        wave=0.
        disp=1.
        return
      endif
C
C Wavelength solution.
      p    = pix - 2048.
      wave = coef(1,teo) + ( coef(2,teo) * p )
     .                   + ( coef(3,teo) * p * p )
     .                   + ( coef(4,teo) * p * p * p )
C Dispersion solution.
      p    = p + 1.
      wave2= coef(1,teo) + ( coef(2,teo) * p )
     .                   + ( coef(3,teo) * p * p )
     .                   + ( coef(4,teo) * p * p * p )
      disp = wave2 - wave
C
      return
      end


C----------------------------------------------------------------------
C Try to estimate a wavelength for a given pixel position in a given row.
C This routine uses QuickFitRows if the ordinary fit fails.
C
      subroutine FindWave(sc,ec,sr,er,row,pix,wave,disp)
C
      implicit none
      integer*4 sc,ec,sr,er,row
      real*8 pix,wave,disp
      real*4 mdn_resid
      integer*4 ord,nltr,nrwd,j,uord
      logical ok,qfr
C
      include 'verbose.inc'
      include 'spim1.inc'
C
C How many lines we have in this row?
      nltr = max(nl(row,1),nl(row,2))
C How many rows are well determined?
      nrwd = 0
      do j=sr,er
        if ((nl(j,1).gt.8).or.(nl(j,2).gt.8)) nrwd=nrwd+1
      enddo
C If we have lots of rows but not many lines in this row,
C try QuickFitRows first.
      if (nltr.lt.2) then
        qfr=.true.
      elseif ((nltr.lt.3).and.(nrwd.gt.2)) then
        qfr=.true.
      elseif ((nltr.lt.5).and.(nrwd.gt.5)) then
        qfr=.true.
      else
        qfr=.false.
      endif
      if (qfr) then
        uord=-1
        call QuickFitRows(sc,ec,sr,er,row,pix,uord,1,wave,disp,ok)
        if (.not.ok) then
          print *,'Error determining wavelength using ot',
     .                         'her rows... try just this row.'
          wave= -1.d0
          ord = -1 
          call ForceVerbose(0)
          call QuickFit(sc,ec,row,pix,wave,disp,ord,ok,mdn_resid)
          call ForceVerbose(2)
          print *,'Error in wavelength scale fit.'
          wave = -1.d0
        endif
      else
        wave= -1.d0
        ord = -1
        call ForceVerbose(0)
        call QuickFit(sc,ec,row,pix,wave,disp,ord,ok,mdn_resid)
        call ForceVerbose(2)
        if (.not.ok) then
          print *,
     .  'Error in wavelength scale fit--- try using other rows.'
          uord=-1
          call QuickFitRows(sc,ec,sr,er,row,pix,uord,1,wave,disp,ok)
          if (.not.ok) then
            print *,'Error determining wavelength using other rows.'
            wave=-1.d0
          endif
        endif
      endif
      return
      end
         

C----------------------------------------------------------------------
C Use the solutions for other rows to estimate wavelengths for a row with 
C (perhaps) no lines id'ed.  Input: sr,er,row,pix,uord   Output: wave,disp,ok .
C This routine fits wavelengths versus the order number (i.e. extracted image
C row number).
C If uord > 1 then it forces the order to be uord, otherwise an order is
C choosen based on the number of points and returned in the uord variable.
C If init=1, then reload the mcoef(0:20,maxr) and mord(maxr) arrays.
C If init=0, do not reload.
C
      subroutine QuickFitRows(sc,ec,sr,er,row,pix,uord,
     .  init,wave,disp,ok)
C
      implicit none
      integer*4 sc,ec,sr,er,row,uord,init
      real*8 pix,wave,disp
      logical ok
C
      include 'spim1.inc'
      include 'verbose.inc'
      include 'soun.inc'
C
      integer*4 ord,np,i,j,k,bad,ihidiff
      real*8 polyval,avdisp,high,wrms,diff,hidiff
      real*4 mdn_resid
      logical GoodIdsRow
C
C Re-initialize and re-load multiple row coeffecient array.
C Fit polynomial for those rows with at least 9 lines.
      if (init.eq.1) then
        do j=1,maxr
          mres(j)=0.
          mord(j)=-1
          do k=0,mpp-1
            mcoef(k,j)=0.d0
          enddo
        enddo
        avdisp=0.d0
        np=0
        do j=sr,er
          if (GoodIdsRow(sc,ec,j)) then
            wave=-1.d0
            ord =-1
            call ForceVerbose(0)
            call QuickFit(sc,ec,j,pix,wave,disp,ord,ok,mdn_resid)
            call ForceVerbose(2)
            if (ok) then
              mord(j)= ord
              mres(j)= mdn_resid
              do k=0,ord
                mcoef(k,j)=coef(k)
              enddo
              np     = np + 1
              avdisp = avdisp + disp
            endif
          endif
        enddo
        if (np.lt.1) then
          if (verbose) write(soun,'(a)')
     .  'ERROR: QuickFitRows: No rows with at least 9 good lines.'
          ok=.false.
          return
        endif
        disp = avdisp / dfloat(np)
      endif
C
C Load arrays.  Evaluate polynomials in the "pix" column of image.
      np=0
      do j=sr,er
        if (mord(j).gt.0) then
          if (mres(j).lt.0.5) then
            np     = np + 1
            x8(np)= dfloat(j)
            y8(np)= polyval(mord(j)+1,mcoef(0,j),pix)
            w8(np)= 1.d0
          else
            if (verbose) then
              write(soun,'(a,i4,f10.3)')
     .    'WARNING: QuickFitRows: Median residual too high in row',
     .                 j,mres(j)
            endif
          endif
        endif
      enddo
C
C Choose order.
      if (np.lt.3) then
        if (verbose) then
          write(soun,'(a)')
     .  'NOTE: Need at least three rows with at least 9 lines for fit.'
        endif
        ok = .false.
        return
      elseif (np.lt.5) then
        ord=2
      elseif (np.lt.9) then
        ord=3
      elseif (np.lt.15) then
        ord=4
      elseif (np.lt.21) then
        ord=5
      else
        ord=6
      endif
C
C User order override.
      if (uord.gt.1) ord=uord
      if (np.lt.ord+1) then
        if (verbose)write(soun,'(a)')
     .  'NOTE: Need more rows with at least 9 good lines to fit.'
        ok = .false.
        return
      endif
C
C Fit polynomial.
      bad=0
3     continue
      call poly_fit_glls(np,x8,y8,w8,ord,coef,ok) 
      if (.not.ok) then
        if (verbose) write(soun,'(a)') 
     .  'ERROR: QuickFitRows: During row fit.'
        return
      endif
      call poly_fit_glls_residuals(np,x8,y8,w8,ord,coef,high,wrms)
C Check.
      if ((wrms/disp.gt.1.0).or.(high/disp.gt.3.0)) then
C Throw out high point.
        if (np-bad.gt.20) then
          hidiff = -1.
          ihidiff= 1
          do i=1,np
            if (w8(i).gt.0.) then
              diff = abs( y8(i) - polyval(ord+1,coef,x8(i)) )
              if (diff.gt.hidiff) then
                hidiff = diff
                ihidiff= i
              endif
            endif
          enddo
          if (verbose) then
            write(soun,'(a,f12.3,f12.3,i5,i5)')
     .  'NOTE: Throw out point: diff,x8,np,bad:',
     .  hidiff/disp,x8(ihidiff),np,bad
          endif
          w8(ihidiff)=0.d0
          bad=bad+1
          goto 3
        endif
        if (verbose) then
          write(soun,'(8x,a,f9.3,a,f9.3)')
     .          'WARNING: QuickFitRows: Residuals(pixels): HIGH=',
     .                       high/disp,'   WRMS=',wrms/disp
        endif
      endif
C Check.
      if (wrms.gt.0.5) then
        if (verbose) then
          write(soun,'(2a)') 
     .  'WARNING: QuickFitRows: Residuals too large-- fail',
     .  'ure of fit in QuickFitRows.'
        endif
        ok=.false.
        return
      endif
C Find wavelength at requested row.
      wave = polyval(ord+1,coef,dfloat(row))
C Return order used.
      uord = ord
      return
      end



C----------------------------------------------------------------------
C Fit wavelengths as a function of pixel, deleting bad lines.
C Set mark=.true. if you want to mark the line on the plot.
C "thres" is the threshold for rejection in pixels.

      subroutine CleanFit(sc,ec,row,thres,mark,ok)
C
      implicit none
      integer*4 sc,ec,row
      real*8 thres
      logical mark,ok
C
      include 'spim1.inc'
      include 'verbose.inc'
C
      real*8 polyval,disp,r8,high,wrms,worst
      real*4 mdn_resid,rr
      integer*4 i,np,ord,ii,ndel,line_no(900)
      integer*4 line_im(900),nn,nrej,iworst
      logical iterate

C Initialize number of deleted lines.
      ndel=0
C Delete saturated lines.
      if (thres.lt.0.) then
        if (verbose) print *,'  CF: Deleting saturated lines only.'
        do ii=1,2
          if (nl(row,ii).gt.0) then
            do i=1,nl(row,ii)
              if (ls(i,row,ii).gt.satuline(ii)) then
                if (verbose) then
                  print '(5x,a,2f10.3,f10.5,2f9.3)',
     .  'Deleting :',x8(i),y8(i),r8
                endif
                ndel=ndel+1
                if (mark.and.pgd) call mark_line(sngl(lc(i,row,ii)),2)
                call DeleteLine(row,ii,i)
              endif
            enddo
          endif
        enddo
        goto 8
      endif
C Delete deviant lines.
C (Re)Load array for wave=func(pix) fit.
11    continue
      np=0
      do ii=1,2
        if (nl(row,ii).gt.0) then
          do i=1,nl(row,ii)
            if ((lw(i,row,ii).gt.0.).and.(lc(i,row,ii).gt.0.)
     .                              .and.(ls(i,row,ii).gt.0.)) then
              np=np+1
              x8(np) = lc(i,row,ii)
              y8(np) = lw(i,row,ii)
              w8(np) = 1.d0
              line_no(np)=i
              line_im(np)=ii
            endif
          enddo
        endif
      enddo
C Initialize number of pixels rejected.
      nrej=0
22    continue
C (Re)Choose order.
      if (np-nrej.lt.2) then
        if (verbose) print *,'No fit possible, need more lines.'
        ok = .false.
        return
      elseif (np-nrej.lt.4) then
        ord=1
      elseif (np-nrej.lt.10) then
        ord=2
      elseif (np-nrej.lt.30) then
        ord=3
      else
        ord=4
      endif
C Fit polynomial.
      call poly_fit_glls(np,x8,y8,w8,ord,coef,ok)
      if (.not.ok) return
C Find dispersion.
      disp = ( polyval(ord+1,coef,x8(np)) - 
     .         polyval(ord+1,coef,x8(1)) ) / ( x8(np) - x8(1) )
C Residuals.
      if (verbose) then
        call poly_fit_glls_residuals(np,x8,y8,w8,ord,coef,high,wrms)
        do i=0,ord
          print *,'CF: coef(',i,')=',coef(i)
        enddo
        print '(a,f10.5)',' CF: Dispersion (A/px)=',disp
        do i=1,np
          rr= sngl(y8(i)-polyval(ord+1,coef,x8(i)))
          print '(i4,f10.3,f11.4,f9.4,f8.3,f5.1)',
     .            i,sngl(x8(i)),sngl(y8(i)),rr,rr/sngl(disp),w8(i)
        enddo
        print '(a,f8.3,a,f8.3,a,f8.3)',' CF: Residuals (px) : Mdn=',
     .      mdn_resid,'  WRMS=',wrms/disp,'  High=',high/disp
      endif
C Find median residual.
      nn=0
      do i=1,np
        r8 = polyval(ord+1,coef,x8(i))
        nn=nn+1
        arr(nn) = sngl(abs(y8(i)-r8))
      enddo
      call find_median(arr,nn,mdn_resid)
      if (verbose) print *,'CF: mdn_resid=',mdn_resid
C Check residuals, throw out bad points, and iterate.
      iterate=.false.
      do i=1,np
        if (w8(i).gt.0.) then
          r8 = polyval(ord+1,coef,x8(i)) - y8(i)
          rr = sngl(abs(r8))
          if ((rr.gt.(7.*mdn_resid)).or.(rr.gt.1.0)) then
            w8(i)=0.d0
            if (verbose) print *,'CF: Reject:',x8(i),rr
            nrej=nrej+1
            iterate=.true.
          endif
        endif
      enddo
      if (nrej.gt.np-3) then
        if (verbose) then
          print '(a,i4,a,i4)',' Rejected too many points :',
     .                   nrej,' of',np
        endif
        ok=.false.
        return
      endif
      if (iterate) goto 22

C Delete worst line which is over threshold.
      iterate=.false.
      worst =-1.d0
      iworst=1
      do i=1,np
        r8 = (polyval(ord+1,coef,x8(i))-y8(i))/disp
        if (abs(r8).gt.worst) then
          worst=abs(r8)
          iworst=i
        endif
      enddo
      if (worst.gt.thres) then 
        i=iworst       
        r8 = (polyval(ord+1,coef,x8(i))-y8(i))/disp
        if (verbose) print '(5x,a,2f10.3,f10.5)',
     .                     'CF: Deleting :',x8(i),y8(i),r8
        ndel=ndel+1
        iterate=.true.
        if (mark.and.pgd) then
          call mark_line(sngl(lc(line_no(i),row,line_im(i))),2)
        endif
        call DeleteLine(row,line_im(i),line_no(i))
      endif
      if (iterate) goto 11

8     continue
      if (verbose) print '(6x,a,i4,a)','Deleted',ndel,' line(s).'
      return
      end


C----------------------------------------------------------------------
C Decide whether a given row has enough suitable line ids for use in
C row-to-row interpolation and fitting.
C
      logical function GoodIdsRow(sc,ec,row)
      implicit none
      integer*4 sc,ec,row
      include 'spim1.inc'
      integer*4 nc,i,k,num
      real*8 p1,p2
      logical ok
C Enough lines in either image?
      ok = ((nl(row,1).gt.8).or.(nl(row,2).gt.8))
      if (.not.ok) goto 9
C Are there enough lines in first third of the row?
      nc = 1+ec-sc
      p2 = dfloat(sc+(nc/3))
      num=0
      do k=1,2
        do i=1,nl(row,k)
          if (lc(i,row,k).lt.p2) num=num+1
        enddo
      enddo
      ok = (num.gt.1)
      if (.not.ok) goto 9
C Are there enough lines in middle third of the row?
      p1 = dfloat(sc+(nc/3))
      p2 = dfloat(sc+(2*nc/3))
      num=0
      do k=1,2
        do i=1,nl(row,k)
          if ((lc(i,row,k).gt.p1).and.(lc(i,row,k).lt.p2)) num=num+1
        enddo
      enddo
      ok = (num.gt.1)
      if (.not.ok) goto 9
C Are there enough lines in last third of the row?
      p1 = dfloat(sc+(2*nc/3))
      num=0
      do k=1,2
        do i=1,nl(row,k)
          if (lc(i,row,k).gt.p1) num=num+1
        enddo
      enddo
      ok = (num.gt.1)
9     GoodIdsRow = ok
      return
      end

C----------------------------------------------------------------------
      subroutine mark_line(x,CI)
      implicit none
      integer*4 CI
      real*4 x,xmin,xmax,ymin,ymax,xx(2),yy(2)
      call PGQWIN(xmin,xmax,ymin,ymax)
      if ((x.lt.xmin).or.(x.gt.xmax)) return
      call PGSCI(CI)
      xx(1)=x
      xx(2)=x
      yy(1)=ymin
      yy(2)=ymax
      call PGLINE(2,xx,yy)
      call PGSCI(1)
      return
      end

C----------------------------------------------------------------------
      subroutine GetKey(cx,cy,key)
      implicit none
      real*4 cx,cy
      character*1 key
      include 'spim1.inc'
      integer*4 i
      if (nautokey.gt.0) then
        key= autokey(1)
        nautokey=nautokey-1
        if (nautokey.gt.0) then
          do i=1,nautokey
            autokey(i) = autokey(i+1)
          enddo
        endif
      else
        call PGCURS(cx,cy,key)
      endif
      return
      end


C----------------------------------------------------------------------
C
      subroutine spim0(a,sc,ec,sr,er,header,bin,nh,nv,row,
     .   file,ps,itm,pix,
     .   both,hist,minval,maxval,minpval,zeroline,nomin0,optimal,
     .   ysa,yea,xs,xe,xtick,nxtick,psfile,plotsign)
C
      implicit none
      integer*4 sc,ec,sr,er,i,n,j,bin,nh,nv,col,row,nxtick,plotsign
      real*4 a(sc:ec,sr:er),xtick
      character*(*) header
      character*(*) file
      character*(*) psfile
      logical ps,wave,itm,pix,both,hist,zeroline,nomin0,optimal
      real*4 xp(9000),x(9000),y(9000),arr(9000),median
      real*4 xmin,xmax,ymin,ymax,xt,yt,ysa(99),yea(99),low,high
      real*4 xhi,xlo,yhi,ylo,xs,xe,minval,maxval,minpval
      character*80 c80,chead,object,targname,wrd
      character c9*9
      real*8 r8,GetSpimWave
      integer*4 ii,lc,jj,usr,uer,inhead,neari,getpos
      real*4 p1,p2,p3
C
      include 'soun.inc'
C

C Defaults for optimal y range.
      p1 =0.01
      p2 =0.99
      p3 =0.08

C Object name.
      object = chead('OBJECT',header)

C If we have a wavelength scale and an xs= and xe= were used, we only want
C one window per page.
      c80 = chead('CTYPE1',header)
      if ( (getpos('WV_0_01',header).ne.-1).or.(c80(1:6).eq.'LAMBDA')
     .                      .or.(c80(1:6).eq.'MULTIS') ) then
        if ((xs.gt.-0.9).and.(xe.gt.-0.9)) then
          nh=1
          nv=1
        endif
      endif

C Open device.
      if (ps) then
        if (row.gt.0) then
          wrd = psfile(1:lc(psfile))//'/PS'
          call PGBEGIN(0,wrd,1,1)
        else
          wrd = psfile(1:lc(psfile))//'/PS'
          call PGBEGIN(0,wrd,plotsign*nh,plotsign*nv)
        endif
        write(soun,'(2a)') ' Output is in : ',psfile(1:lc(psfile))
      else
        if (row.gt.0) then
          call PGBEGIN(0,'/XDISP',1,1)
        else
          call PGBEGIN(0,'/XDISP',plotsign*nh,plotsign*nv)
        endif
      endif
      if (ps) then
        call PGASK(.false.)
      else
        call PGASK(.true.)
      endif
      call PGVSTAND
      call PGSLW(1)
      call PGSCF(1)
      if ((row.gt.0).or.(nh.eq.1).or.(nv.eq.1)) then
        call PGSCH(1.4)
      else
        call PGSCH(2.4)
      endif

C If -pix not specfied, see if data has wavelength scale, initialize function.
      if (pix) then
        wave=.false.
      else
        r8 = GetSpimWave(header,0.d0,0)
        wave = (r8.gt.-1.d-60)
      endif

C Do each row unless otherwise requested.
      if (row.gt.0) then
        usr=row
        uer=row
      else
        usr=sr
        uer=er
      endif

C Each row.
      do j=usr,uer

C Bin data.
        n=0  
        col=sc
        do while((col+bin-1).le.ec)
          xt=0.
          yt=0.
          do i=col,(col+bin-1)
            yt= yt + a(i,j)
            xt= xt + float(i)
          enddo
          n=n+1
          x(n)= xt / float(bin)
          xp(n)= x(n)
          y(n)= yt / float(bin)
          col = col+bin
        enddo
C Convert to wavelengths if possible.
        if (wave) then
          do i=1,n
            x(i) = sngl(GetSpimWave(header,dble(x(i)),j))
          enddo
        endif
C Set x and y scale.
        xlo=x(1)
        xhi=x(1)
        do i=2,n 
          if (x(i).lt.xlo) xlo=x(i)
          if (x(i).gt.xhi) xhi=x(i)
        enddo
        xmin = xlo - (xhi-xlo)*0.01
        xmax = xhi + (xhi-xlo)*0.01
C If xs and xe have been given, plot only segment given.
        if ((xs.gt.-0.9).and.(xe.gt.-0.9)) then
          if ((xs.lt.xmax).and.(xe.gt.xmin)) then
            xmin = max(xs,xmin)
            xmax = min(xe,xmax)
          else
            goto 750
          endif
        endif
C Find y extremes between x bounds.
        ylo=+1.e+30
        yhi=-1.e+30
        do i=1,n 
          if ((x(i).gt.xmin).and.(x(i).lt.xmax)) then
            if (y(i).lt.ylo) ylo=y(i)
            if (y(i).gt.yhi) yhi=y(i)
          endif
        enddo
C In case of failure.
        if ((yhi.lt.-1.e+29).or.(ylo.gt.+1.e+29)) then
          yhi=1. 
          ylo=0. 
        endif
C Set actual bounds.
        if (minpval.gt.-1.e+29) then
          ymax = yhi*1.03
          ymin = min(ymax-1.e-20,ymax*minpval)
        elseif (minval.gt.-1.e+29) then
          ymin = min(yhi-1.e-20,minval)
          ymax = yhi + (yhi-ymin)*0.03
        elseif (ylo.gt.-1.e-30) then
          if (nomin0) then
            ymin = ylo - (yhi-ylo)*0.03
            ymax = yhi + (yhi-ylo)*0.03
          else
            ymin = 0.
            ymax = yhi*1.03
          endif
        else
          ymin = ylo - (yhi-ylo)*0.03
          ymax = yhi + (yhi-ylo)*0.03
        endif
        if (maxval.gt.-1.e+29) ymax=max(ylo+1.e-20,maxval)
C Try optimal range.
        if (optimal) then
          do i=1,n
            arr(i) = y(i)
          enddo
          call find_median(arr,n,median)
          low = arr(nint(float(n)*p1))
          high= arr(nint(float(n)*p2))
          if (low.gt.0.) then
            ymin = 0.
          else
            ymin = low - ((median-low)*p3)
          endif
          ymax = high + (high*p3)
        endif
C Override by user.
        if (abs(ysa(j)-yea(j)).gt.1.e-20) then
          ymin = ysa(j)
          ymax = yea(j)
        endif
C Just in case.
        if (abs(ymax-ymin).lt.0.0001) then
          ymin=0.
          ymax=1.
        endif

C Set window.
        call PGPAGE
        call PGWINDOW(xmin,xmax,ymin,ymax)
        if (both) then
          if (itm) then
            call PGBOX('BINST',xtick,nxtick,'BICNST',0.,0)
          else
            call PGBOX('BCNST',xtick,nxtick,'BCNST',0.,0)
          endif
        else
          if (itm) then
            call PGBOX('BICNST',xtick,nxtick,'BICNST',0.,0)
          else
            call PGBOX('BCNST',xtick,nxtick,'BCNST',0.,0)
          endif
        endif
C plot data.
        if (hist) then
          call PG_BinBuf(n,x,y,.true.)
        else
          call PG_LineBuf(n,x,y)
        endif
C Draw line at y=0.0.
        if (zeroline) call PG_HLine(xmin,xmax,0.)
C Write various text stuff on plot, write filename on first plot.
        if (mod(j-1,nh*nv).eq.0) then
          ii=index(file,'.fits') - 1
          if (ii.lt.1) ii=lc(file)
          ii=min(20,ii)
          jj=min(30,lc(object))
          write(c80,'(i3,3x,a,3x,a)') j,file(1:ii),object(1:jj)
          if (both) then
            call PGMTEXT('T',-1.5,0.1,0.,c80)
          else
            call PGMTEXT('T',0.5,0.1,0.,c80)
          endif
        elseif (mod(j-2,nh*nv).eq.0) then
          jj=inhead('OBSNUM',header)
          call GetFitsDate(header,c9,4)
          write(c80,'(i3,a,i4,4x,2a,i2,a)') j,'    Obs#',jj,c9,
     .              '   bin=',max(0,min(99,bin)),'px'
          if (both) then
            call PGMTEXT('T',-1.5,0.1,0.,c80)
          else
            call PGMTEXT('T',0.5,0.1,0.,c80)
          endif
        elseif (mod(j-3,nh*nv).eq.0) then
          targname = chead('TARGNAME',header)
          write(c80,'(i3,3x,a)') j,targname(1:min(30,lc(targname)))
          if (both) then
            call PGMTEXT('T',-1.5,0.1,0.,c80)
          else
            call PGMTEXT('T',0.5,0.1,0.,c80)
          endif
        else
          if (both) then
            write(c80,'(i3,37x)') j
            call PGMTEXT('T',-1.5,0.1,0.,c80)
          else
            write(c80,'(i3,37x)') j
            call PGMTEXT('T',0.5,0.1,0.,c80)
          endif
        endif

C Show pixel scale on top.
        if (both) then
C Find approximate xmin and xmax in pixel space.
          xmin = xp( neari(xmin,n,x) )
          xmax = xp( neari(xmax,n,x) )
          call PGWINDOW(xmin,xmax,ymin,ymax)
          call PGBOX('ICMST',0.,0,'',0.,0)
        endif

750     continue

      enddo
      call PGEND
      return
      end

C----------------------------------------------------------------------
C
      logical function findsinglearg(arg,narg,s,d,wrds,nwrd)
C
      implicit none
      character*(*) arg(*)
      integer*4 narg
      character*(*) s,d,wrds(*)
      integer*4 nwrd
C
      integer*4 i,k,lc,j,jj
      character*80 subarg(1)
      logical findarg
      findsinglearg = .false. 
      k=lc(s)
      i=1
      do while(i.le.narg)
        if (arg(i)(1:k).eq.s(1:k)) then
          subarg(1) = arg(i) 
          findsinglearg = findarg(subarg,1,s,d,wrds,nwrd)
          jj=0
          do j=1,narg
            if (j.ne.i) then
              jj=jj+1
              arg(jj) = arg(j)
            endif
          enddo
          narg=narg-1
          i=narg+1
        endif
        i=i+1
      enddo
      return
      end
        

C----------------------------------------------------------------------
C Interactive spectra-in-an-image plotting.
C The second image is in x().  It is appended to the end of the first image
C in a(), and then x() is filled with the pixel values.

      subroutine spim1(a,x,sc,ec,sr,er,header,file,idfile,row)

      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er,1:2),x(sc:ec,sr:er)
      character*(*) header(2)
      character*(*) file(2)
      character*(*) idfile
      integer*4 row
C Line ids, etc.
      include 'spim1.inc'
      include 'verbose.inc'
      include 'soun.inc'
C Maximum number of rows.
      integer*4 mr
      parameter(mr=200)
      real*4 hi,yma(mr,1:2),cx,cy,r,r1,r2,r3,r4,valread
      real*4 xmin(mr),xmax(mr),ymin(mr),ymax(mr),slop,mdn_resid,hipix
      real*8 r81,r82,r83,disp,thres,wrms,high,orwt
      integer*4 i,ii,i1,i2,neari,neari_bs8,ikey,npt
      integer*4 nrej,k,lastchar,narr
      integer*4 nh,nv,j1,j2,j,nc,ino,ord,uord
      character*80 ThArFile, CuArFile, XeFile, HgNeFile
      character*80 CuAr, Xe, HgNe, chead, c80
      character key*1, dig1*1, dig2*1, c2*2, c1*1
      logical yfixzero,yfix,ok

C Set ThAr (and or other lines) database filename.
      call GetMakeeHome(c80)
      ThArFile = c80(1:lastchar(c80))//'ThAr.dat'    ! for HIRES
      CuArFile = c80(1:lastchar(c80))//'CuAr.dat'    ! for ESI
      XeFile   = c80(1:lastchar(c80))//'Xe.dat'      ! for ESI
      HgNeFile = c80(1:lastchar(c80))//'HgNe.dat'    ! for ESI

C Zero line-ids.
      do ino=1,2
        do j=1,maxr
          nl(j,ino)=0
          do i=1,maxl
            lc(i,j,ino)=0.d0
            lw(i,j,ino)=0.d0
          enddo
        enddo
      enddo

C Check size.
      if (er.gt.mr) then
        write(soun,'(a)') 'Last row number is too large-- aborting.'
        return
      endif

C Read arc line database lists appropriate for instrument and lamp.
      nthar=0
      if (spim1_HIRES) then
C Read Thorium-Argon line list.
        nthar=0
        open(1,file=ThArFile,status='old')
11      continue
        read(1,*,end=55) r81,r82,i
        nthar       = nthar+1
        thar(nthar) = r81
        tharI(nthar)= r82
        tharR(nthar)= i
        goto 11
55      close(1)
      endif
C ESI.
      if (spim1_ESI) then
        nthar=0
        CuAr = chead('LAMPCU1',header)    ! CuAr.
        Xe   = chead('LAMPNE1',header)    ! Xe?
        HgNe = chead('LAMPAR1',header)    ! HgNe?
        if (CuAr(1:2).eq.'on') then
          open(1,file=CuArFile,status='old')
101       read(1,*,end=501) r81
          nthar = nthar + 1
          thar(nthar) = r81
          tharI(nthar)= 1.
          tharR(nthar)= 1.
          goto 101
501       close(1)
        endif
        if (Xe(1:2).eq.'on') then
          open(1,file=XeFile,status='old')
102       read(1,*,end=502) r81
          nthar = nthar + 1
          thar(nthar) = r81
          tharI(nthar)= 1.
          tharR(nthar)= 1.
          goto 102
502       close(1)
        endif
        if (HgNe(1:2).eq.'on') then
          open(1,file=HgNeFile,status='old')
103       read(1,*,end=503) r81
          nthar = nthar + 1
          thar(nthar) = r81
          tharI(nthar)= 1.
          tharR(nthar)= 1.
          goto 103
503       close(1)
        endif
C Sort list. (WARNING: Only works if tharI() and tharR() are all 1.)
        call qcksrt8(nthar,thar)
      endif

C Read user line list.
      if (idfile.eq.' ') then
        idfile='junk.ids'
      else
        call ReadIDs(idfile)
      endif

C Defaults.
      hipix = 400.
      newli = .false.
      newim = .false.
      if (header(2).eq.' ') then
        hide  = .true.
      else
        hide  = .false.
      endif
      hist  = .false.
      yfix  = .false.
      yfixzero = .true.
      NoCentroid = .false.
      nautokey = 0
      cx = 0.
      cy = 0.
      pgd= .true.

C Fill second half of "a" array.
      do i=sc,ec
        do j=sr,er
          a(i,j,2)=x(i,j)
        enddo
      enddo

C Set defaults for weak and saturated lines based on which image is weaker.
C Default if 2 is weaker than 1.
      weakline(1) = 80.
      weakline(2) = 500.
C Do not allow saturated lines in first image.
      satuline(1) = 64000.
C Allow saturated lines in second image.
      satuline(2) = 99000.
      if (numim.eq.1) then
C If only one image, allow saturated lines in first (and only) image.
        satuline(1) = 99000.
      else
C Which image is stronger?
        r1=0.
        r2=0.
        do i=sc,ec,10
          do j=sr,er
            r1=r1+a(i,j,1)
            r2=r2+a(i,j,2)
          enddo
        enddo
C Reverse defaults if 1 is weaker than 2.
        if (r1.lt.r2) then
          weakline(2) = 80.
          weakline(1) = 500.
          satuline(2) = 64000.
          satuline(1) = 99000.
        endif
      endif

C Fill x-matrix.
      do j=sr,er
        do i=sc,ec
          x(i,j)=float(i) 
        enddo
      enddo
      nc=1+ec-sc

C Fill ymax array.
      do ino=1,2
        do j=sr,er
          hi=1.e-20
          do i=sc,ec
            if (a(i,j,ino).gt.hi) hi=a(i,j,ino)
          enddo
          yma(j,ino)=hi*1.03
          xmax(j)=-1.
        enddo
      enddo

C Start by displaying the first image as the primary spectrum.
      ino=1
C Start by showing middle row.
      if (row.lt.-1000) row=(sr+er)/2
      row=max(sr,min(er,row))     

CCCCCCCCCCCCCCCCCCC Temporary
C     if (twod) then
C       print *,'WARNING:  "-twod"  IS A WORK-IN-PROGRESS OPTION.'
C       print *,'WARNING:  YOU PROBABLY SHOULD NOT BE USING THIS...'
C go direct to surface fit.
C       print '(a,$)',' usr,uer : '
C       read(5,*) usr,uer
C       call FitSurfaceWaves(sc,ec,sr,er,usr,uer,ok)
C       print *,'ok=',ok
C       if (ino.ne.-987) stop
C     endif
CCCCCCCCCCCCCCCCCCC Temporary
     
C Open display.
1     continue
      if (vt100) then
        call PGBEGIN(0,'/RETRO',1,1)
      else
        call PGBEGIN(0,'/XDISP',1,1)
      endif
      call PGASK(.false.)
      call PGVSTAND
      call PGSLW(1)
      call PGSCF(1)
      call PGSCH(1.3)

C Set x and y limits.
2     continue
C First time.
      if (xmax(row).lt.0.) then
        xmin(row)=float(sc)
        xmax(row)=float(ec)
        ymin(row)=0.
        ymax(row)=yma(row,ino)
        cx = (xmin(row)+xmax(row))/2.
        cy = (ymin(row)+ymax(row))/2.
      endif
C Fix ymin at zero.
      if (yfixzero) ymin(row)=0.
C Look for best ymax.
      if (.not.yfix) then
        i1 = neari(xmin(row),nc,x(sc,row)) + sc - 1
        i2 = neari(xmax(row),nc,x(sc,row)) + sc - 1
        hi = ymin(row)
        do i=i1,i2
          if (a(i,row,ino).gt.hi) hi=a(i,row,ino)
        enddo
        ymax(row) = ymin(row) + (hi-ymin(row))*1.03 + 1.e-20
      endif

C Clear and replot.
3     continue
      call PGPAGE
C Set window and box.
      call PGWINDOW(xmin(row),xmax(row),ymin(row),ymax(row))
      call PGBOX('BICNST',0.,0,'BICNST',0.,0)
C Plot primary spectrum.
      if (hist) then
        call PG_BinBuf(nc,x(sc,row),a(sc,row,ino),.true.)
      else
        call PG_LineBuf(nc,x(sc,row),a(sc,row,ino))
      endif
C Over-plot secondary spectrum.
      if ((.not.hide).and.(numim.eq.2)) then
        i=2
        if (ino.eq.2) i=1
        call PGSCI(2)
        if (hist) then
          call PG_BinBuf(nc,x(sc,row),a(sc,row,i),.true.)
        else
          call PG_LineBuf(nc,x(sc,row),a(sc,row,i))
        endif
        call PGSCI(1)
      endif
C Write filename and row on plot.
      i=2
      if (ino.eq.2) i=1
      write(c80,'(a,i3,a,i3,i5,3a,i1,3(a))') 
     .      ' Row=',row,' of',er,ino,': ',
     .      file(ino)(1:lastchar(file(ino))),
     .      '   [',i,': ',file(i)(1:lastchar(file(i))),']'
      call PGMTEXT('B',2.5,0.5,0.5,c80)

C Mark any line-ids.
      call MarkIDs(row,ino,sc,ec,sr,er,a,x,3)
      if ((.not.hide).and.(numim.eq.2)) then
        i=2
        if (ino.eq.2) i=1
        call MarkIDs(row,i,sc,ec,sr,er,a,x,2)
      endif

C Get key stroke.
4     continue
      call GetKey(cx,cy,key)
      ikey=ichar(key)
      if ((key.eq.'/').or.(key.eq.'?')) then

      print *,' '
      print '(a)','Switching between rows:'
      print *,' 5  : enter two digit row number.'
      print *,'+,= : increase row number.'
      print *,'-,_ : decrease row number.'
      print *,' m  : multiple plots.'
      print *,' '
      print '(a)','Setting plot ranges:'
      print *,'i,I : zoom in (x).'
      print *,'o,O : zoom out (x).'
      print *,' p  : pan (center at cursor).'
      print *,' [  : pan left.'
      print *,' ]  : pan right.'
      print *,' y  : zoom out (y).'
      print *,' f  : fix y limits (no auto-scaling) [toggle].'
      print *,' F  : fix y minimum at zero [toggle].'
      print *,' h  : histogram mode [toggle].'
      print *,' w  : show whole plot.'
      print *,' u  : update (redraw) plot.'
      print *,'LMB : set x limits (Left Mouse Button).'
      print *,'MMB : set both x and y limits (Middle Mouse Button).'
      print *,'RMB : set y limits (Right Mouse Button).'
      print *,' '
      print '(a)','Special:'
      print *,' x  : Set a range of pixels to zero.'
      print *,' z  : Zap a pixel (set to median of nearest 12 pixels.)'
      print *,' d  : Set data value directly.'
      print *,' '
      print '(a)',
     .  'Identifying arc-lamp lines and fitting a wavelength scale:'
      print *,' c  : centroid and name an arclamp line.'
      print *,' g  : centroid and guess wavelength of a line.'
      print *,' e  : centroid and guess wavelength of a l',
     .                                 'ine using ESI scale.'
      print *,' s  : estimate wavelength at cursor position.'
      print *,' S  : same as "s", but based on line IDs in OTHER rows.'
      print *,' q  : show results of a quick fit to lines in this row.'
      print *,' M  : mark the positions of known lines.'
      print *,'#/3/4:show nearby lines in database based on dispersion.'
      print *,
     .'^G  : identify unmarked lines using fit to lines in this row.'
      print *,
     .'^R  : identify unmarked lines using fit to lines in other rows.'
      print *,'^Q  : Do FINAL fit for current row.'
      print *,
     .  ' C  : CLEAN out bad lines by fitting and deleting line IDs.'
      print *,
     .  ' T  : Change thresholds for weak lines and saturated lines.'
      print *,'^T  : Combination of keys:  ^G 2 ^G 1 q '
      print *,'^F  : Do multiple ^T options.'
      print *,'^H  : Combination of keys:  ^G q 4 + '
      print *,'^X  : Do multiple ^H options.'
      print *,' '
      print '(a)','Deleting lines from ID list:'
      print *,' k  : delete a line id.'
      print *,'^K  : delete ALL the lines in this row.'
      print *,' '
      print '(a)','Reading and writing line-ID list:'
      print *,' P  : print line ids for current row onto screen.'
      print *,' W  : write line ids to current line id file : ',
     .               idfile(1:lastchar(idfile))
      print *,' R  : read in line id list from a new file.'
      print *,' '
      print '(a)','Two-image plotting:'
      print *,' 1  : Make first image the primary spectrum.'
      print *,' 2  : Make second image the primary spectrum.'
      print *,' 0  : Show/hide secondary spectrum [toggle].'
      print *,' '
      print '(a)','Other:'
      print *,
     .  '^E  : Show the row and column of original image at cursor.'
      print *,' N  : (NO) centroiding when marking line [toggle].'
      print *,' J  : Create PostScript file "junk.ps" of current plot.'
      print *,'^J  : Same as "J" but also send junk.ps to printer.'
      print *,'?,/ : this help.'
      print *,'ESC : exit.'

        goto 4

      elseif (key.eq.'x') then     ! Set range of pixels to zero.
        r=cx
        print *,' Select second limit to define range of pixels...'
        call GetKey(cx,cy,key)
        i1 = max(sc,min(ec,nint(min(r,cx))))
        i2 = max(sc,min(ec,nint(max(r,cx))))
        do i=i1,i2
          a(i,row,ino) = 0.
          newim=.true.
        enddo
        goto 2

      elseif (key.eq.'z') then     ! Zap a pixel.
        k = max(sc,min(ec,nint(cx)))
        i1= max(sc,min(ec,k-6))
        i2= max(sc,min(ec,k+6))
        narr=0
        do i=i1,i2
          if (i.ne.k) then
            narr=narr+1
            arr(narr) = a(i,row,ino)
          endif
        enddo
        if (narr.gt.1) then
          call find_median(arr,narr,r)
          a(k,row,ino)=r 
          newim=.true.
        endif
        goto 2

      elseif (key.eq.'d') then     ! Set range of pixels to entered value.
        r=cx
        print *,' Select second limit to define range of pixels...'
        call GetKey(cx,cy,key)
        i1 = max(sc,min(ec,nint(min(r,cx))))
        i2 = max(sc,min(ec,nint(max(r,cx))))
770     print '(a,$)',' Enter data value for this/these pixel(s): '
        read(5,*,err=770) r
        do i=i1,i2
          a(i,row,ino) = r
          newim=.true.
        enddo
        goto 2

      elseif (key.eq.'T') then   ! Change thresholds for choosing weak lines
        do ii=1,numim
          print '(a,i2,a,2f10.1)',
     . ' For image',ii,'  weakline,satuline=',weakline(ii),satuline(ii)
          print '(a,$)',' New minimum peak level (weakline) : '
          read(5,*) weakline(ii)
          print '(a,$)',' New maximum peak level (satuline) : '
          read(5,*) satuline(ii)
          print '(a,i2,a,2f10.1)',
     . ' For image',ii,'  weakline,satuline=',weakline(ii),satuline(ii)
        enddo
        goto 4

      elseif (ikey.eq.20) then   ! ^T : Autokey combination
        autokey(1) = char(7)     ! ^G
        autokey(2) = '2'
        autokey(3) = char(7)
        autokey(4) = '1'
        autokey(5) = 'q'
        nautokey = 5
        goto 4

      elseif (ikey.eq.6) then   ! ^F : Multiple autokey combination
        print '(a,$)',' (^T) Forward or backward (+/-) : '
        read(5,'(a)',err=1) c1
        print '(a,$)',' How many rows : '
        read(5,*,err=1) k
        j=1
        do i=1,k
          autokey(j)  = char(7)
          autokey(j+1)= '2'
          autokey(j+2)= char(7)
          autokey(j+3)= '1'
          autokey(j+4)= 'q'
          autokey(j+5)= '4'
          autokey(j+6)= '+'
          if (c1.eq.'+') then
            autokey(j+7)= '+'
          else
            autokey(j+7)= '-'
          endif
          j=j+8
        enddo
        nautokey=j-1
        goto 4

      elseif (ikey.eq.8) then   ! ^H : Multiple autokey combination
        autokey(1)  = char(7)
        autokey(2)= 'q'
        autokey(3)= '4'
        autokey(4)= '+'
        nautokey=4
        goto 4

      elseif (ikey.eq.24) then   ! ^X : Multiple autokey combination
        print '(a,$)',' (^H) Forward or backward (+/-) : '
        read(5,'(a)',err=1) c1
        print '(a,$)',' How many rows : '
        read(5,*,err=1) k
        j=1
        do i=1,k
          autokey(j)  = char(7)
          autokey(j+1)= 'q'
          autokey(j+2)= '4'
          autokey(j+3)= '+'
          if (c1.eq.'+') then
            autokey(j+4)= '+'
          else
            autokey(j+4)= '-'
          endif
          j=j+5
        enddo
        nautokey=j-1
        goto 4

      elseif (ikey.eq.17) then   ! ^Q : final fit for current row.
        call PGEND
        print '(a,2i5)',' Starting row, ending row : ',sr,er
        print '(a,$)',' Enter first and last row to fit : '
        read(5,*) j1,j2
751     print '(a,$)',' Verbose? (1/0) [1] : '
        read(5,'(a)',err=751) c80
        if (index(c80,'0').gt.0) then
          call ForceVerbose(0)
        else
          call ForceVerbose(1)
        endif
752     print '(a,$)',' Threshold for rejection(px)? [0.4] : '
        c80=' '
        read(5,'(a)',err=752) c80
        thres=dble(max(0.05,min(10.0,valread(c80))))
        if (c80.eq.' ') thres=0.4d0
        print '(a,f6.2)',' Threshold=',thres
753     print '(a,$)',
     .  ' Weight for points derived from other rows? [0.3] : '
        c80=' '
        read(5,'(a)',err=753) c80
        orwt=dble(max(0.01,min(10.0,valread(c80))))
        if (c80.eq.' ') orwt=0.3d0
        print '(a,f6.2)',' orwt=',orwt
        if (verbose) then
          call PGBEGIN(0,'/XDISP',1,1)
          call PGASK(.false.)
          call PGVSTAND
        endif
        do j=j1,j2
          call FinalFit(sc,ec,sr,er,j,thres,orwt,header,' ',
     .                  ord,wrms,high,npt,nrej,ok)
          print '(a,i3,a,i2,a,f8.4,a,f8.4,a,i3,a,i3)',
     .  ' Final fit: ROW=',j,
     .  '  ORD=',ord,'  WRMS=',wrms,'  HIGH=',high,
     .  '  NPT=',npt,'  NREJ=',nrej
          lrms(j)= wrms
          lord(j)= ord
          newli  = .true.
        enddo
        if (verbose) call PGEND
        call ForceVerbose(2)
        goto 1

      elseif (ikey.eq.5) then   ! ^E : show original row and column.

        print *,'THIS function no longer works... tab -may00.'
C       r1 = sngl(polyarr(co(1,row),dble(cx)))
C       i = max(sc,min(ec,nint(cx)))
C       print '(a,i3,a,f8.2,a,f8.2,a,1pe15.7)',
C    .  '  Order=',row,'   Row=',r1,
C    .  '   Column=',cx,'   Sp. Value=',a(i,row,ino)
        goto 4

      elseif ((key.eq.'J').or.(ikey.eq.10)) then   ! create PS file junk.ps
        call PGEND
        call PGBEGIN(0,'junk.ps/PS',1,1)
        call PGASK(.false.)
        call PGVSTAND
        call PGSLW(1)
        call PGSCF(1)
        call PGSCH(1.0)
        call PGPAGE
        call PGWINDOW(xmin(row),xmax(row),ymin(row),ymax(row))
        call PGBOX('BICNST',0.,0,'BICNST',0.,0)
        if (hist) then
          call PG_BinBuf(nc,x(sc,row),a(sc,row,ino),.true.)
        else
          call PG_LineBuf(nc,x(sc,row),a(sc,row,ino))
        endif
        if ((.not.hide).and.(numim.eq.2)) then
          i=2
          if (ino.eq.2) i=1
          call PGSLS(4)
          if (hist) then
            call PG_BinBuf(nc,x(sc,row),a(sc,row,i),.true.)
          else
            call PG_LineBuf(nc,x(sc,row),a(sc,row,i))
          endif
          call PGSLS(1)
        endif
        i=2
        if (ino.eq.2) i=1
        write(c80,'(a,i3,a,i3,i5,3a,i1,3(a))') 
     .        ' Row=',row,' of',er,ino,': ',
     .        file(ino)(1:lastchar(file(ino))),
     .        '   [',i,': ',file(i)(1:lastchar(file(i))),']'
        call PGMTEXT('B',2.5,0.5,0.5,c80)
        call MarkIDs(row,ino,sc,ec,sr,er,a,x,1)
        if ((.not.hide).and.(numim.eq.2)) then
          i=2
          if (ino.eq.2) i=1
          call PGSLS(4)
          call MarkIDs(row,i,sc,ec,sr,er,a,x,1)
          call PGSLS(1)
        endif
        call PGEND
        if (ikey.eq.10) then
          call system('/usr/ucb/lpr junk.ps')
          print *,'Sent PostScript file "junk.ps" to printer.'
        else
          print *,
     .  'Created PostScript file "junk.ps" in default directory.'
        endif
        goto 1

      elseif (key.eq.'1') then     ! make first image primary spectrum
        ino=1
        goto 3

      elseif (key.eq.'2') then     ! make second image primary spectrum
        if (numim.eq.2) then
          ino=2
        else
          print *,'There is only one image.'
        endif
        goto 3

      elseif (key.eq.'0') then     ! show/hide secondary spectrum
        if (hide) then
          hide=.false.
        else
          hide=.true.
        endif
        goto 3

      elseif (key.eq.'s') then     ! estimate wavelength at cursor position
        call  FindWave(sc,ec,sr,er,row,dble(cx),r81,disp)
        if (r81.gt.0.) then
          r82 = thar(neari_bs8(r81,nthar,thar))
          print '(a,f9.3,2(a,f10.4),a,f7.3,a,f6.2,a)',
     .      'Pix=',cx,'   Wav=',r81,
     .      '   Tab=',r82,'   Res=',min(999.d0,max(-99.d0,r81-r82)),
     .      ' (',min(999.d0,max(-99.d0,(r81-r82)/disp)),'px)'
        endif
        goto 4

      elseif (key.eq.'S') then     ! same as "s", but use lines in other rows.
        uord=-1
        call QuickFitRows(sc,ec,sr,er,row,dble(cx),uord,1,r81,disp,ok)
        if (ok) then
          r82 = thar(neari_bs8(r81,nthar,thar))

          print '(a,f9.3,2(a,f10.4),a,f7.3,a,f6.2,a)',
     .      'Pix=',cx,'   Wav=',r81,
     .      '   Tab=',r82,'   Res=',min(999.d0,max(-99.d0,r81-r82)),
     .      ' (',min(999.d0,max(-99.d0,(r81-r82)/disp)),'px)'

        endif
        goto 4

      elseif (key.eq.'q') then     ! do a quick fit for row and show residuals
        r81 = -1.d0
        call PGEND
        ord=-1
        call ForceVerbose(1)
        call QuickFit(sc,ec,row,dble(cx),r81,disp,ord,ok,mdn_resid)
        call ForceVerbose(2)
        if (.not.ok) print *,'Error in wavelength scale fit.'
        goto 1

      elseif (key.eq.'C') then     ! do a quick fit and delete bad line IDs.
        print '(a,$)',
     .  ' Residual threshold in pixels (-1.=no saturated) : '
        read(5,'(a)') c80
        if (c80.eq.' ') then
          thres = 0.5d0
        else
          thres = abs(dble(valread(c80)))
        endif
        print '(a,f7.3,a)',' Threshold =',thres,'px.'
        call CleanFit(sc,ec,row,thres,.true.,ok)
        if (.not.ok) print *,' Error. Failure in cleaning fit.'
        goto 4

      elseif (key.eq.'k') then     ! delete a pixel and wavelength id
        slop=0.01*(xmax(row)-xmin(row))
        call NearID(cx,row,ino,i,slop)
        if (i.gt.0) then
          call mark_line(sngl(lc(i,row,ino)),2)
          call DeleteLine(row,ino,i)
        else
          print *,'Could not find line to delete.'
        endif
        goto 4

      elseif (ikey.eq.11) then     ! ^K delete ALL the lines in this row.
        newli=.true.
        nl(row,1)=0
        nl(row,2)=0
        goto 3

      elseif (key.eq.'M') then   ! M : mark the positions of known lines.
        call MarkKnownLines(sc,ec,row,ino) 
        goto 4

      elseif (ikey.eq.07) then   ! ^G : identify unmarked lines based on table.
        call IdentifyLines(sc,ec,sr,er,a,x,row,ino,mdn_resid,ok)
        if (.not.ok) print *,'Error identifying lines.'
        goto 4

      elseif (ikey.eq.18) then   ! ^R : identify unmarked lines based on table.
        call IdentifyLinesUsingRows(sc,ec,sr,er,a,x,row,ino,0,ok)
        if (.not.ok) print *,'Error identifying lines.'
        goto 4

      elseif (key.eq.'g') then     ! centroid and guess wavelength of a line
        call line_center(sc,ec,sr,er,a,x,row,ino,cx-4.,cx+4.,r1,r3,r4)
        if (abs(r1+r3+r4).lt.1.e-10) then
          print *,'Centroid failure.'
          goto 4
        endif
        slop=0.01*(xmax(row)-xmin(row))
        call NearID(r1,row,ino,i,slop)
        if (i.ne.0) then
          print *,'Too close to known line, delete line first.'
          goto 4
        endif
        call mark_line(r1,4)
        call  FindWave(sc,ec,sr,er,row,dble(r1),r81,disp)
        if (r81.gt.0.) then
          r82 = thar(neari_bs8(r81,nthar,thar))
          print '(a,f9.3,2(a,f10.4),a,f7.3,a,f6.2,a)',
     .      'Pix=',r1,'   Wav=',r81,
     .      '   Tab=',r82,'   Res=',min(999.d0,max(-99.d0,r81-r82)),
     .      ' (',min(999.d0,max(-99.d0,(r81-r82)/disp)),'px)'
        endif
        r83=abs((r81-r82)/disp)
        if (r83.lt.3.0) then
          print '(a,f10.4)',' Adding line :',r82
          call AddLine(row,ino,dble(r1),r82,dble(r3))
          call MarkIDs(row,ino,sc,ec,sr,er,a,x,3)
        elseif (r83.lt.9.0) then
          print '(a,f5.2,a)',
     .       ' Residual is',r83,'px; do you want to accept? (y/n)...'
          print *,'Hit "y" or "n" in plot window.'
          call PGCURS(cx,cy,key)
          if (key.eq.'y') then
            print '(a,f10.4)',' Adding line :',r82
            call AddLine(row,ino,dble(r1),r82,dble(r3))
            call MarkIDs(row,ino,sc,ec,sr,er,a,x,3)
          else
            print *,'Ok. Not accepted.'
          endif
        else
          print *,'No suitable line.'
        endif
        goto 4

      elseif (key.eq.'e') then     ! centroid and guess (using ESI scale).
C Centroid.
        call line_center(sc,ec,sr,er,a,x,row,ino,cx-4.,cx+4.,r1,r3,r4)
        if (abs(r1+r3+r4).lt.1.e-10) then
          print *,'Centroid failure.'
          goto 4
        endif
        slop=0.01*(xmax(row)-xmin(row))
C Is there another close ID already in the list?
        call NearID(r1,row,ino,i,slop)
        if (i.ne.0) then
          print *,'Too close to known line, delete line first.'
          goto 4
        endif
C Mark the line on the plot.
        call mark_line(r1,4)
C Find the wavelength at this pixel using ESI scale.
        call  FindWave_ESI(row,dble(r1),r81,disp)
        if (r81.gt.0.) then
          r82 = thar(neari_bs8(r81,nthar,thar))
          print '(a,f9.3,2(a,f10.4),a,f7.3,a,f6.2,a)',
     .      'Pix=',r1,'   Wav=',r81,
     .      '   Tab=',r82,'   Res=',min(999.d0,max(-99.d0,r81-r82)),
     .      ' (',min(999.d0,max(-99.d0,(r81-r82)/disp)),'px)'
        endif
C Close enough?
        r83=abs((r81-r82)/disp)
        if (r83.lt.3.0) then
          print '(a,f10.4)',' Adding line :',r82
          call AddLine(row,ino,dble(r1),r82,dble(r3))
          call MarkIDs(row,ino,sc,ec,sr,er,a,x,3)
C Ask first.
        elseif (r83.lt.15.0) then
          print '(a,f5.2,a)',
     .       ' Residual is',r83,'px; do you want to accept? (y/n)...'
          print *,'Hit "y" or "n" in plot window.'
          call PGCURS(cx,cy,key)
          if (key.eq.'y') then
            print '(a,f10.4)',' Adding line :',r82
            call AddLine(row,ino,dble(r1),r82,dble(r3))
            call MarkIDs(row,ino,sc,ec,sr,er,a,x,3)
          else
            print *,'Ok. Not accepted.'
          endif
C Failure.
        else
          print *,'No suitable line.'
        endif
        goto 4

      elseif (key.eq.'c') then     ! line center
        call line_center(sc,ec,sr,er,a,x,row,ino,cx-4.,cx+4.,r1,r3,r4)
        if (abs(r1+r3+r4).lt.1.e-10) then
          print *,'Centroid failure.'
          goto 4
        endif
        slop=0.01*(xmax(row)-xmin(row))
        call NearID(r1,row,ino,i,slop)
        if (i.ne.0) then
          print *,'Too close to known line, delete line first.'
          goto 4
        endif
        call mark_line(r1,4)
        print *,'Enter wavelength in plot window.' 
        call PG_ReadPlotReal(r82,1.2,'T',0.5,0.4)
        call AddLine(row,ino,dble(r1),r82,dble(r3))
        call MarkIDs(row,ino,sc,ec,sr,er,a,x,3)
        goto 4

      elseif (key.eq.'R') then     ! read line ids
        print '(a,a)',' Old filename of line ids : ',
     .                          idfile(1:lastchar(idfile))
704     print '(a,$)',' New filename of line ids : '
        read(5,'(a)',err=704) c80
        if (c80.eq.' ') then
          print *,'Keeping same filename.'
        else
          idfile = c80
          print '(a,a)',' New filename : ',idfile(1:lastchar(idfile))
        endif
        call ReadIDs(idfile)
        goto 4

      elseif (key.eq.'P') then     ! print line ids
        call PrintIDs(row)
        goto 4

      elseif (key.eq.'W') then     ! write line ids
        call WriteIDs(idfile)
        goto 4

      elseif (key.eq.'m') then     ! multiple plots
22      continue
        call PGEND
        print *,'Enter -1 to quit.'
705     print '(a,$)',' Number of horizontal plots : '
        read(5,*,err=705) nh
        if (nh.lt.0) goto 1
        nh = max(1,min(10,nh))
706     print '(a,$)','   Number of vertical plots : '
        read(5,*,err=706) nv
        nv = max(1,min(10,nv))
707     print '(a,$)','        Starting row number : '
        read(5,*,err=707) j
        j1 = max(sr,min(er,j))
        j2 = max(sr,min(er,j+(nh*nv)-1))
        call spim1_MultiPlot(sc,ec,sr,er,a,x,nh,nv,j1,j2,ino)
        goto 22

      elseif ((key.eq.'#').or.(key.eq.'3').or.(key.eq.'4')) then  ! Guessing
        r1 = cx
        if (key.eq.'#') then
          print *,'Enter wavelength at cursor in plot window.' 
          call PG_ReadPlotReal(r81,1.2,'T',0.5,0.4)
        else
          r81=0.
        endif
C Find nearest line ID.
        if (r81.lt.1.) then
          slop=0.01*(xmax(row)-xmin(row))
          call NearID(cx,row,ino,i,slop)
          if (i.gt.0) then
            r81 = lw(i,row,ino) 
            r1  = lc(i,row,ino)
          endif
        else
C See if there is a better value.
          r82 = thar(neari_bs8(r81,nthar,thar))
          if (abs(r81-r82).lt.0.11) then
            write(soun,'(f10.4,a,f10.4)') r81,' --->',r82
            r81=r82
          endif 
        endif 
C Show nearby lines.
        call spim1_nearby_guesses(row,r1,r81,key)
        goto 4

      elseif (key.eq.'w') then     ! reset limits.
        xmax(row)=-1.
        goto 2

      elseif (key.eq.'u') then     ! redraw (update) plot.
        goto 2

      elseif (key.eq.'N') then     ! no centroiding (toggle)
        if (NoCentroid) then
          NoCentroid = .false.
        else
          NoCentroid = .true.
        endif
        print *,'NoCentroid=',NoCentroid
        goto 4

      elseif (key.eq.'h') then     ! histogram mode (toggle)
        if (hist) then
          hist = .false.
        else
          hist = .true.
        endif
        print *,'hist=',hist
        goto 2

      elseif (key.eq.'f') then     ! fix y limits (toggle)
        if (yfix) then
          yfix = .false.
          print *,'yfix=',yfix
          goto 2
        else
          yfix = .true.
          print *,'yfix=',yfix
          goto 4
        endif

      elseif (key.eq.'F') then     ! fix y zero (toggle)
        if (yfixzero) then
          yfixzero = .false.
        else
          yfixzero = .true.
        endif
        print *,'yfixzero=',yfixzero
        goto 2

      elseif (key.eq.'p') then     ! pan center
        r = (xmax(row)-xmin(row))/2.
        xmin(row) = max(x(sc,row),cx-r)
        xmax(row) = min(x(ec,row),cx+r)
        cx = (xmax(row)+xmin(row))/2.
        goto 2

      elseif (key.eq.'[') then     ! pan left
        r1 = xmax(row)-xmin(row)
        xmin(row) = max(x(sc,row),xmin(row)-r1)
        xmax(row) = xmin(row) + r1
        cx = (xmax(row)+xmin(row))/2.
        goto 2

      elseif (key.eq.']') then     ! pan right
        r1 = xmax(row)-xmin(row)
        xmax(row) = min(x(ec,row),xmax(row)+r1)
        xmin(row) = xmax(row) - r1
        cx = (xmax(row)+xmin(row))/2.
        goto 2

      elseif (key.eq.'i') then     ! zoom in
        r = (xmax(row)-xmin(row))/8.
        xmin(row) = max(x(sc,row),cx-r)
        xmax(row) = min(x(ec,row),cx+r)
        goto 2

      elseif (key.eq.'I') then     ! zoom in more
        r = (xmax(row)-xmin(row))/16.
        xmin(row) = max(x(sc,row),cx-r)
        xmax(row) = min(x(ec,row),cx+r)
        goto 2

      elseif (key.eq.'o') then     ! zoom out
        r = (xmax(row)-xmin(row))*2.
        xmin(row) = max(x(sc,row),cx-r)
        xmax(row) = min(x(ec,row),cx+r)
        goto 2

      elseif (key.eq.'O') then     ! zoom out more
        r = (xmax(row)-xmin(row))*4.
        xmin(row) = max(x(sc,row),cx-r)
        xmax(row) = min(x(ec,row),cx+r)
        goto 2

      elseif (key.eq.'y') then     ! zoom out in y
        r = (ymax(row)-ymin(row))
        ymin(row) = ymin(row) - r
        ymax(row) = ymax(row) + r
        if (yfixzero) ymin(row)=0.
        goto 3
          
      elseif (key.eq.'A') then     ! Left Mouse Button (select x limits)
        r1 = max(x(sc,row),min(x(ec,row),cx))
        print *,' Select second x limit...'
        call GetKey(cx,cy,key)
        r2 = max(x(sc,row),min(x(ec,row),cx))
        xmin(row) = min(r1,r2)
        xmax(row) = max(r1,r2)
        cx = (xmax(row)+xmin(row))/2.
        cy = (ymax(row)+ymin(row))/2.
        goto 2
 
      elseif (key.eq.'D') then     ! Middle Mouse Button (select both limits)
        r1 = max(x(sc,row),min(x(ec,row),cx))
        r3=cy
        print *,' Select second x,y limit...'
        call GetKey(cx,cy,key)
        r2 = max(x(sc,row),min(x(ec,row),cx))
        xmin(row) = min(r1,r2)
        xmax(row) = max(r1,r2)
        ymin(row) = min(r3,cy)
        ymax(row) = max(r3,cy)
        cx = (xmax(row)+xmin(row))/2.
        cy = (ymax(row)+ymin(row))/2.
        goto 3
 
      elseif (key.eq.'X') then     ! Right Mouse Button (select y limits)
        r=cy
        print *,' Select second y limit...'
        call GetKey(cx,cy,key)
        ymin(row) = min(r,cy)
        ymax(row) = max(r,cy)
        cx = (xmax(row)+xmin(row))/2.
        cy = (ymax(row)+ymin(row))/2.
        goto 3
         
      elseif (ikey.eq.27) then     ! ESC = quit
        if (newli) then
701       print '(a,$)',
     .  'Do you want to save line ids before quitting (1/0)? '
          read(5,*,err=701) ii
          if (ii.eq.1) call WriteIDs(idfile)
        endif
        print *,'Exiting...'
        goto 800
      
      elseif ((key.eq.'-').or.(key.eq.'_')) then
        row=row-1
        if (row.lt.sr) then
          print *,'You are on the lowest row.'
          row=sr
          goto 4
        endif
        goto 2
        
      elseif ((key.eq.'+').or.(key.eq.'=')) then
        row=row+1
        if (row.gt.er) then
          print *,'You are on the highest row.'
          row=er
          goto 4
        endif
        goto 2
        
      elseif (key.eq.'5') then
708     print '(a)','Enter two digit row number in plot window.'
        call GetKey(cx,cy,dig1)
        call GetKey(cx,cy,dig2)
        c2=dig1//dig2
        read(c2,'(i2)',err=708) row
        if (row.lt.sr) then
          print *,'The lowest row is',sr
          row=sr
          goto 4
        elseif (row.gt.er) then
          print *,'The highest row is',er
          row=er
          goto 4
        endif
        goto 2

      else
        print *,'Try again.'
        goto 4

      endif

800   continue
      call PGEND
      return
      end

C----------------------------------------------------------------------
      subroutine spim1_MultiPlot(sc,ec,sr,er,a,x,nh,nv,j1,j2,ino)
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er,1:2),x(sc:ec,sr:er)
      real*4 xmin,xmax,ymin,ymax,hi
      integer*4 nh,nv,j,nc,j1,j2,i,ino
      character c4*4
      include 'spim1.inc'
C Open display.
      if (vt100) then
        call PGBEGIN(0,'/RETRO',+1*nh,nv)
      else
        call PGBEGIN(0,'/XDISP',+1*nh,nv)
      endif
      call PGASK(.false.)
      call PGVSTAND
      call PGSLW(1)
      call PGSCF(1)
      call PGSCH(2.5)
C Plot rows.
      nc = 1+ec-sc
      do j=j1,j2
        xmin=x(sc,j)
        xmax=x(ec,j)
        ymin=0.
        hi=0.
        do i=sc,ec
          if (a(i,j,ino).gt.hi) hi=a(i,j,ino)
        enddo
        ymax= hi*1.03 + 1.e-20
        call PGPAGE
        call PGWINDOW(xmin,xmax,ymin,ymax)
        call PGBOX('BICNST',0.,0,'BICNST',0.,0)
        call PG_LineBuf(nc,x(sc,j),a(sc,j,ino))
        write(c4,'(i4)') j
        call PGMTEXT('T',0.5,0.5,0.5,c4)
      enddo
      return
      end


C----------------------------------------------------------------------
C (Re)Plot spectral data.
C   mode : 1 : Open plot window + 
C   mode : 2 : reset x limits +
C   mode : 3 : reset y limits +
C   mode : 4 : clear and replot.
C   mode : 5 : replot.
C
      subroutine plotspec(row,mode)

      implicit none
      integer*4 row,mode
      include 'spim2.inc'
      real*4 xlo,xhi,r,xmin0,xmax0,ylo,yhi
      real*4 dispersion,cx,cy,r1,r2
      integer*4 i,j,lc,color,other_color
      character c80*80, c2*2
      logical AtmAbsExist
C
      common /plotspecBLK/ dispersion
C

C Mode select.
      if (mode.eq.2) goto 2
      if (mode.eq.3) goto 3
      if (mode.eq.4) goto 4
      if (mode.eq.5) goto 5

C Open display.
1     continue
      call PGBEGIN(0,'/XDISP',1,1)
      call PGASK(.false.)
      call PGVSTAND
      call PGSLW(1)
      call PGSCF(1)
      call PGSCH(1.3)

C Reset x limits.
2     continue
C Absolute x limits.
      xlo  = w(sc(prim),row,prim)
      xhi  = w(ec(prim),row,prim)
      r    = max(xhi-xlo,1.e-20)
      xmin0= xlo - xex*r
      xmax0= xhi + xex*r
C Angstroms per pixel.
      dispersion = w(2,row,prim)-w(1,row,prim)
      xmin= xmin0
      xmax= xmax0

C Set y limits.
3     continue
      xmax = max(xmax,w(sc(prim)+2,row,prim))
      xmin = min(xmin,w(ec(prim)-2,row,prim))
      r    = (xmax+xmin)/2.
      xmax = max(xmax,r+2.*dispersion)
      xmin = min(xmin,r-2.*dispersion)
      if (yfix) goto 4
      xlo=xmin
      xhi=xmax
      call getylim(row,xlo,xhi,ylo,yhi)
      if (.not.hidebits) then
        if (row.gt.1) then
          call getylim(row-1,xlo,xhi,r1,r2)
          ylo = min(ylo,r1)
          yhi = max(yhi,r2)
        endif
        if (row.lt.nrw(prim)) then
          call getylim(row+1,xlo,xhi,r1,r2)
          ylo = min(ylo,r1)
          yhi = max(yhi,r2)
        endif
      endif
      r = max(yhi-ylo,1.e-20)
      ymin = ylo - 0.02*r
      ymax = yhi + 0.02*r
      cx = (xmin+xmax)/2.
      cy = (ymin+ymax)/2.
C Fix ymin at zero.
      if (yfixzero) ymin=0.

C Clear and replot.
4     continue
      call PGPAGE
      xmax = max(xmax,w(sc(prim)+2,row,prim))
      xmin = min(xmin,w(ec(prim)-2,row,prim))
      r    = (xmax+xmin)/2.
      xmax = max(xmax,r+2.*dispersion)
      xmin = min(xmin,r-2.*dispersion)
C Set window and box.
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBOX('BICNST',0.,0,'BICNST',0.,0)
C Replot.
5     continue
C Plot original data if modified.
      if ((.not.hideorig).and.(modified(prim))) then
        call PGSCI(15)      ! light gray
        call PlotIt(nc(prim),w(sc(prim),row,prim),
     .  fo(sc(prim),row,prim),hist)
        call PGSCI(1)
      endif
C Plot spectrum.
      if (ShowBin) then
        call PlotIt(ncb(prim),wb(sc(prim),row,prim),
     .  fb(sc(prim),row,prim),hist)
      else
        call PlotIt(nc(prim),w(sc(prim),row,prim),
     .  f(sc(prim),row,prim),hist)
      endif
C Plot error array.
      call PGSLS(4)
      if (ShowBin) then
        call PlotIt(ncb(prim),wb(sc(prim),row,prim),
     .  eb(sc(prim),row,prim),hist)
      else
        call PlotIt(nc(prim),w(sc(prim),row,prim),
     .  e(sc(prim),row,prim),hist)
      endif
      call PGSLS(1)
C Plot big spectrum.
      if (ncbigb.gt.0) then
        call PGSCI(4)
        call PlotIt(ncbigb,wwb,ffb,hist)
        call PGSCI(1)
      endif

C Write image, root and row on plot.
      if (ShowBin) then
        c2=' T'
      else
        c2=' F'
      endif
      write(c80,'(a,i2,3a,i3,a,i2)')
     .        'Prim=',prim,
     .        '  File= ',dfile(prim)(1:lc(dfile(prim))),
     .        '  Row=',row,
     .        '  Bin= ',bin
      if (ShowBin) c80=c80(1:lc(c80))//'  ShowBin'
      if (modified(prim)) c80=c80(1:lc(c80))//'  Modified'
      call PGMTEXT('B',2.5,0.5,0.5,c80)

C Indicate whether atmospheric absorption lines are present.
      if (AtmIm.gt.0) then
        r1=xmin-1.
        r2=xmax+1.
        if (AtmAbsExist(r1,r2)) then
          if (StarDivDone(row)) then
            call PGMTEXT('T',0.5,1.0,1.0,'Atm.Abs.*')
          else
            call PGMTEXT('T',0.5,1.0,1.0,'Atm.Abs.')
          endif
        endif
      endif

C Plot any other images.
      if (.not.hide) then
        color=1
        do i=1,nim
          if ((i.ne.prim).and.(ec(i).gt.0)) then
            color=color+1
            call PGSCI(color)
            if (ShowBin) then
              call PlotIt(ncb(i),wb(sc(i),row,i),fb(sc(i),row,i),hist)
            else
              call PlotIt(nc(i),w(sc(i),row,i),f(sc(i),row,i),hist)
            endif
          endif
        enddo
        call PGSCI(1)
      endif

C Plot any other bits of spectra from other orders.
      other_color=3
      if (.not.hidebits) then
        do i=1,nim
        if ((.not.hide).or.(i.eq.prim)) then
          do j=1,nrw(i)
            if (j.ne.row) then
              if ((w(sc(i),j,i).lt.xmax).and.
     .                       (w(ec(i),j,i).gt.xmin)) then
                if (i.eq.prim) then
                  color = 7           ! yellow
                else
                  other_color = other_color + 1
                  color = other_color
                endif
                call PGSCI(color)
                if (ShowBin) then
                  call PlotIt(ncb(i),wb(sc(i),j,i),fb(sc(i),j,i),hist)
                else
                  call PlotIt(nc(i),w(sc(i),j,i),f(sc(i),j,i),hist)
                endif
              endif
            endif
          enddo
        endif
        enddo
        call PGSCI(1)
      endif

C Plot zero if present.
      if (ymin.lt.0.) then
        call PGSCI(12)
        call PGSLS(3)
        call PG_HLine(xmin,xmax,0.)
        call PGSLS(1)
        call PGSCI(1)
      endif

      return
      end


C----------------------------------------------------------------------
C Interactive spectra-in-an-image plotting (version C for combining data).
C
      subroutine spim2(row)

      implicit none
      integer*4 row
C
      include 'spim2.inc'
C
      real*4 xmin0,xmax0
      real*4 cx,cy,r1,r2,r3,r4,w1,w2,r,diff,lo
      real*8 r8
C
      character key*1, dig1*1, dig2*1, c2*2
      integer*4 i,j,ikey,im1,im2,lc
      logical ok,replot,RePosCurs

C Re-position cursor.
      RePosCurs=.true.

C Set defaults.
      ShowBin = .false.
      hide    = .false.
      hidebits= .false.
      hideorig= .true.
      hist    = .false.
      yfix    = .false.
      yfixzero= .true.
      xex     = 0.60
      if (AtmIm.gt.0) xex=0.01
      if (ncbig.gt.0) then
        xex     = 0.01
        hide    =.true.
        hidebits=.true.
      endif
      do j=1,mrw
        ys(j)=1.
      enddo

C Initialize binning arrays.
      call rebin()

1     continue
      call plotspec(row,1)
      goto 6
2     continue
      call plotspec(row,2)
      goto 6
3     continue
      call plotspec(row,3)
      goto 6
4     continue
      call plotspec(row,4)
      goto 6
5     continue
      call plotspec(row,5)

C Get key stroke.
6     continue

C Maximum x range for this row.
      r1 = w(sc(prim),row,prim)
      r2 = w(ec(prim),row,prim)
      r3 = max(r2-r1,1.e-20)
      xmin0 = r1 - xex*r3
      xmax0 = r2 + xex*r3

      if (RePosCurs) then
        cx=(xmin+xmax)/2.
        cy=(ymin+ymax)/2.
        RePosCurs=.false.
      endif
      call PGCURS(cx,cy,key)
      ikey=ichar(key)
      if ((key.eq.'/').or.(key.eq.'?')) then
        print *,' '
        print '(a)','Switching between rows:'
        print *,' 5  : enter two digit row number.'
        print *,'+,= : increase row number.'
        print *,'-,_ : decrease row number.'
        print *,' 6  : enter a new one digit image number.'
        print *,' '
        print '(a)','Setting display:'
        print *,'i,I : zoom in (x).'
        print *,'o,O : zoom out (x).'
        print *,' p  : pan (center at cursor).'
        print *,' [  : pan left.'
        print *,' ]  : pan right.'
        print *,' y  : zoom out (y).'
        print *,' f  : fix y limits (no auto-scaling) [toggle].'
        print *,' F  : fix y minimum at zero [toggle].'
        print *,' h  : histogram mode [toggle].'
        print *,' w  : show whole plot.'
        print *,' u  : update (redraw) plot.'
        print *,'LMB : set x limits (Left Mouse Button).'
        print *,'MMB : set both x and y limits (Middle Mouse Button).'
        print *,'RMB : set y limits (Right Mouse Button).'
        print *,' 0  : Hide/show other images [toggle].'
        print *,' 1  : Hide/show bits of other images [toggle].'
        print *,' 2  : Hide/show original image [toggle].'
        print *,' '
        print '(a)','Rescaling:'
        print *,' S  : Rescale the orders to match each other.'
        print *,' $  : Scale current row.'
        print *,' @  : Experimental scaling...'
        print *,
     . ' M  : Match two images by dividing and fitting polynomial.'
        print *,
     . ' g/v: Scale/fit current row to match big spectrum(v=quick in).'
        print *,
     . ' G  : Scale/fit current row to match big spectrum (LL).'
        print *,' '
        print '(a)','Other:'
        print *,' j  : Change min_reject value (used in "g/v").'
        print *,' V  : Change poly.order for "v" option.'
        print *,
     .  ' J  : Create PostScript file "junk.ps" of current plot.'
        print *,'^J  : Same as "J" but also send junk.ps to printer.'
        print *,'?,/ : this help.'
        print *,' m  : Set a region to zero (mask).'
        print *,' z  : Zap a pixel (set it to zero.)'
        print *,' n  : Set a region to a given value.'
        print *,' r  : Restore a region.'
        print *,' b  : Choose new re-binning option.'
        print *,' x  : Change x extension from: ',xex
        print *,' %  : Correct for deviant pixels using error arrays.'
        print *,' W  : Read or write big spectrum.'
        print *,' C  : Close, then open PGPLOT display.'
        print *,
     .  '^F  : Fit polynomial through data and load into big spectrum.'
        print *,'^A  : Atmospheric Absorption Corrections.'
        print *,'ESC : exit.'
        goto 6

      elseif (ikey.eq.1) then      !  ^A  Atm. Absorp. Corrections.
        call AtmAbsCorr(row,replot)
        call rebin()
        print *,' Returning to main menu...'
        if (replot) goto 2
        goto 6

      elseif (key.eq.'g') then     ! Scale/fit current row to match big spec.
        i=0
        call Match_To_Big(row,i)
        call rebin()
        modified(prim)=.true.
        print *,'Returning to main menu...'
        goto 2

      elseif (key.eq.'v') then     ! Same as "g" but do (v_order) order fit.
        i=1
        call Match_To_Big(row,i)
        call rebin()
        modified(prim)=.true.
        print *,'Returning to main menu...'
        goto 2

      elseif (key.eq.'G') then     ! Scale/fit current row to match big spec.
        call Match_To_Big_ll(row)
        call rebin()
        modified(prim)=.true.
        goto 2

      elseif (ikey.eq.6) then      ! ^F : Fit polynomial through data.
        call spim2_polyfit(row)
        goto 2

      elseif (key.eq.'C') then     ! Close, then open PGPLOT display window.
        call PGEND
        print '(a,$)',
     .  ' PGPLOT display closed, hit return to reopen display.'
        read(5,'(a)') c2
        call PGBEGIN(0,'/XDISP',1,1)
        call PGASK(.false.)
        call PGVSTAND
        goto 1

      elseif (key.eq.'W') then     ! Read or write big spectrum.
771     print '(a,$)',' Read(1) or Write(2) : '
        read(5,*,err=771) i
        if (bigfile.ne.' ')
     .   print '(2a)',' Current big spectrum filename: ',
     .  bigfile(1:lc(bigfile))
772     print '(a,$)',' Enter filename : '
        read(5,'(a)',err=772) bigfile
        call AddFitsExt(bigfile)
        call rw_bigspec(i)
        goto 1

      elseif (key.eq.'M') then     ! Match images.
781     print '(a,$)',
     .      ' Enter image to be scaled and reference image number : '
        read(5,*,err=781) im1,im2
        call MatchImages(im1,im2)
        call rebin()
        goto 1

      elseif (key.eq.'x') then     ! Choose new x extension.
        print '(a,f8.4)',' Old x extension:',xex
702     print '(a,$)',' New x extension: '
        read(5,*,err=702) xex
        goto 2

      elseif (key.eq.'%') then     ! Correct deviant pixels.
703     print '(a,$)',
     .  ' Median box size to use for scaling (40 recomm.): '
        read(5,*,err=703) i
7035    print '(a,$)',' Threshold in sigmas (5.0 recomm.): '
        read(5,*,err=7035) r1
7037    print '(a,$)','           Row number (0=all rows): '
        read(5,*,err=7037) j
        print *,'Correct deviants in image: ',prim,i,r1,j
        call CorrectDeviants(prim,i,r1,j)
        modified(prim)=.true.
        call rebin()
        print *,'All done rejecting deviant pixels.'
        goto 4

      elseif (key.eq.'$') then     ! Scale current row.
        call spim_ScaleCurrentRow(row)
        goto 3

      elseif (key.eq.'@') then     ! Experimental scaling...
        call spim_RowScale(row)
        goto 3

      elseif (key.eq.'S') then     ! Rescale
704     print '(a,$)',
     .' Enter image to be scaled and reference image (0,0 to return): '
        read(5,*,err=704) im1,im2
        if ((im1.ne.0).or.(im2.ne.0)) then
          do i=1,nrw(im1)
            ys(i)= 1.0
          enddo
        endif
        call RescaleRefit(im1,im2,ok)
        if (ok) then
          call RescaleApply(im1)
          call rebin()
        endif
        goto 1

      elseif (key.eq.'b') then     ! Choose new re-binning.
        print *,'Enter new binning in plot window.'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        bin = nint(r8)
        if ((bin.lt.1).or.(bin.gt.99)) then
          print *,'ERROR- binning must be between 1 and 99.'
        else
          call rebin()
          ShowBin = .true.
          if (bin.eq.1) ShowBin=.false.
        endif
        goto 4

      elseif (key.eq.'V') then     ! Choose new v_order
        print *,
     . 'Enter new "v" option default polynomial order in plot window.'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        v_order = nint(r8)
        print *,'New v_order=',v_order
        goto 6

      elseif (key.eq.'r') then     ! Restore region.
        r1 = max(xmin0,min(xmax0,cx))
        print *,
     .  ' Select second limit for restore region ("a" to abort)...'
        call PGCURS(cx,cy,key)
        if (key.eq.'a') goto 4
        r2 = max(xmin0,min(xmax0,cx))
        w1 = min(r1,r2)
        w2 = max(r1,r2)
        do i=sc(prim),ec(prim)
          if ((w(i,row,prim).gt.w1).and.(w(i,row,prim).lt.w2)) then
            f(i,row,prim)=fo(i,row,prim)
            e(i,row,prim)=eo(i,row,prim)
          endif
        enddo
        modified(prim) = .true.
        call rebin()
        goto 3

      elseif (key.eq.'j') then     ! Mask region.
        min_reject = 0.0
        print '(a)','"min_reject" has been reset.  You will be asked'
        print '(a)','for a new min_reject value on next "g or v".'
        goto 6

      elseif (key.eq.'m') then     ! Mask region.
        r1 = max(xmin0,min(xmax0,cx))
        call PGSCI(8)
        call PG_VLine(ymin,ymax,r1)
        print *,' Select second limit for mask ("a" to abort)...'
        call PGCURS(cx,cy,key)
        if (key.eq.'a') goto 4
        r2 = max(xmin0,min(xmax0,cx))
        call PG_VLine(ymin,ymax,r2)
        call PG_HLine(r1,r2,(ymin+ymax)/2.)
        w1 = min(r1,r2)
        w2 = max(r1,r2)
        do i=sc(prim),ec(prim)
          if ((w(i,row,prim).gt.w1).and.(w(i,row,prim).lt.w2)) then
            f(i,row,prim)=0.
            e(i,row,prim)=-1.
          endif
        enddo
        modified(prim) = .true.
        call rebin()
        call PGSCI(1)
        goto 6

      elseif (key.eq.'z') then     ! zap pixel.
        w1 = max(xmin0,min(xmax0,cx))
        lo = 1.e+30
        j  = 0
        do i=sc(prim),ec(prim)
          diff = abs( w1 - w(i,row,prim) )
          if (diff.lt.lo) then
            lo= diff
            j = i
          endif
        enddo
        if (j.gt.0) then
          f(j,row,prim)=0.
          e(j,row,prim)=-1.
          modified(prim) = .true.
          call PGSCI(3)
          w1 = w(j,row,prim)
          call PG_Mark(w1,ymin,ymax)
          call PGSCI(1)
        else
          print *,'Error- Nothing zapped.'
        endif
        call rebin()
        goto 6

      elseif (key.eq.'n') then     ! Set region to a value.
        r1 = max(xmin0,min(xmax0,cx))
        print *,' Select second limit for value set ("a" to abort)...'
        call PGCURS(cx,cy,key)
        if (key.eq.'a') goto 4
        print '(a,$)',  '  Specify value for data : '
        read(5,*,err=1) r3
        if (efile(prim).ne.' ') then
          print '(a,$)',' Specify value for error : '
          read(5,*,err=1) r4
        else
          r4=0.
        endif
        r2 = max(xmin0,min(xmax0,cx))
        w1 = min(r1,r2)
        w2 = max(r1,r2)
        do i=sc(prim),ec(prim)
          if ((w(i,row,prim).gt.w1).and.(w(i,row,prim).lt.w2)) then
            f(i,row,prim)=r3
            e(i,row,prim)=r4
          endif
        enddo
        modified(prim)=.true.
        call rebin()
        goto 3

      elseif (key.eq.'2') then   ! hide original image (toggle)
        call Toggle(hideorig)
        print *,'hideorig=',hideorig
        goto 4

      elseif (key.eq.'1') then   ! hide bits of other images (toggle)
        call Toggle(hidebits)
        print *,'hidebits=',hidebits
        goto 4

      elseif (key.eq.'0') then   ! hide other images (toggle)
        call Toggle(hide)
        print *,'hide=',hide
        goto 4

      elseif ((key.eq.'J').or.(ikey.eq.10)) then   ! create PS file junk.ps
        call PGEND
        call PGBEGIN(0,'junk.ps/PS',1,1)
        call PGASK(.false.)
        call PGVSTAND
        call PGSLW(1)
        call PGSCF(1)
        call PGSCH(1.0)
        call PGPAGE
        call PGWINDOW(xmin,xmax,ymin,ymax)
        call PGBOX('BICNST',0.,0,'BICNST',0.,0)
        if (ShowBin) then
         call PlotIt(ncb(prim),wb(sc(prim),row,prim),
     .  fb(sc(prim),row,prim),hist)
        else
         call PlotIt(nc(prim),w(sc(prim),row,prim),
     .  f(sc(prim),row,prim),hist)
        endif
        call PGEND
        if (ikey.eq.10) then
          call system('/usr/ucb/lpr junk.ps')
          print *,'Sent PostScript file "junk.ps" to printer.'
        else
          print *,
     .  'Created PostScript file "junk.ps" in default directory.'
        endif
        call PGBEGIN(0,'/XDISP',1,1)
        call PGASK(.false.)
        call PGVSTAND
        call PGSLW(1)
        call PGSCF(1)
        call PGSCH(1.3)
        goto 4

      elseif (key.eq.'w') then     ! reset x and y limits.
        goto 2

      elseif (key.eq.'u') then     ! redraw (update) plot.
        goto 3

      elseif (key.eq.'h') then     ! histogram mode (toggle)
        call Toggle(hist)
        print *,'hist=',hist
        goto 4

      elseif (key.eq.'f') then     ! fix y limits (toggle)
        call Toggle(yfix)
        print *,'yfix=',yfix
        goto 6

      elseif (key.eq.'F') then     ! fix y zero (toggle)
        call Toggle(yfixzero)
        print *,'yfixzero=',yfixzero
        goto 3

      elseif (key.eq.'p') then     ! pan center
        r = (xmax-xmin)/2.
        xmin = max(xmin0,cx-r)
        xmax = min(xmax0,cx+r)
        cx = (xmax+xmin)/2.
        goto 3

      elseif (key.eq.'[') then     ! pan left
        r = xmax-xmin
        xmin = max(xmin0,xmin-r)
        xmax = xmin + r
        cx = (xmax+xmin)/2.
        RePosCurs=.true.
        goto 3

      elseif (key.eq.']') then     ! pan right
        r = xmax-xmin
        xmax = min(xmax0,xmax+r)
        xmin = xmax - r
        cx = (xmax+xmin)/2.
        RePosCurs=.true.
        goto 3

      elseif (key.eq.'i') then     ! zoom in
        r = (xmax-xmin)/8.
        xmin= max(xmin0,cx-r)
        xmax= min(xmax0,cx+r)
        goto 3

      elseif (key.eq.'I') then     ! zoom in more
        r = (xmax-xmin)/16.
        xmin= max(xmin0,cx-r)
        xmax= min(xmax0,cx+r)
        goto 3

      elseif (key.eq.'o') then     ! zoom out
        r = (xmax-xmin)*2.
        xmin= max(xmin0,cx-r)
        xmax= min(xmax0,cx+r)
        goto 3

      elseif (key.eq.'O') then     ! zoom out more
        r = (xmax-xmin)*4.
        xmin= max(xmin0,cx-r)
        xmax= min(xmax0,cx+r)
        goto 3

      elseif (key.eq.'y') then     ! zoom out in y
        r = (ymax-ymin)
        ymin = ymin - r
        ymax = ymax + r
        if (yfixzero) ymin=0.
        goto 4
          
      elseif (key.eq.'A') then     ! Left Mouse Button (select x limits)
        r1 = max(xmin0,min(xmax0,cx))
        print *,' Select second x limit...'
        call PGCURS(cx,cy,key)
        r2 = max(xmin0,min(xmax0,cx))
        xmin = min(r1,r2)
        xmax = max(r1,r2)
        cx = (xmax+xmin)/2.
        cy = (ymax+ymin)/2.
        goto 3

      elseif (key.eq.'D') then     ! Middle Mouse Button (select both limits)
        r1 = max(xmin0,min(xmax0,cx))
        r3=cy
        print *,' Select second x,y limit...'
        call PGCURS(cx,cy,key)
        r2 = max(xmin0,min(xmax0,cx))
        xmin = min(r1,r2)
        xmax = max(r1,r2)
        ymin = min(r3,cy)
        ymax = max(r3,cy)
        cx = (xmax+xmin)/2.
        cy = (ymax+ymin)/2.
        goto 4
 
      elseif (key.eq.'X') then     ! Right Mouse Button (select y limits)
        r=cy
        print *,' Select second y limit...'
        call PGCURS(cx,cy,key)
        ymin = min(r,cy)
        ymax = max(r,cy)
        cx = (xmax+xmin)/2.
        cy = (ymax+ymin)/2.
        goto 4
         
      elseif (ikey.eq.27) then     ! ESC = quit
        print *,'Exiting...'
        goto 800
      
      elseif ((key.eq.'-').or.(key.eq.'_')) then
        row=row-1
        if (row.lt.1) then
          print *,'You are on the lowest row.'
          row=1
          goto 6
        endif
        RePosCurs=.true.
        goto 2
        
      elseif ((key.eq.'+').or.(key.eq.'=')) then
        row=row+1
        if (row.gt.nrw(prim)) then
          print *,'You are on the highest row.'
          row=nrw(prim)
          goto 6
        endif
        RePosCurs=.true.
        goto 2

      elseif (key.eq.'6') then
707     print '(a)',
     .  'Enter one digit new primary image number in plot window.'
        call PGCURS(cx,cy,dig2)
        c2='0'//dig2
        read(c2,'(i2)',err=707) prim
        prim = max(1,min(nim,prim))
        goto 2
        
      elseif (key.eq.'5') then
708     print '(a)','Enter two digit row number in plot window.'
        call PGCURS(cx,cy,dig1)
        call PGCURS(cx,cy,dig2)
        c2=dig1//dig2
        read(c2,'(i2)',err=708) row
        if (row.lt.1) then
          print *,'The lowest row is 1'
          row=1
          goto 6
        elseif (row.gt.nrw(prim)) then
          print *,'The highest row is',nrw(prim)
          row=nrw(prim)
          goto 6
        endif
        RePosCurs=.true.
        goto 2

      else
        print *,'Try again.'
        goto 6

      endif

800   continue
      call PGEND
      return
      end

C----------------------------------------------------------------------
C Rebin data.
C
      subroutine rebin()
C
      implicit none
C
      include 'spim2.inc'
C
      integer*4 i,n,num,im,ii,row
      real*4 fsum,vsum
      real*8 w8sum
      logical mask,zero
      do im=1,nim
        do row=1,nrw(im)
          n=sc(im)-1
          do i=sc(im),ec(im),bin
            zero =.false.
            mask =.false.
            fsum = 0.
            vsum = 0.
            w8sum= 0.d0
            num  = 0
            do ii=min(ec(im),i),min(ec(im),i+(bin-1))
              fsum = fsum + f(ii,row,im)
              if (abs(f(ii,row,im)).lt.1.e-30) zero=.true.
              vsum = vsum + (e(ii,row,im)*e(ii,row,im))
              if (e(ii,row,im).lt.1.e-30) mask=.true.
              w8sum= w8sum+ wo8(ii,row,im)
              num  = num  + 1
            enddo
            if (num.gt.0) then
              n=n+1
              fb(n,row,im)= fsum / float(num)
              eb(n,row,im)= sqrt( vsum / float(num*num) )
              wb(n,row,im)= sngl( w8sum / dfloat(num) )
            endif
            if (mask) eb(n,row,im)=-1.0
            if (zero) fb(n,row,im)= 0.0
          enddo
          scb(im) = sc(im)
          ecb(im) = n
          ncb(im) = 1 + ecb(im) - scb(im)
        enddo
      enddo
      n=0 
      do i=1,ncbig,bin
        fsum = 0.
        w8sum= 0.d0
        num  = 0
        do ii=min(ncbig,i),min(ncbig,i+(bin-1))
          fsum = fsum + ff(ii)
          w8sum= w8sum+ dble(ww(ii))
          num  = num  + 1
        enddo
        if (num.gt.0) then
          n=n+1
          ffb(n) = fsum / float(num)
          wwb(n) = sngl( w8sum / dfloat(num) )
        endif
      enddo
      ncbigb = n
      return
      end

C----------------------------------------------------------------------
      subroutine PlotIt(n,x,y,hist)
      implicit none
      integer*4 n
      real*4 x(*),y(*)
      logical hist
      if (hist) then
        call PG_BinBuf(n,x,y,.true.)
      else
        call PG_LineBuf(n,x,y)
      endif
      return
      end

C----------------------------------------------------------------------
      subroutine Toggle(x)
      implicit none
      logical x
      if (x) then
        x = .false.
      else
        x = .true.
      endif
      return
      end


C----------------------------------------------------------------------
C Match one image to another.
C Divide im2 by im1 and fit polynomial, then scale im1.
C
      subroutine MatchImages(im1,im2)
C
      implicit none
      integer*4 im1,im2
C
      include 'spim2.inc'
C
      real*8 wave,polyval,polyvaloff,high,wrms,wppd,coef(0:99)
      real*8 wave1,wave2,wav,xoff
      real*8 r1,r2,r3,r4,crval1,cdelt1,xp,x8lo,x8hi
      real*8 fhead,r8,w1,w2,thres,f8
      real*4 xx(90000),yy(90000)
      real*4 median,yhi,sum,xlo,xhi
      integer*4 row,i,j,k,n8,num,col,n,lc,i1,i2
      integer*4 narr,order,nn,option
      character c1*1, file*80
      logical ok

C Go through each pixel.
C Find ratio between image 1 and image 2 for "similar" pixels.
      nn = 0
      do row=1,nrw(im1) 
C Find median in this row.
        narr=0
        do col=sc(im1),ec(im1)
          narr=narr+1
          arr(narr) = f(col,row,im1)
        enddo
        call find_median(arr,narr,median)
C Find ratio for each column.
        do col=sc(im1),ec(im1)
          if (e(col,row,im1).gt.0.) then
            wave = w(col,row,im1)
C Initialize sum.
            sum = 0.
            num = 0
C Find pixel from other image.
            do j=1,nrw(im2)
              k = nint(polyval(iord(j,im2)+1,icof(0,j,im2),dble(wave)))
C Is this pixel within row?
              if ((k.gt.sc(im2)).and.(k.lt.ec(im2))) then
C Is it not masked?
                if (e(k,j,im2).gt.0.) then
C Is it larger than half the median value (avoid absorption troughs)?
                  if (f(col,row,im1).gt.median/2.) then
                    sum = sum + ( f(k,j,im2) / f(col,row,im1) )
                    num = num + 1
                  endif
                endif
              endif
            enddo
C Add this wavelength point to array.
            if ((num.gt.0).and.(sum.gt.0.)) then
              nn = nn + 1
              xx(nn) = wave
              yy(nn) = sum / float(num)
            endif
          endif
        enddo
      enddo
C Median binning of points.
      n8=0
      do i=1,nn-41,41 
        n8=n8+1
C First get x value.
        narr=0
        do j=i,i+40
          narr=narr+1
          arr(narr)=xx(j)
        enddo
        call find_median(arr,narr,median)
        x8(n8) = dble(median)
C Now get y value.
        narr=0
        do j=i,i+40
          narr=narr+1
          arr(narr)=yy(j)
        enddo
        call find_median(arr,narr,median)
        y8(n8) = dble(median)
        z8(n8) = 1.d0
      enddo
C Show array and fit polynomial.
C Initial order.
      order = -1
C Load array for plotting.
1     continue
      xlo=+1.e+30
      xhi=-1.e+30
      yhi=-1.e+30
      nn=0
      do i=1,n8
        if (z8(i).gt.0.) then
          nn=nn+1
          xx(nn) = sngl(x8(i))
          yy(nn) = sngl(y8(i))
          if (yy(nn).gt.yhi) yhi=yy(nn)
          if (xx(nn).lt.xlo) xlo=xx(nn)
          if (xx(nn).gt.xhi) xhi=xx(nn)
        endif
      enddo
      xmin = xlo
      xmax = xhi
C OVERRIDE-- Based on total wavelength range.
      xmin = min(xlo,w(sc(im1),1,im1))
      xmax = max(xhi,w(ec(im1),nrw(im1),im1))
      ymin = 0.
      ymax = yhi*1.03
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBOX('BCNST',0.,0,'BCNST',0.,0)
      call PGSLW(1)
      call PGPT(nn,xx,yy,-1)
C Draw polynomial curve over data points.
      if (order.gt.-1) then
        nn=0
        do i=1,100
          nn=nn+1
          xx(nn)= xmin + (float(i)*(xmax-xmin)/100.)
          yy(nn)= sngl(polyvaloff(order+1,coef,dble(xx(nn)),xoff))
        enddo
        call PGSCI(2)
        call PGLINE(nn,xx,yy)
        call PGSCI(1)
      endif
C Options.
2     continue
      print *,'Options: (1) Fit polynomial.'
      print *,'         (2) Throw out deviant points.'
      print *,'         (3) Multiply image 1 by fit (and exit.)'
      print *,'         (4) Exit with no rescaling.'
      print *,
     .'         (5) Throw out points in a given wavelength region.'
      print *,'         (6) Throw out points in box.'
      print *,'         (7) Add an artificial point.'
      print *,'         (8) Write out points as a FITS spectrum.'
      print *,
     .'         (9) Read in FITS spectrum and rescale data (and exit.)'
      print *,'         (0) Close, then reopen PGPLOT display window.'
      print '(a,$)',' Enter option : '
      read(5,*,err=2) option

      if (option.eq.0) then
        call PGEND
        print '(a,$)',
     .  ' PGPLOT display closed, hit return to reopen display.'
        read(5,'(a)') c1
        call PGBEGIN(0,'/XDISP',1,1)
        call PGASK(.false.)
        call PGVSTAND
        goto 1

C Fit polynomial.
      elseif (option.eq.1) then
        print '(a,$)',' Enter polynomial order (0 to 18) : '
        read(5,*,err=2) order
        order = max(0,min(18,order))
        call poly_fit_gj(n8,x8,y8,z8,order,coef,xoff,0,ok)
        if (.not.ok) then
          print *,'Error fitting polynomial.'
          goto 2
        endif
        call poly_fit_gj_residuals(n8,x8,y8,z8,order,coef,
     .                             xoff,high,wrms,wppd)
        print *,'high,wrms,wppd=',sngl(high),sngl(wrms),sngl(wppd)
        goto 1

C Reject points based on threshold.
      elseif (option.eq.2) then
        if (order.gt.-1) then
          print '(a,$)',' Enter threshold in sigmas : '
          read(5,*,err=2) thres
          do i=1,n8
            f8 = polyvaloff(order+1,coef,x8(i),xoff)
            if (abs(y8(i)-f8)/wrms.gt.thres) z8(i)=0.d0
          enddo
        else
          print *,'Fit polynomial first.'
        endif
        goto 2

C Rescale image and exit.
      elseif (option.eq.3) then
        if (order.lt.0) then
          print *,'Fit polynomial first.'
          goto 2
        endif
        do row=1,nrw(im1)
          do i=sc(im1),ec(im1)
            f8 = polyvaloff(order+1,coef,dble(w(i,row,im1)),xoff)
            f(i,row,im1) = f(i,row,im1) * sngl(f8)
            if (e(i,row,im1).gt.0.) then
              e(i,row,im1) = e(i,row,im1) * sngl(f8)
            endif
          enddo
        enddo
        modified(im1) = .true.
        print *,'All done with rescaling.'

C Add artificial point.
      elseif (option.eq.7) then
        print '(a,$)',' Enter wave, value, and weight : '
        read(5,*) r1,r2,r3
        n8=n8+1
        x8(n8)=r1
        y8(n8)=r2
        z8(n8)=r3
        print '(i3,3f9.3)',n8,x8(n8),y8(n8),z8(n8)
        goto 1

C Delete points in a box.
      elseif (option.eq.6) then
        print '(a,$)',' Enter xmin, xmax, ymin, ymax : '
        read(5,*) r1,r2,r3,r4
        do i=1,n8
          if ((x8(i).gt.r1).and.(x8(i).lt.r2).and.
     .        (y8(i).gt.r3).and.(y8(i).lt.r4)) z8(i)=0.d0
        enddo
        goto 2

C Exit without rescaling.
      elseif (option.eq.4) then
        print *,'Exiting without rescaling.'

C Delete points in wavelength range.
      elseif (option.eq.5) then
        print '(a,$)',' Enter starting wavelength : '
        read(5,*,err=2) wave1
        print '(a,$)',' Enter ending wavelength : '
        read(5,*,err=2) wave2
        do i=1,n8
          if ((x8(i).gt.wave1).and.(x8(i).lt.wave2)) z8(i)=0.d0
        enddo
        goto 2

C Write out FITS spectrum.
      elseif (option.eq.8) then
        print '(a,$)',' Enter filename : '
        read(5,'(a)') file
        call AddFitsExt(file)
        x8lo= wo8(sc(im1),1,im1) - 1.d0
        x8hi= wo8(ec(im1),nrw(im1),im1) + 1.d0
        crval1 = x8lo
        cdelt1 = 1.d0
        xp = crval1
C Clear and load arr() array.
        do i=1,9000
          arr(i)=0.
        enddo
        n=0
        do while(xp.lt.x8hi)
          sum=0.
          num=0
          do i=1,n8
            if (abs(x8(i)-xp).lt.cdelt1/2.) then
              sum = sum + sngl(y8(i))
              num = num + 1
            endif
          enddo
          n=n+1
          if (num.gt.0) arr(n)=sum/float(num)
          xp=xp+cdelt1
        enddo
        call make_basic_header(temphead,1,n,0,1)
        call fheadset('CRVAL1',crval1,temphead)
        call fheadset('CDELT1',cdelt1,temphead)
        call cheadset('CTYPE1','LAMBDA',temphead)
        call inheadset('OBSNUM',0,temphead)
        call inheadset('EXPOSURE',0,temphead)
        print '(2a)',' Writing : ',file(1:lc(file))
        call writefits(file,arr,temphead,ok)
        goto 2

C Read FITS spectrum, rescale image and exit.
      elseif (option.eq.9) then
        print '(a,$)',' Enter filename : '
        read(5,'(a)') file
        call AddFitsExt(file)
        call readfits(file,arr,marr,temphead,ok)
        crval1 = fhead('CRVAL1',temphead)
        cdelt1 = fhead('CDELT1',temphead)
        do row=1,nrw(im1)
          do i=sc(im1),ec(im1)
            wav= wo8(i,row,im1)
            k = 1 + nint( ( wav - crval1 ) / cdelt1 )
            r8= crval1 + ( dfloat(k-1)*cdelt1 )
            if (r8.lt.wav) then
              i1 = k
              i2 = k+1
            else
              i1 = k-1
              i2 = k
            endif
            if ((arr(i1).lt.1.e-30).or.(arr(i2).lt.1.e-30)) then
              print *,'ERROR- scale factor too small at wave=',wav
              call exit(1)
            endif
            w1 = crval1 + ( dfloat(i1-1)*cdelt1 )
            w2 = crval1 + ( dfloat(i2-1)*cdelt1 )
            f8=(((w2-wav)*dble(arr(i1)))+
     .                ((wav-w1)*dble(arr(i2))))/(w2-w1)
            f(i,row,im1) = f(i,row,im1) * sngl(f8)
            if (e(i,row,im1).gt.0.) then
              e(i,row,im1) = e(i,row,im1) * sngl(f8)
            endif
          enddo
        enddo
        modified(im1) = .true.
        print *,'All done with rescaling.'

      else
        goto 2

      endif
      return
      end


C----------------------------------------------------------------------
C Rescale orders in im1 to match.  Use im2 as a reference.
C "fixedrow" is a row which does not change.
C Go to increasing rows from fixedrow.
C The rows and scale factors are recorded in ys().
 
      subroutine RescaleOrders_Up(fixedrow,im1,im2)
 
      implicit none
      integer*4 fixedrow,im1,im2
      include 'spim2.inc'
      integer*4 i,jlo,row,num,k,j
      real*4 sum1,sum2,sum3,sum4,sum5,sum6,wsum,s2n,wt
      real*4 cpw,diff,lodiff,scale,wvrad,wvpt,wdf,apxdsp
      integer*4 npix_wvrad,neari
 
C Set wavelength radius for scaling zone.
11    print '(a)',' Set wavelength radius for scaling zone.'
      print '(a,$)',' npix_wvrad (100 sugg.)= '
      read(5,*,err=11) npix_wvrad

 
C Do rows above fixedrow.
      DO row=fixedrow+1,nrw(im1)
 
C Approximate dispersion.
        apxdsp= (w(ec(im1),row,im1)-w(sc(im1),row,im1))/
     .                                    float(ec(im1)-sc(im1))
        wvrad = float(npix_wvrad) * apxdsp

C Work on left edge of order.
C Center point wavelength between ends of orders.
        cpw = ( w(ec(im1),row-1,im1) + w(sc(im1),row,im1) ) / 2.
C Wave difference between end points.  Positive means overlap.
        wdf = w(ec(im1),row-1,im1) - w(sc(im1),row,im1)
C Find a row in im2 whose center is close to "cpw".
        lodiff= 1.e+30
        jlo   = 0
        do j=1,nrw(im2)
          diff= abs(cpw-w((sc(im2)+ec(im2))/2,j,im2))
          if (diff.lt.lodiff) then
            lodiff= diff
            jlo   = j
          endif
        enddo
        if (jlo.eq.0) goto 809
 
C Find means for overlap with previous row.
        sum1 = 0.
        sum2 = 0.
        wsum = 0.
        num  = 0
        if (wdf.gt.2.*wvrad) then
          wvpt = cpw
        else
          wvpt = w(ec(im1),row-1,im1)
        endif
C Look at all pixels in image two that are within wvrad of wvpt.
        do i=sc(im2),ec(im2)
        if (abs(w(i,jlo,im2)-wvpt).lt.wvrad) then
C Find closest corresponding point in image one.
          k = neari(w(i,jlo,im2),ec(im1),w(1,row-1,im1))
C Is this point within image one?
          if ((k.gt.sc(im1)).and.(k.lt.ec(im1))) then
C Is this a valid point in both images?
            if ((e(k,row-1,im1).gt.0.).and.(e(i,jlo,im2).gt.0.)) then
              s2n  = f(k,row-1,im1)/e(k,row-1,im1) + 
     .                         f(i,jlo,im2)/e(i,jlo,im2)
              s2n  = max(1.e-10,s2n)
              wt   = s2n*s2n
              sum1 = sum1 + wt * f(k,row-1,im1) * ys(row-1)
              sum2 = sum2 + wt * f(i,jlo,im2)
              wsum = wsum + wt
              num  = num  + 1
            endif
          endif
        endif
        enddo
        if (num.lt.10) then
          print *,'1. row-1,jlo,num=',row-1,jlo,num
          goto 801
        endif
        sum1 = sum1 / wsum
        sum2 = sum2 / wsum
 
C Find means for overlap with this row.
        sum3 = 0.
        sum4 = 0.
        wsum = 0.
        num  = 0
        if (wdf.gt.2.*wvrad) then
          wvpt = cpw
        else
          wvpt = w(sc(im1),row,im1)
        endif
C Look at each column in image two which is within wvrad of wvpt.
        do i=sc(im2),ec(im2)
        if (abs(w(i,jlo,im2)-wvpt).lt.wvrad) then
C Find closest corresponding point in image one.
          k = neari(w(i,jlo,im2),ec(im1),w(1,row,im1))
C Is this point within image one?
          if ((k.gt.sc(im1)).and.(k.lt.ec(im1))) then
C Is this a valid point in both images?
            if ((e(k,row,im1).gt.0.).and.(e(i,jlo,im2).gt.0.)) then
              s2n  = f(k,row,im1)/e(k,row,im1) + 
     .                       f(i,jlo,im2)/e(i,jlo,im2)
              s2n  = max(1.e-10,s2n)
              wt   = s2n*s2n
              sum3 = sum3 + wt * f(k,row,im1) * ys(row)
              sum4 = sum4 + wt * f(i,jlo,im2)
              wsum = wsum + wt
              num  = num  + 1
            endif
          endif
        endif
        enddo
        if (num.lt.10) then
          print *,'2. row,jlo,num=',row,jlo,num
          goto 801
        endif
        sum3 = sum3 / wsum
        sum4 = sum4 / wsum
 
C Find means for overlap with previous row in the same image (direct overlap.)
        sum5 = 0.
        sum6 = 0.
        wsum = 0.
        num  = 0
        wvpt = cpw
        do i=sc(im1),ec(im1)
        if (abs(w(i,row,im1)-wvpt).lt.wvrad) then
          k = neari(w(i,row,im1),ec(im1),w(1,row-1,im1))
          if ((k.gt.sc(im1)).and.(k.lt.ec(im1))) then
            if ((e(i,row,im1).gt.0.).and.(e(k,row-1,im1).gt.0.)) then
              s2n  = f(i,row,im1)/e(i,row,im1) + 
     .                        f(k,row-1,im1)/e(k,row-1,im1)
              s2n  = max(1.e-10,s2n)
              wt   = s2n*s2n
              sum5 = sum5 + wt * f(i,row,im1) * ys(row)
              sum6 = sum6 + wt * f(k,row-1,im1) * ys(row-1)
              wsum = wsum + wt
              num  = num  + 1
            endif
          endif
        endif
        enddo
        if (num.lt.10) then
          sum5 = 0.
          sum6 = 0.
        else
          sum5 = sum5 / wsum
          sum6 = sum6 / wsum
        endif
 
C Rescale the row.
        scale = ( sum1 / sum2 ) * ( sum4 / sum3 )
C Factor in the direct overlap if it exists.
        if ((sum5.gt.0.).and.(sum6.gt.0.)) then
          scale = ( scale + ( sum6 / sum5 ) ) / 2.
        endif
        print *,'Row: ',row,'  Relative scale:',scale
C This scale is relative to old scaling.
C Apply old scale to create absolute scale.
        scale = scale * ys(row)
        print *,'Row: ',row,'       New scale:',scale
        ys(row) = scale
 
      ENDDO
 
      return
 
801   print *,'No overlap region, no more rescaling possible.'
      return
809   print *,'Horrible error in RescaleOrders_Up.'
      return
      end
 
C----------------------------------------------------------------------
C Rescale orders in im1 to match.  Use im2 as a reference.
C "fixedrow" is a row which does not change.
C Go to decreasing rows from fixedrow.
C The rows and scale factors are recorded in ys().
 
      subroutine RescaleOrders_Down(fixedrow,im1,im2)
 
      implicit none
      integer*4 fixedrow,im1,im2
      include 'spim2.inc'
      integer*4 i,jlo,row,num,k,j
      real*4 sum1,sum2,sum3,sum4,sum5,sum6,wsum,s2n,wt
      real*4 cpw,diff,lodiff,scale,wvrad,wvpt,wdf,apxdsp
      integer*4 npix_wvrad,neari
 
C Set wavelength radius for scaling zone.
11    print '(a)',' Set wavelength radius for scaling zone.'
      print '(a,$)',' npix_wvrad (100 sugg.)= '
      read(5,*,err=11) npix_wvrad
 
C Do rows below fixedrow.
      DO row=fixedrow-1,1,-1
 
C Approximate dispersion.
        apxdsp= (w(ec(im1),row,im1)-w(sc(im1),row,im1))/
     .                                   float(ec(im1)-sc(im1))
        wvrad = float(npix_wvrad) * apxdsp

C Work on left edge of order.
C Center point wavelength between ends of orders.
        cpw = ( w(ec(im1),row,im1) + w(sc(im1),row+1,im1) ) / 2.
C Wave difference between end points.  Positive means overlap.
        wdf = w(ec(im1),row,im1) - w(sc(im1),row+1,im1)
C Find a row in im2 whose center is close to "cpw".
        lodiff= 1.e+30
        jlo   = 0
        do j=1,nrw(im2)
          diff= abs(cpw-w((sc(im2)+ec(im2))/2,j,im2))
          if (diff.lt.lodiff) then
            lodiff= diff
            jlo   = j
          endif
        enddo
        if (jlo.eq.0) goto 809
 
C Find means of this row and in image two.
        sum1 = 0.
        sum2 = 0.
        wsum = 0.
        num  = 0
        if (wdf.gt.2.*wvrad) then
          wvpt = cpw
        else
          wvpt = w(ec(im1),row,im1)
        endif
C Look at all pixels in image two that are within wvrad of wvpt.
        do i=sc(im2),ec(im2)
        if (abs(w(i,jlo,im2)-wvpt).lt.wvrad) then
C Find closest corresponding point in image one.
          k = neari(w(i,jlo,im2),ec(im1),w(1,row,im1))
C Is this point within image one?
          if ((k.gt.sc(im1)).and.(k.lt.ec(im1))) then
C Is this a valid point in both images?
            if ((e(k,row,im1).gt.0.).and.(e(i,jlo,im2).gt.0.)) then
              s2n  = f(k,row,im1)/e(k,row,im1) + 
     .                       f(i,jlo,im2)/e(i,jlo,im2)
              s2n  = max(1.e-10,s2n)
              wt   = s2n*s2n
              sum1 = sum1 + wt * f(k,row,im1) * ys(row)
              sum2 = sum2 + wt * f(i,jlo,im2)
              wsum = wsum + wt
              num  = num  + 1
            endif
          endif
        endif
        enddo
        sum1 = sum1 / wsum
        sum2 = sum2 / wsum
 
C Find means for overlap with next row.
        sum3 = 0.
        sum4 = 0.
        wsum = 0.
        num  = 0
        if (wdf.gt.2.*wvrad) then
          wvpt = cpw
        else
          wvpt = w(sc(im1),row+1,im1)
        endif
C Look at each column in image two which is within wvrad of wvpt.
        do i=sc(im2),ec(im2)
        if (abs(w(i,jlo,im2)-wvpt).lt.wvrad) then
C Find closest corresponding point in image one.
          k = neari(w(i,jlo,im2),ec(im1),w(1,row+1,im1))
C Is this point within image one?
          if ((k.gt.sc(im1)).and.(k.lt.ec(im1))) then
C Is this a valid point in both images?
            if ((e(k,row+1,im1).gt.0.).and.(e(i,jlo,im2).gt.0.)) then
              s2n  = f(k,row+1,im1)/e(k,row+1,im1) + 
     .                          f(i,jlo,im2)/e(i,jlo,im2)
              s2n  = max(1.e-10,s2n)
              wt   = s2n*s2n
              sum3 = sum3 + wt * f(k,row+1,im1) * ys(row+1)
              sum4 = sum4 + wt * f(i,jlo,im2)
              wsum = wsum + wt
              num  = num  + 1
            endif
          endif
        endif
        enddo
        sum3 = sum3 / wsum
        sum4 = sum4 / wsum
 
C Find means for overlap with previous row in the same image (direct overlap.)
        sum5 = 0.
        sum6 = 0.
        wsum = 0.
        num  = 0
        wvpt = cpw
        do i=sc(im1),ec(im1)
        if (abs(w(i,row+1,im1)-wvpt).lt.wvrad) then
          k = neari(w(i,row+1,im1),ec(im1),w(1,row,im1))
          if ((k.gt.sc(im1)).and.(k.lt.ec(im1))) then
            if ((e(i,row+1,im1).gt.0.).and.(e(k,row,im1).gt.0.)) then
              s2n  = f(i,row+1,im1)/e(i,row+1,im1) + 
     .                         f(k,row,im1)/e(k,row,im1)
              s2n  = max(1.e-10,s2n)
              wt   = s2n*s2n
              sum5 = sum5 + wt * f(k,row,im1) * ys(row)
              sum6 = sum6 + wt * f(i,row+1,im1) * ys(row+1)
              wsum = wsum + wt
              num  = num  + 1
            endif
          endif
        endif
        enddo
        if (num.lt.10) then
          sum5 = 0.
          sum6 = 0.
        else
          sum5 = sum5 / wsum
          sum6 = sum6 / wsum
        endif
 
C Rescale the row.
        scale = ( sum2 / sum1 ) * ( sum3 / sum4 )
C Factor in the direct overlap if it exists.
        if ((sum5.gt.0.).and.(sum6.gt.0.)) then
          scale = ( scale + ( sum6 / sum5 ) ) / 2.
        endif
        print *,'Row: ',row,'  Relative scale:',scale
C This scale is relative to old scaling.
C Apply old scale to create absolute scale.
        scale = scale * ys(row)
        print *,'Row: ',row,'       New scale:',scale
        ys(row) = scale
 
      ENDDO
 
      return
 
801   print *,'No overlap region, no more rescaling possible.'
      return
809   print *,'Horrible error in RescaleOrders_Up.'
      return
      end
 
 
C----------------------------------------------------------------------
C Fit a polynomial to the scaling values and adjust the scalings and extend
C the scalings to unscaled rows.
C
      subroutine RescaleRefit(im1,im2,ok)
C
      implicit none
      integer*4 im1,im2
      include 'spim2.inc'
      real*4 xx(99),yy(99),value,ysold
      real*8 coef(0:99),polyval
      integer*4 i,order,nn,row,option,fixedrow
      integer*4 rowfit,n8,k,rowrpl1,rowrpl2
      integer*4 save_im1,save_im2
      logical ok,showpoly
C
      common /RESCALEREFITBLK/ save_im1,save_im2,showpoly
C
 
C If returning, skip to plotting.
      if ((im1.eq.0).and.(im2.eq.0)) then
        im1 = save_im1
        im2 = save_im2
        goto 1
      else
C Save image numbers.
        save_im1 = im1
        save_im2 = im2
      endif
       
C Create x array for plot.
      do row=1,nrw(im1)
        xx(row)  = float(row)
      enddo
 
C Default polynomial fit order.
      order = 2
      showpoly = .false.
 
C Plot ranges.
      xmin = 0.
      xmax = float(nrw(im1)+1)
      ymin = 0.
C Clear screen, set window, plot points.
1     continue
      ymax = 0.0001
      do i=1,nrw(im1)
        if ((xx(i).gt.xmin-0.1).and.(xx(i).lt.xmax+0.1)) then
          if ((ys(i)*1.2).gt.ymax) ymax=ys(i)*1.2
        endif
      enddo
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBOX('BCNST',0.,0,'BCNST',0.,0)
      call PGPT(nrw(im1),xx,ys,3)
      if (showpoly) then
        nn=0
        do i=1,nrw(im1)
          nn=nn+1
          xx(nn) = float(i)
          yy(nn) = sngl(polyval(order+1,coef,dfloat(i)))
        enddo
        call PGSCI(2)
        call PGLINE(nn,xx,yy)
        call PGSCI(1)
        showpoly = .false.
      endif
C Choose option.
2     continue
      print *,'OPTIONS: (1) Change a point.'
      print *,'         (2) (Re)do rescaling upwards.'
      print *,'         (3) (Re)do rescaling downwards.'
      print *,'         (4) Clear scale factors.'
      print *,
     .'         (5) Change default to order of polynomial from:',order
      print *,'         (6) Enter xmin and xmax.'
      print *,
     .'         (7) Replace a set of points with polynomial fit.'
      print *,'         (8) Replace a point with polynomial fit.'
      print *,'         (9) Apply rescaling.'
      print *,'        (-9) Abort rescaling.'
      print *,
     .'        (10) Exit routine temporarily. Use "S" to return.'
      print *,'        (11) Look at a row and change scaling.'
      print '(a,$)',' Enter option : '
      read(5,*) option
 
      if (option.eq.1) then        ! change a point
        print '(a,$)',' Enter row and new value : '
        read(5,*,err=990) row,value
        row = max(1,min(nrw(im1),row))
        ys(row) = value
        goto 1
 
      elseif (option.eq.2) then        ! Redo rescaling upwards.
        print '(a,$)',' Rescaling upwards. Enter fixed row : '
        read(5,*,err=990) fixedrow
        fixedrow = max(1,min(nrw(im1)-1,fixedrow))
        print *,'fixedrow =',fixedrow
        call RescaleOrders_Up(fixedrow,im1,im2)
        goto 1
 
      elseif (option.eq.3) then        ! Redo rescaling downwards.
        print '(a,$)',' Rescaling downwards. Enter fixed row : '
        read(5,*,err=990) fixedrow
        fixedrow = max(2,min(nrw(im1),fixedrow))
        print *,'fixedrow =',fixedrow
        call RescaleOrders_Down(fixedrow,im1,im2)
        goto 1
 
      elseif (option.eq.4) then        ! Clear scale factors.
        print *,' Clear scale factors.'
        do row=1,nrw(im1)
          ys(row) = 1.
        enddo
        goto 1
 
      elseif (option.eq.5) then        ! change default order
        print '(a,$)',' Enter new order : '
        read(5,*,err=990) order
        order = max(0,min(18,order))
        print *,'New order=',order
        goto 2
 
      elseif (option.eq.6) then        ! new xmin and xmax
        print '(a,$)',' Enter new xmin and xmax : '
        read(5,*,err=990) xmin,xmax
        goto 1
 
C Extrapolate with polynomial.

      elseif ((option.eq.7).or.(option.eq.8)) then
        rowrpl1=0
        if (option.eq.7) then
          print '(a,$)',' Enter first row to replace : '
          read(5,*) rowrpl1
          rowrpl1 = max(1,min(nrw(im1),rowrpl1))
          print '(a,$)',' Enter last row to replace : '
          read(5,*) rowrpl2
          rowrpl2 = max(1,min(nrw(im1),rowrpl2))
        else
          print '(a,$)',' Enter row to replace: '
          read(5,*,err=990) rowfit
          rowfit = max(1,min(nrw(im1),rowfit))
        endif
        print *,'Enter rows to use in fit.'
        print *,
     .  '(0=stop; -n = use the rows between n and the last entry.)'
        n8=0
4       continue
        print '(a,$)',' Enter row to fit to: '
        read(5,*,err=990) row
        if (row.gt.0) then
          row = max(1,min(nrw(im1),row))  
          n8=n8+1
          x8(n8) = dfloat(row)
          y8(n8) = dble(ys(row))
          z8(n8) = 1.d0
          print *,'Fit will include: ',row
          goto 4
        elseif (row.lt.0) then
          row = max(1,min(nrw(im1),abs(row)))  
          k = nint(x8(n8))
          do i=min(k,row),max(k,row)
            if (i.ne.k) then
              n8=n8+1
              x8(n8) = dfloat(i)
              y8(n8) = dble(ys(i))
              z8(n8) = 1.d0
              print *,'Fit will include: ',i
            endif
          enddo
          goto 4
        endif
        if (n8.lt.order+1) goto 990
        call poly_fit_glls(n8,x8,y8,z8,order,coef,ok)
        if (.not.ok) goto 990
        if (rowrpl1.gt.0) then
          do i=rowrpl1,rowrpl2
            ys(i) =  sngl(polyval(order+1,coef,dfloat(i)))
          enddo
        else
          ysold = ys(rowfit)
          ys(rowfit) = sngl(polyval(order+1,coef,dfloat(rowfit)))
          print '(a,$)',
     .'  Rescale other rows upward (+1), downward (-1), or none (0) : '
          read(5,*,err=990) k
          if (k.eq.1) then
            do i=rowfit+1,nrw(im1)
              ys(i) = ys(i) * ( ys(rowfit) / ysold )
            enddo
          elseif (k.eq.-1) then
            do i=rowfit-1,1,-1
              ys(i) = ys(i) * ( ys(rowfit) / ysold )
            enddo
          endif
        endif
        showpoly=.true.
        goto 1
 
      elseif (option.eq.9) then        ! exit and rescale
        goto 800
 
      elseif (option.eq.-9) then       ! abort without rescaling
        goto 810

      elseif (option.eq.10) then       ! temporary exit
        goto 820
 
      elseif (option.eq.11) then       ! check out a row
7001    print '(a,$)',' Enter row of interest : '
        read(5,*,err=7001) i
        call RescaleInt(i)
        goto 1
 
      else
        goto 2
 
      endif
 
800   continue
      print *,'Rescaling data and error arrays.'
      ok = .true.
      return
 
810   continue
      print *,'No rescaling done.'
      ok = .false.
      return

820   continue
      print *,'Temporary exit, use "S" with "0,0" to return.'
      ok = .false.
      return
 
990   print *,
     .  'ERROR... returning to previous menu. No rescaling done.'
      ok=.false.
      return
      end
 
 
C----------------------------------------------------------------------
C Rescale with new scaling factors.
      subroutine RescaleApply(imno)
      implicit none
      integer*4 imno
      include 'spim2.inc'
      integer*4 row,i
      do row=1,nrw(imno)
        do i=sc(imno),ec(imno)
          f(i,row,imno) = f(i,row,imno) * ys(row)
          if (e(i,row,imno).gt.0.) then
            e(i,row,imno) = e(i,row,imno) * ys(row)
          endif
        enddo
      enddo
      modified(imno) = .true.
      return
      end
 
 
C-------------------------------------------------------------------------
C Try to find and mask deviant pixels which may be caused by cosmic-ray,
C radiation event, or sky subtraction problems.
C im = image to operate on,
C mbs = median box size, thres = sigmas threshold for rejection
C selrow = selected row (do this row only, use 0 for all rows.)
C
      subroutine CorrectDeviants(im,mbs,thres,selrow)
C
      implicit none
      integer*4 im,mbs,selrow
      real*4 thres
      include 'spim2.inc'
      integer*4 i,row,col,n,oim,orow,k,hipt
      real*4 rr,get_level,val(99),vae(99),ref,ree
      real*4 w2,w3,hiv,lov,hie,loe,hidev,binval
      real*8 polyval,rk,wave1,wave2
      logical ok

C Initialize.
      ref=0.
      ree=0.

C Go through each row of this image.
      do row=1,nrw(im)
      if ((selrow.eq.0).or.(row.eq.selrow)) then
C Look at each valid column.
        do col=sc(im),ec(im)
          n=0
          if (e(col,row,im).gt.0.) then
C Find median for this pixel.
            rr = get_level(col,row,im,mbs,ok)
            if (.not.ok) goto 930
            if (abs(rr).gt.1.e-20) then
C Scale flux and error and record.
              n=n+1
              val(n) = f(col,row,im) / rr
              vae(n) = e(col,row,im) / rr
C Look for other pixels in other images.
              do oim=1,nim
C Find all pixels that correspond to primary pixel.
      do orow=1,nrw(oim)
      if ((oim.ne.im).or.(orow.ne.row)) then
      if ( (blwv(orow,oim).lt.w(col,row,im)).and.
     .     (rdwv(orow,oim).gt.w(col,row,im)) ) then
        call GetXFromY(rk,dble(w(col,row,im)),
     .              cof(0,orow,oim),ord(orow,oim)+1,
     .                icof(0,orow,oim),iord(orow,oim)+1,ok)
        if (.not.ok) goto 930
        k = nint(rk)
        if ((k.gt.sc(oim)).and.(k.lt.ec(oim))) then
          if (e(k,orow,oim).gt.0.) then
            rr = get_level(k,orow,oim,mbs,ok)
            if (.not.ok) goto 930
            if (abs(rr).gt.1.e-20) then
              n=n+1
C Get the actual value of the flux in this region.
              wave1 = polyval(ord(row,im)+1,cof(0,row,im),
     .                                      dfloat(col)-0.5d0)
              wave2 = polyval(ord(row,im)+1,cof(0,row,im),
     .                                      dfloat(col)+0.5d0)
              val(n)= binval(1,orow,oim,wave1,wave2,ok) / rr
              if (.not.ok) goto 930
              vae(n)= binval(2,orow,oim,wave1,wave2,ok) / rr
              if (.not.ok) goto 930
C Delete this point if zero.
              if ((abs(val(n)).lt.1.e-20).or.(vae(n).lt.1.e-20)) then
                n=n-1
              endif
            endif
          endif
        endif
      endif
      endif
      enddo
              enddo
C Are there any other pixels to compare with?
              if (n.gt.1) then

C If there is only one other pixel, use this value as reference.
                if (n.eq.2) then
                  ref= val(2)
                  ree= vae(2)
C If there are two values, use mean as reference.
                elseif (n.eq.3) then
                  w2 = 1.0 / ( vae(2)*vae(2) )
                  w3 = 1.0 / ( vae(3)*vae(3) )
                  ref= ( val(2)*w2 + val(3)*w3 ) / ( w2 + w3 )
                  ree= 1.0 / sqrt( w2 + w3 )

C If there are 3 or more pixels in other images...
                elseif (n.gt.3) then
C Find highest and lowest value among other values.
                  hiv=-1.e+30
                  hie=-1.e+30
                  lov=+1.e+30
                  loe=+1.e+30
                  do i=2,n
                    if (val(i).gt.hiv) then
                      hiv = val(i)
                      hie = vae(i)
                    endif
                    if (val(i).lt.lov) then
                      lov = val(i)
                      loe = vae(i)
                    endif
                  enddo
C Find average weighted value.
                  ref=0.
                  ree=0.
                  do i=2,n
                    w2 = 1.0 / ( vae(i)*vae(i) )
                    ref= ref + val(i)*w2
                    ree= ree + w2
                  enddo
                  ref= ref / ree
                  ree= 1.0 / sqrt( ree )
C Are the high and low values consistent?
                  if ((hiv-lov)/sqrt(hie*hie+loe*loe).lt.5.) then
C If so, use average.
                    continue
                  else
C If not, throw out most deviant pixel, and use mean of remaining.
                    hidev=-1.e+30
                    hipt = 2
                    do i=2,n
                      if (abs(val(i)-ref).gt.hidev) then
                        hidev= val(i)-ref
                        hipt = i
                      endif
                    enddo
                    ref=0.
                    ree=0.
                    do i=2,n
                      if (i.ne.hipt) then
                        w2 = 1.0 / ( vae(i)*vae(i) )
                        ref= ref + val(i)*w2
                        ree= ree + w2
                      endif
                    enddo
                    ref= ref / ree
                    ree= 1.0 / sqrt( ree )
                  endif
                endif
C Is primary pixel inconsistent with reference?
                if ( abs(val(1)-ref) / sqrt(vae(1)*vae(1)+ree*ree)
     .                    .gt.thres ) then
C If so, reject if 2 or more reference pixels.
                  if (n.gt.2) then
                    f(col,row,im) = 0.
                    e(col,row,im) =-1.
                    print '(a,3i4,2f10.4)',
     .                ' Rejecting:',col,row,n,w(col,row,im),
     .                abs(val(1)-ref)/sqrt(vae(1)*vae(1)+ree*ree)
                  else
C Otherwise, decide which pixel is worse.
                    if (abs(val(1)).gt.abs(val(2))) then
                      f(col,row,im) = 0.
                      e(col,row,im) = -1.
          print '(a,3i4,2f10.4)',' Rejecting:',col,row,n,w(col,row,im),
     .    abs(val(1)-ref)/sqrt(vae(1)*vae(1)+ree*ree)
                    endif
                  endif
                endif    ! inconsistent with reference?
              endif      ! n.gt.1 ?
            endif        ! (abs(rr).gt.1.e-20) ?
          endif          ! (e(col,row,im).gt.0.) ?
        enddo            ! col
      endif 
      enddo              ! row

      return
930   continue
      print *,'Problem in CorrectDeviants.'
      return
      end


C----------------------------------------------------------------------
C Find the median value around a pixel given by col,row,im with a median
C box size of umbs columns.
C
      real*4 function get_level(col,row,im,umbs,ok)
C
      implicit none
      integer*4 col,row,im,umbs
      logical ok
      include 'spim2.inc'
      integer*4 col1,col2,i,mbs,n
      real*4 rr
      mbs= umbs
      ok = .true.
      get_level=0.
1     continue
      col1 = max(sc(im),min(ec(im),col-(mbs/2)))
      col2 = max(sc(im),min(ec(im),col+(mbs/2)))
      n=0
      do i=col1,col2
        if ((i.ne.col).and.(e(i,row,im).gt.0.)) then
          n=n+1
          arr(n) = f(i,row,im)
        endif
      enddo
      if (n.lt.4) then
        mbs=mbs+1
        if (mbs.gt.nc(im)) then
          print *,
     .  'Error finding level in get_level, not enough good pixels.'
          ok=.false.
          return
        endif
        goto 1
      endif
      call find_median(arr,n,rr)
      get_level = arr(nint(float(n)*0.75))
      return
      end


C----------------------------------------------------------------------
C Return average flux between wavelength 1 (wave1) and wavelength 2 (wave2).
C mode=1, use f(); mode=2, use e().
C
      real*4 function binval(mode,row,im,wave1,wave2,ok)
C
      implicit none
      integer*4 mode,row,im
      real*8 wave1,wave2,wv1,wv2,pix1,pix2
      real*4 bval
      include 'spim2.inc'
      logical ok
      binval=0.
      wv1 = min(wave1,wave2)
      wv2 = max(wave1,wave2)
      call GetXFromY(pix1,wv1,cof(0,row,im), ord(row,im)+1,
     .                       icof(0,row,im),iord(row,im)+1,ok)
      if (.not.ok) return
      call GetXFromY(pix2,wv2,cof(0,row,im), ord(row,im)+1,
     .                       icof(0,row,im),iord(row,im)+1,ok)
      if (.not.ok) return
      if (mode.eq.1) then
        call linear_fluxave(f(sc(im),row,im),sc(im),ec(im),1,1,1,
     .                      pix1,pix2,bval,.true.,.false.)
      elseif (mode.eq.2) then
        call linear_fluxave(e(sc(im),row,im),sc(im),ec(im),1,1,1,
     .                      pix1,pix2,bval,.true.,.false.)
      else
        bval=0.
      endif
      binval = bval
      return
      end


C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C- - - -  BELOW IS THE SAME ROUTINE WHICH IS IN linear.f   - - - - - - -
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

C----------------------------------------------------------------------
C Find flux (per unit original bin) between the pixel values pix1 and pix2.
C
      subroutine linear_fluxave(a,sc,ec,sr,er,row,pix1,pix2,
     .  bval,nozero,lgi)
C
      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er),bval
      real*8 pix1,pix2
      logical nozero,lgi,more
      integer*4 i,i0,i1,i2
C     real*8 Ym,Yp,x1,x3,area
      real*8 sum,wsum,b0,b1,Y0
C Check for zero pixels.
      if (nozero) then
        more = .true.
        do i=nint(pix1),nint(pix2)
          if (abs(a(i,row)).lt.1.e-30) more = .false.
        enddo
        if (.not.more) then
          bval = 0.
          return
        endif
      endif
C Sum and weight.
      sum = 0.d0
      wsum= 0.d0
C Whole pixels.
      i1 = nint(pix1) + 1
      i2 = nint(pix2) - 1
      if (i2.ge.i1) then
        do i=i1,i2
          sum = sum + dble(a(i,row))
          wsum= wsum+ 1.d0
        enddo
      endif
      IF (lgi) THEN

      print *,'lgi not available'
      call exit(1)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C More to calculate.
C     more = .true.
C Left fractional pixel.
C     b0 = pix1
C     b1 = dfloat(nint(pix1)) + 0.5d0
C     if (b1.gt.pix2) then
C       b1 = pix2
C       more = .false.
C     endif
C     i0 = nint(pix1)
C     Y0 = dble(a(i0,row))
C     Ym = dble(a(max(sc,i0-1),row))
C     Yp = dble(a(min(ec,i0+1),row))
C     x1 = dfloat(i0) - 1.d0
C     x3 = dfloat(i0) + 1.d0
C     call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
C     sum = sum + area
C     wsum= wsum+ (b1-b0)
C Right fractional pixel.
C     if (more) then
C       b1 = pix2
C       b0 = dfloat(nint(pix2)) - 0.5d0
C       i0 = nint(pix2)
C       Y0 = dble(a(i0,row))
C       Ym = dble(a(max(sc,i0-1),row))
C       Yp = dble(a(min(ec,i0+1),row))
C       x1 = dfloat(i0) - 1.d0
C       x3 = dfloat(i0) + 1.d0
C       call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
C       sum = sum + area
C       wsum= wsum+ (b1-b0)
C     endif
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      ELSE
C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))
      sum = sum + (b1-b0)*Y0
      wsum= wsum+ (b1-b0)
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))
        sum = sum + (b1-b0)*Y0
        wsum= wsum+ (b1-b0)
      endif
      ENDIF
C Average flux per unit original bin.
      bval = sngl( sum / wsum )
      return
      end

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC 
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC 

C----------------------------------------------------------------------
C Interactive rescaling.
C
      subroutine RescaleInt(row)

      implicit none
      integer*4 row
      include 'spim2.inc'
      real*4 xx(9000),yy(9000)
      real*4 xxl(9000),yyl(9000)
      real*4 xxr(9000),yyr(9000)
      real*4 yscale,new_yscale,yhi,xlo,xhi,r,ys_row
      integer*4 i,nn,k,nnl,nnr
      character c1*1

      print '(a,i3,a,f9.4)',
     .  ' Future rescaling for row',row,' is',ys(row)
7001  print '(a,$)',' Enter new scale factor : '
      read(5,*,err=7001) yscale

C Set data.
      nn =0
      nnl=0
      nnr=0
      if (ShowBin) then
        do i=sc(prim),sc(prim)+ncb(prim)-1
          nn=nn+1
          xx(nn)=wb(i,row,prim)
          yy(nn)=fb(i,row,prim)
        enddo
        if (row.ne.1) then
          do i=sc(prim),sc(prim)+ncb(prim)-1
            nnl=nnl+1
            xxl(nnl)=wb(i,row-1,prim)
            yyl(nnl)=fb(i,row-1,prim)
          enddo
        endif
        if (row.ne.nrw(prim)) then
          do i=sc(prim),sc(prim)+ncb(prim)-1
            nnr=nnr+1
            xxr(nnr)=wb(i,row+1,prim)
            yyr(nnr)=fb(i,row+1,prim)
          enddo
        endif
      else
        do i=sc(prim),sc(prim)+nc(prim)-1
          nn=nn+1
          xx(nn)=w(i,row,prim)
          yy(nn)=f(i,row,prim)
        enddo
        if (row.ne.1) then
          do i=sc(prim),sc(prim)+nc(prim)-1
            nnl=nnl+1
            xxl(nnl)=w(i,row-1,prim)
            yyl(nnl)=f(i,row-1,prim)
          enddo
        endif
        if (row.ne.nrw(prim)) then
          do i=sc(prim),sc(prim)+nc(prim)-1
            nnr=nnr+1
            xxr(nnr)=w(i,row+1,prim)
            yyr(nnr)=f(i,row+1,prim)
          enddo
        endif
      endif

C Scale data.
      print *,' Scaling data using future scale factors.'
      do i=1,nn
        yy(i) = yy(i) * yscale
      enddo
      do i=1,nnl
        yyl(i)= yyl(i)* ys(row-1)
      enddo
      do i=1,nnr
        yyr(i)= yyr(i)* ys(row+1)
      enddo
C x limits.
      xlo  = w(sc(prim),row,prim)
      xhi  = w(ec(prim),row,prim)
      r    = max(xhi-xlo,1.e-20)
      xmin = xlo - xex*r
      xmax = xhi + xex*r
C y limits.
3     continue
      yhi=0.
      do i=1,nn
        if (yy(i).gt.yhi) yhi=yy(i)
      enddo
      do i=1,nnl
        if (yyl(i).gt.yhi) yhi=yyl(i)
      enddo
      do i=1,nnr
        if (yyr(i).gt.yhi) yhi=yyr(i)
      enddo
      ymin = yhi*(-0.05)
      ymax = yhi*(+1.05)

C Clear plot.
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBOX('BCNST',0.,0,'BCNST',0.,0)

C Replot spectrum.
      call PGSCI(1)
      call PlotIt(nn,xx,yy,hist)
      call PGSCI(7)
      call PlotIt(nnl,xxl,yyl,hist)
      call PlotIt(nnr,xxr,yyr,hist)
      call PGSCI(1)

7002  print '(a,$)',' Accept(a), Retry(r), Quit(q) : '
      read(5,'(a)',err=7002) c1
      if (c1.eq.'a') then
7003    print '(a,$)',' Rescale from this row up (+1) or down (-1) : '
        read(5,*,err=7003) k
        ys_row = ys(row)
        if (k.eq.1) then
          do i=row,nrw(prim)
            ys(i) = ys(i) * ( yscale / ys_row )
          enddo
        elseif (k.eq.-1) then
          do i=1,row
            ys(i) = ys(i) * ( yscale / ys_row )
          enddo
        else
          goto 7003
        endif

      elseif (c1.eq.'r') then
7004    print '(a,$)',' Enter new scale factor : '
        read(5,*,err=7004) new_yscale
C Re-Scale data.
        do i=1,nn
          yy(i) = yy(i) * ( new_yscale / yscale )
        enddo
        yscale = new_yscale
        goto 3

      endif

      return
      end

C----------------------------------------------------------------------
C mode=1 : Read file , mode=2 : Write file.
C
      subroutine rw_bigspec(mode)
      implicit none
      integer*4 mode
C
      include 'spim2.inc'
C
      logical ok
      real*8 wave1,wave2,getx,crval1,cdelt1
      integer*4 lc,naxis1,inhead
      if (mode.eq.1) then
        print '(2a)',' Reading big file : ',bigfile(1:lc(bigfile))
        call readfits_header(bigfile,bighead,ok)
        if (.not.ok) goto 910
        call readfitsxy(bigfile,ww,ff,mptbig,ncbig)
      elseif (mode.eq.2) then
        if (ncbig.lt.2) goto 910
        if (bighead.eq.' ') then
          call make_basic_header(bighead,1,ncbig,0,1)
          crval1=dble(ww(1))
          cdelt1=dble(ww(2)-ww(1))
          call cheadset('OBJECT',bigfile,bighead)
          call cheadset('CTYPE1','LINEAR',bighead)
          call fheadset('CRVAL1',crval1,bighead)
          call fheadset('CDELT1',cdelt1,bighead)
        else
          wave1 = getx(-1.d0,bighead)
          wave1 = getx(+1.d0,bighead)
          wave2 = getx(+2.d0,bighead)
          if ((abs(dble(ww(1))-wave1).gt.1.d-2).or.
     .        (abs(dble(ww(2))-wave2).gt.1.d-2)) then
            print *,
     .  'Error- wavelengths in data and header do not match.'
            goto 910
          endif
          naxis1 = inhead('NAXIS1',bighead)
          if (naxis1.ne.ncbig) then
            print *,
     .  'Error- number of pixels in data and header do not match.'
            goto 910
          endif
        endif
        print '(2a)',' Writing: ',bigfile(1:lc(bigfile))
        call writefits(bigfile,ff,bighead,ok)
        if (.not.ok) goto 910
      endif
      return
910   continue
      print *,'Error reading or writing big spectrum.'
      return
      end


C----------------------------------------------------------------------
      subroutine getylim(row,xlo,xhi,ylo,yhi)
      implicit none
      integer*4 row,i
      real*4 xlo,xhi,ylo,yhi
      include 'spim2.inc'
      ylo = +1.e+30
      yhi = -1.e+30
      if (ShowBin) then
        do i=sc(prim),(ncb(prim)+sc(prim)-1)
          if ((wb(i,row,prim).gt.xlo).and.(wb(i,row,prim).lt.xhi)) then
            if (fb(i,row,prim).gt.yhi) yhi = fb(i,row,prim)
            if (fb(i,row,prim).lt.ylo) ylo = fb(i,row,prim)
          endif
        enddo
      else
        do i=sc(prim),ec(prim)
          if ((w(i,row,prim).gt.xlo).and.(w(i,row,prim).lt.xhi)) then
            if (f(i,row,prim).gt.yhi) yhi = f(i,row,prim)
            if (f(i,row,prim).lt.ylo) ylo = f(i,row,prim)
          endif
        enddo
      endif
      return
      end

C----------------------------------------------------------------------
C Fit polynomial through data and load into big spectrum.'
C
      subroutine spim2_polyfit(row)

      implicit none
      integer*4 row
      include 'spim2.inc'
      real*4 wr(99,2),xval
      real*4 xx(9000),yy(9000),cx,cy,wave,x1,x2,wmin,wmax
      integer*4 nwr,order,im,col,n,nn,i,neari,i1,i2,j
      real*8 coef(0:99),polyval
      character*1 key
      logical ok

C Get wavelength segments.
701   continue
      nwr=0
      call PGSCI(8)
      print '(a)',
     .  'Use space bar to define wavelength segments, "q" to stop.'
1     continue
      call PGCURS(cx,cy,key)
      if (key.eq.' ') then
        x1=cx
        call PG_Mark(x1,ymin,ymax)
        call PGCURS(cx,cy,key)
        if (key.eq.' ') then
          x2=cx
          call PG_Mark(x2,ymin,ymax)
          call PG_HLine(x1,x2,(ymin+ymax)/2.)
          nwr=nwr+1
          wr(nwr,1) = min(x1,x2)
          wr(nwr,2) = max(x1,x2)
        endif
      endif
      if (key.ne.'q') goto 1
      call PGSCI(1)
        
C Load data.
      print *,'Loading data from all images and rows...'
      n=0
      do im=1,nim
        do j=1,nrw(im)
          do col=sc(im),ec(im)
            wave=w(col,j,im)
            do i=1,nwr
              if ((wave.gt.wr(i,1)).and.(wave.lt.wr(i,2))) then
                n=n+1
                x8(n)=dble(wave)
                y8(n)=dble(f(col,j,im))
                z8(n)=1.d0
                if (abs(f(col,j,im)).lt.1.e-20) z8(n)=0.d0
              endif
            enddo
          enddo
        enddo
      enddo

C Fit polynomial.
703   print '(a,$)',' Enter polynomial order : '
      read(5,*,err=703) order
      call poly_fit_glls(n,x8,y8,z8,order,coef,ok)

C Show polynomial.
      nn=0
      do xval=xmin,xmax,0.1
        nn     = nn + 1
        xx(nn) = xval
        yy(nn) = sngl(polyval(order+1,coef,dble(xval)))
      enddo
      call PGSCI(5)
      call PGLINE(nn,xx,yy)
      call PGSCI(1)

C Decision time.
705   print '(a,$)',
     .  ' Replace(r), quit(q), new order(o), new segments(s) : '
      read(5,'(a)',err=705) key
      if (key.eq.'o') then
        call PGSCI(0)               ! erase polynomial.
        call PGLINE(nn,xx,yy)
        call PGSCI(1)
        goto 703
      elseif (key.eq.'s') then
        call plotspec(row,2)        ! replot data.
        goto 701
      elseif (key.eq.'r') then
        print *,' Define section to replace by hitting <sp> twice.'
        call PGCURS(x1,cy,key)
        call PGSLS(4)
        call PG_Mark(x1,ymin,ymax)
        call PGCURS(x2,cy,key)
        call PG_Mark(x2,ymin,ymax)
        call PGSLS(1)
C Initialize big spectrum.
        if (ncbig.eq.0) then
          wmin=+1.e+30
          wmax=-1.e+30
          do im=1,nim
            if (w(sc(im),1,im).lt.wmin) wmin=w(sc(im),1,im)
            if (w(ec(im),nrw(im),im).gt.wmax) wmax=w(ec(im),nrw(im),im)
          enddo
          wmin=float(nint(wmin*10.))/10.
          wmax=float(nint(wmax*10.))/10.
          ncbig=nint((wmax-wmin)/0.1)+1
          do i=1,ncbig
            ww(i) = wmin + (dfloat(i-1)*0.1)
            ff(i) = 0.
          enddo
        endif
C Replace data into big spectrum.
        i1 = neari(min(x1,x2),ncbig,ww)
        i2 = neari(max(x1,x2),ncbig,ww)
        do i=i1,i2
          ff(i) = sngl(polyval(order+1,coef,dble(ww(i))))
        enddo
      endif

      return
      end

C----------------------------------------------------------------------
C Scale/fit current row to match big spec. mode=0 is normal, mode=1 is special
C quick in with 8th order fit to entire data set.
C
      subroutine Match_To_Big(row,mode)
C
      implicit none
      integer*4 row,mode
C
      include 'spim2.inc'
C
      integer*4 i,k,neari_bs,nd,order
      real*8 coef(0:99),polyvaloff,xoff
      real*4 xd(9000),yd(9000),x1,x2,wave,xart(99),yart(99),zart(99)
      real*4 wr(99,2),xval,xx(9000),yy(9000),rr,y1,y2
      real*4 fperc,cx,cy,sum1,sum2,xsum,valread
      integer*4 nwr,n,nn,ii,num,numx,nart,lc
      character key*1,wrd*80
      logical ok,dot2dot

C Set cursor position.
      cx=(xmin+xmax)/2.
      cy=ymin+((ymax-ymin)/4.)
      dot2dot=.true.
      nart=0

1     continue

C Set min_reject...
      if ((min_reject.lt.0.01).or.(min_reject.gt.0.9)) then
        print '(a,$)','Enter min_reject (sugg: 0.05) = '
        read(5,*,err=1) min_reject
      endif

C Find median of current row.
      nn=0
      do i=sc(prim),ec(prim)
        nn=nn+1
        arr(nn)=f(i,row,prim)
      enddo
      call find_median(arr,nn,rr)
      fperc = rr*min_reject
      print '(a,f10.3,a,f5.2,a)',
     .      ' Ignoring pixels below :',fperc,' (',min_reject,' )'

      IF (bin.gt.1) THEN
C
C Divide current row by big spectrum.
      nd=0
      do i=sc(prim),ec(prim),bin
        sum1=0.
        sum2=0.
        xsum=0.
        numx=0
        num =0
        do ii=min(i,ec(prim)),min(i+bin-1,ec(prim))
          wave= w(ii,row,prim)
          k = neari_bs(wave,ncbig,ww)
          xsum = xsum + wave
          numx = numx + 1
          if (e(ii,row,prim).gt.-0.1) then
            sum1 = sum1 + ff(k)
            sum2 = sum2 + f(ii,row,prim)
            num  = num  + 1
          endif
        enddo
        if (numx.gt.0) then
          nd=nd+1
          xd(nd) = xsum / float(numx)
          if ((sum1.gt.0.).and.(sum2.gt.0.)) then
            yd(nd)= sum1 / sum2
            if ((sum2/float(num)).lt.fperc) yd(nd)=0.
          else
            yd(nd)= 0.
          endif
        endif
      enddo
C
      ELSE
C
C Divide current row by big spectrum.
      nd=0
      do i=sc(prim),ec(prim)
        wave= w(i,row,prim)
        k = neari_bs(wave,ncbig,ww)
        nd=nd+1
        xd(nd)= wave
        if ((f(i,row,prim).gt.fperc).and.(e(i,row,prim).gt.-0.1)) then
          yd(nd)= ff(k) / f(i,row,prim)
        else
          yd(nd)= 0.
        endif
      enddo
C
      ENDIF

C Set plot levels.
      do i=1,nd
        arr(i)=yd(i)
      enddo
      x1  = w(sc(prim),row,prim)
      x2  = w(ec(prim),row,prim)
      xmin= x1 - ((x2-x1)*0.05)
      xmax= x2 + ((x2-x1)*0.05)
      call find_median(arr,nd,x1)
      x1 = arr(nint(float(nd)*0.95))
      ymin= 0.
      ymax= x1*1.2

C Plot result.
2     continue
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBOX('BICNST',0.,0,'BICNST',0.,0)
      if (dot2dot) then
        call PGLINE(nd,xd,yd)
      else
        call PGSCI(8)
        call PGPT(nd,xd,yd,3)
      endif
      call PGSCI(2)
      call PG_HLine(xmin,xmax,1.0)
      call PGSCI(1)

C Get wavelength segments.
      if (mode.eq.1) then
        nwr=1
        wr(nwr,1) = xmin
        wr(nwr,2) = xmax
      else
3       continue
        call GetSegs(wr,nwr,8,key)
        if (key.eq.'q') return
        if (nwr.lt.1) then
          print *,'No region specified. Try again.'
          goto 3
        endif
      endif

C Load data.
702   continue
      n=0
      do k=1,nd
        do i=1,nwr
          if ((xd(k).gt.wr(i,1)).and.(xd(k).lt.wr(i,2))) then
            n=n+1
            x8(n)=dble(xd(k))
            y8(n)=dble(yd(k))
            z8(n)=1.d0
            if (abs(yd(k)).lt.1.e-20) z8(n)=0.d0
          endif
        enddo
      enddo
      do k=1,nart
        n=n+1
        x8(n)=dble(xart(k))
        y8(n)=dble(yart(k))
        z8(n)=dble(zart(k))
      enddo

C Fit polynomial.
703   continue
      call PGSCI(0)
      write(wrd,'(a,i3)') 'Using polynomial order=',order
      call PGMTEXT('T',0.5,0.1,0.0,wrd)
      call PGSCI(1)
      if (mode.eq.1) then
        order = v_order
        mode=0       ! Go back to normal mode after initial entry.
      else
        print '(a)',
     .  ' Polynomial order ("-"=median,0,1,2,... 9) (q to quit).'
        call PGCURS(cx,cy,key)
        if (key.eq.'q') return
        if (key.eq.'-') then
          order = -1
        else
          order = nint(valread(key))
        endif
      endif
      if ((order.gt.9).or.(order.lt.-1)) then
        print *,'ERROR: Order out of range :',order
        goto 703
      endif
      write(wrd,'(a,i3)') 'Using polynomial order=',order
      call PGMTEXT('T',0.5,0.1,0.0,wrd)
      print '(a)',wrd(1:lc(wrd))
      if (order.eq.-1) then
        nn=0
        do i=1,n
          if (z8(i).gt.0.) then
            nn=nn+1
            arr(nn)=sngl(y8(i))
          endif
        enddo
        call find_median(arr,nn,x1)
        order=0
        coef(0)=dble(x1)
        coef(1)=0.d0
      else
        call poly_fit_gj(n,x8,y8,z8,order,coef,xoff,0,ok)
      endif

C Show polynomial.
      nn=0
      do xval=xmin,xmax,0.1
        nn     = nn + 1
        xx(nn) = xval
        yy(nn) = sngl(polyvaloff(order+1,coef,dble(xval),xoff))
      enddo
      call PGSCI(3)
      call PGLINE(nn,xx,yy)
      call PGSCI(1)

C Decision time.
705   continue
C
      print '(a)',
     . 'Dots(d), multiply(m/n=m,w,q), quit(q), mask(k/K,j)'
      print '(a)',
     . 'order(o), segs(s), min_reject(j), Artificial points(a).'
C
      call PGCURS(cx,cy,key)
      if (key.eq.'j') then
        min_reject = 0.0
        goto 1
      elseif (key.eq.'d') then
        dot2dot = (.not.dot2dot)
        goto 2
      elseif (key.eq.'a') then
        print *,'Click to define position of artificial point.'
        call PGCURS(cx,cy,key)
        nart=nart+1
        xart(nart)=cx
        yart(nart)=cy
        zart(nart)=10.
        call PGSCI(4)
        call PGSCH(2.5)
        call PGPT(1,cx,cy,3)
        call PGSCH(1.3)
        call PGSCI(0)
        call PGLINE(nn,xx,yy)
        call PGSCI(1)
        goto 702
      elseif ((key.eq.'K').or.(key.eq.'j')) then
        print *,
     .  'Click twice to define maximum and minimum acceptable pixels.'
        call PGCURS(cx,cy,key)
        y1=cy
        call PGCURS(cx,cy,key)
        y2=cy
        y2=max(y1,y2)
        y1=min(y1,cy)
        do i=1,nd
          if ((yd(i).lt.y1).or.(yd(i).gt.y2)) yd(i)=0.
        enddo
        goto 2
      elseif (key.eq.'k') then
        call GetSegs(wr,nwr,2,key)
        if (key.eq.'q') return
        do i=1,nd
          do k=1,nwr
            if ((xd(i).gt.wr(k,1)).and.(xd(i).lt.wr(k,2))) then
              yd(i)=0.
            endif
          enddo
        enddo
        goto 2
      elseif (key.eq.'o') then
        call PGSCI(0)
        call PGLINE(nn,xx,yy)
        call PGSCI(1)
        goto 703
      elseif (key.eq.'s') then
        goto 2
      elseif (key.eq.'m') then
        print *,' Select region to multiply by fit.'
        call GetSegs(wr,nwr,8,key)
        if (key.eq.'q') return
        do i=sc(prim),ec(prim)
          do k=1,nwr
            if ((w(i,row,prim).gt.wr(k,1)).and.
     .                  (w(i,row,prim).lt.wr(k,2))) then
              rr = sngl(polyvaloff(order+1,coef,wo8(i,row,prim),xoff))
              f(i,row,prim) = f(i,row,prim) * rr
              if (e(i,row,prim).gt.0.) then
                e(i,row,prim) = e(i,row,prim) * rr
              endif
            endif
          enddo
        enddo
        print *,'Showing corrected data.'
        call rebin()
        goto 1
      elseif (key.eq.'n') then
        nwr=1
        wr(nwr,1) = xmin
        wr(nwr,2) = xmax
        do i=sc(prim),ec(prim)
          do k=1,nwr
            if ((w(i,row,prim).gt.wr(k,1)).and.
     .                   (w(i,row,prim).lt.wr(k,2))) then
              rr = sngl(polyvaloff(order+1,coef,wo8(i,row,prim),xoff))
              f(i,row,prim) = f(i,row,prim) * rr
              if (e(i,row,prim).gt.0.) then
                e(i,row,prim) = e(i,row,prim) * rr
              endif
            endif
          enddo
        enddo
        print *,'Showing corrected data.'
        call rebin()
      elseif (key.eq.'q') then
        continue
      else
        goto 705
      endif

      return
      end

C----------------------------------------------------------------------
C Scale/fit current row to match big spec.
C
      subroutine Match_To_Big_ll(row)
C
      implicit none
      integer*4 row
      include 'spim2.inc'
      integer*4 i,k,neari_bs,nd
      real*4 xd(9000),yd(9000)
      real*4 x1,x2,wave,cx,cy
      character*1 key

C Divide current row by big spectrum.
1     continue
      nd=0
      do i=sc(prim),ec(prim)
        wave= w(i,row,prim)
        k = neari_bs(wave,ncbig,ww)
        nd=nd+1
        xd(nd)= wave
        if (abs(f(i,row,prim)).gt.1.e-20) then
          yd(nd)= ff(k) / f(i,row,prim)
        else
          yd(nd)= 0.
        endif
        arr(nd) = yd(nd)
      enddo
      x1  = w(sc(prim),row,prim)
      x2  = w(ec(prim),row,prim)
      xmin= x1 - ((x2-x1)*0.05)
      xmax= x2 + ((x2-x1)*0.05)
      call find_median(arr,nd,x1)
      ymin= 0.
      ymax= x1*1.7

C Plot result.
2     continue
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBOX('BICNST',0.,0,'BICNST',0.,0)
      call PGLINE(nd,xd,yd)
      call PGSCI(2)
      call PG_HLine(xmin,xmax,1.0)
      call PGSCI(3)
      call PG_HLine(xmin,xmax,x1)
      call PGSCI(1)

77    continue
      print *,'Hit t to set level or m to accept median.'
      call PGCURS(cx,cy,key)
      if (key.eq.'m') then
        do i=sc(prim),ec(prim)
          f(i,row,prim) = f(i,row,prim) * x1
          if (e(i,row,prim).gt.0.) e(i,row,prim) = e(i,row,prim) * x1
        enddo
      elseif (key.eq.'t') then
        do i=sc(prim),ec(prim)
          f(i,row,prim) = f(i,row,prim) * cy
          if (e(i,row,prim).gt.0.) e(i,row,prim) = e(i,row,prim) * cy
        enddo
      else
        goto 77
      endif

      return
      end


C----------------------------------------------------------------------
C Get wavelength segments.
C
      subroutine GetSegs(seg,nseg,color,key)
C
      implicit none
      integer*4 nseg,color
      real*4 seg(99,2)
      include 'spim2.inc'
      real*4 cx,cy,x1,x2
      character key*1
      call PGSCI(color)
      print '(a)',' Use space bar to define wavelength'
      print '(a)',' segments.  Use "q" to stop.'
      print '(a)',' Use "w" to define whole range.'
      nseg=0
1     continue
      call PGCURS(cx,cy,key)
      if (key.eq.' ') then
        x1=cx
        call PG_Mark(x1,ymin,ymax)
        call PGCURS(cx,cy,key)
        if (key.eq.' ') then
          x2=cx
          call PG_Mark(x2,ymin,ymax)
          call PG_HLine(x1,x2,(ymin+ymax)/2.)
          nseg=nseg+1
          seg(nseg,1) = min(x1,x2)
          seg(nseg,2) = max(x1,x2)
        endif
        goto 1
      elseif (key.eq.'w') then
        nseg=1
        seg(nseg,1) = xmin
        seg(nseg,2) = xmax
      endif
      call PGSCI(1)
      return
      end


C----------------------------------------------------------------------
C Manipulate Atmospheric Absorption Star.
C
      subroutine AtmAbsCorr(row,replot)
C
      implicit none
      integer*4 row
      include 'spim2.inc'
      integer*4 i,j,k,i1,i2,neari,npxsh,nh,nv,ii,nn,zeroreg
      real*4 r1,r2,gap1,gap2,rr,valread,cx,cy,get_airmass
      real*4 y1,y2,pxsh(99),saveStarPS,hi,lo,rms,mean
      character key*1, c80*80
      logical replot
C
      common /ATMABSCORRBLK/ zeroreg
C

C Set parameters on first entry.
      if ((ObjAir.lt.1.).or.(StarAir.lt.1.)) then
        i=prim
        ObjAir = get_airmass(i)
        i=AtmIm
        StarAir= get_airmass(i)
        StarPS = 0.
      endif

C Set some defaults.
      replot=.true.
      cx = (xmin+xmax)/2.
      cy = ymin
      if (AtmIm.eq.0) then
        print *,' NO ATMOSPHERIC STAR LOADED.'
        return
      endif
      if ( (nrw(prim).ne.nrw(AtmIm)).or.
     .     (sc(prim).ne.sc(AtmIm)).or.
     .     (ec(prim).ne.ec(AtmIm)) ) then
        print *,'Dimensions of star and object images do not match.'
        return
      endif
C
1     continue
      print *,'Choices:'
      print *,'   (a) = (Re)Set parameters for current images.'
      print *,'   (b) = Set parameters.'
      print '(5x,3(a,f7.4))','   ObjAir=',ObjAir,
     .                       '   StarAir=',StarAir,
     .                       '   StarPS=',StarPS
      print *,'   (c) = Set regions outside absorption lines to 1.0 .'
      print *,'   (d) = Divide object by star (all rows).'
      print *,'   (e) = Multiply object by star (all rows).'
      print *,'   (f) = Divide object by star (current row only).'
      print *,'   (g) = Multiply object by star (current row only).'
      print *,'   (h) = Show location of atmospheric features.'
      print *,' (i/I) = Plot the result of several pixel shifts.'
      print *,'   (j) = Plot the result of several airmass variations.'
      print *,'   (q) = Quit (return to main menu.)'
      print *,' (?,/) = Show this sub-menu.'
2     continue
      print *,' Hit key (a-h,q,?) : '
      call PGCURS(cx,cy,key)
C
      if (key.eq.'q') then
        print *,'Adding header cards...'
        call fheadset('OBJAIR', dble(ObjAir), header(1))
        call fheadset('STARAIR',dble(StarAir),header(1))
        call fheadset('STARPS', dble(StarPS), header(1))
        return
C
      elseif ((key.eq.'?').or.(key.eq.'/')) then
        goto 1
C
C (a) = (Re)Set parameters for current images.
      elseif (key.eq.'a') then
        i=prim
        ObjAir = get_airmass(i)
        i=AtmIm
        StarAir= get_airmass(i)
        StarPS = 0.
        print '(5x,3(a,f7.4))','   ObjAir=',ObjAir,
     .                         '   StarAir=',StarAir,
     .                         '   StarPS=',StarPS
        replot=.false.
C
C (b) = Set parameters.
      elseif (key.eq.'b') then
        print '(a,f7.4,a,$)',' ObjAir [',ObjAir,'] = '
        read(5,'(a)') c80
        rr=valread(c80)
        if ((c80.eq.' ').or.(rr.gt.10.0).or.(rr.lt.0.5)) then
          print *,'No change.'
        else
          ObjAir=rr
        endif
        print '(a,f7.4,a,$)',' StarAir [',StarAir,'] = '
        read(5,'(a)') c80
        rr=valread(c80)
        if ((c80.eq.' ').or.(rr.gt.10.0).or.(rr.lt.0.5)) then
          print *,'No change.'
        else
          StarAir=rr
        endif
        print '(a,f7.4,a,$)',' StarPS [',StarPS,'] = '
        read(5,'(a)') c80
        rr=valread(c80)
        if ((c80.eq.' ').or.(rr.gt.+900.0).or.(rr.lt.-900.0)) then
          print *,'No change.'
        else
          StarPS=rr
        endif
        print '(5x,3(a,f7.4))','   ObjAir=',ObjAir,
     .                         '   StarAir=',StarAir,
     .                         '   StarPS=',StarPS
        replot=.false.
C
C (c) = Set regions outside absorption lines to 1.0 .
      elseif (key.eq.'c') then
        print *,'Setting regions outside absorption lines to 1.0 ...'
C Do each row.
        do j=1,nrw(AtmIm)
          r1 = w(sc(AtmIm),j,AtmIm)
          r2 = w(ec(AtmIm),j,AtmIm)
C Consider each gap between atmospheric lines.
          do k=0,naal
            if (k.eq.0) then
              gap1=0.
              gap2=aablu(k+1)
            elseif (k.eq.naal) then
              gap1=aared(k)
              gap2=99999.
            else
              gap1 = aared(k)
              gap2 = aablu(k+1)
            endif
            if ((gap2.gt.r1).and.(gap1.lt.r2)) then
              i1 = 
     .  neari( gap1 , 1+ec(AtmIm)-sc(AtmIm) , w(sc(AtmIm),j,AtmIm) )
              i1 = 
     .  max(sc(AtmIm),min(ec(AtmIm), ( i1 + sc(AtmIm) - 1 ) ))
              i2 = 
     .  neari( gap2 , 1+ec(AtmIm)-sc(AtmIm) , w(sc(AtmIm),j,AtmIm) )
              i2 = 
     .  max(sc(AtmIm),min(ec(AtmIm), ( i2 + sc(AtmIm) - 1 ) ))
              do i=i1,i2
                f(i,j,AtmIm) = 1.0
                e(i,j,AtmIm) = 1.0
              enddo
            endif
          enddo
        enddo
        replot=.true.
        modified(AtmIm)=.true.
        zeroreg=714
C
C (d) = Divide object by star (all rows.)
      elseif (key.eq.'d') then
        if (zeroreg.ne.714) print *,
     .  '**WARNING-- OTHER REGIONS NOT SET TO ONE!'
        print *,'Divide object by star (all rows.)'
        do j=1,nrw(prim)
          call AtmAbsCorr_DivMul(j,1)
          StarDivDone(j)=.true.
        enddo
        replot=.true.
        modified(prim)=.true.
C
C (e) = Multiply object by star (all rows.)'
      elseif (key.eq.'e') then
        if (zeroreg.ne.714) print *,
     .  '**WARNING-- OTHER REGIONS NOT SET TO ONE!'
        print *,'Multiply object by star (all rows.)'
        do j=1,nrw(prim)
          call AtmAbsCorr_DivMul(j,2)
          StarDivDone(j)=.false.
        enddo
        replot=.true.
        modified(prim)=.true.
C
C (f) = Divide object by star (current row only).
      elseif (key.eq.'f') then
        if (zeroreg.ne.714) print *,
     .  '**WARNING-- OTHER REGIONS NOT SET TO ONE!'
        call AtmAbsCorr_DivMul(row,1)
        StarDivDone(row)=.true.
        modified(prim)=.true.
        replot=.true.
C
C (g) = Multiply object by star (current row only).'
      elseif (key.eq.'g') then
        if (zeroreg.ne.714) print *,
     .  '**WARNING-- OTHER REGIONS NOT SET TO ONE!'
        call AtmAbsCorr_DivMul(row,2)
        StarDivDone(row)=.false.
        modified(prim)=.true.
        replot=.true.
C
C (h) = Show location of atmospheric absorption features.
      elseif (key.eq.'h') then
        y1 = ymin + (0.1*(ymax-ymin))
        y2 = ymax - (0.1*(ymax-ymin))
        call PGSCI(4)
        do i=1,naal
          r1=aablu(i)
          r2=aared(i)
          call PG_Line(r1,r2,y1,y1)
          call PG_Line(r1,r2,y2,y2)
          call PG_Line(r1,r1,y1,y2)
          call PG_Line(r2,r2,y1,y2)
          call PG_Line(r1,r2,y1,y2)
          call PG_Line(r2,r1,y1,y2)
        enddo
        call PGSCI(1)
        replot=.false.
C
C (i) = Plot the result of several pixel shifts.
      elseif ((key.eq.'i').or.(key.eq.'I')) then
        if (key.eq.'i') then
          print '(a,$)',' Enter best guess at pixel shift : '
          read(5,*) rr
          npxsh=9
          pxsh(1)=rr-0.4
          pxsh(2)=rr-0.3
          pxsh(3)=rr-0.2
          pxsh(4)=rr-0.1
          pxsh(5)=rr
          pxsh(6)=rr+0.1
          pxsh(7)=rr+0.2
          pxsh(8)=rr+0.3
          pxsh(9)=rr+0.4
        else
          print *,'Enter all desired pixel shifts (enter 99 to stop).'
          rr=0.
          npxsh=0
          do while(rr.lt.98.)
            print '(a,$)',' Pixel shift : '
            read(5,*,err=1) rr
            if (rr.lt.98.) then
              npxsh=npxsh+1
              pxsh(npxsh)=rr
            endif
          enddo
        endif
        if (npxsh.lt.3) then
          nh=1
        elseif (npxsh.lt.7) then
          nh=2
        else
          nh=3
        endif
        nv=nint(0.49+(float(npxsh)/float(nh)))
        call PGEND
        call PGBEGIN(0,'/XDISP',+1*nh,nv)
        call PGASK(.false.)
        saveStarPS=StarPS
        do i=1,npxsh
          StarPS=pxsh(i)
          call AtmAbsCorr_DivMul(row,1)
          if (bin.gt.1) call rebin()
          call plotspec(row,4)
          write(c80,'(a,f7.3)') 'Shift=',pxsh(i)
          call PGQCH(rr)
          call PGSCH(rr*3.)
          call PGMTEXT('T',0.5,0.1,0.0,c80)
C Find RMS and peak-to-peak.
          nn=0
          mean=0.
          do ii=sc(prim),ec(prim)
            if ((w(ii,row,prim).gt.xmin).and.
     .                   (w(ii,row,prim).lt.xmax)) then  
              if (e(ii,row,prim).gt.0.) then
                nn=nn+1
                arr(nn)=f(ii,row,prim)
                mean=mean+arr(nn)
              endif
            endif
          enddo
          mean=mean/float(nn)
          rms=0.
          hi=arr(1)
          lo=arr(1)
          do ii=1,nn
            rms=rms+((mean-arr(ii))*(mean-arr(ii)))
            if (arr(ii).gt.hi) hi=arr(ii)
            if (arr(ii).lt.lo) lo=arr(ii)
          enddo
          rms=sqrt(rms/float(nn))
          write(c80,'(2f9.2)',err=1) rms,hi-lo
          call PGMTEXT('T',0.5,0.5,0.0,c80)
          call PGSCH(rr)
          call AtmAbsCorr_DivMul(row,2)
        enddo
        StarPS=saveStarPS
        call pauseit()
        call PGEND
        call PGBEGIN(0,'/XDISP',1,1)
        call PGASK(.false.)
        if (bin.gt.1) call rebin()
        call plotspec(row,4)
        replot=.false.

      else
        print *,'UNKNOWN OPTION.'
        goto 1
      endif
C
C Replot spectrum if requested.
      if (replot) then
        if (bin.gt.1) call rebin()
        call plotspec(row,3)
      endif
      goto 2
      end


C----------------------------------------------------------------------
C Divide or multiply object by star for given row.
C mode=1 : divide , mode=2 : multiply.
C
      subroutine AtmAbsCorr_DivMul(row,mode)
      implicit none
      integer*4 row,mode
      include 'spim2.inc'
      integer*4 i
      real*8 airr,pix1,pix2
      real*4 bval
      if ((ObjAir.lt.1.).or.(StarAir.lt.1.)) then 
        print *,'Error-- ObjAir or StarAir is less than 1.'
        return
      endif
      airr = dble(ObjAir) / dble(StarAir)
      do i=sc(prim),ec(prim)
        pix1=dble(StarPS)+dfloat(i)-0.5d0
        pix2=dble(StarPS)+dfloat(i)+0.5d0
        call linear_fluxave(f(sc(AtmIm),row,AtmIm),sc(AtmIm),
     .           ec(AtmIm),1,1,1,pix1,pix2,bval,.true.,.false.)
        if (bval.lt.1.e-6) bval=1.0
        if (mode.eq.1) then
          f(i,row,prim) = 
     .  sngl( dble(f(i,row,prim)) / ( dble(bval) ** airr ) )
          e(i,row,prim) = 
     .  sngl( dble(e(i,row,prim)) / ( dble(bval) ** airr ) )
        elseif (mode.eq.2) then
          f(i,row,prim) = 
     .  sngl( dble(f(i,row,prim)) * ( dble(bval) ** airr ) )
          e(i,row,prim) = 
     .  sngl( dble(e(i,row,prim)) * ( dble(bval) ** airr ) )
        else
          print *,'ERROR- Unknown mode in AtmAbsCorr_DivMul...'
          call exit(1)
        endif
      enddo
      return
      end


C----------------------------------------------------------------------
      real*4 function get_airmass(im)
      implicit none
      integer*4 im
      include 'spim2.inc'
      real*8 ha,dec,lat,airmass,azimuth,tzd,azd,pa,fhead,eps
      lat= fhead('LATITUDE',header(im))
      dec= fhead('DEC',header(im))
      ha = fhead('HA',header(im))
      eps= fhead('EXPOSURE',header(im))
      ha = ha + (eps/3600.d0)
      call SkyStuff(ha,dec,lat,airmass,azimuth,tzd,azd,pa)
      get_airmass = sngl(airmass)
      return
      end


C---------------------------------------------------------------------- 
C
      subroutine CD_ops(arg,narg,cd)
C
      implicit none
      character*(*) arg(*)
      integer*4 narg
      real*4 cd(9,2)
C
      character*80 wrds(9)
      integer*4 i,n
      real*4 valread
      logical findarg
      do i=1,9
        cd(i,1)=0.
        cd(i,2)=0.
      enddo
      if (findarg(arg,narg,'cd1=',':',wrds,n)) then
        if (n.ne.2) goto 933
        cd(1,1)=valread(wrds(1))
        cd(1,2)=valread(wrds(2))
      endif
      if (findarg(arg,narg,'cd2=',':',wrds,n)) then
        if (n.ne.2) goto 933
        cd(2,1)=valread(wrds(1))
        cd(2,2)=valread(wrds(2))
      endif
      if (findarg(arg,narg,'cd3=',':',wrds,n)) then
        if (n.ne.2) goto 933
        cd(3,1)=valread(wrds(1))
        cd(3,2)=valread(wrds(2))
      endif
      if (findarg(arg,narg,'cd4=',':',wrds,n)) then
        if (n.ne.2) goto 933
        cd(4,1)=valread(wrds(1))
        cd(4,2)=valread(wrds(2))
      endif
      if (findarg(arg,narg,'cd5=',':',wrds,n)) then
        if (n.ne.2) goto 933
        cd(5,1)=valread(wrds(1))
        cd(5,2)=valread(wrds(2))
      endif
      if (findarg(arg,narg,'cd6=',':',wrds,n)) then
        if (n.ne.2) goto 933
        cd(6,1)=valread(wrds(1))
        cd(6,2)=valread(wrds(2))
      endif
      if (findarg(arg,narg,'cd7=',':',wrds,n)) then
        if (n.ne.2) goto 933
        cd(7,1)=valread(wrds(1))
        cd(7,2)=valread(wrds(2))
      endif
      if (findarg(arg,narg,'cd8=',':',wrds,n)) then
        if (n.ne.2) goto 933
        cd(8,1)=valread(wrds(1))
        cd(8,2)=valread(wrds(2))
      endif
      if (findarg(arg,narg,'cd9=',':',wrds,n)) then
        if (n.ne.2) goto 933
        cd(9,1)=valread(wrds(1))
        cd(9,2)=valread(wrds(2))
      endif
      return
933   continue
      print *,'ERROR with cd#= options...'
      call exit(1)
      end

C----------------------------------------------------------------------
C See if atmospheric absorption lines exist between two wavelengths.
      logical function AtmAbsExist(bw,rw)
      implicit none
      real*4 bw,rw
      include 'spim2.inc'
      logical ok
      integer*4 i
      if (naal.eq.0) then
        print *,'Error-- no atmospheric lines loaded.'
        AtmAbsExist = .false.
        return
      endif
      ok=.false.
      i=1
      do while ((i.le.naal).and.(.not.ok))
        if ((aablu(i).gt.bw).and.(aablu(i).lt.rw)) then
          ok=.true.
        elseif ((aared(i).gt.bw).and.(aared(i).lt.rw)) then
          ok=.true.
        elseif ((aablu(i).lt.bw).and.(aared(i).gt.rw)) then
          ok=.true.
        endif
        i=i+1
      enddo
      AtmAbsExist = ok
      return
      end

C----------------------------------------------------------------------
C Change "verbose" common parameter temporarily.
C  "mode": 0=.false., 1=.true., 2=return to previous.
C
      subroutine ForceVerbose(mode)
C
      implicit none
      integer*4 mode
      include 'verbose.inc'
      logical temp
C
      common /FORCEVERBOSEBLK/ temp
C
      if (mode.eq.0) then
        temp = verbose
        verbose=.false.  
      elseif (mode.eq.1) then
        temp = verbose
        verbose=.true.  
      elseif (mode.eq.2) then
        verbose=temp
      endif
      return
      end

C----------------------------------------------------------------------
C spim0.f
C
      subroutine spim0_main(arg,narg,ok)
C
      implicit none
      character*(*) arg(*)
      integer*4 narg
      logical ok
C
      include 'soun.inc'
C
      integer*4 nc,nr,sc,sr,er,ec,inhead,i,bin,row,nh,nv,k1,k2,k,lc
      integer*4 slw,nxtick,plotsign
      character header*57600,c200*200
      character*80 file,wrd,wrds(99),psfile
      integer*4 maxpt
      parameter(maxpt=201000)
      real*4 a(maxpt),valread,ysa(99),yea(99),xs,xe,xc,xr
      real*4 xtick,minval,minpval,maxval
      logical findarg,ps,lpr,itm,pix,both,hist,zeroline,nomin0,optimal

C Look at command line parameters.
      ok=.true.

C Clear arrays.
      do i=1,99
        ysa(i)=0.
        yea(i)=0.
      enddo
C Look for y dimension commands.
      k=0
      do i=1,narg
        if (arg(i)(1:2).eq.'y=') then
          k1=index(arg(i),',')
          arg(i)(k1:k1)=' '
          k2=index(arg(i),',')
          arg(i)(k2:k2)=' '
          k = max(1,min(99,nint(valread(arg(i)(3:k1-1)))))
          ysa(k) = valread(arg(i)(k1+1:k2-1))
          yea(k) = valread(arg(i)(k2+1:lc(arg(i))))
        endif
      enddo
      if (.not.findarg(arg,narg,'y=',':',wrds,i)) then
        if (k.gt.0) then
          write(soun,'(a)') 'Error with y= option.'
          ok=.false.
          return
        endif
      endif 
C Get other parameters.
      row= 0
      if (findarg(arg,narg,'row=',':',wrd,i)) row = nint(valread(wrd))
      bin= 1
      if (findarg(arg,narg,'bin=',':',wrd,i)) bin = nint(valread(wrd))
      bin= max(1,min(100,bin))
      nh=0
      if (findarg(arg,narg,'nh=',':',wrd,i)) nh = nint(valread(wrd))
      nh= max(0,min(9,nh))
      nv=0
      if (findarg(arg,narg,'nv=',':',wrd,i)) nv = nint(valread(wrd))
      nv= max(0,min(9,nv))
C X tick marks.
      xtick=0.
      nxtick=0
      if (findarg(arg,narg,'tick=',',',wrds,i)) then
        if (i.ne.2) then
          write(soun,'(a)') 'Error reading tick= option.'
        else
          xtick = valread(wrds(1))
          nxtick= nint(valread(wrds(2)))
        endif
      endif
C Specify x range.
      xs=-1.0
      xe=-1.0
      xc=-1.0
      xr=+2.0
      if (findarg(arg,narg,'xs=',':',wrd,i)) xs = valread(wrd)
      if (findarg(arg,narg,'xe=',':',wrd,i)) xe = valread(wrd)
      if (findarg(arg,narg,'xc=',':',wrd,i)) xc = valread(wrd)
      if (findarg(arg,narg,'xr=',':',wrd,i)) xr = valread(wrd)
      if (xc.gt.0.) then
        xs = xc - abs(xr)
        xe = xc + abs(xr)
      endif
      psfile = 'junk.ps'
      ps  = findarg(arg,narg,'-ps',':',wrd,i)
      if (findarg(arg,narg,'ps=',':',wrd,i)) then
        ps = .true.
        psfile = wrd
      endif
      lpr = findarg(arg,narg,'-lpr',':',wrd,i)
      itm = findarg(arg,narg,'-itm',':',wrd,i)
      pix = findarg(arg,narg,'-pix',':',wrd,i)
      both= findarg(arg,narg,'-both',':',wrd,i)
      hist= findarg(arg,narg,'-hist',':',wrd,i)
      nomin0 = findarg(arg,narg,'-nomin0',':',wrd,i)
      optimal= findarg(arg,narg,'-opt',':',wrd,i)
      minval = -1.e+30
      if (findarg(arg,narg,'min=',':',wrd,i)) minval = valread(wrd)
      maxval = -1.e+30
      if (findarg(arg,narg,'max=',':',wrd,i)) maxval = valread(wrd)
      minpval= -1.e+30
      if (findarg(arg,narg,'minp=',':',wrd,i)) minpval = valread(wrd)
      zeroline= findarg(arg,narg,'-zeroline',':',wrd,i)
      zeroline= .not.findarg(arg,narg,'-nozeroline',':',wrd,i)
      slw=5
      if (findarg(arg,narg,'slw=',':',wrd,i)) then
        slw = max(1,min(9,nint(valread(wrd))))
        write(soun,'(a,i5)') 'SLW=',slw
      endif
      if (lpr) ps=.true.
      plotsign=+1
      if ( findarg(arg,narg,'-reverse',':',wrd,i).or.
     .     findarg(arg,narg,'-rev',    ':',wrd,i) ) plotsign=-1
C
      if (narg.ne.1) then
        print *,
     .  'Syntax: spim0 (FITS file)   [bin=n]  [row=n]  [nh=v nv=v]'
        print *,
     .  '  [-pix]  [slw=]  [xs= xe= ]  [-both]  [-hist]  [tick=x,n]'
        print *,'  [-nozeroline]  [-ps | ps=file ]  [-lpr]  [-itm]'
        print *,'  [min=v]  [max=v]  [minp=v]  [-nomin0] [-opt]'
        print *,'  [y=row#,ymin,ymax]  [y=...]  [-rev[erse]]'
        print *,'Spectra-in-an-image plotting program.'
        print *,'   bin=n : n pixel binning.'
        print *,'   row=n : show only row n.'
        print *,
     .  '   nh=v  : v is the number of horizontal plots (default=4)'
        print *,
     .  '   nv=v  : v is the number of vertical plots (default=5)'
        print *,
     .  '   -itm  : Invert tick marks, so they are facing out.'
        print *,
     .  '   -pix  : Use pixel scale, even if wavelength calibrated.'
        print *,'  -both  : Show both wavelengths and pixel scale.'
        print *,'  -hist  : Show plot in histogram format.'
        print *,
     .  '  min=v  : Force minimum to be "v" on all plots (unless y=).'
        print *,
     .  '  max=v  : Force maximum to be "v" on all plots (unless y=).'
        print *,
     .  '  minp=v : Force minimum to be percentage of maximum value.'
        print *,
     .  '  -opt   : Optimal ymin and ymax values selected by program.'
        print *,
     .  '  y=     : Specify ymin and ymax explicitly for a given row.'
        print *,'  slw=   : Set line width in PostScript file (def=5).'
        print *,' xs= xe= : Define horizontal axis bounds to plot.'
        print *,
     .  ' tick=x,n: Set interval for x ticks(x) and # of subticks(n).'
        print *,'   -ps   : produce "junk.ps" PostScript file.'
        print *,' ps=file : produce "file" PostScript file.'
        print *,'   -lpr  : produce "junk.ps" and send to printer.'
        print *,
     .  ' -nomin0 : Use best minimum rather than default of zero.'
        print *,
     .  '-nozeroline : Do not draw a line at y=0.0 on all plots.'
        print *,
     .  '-reverse : Reverse left-right, top-bottom order of plots.'
        ok=.false.
        return
      endif
      file = arg(1)
      call AddFitsExt(file)

      if (both.and.pix) then
        write(soun,'(a)') 
     .  'ERROR- You cannot specify both -pix and -both.'
        ok=.false.
        return
      endif

C Read fits file...
      call readfits(file,a,maxpt,header,ok)
      if (.not.ok) then
        write(soun,'(a)') 'Error reading fits file.'
        ok=.false.
        return
      endif
      nc = inhead('NAXIS1',header)
      nr = inhead('NAXIS2',header)
      sc = inhead('CRVAL1',header)
      sr = inhead('CRVAL2',header)
      ec = sc + nc - 1
      er = sr + nr - 1

C Decide number of plots horizontal and vertical.
      if ((nh.le.1).and.(nv.le.1)) then
        if (nr.le.9) then
          nh=3
          nv=3
        elseif (nr.le.13) then
          nh=3
          nv=4
        elseif (nr.lt.17) then
          nh=4
          nv=4
        else
          nh=4
          nv=5
        endif
      endif

C Plot.
      call spim0(a,sc,ec,sr,er,header,bin,nh,nv,row,file,ps,itm,pix,
     .     both,hist,minval,maxval,minpval,zeroline,nomin0,optimal,
     .     ysa,yea,xs,xe,xtick,nxtick,psfile,plotsign)

C Fix SLW in PostScript file.
      if (ps.and.(slw.ne.5)) then
        open(1,file=psfile,status='old')
        open(2,file='junk.ps.2',status='unknown')
1       continue
        read(1,'(a)',end=5) c200
        if (c200(2:4).eq.'SLW') then
          i=index(c200,'5 mul')
          if (i.eq.0) then
            write(soun,'(a)') 'ERROR changing line width.'
            ok=.false.
            return
          else
            write(c200(i:i),'(i1)') slw
          endif
        endif
        if (c200.eq.' ') then
          write(2,'(a)') ' '
        else
          write(2,'(a)') c200(1:lc(c200))
        endif
        goto 1
5       continue
        close(1)
        close(2)
        write(wrd,'(2a)') 'mv junk.ps.2 ',psfile(1:lc(psfile))
        call system(wrd)
      endif
        
      if (lpr) then
        write(wrd,'(2a)') '/usr/ucb/lpr ',psfile(1:lc(psfile))
        write(soun,'(a)') wrd(1:lc(wrd))
        call system(wrd)
      endif

      return
      end


C----------------------------------------------------------------------
C Final wavelength scale solution for a given row.
C First we solve for row fit at ID points for this row to determine a possible
C offset.  Second we solve for row fits at 50 column intervals applying
C possible offset.  Finally, we solve wave=func(pix) for this row using lower
C weights for row fit solution points.
C "thres" is the pixel residual threshold for rejection (0.5px suggested).
C
C If verbose, the following PGPLOT window must be set up before calling:
C Set up plot window if verbose.
C     call PGBEGIN(0,'/XDISP',1,1)
C     call PGASK(.false.)
C     call PGVSTAND
C And this must be called afterwards:
C     call PGEND
C
C   Input: sc,ec,sr,er,row,thres,header(2)
C  Output: ord,wrms,high,npt,nrej,ok {and coef() in spim1.inc}
C
      subroutine FinalFit(sc,ec,sr,er,row,thres,orwt,header,frpsfile,
     .                    ord,wrms,high,npt,nrej,ok)

      implicit none
      integer*4 sc,ec,sr,er,row,ord,npt
      real*8 thres,wrms,high,orwt
      character*(*) header(2),frpsfile
      logical ok
C
      include 'verbose.inc'
      include 'spim1.inc'
      include 'soun.inc'
C
      real*8 wave,pix,disp,offset,wsum,polyval,r8,lowdiff,r81,r82
      real*8 coef_orf(0:20),xoff
      integer*4 narr,ii,uord,i,j,k,np,npreal,col
      integer*4 inhead,highpt,nrej,kld,bestpt,nc,nn
      real*4 median,cx,cy,rr,xx(9000),yy(9000)
      character c7*7,c68*68,key*1,c8*8

C This should never happen, but just in case.
      if ((frpsfile.ne.' ').and.(verbose)) then
        write(soun,'(a)')
     .  'ERROR frpsfile and verbose both set at the same time!'
        call exit(1)
      endif

C ESTIMATE OFFSET.
C Solve for row fits at ID points to estimate offset.
C Initialize row polynomial coeffecients.
      pix = dfloat(sc+ec)/2.d0
      uord=-1
      call QuickFitRows(sc,ec,sr,er,row,pix,uord,1,wave,disp,ok)
      if (.not.ok) then
        if (verbose) write(soun,'(a)')'Error with QuickFitRows.'
        return
      endif
C Calculate solution for each real ID.  Store residual in pixels.
      narr=0
      do ii=1,2
        do i=1,nl(row,ii)
          pix = lc(i,row,ii)
          uord=-1
          call QuickFitRows(sc,ec,sr,er,row,pix,uord,0,wave,disp,ok)
          if (.not.ok) then
            if (verbose) write(soun,'(a)')'Error with QuickFitRows.'
            return
          endif
          narr=narr+1
          arr(narr)=sngl((wave-lw(i,row,ii))/disp)
        enddo
      enddo
C Find median, apply offset.
      call find_median(arr,narr,median)
      offset=dble(median)
      if (offset.gt.0.2) then
        if (verbose) write(soun,'(a,f9.4,a)')
     .            ' Applying offset of',offset,
     .            ' pixels to row fit points.'
      endif

C LOAD IDs INTO FINAL FIT ARRAY.
      np=0
      do ii=1,2
        do i=1,nl(row,ii)
          np=np+1
          x8b(np)=lc(i,row,ii)
          y8b(np)=lw(i,row,ii)
          w8b(np)=1.d0
        enddo
      enddo
      npreal=np
C Issue possible warning.
      if (npreal.lt.8) then
        if (verbose) then
          write(soun,'(a,i3,a,i4)')
     .  ' WARNING: Only',npreal,' real IDs in row',row
        endif
      endif
 
C ADD POINTS FROM ROW FIT AT 40 PIXEL INTERVALS.  APPLY OFFSET.  WEIGHT=orwt
      nc=1+ec-sc
      do col=sc+15,ec,40
        pix = dfloat(col)
        uord= -1
        call QuickFitRows(sc,ec,sr,er,row,pix,uord,0,wave,disp,ok)
        np=np+1
        x8b(np)=pix
        y8b(np)=wave - (offset*disp)
        w8b(np)=orwt
C Increase weight for points near edges.
        if ((abs(col-sc).lt.nc/10).or.(abs(col-ec).lt.nc/10)) then
          w8b(np)=orwt*2.0
        endif
      enddo

C FIT FINAL POLYNOMIAL.
      nrej=0
      ord=6
7     continue
      do i=0,20
        coef(i)=0.d0
      enddo
      call poly_fit_glls(np,x8b,y8b,w8b,ord,coef,ok)
      if (.not.ok) then
        if (ord.eq.6) then
          ord=5
          goto 7
        endif
        write(soun,'(a)')'ERROR fitting final polynomial.'
        return
      endif
C Check residuals on real points.
      high=0.d0
      highpt=1
      wrms=0.d0
      wsum=0.d0
      do i=1,npreal
        if (w8b(i).gt.0.) then
          r8 = y8b(i) - polyval(ord+1,coef,x8b(i))
          if (abs(r8/disp).gt.thres/1.5d0) then
            if (verbose) write(soun,'(i4,5(f11.4))')
     .                     i,x8b(i),y8b(i),w8b(i),r8,r8/disp
          endif
          if (abs(r8).gt.abs(high)) then
            high=r8
            highpt=i
          endif
          wrms = wrms + (w8b(i)*r8*r8)
          wsum = wsum + w8b(i)
        endif
      enddo

C Convert deviation to pixels.
      high = high/disp
C Reject highest point?
      if (abs(high).gt.thres) then
        i=highpt
        w8b(i)=0.d0
        nrej=nrej+1
        if (verbose) write(soun,'(a,3(f11.4))')
     .  ' Rejecting:',x8b(i),y8b(i),high
        goto 7
      endif
C Weighted-root-mean-square. Convert to pixels.
      if (wsum.lt.1.d-20) then
        wrms=0.
      else
        wrms = sqrt(wrms/wsum)/disp
      endif
      if (wrms.gt.1.0) then
        if (ord.eq.6) then
          ord=5
          goto 7
        endif
        if (verbose) then
          write(soun,'(a)')
     .  'ERROR- residuals too high when fitting final polynomial.'
        endif
        ok=.false.
        return
      endif
      npt=npreal-nrej

C Look for deviations from other row fits by fitting a polynomial to these
C points and comparing against real IDs.
      nn=np-npreal
      j =npreal+1
      k = 10           ! This is the polynomial order number (start at 10th).
61    continue
      if (k.gt.20) goto 71    ! Go up to 20th order (maximum).
C
C Old fitting.. replace with poly_fit_gj so can go to higher order. tab 13apr99
C     call poly_fit_glls(nn,x8b(j),y8b(j),w8b(j),k,coef_orf,ok)
C
C New fitting routine.
      call poly_fit_gj(nn,x8b(j),y8b(j),w8b(j),k,coef_orf,xoff,0,ok)
C
C Check fit. Try again with higher order if fit fails.
      r81=0.d0
      do i=j,np
        r8 = abs( (y8b(i)-polyval(k+1,coef_orf,x8b(i)-xoff)) / disp )
        if (r8.gt.r81) r81=r8
      enddo
C Tolerance of 0.02 pixels in fit to "fake" lines (from other rows).
      if (r81.gt.0.02) ok=.false.
      if (.not.ok) then
        k=k+1
        goto 61
      endif
C Check real IDs.
      do i=1,npreal
        if (w8b(i).gt.orwt) then
          r8 = (y8b(i)-polyval(k+1,coef_orf,x8b(i)-xoff)) / disp
          if (abs(r8).gt.thres) then
            w8b(i)=orwt*0.8d0
            if (verbose) then
              write(soun,'(a,4f10.3)')
     .  ' Lower weight on : ',x8b(i),y8b(i),w8b(i),r8
            endif
            ok=.false.
          endif
        endif
      enddo
C Redo entire fit.
      if (.not.ok) goto 7

C:::::::::
      IF (frpsfile.ne.' ') THEN
      call PGPAGE
      call PGWINDOW(float(sc),float(ec),-0.44,+0.44)
      call PGBOX('BCNST',0.,0,'BCNST',0.,0)
C Plot zero line.
      xx(1)=float(sc)
      xx(2)=float(ec)
      yy(1)=0.
      yy(2)=0.
      call PGLINE(2,xx,yy)
C Plot real data points.
      nn=0
      do i=1,npreal
        nn=nn+1
        xx(nn)=sngl( x8b(i) )
        yy(nn)=sngl((y8b(i)-polyval(ord+1,coef,dble(xx(nn)))) / disp)
        yy(nn)=max(-0.4,min(0.4,yy(nn)))
      enddo
C Circles.
      call PGPT(nn,xx,yy,4)
C Plot X's over points with weight < 0.0001 .
      nn=0
      do i=1,npreal
        if (w8b(i).lt.0.0001) then
          nn=nn+1
          xx(nn)=sngl( x8b(i) )
          yy(nn)=sngl((y8b(i)-polyval(ord+1,coef,dble(xx(nn)))) / disp)
          yy(nn)=max(-0.4,min(0.4,yy(nn)))
        endif
      enddo
      call PGQCH(rr)
      call PGSCH(rr*2.)
      if (nn.gt.0) call PGPT(nn,xx,yy,5)
      call PGSCH(rr)
C Plot up arrows over large positive residuals.
      nn=0
      do i=1,npreal
        rr = sngl( (y8b(i)-polyval(ord+1,coef,x8b(i))) / disp )
        if (rr.gt.0.4) then
          nn=nn+1
          xx(nn) = sngl( x8b(i) )
          yy(nn) = 0.4
        endif
      enddo
      call PGQCH(rr)
      call PGSCH(rr*2.)
      if (nn.gt.0) call PGPT(nn,xx,yy,30)
      call PGSCH(rr)
C Plot down arrows over large positive residuals.
      nn=0
      do i=1,npreal
        rr = sngl( (y8b(i)-polyval(ord+1,coef,x8b(i))) / disp )
        if (rr.lt.-0.4) then
          nn=nn+1
          xx(nn) = sngl( x8b(i) )
          yy(nn) = -0.4
        endif
      enddo
      call PGQCH(rr)
      call PGSCH(rr*2.)
      if (nn.gt.0) call PGPT(nn,xx,yy,31)
      call PGSCH(rr)
C Plot data points derived from fits to other rows.
      nn=0
      do i=npreal+1,np
        nn=nn+1
        xx(nn)=sngl( x8b(i) )
        yy(nn)=sngl((y8b(i)-polyval(ord+1,coef,dble(xx(nn)))) / disp)
        yy(nn)=max(-0.7,min(0.7,yy(nn)))
      enddo
      call PGPT(nn,xx,yy,-1)
C Labelling.
      write(c68,'(3i5,2f8.3)') row,ord,npt,max(-1.,min(9.,wrms)),
     .                                    max(-1.,min(9.,high))
      call PGMTEXT('T',0.4,0.5,0.5,c68)
      ENDIF
C:::::::::

C:::::::::
71    continue
      IF (verbose.and.pgd) THEN
      call PGPAGE
      call PGWINDOW(float(sc),float(ec),-0.72,+0.72)
      call PGBBUF
      call PGBOX('BICNST',0.,0,'BICNST',0.,0)
C Plot zero line.
      xx(1)=float(sc)
      xx(2)=float(ec)
      yy(1)=0.
      yy(2)=0.
      call PGSLS(4)
      call PGLINE(2,xx,yy)
      call PGSLS(1)
C Plot real data points with weight > 0.99 .
      nn=0
      do i=1,npreal
        if (w8b(i).gt.0.99) then
          nn=nn+1
          xx(nn)=sngl( x8b(i) )
          yy(nn)=sngl((y8b(i)-polyval(ord+1,coef,dble(xx(nn)))) / disp)
          yy(nn)=max(-0.7,min(0.7,yy(nn)))
        endif
      enddo
      call PGSCI(3)
      call PGPT(nn,xx,yy,3)
C Plot real data points with weight < 0.01 .
      nn=0
      do i=1,npreal
        if (w8b(i).lt.0.01) then
          nn=nn+1
          xx(nn)=sngl( x8b(i) )
          yy(nn)=sngl((y8b(i)-polyval(ord+1,coef,dble(xx(nn)))) / disp)
          yy(nn)=max(-0.7,min(0.7,yy(nn)))
        endif
      enddo
      call PGSCI(2)
      call PGPT(nn,xx,yy,3)
      call PGSCI(1)
C Plot real data points with:  0.009 < weight < 0.991 .
      nn=0
      do i=1,npreal
        if ((w8b(i).gt.0.009).and.(w8b(i).lt.0.991)) then
          nn=nn+1
          xx(nn)=sngl( x8b(i) )
          yy(nn)=sngl((y8b(i)-polyval(ord+1,coef,dble(xx(nn)))) / disp)
          yy(nn)=max(-0.7,min(0.7,yy(nn)))
        endif
      enddo
      call PGSCI(4)
      call PGPT(nn,xx,yy,3)
      call PGSCI(1)
C Plot data points derived from fits to other rows.
      nn=0
      do i=npreal+1,np
        nn=nn+1
        xx(nn)=sngl( x8b(i) )
        yy(nn)=sngl((y8b(i)-polyval(ord+1,coef,dble(xx(nn)))) / disp)
        yy(nn)=max(-0.7,min(0.7,yy(nn)))
      enddo
      call PGSCI(8)
      call PGPT(nn,xx,yy,6)
      call PGSCI(1)
C Labelling.
      call PGMTEXT('L',2.0,0.5,0.5,'Residual (pixels)')
      call PGMTEXT('B',2.5,0.5,0.5,'Pixel')
      write(c8,'(a,i4)') 'ROW=',row
      call PGMTEXT('T',2.0,0.5,0.5,c8)
      call PGEBUF
C Ask user for interaction.
      print '(a,a,$)',c8,'  Interactive rejections? (1/0/-) [0] : '
      read(5,'(a)',err=71) c8
      if (c8.eq.' ') c8='0'
      if ((npreal.gt.0).and.(c8(1:1).ne.'0')) then
        if (c8.eq.'-') then
          i=-1
          goto 75
        endif
73      continue
        print '(a)',
     .  ' Enter line for weight change. (0=show list,-1=use mouse,'
        print '(a,$)',
     .  '  -2=change threshold,-3=change orwt,-4=continue) : ' 
        read(5,*,err=73) i
75      continue
        if (i.eq.0) then
          do i=1,npreal
            r8 = y8b(i) - polyval(ord+1,coef,x8b(i))
            print '(i4,5(f11.4))',i,x8b(i),y8b(i),w8b(i),r8,r8/disp
          enddo
          goto 73
        elseif (i.eq.-1) then
          print '(a)',
     .  ' Hit LMB on point to change weight. RMB to continue.'
76        continue
          call GetKey(cx,cy,key)
          if (key.ne.'A') goto 7
C Find point.
          r8 = (y8b(1)-polyval(ord+1,coef,x8b(1)))/disp
          r82 = (abs(dble(cx)-x8b(1))/1000.d0)+abs(dble(cy)-r8)
          bestpt=1
          do i=2,npreal
            r8 = (y8b(i)-polyval(ord+1,coef,x8b(i)))/disp
            r81= (abs(dble(cx)-x8b(i))/1000.d0)+abs(dble(cy)-r8)
            if (r81.lt.r82) then
              r82=r81
              bestpt=i
            endif
          enddo
          i=bestpt
C Change weight and change color.
          if (w8b(i).lt.0.5) then
            w8b(i)=1.
            nrej=nrej-1
            call PGSCI(3)
          else
            w8b(i)=0.
            nrej=nrej+1
            call PGSCI(2)
          endif
          xx(1)=sngl(x8b(i))
          yy(1) = sngl((y8b(i)-polyval(ord+1,coef,dble(xx(1)))) / disp)
          yy(1) = max(-0.7,min(0.7,yy(1)))
          call PGPT(1,xx,yy,3) 
          call PGSCI(1)
          r8 = y8b(i)-polyval(ord+1,coef,x8b(i))
          print '(i4,5(f11.4))',i,x8b(i),y8b(i),w8b(i),r8,r8/disp
          goto 76
        elseif (i.eq.-2) then
          print *,'thres=',sngl(thres)
          print '(a,$)',' New threshold : '
          read(5,*,err=73) r8
          thres = max(0.01,min(10.0,r8))
          print *,'thres=',sngl(thres)
          goto 7
        elseif (i.eq.-3) then
          print *,'orwt=',sngl(orwt)
          print '(a,$)',' New "other row fit points" weight : '
          read(5,*,err=73) r8
          orwt = max(0.0001,min(100.0,r8))
          print *,'orwt=',sngl(orwt)
          do i=npreal+1,np
            w8b(i)=orwt
            if ((abs(nint(x8b(i))-sc).lt.nc/10).or.
     .          (abs(nint(x8b(i))-ec).lt.nc/10)) then
              w8b(i)=orwt*2.0
            endif
          enddo
          goto 7
        elseif (i.gt.0) then
          i=min(i,npreal)
          r8 = y8b(i)-polyval(ord+1,coef,x8b(i))
          print '(i4,5(f11.4))',i,x8b(i),y8b(i),w8b(i),r8,r8/disp
          print '(a,i3,a,$)',' Enter new weight for line#',i,' : '
          read(5,*,err=73) r8
          if (abs(w8b(i)-r8).gt.0.0001) then
            w8b(i)=r8
            if (r8.lt.0.1) nrej=nrej+1
            goto 7
          endif
        endif
      endif
      ENDIF
C:::::::::

C Warn if no real points left.
      if (npreal-nrej.le.0) then
        write(soun,'(a)') ' WARNING: No real IDs left.'
        wrms=0.d0
        high=0.d0
        npt =0
      endif

C ADD POLYNOMIAL COEFFECIENTS TO HEADER.
      write(c68,'(4(1pe17.9))') coef(0),coef(1),coef(2),coef(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      j=inhead('NAXIS',header(2))
      call cheadset(c7,c68,header(1))
      if (j.eq.2) call cheadset(c7,c68,header(2))
      write(c68,'(4(1pe17.9))') coef(4),coef(5),coef(6),coef(7)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      call cheadset(c7,c68,header(1))
      if (j.eq.2) call cheadset(c7,c68,header(2))
      newim=.true.

C Set final fit residual and weight for later output.
      do ii=1,2
        do i=1,nl(row,ii)
C Find line.
          lowdiff=1.d+30
          kld=1
          do k=1,npreal
            if (abs(lc(i,row,ii)-x8b(k)).lt.lowdiff) then
              kld=k
              lowdiff = abs(lc(i,row,ii)-x8b(k))
            endif
          enddo
C Record if found.
          if (lowdiff.lt.0.0001) then
            lres(i,row,ii) =
     .           ( y8b(kld) - polyval(ord+1,coef,x8b(kld)) ) / disp
            lwgt(i,row,ii) = w8b(kld)
          endif
        enddo
      enddo

      return
      end




C----------------------------------------------------------------------
C List .log files.  Use listfile=' ', to list all files.
C
      subroutine mk_logsum_list(minexpos,listfile)
C
      implicit none
      integer*4 minexpos      ! Min. exposure time for inclusion (input)
      character*(*) listfile  ! File with list of log files (input)
C
      character*80 wrd,file
      integer*4 lc
C
      call mk_logsum(' ',0,0)
      if (listfile.eq.' ') then
        call system('/bin/rm -f mk_logsum.list')
        call system('/bin/ls *.log > mk_logsum.list')
        file='mk_logsum.list'
      else
        file=listfile
      endif
      open(3,file=file,status='old')
1     read(3,'(a)',end=9) wrd 
      print *,'Reading log file ',wrd(1:lc(wrd))
      call mk_logsum(wrd,minexpos,1)
      goto 1
9     continue
      if (listfile.eq.' ') then
        close(3,status='delete')
      else
        close(3)
      endif
      call mk_logsum(' ',0,2)
C
      return
      end


C----------------------------------------------------------------------
C makee .log files quality analysis.
C Plot quantities such as cosmic rays rejected, etc.
C
      subroutine mk_logsum(logfile,minexpos,mode)
C
      implicit none
      character*(*) logfile     ! Filename of .log file (input).
      integer*4 minexpos        ! Min. exposure time for inclusion (input).
      integer*4 mode            ! Mode: 0 = initialize.  (input).
C                               !       1 = load data from log file.
C                               !       2 = write plot file.
      include 'soun.inc'
      include 'global.inc'
C
      integer*4 mob,meo
      parameter(mob=900,meo=90)
      integer*4 obs(mob),neo(mob),nob,expos(mob)
      real*4 pm_pri(meo,mob),pm_adj(meo,mob),pm_add(meo,mob)
      real*4 pm_crf(meo,mob)
      real*4 pm_high(meo,mob),pm_low(meo,mob),xx(meo),yy(meo),xlim
      real*4 xmin,xmax,ymin,ymax
      character*80 lf(mob)
      character*200 line
      integer*4 lc,i,j,totneo,nn,slw
      real*4 valread,sch
      real*8 fgetlinevalue

C Initialize.
      if (mode.eq.0) then
        nob=0
        return
      endif
      sch=1.9
      slw=2

C Load.
      IF (mode.eq.1) THEN

C Number of observations.
      nob=nob+1
      neo(nob)=0
C Open file.
      open(1,file=logfile,status='old')
      lf(nob) = logfile

C Look for MAKEE.
10    read(1,'(a)',end=50) line
      if (index(line,'.... MAKEE: Keck').gt.0) goto 1
      goto 10
C Return if not found.
50    continue
      close(1)
      return

C Find obs# and exposure time...
1     continue
      Global_HIRES = .false.
      Global_ESI   = .false.
      if (index(line,'HIRES').gt.0) Global_HIRES = .true.
      if (index(line,'ESI')  .gt.0) Global_ESI   = .true.
C
      read(1,'(a)',end=901) line
      if (index(line,'Object to extract:').gt.0) then
        read(1,'(a)',end=902) line
        obs(nob)  = nint(valread(line(52:55)))
        expos(nob)= nint(valread(line(57:61)))
        goto 11
      endif
      goto 1

11    continue
C Reject.
      if (expos(nob).lt.minexpos) then
        write(soun,'(a,i6,2a)') 
     .      'Short exposure:',expos(nob),'s, ignore: ',
     .      logfile(1:lc(logfile))
        nob=nob-1
        return
      endif

C Find mode.
      read(1,'(a)',end=9011) line
      if (line(1:5).eq.'Mode=') then
        i = nint(valread(line(6:8)))
        if (i.eq.4) then
          nob=nob-1
          write(soun,'(2a)') 
     .  'Mode is 4 ignore: ',logfile(1:lc(logfile))
          return
        endif
        goto 2
      endif
      goto 11

C Find echelle orders.
2     read(1,'(a)',end=5) line
      if (index(line,'Extracting echelle order').gt.0) then
        neo(nob) = neo(nob) + 1
        totneo = nint( fgetlinevalue(line,2) )
C
        read(1,'(a)',end=903) line
        do while(index(line,'Cosmic Ray Fraction:').eq.0)
          read(1,'(a)',end=904) line
        enddo
        pm_crf(neo(nob),nob) = 
     .    sngl( fgetlinevalue(line,2) / fgetlinevalue(line,1) )
C
        read(1,'(a)',end=903) line
        do while(index(line,'Sky Pixels Masked').eq.0)
          read(1,'(a)',end=904) line
        enddo
        pm_pri(neo(nob),nob) = sngl( fgetlinevalue(line,1) )
        pm_adj(neo(nob),nob) = sngl( fgetlinevalue(line,2) )
        pm_add(neo(nob),nob) = sngl( fgetlinevalue(line,3) )
C
        read(1,'(a)',end=905) line
        do while( (index(line,'Object Pixels Masked').eq.0).and.
     .            (index(line,'Object flux straight').eq.0) )
          read(1,'(a)',end=906) line
        enddo
        if (index(line,'Object Pixels Masked').gt.0) then
          pm_high(neo(nob),nob)= sngl( fgetlinevalue(line,1) )
          pm_low(neo(nob),nob) = sngl( fgetlinevalue(line,2) )
        else
          pm_high(neo(nob),nob)= 0.
          pm_low(neo(nob),nob) = 0.
        endif
C Last one...
        if (neo(nob).eq.totneo) goto 5
        goto 2
      endif
      goto 2
5     continue
      close(1)

      return
      ENDIF

C Plot.
      IF (mode.eq.2) THEN

C Write table file.
      write(soun,'(a)') 'Writing: logsum.tbl'
      open(1,file='logsum.tbl',status='unknown')
      write(1,'(2x)')
      write(1,'(a,i5)') 'Total number of log files:',nob
      write(1,'(2x)')
      do j=1,nob
        write(1,'(2x)')
        write(1,'(a,i5,a,i5,2a,i5,a)') 
     .  'Obs#:',obs(j),'   # of orders:',neo(j),
     .  '    Log: ',lf(j)(1:lc(lf(j))),expos(j),'s'
        write(1,'(a)') 
     .  'Order Sky:Pri Sky:Adj Sky:Add Obj:High Obj:Low  CRF'
        do i=1,neo(j)
          write(1,'(i4,5i8,f7.2)') 
     .  i,nint(pm_pri(i,j)),nint(pm_adj(i,j)),
     .  nint(pm_add(i,j)),nint(pm_high(i,j)),
     .  nint(pm_low(i,j)),pm_crf(i,j)
        enddo
      enddo
      close(1)

C Plotting.
      write(soun,'(a)') 'Writing: logsum.ps'
      call PGBEGIN(0,'logsum.ps/PS',3,2)
C     call PGBEGIN(0,'/XDISP',3,2)
      call PGASK(.false.)
      call PGSLW(slw)
      call PGSCH(sch)
      call PGSVP(0.20,0.95,0.15,0.9)

C Fixed ranges.
      ymin = 0.
      ymax = float(nob) + 1.
      xmin = 0.

C Sky:Primary
      xmax = 1.
      do j=1,nob
        do i=1,neo(j)
          if (pm_pri(i,j).gt.xmax) xmax=pm_pri(i,j)
        enddo
      enddo
      xmax = xmax*1.06
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBBUF
      call PGBOX('BCNST',0.,0,'BCST',0.,0)
      call ls_label_obs(nob,obs,sch)
      do j=1,nob
        do i=1,neo(j)
          xx(i) = pm_pri(i,j)
          yy(i) = float(j)
        enddo
        call PGPT(neo(j),xx,yy,4)
      enddo
      call PGMTEXT('T',0.5,0.5,0.5,'Sky: Primary')
      call ls_labels()
      call PGEBUF

C Sky:Adjacent
      xmax = 1.
      do j=1,nob
        do i=1,neo(j)
          if (pm_adj(i,j).gt.xmax) xmax=pm_adj(i,j)
        enddo
      enddo
      xmax = xmax*1.06
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBBUF
      call PGBOX('BCNST',0.,0,'BCST',0.,0)
      call ls_label_obs(nob,obs,sch)
      do j=1,nob
        do i=1,neo(j)
          xx(i) = pm_adj(i,j)
          yy(i) = float(j)
        enddo
        call PGPT(neo(j),xx,yy,4)
      enddo
      call PGMTEXT('T',0.5,0.5,0.5,'Sky: Adjacent')
      call ls_labels()
      call PGEBUF

C Sky:Additional
      xmax = 1.
      do j=1,nob
        do i=1,neo(j)
          if (pm_add(i,j).gt.xmax) xmax=pm_add(i,j)
        enddo
      enddo
      xmax = xmax*1.06
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBBUF
      call PGBOX('BCNST',0.,0,'BCST',0.,0)
      call ls_label_obs(nob,obs,sch)
      do j=1,nob
        do i=1,neo(j)
          xx(i) = pm_add(i,j)
          yy(i) = float(j)
        enddo
        call PGPT(neo(j),xx,yy,4)
      enddo
      call PGMTEXT('T',0.5,0.5,0.5,'Sky: Additional')
      call ls_labels()
      call PGEBUF

C Sky:Cosmic Ray Fraction
      xmax = 1.
      do j=1,nob
        do i=1,neo(j)
          if (pm_crf(i,j).gt.xmax) xmax=pm_crf(i,j)
        enddo
      enddo
      xmax = xmax*1.06
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBBUF
      call PGBOX('BCNST',0.,0,'BCST',0.,0)
      call ls_label_obs(nob,obs,sch)
      do j=1,nob
        do i=1,neo(j)
          xx(i) = pm_crf(i,j)
          yy(i) = float(j)
        enddo
        call PGPT(neo(j),xx,yy,4)
      enddo
      call PGMTEXT('T',0.5,0.5,0.5,'Cosmic Rays: Actual/Expected')
      call ls_labels()
      call PGEBUF

C Object:High
      xmax = 1.
      do j=1,nob
        do i=1,neo(j)
          if (pm_high(i,j).gt.xmax) xmax=pm_high(i,j)
        enddo
      enddo
      xmax = xmax*1.06
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBBUF
      call PGBOX('BCNST',0.,0,'BCST',0.,0)
      call ls_label_obs(nob,obs,sch)
      do j=1,nob
        do i=1,neo(j)
          xx(i) = pm_high(i,j)
          yy(i) = float(j)
        enddo
        call PGPT(neo(j),xx,yy,4)
C Warning markers.
        nn=0
        xlim = float(expos(j)+60)/6.
        do i=1,neo(j)
          if (pm_high(i,j).gt.xlim) then
            nn=nn+1
            xx(nn) = pm_high(i,j)
            yy(nn) = float(j)
          endif
          call PGSLW(slw*2)
          call PGSCI(2)
          call PGPT(nn,xx,yy,17)
          call PGSCI(1)
          call PGSLW(slw)
        enddo
      enddo
      call PGMTEXT('T',0.5,0.5,0.5,'Pixels Masked in Object: High')
      call ls_labels()
      call PGEBUF

C Object:Low
      xmax = 1.
      do j=1,nob
        do i=1,neo(j)
          if (pm_low(i,j).gt.xmax) xmax=pm_low(i,j)
        enddo
      enddo
      xmax = xmax*1.06
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBBUF
      call PGBOX('BCNST',0.,0,'BCST',0.,0)
      call ls_label_obs(nob,obs,sch)
      do j=1,nob
        do i=1,neo(j)
          xx(i) = pm_low(i,j)
          yy(i) = float(j)
        enddo
        call PGPT(neo(j),xx,yy,4)
      enddo
      call PGMTEXT('T',0.5,0.5,0.5,'Pixels Masked in Object: Low')
      call ls_labels()
      call PGEBUF

      call PGEND

      ENDIF
        
      return
901   write(soun,'(a)') 'Error: 901'
      goto 900
9011  write(soun,'(a)') 'Error: 9011'
      goto 900
9012  write(soun,'(a)') 'Error: 9012'
      goto 900
902   write(soun,'(a)') 'Error: 902'
      goto 900
903   write(soun,'(a)') 'Error: 903'
      goto 900
904   write(soun,'(a)') 'Error: 904'
      goto 900
905   write(soun,'(a)') 'Error: 905'
      goto 900
906   write(soun,'(a)') 'Error: 906'
      goto 900
900   continue
      write(soun,'(2a)') 
     .  'Error reading log file: ',logfile(1:lc(logfile))
      return
      end


C----------------------------------------------------------------------
C Observation number labels.
C
      subroutine ls_label_obs(nob,obs,sch)
C
      implicit none
      integer*4 nob,obs(nob),i,inc
      character*3 c3
      real*4 sch
C
      if (nob.gt.100) then
        inc = 5
      elseif (nob.gt.60) then
        inc = 4
      elseif (nob.gt.40) then
        inc = 3
      elseif (nob.gt.20) then
        inc = 2
      else
        inc = 1
      endif
C
      call PGSCH(1.5)
      do i=1,nob,inc
        write(c3,'(i3)') obs(i)
        call PGPTEXT(0.,float(i)-(nob*0.02),0.,1.4,c3)
      enddo
      call PGSCH(sch)
C
      return
      end

C----------------------------------------------------------------------
      subroutine ls_labels()
      call PGMTEXT('B',2.3,0.5,0.5,'Pixels Masked')
      call PGMTEXT('L',3.0,0.5,0.5,'Observation number')
      return
      end


C----------------------------------------------------------------------
C "$" option...
C
Ctemp
C           Set this to OLD_ before, why?
Ctemp
      subroutine spim_ScaleCurrentRow(row)
Ctemp
Ctemp
C
      implicit none
      integer*4 row
C
      include 'spim2.inc'
C
      real*8 r8,accum
      integer*4 i,j,k
C
7001  print '(a,$)',' Value to scale this row by (-1=continuous): '
      read(5,*,err=7001) r8
      if (r8.lt.0.) then
        accum=1.d0
        print *,
     .  'Use -1 to stop, -2 to stop and scale other rows also.'
7003    print '(a,1pe12.5,a,$)',
     .  '(Accum=',accum,') Value to scale this row by: '
        read(5,*,err=7003) r8
        if (r8.gt.0.) then
          accum = accum * r8
          do i=sc(prim),ec(prim)
            f(i,row,prim) = sngl( dble(f(i,row,prim)) * r8 )
            if (e(i,row,prim).gt.0.) then
              e(i,row,prim) = sngl( dble(e(i,row,prim)) * r8 )
            endif
          enddo
          modified(prim)=.true.
          call rebin()
          call plotspec(row,3)
          goto 7003
        elseif (nint(r8).eq.-2) then
          print *,'Accumulated scaling factor=',accum
7005      print '(a,$)',
     .  ' Scale lower rows(1), higher rows(2), both(3) : '
          read(5,*,err=7005) k
          r8=accum
          if ((k.eq.2).or.(k.eq.3)) then
            do j=row+1,nrw(prim) 
              do i=sc(prim),ec(prim)
                f(i,j,prim) = sngl( dble(f(i,j,prim)) * r8 )
                if (e(i,j,prim).gt.0.) then
                  e(i,j,prim) = sngl( dble(e(i,j,prim)) * r8 )
                endif
              enddo
            enddo
          endif
          if ((k.eq.1).or.(k.eq.3)) then
            do j=1,row-1
              do i=sc(prim),ec(prim)
                f(i,j,prim) = sngl( dble(f(i,j,prim)) * r8 )
                if (e(i,j,prim).gt.0.) then
                  e(i,j,prim) = sngl( dble(e(i,j,prim)) * r8 )
                endif
              enddo
            enddo
          endif
        else
          print *,'Accumulated scaling factor=',accum
        endif
        modified(prim)=.true.
        call rebin()
      else
        do i=sc(prim),ec(prim)
          f(i,row,prim) = sngl( dble(f(i,row,prim)) * r8 )
          if (e(i,row,prim).gt.0.) then
            e(i,row,prim) = sngl( dble(e(i,row,prim)) * r8 )
          endif
        enddo
        modified(prim)=.true.
        call rebin()
      endif
C
      return
      end

C----------------------------------------------------------------------
C "@" option...
C
Ctemp
C           Set this to OLD_ before, why?
Ctemp
      subroutine spim_RowScale(row)
Ctemp
Ctemp
C
      implicit none
      integer*4 row
C
      include 'spim2.inc'
C
      real*8 sf
      integer*4 i,oim,oir1,oir2
C
C Check.
      print *,'nim=',nim
      if (nim.ne.2) then
        print *,'ERROR: must have two images for this routine.'
        return
      endif

C Other image.
      if (prim.eq.1) oim=2
      if (prim.eq.2) oim=1

C Scale factor.
      sf = 1.0

C Ask?
701   print '(a,$)','New scaling factor: '
      read(5,*,err=701) sf

C Bracketting rows.
      call spim_OtherImageAdjacentRows(row,oim,oir1,oir2)
      print *,'Bracketting rows in other image: ',oir1,oir2

C Scale it.
      print *,'Scaling factor: ',sf
      do i=sc(prim),ec(prim)
        f(i,row,prim) = sngl( dble(f(i,row,prim)) * sf )
        if (e(i,row,prim).gt.0.) then
          e(i,row,prim) = sngl( dble(e(i,row,prim)) * sf )
        endif
      enddo
      modified(prim)=.true.
      call rebin()
C
      return
      end


C----------------------------------------------------------------------
C Find bracketting rows.
C
Ctemp
C           Set this to OLD_ before, why?
Ctemp
      subroutine spim_OtherImageAdjacentRows(row,oim,oir1,oir2)
Ctemp
Ctemp
C
      implicit none
      integer*4 row            ! primary image and current row (input)
      integer*4 oim            ! other image (input)
      integer*4 oir1,oir2      ! other image adjacent rows (output)
C
      include 'spim2.inc'
C
      real*4 sw,ew
      integer*4 j
C
C Find rows that bracket starting and ending wavelengths.
      sw = w(1,row,prim)
      ew = w(nc(prim),row,prim)
Ctemp
      print *,'sw,ew,prim,oim=',sw,ew,prim,oim
      do j=1,nrw(oim)
        print *,'row,s,e,nc=',j,w(1,j,oim),w(nc(oim),j,oim),nc(oim)
      enddo
Ctemp
      do j=1,nrw(oim)
        if ((w(1,j,oim).lt.sw).and.(w(nc(oim),j,oim).gt.sw)) oir1=j
        if ((w(1,j,oim).lt.ew).and.(w(nc(oim),j,oim).gt.ew)) oir2=j
      enddo
C
C Check.
      if (oir1.eq.oir2) then
        print *,'WARNING: bracketting rows the same : ',oir1,oir2
      endif
      if (abs(oir1-oir2).ne.1) then
        print *,'WARNING: bracketting rows not adjacent : ',oir1,oir2
      endif
C
      return
      end


C----------------------------------------------------------------------
C Draw and list nearby lines.
C
      subroutine spim1_nearby_guesses(row,cx,wave,key)
C
      implicit none
      integer*4 row
      real*4 cx
      real*8 wave,pix
      character*1 key
C
      include 'spim1.inc'
C
      real*4 spim1_median_dispersion
      real*4 xmin,xmax,ymin,ymax,sch
      real*8 col,disp,r81
      integer*4 ii
      character c11*11
C
C Window size.
      call PGQWIN(xmin,xmax,ymin,ymax)
C Set.
      pix = cx
C Echo.
      print *,'   pix = ',pix,'    wave = ',wave
C Find dispersion.
      disp = spim1_median_dispersion(row,cx)
      print '(a,f9.5)','Median nearby dispersion (Ang/pix): ',disp
      if (key.eq.'4') then
        print *,'Enter your own dispersion in plot window...'
        call PG_ReadPlotReal(r81,1.2,'T',0.5,0.4)
        disp = r81
        print '(a,f9.5)','Using user dispersion (Ang/pix): ',disp
      endif
C
C List nearby lines and there pixel position guesses.
C
      call PGQCH(sch)
      call PGSCH(sch*0.6)
      call PGSLS(4)
      do ii=1,nthar
        col = pix + ( (thar(ii) - wave) / disp )
        if ((col.gt.xmin).and.(col.lt.xmax)) then
CCC       print '(a,f9.3,a,f10.3)','col=',col, '  line=',thar(ii)
          call mark_line(sngl(col),2)
          write(c11,'(f9.3,2x)') thar(ii)
          call PGPTXT( sngl(col) + (0.0045*(xmax-xmin)),
     .                 ymax-(0.15*(ymax-ymin)), 90., 0., c11)
        endif
      enddo
      call PGSLS(1)
      call PGSCH(sch)
C
      return
      end



C----------------------------------------------------------------------
C Find a "median" dispersion for line IDs in given row near given pixel.
C
      real*4 function spim1_median_dispersion(row,cx)
C
      implicit none
      integer*4 row
      real*4 cx
C
      include 'spim1.inc'
C
      real*8 pix,pixdist
      integer*4 ii,nn
C
      pix = dble(cx)
      nn = 0
      pixdist = 500.
      do while ((nn.lt.7).and.(pixdist.lt.4000.))
        nn=0
        do ii=1,nl(row,1)-1
          if (abs(lc(ii,row,1) - pix).lt.pixdist) then
            nn=nn+1
            arr(nn) = ( lw(ii+1,row,1) - lw(ii,row,1) ) / 
     .                ( lc(ii+1,row,1) - lc(ii,row,1) )
          endif
        enddo
        pixdist = pixdist + 100.
      enddo
C
      if (nn.lt.2) then
        print *,'ERROR: Too few lines for median dispersion guess.'
        spim1_median_dispersion = -1.
        return
      endif
C
C Echo.
      do ii=1,nn
        print '(a,i3,a,f9.4)','disp[',ii,']=',arr(ii)
      enddo
C Median.
      call find_median(arr,nn,spim1_median_dispersion)
C
      return
      end

