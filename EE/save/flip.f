C flip.f                                                    tab  Feb98
C
C Rotate a 2-D image in increments of 90 degrees.
C
      implicit none
C
      include 'maxpix.inc'
      include 'soun.inc'
C
      real*4 a(maxpix)
      integer*4 i,narg,lc
      character*57600 header
      character*80 arg(9),infile,outfile,wrd,flip
      logical ok,findarg

C Standard output unit number.
      soun = 6

C Command line arguments.
      call arguments(arg,narg,9)
      flip='None'
      if (findarg(arg,narg,'flip=',',',wrd,i)) flip=wrd

C Syntax.
      if (narg.ne.2) then
        print *,'Syntax:  flip (input FITS file) (output FITS file)'
        print *,'                          (flip=)'
        print *,'  '
        print *,'  Flip a 2-D FITS image file either left-right'
        print *,'    (in columns) or top-bottom (in rows).'
        print *,'  '
        print *,'  flip=   :  Specify either:  LR (left-right)'
        print *,'             or TB (top-bottom).'
        print *,'  '
        call exit(0)
      endif

C Read files.
      infile = arg(1)
      call AddFitsExt(infile)
      outfile= arg(2)
      call AddFitsExt(outfile)
      call readfits(infile,a,maxpix,header,ok)
      if (.not.ok) goto 801

      call flip_fits(a,header,flip,ok)
      if (.not.ok) goto 802

C Write file.
      print '(2a)',' Writing : ',outfile(1:lc(outfile))
      call writefits(outfile,a,header,ok)
      if (.not.ok) goto 801

      stop
801   print *,'Error writing or reading FITS image.'
      stop
802   print *,'Error flipping image.'
      stop
      end

