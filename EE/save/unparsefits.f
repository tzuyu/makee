C unparsefits.f
C
      implicit none
      integer*4 nc,nr,sc,sr,er,ec,narg,row,i,lc,k
      character*57600 header,temphead
      character*80 arg(9),fullfile,newfile,chead,object
      integer*4 maxpt
      parameter(maxpt = 201000)
      real*4 a(maxpt),b(maxpt)
      logical ok

C Get command line parameters.
      call arguments(arg,narg,9)
      if (narg.ne.1) then
        print *,
     .  'Syntax: unparsefits (pixel or wavelength calib. FITS file)'
        print *,
     .  '   Assumes that individual FITS files were created by'
        print *,
     .  '   "parsefits".  This program takes the header from original'
        print *,
     .  '   2-D FITS files and puts in the data from the individual'
        print *,
     .  '   1-D files.  WARNING: Overwrites original 2-D FITS file.'
        call exit(0)
      endif
      fullfile = arg(1)
      call AddFitsExt(fullfile)

C Read fits file...
      print '(2a)',' Reading : ',fullfile(1:lc(fullfile))
      call readfits(fullfile,a,maxpt,header,ok)
      if (.not.ok) then
        print *,'Error reading fits file: ',fullfile
        call exit(1)
      endif
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
      if ((nc.eq.0).or.(nr.eq.0)) then
        print *,'ERROR-- Images must be two dimensional.'
        call exit(1)
      endif
      object = chead('OBJECT',header)

      DO row=sr,er

C Read in the individual FITS files.
      i=index(fullfile,'.fits')
      if (i.eq.0) then
        print *,'Extension must be .fits .'
        call exit(1)
      endif
      write(newfile,'(a,a,i3.3,a)') fullfile(1:i-1),'-',row,'.fits'
      call readfits(newfile,b,maxpt,temphead,ok)
      if (.not.ok) then
        print *,'Error writing fits file: ',newfile
        call exit(1)
      endif

C Put in data.
      do i=1,nc
        k = i + (row-sr)*nc
        a(k) = b(i)
      enddo
 
      ENDDO

C Write full file back out.
      print '(2a)',' Over-writing : ',fullfile(1:lc(fullfile))
      call writefits(fullfile,a,header,ok)

      print *,'All done.'
      stop
      end

