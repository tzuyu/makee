C makeepipe                                  tab - Nov98,Dec98,Mar99

C
C Executive program for running makee.  
C 1.Lists files. (done)
C 2.Find E/DN and readout noise.  (in progress)
C 3.Creates bias. (testing)
C 4.Averages flats.  (done)
C 5.Sets up script.  (done)
C 6.Executes script.  (done)
C
      implicit none
C
      include 'soun.inc'         ! Standard output unit number.
      include 'verbose.inc'      ! Verbosity?
      include 'global.inc'
C
      include 'makeepipe.inc'    ! includes big arrays: obj() and flat().
C
      character*200 arg(30),wrd,wrds(6),imfile,scriptname,logname
      character c9*9, c24*24, c17a*17, c17b*17, fdate*24
      real*4 valread,rr
      integer*4 narg,i,n,j,time,access,lc,count
      integer*4 sleeptime,hh,mm,hh0,mm0,nexec,count_redux
      logical findarg,ok,options

C Standard output unit number (default).
      soun=6

C Clear instrument flags.
      Global_HIRES= .false. 
      Global_ESI  = .false. 

C Time flag to mark temporary files.
      start_time = time()
      write(c9,'(i10.10)') start_time
      timeflag=c9(2:9)

C Get arguments from command line.
      call arguments(arg,narg,30)

C RAW HIRES FITS images directory.
      rawdir='./'
      if (findarg(arg,narg,'raw=',':',wrd,i)) rawdir=wrd

C Which steps?
      do i=1,6
        step(i)=.true.
      enddo
      if (findarg(arg,narg,'step=',',',wrds,n)) then
        do i=1,6
          step(i)=.false.
        enddo
        do i=1,n
          j = max(1,min(6,nint(valread(wrds(i)))))
          step(j)=.true.
        enddo
      endif

C Skip which ones?
      if (findarg(arg,narg,'skip=',',',wrds,n)) then
        do i=1,n
          j = max(1,min(6,nint(valread(wrds(i)))))
          step(j)=.false.
        enddo
      endif
C
C New OBSERVER name.
      observer=' '
      if (findarg(arg,narg,'observer=',':',wrd,i)) observer=wrd
C BIAS image.
      userbiasfile=' '
      if (findarg(arg,narg,'bias=',':',wrd,i)) userbiasfile=wrd
C Prefix to RAW HIRES FITS files.
      pre_fix=' '
      if (findarg(arg,narg,'prefix=',':',wrd,i)) pre_fix=wrd
C Image list file.
      imfile=' '
      if (findarg(arg,narg,'imagelist=',':',wrd,i)) imfile=wrd
C Script file name.
      scriptname='makee.script'
      if (findarg(arg,narg,'scriptname=',':',wrd,i)) scriptname=wrd
      logname = scriptname(1:lc(scriptname))//'.log'
C MAKEE parameter file.
      pffile=' '
      if (findarg(arg,narg,'pf=',':',wrd,i)) pffile=wrd
C Flat time tolerance in minutes.
      fttol=60.
      if (findarg(arg,narg,'fttol=',':',wrd,i)) fttol=valread(wrd)
C Slit width tolerance.
      slittol=0.2
      if (findarg(arg,narg,'slittol=',':',wrd,i)) slittol=valread(wrd)
C Echelle angle tolerance.
      echtol=0.0021
      if (findarg(arg,narg,'echtol=',':',wrd,i)) echtol=valread(wrd)
C Cross disperser tolerance.
      xdtol =0.0021
      if (findarg(arg,narg,'xdtol=',':',wrd,i)) xdtol=valread(wrd)
C Electrons per Digital Number.
      user_eperdn=0.
      if (findarg(arg,narg,'eperdn=',':',wrd,i)) then
        user_eperdn=valread(wrd)
      endif
C Readout noise in electrons.
      user_ronoise=0.
      if (findarg(arg,narg,'ronoise=',':',wrd,i)) then
        user_ronoise=valread(wrd)
      endif
C Saturation level in DN.
      saturation=64000.
      if (findarg(arg,narg,'sat=',':',wrd,i)) saturation=valread(wrd)
C Cycles.
      cycle = findarg(arg,narg,'-cycle',':',wrd,i)
C Set stop time.
      rr = 12.
      if (findarg(arg,narg,'stopin=',':',wrd,i)) rr = valread(wrd)
      if (findarg(arg,narg,'stopat=',':',wrds,n)) then
        if (n.gt.0) then
          hh = valread(wrds(1))
          mm = 0. 
          if (n.gt.1) mm = valread(wrds(2))
          hh = hh + (mm/60.)
          c24 = fdate()
          i = index(c24,':')
          hh0 = valread(c24(i-2:i-1))
          mm0 = valread(c24(i+1:i+2))
          hh0 = hh0 + (mm0/60.)
          if (hh.lt.hh0) hh=hh+24.
          rr = hh - hh0
        endif
      endif
      stop_time = start_time + nint(rr*3600.)
C Sleep time.
      sleeptime=180
      if (findarg(arg,narg,'sleep=',':',wrd,i)) then
        sleeptime=nint(valread(wrd))
      endif
C
      lpr=' '
      if (findarg(arg,narg,'lpr=',':',wrd,i)) lpr = wrd
      logit   = findarg(arg,narg,'-log',':',wrd,i)
      lesspr  = findarg(arg,narg,'-lesspr',':',wrd,i)
      noobject= findarg(arg,narg,'-noobject',':',wrd,i)
      rename  = .not.findarg(arg,narg,'-norename',':',wrd,i)
      update  = findarg(arg,narg,'-update',':',wrd,i)
      testmode= findarg(arg,narg,'-test',':',wrd,i)
      nomedian= findarg(arg,narg,'-nomedian',':',wrd,i)
      UseAddArcs = findarg(arg,narg,'-addarc',':',wrd,i)
      options = findarg(arg,narg,'-options',':',wrd,i).or.
     .          findarg(arg,narg,'-help',':',wrd,i)
      verbose = findarg(arg,narg,'-verbose',':',wrd,i).or.
     .          findarg(arg,narg,'-v',':',wrd,i)
C
C Syntax.
      ok=(narg.eq.0).or.(narg.eq.1)
      if (ok) ok=(narg.eq.1).or.(.not.step(1))
      if (options) ok=.false.
C
      if (.not.ok) then
        print *,
     .  'Syntax: makeepipe  ( all | (file with list of files) )'
        print *,'   [ step={1,2,3,4,5,6} ]  [ skip={1,2,3,4,5,6} ]'
        print *,'   [ raw= ]  [ bias= ]  [ -noobject ]  [ -norename ]'
        print *,
     .  '   [ echtol= ]  [ xdtol= ]  [slittol=]  [fttol=]  [-verbose]'
        print *,'   [ eperdn= ]  [ ronoise= ]  [ sat= ]  [ pf= ]'
        print *,
     .  '   [ imagelist=]  [scriptname=]  [observer=]  [prefix=]'
        print *,
     .  '   [ -update ]  [ -cycle sleep= [ stopin= | stopat= ] ]'
        print *,'   [ lpr= ]  [-lesspr]  [ -nomedian ]  [ -log ]'
        print *,'   [ -addarc ]'
        print *,' '
        print *,'   [ -help (see complete description of options) ]'
        print *,'   [ -options (see complete description of options) ]'
        print *,' '
        print *,
     .  '   Reduces HIRES FITS files.  Makes observations listing,'
        print *,
     .  '   creates bias frame, groups and averages flat fields,'
        print *,
     .  '   sets up and  executes script to run "makee" on objects.'
        print *,' '
        print *,
     .  '   The first parameter is required.  If "all" is given then'
        print *,
     .  '   all the ".fits" files in the raw image directory are used.'
        print *,
     .  '   Alternatively you can specify a file containing a list of'
        print *,
     .  '   FITS files in the raw image directory, or if step#1 is'
        print *,
     .  '   excluded then the filenames are read from "image.list".'
        print *,' '
        print *,
     .  '   The two files "exclude.list" and "exclude.redux" can be'
        print *,
     .  '   used to list files to be excluded from the listing and/or'
        print *,'   from the reductions.'
        print *,' '
        if (options) then
        print *,'Options:'
        print *,
     .  '  step=    : Specify one or more steps to be executed:'
        print *,'                (default: execute all steps)'
        print *,'     (1) = Create listing file "image.list".'
        print *,
     .  '     (2) = Find e-/DN and readout noise using darks, flats.'
        print *,'     (3) = Create BIAS.fits image file.'
        print *,'     (4) = Average flat field images.'
        print *,'     (5) = Write "makee.script" script file.'
        print *,'     (6) = Execute "makee.script" script file.'
        print *,' '
        print *,' skip=     : Skip one of more steps.'
        print *,
     .  ' raw=      : Directory name for raw HIRES image files'
        print *,'                (default= current directory.)'
        print *,
     .  ' bias=     : Name of bias file (for makee) (if specified,'
        print *,'                 skip step #3.)'
        print *,
     . 'imagelist= : File with "makeepipe-style" listing of HIRES FITS'
        print *,
     .  '             files to reduce (if used, step #1 is skipped).'
        print *,
     .  ' -noobject : Do not change object name (using .list file)'
        print *,
     .  ' -norename : Do not rename lamp files: [lamp/filter]'
        print *,
     .  ' -nomedian : Do not calculate medians for listing file.'
        print *,' -log      : Send "makee" output to log files.'
        print *,' -addarc   : Use additional arclamps (if available.)'
        print *,
     .  ' -update   : Extract only new objects (i.e. only extract'
        print *,'                 objects without a Flux-*.fits file.)'
        print *,
     .  ' -cycle    : Cycle program in loop (implies "-update").'
        print *,'stopin=h   : Stop cycling in "h" hours (default=12).'
        print *,'stopat=HH:MM : Stop cycling at "HH:MM" (HST).'
        print *,
     .  'sleep=     : Sleep time between cycles in seconds (def=180).'
        print *,'scriptname=: Filename for the makee script.'
        print *,
     .  ' observer= : Change all OBSERVER cards to this string.'
        print *,
     .  '  prefix=  : Prefix to RAW FITS file names, e.g. "hires".'
        print *,' lpr=      : Send makee output to this printer.'
        print *,
     .  ' -lesspr   : Send less output to printer (just Flux*.fits).'
        print *,
     .  ' pf=       : Use this parameter file (default: makee.param)'
        print *,
     .  ' echtol=   : Echelle angle tolerance (default= 0.001).'
        print *,'               (To qualify as the same setup.)'
        print *,
     .  ' xdtol=    : Cross disperser angle tolerance (default=0.0016).'
        print *,' slittol=  : Slit width tolerance (default=0.2).'
        print *,
     .  ' fttol=    : Flat averaging delta-time tolerance in minutes.'
        print *,
     .  '               (acceptable time seperation) (default=60.)'
        print *,
     .  ' eperdn=   : Electrons per digital number (for makee).'
        print *,
     .  ' ronoise=  : Readout noise in electrons (for makee).'
        print *,
     .  '                (if eperdn= or ronoise= used, skip step#2.)'
        print *,' sat=      : Saturation level in digital number.'
        print *,'                (default=  64000.)'
        endif
        stop
      endif

C Check.
      if ((cycle).and.(logit)) then
        print *, 'Cannot use "-cycle" and "-log" together since "-log"'
        print *, 'redirects output which cannot be done within the'
        print *, 'program.  Use "-update" and then source script after'
        print *, 'makeepipe finishes.'
        call exit(1)
      endif

C Check.
      if ((.not.update).and.(cycle)) then
        print *, 'NOTE: "-cycle" implies "-update".'
        update=.true.
      endif

C Check.
      if (step(1).and.(imfile.ne.' ')) then
        print *, 'Skipping step #1 since "imagelist=" option used.' 
        step(1)=.false.
      endif  

C Default.
      if (imfile.eq.' ') imfile='image.list'

C Echo.
      if (cycle) then
        call make_time_string(start_time,c17a,1)
        call make_time_string(stop_time,c17b,1)
        print *, ':::::: Cycling and updating ::::::'
        print *, 
     .  ':::::: Start time=',c17a,'   Stop time=',c17b,' :::::::'
      endif

C Start of cycle.
111   continue

C Initial.
      mp_nfi=0

C Initialize makee.script.
      print *,'Initialize script : ',scriptname(1:lc(scriptname))
      open(1,file=scriptname,status='unknown')
      write(1,'(a)') '# '
      close(1)

C STEP 1: List files. (If cycle>1, go to sleep step if no new extractions.)
      if (step(1)) then
C Does image.list file exist?
        if (access(imfile,'r').eq.0) then
          if (update) then
            call mp_load_block(imfile,ok)  ! Load common block.
          else
            call mp_backup_file(imfile)    ! Move image.list file to old name.
          endif
        endif
        print *, 'Step #1: Listing files...'
        count=0
        count_redux=0
        call mp_write_image_list(arg(1),imfile,count,count_redux,ok)
        if (.not.ok) call exit(1)
        if (update) then
          print *, count,' new raw HIRES FITS files...'
          print *, count_redux,
     .  ' raw HIRES FITS files to be extracted...'
        endif
        if ((cycle).and.(count_redux.eq.0)) then
          nexec=0
          goto 888
        endif
      endif

C Look for "image.list" file.
      if (access(imfile,'r').ne.0) then
        print *, 
     .  'ERROR: Cannot find "image.list" file (try running step 1).'
        call exit(1)
      endif

C Write other (e.g. PostScript) versions of "image.list" file.
      call mp_write_other(imfile,ok)

C Early exit.
      if (.not.(step(2).or.step(3).or.step(4).or.
     .                       step(5).or.step(6))) stop

C Load common block.
      print *, 'Loading common block... (1)'
      call mp_load_block(imfile,ok)
      if (.not.ok) call exit(1)

C Default EPERDN and RONOISE.

C STEP 2: Find E/DN and ronoise.
      if (step(2)) then
        print *,'Find E/DN and readout noise.'
        call mp_find_eperdn_ronoise()
      endif

C STEP 3: Create BIAS image.
      if (step(3)) then
        if (userbiasfile.ne.' ') then
          print *, 'Step #3: Skipping (BIAS file specfied by user.)'
        else
          print *, 'Step #3: Create 1-D BIAS image files.'
          call BiasMaker(ok)
        endif
      endif

C STEP 4: Average flat field images.
      if (step(4)) then
        print *, 'Step #4: Averaging flat field images...'
        call mp_average_flats(imfile,ok)
        if (.not.ok) call exit(1)
C Write other versions.
        call mp_write_other(imfile,ok)
C Load common block again (to include flats).
        if (step(5).or.step(6)) then
          call mp_load_block(imfile,ok)
          if (.not.ok) call exit(1)
        endif
      endif

C STEP 5: Create makee.script.
      if (step(5)) then
        print *, 
     .  'Step #5: Create makee script: ',scriptname(1:lc(scriptname))
        call mp_write_script(scriptname,logname,ok)
        if (.not.ok) call exit(1)
      endif

C STEP 6: Execute makee.script.
      if (step(6)) then
        print *, 'Step #6: Execute the makee script.'
        call mp_execute(scriptname,nexec)
      endif

888   continue

C Sleep before next cycle.  Check time to stop.
      i = time()
      if ((cycle).and.(i.lt.stop_time)) then
C Echo.
        call make_time_string(i,c17a,1)
        call make_time_string(stop_time,c17b,1)
        print *, 
     .  ':::::: Current time=',c17a,'   Stop time=',c17b,' :::::::'
C If we executed any "makee" programs, sleep a while.
        if (nexec.eq.0) then
          print '(a,i8)','sleep',sleeptime
          call sleep(sleeptime)
        endif
        goto 111
      endif

      stop
      end

C----------------------------------------------------------------------
C Execute makee.script by reading script, finding makee commands
C
      subroutine mp_execute(scriptname,nexec)
C
      implicit none
      character*(*) scriptname
      integer*4 nexec
C
      include 'makeepipe.inc'
C
      character*200 w(40),wrd
      integer*4 nw,fc,lc
      logical ok
C
      nexec = 0
C
C Run makee commands.
      open(29,file=scriptname,status='old')
1     read(29,'(a)',end=5) wrd
      wrd = wrd(fc(wrd):lc(wrd))
      call parse_words(wrd,w,nw)
      if (w(1)(1:5).eq.'makee') then
        print '(a)',
     .  '  - - - - - - - - - - - - - - - - - - - - - - - - - - -'
        print '(a)',wrd(1:lc(wrd))
        print '(a)',
     .  '  - - - - - - - - - - - - - - - - - - - - - - - - - - -'
        if (.not.testmode) call makee(w(2),nw-1,ok)
        nexec = nexec + 1
      endif
      goto 1
5     close(29)
C
      return
      end


C----------------------------------------------------------------------
C Parse string into blank separated words.
C
      subroutine parse_words(s,w,nw)
C
      implicit none
      character*(*) s
      character*(*) w(*)
      integer*4 nw,i,k,lc
C
      i=1
      nw=0
      k=len(s)
      do while(i.le.k)
C Find non-blank.
        do while((i.le.k).and.(s(i:i).eq.' '))
          i=i+1
        enddo
C Make word.
        if ((i.le.k).and.(s(i:i).ne.' ')) then
          nw=nw+1
          w(nw)=' '
          do while((i.le.k).and.(s(i:i).ne.' '))
            w(nw) = w(nw)(1:lc(w(nw)))//s(i:i)
            i=i+1
          enddo
        endif
      enddo
      return
      end

C----------------------------------------------------------------------
C Write script to run "makee" on objects and stars.
C
      subroutine mp_write_script(scriptfile,logfile,ok)
C
      implicit none
      character*(*) scriptfile
      character*(*) logfile
      logical ok
C
      include 'makeepipe.inc'
      include 'soun.inc'
C
      character*80 objfile,starfile,flatfile,arcfile1b,arcfile2b,file
      character*80 arcfile1,arcfile2,file1,file2,fluxfile,biasfile
      character*500 wrd,wrd2
      integer*4 i,ii,lc,revindex,access,j,jj,iarc1,iarc2,iarcx,numfi
      logical ExcludeFromRedux

C Open script file.
      open(28,file=scriptfile,status='unknown')
      write(28,'(a)') '# '
      write(28,'(a)') '#  makee.script:'
      open(29,file=logfile,status='unknown')
      write(29,'(a)') '..........................................'

C Total number of files.
      numfi = mp_nfi

C Look through all files for objects and stars.
      DO ii=1,mp_nfi 
      IF ((mp_type(ii).eq.'OBJT').or.(mp_type(ii).eq.'STAR')) THEN

C ....................Object file.
      objfile = mp_fi(ii)
      call AddFitsExt(objfile)

C Flux filename.
      j = revindex(objfile,'.')
      if (j.eq.0) j=lc(objfile)+1
      write(fluxfile,'(a,i3.3,a)') 'Flux-',mp_obs(ii),'.fits'

C Not excluded?
      IF (.not.ExcludeFromRedux(objfile)) THEN

C Update?
      IF ((.not.update).or.(access(fluxfile,'r').ne.0)) THEN

C Echo.
      write(soun,'(4a)') 'Write makee extraction line for: ',
     .    objfile(1:lc(objfile)),' : 
     .  ',mp_object(ii)(1:lc(mp_object(ii)))
      write(29  ,'(4a)') 'Write makee extraction line for: ',
     .    objfile(1:lc(objfile)),' : 
     .  ',mp_object(ii)(1:lc(mp_object(ii)))

C Look for bias file.
      if (userbiasfile.eq.' ') then
        write(biasfile,'(a,i1.1,a,i1.1,a)') 'bias1d_',mp_xbin(ii),
     .                             'x',mp_ybin(ii),'.fits'
        if (access(biasfile,'r').ne.0) biasfile=' '
      else
        biasfile = userbiasfile
      endif

C ....................Star file.
C Same file.
      if (mp_type(ii).eq.'STAR') then
        starfile = mp_fi(ii)
      else
C Find best star to go with this one.
        call mp_find_match_file(ii,1,numfi,'STAR',file1,jj)
C Find best pinhole to go with this one.
        call mp_find_match_file(ii,1,numfi,'PINH',file2,jj)
C Found either?
        if ((file1.eq.' ').and.(file2.eq.' ')) then
         write(soun,'(a)')
     .  'WARNING: Cannot find star or pinhole for trace, using object.'
         write(29  ,'(a)')
     .  'WARNING: Cannot find star or pinhole for trace, using object.'
          starfile = objfile
C Use the star if no pinhole.
        elseif (file2.eq.' ') then
          starfile = file1
C Use the pinhole if no star.
        elseif (file1.eq.' ') then
          starfile = file2
C Use the star if we have both (could make a better choice in some cases)
C           (for example, if star is too faint, perhaps pinhole is better)
        else
          starfile = file1
C
        endif
      endif

C ....................Flat file.
C Find best averaged flat field images to go with object (FLAV).
      call mp_find_match_file(ii,1,numfi,'FLAV',file1,jj)
C Find best non-averaged flat field (FLAT).
      call mp_find_match_file(ii,1,numfi,'FLAT',file2,jj)
C Found either?
      if ((file1.eq.' ').and.(file2.eq.' ')) then
        write(soun,'(a)')
     .  'ERROR: Cannot find acceptable flat field to go with object.'
        write(29  ,'(a)')
     .  'ERROR: Cannot find acceptable flat field to go with object.'
        flatfile = ' '
C Use the FLAT if no FLAV.
      elseif (file1.eq.' ') then
        flatfile = file2
C Use FLAV.
      else
        flatfile = file1
      endif

C ....................Arc file.

C Find best arc lamp for this object before object.
      call mp_find_match_file(ii,1,ii-1,'ARCL',arcfile1,iarc1)
C Find next best arc lamp before best arclamp.
      call mp_find_match_file(ii,1,iarc1-1,'ARCL',file,iarcx)
C If close to first arc, combine lines.
      if ( (abs(mp_time(iarc1)-mp_time(iarcx)).lt.600).and.
     .     (abs(mp_obs(iarc1) -mp_obs(iarcx)) .eq.1  ) ) then
        arcfile1b = file
      else
        arcfile1b = ' '
      endif

C Find best arc lamp for this object after object.
      call mp_find_match_file(ii,ii+1,numfi,'ARCL',arcfile2,iarc2)
C Find next best arc lamp after best arclamp.
      call mp_find_match_file(ii,iarc2+1,numfi,'ARCL',file,iarcx)
C If close to first arc, combine lines.
      if ( (abs(mp_time(iarc2)-mp_time(iarcx)).lt.600).and.
     .     (abs(mp_obs(iarc2) -mp_obs(iarcx)) .eq.1  ) ) then
        arcfile2b = file
      else
        arcfile2b = ' '
      endif

C Two arc sets.
      if ((arcfile1.ne.' ').and.(arcfile2.ne.' ')) then
C Bracket? (within 2:15 and both adjacent to object)
        if ( (abs(mp_time(iarc1)-mp_time(iarc2)).lt.8100).and.
     .       (abs(mp_obs(iarc1) -mp_obs(iarc2)) .eq.2   ) ) then
          write(soun,'(a,2i5)') 
     .  'Note: Bracketting arc lamps:',mp_obs(iarc1),mp_obs(iarc2)
          write(29  ,'(a,2i5)') 
     .  'Note: Bracketting arc lamps:',mp_obs(iarc1),mp_obs(iarc2)
        else
C Use only the best arcs (before or after?)
          if ( abs(mp_time(ii)-mp_time(iarc1)).lt.
     .         abs(mp_time(ii)-mp_time(iarc2)) ) then
            arcfile2 =' '
            arcfile2b=' '
          else
            arcfile1 =' '
            arcfile1b=' '
          endif
        endif
      elseif (arcfile1.ne.' ') then
C Before object arc only.
        arcfile2 =' '
        arcfile2b=' '
      elseif (arcfile2.ne.' ') then
C After object arc only.
        arcfile1 =' '
        arcfile1b=' '
      else
C No arcs.
        write(soun,'(a)') 'WARNING: No arc lamp found for this object.'
        write(29  ,'(a)') 'WARNING: No arc lamp found for this object.'
        arcfile1 =' '
        arcfile1b=' '
        arcfile2 =' '
        arcfile2b=' '
      endif


C ....................Write line if possible.
      if (flatfile.eq.' ') then
        write(soun,'(a)') 'ERROR: No flat field, cannot do extraction.'
        write(29  ,'(a)') 'ERROR: No flat field, cannot do extraction.'
      else
        write(28,'(a)') '# '
C Comment line.
        write(28,'(a,i4,5a,i5,a,2f8.4,3a,i2,a,i2)') 
     .     '#  ',mp_obs(ii),' : ',
     .     mp_object(ii)(1:lc(mp_object(ii))),' : ',
     .     mp_coord(ii)(1:lc(mp_coord(ii))),' :',
     .     nint(mp_expos(ii)),'s :',
     .     mp_echa(ii),mp_xda(ii),' : ',mp_deck(ii),' :',
     .     mp_xbin(ii),' x',mp_ybin(ii)
        write(29,'(a,i4,5a,i5,a,2f8.4,3a,i2,a,i2)') 
     .     'Obs',mp_obs(ii),' : ',
     .     mp_object(ii)(1:lc(mp_object(ii))),' : ',
     .     mp_coord(ii)(1:lc(mp_coord(ii))),' :',
     .     nint(mp_expos(ii)),'s :',
     .     mp_echa(ii),mp_xda(ii),' : ',mp_deck(ii),' :',
     .     mp_xbin(ii),' x',mp_ybin(ii)
        write(29,'(a)') '..........................................'
C Required three files.
        write(wrd,'(4(a,2x))') 'makee',objfile(1:lc(objfile)),
     .           starfile(1:lc(starfile)),flatfile(1:lc(flatfile))
C First and second arc files.
        if (arcfile1.ne.' ') 
     .  wrd=wrd(1:lc(wrd))//'  '//arcfile1(1:lc(arcfile1))
        if (arcfile2.ne.' ') 
     .  wrd=wrd(1:lc(wrd))//'  '//arcfile2(1:lc(arcfile2))
C Companion arcs.
        if (UseAddArcs) then
C Have two bracketting arcs...
          if ((arcfile1.ne.' ').and.(arcfile2.ne.' ')) then
            if (arcfile1b.ne.' ') then
              wrd = 
     .  wrd(1:lc(wrd))//'  arc1add='//arcfile1b(1:lc(arcfile1b))
            endif
            if (arcfile2b.ne.' ') then
              wrd = 
     .  wrd(1:lc(wrd))//'  arc2add='//arcfile2b(1:lc(arcfile2b))
            endif
C Only have one arc, so any additional arc must go with "arc1add=".
          else
            if ((arcfile1.ne.' ').and.(arcfile1b.ne.' ')) then
              wrd = 
     .  wrd(1:lc(wrd))//'  arc1add='//arcfile1b(1:lc(arcfile1b))
            endif
C Note that additional arclamp goes with only arc listed (called arcfile2).
            if ((arcfile2.ne.' ').and.(arcfile2b.ne.' ')) then
              wrd = 
     .  wrd(1:lc(wrd))//'  arc1add='//arcfile2b(1:lc(arcfile2b))
            endif
          endif
        endif

C Bias file?
        if (biasfile.ne.' ') then
          wrd = wrd(1:lc(wrd))//'  bias='//biasfile(1:lc(biasfile))
        endif

C Raw directory?
        if ((rawdir.ne.' ').and.(rawdir(1:lc(rawdir)).ne.'./')) then
          wrd = wrd(1:lc(wrd))//'  raw='//rawdir(1:lc(rawdir))
        endif
C New object name?
        if (.not.noobject) then
          wrd2 = mp_object(ii)
          if (wrd2(1:1).ne.char(92)) then    ! backslash
            do i=1,lc(wrd2)
              if (wrd2(i:i).eq.' ') wrd2(i:i)='~'
            enddo
            wrd = wrd(1:lc(wrd))//'  object='//wrd2(1:lc(wrd2))
          endif
        endif
C New observer name?
        if (observer.ne.' ') then
          wrd = wrd(1:lc(wrd))//'  observer='//observer(1:lc(observer))
        endif
C Print it out?
        if (lpr.ne.' ') wrd = wrd(1:lc(wrd))//'  lpr='//lpr(1:lc(lpr))
C Less printout?
        if (lesspr) wrd = wrd(1:lc(wrd))//'  -lesspr'
C Log file?
        i = index(objfile,'.fits') - 1
        if (i.lt.0) i=lc(objfile)
        if (logit) wrd = wrd(1:lc(wrd))//' log='//objfile(1:i)//'.log'
C Write execution line to script file.
        write(28,'(a)') wrd(1:lc(wrd))
      endif

      ENDIF
      ENDIF
      ENDIF
      ENDDO

C Close script file.
      close(28)
      ok=.true.

      return
      end


C----------------------------------------------------------------------
C Make flat field average images (step#4).
C Group flats of same setup taken within "fttol" minutes of first flat found.
C Look in "image.list" file to see if flat already done (update only).
C Delete all old flats from image.list (non-update only).
C Append new flat to image.list.
C Name FITS files:  flat####.fits, e.g. flat0045.fits if it starts with obs#45.
C Creates new FITS files in current directory.
C
      subroutine mp_average_flats(imfile,ok)
C
      implicit none
      character*(*) imfile    ! "image.list" to append to (input).
      logical ok              ! Successful? (output)
C
      include 'makeepipe.inc'
      include 'soun.inc'
      include 'verbose.inc'
C
      character*4 c4
      integer*4 lc,fc,ii,nfi,jj,lco,j
      character*57600 header
      character*80 flatfile,object
      logical doave

C If not updating, delete all FLAV from image.list.
      if (.not.update) call mp_delete_from_list(imfile,' FLAV ')

C Re-load image.list file.
      print *, 'Loading common block... (2)'
      call mp_load_block(imfile,ok)
      if (.not.ok) call exit(1)

C Check.
      if (mp_nfi.lt.1) then
        print *, 
     .  'ERROR: mp_average_flats: No files in image list file.'
        ok=.false.
        return
      endif

C Reset flags.
      do ii=1,mp_nfi
        mp_flag(ii)=0 
      enddo

C Using common block, group flats.
      DO ii=1,mp_nfi
C Not already used?  Is this a flat?
      IF ( (mp_flag(ii).eq.0).and.(mp_type(ii).eq.'FLAT') ) THEN

C Flag it and set filename.
      mp_flag(ii)=1
      nfi = 1
      fi(nfi) = mp_fi(ii)
C "Would-be" flat filename.
      write(flatfile,'(a,i4.4,a)') 'flat_',mp_obs(ii),'.fits'
C "Would-be" flat object name.
      write(c4,'(i4)') mp_obs(ii)
      object = '<'//c4(fc(c4):lc(c4))

C Look for other flats of this setup.
      do jj=1,mp_nfi

C FLAT which is not already used?
        if ((mp_flag(jj).eq.0).and.(mp_type(jj).eq.'FLAT')) then
C Within the delta-time tolerance?
        if ( abs(float(mp_time(ii)-mp_time(jj))/60.) .lt. fttol ) then
C Within echelle tolerance?
        if ( abs(mp_echa(ii)-mp_echa(jj)) .lt. echtol ) then
C Within cross disperser tolerance?
        if ( abs(mp_xda(ii)-mp_xda(jj)) .lt. xdtol ) then

C Within slit width tolerance? (If decker starts with "A") ?????????
        if ( (mp_deck(ii)(1:1).ne.'A') .or. (mp_slit(ii).gt.5.) .or.
     .       (abs(mp_slit(ii)-mp_slit(jj)).lt.slittol) ) then
C Same decker?
        if (mp_deck(ii).eq.mp_deck(jj)) then
C Same binning?
        if ((mp_xbin(ii).eq.mp_xbin(jj)).and.
     .               (mp_ybin(ii).eq.mp_ybin(jj))) then
C Filter#1 the same?
        if (mp_fil1(ii).eq.mp_fil1(jj)) then
       
C Nearby flat with same setup!  Add file to list.
          nfi=nfi+1
          fi(nfi) = mp_fi(jj)
          mp_flag(jj) = 1
          write(c4,'(i4)') mp_obs(jj)
          object = object(1:lc(object))//'+'//c4(fc(c4):lc(c4))

        endif
        endif
        endif
        endif

        endif
        endif
        endif
        endif

      enddo

C Object name.
      object = object(1:lc(object))//'>'

C Do we have more than one file?
      if (nfi.gt.1) then

C Echo.
        if (verbose) then
          print *,'Average these flats (in ',rawdir(1:lc(rawdir)),') :'
          do j=1,nfi
            print '(a)',fi(j)(1:lc(fi(j)))
          enddo
        endif

C Now that we have a group, do we already have this average? (if updating)
        if (update) then
          doave=.true.
C Last usuable character.
          lco = min(lc(object),16)
          do jj=1,mp_nfi
            if (mp_type(jj).eq.'FLAV') then
              if (index(mp_object(jj),object(1:lco)).gt.0) then
                doave=.false.
              endif
            endif
          enddo
          if (.not.doave) then
            if (verbose) print *, '....already have this average.'
            goto 800
          endif
        endif

C Do average!
        call mp_average_images(nfi,fi,rawdir,obj,flat,header,ok)
        if (.not.ok) return
        call cheadset('OBJECT',object,header)

C Write out average flat file.
        print *, 'Writing: ',flatfile(1:lc(flatfile)),
     .                 ' ................... ',object(1:lc(object))
        call writefits(flatfile,obj,header,ok)
        if (.not.ok) return

C Delete this filename from image.list if it exists.
        call mp_delete_from_list(imfile,flatfile(1:lc(flatfile)))

C Now append it to image.list.
        call open_for_append(imfile,1)
        call mp_write_line(1,flatfile,header,-1.,-1.,-1.,'FLAV')
        close(1)

      endif

800   continue

      ENDIF
      ENDDO    !  ii=1,mp_nfi

      return
      end


C----------------------------------------------------------------------
C Load values from "image.list" file to common block.
C
      subroutine mp_load_block(imfile,ok)
C
      implicit none
      character*(*) imfile
      logical ok
C
      include 'makeepipe.inc'
C
      character c17*17, line*200
      integer*4 k,lc
      real*4 valread
C
C Open image.list file.
      open(11,file=imfile,status='old')
C
C Read header line.
      read(11,'(a)',end=9) line
      k=0
1     continue
      read(11,'(a)',end=5) line
      k=k+1
      if (k.gt.mf) then
        print *, 
     .  'ERROR: Too many files (try shorter file list).  Max=',mf
        ok=.false.
        return
      endif
      mp_fi(k)     = line(145:lc(line))
      mp_object(k) = line(1:16)
      mp_coord(k)  = line(19:27)
      mp_obs(k)    = nint(valread(line(48:51)))
      mp_type(k)   = line(58:61)
      mp_lfil(k)   = line(69:72)
      mp_fil1(k)   = line(75:78)
      mp_deck(k)   = line(97:99)//' '
      mp_expos(k)  = valread(line(52:56))
      mp_echa(k)   = valread(line(80:87))
      mp_xda(k)    = valread(line(88:95))
      mp_slit(k)   = valread(line(100:105))
      mp_mdn(k)    = valread(line(106:111))
      mp_high(k)   = valread(line(112:117))
      mp_low(k)    = valread(line(118:123))
      mp_xbin(k)   = nint(valread(line(137:139)))
      mp_ybin(k)   = nint(valread(line(140:142)))

C Debugging check (only used when re-writing code).
Ctemp
      if (k.lt.0) then
      print *, 'k=',k
      print *, 'fi=',mp_fi(k)(1:lc(mp_fi(k)))
      print *, 'type=',mp_type(k)
      print *, 'object=',mp_object(k)(1:lc(mp_object(k)))
      print *, 'coord=',mp_coord(k)
      print *, 'lfil=',mp_lfil(k)
      print *, 'fil1=',mp_fil1(k)
      print *, 'deck=',mp_deck(k)
      print *, 'expos=',mp_expos(k)
      print *, 'echa=',mp_echa(k)
      print *, 'xda=',mp_xda(k)
      print *, 'slit=',mp_slit(k)
      print *, 'mdn=',mp_mdn(k)
      print *, 'low=',mp_low(k)
      print *, 'high=',mp_high(k)
      print *, 'xbin=',mp_xbin(k)
      print *, 'ybin=',mp_ybin(k)
      endif
Ctemp

C Compute seconds since 1/1/1970.
      c17 = line(30:46)
      call convert_time_string(c17,mp_time(k),1)
      mp_time(k) = mp_time(k) + nint(mp_expos(k)/2.)

      goto 1        ! next line.
C
5     continue
      if (k.lt.1) goto 9
      close(11)
      mp_nfi=k
      ok=.true.
      return
C
9     close(11)
      print *, 'ERROR: Reading: ',imfile(1:lc(imfile))
      mp_nfi=0
      ok=.false.
      return
      end


C----------------------------------------------------------------------
C Writes new listing file.  OR appends new files to old list.
C  
      subroutine mp_write_image_list(ufile,imfile,count,count_redux,ok)
C
      implicit none
      character*(*) ufile       ! File with list of FITS files  (input).
      character*(*) imfile      ! "image.list" filename (input).
      integer*4 count           ! Number of new files (output.)
      integer*4 count_redux     ! Number of new extractions to be done (output.)
      logical ok                ! Success? (output).
C
      include 'makeepipe.inc'
      include 'soun.inc'
      include 'verbose.inc'
C
      real*4 medmax,highmax,lowmax
      integer*4 ii,i,j,sc,ec,sr,er,nc,nr,lc,xbin,ybin,ifi,nfi
      character*57600 header
      character*4 imtype
      character*80 chead,filelist,fluxfile,fdir,status,instrume,root
      character*200 wrd
      integer*4 filetime,now,time,tt,revindex,access
      logical ExcludeFromRedux,go,ExcludeFromList

C Current time.
      now = time()

C File directory.
      fdir = rawdir

C Open output file.
      print *, 'Writing : ',imfile(1:lc(imfile))
      if (access(imfile,'r').eq.0) then
        call open_for_append(imfile,21)
      else
        open(21,file=imfile,status='unknown')
C Add header line if starting new list file.
        write(21,'(6a)')
     .  '  object name    coord(1950)   date     ti',
     .  'me    obs intg type lamp  ',
     .  'lfil  fil1  ',
     .  ' echang  xdang  deck slit   mdn  high   low ',
     .  'shut htch cd xb yb',
     .  '  file name'
      endif

C Set filename of file containing list of FITS files.
      if (ufile.eq.'all') then
        call system('/usr/bin/ls '//fdir(1:lc(fdir))//'/'//
     .        pre_fix(1:lc(pre_fix))//'*.fits >! list-'//timeflag)
        filelist = 'list-'//timeflag
      else 
        filelist = ufile
      endif

C Load the filename arrays.
      open(1,file=filelist,status='old')
      nfi=0
1     read(1,'(a)',end=5) wrd
C Pre-pend raw directory if necessary.
      if (index(wrd,fdir(1:lc(fdir))).eq.0) then
        wrd = fdir(1:lc(fdir))//'/'//wrd(1:lc(wrd))
      endif
C Another file.
      nfi=nfi+1
      call AddFitsExt(wrd)
      fi(nfi) = wrd
      tt= filetime(wrd)
C Is a file written too recently (within 30 seconds)?  If so, delete from list.
      if (now-tt.lt.30) then
        nfi=nfi-1
      else
        if (ExcludeFromList(fi(nfi))) nfi=nfi-1
      endif
      goto 1
5     continue
      if (ufile.eq.'all') then 
        close(1,status='delete')
      else
        close(1)
      endif

C Check.
      if (nfi.eq.0) then
        print *, 'mp_write_image_list: ERROR: Found no FITS files.'
        goto 9
      endif

C Go through file list.
      count=0
      DO ifi=1,nfi

C Is this file already in the list?
      i = revindex(fi(ifi),'/') + 1
      j = revindex(fi(ifi),'.fits') - 1
      if (j.le.i) j=lc(fi(ifi)) 
      root = fi(ifi)(i:j)
      go=.true.
      do ii=1,mp_nfi
        if (index(mp_fi(ii),root(1:lc(root))).gt.0) go=.false.
      enddo

C Continue?
      IF (go) THEN

C Read FITS header.
      if (verbose) print *, 'Reading : ',fi(ifi)(1:lc(fi(ifi)))
      call readfits_header(fi(ifi),header,ok)
C Image dimensions.
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
C Status card.
      status = chead('STATUS',header)
C Instrument card.
      instrume = chead('INSTRUME',header)

C Is this an acceptable RAW HIRES FITS image?  If not, skip.
      if ((nr.lt.50).or.(status(1:5).eq.'PROCE').or.
     .                       (index(instrume,'HIRES').eq.0)) then
        print *, 'WARNING: This appears to not be a raw FITS image: ',
     .           fi(ifi)(1:lc(fi(ifi))),', skipping...'
        print *, '       : STATUS=',status(1:lc(status)),
     .                 '  INSTRUME=',instrume(1:lc(instrume)),
     .                 '  NROWS=',nr
        go=.false.
      endif

C Continue?
      IF (go) THEN
    
C Find "median" values?
      if (.not.nomedian) then
C Must read data. 
        call readfits(fi(ifi),obj,maxpix,header,ok)
C Find binning factors.
        call GetBinning(header,xbin,ybin)
C Find "representative" maximum along columns.
        call ColumnMaximum(sc,ec,sr,er,obj,medmax,highmax,lowmax,xbin)
      else
        medmax =-1.
        highmax=-1.
        lowmax =-1.
      endif
C Decide what kind of image this is.
      call ImageType(header,medmax,highmax,lowmax,imtype,saturation)
C Write a line to the "image.list" file.
      call mp_write_line(21,fi(ifi),header,medmax,
     .  highmax,lowmax,imtype)
C Count.
      count=count+1
C Report.
      print *, 'New raw FITS file: ',fi(ifi)(1:lc(fi(ifi)))

      ENDIF
      ENDIF
      ENDDO

C Close "image.list" file.
      close(21)

C Load common block.
      print *, 'Loading common block... (3)'
      call mp_load_block(imfile,ok)
      if (.not.ok) call exit(1)

C How many non-excluded OBJT or STARs which do not have Flux-*.fits files?
      count_redux=0
      do ii=1,mp_nfi
        if (.not.ExcludeFromRedux(mp_fi(ii))) then
          if ((mp_type(ii).eq.'OBJT').or.(mp_type(ii).eq.'STAR')) then
            write(fluxfile,'(a,i3.3,a)') 'Flux-',mp_obs(ii),'.fits'
            if (access(fluxfile,'r').ne.0) then
              count_redux = count_redux + 1
              print *, 'New extraction on: ',mp_fi(ii)(1:lc(mp_fi(ii)))
            endif
          endif
        endif
      enddo

C Ok.
      ok=.true.
      return

9     continue
      ok=.false.
      return
      end


C----------------------------------------------------------------------
C Return .true. if obs#i is the same setup as obs#j and also returns the
C time difference between the two observations in seconds.
C
      logical function mp_match(i,j,dt,mode)
C
      implicit none
      integer*4 i     ! first observation array element number (input).
      integer*4 j     ! second observation array element number (input).
      integer*4 dt    ! time difference in seconds (output).
      integer*4 mode  ! 0 = normal,  1= decker D5 matches with all (input).
C
      include 'makeepipe.inc'
C

C Time difference in seconds.
      dt = abs( mp_time(i) - mp_time(j) )

Ctemp
C QUESTION: Which combination of slit widths and deckers are compatible?
Ctemp

C Same setup?  Check several things.

C Same decker?
      if ((mode.eq.0).or.
     . ((mp_deck(i)(1:2).ne.'D5').and.(mp_deck(j)(1:2).ne.'D5'))) then 
        if  (mp_deck(i).ne.mp_deck(j)) goto 801
      endif

C Same echelle angle?
      if (abs(mp_echa(i)-mp_echa(j)).gt.echtol) goto 801
       
C Same cross disperser angle?
      if (abs(mp_xda(i)-mp_xda(j)).gt.xdtol) goto 801
       
C X-Binning the same?
      if (mp_xbin(i).ne.mp_xbin(j)) goto 801
       
C Y-Binning the same?
      if (mp_ybin(i).ne.mp_ybin(j)) goto 801

C Filter#1 the same?
      if (mp_fil1(i).ne.mp_fil1(j)) goto 801
       
C Slit width the same?

Ctemp
CCCCCCCC WHAT TO USE?
Ctemp

      if (mp_deck(i)(1:1).eq.'A') then
        if (mp_slit(i).lt.5.) then
          if (abs(mp_slit(i)-mp_slit(j)).gt.slittol) goto 801
        endif
      endif
       
C A match. :)
      mp_match=.true.
      return

C Not a match. :(
801   continue
      mp_match=.false.
      return
      end
       

C----------------------------------------------------------------------
C Find best match with file type "TYPE" to observation "i".
C
      subroutine mp_find_match_file(i,j1,j2,type,file,loj)
C
      implicit none
      integer*4 i           ! object array element number (input)
      integer*4 j1,j2       ! only look at objects within this range (input)
      character*(*) type    ! type to find (e.g. "FLAT", "ARCL", etc.) (input)
      character*(*) file    ! best file (output)
      integer*4 loj         ! index number for best file (output)
C
      include 'makeepipe.inc'
C
      integer*4 lodt,dt,j,mode
      logical mp_match
C
C Set mode.
      if (type.eq.'PINH') then
        mode=1
      else
        mode=0
      endif
C
      file=' '
      loj =0 
      lodt=9000000
      do j=j1,j2
        if (mp_type(j).eq.type) then
          if (mp_match(i,j,dt,mode)) then
            if (dt.lt.lodt) then
              lodt= dt
              file= mp_fi(j)  
              loj = j
            endif
          endif
        endif
      enddo
C
      return
      end


C----------------------------------------------------------------------
C Write a line to the image.list file.
C
      subroutine mp_write_line(oun,file,header,medmax,
     .  highmax,lowmax,imtype)
C
      implicit none
      integer*4 oun
      character*(*) file
      character*(*) header
      real*4 medmax,highmax,lowmax
      character*(*) imtype
C
      include 'makeepipe.inc'
      include 'soun.inc'
      include 'verbose.inc'
C
      real*4 echa,xda,valread,slit
      real*8 fhead,ra,dec
      integer*4 j,k,lc,xbin,ybin,fc
      integer*4 frameno,inhead,exposure,revindex
      integer*4 hh,nn,ss,year4,yy,mm,dd,equinox
      character*80 chead,lampname,deckname,fil1name,lfilname
      character*80 object,shutter,hatch,wrd,ts,coord
      character c9*9, c7a*7, c7b*7, cd*2
      logical lhead

C Find binning factors.
      call GetBinning(header,xbin,ybin)

C Various cards.
      deckname = chead('DECKNAME',header)
      call upper_case(deckname)
      lampname = chead('LAMPNAME',header)
      call lower_case(lampname)
      lfilname = chead('LFILNAME',header)
      call lower_case(lfilname)
      fil1name = chead('FIL1NAME',header)
      call lower_case(fil1name)
      wrd= chead('XDISPERS',header)
      call lower_case(wrd)
      cd = wrd(fc(wrd):fc(wrd)+1)
      object   = chead('OBJECT',header)
      frameno  =inhead('FRAMENO',header)
      exposure =inhead('EXPOSURE',header)
      echa= max(-99.,min(999.,sngl(fhead('ECHANGL',header))))
      xda = max(-99.,min(999.,sngl(fhead('XDANGL',header))))
      slit= max(-99.,min(999.,sngl(fhead('SLITWID',header))))
C Hatch.
      if (lhead('HATOPEN',header)) then
        hatch='open'
      else
        hatch='closed'
      endif
C Shutter.
      if (lhead('DARKOPEN',header)) then
        shutter='open'
      else
        shutter='closed'
      endif
C Coordinate name.
      equinox = inhead('EQUINOX',header)
      ra = fhead('RA',header)
      dec= fhead('DEC',header)
      if (equinox.eq.1950) then
        call write_radec(ra,dec,c9,2)
      elseif (equinox.eq.2000) then
        call precess(2000.d0,1950.d0,ra,dec)
        call write_radec(ra,dec,c9,2)
      else
        if (verbose) print *, 
     .  'Unrecognized equinox (assume 2000): ',equinox
        call precess(2000.d0,1950.d0,ra,dec)
        call write_radec(ra,dec,c9,2)
      endif
      coord = c9
C Rename lamps.
      if ((imtype.ne.'FLAV').and.(rename).and.(lampname(1:4).ne.'none')
     .                           .and.(lampname(1:4).ne.'unde')) then
        if (deckname(1:2).eq.'D5') then
          object = '['//lampname(1:lc(lampname))//'/'//
     .                    lfilname(1:lc(lfilname))//':'//
     .                    deckname(1:lc(deckname))//']'
        else
          object = '['//lampname(1:lc(lampname))//'/'//
     .                    lfilname(1:lc(lfilname))//']'
        endif
      endif
C Date and time string.
      call ReadFitsDate('DATE-OBS',header,year4,yy,mm,dd)
      wrd = chead('UT',header)
      j = index(wrd,':')
      hh = nint(valread(wrd(1:j-1)))
      wrd(j:j)=' '
      k = index(wrd,':')
      nn = nint(valread(wrd(j+1:k-1)))
      ss = nint(valread(wrd(k+1:k+5)))
      write(ts,'(i2.2,a,i2.2,a,i2.2,1x,i2.2,a,i2.2,a,i2.2)')
     .    yy,'/',mm,'/',dd,hh,':',nn,':',ss

C Abreviated filename.
      k = max(0,revindex(file,'/'))
      wrd = file(k+1:lc(file))

C Echelle and XD angles.
      write(c7a,'(f7.4)') echa
      write(c7b,'(f7.4)') xda
      if (c7a(1:1).eq.' ') c7a(1:1)='+'
      if (c7b(1:1).eq.' ') c7b(1:1)='+'

C Write line.
      write(oun,'(a,2x,a,2x,a,i5,i5,7(1x,a),f6.2,3i6,
     .             3(1x,a),2i3,a,a)')
     .     object(1:16),coord(1:9),
     .     ts(1:17),frameno,exposure,imtype,lampname(1:5),
     .     lfilname(1:5),fil1name(1:5),
     .     c7a,c7b,
     .     deckname(1:3),slit,
     .     nint(medmax),nint(highmax),nint(lowmax),
     .     shutter(1:4),hatch(1:4),cd,xbin,ybin,
     .     '  ',wrd(1:lc(wrd))

      return
      end


C---------------------------------------------------------------------- 
C Write other versions of image.list file.
C
      subroutine mp_write_other(imfile,ok)
C
      implicit none
      character*(*) imfile
      logical ok
C
      character*200 line,wrd,imfile80,simfile
      integer*4 lc

C Filenames.
      imfile80= imfile(1:lc(imfile))//'.80'
      simfile = imfile(1:lc(imfile))//'.sort'

C Create "image80.list" file.
      open(11,file=imfile,status='old')
      open(12,file=imfile80,status='unknown')
101   read(11,'(a)',end=501) line
      write(12,'(2a)') line(1:28),line(48:99)
      goto 101
501   close(11)
      close(12)
      print *, 'Writing : ',imfile80(1:lc(imfile80))

C Write PostScript file.
      wrd = imfile(1:lc(imfile))//'.ps'
      call ASCII_to_PS(imfile,wrd,8,10,.false.,.true.,.true.)
      print *, 'Writing : ',wrd(1:lc(wrd))

C Now make sorted version.
      call mp_image_list_sort(imfile,simfile)

C Write PostScript file.
      wrd = simfile(1:lc(simfile))//'.ps'
      call ASCII_to_PS(simfile,wrd,8,10,.false.,.true.,.true.)
      print *, 'Writing : ',wrd(1:lc(wrd))

      return
      end


C----------------------------------------------------------------------
C Delete all lines in the image.list file with string "s".
C
      subroutine mp_delete_from_list(imfile,s)
C
      implicit none
      character*(*) imfile,s
C
      include 'makeepipe.inc'
C
      character*200 line,wrd
      integer*4 lc

C Copy to temp file.
      open(1,file=imfile,status='old')   
      wrd = imfile(1:lc(imfile))//timeflag
      open(2,file=wrd,status='unknown')   
101   read(1,'(a)',end=501) line
      write(2,'(a)') line(1:lc(line))
      goto 101
501   close(1)
      close(2)
C Copy back, deleting string lines.
      wrd = imfile(1:lc(imfile))//timeflag
      open(1,file=wrd,status='old')   
      open(2,file=imfile,status='unknown')   
102   read(1,'(a)',end=502) line
      if (index(line,s).eq.0) write(2,'(a)') line(1:lc(line))
      goto 102
502   close(1,status='delete')
      close(2)

      return
      end


C----------------------------------------------------------------------
C Move file to a backup copy.
C
      subroutine mp_backup_file(file)
C
      implicit none
      character*(*) file
      character*80 oldfile,wrd
      integer*4 k,lc,access
C
      k=0
1     k=k+1
      write(oldfile,'(2a,i4.4)') file(1:lc(file)),'-',k
      if (k.lt.999) then
        if (access(oldfile,'r').eq.0) goto 1
      endif
      print '(4a)','/usr/bin/mv ',file(1:lc(file)),' ',
     .  oldfile(1:lc(oldfile))
      wrd = '/usr/bin/mv '//file//' '//oldfile
      call system(wrd)
      return
      end


C----------------------------------------------------------------------
C Average images together.
C
      subroutine mp_average_images(nfi,fi,rawdir,a,b,header,ok)
C
      implicit none
      integer*4 nfi           ! Number of files (input)
      character*(*) fi(nfi)   ! Filenames (input)
      character*(*) rawdir    ! Raw directory to pre-pend (input)
      real*4 a(*)             ! Final average image (output)
      real*4 b(*)             ! Scratch (scratch)
      character*(*) header    ! Final average images FITS header (output)
      logical ok              ! Success? (output)
C
      include 'maxpix.inc'
C
      integer*4 sc,ec,sr,er,nc,nr,ifi,j,lc
      character*200 file
C
C Add them in reverse order, to end up with first header. 
      do ifi=nfi,1,-1
        file = rawdir(1:lc(rawdir))//'/'//fi(ifi)(1:lc(fi(ifi)))
        call AddFitsExt(file)
        print *, 'Reading flat: ',file(1:lc(file))
        call readfits(file,b,maxpix,header,ok)
        call GetDimensions(header,sc,ec,sr,er,nc,nr)
        print *, 'nc,nr=',nc,nr,nc*nr
        if (ifi.eq.nfi) then
          do j=1,nc*nr
            a(j) = b(j)
          enddo
        else
          do j=1,nc*nr
            a(j) = a(j) + b(j)
          enddo
        endif
      enddo
      do j=1,nc*nr
        a(j) = a(j) / float(nfi)
      enddo
C
      return
      end


C----------------------------------------------------------------------
C Sort image.list file.
C
      subroutine mp_image_list_sort(imfile,simfile)
C
      implicit none
      character*(*) imfile,simfile
      character*200 firstline
C
      include 'makeepipe.inc'
C
      integer*4 ii,jj,dt,lc
      logical mp_match,ok

C Load in block. 
      call mp_load_block(imfile,ok)

C Load in lines from file.
      open(1,file=imfile,status='old')
      read(1,'(a)',end=9,err=9) firstline
      do ii=1,mp_nfi
        read(1,'(a)',end=9,err=9) lline(ii)
        mp_flag(ii)=0
C Do not list averages.
        if (mp_type(ii).eq.'FLAV') mp_flag(ii)=1
      enddo
      close(1)

C Open output file.
      open(2,file=simfile,status='unknown')

C First line.
      write(2,'(a)') firstline(1:lc(firstline))

C Group according to setup.
      DO ii=1,mp_nfi
      IF (mp_flag(ii).eq.0) THEN

C Deliminating line.
      write(2,'(a)') '...'

C Grab and write line.
      write(2,'(a)') lline(ii)(1:lc(lline(ii)))
      mp_flag(ii)=1

C Write other lines with matching setup.
      do jj=1,mp_nfi
      if (mp_flag(jj).eq.0) then
        if (mp_match(ii,jj,dt,1)) then
          write(2,'(a)') lline(jj)(1:lc(lline(jj)))
          mp_flag(jj)=1
        endif
      endif
      enddo

      ENDIF
      ENDDO
      
C Final deliminating line.
      write(2,'(a)') '...'

C Close output file.
      close(2)
      return

9     continue
      print *, 'ERROR making sorted listing...'
      return
      end


C----------------------------------------------------------------------
C Create 1D bias image files using files given in imfile.
C Creates bias images of different binnings: 1x1, 1x2, 2x2, etc. up to 8x8.
C Filenames and table values are stored in "makeepipe.inc" common block.
C
      subroutine BiasMaker(ok)
C
      implicit none
      logical ok                 ! Success? (output).
C
C Includes big arrays: obj() and flat(), and sets maxpix, maxcol, maxord.
      include 'makeepipe.inc'
C
C Maximum number of bias files.
      integer*4 maxbf
      parameter(maxbf=10)
C
      real*4 biases(maxcol,maxbf),bias(maxcol),arr(maxcol)
C
      integer*4 ii,col,narr,ixb,iyb,xb,yb,ndark,idark
      integer*4 sc,ec,sr,er,nc,nr,fsc,fec,fsr,fer,lc
      character*80 file,wrd
      character*57600 header
      real*4 eperdn,ronoise
C
      common /BIASMAKERBLK/ fsc,fec,fsr,fer
C

C Different binnings.
      DO ixb=0,3
      DO iyb=0,3

C Set binnings.
      xb = 2**ixb
      yb = 2**iyb

C Go through list and find DARKs.
      ndark=0
      DO ii=1,mp_nfi
      IF (mp_type(ii).eq.'DARK') THEN                        ! Only short darks.
      IF ((mp_xbin(ii).eq.xb).and.(mp_ybin(ii).eq.yb)) THEN  ! same binning?

C Count.
      ndark=ndark+1
      IF (ndark.le.maxbf) THEN          ! Maximum number needed.

C DARK filename.
      file = rawdir(1:lc(rawdir))//'/'//mp_fi(ii)(1:lc(mp_fi(ii)))
      call AddFitsExt(file)
C Read FITS data.
      call readfits(file,obj,maxpix,header,ok)
      if (.not.ok) goto 900
C Set global instrument flags.
      call EE_CheckInstrument( header )
C Set mask file and eperdn and ronoise if first dark.
      if (ndark.lt.2) then
        call mk_select_maskfile(header,maskfile)
        call mk_select_eperdn_ronoise(xb,yb,user_eperdn,
     .                  user_ronoise,header,eperdn,ronoise)
      endif
C Size.
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
C If necessary, fix header cards and Baseline-Interpolate-Window.
      call FixKeckCards(file,header,eperdn,ronoise)
      call BIW(obj,flat,header,maskfile,0,sc,ec,sr,er,ok)
      if (.not.ok) goto 900
C Check.
      if (ndark.eq.1) then
        fsc=sc
        fec=ec
        fsr=sr
        fer=er
      else
        if ( (fsc.ne.sc).or.(fec.ne.ec).or.(fsr.ne.sr).or.
     .                                       (fer.ne.er) ) then
          print *,
     .  'ERROR: Dimensions do not match on darks with same binning.'
          goto 900
        endif
      endif
C Bad pixel masking.
      if (maskfile.ne.' ') then
        print *,'Masking bad regions...'
        call Mask_Bad_Regions(obj,sc,ec,sr,er,maskfile)
      endif
C Mush bias image (replace columns with median values).
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
C Check.
      if ((1+ec-sc).gt.maxcol) then
        print *,
     .  'ERROR: Programming: Need larger array in BiasMaker().'
        goto 900
      endif
      call CB_Mush(obj,sc,ec,sr,er,flat,biases(1,ndark))

      ELSE

C Too many.
      print *,'More darks than needed, only using first ',maxbf
      ndark=ndark-1

      ENDIF      ! IF (ndark.le.maxbf) THEN  ...
      ENDIF      ! IF ((mp_xbin(ii).eq.xb).and.(mp_ybin(ii).eq.yb)....
      ENDIF      ! IF (mp_type(ii).eq.'DARK') THEN ...
      ENDDO      ! DO ii=1,mp_nfi

C Construct 1D bias image file, if we found some darks.
      if (ndark.gt.0) then
        do col=1,nc
          narr=0
          do idark=1,ndark
            narr=narr+1
            arr(narr) = biases(col,idark)
          enddo
          call find_median(arr,narr,bias(col))
        enddo
C Set header cards.
        call inheadset('NAXIS2',1,header)
        call inheadset('CRVAL2',0,header)
        call cheadset('CTYPE1','PIXEL',header)
        write(wrd,'(a,i1,a,i1)') 'Mushed Bias Image: ',xb,'x',yb
        call cheadset('OBJECT',wrd,header)
C Write out Mush image.
        write(wrd,'(a,i1,a,i1,a)') 'bias1d_',xb,'x',yb,'.fits'
        print *,' '
        print *,':::: Writing new 1D BIAS file: ',wrd(1:lc(wrd))
        print *,' '
        call writefits(wrd,bias,header,ok)
      endif

      ENDDO      ! DO ixb=0,3
      ENDDO      ! DO iyb=0,3

800   continue
      ok=.true.
      return

C Error.
900   continue
      print *,'ERROR: Creating bias image, use default.'
      ok=.false.
      return
      end

C---------------------------------------------------------------------- 
C Should we exclude this "file" from listing?
C
C "exclude.list" should contain a list of root filenames to be excluded,
C    e.g. "hires0034.fits", not the entire path.
C
      logical function ExcludeFromList(file)
C
      implicit none
      character*(*) file
C
      character*15 readflag
      integer*4 nf,fc,lc,access,i,j,revindex
      character*40 xfi(900)
      character*200 wrd
C
      common /EXCLUDEFROMLISTBLK/ nf
C
C Load exclude.list filenames, if not loaded yet.
      if (readflag.ne.'ThisHasBeenRead') then
        nf=0
        if (access('exclude.list','r').eq.0) then
          open(1,file='exclude.list',status='old')
101       read(1,'(a)',end=501) wrd
          wrd = wrd(fc(wrd):lc(wrd))
          if (lc(wrd).gt.80) then      ! reading an image.list type file?
            i=index(wrd,'.fits')       ! pull filename off line.
            if (i.gt.0) then
              j = revindex(wrd(1:i),' ')
              if (j.gt.0) then
                wrd = wrd(j+1:i+4)
              else
                wrd = ' '
              endif
            else
              wrd = ' '
            endif
          endif
          if (lc(wrd).gt.1) then
            nf = nf + 1
            xfi(nf) = wrd(1:lc(wrd))
          endif
          goto 101
501       close(1)
        endif
        readflag='ThisHasBeenRead'
      endif
C 
C Look for file.
      ExcludeFromList=.false.
      i=1
      do while((.not.ExcludeFromList).and.(i.le.nf))
        j = lc(xfi(i))
        if (index(file,xfi(i)(1:j)).ne.0) ExcludeFromList=.true.
        i=i+1
      enddo
C
      return
      end

C---------------------------------------------------------------------- 
C Should we exclude this "file" from reductions?  (Note: this exclusion does
C    not prevent a file from being used as a "STAR" or "ARCL" or "FLAT".)
C
C "exclude.redux" should contain a list of root filenames to be excluded,
C    e.g. "hires0034.fits", not the entire path.
C
      logical function ExcludeFromRedux(file)
C
      implicit none
      character*(*) file
C
      character*15 readflag
      integer*4 nf,fc,lc,access,i,j,revindex
      character*40 xfi(900)
      character*200 wrd
C
      common /EXCLUDEFROMREDUXBLK/ nf
C
C Load exclude.redux filenames, if not loaded yet.
      if (readflag.ne.'ThisHasBeenRead') then
        nf=0
        if (access('exclude.redux','r').eq.0) then
          open(1,file='exclude.redux',status='old')
101       read(1,'(a)',end=501) wrd
          wrd = wrd(fc(wrd):lc(wrd))
          if (lc(wrd).gt.80) then      ! reading an image.list type file?
            i=index(wrd,'.fits')       ! pull filename off line.
            if (i.gt.0) then
              j = revindex(wrd(1:i),' ')
              if (j.gt.0) then
                wrd = wrd(j+1:i+4)
              else
                wrd = ' '
              endif
            else
              wrd = ' '
            endif
          endif
          if (lc(wrd).gt.1) then
            nf = nf + 1
            xfi(nf) = wrd(1:lc(wrd))
          endif
          goto 101
501       close(1)
        endif
        readflag='ThisHasBeenRead'
      endif
C 
C Look for file.
      ExcludeFromRedux=.false.
      i=1
      do while((.not.ExcludeFromRedux).and.(i.le.nf))
        j = lc(xfi(i))
        if (index(file,xfi(i)(1:j)).ne.0) ExcludeFromRedux=.true.
        i=i+1
      enddo
C
      return
      end


C----------------------------------------------------------------------
C Estimate eperdn and ronoise for various binning factors.
C
C (Not yet) Create "eperdn.dat" and "ronoise_(xbin)x(ybin).dat" (if requested).
C
      subroutine mp_find_eperdn_ronoise()
C
      implicit none
C
      include 'makeepipe.inc'
      include 'global.inc'
      include 'soun.inc'
C
      integer*4 i,k,xbin,ybin,lc,filenumber
      real*4 eperdn,ronoise,r4
      character*80 wrd,file
C
C Information file.
      open(16,file='Measured_eperdn.txt',status='unknown')
C
      do xbin=1,8
      do ybin=1,8
      k=0
      do i=1,mp_nfi
        if ((mp_type(i).eq.'DARK').and.
     .      (mp_xbin(i).eq.xbin).and.(mp_ybin(i).eq.ybin)) then
          k=k+1
          filenumber = i
        endif
      enddo
      if (k.gt.1) then
        write(soun,'(a,2i4)') 
     .  '.....Find eperdn,ronoise for binning:',xbin,ybin
        call mp_eperdn_ronoise(filenumber,eperdn,ronoise)
        write(soun,'(a,2i3,2(a,f8.4))') 'binning=',xbin,ybin,
     .          ' : eperdn=',eperdn,'    ronoise=',ronoise
        write(16  ,'(a,2i3,2(a,f8.4))') 'binning=',xbin,ybin,
     .          ' : eperdn=',eperdn,'    ronoise=',ronoise
C 
C Check globals. 
        call EE_CheckInstrument(' ')
C For reference only...
        write(soun,'(2x)')
        write(soun,'(a)') 
     .  'For reference purposes the default values are:'
        call GetMakeeHome(wrd)
        write(file,'(2a,i1,a,i1,a)') wrd(1:lc(wrd)),
     .                  '/ronoise_',xbin,'x',ybin,'.dat'
        call mk_read_dat(file,r4)

        write(soun,'(2a,f9.4)') file(1:lc(file)),':',r4
        if(Global_HIRES)file=wrd(1:lc(wrd))//'/eperdnHIRES_higain.dat'
        if(Global_ESI  )file=wrd(1:lc(wrd))//'/eperdnESI_higain.dat'
        call mk_read_dat(file,r4)
        write(soun,'(2a,f9.4)') file(1:lc(file)),':',r4

        if(Global_HIRES)file=wrd(1:lc(wrd))//'/eperdnHIRES_logain.dat'
        if(Global_ESI  )file=wrd(1:lc(wrd))//'/eperdnESI_logain.dat'
        call mk_read_dat(file,r4)
        write(soun,'(2a,f9.4)') file(1:lc(file)),':',r4
C
        write(soun,'(2x)')
        write(soun,'(a)') 
     .  'To set new values, create "eperdn.dat" and'
        write(soun,'(a)') 
     .  '"ronoise_(xbin)x(ybin).dat" (e.g. ronoise_1x2.dat)'
        write(soun,'(a)') 
     .  'in your working directory.  Enter a single value'
        write(soun,'(a)') 'on the first line of each of these files.'
        write(soun,'(a)') 'Or you can use the eperdn= and ronoise='
        write(soun,'(a)') 'options in makee.'
        write(soun,'(2x)')
C
      endif
      enddo
      enddo
C
      close(16)
C
      return
      end


C----------------------------------------------------------------------
C Estimate eperdn and ronoise for given binning factors (in header of
C  given file number).
C
      subroutine mp_eperdn_ronoise(filenumber,eperdn,ronoise)
C
      implicit none
      integer*4 filenumber     ! file number of example FITS file (input)
      real*4 eperdn            ! electrons per digital number (output)
      real*4 ronoise           ! readout noise in electrons (output)
C
      include 'makeepipe.inc'
      include 'soun.inc'
C
      integer*4 xbin,ybin
      character*57600 header
      character*80 file1,file2
      real*4 rov(5),epd(19),mdnrov
      integer*4 nrov,nepd,i,j,lc
      logical ok,mp_same_flat

C Use first file to get an example FITS header and check globals.
      file1 = mp_fi(filenumber)
      call AddFitsExt(file1)
      file1 = rawdir(1:lc(rawdir))//'/'//file1(1:lc(file1))
      call readfits_header(file1,header,ok)
      if (.not.ok) then
        write(soun,'(2a)') 
     .    'ERROR: could not read file: ',file1(1:lc(file1))
        call exit(0)
      endif
C Set global instrument flag.
      call EE_CheckInstrument( header )

C Grab binning value.
      call GetBinning(header,xbin,ybin)

C Find maskfile for the requested binning.
      call mk_select_maskfile(header,maskfile)

C Select pairs of darks.
      nrov = 0
      i=0
      do while ((i.le.mp_nfi).and.(nrov.lt.5))
        file1=' '
        i=i+1
        do while((file1.eq.' ').and.(i.le.mp_nfi))
          if ( (mp_type(i).eq.'DARK').and.
     .         (mp_xbin(i).eq.xbin).and.(mp_ybin(i).eq.ybin) ) then
            file1=mp_fi(i)
          else
            i=i+1
          endif
        enddo
        file2=' '
        i=i+1
        do while((file2.eq.' ').and.(i.le.mp_nfi))
          if ( (mp_type(i).eq.'DARK').and.
     .         (mp_xbin(i).eq.xbin).and.(mp_ybin(i).eq.ybin) ) then
            file2=mp_fi(i)
          else
            i=i+1
          endif
        enddo
C Check and run.
        if ((file1.eq.' ').or.(file2.eq.' ')) then
          i=mp_nfi+1
        else
C Add directory name.
         file1 = rawdir(1:lc(rawdir))//'/'//file1(1:lc(file1))
         file2 = rawdir(1:lc(rawdir))//'/'//file2(1:lc(file2))
         write(soun,'(4a)')
     .  'Darks: ',file1(1:lc(file1)),', ',file2(1:lc(file2))
         nrov=nrov+1
         call ReadoutVariance(file1,file2,maskfile,obj,flat,
     .                                      header,rov(nrov),ok)
         write(soun,'(a,i4,a,f8.4)') 'ROV(',nrov,')=',rov(nrov)
         if (.not.ok) return
        endif
      enddo

C Any darks? Find median value of rov.
      if (nrov.gt.0) then
        call find_median(rov,nrov,mdnrov)
        write(soun,'(a,f8.4)')'Median readout variance (DN)=',mdnrov
      else
         write(soun,'(2a)')
     .   'WARNING: no darks, set readout variance to 1.'
         mdnrov = 1.
      endif

C Select pairs of flats.
      nepd = 0
      i=0
      do while ((i.le.mp_nfi).and.(nepd.lt.19))
        file1=' '
        i=i+1
        do while((file1.eq.' ').and.(i.le.mp_nfi))
          if ( (mp_type(i).eq.'FLAT').and.
     .           (mp_xbin(i).eq.xbin).and.(mp_ybin(i).eq.ybin) ) then
            file1=mp_fi(i)
          else
            i=i+1
          endif
        enddo
C Find a match for this one.
        j=i+1
        file2=' '
        do while ((file2.eq.' ').and.(j.lt.mp_nfi))
          if (mp_same_flat(i,j)) then
            file2 = mp_fi(j)
          else
            j=j+1
          endif
        enddo
C Check and run.
        if ((file1.eq.' ').or.(file2.eq.' ')) then
          continue
        else
          file1 = rawdir(1:lc(rawdir))//'/'//file1(1:lc(file1))
          file2 = rawdir(1:lc(rawdir))//'/'//file2(1:lc(file2))
          write(soun,'(4a)')
     .  'Flats: ',file1(1:lc(file1)),', ',file2(1:lc(file2))
          nepd=nepd+1
          call InverseGain(file1,file2,maskfile,obj,flat,header,
     .                                         mdnrov,epd(nepd),ok)
          write(soun,'(a,i4,a,f8.4)') 'EPD(',nepd,')=',epd(nepd)
          if (.not.ok) return
        endif
        i=i+1
      enddo
C Any flat pairs? Find median value of EPD.
      if (nepd.gt.0) then
        call find_median(epd,nepd,eperdn)
        write(soun,'(a,f8.4)') 'Median eperdn=',eperdn
      else
        write(soun,'(2a)')
     .  'WARNING: no flat pairs, set eperdn to 1.'
        eperdn = 1.
      endif
      ronoise = sqrt(max(0.0001,mdnrov)) * eperdn

      return
      end

C----------------------------------------------------------------------
C
      logical function mp_same_flat(i,j)
C
      integer*4 i,j
C
      include 'makeepipe.inc'
C
      mp_same_flat = ( (abs(mp_echa(i)-mp_echa(j)).lt.0.01).and.
     .                  (abs(mp_xda(i)-mp_xda(j)).lt.0.01).and.
     .                  (mp_expos(i).eq.mp_expos(j)).and.
     .                  (mp_type(i).eq.mp_type(j)).and.
     .                  (mp_slit(i).eq.mp_slit(j)).and.
     .                  (mp_fil1(i).eq.mp_fil1(j)).and.
     .                  (mp_lfil(i).eq.mp_lfil(j)).and.
     .                  (mp_xbin(i).eq.mp_xbin(j)).and.
     .                  (mp_ybin(i).eq.mp_ybin(j)).and.
     .                  (mp_deck(i).eq.mp_deck(j)) )
C
      return
      end
