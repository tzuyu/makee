C eeu2.f                                tab  1995-1999

CENDOFMAIN

C------------------------------------------------------------------------
C Get Variance for a pixel.  Uses EE common block.
C
      real*4 function EE_getvar(obj,flat)
C
      implicit none
      real*4 obj,flat
      include 'global.inc'
      if (flat.lt.1.e-10) then
        EE_getvar = 1.e+10
      else
        EE_getvar = 
     .  ( (max(0.,obj)/Global_eperdn) + (Global_rov/flat) ) / flat
      endif
      return
      end

C------------------------------------------------------------------------
C Get Variance for a pixel.  Uses PSE common block.
C
      real*4 function PSE_getvar(obj,flat)
C
      implicit none
      real*4 obj,flat
      include 'global.inc'
      if (flat.lt.1.e-10) then
        PSE_getvar= 1.e+10
      else
        PSE_getvar= 
     .  ((max(0.,obj)/Global_eperdn)+(Global_rov/flat))/flat
      endif
      return
      end

C----------------------------------------------------------------------
C Find the mean of the flat field in each order.
      subroutine EE_FlatMeans(sc,ec,sr,er,flat)
      implicit none
      integer*4 sc,ec,sr,er
      real*4 flat(sc:ec,sr:er),sum
      integer*4 eon,num
      include 'makee.inc'
      include 'global.inc'
      do eon=1,Global_neo
        call EE_OrderSum(sc,ec,sr,er,flat,eon,0.,sum,num,20)
        fltmn(eon)=sum/float(max(1,num))
      enddo
      return
      end

C----------------------------------------------------------------------
C Determines a scaling factor for the flat field image using the 
C order traces already determined.
      subroutine EE_FlatScale(sc,ec,sr,er,flat,scale_factor)
      implicit none
      integer*4 sc,ec,sr,er
      real*4 flat(sc:ec,sr:er),scale_factor,sum
      integer*4 eon,n,num
      include 'global.inc'
      include 'scratch.inc'
      n=0
      do eon=1,Global_neo
        call EE_OrderSum(sc,ec,sr,er,flat,eon,0.,sum,num,20)
        n=n+1
        arr(n)=sum/float(max(1,num))
      enddo
      call find_median(arr,n,scale_factor)
      return
      end

C----------------------------------------------------------------------
C Guesses at a scaling factor for the flat field image by taking a mean
C of all pixels above "lower_limit".
C
      subroutine EE_FlatScaleGuess(sc,ec,sr,er,flat,
     .  lower_limit,scale_factor)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 flat(sc:ec,sr:er),lower_limit,scale_factor,sum
      integer*4 row,col,num
      include 'makee.inc'
      num=0
      sum=0.
      do col=sc,ec
        do row=sr,er
          if (flat(col,row).gt.lower_limit) then
            num=num+1
            sum=sum+flat(col,row)
          endif
        enddo
      enddo
      scale_factor = sum / float(max(1,num))
      return
      end

C----------------------------------------------------------------------
C Divide image A by image B.  Ignores pixels in B which are less than 1.e-5.
C Do not divide pixels which are less than -1.e+5 in A (masked pixels).
C
      subroutine EE_ImageDivide(sc,ec,sr,er,A,B)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 A(sc:ec,sr:er),B(sc:ec,sr:er)
      integer*4 row,col
      do col=sc,ec
        do row=sr,er
          if (A(col,row).gt.-1.e+5) then
            if (B(col,row).lt.1.e-5) then
              A(col,row) = 0.
            else
              A(col,row) = A(col,row) / B(col,row)
            endif
          endif
        enddo
      enddo
      return
      end

C----------------------------------------------------------------------
C Divide image A by a constant.  Ignore masked pixels.
C
      subroutine EE_ImageDivideConst(sc,ec,sr,er,A,constant)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 A(sc:ec,sr:er),constant
      integer*4 row,col
C
      include 'soun.inc'
C
      if (constant.lt.1.e-5) then
        write(soun,'(a,1pe15.7)') 
     .   'WARNING: EE_ImageDivideConst: constant to small : ',constant
        return
      endif
      do col=sc,ec
        do row=sr,er
          if (A(col,row).gt.-1.e+5) then
            A(col,row) = A(col,row) / constant
          endif
        enddo
      enddo
      return
      end
      
C----------------------------------------------------------------------
C Test ranges.
C
      subroutine test_range(sc,ec,sr,er,a)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er)
      include 'makee.inc'   
      include 'global.inc'   
      integer*4 i,col,row
      real*8 polyarroff
      print *,'test_range:  neo=',Global_neo
      do i=1,Global_neo 
        do col=sc,ec
          row = nint( polyarroff(bk1o(0,i),dfloat(col)) )
          row = min(er,max(sr,row))
          a(col,row) = -1.e+10
          row = nint( polyarroff(sp1(0,i),dfloat(col)) )
          row = min(er,max(sr,row))
          a(col,row) = -1.e+10
          row = nint( ( polyarroff(sp1(0,i),dfloat(col))
     .             + polyarroff(sp2(0,i),dfloat(col)) ) / 2.d0 )
          row = min(er,max(sr,row))
          a(col,row) = -1.e+10
          row = nint( polyarroff(sp2(0,i),dfloat(col)) )
          row = min(er,max(sr,row))
          a(col,row) = -1.e+10
          row = nint( polyarroff(bk2o(0,i),dfloat(col)) )
          row = min(er,max(sr,row))
          a(col,row) = -1.e+10
        enddo
      enddo
      return
      end

C----------------------------------------------------------------------
C Is the given column near a sky-line?
C This routine returns the sigmas significance of a possible sky-line.
C Use columns offset by "os1" and "os2" from column.
C If either os1 or os2 is >90 or <90, then it will not be considered.
C
      subroutine PSE_NearSky(sc,ec,sr,er,obj,flat,rowbnd,
     .  col,os1,os2,skysig)
C
      implicit none
      integer*4 sc,ec,sr,er,rowbnd(4),col,os1,os2
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er),skysig
      real*4 err(0:2),mdntop(0:2),mdnbot(0:2),errtop,errbot
      real*4 mdnflat,PSE_getvar,error,ss1,ss2,sstop,ssbot
      integer*4 i,j,jj,n,off(0:2),k
      logical PSE_Masked
C
C###  include 'pse.inc'
      include 'scratch.inc'
C
C Median of flat in this column.
      n=0
      do jj=0,2,2
        do j=rowbnd(1+jj),rowbnd(2+jj)
          if (.not.PSE_Masked(flat(col,j))) then
            n=n+1
            arr(n) = flat(col,j)
          endif
        enddo
      enddo
      call find_median(arr,n,mdnflat)
C Column offsets.
      off(0)=0
      off(1)=os1
      off(2)=os2
C Top side median of columns.
      do k=0,2
        if (abs(off(k)).lt.90) then
          i=max(sc,min(ec,col+off(k)))
          n=0
          do j=rowbnd(1),rowbnd(2)
            if (.not.PSE_Masked(obj(i,j))) then
              n=n+1
              arr(n)=obj(i,j)
            endif
          enddo
          if (n.gt.2) then
            call find_median(arr,n,mdntop(k))
          else
            call find_average(arr,n,mdntop(k))
          endif
          err(k) = sqrt(PSE_getvar(mdntop(k),mdnflat))
        else
          err(k) = err(0)
        endif
      enddo
      errtop= max(1.e-10,min(min(err(0),err(1)),err(2)))
C Bottom side median of columns.
      do k=0,2
        if (abs(off(k)).lt.90) then
          i=max(sc,min(ec,col+off(k)))
          n=0
          do j=rowbnd(3),rowbnd(4)
            if (.not.PSE_Masked(obj(i,j))) then
              n=n+1
              arr(n)=obj(i,j)
            endif
          enddo
          if (n.gt.2) then
            call find_median(arr,n,mdnbot(k))
          else
            call find_average(arr,n,mdnbot(k))
          endif
          err(k) = sqrt(PSE_getvar(mdnbot(k),mdnflat))
        else
          err(k) = err(0)
        endif
      enddo
      errbot= max(1.e-10,min(min(err(0),err(1)),err(2)))
      error = (errbot+errtop)/2. 
C Find sky sigmas.
      ss1=0.
      ss2=0.
      if (abs(off(1)).lt.90) ss1=abs(mdntop(1)-mdntop(0))/error
      if (abs(off(2)).lt.90) ss2=abs(mdntop(2)-mdntop(0))/error
      sstop = max(ss1,ss2)
      if (abs(off(1)).lt.90) ss1=abs(mdnbot(1)-mdnbot(0))/error
      if (abs(off(2)).lt.90) ss2=abs(mdnbot(2)-mdnbot(0))/error
      ssbot = max(ss1,ss2)
C In case one side is effected by a large CR group.
      skysig= min(sstop,ssbot)
      return
      end

C----------------------------------------------------------------------
C Calculate a median in the dispersion direction. Result in prof(col,row).
C Median box size determined by PSEOM_ProfMdnBox. Values less than +1.e-15
C are set to zero, so that median will be positive.
C
      subroutine PSE_ProfMdn(usc,uec,sc,ec,sr,er,obj,ssr,ser,
     .  sky,prof,col,row)
C
      implicit none
      integer*4 usc,uec,sc,ec,sr,er,ssr,ser
      real*4 obj(sc:ec,sr:er),sky(sc:ec,ssr:ser),prof(sc:ec,ssr:ser)
      integer*4 col,row,i,n
      logical PSE_Masked
C
      include 'pse.inc'
      include 'scratch.inc'
C
      n=0
      do i=max(usc,col-(PSEOM_ProfMdnBox/2)),
     .  min(uec,col+(PSEOM_ProfMdnBox/2))
        if (.not.PSE_Masked(obj(i,row))) then
          n=n+1
          arr(n)= obj(i,row) - sky(i,row)
        endif
      enddo
C Mask whole profile for this row if not enough good pixels.
      if (n.lt.max(2,PSEOM_ProfMdnBox/3)) then
        call PSE_Mask(prof(col,row),1.)
      else
        call find_median(arr,n,prof(col,row))
        if (prof(col,row).lt.1.e-15) prof(col,row)=0.
      endif
      return
      end


C----------------------------------------------------------------------
C Find best object width and centroid from list.
C
      subroutine EE_FO_BestWidthCentroid(neo,ow,cent,peak,fwhm,
     .                                   mdnbg,rmsbg)
C
      implicit none
      integer*4 neo                                    ! input
      real*4 ow(*),cent(*),peak(*),fwhm(*)             ! input/output
      real*4 mdnbg(*),rmsbg(*)                         ! input
C
      include 'makee.inc'                     ! need EEFO_Polyoffset
      include 'scratch.inc'                   ! need arr() scratch array.
      include 'global.inc'                    ! need Global_HIRES
      include 'soun.inc'                      ! need Global_HIRES
C
      integer*4 ii,iilo,kk,nn,ihidev,ord
      integer*4 maxrej,numrej
      real*4 median_ow,lo,dev,hidev
      real*8 polyval
      logical ok,again,rejection
C
C Weight all points 1 initially.
      do ii=1,neo
        x8(ii) = dfloat(ii)
        y8(ii) = dble(cent(ii))
        w8(ii) = 1.
      enddo
C
C If not all orders ok, use only those marked.
      kk=0
      do ii=1,neo
        if (.not.oko(ii)) kk=kk+1
      enddo
      IF (kk.gt.0) THEN
        write(soun,'(a)') 'NOTE: ..................................'
        write(soun,'(a)') 'NOTE: Overriding normal order centroids.'
        do ii=1,neo
          if (oko(ii)) then
            write(soun,'(a,i3)') 'NOTE: Using order (row): ',ii
          else
            w8(ii) = 0.
          endif
        enddo
        write(soun,'(a)') 'NOTE: ..................................'
C Choose median object width of non-rejected values.
        nn=0
        do ii=1,neo
          if (w8(ii).gt.0.) then
            nn=nn+1
            arr(nn) = ow(ii)
          endif
        enddo
        call find_median(arr,nn,median_ow)
C Fit straight line through points.
        ord = 1
        call poly_fit_glls(neo,x8,y8,w8,ord,coef,ok)
        if (.not.ok) write(soun,'(a)')
     .     'ERROR: EE_FO_BWC:0: Fitting polynomial.'
C Replace centroids with fit and object widths with median.
        do ii=1,neo
          cent(ii) = sngl(polyval(ord+1,coef,dfloat(ii)))
          ow(ii) = median_ow
        enddo
        return
      ENDIF
C
C
C ESI: Reject the first two orders.
      if (Global_ESI) then
        w8(1)  =0.
        w8(2)  =0.
      endif

C Reject three peaks for low S/N.
      if ((neo.gt.7).and.(Global_ESI)) then
        do kk=1,3
          iilo = 0
          lo   = 1.e+36
          do ii=1,neo
            if ((w8(ii).gt.0.).and.
     .                ((peak(ii)-mdnbg(ii))/rmsbg(ii).lt.lo)) then
              iilo = ii
              lo   = (peak(iilo) - mdnbg(iilo)) / rmsbg(iilo)
            endif
          enddo
          if (iilo.gt.0) w8(iilo) = 0.
        enddo
      endif

C Now choose median object width of non-rejected values.
      nn=0
      do ii=1,neo
        if (w8(ii).gt.0.) then
          nn=nn+1
          arr(nn) = ow(ii)
        endif
      enddo
      call find_median(arr,nn,median_ow)

C For HIRES, use old method.
      IF (Global_HIRES) THEN

C Fit Nth order polynomial to centroid.
        ord = EEFO_PolyOffset
        if (neo.lt.10) ord=1
C Reject worse 10% of points-- one at a time.
        do kk=1,max(nint(float(neo)*0.10),2)
          call poly_fit_glls(neo,x8,y8,w8,ord,coef,ok)
          if (.not.ok) write(soun,'(a)')
     .     'ERROR: EE_FO_BWC:1: Fitting polynomial.'
          ihidev= 0
          hidev = -1.e+30
          do ii=1,neo
            if (w8(ii).gt.0.) then
              dev = sngl(abs(y8(ii)-polyval(ord+1,coef,x8(ii))))
              if (dev.gt.hidev) then
                ihidev= ii
                hidev = dev
              endif
            endif
          enddo
          if (ihidev.gt.0) w8(ihidev)=0.d0
        enddo
C Replace centroids with fit.
        do ii=1,neo
          cent(ii) = sngl(polyval(ord+1,coef,dfloat(ii)))
        enddo

C New technique for ESI.
      ELSE

C Maximum rejection. Must have at least 4 points left.
C We've rejected five points already.
        maxrej = neo - 4
        numrej = 5

C Load polynomial order.
        ord = EEFO_PolyOffset
        if (neo.lt.5) ord=1

C Fit and reject bad points (more than 1 pixel away from fit)...
        again=.true.
C Fit initial polynomial.
        call poly_fit_glls(neo,x8,y8,w8,ord,coef,ok)
        if (.not.ok) write(soun,'(a)')
     .     'ERROR: EE_FO_BWC:2: Fitting polynomial.'
        if (numrej.ge.maxrej) again=.false.
C Fit and reject.
        do while( again )
          call poly_fit_glls(neo,x8,y8,w8,ord,coef,ok)
          if (.not.ok) write(soun,'(a)')
     .     'ERROR: EE_FO_BWC:3: Fitting polynomial.'
          ihidev= 0
          hidev = -1.e+30
          rejection = .false. 
          do ii=1,neo
            if ((.not.rejection).and.(w8(ii).gt.0.)) then
              dev = sngl(abs(y8(ii)-polyval(ord+1,coef,x8(ii))))
              if (dev.gt.1.) then
                rejection = .true. 
                w8(ii) = 0.
                numrej = numrej + 1
              endif
            endif
          enddo
          if ((.not.rejection).or.(numrej.ge.maxrej)) again=.false.
        enddo

C Replace centroids with fit.
        do ii=1,neo
          cent(ii) = sngl(polyval(ord+1,coef,dfloat(ii)))
        enddo

      ENDIF
C
C Load object widths.
      do ii=1,neo
        ow(ii) = median_ow
      enddo
C
      return
      end


C----------------------------------------------------------------------
C Write profile FITS file.
C
      subroutine EE_WriteProfileFile(pn,px,py)
C
      implicit none
C
      include 'soun.inc'
      include 'global.inc'
      include 'makee.inc'
C
      integer*4 pn(mo),pm
      parameter( pm = MK_mpv * mo )
      real*4 px(MK_mpv,mo),py(MK_mpv,mo)
      real*4 aa(pm),hix,lox
      integer*4 ii,eon,nc,nr,pixno,col,row,lc
      character*57600 header
      character*80 ffile
      logical ok
C
C Lowest and highest x value.
      lox=+1.e+30
      hix=-1.e+30
      do eon=1,Global_neo
        if (px(1,eon)      .lt.lox) lox = px(1,eon)
        if (px(pn(eon),eon).gt.hix) hix = px(pn(eon),eon)
      enddo
C Clear.
      do ii=1,pm
        aa(ii)=0.
      enddo
C Load values.
      ok=.true.
      nr = Global_neo
      nc = nint(hix - lox) + 1
      do eon=1,Global_neo
        row = eon
        do ii=1,pn(eon)
          col = nint( px(ii,eon) - lox )
          pixno = 1 + col + ((row - 1) * nc)
          if (pixno.lt.pm) then
            aa(pixno) = aa(pixno) + py(ii,eon)
          else
            ok=.false.
          endif
        enddo
      enddo
C Check.
      if (.not.ok) then
        write(soun,'(a)') 'WARNING: Profile file exceeded limits.'
      endif
C Build header.
      call make_basic_header(header,0,nc,0,nr)
C Filename.
      ii=max(-1,min(9999,Global_obsnum))
      ffile=' '
      if (ii.gt.999) then
        write(ffile,'(a,I4.4,a)') 'profiles-',ii,'.fits'
      else
        write(ffile,'(a,I3.3,a)') 'profiles-',ii,'.fits'
      endif
      write(soun,'(2a)') 'Writing FITS profile file: ',
     .                    ffile(1:lc(ffile))
      call cheadset('FILENAME',ffile,header)
      call cheadset('OBJECT',ffile,header)
      call cheadset('CTYPE1','PIXEL',header)
      call fheadset('CRVAL1',dble(lox),header)
      call fheadset('CRVAL2',1.d0,header)
C Write file.
      call writefits(ffile,aa,header,ok)
C
      return
      end


C----------------------------------------------------------------------
C Find object limits by probing the object flux.  Uses the bright star
C trace and the background limits determined previously.
C This routine also finds the profile model. Creates sp1(9,99) and sp2(9,99).
C Also sets bk1i() and bk2i().
C
      subroutine EE_FindObject(sc,ec,sr,er,obj)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 obj(sc:ec,sr:er)
C
      include 'makee.inc'
      include 'global.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
      real*4 fwhm(mo),peak(mo),bg(mo),ow(mo),cent(mo)
      real*4 rmsbg(mo),mdnbg(mo)
      real*4 trow,uprs,lors,rr4
      real*4 xp(MK_mpv),yp(MK_mpv),pd_xp(MK_mpv,mo),pd_yp(MK_mpv,mo)
      real*4 pd_bg(mo),pd_fwhm(mo)
      real*4 xtemp1(mo)
      real*4 EE_GetSkySpacer,EE_GetSkyMin
      real*4 xmin,xmax,ymin,ymax,aspp_spa
      real*4 save_ow(mo),save_cent(mo)
      real*4 ob1,ob2,obc,bb1o,bb2o,bb1i,bb2i
      real*8 cc,polyarroff,fhead
      integer*4 eon,i,ii,n,k,k2,pd_n(mo),lc
      character c40*40, c80*80, card*8

C Arcseconds per pixel in spatial direction.
      aspp_spa = sngl(fhead('ASPP_SPA',ObjectHeader))

C Center column.
      cc = dfloat(sc+ec)/2.d0

C Find object boundaries in each echelle order.
      DO eon=1,Global_neo

C Get slit boundaries using the previously determined bk1o and bk2o arrays.
        call EE_BackgroundLimits(eon,cc,trow,uprs,lors)
C Calculate profile model.
        call EE_MakeProfile(sc,ec,sr,er,obj,eon)
C Check.
        if (nint(pparm(1,eon)).gt.MK_mpv) then
          write(soun,'(a,i5,a,i5)') 
     .     'ERROR: EEFO: too many points in profile: pparm(1,eon)=',
     .     nint(pparm(1,eon)),'  MK_mpv=',MK_mpv
          call exit(0)
        endif
C Load profile into xp,yp array.
        n=0
        do i=1,nint(pparm(1,eon)) 
          n=n+1
          xp(n)=pparm(2,eon)+(float(i-1)*pparm(3,eon))
          yp(n)=pval(i,eon)
C Transfer into plot data array for later plotting.
          pd_xp(n,eon) = xp(n)
          pd_yp(n,eon) = yp(n)
        enddo
        pd_n(eon) = n
C Find object width (ow) and centroid (cent) for object profile.
        call EE_FindObjectWidth(n,xp,yp,
     .       ow(eon),cent(eon),fwhm(eon),bg(eon),
     .       peak(eon),mdnbg(eon),rmsbg(eon),eon)
        pd_bg(eon)  = bg(eon)
        pd_fwhm(eon)= fwhm(eon)

      ENDDO

C Save.
      do ii=1,Global_neo
        save_ow(ii)  = ow(ii)
        save_cent(ii)= cent(ii)
        xtemp1(ii)   = float(ii)
      enddo

C Find best object width and centroid curve.
      call EE_FO_BestWidthCentroid(Global_neo,ow,cent,
     .                             peak,fwhm,mdnbg,rmsbg)

C Override object width median if user requested.
      if (UserHalfWidth.gt.0.99) then
        do ii=1,Global_neo
          ow(ii) = UserHalfWidth
        enddo
      endif

C Override object widths and centers with "uop" values.
      if (uop_exist) then
        do ii=1,Global_neo
          if (uop_h(ii).gt.0.) then
            cent(ii)= uop_c(ii)
            ow(ii)  = uop_h(ii)
          endif
        enddo
      endif

C Save (re-fit) centroid and background for later use within PSE()...
      DO eon=1,Global_neo
        pparm(4,eon) = cent(eon)
        pparm(5,eon) = bg(eon)
      ENDDO

C Save a file called "profiles-###.fits" which contains the profiles.
      call EE_WriteProfileFile(pd_n,pd_xp,pd_yp)

C Create sp1(0:99,99) and sp2(0:99,99).
      do eon=1,Global_neo
        do i=0,mpp
          sp1(i,eon) = eo(i,eon)
          sp2(i,eon) = eo(i,eon)
        enddo
C Make sure object range is within background range.
        sp1(2,eon) = sp1(2,eon) + cent(eon) - ow(eon)
        sp1(2,eon) = max(sp1(2,eon),bk1o(2,eon)+0.1)
        sp2(2,eon) = sp2(2,eon) + cent(eon) + ow(eon)
        sp2(2,eon) = min(sp2(2,eon),bk2o(2,eon)-0.1)
C Check for very wide object range.
        if ( ((sp1(2,eon)-bk1o(2,eon)).lt.0.5).and.
     .       ((bk2o(2,eon)-sp2(2,eon)).lt.0.5) ) then
          write(soun,'(a)') 
     .  'ERROR: Object width is too wide.  ObjectFraction should be'
          write(soun,'(a)') 
     .  'ERROR:   smaller.  Forcing smaller object width.'
          sp1(2,eon) = max(sp1(2,eon),bk1o(2,eon)+0.5)
          sp2(2,eon) = min(sp2(2,eon),bk2o(2,eon)-0.5)
        endif
C Set inner background.
        do i=0,mpp
          bk1i(i,eon) = sp1(i,eon)
          bk2i(i,eon) = sp2(i,eon)
        enddo
        bk1i(2,eon) = sp1(2,eon) - EE_GetSkySpacer()
        bk2i(2,eon) = sp2(2,eon) + EE_GetSkySpacer()
C Move inner backgrounds inward if not enough sky.
        if ( (bk1i(2,eon)-bk1o(2,eon)).lt.EE_GetSkyMin() ) then
          bk1i(2,eon) = bk1o(2,eon) + EE_GetSkyMin()
        endif
        if ( (bk2o(2,eon)-bk2i(2,eon)).lt.EE_GetSkyMin() ) then
          bk2i(2,eon) = bk2o(2,eon) - EE_GetSkyMin()
        endif
C Store object center and half width for all orders in object header.
        write(c80,'(2(f9.2))') cent(eon),ow(eon)
        write(card,'(a,i2.2)') 'OBJPOS',eon
        call cheadset(card,c80,ObjectHeader)
C Override object backgrounds with "uop" values.
        if (uop_exist) then
          if(uop_b1o(eon).gt.-800.)bk1o(2,eon)=eo(2,eon)+uop_b1o(eon)
          if(uop_b1i(eon).gt.-800.)bk1i(2,eon)=eo(2,eon)+uop_b1i(eon)
          if(uop_b2i(eon).gt.-800.)bk2i(2,eon)=eo(2,eon)+uop_b2i(eon)
          if(uop_b2o(eon).gt.-800.)bk2o(2,eon)=eo(2,eon)+uop_b2o(eon)
        endif
      enddo

C  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .

C Open plot to show object widths, centroid offsets, and profiles.
      k=max(-1,min(9999,Global_obsnum))
      c40=' '
      if (k.gt.999) then
        write(c40,'(a,I4.4,a)') 'profiles-',k,'.ps'
      else
        write(c40,'(a,I3.3,a)') 'profiles-',k,'.ps'
      endif
      write(soun,'(2a)')'Writing PostScript plot file: ',c40(1:lc(c40))
      call EE_OpGrid(Global_neo+2,k,k2)
      c40 = c40(1:lc(c40))//'/PS'
      call PGBEGIN(0,c40,+1*k,k2)
      call PGASK(.false.)

C Plot object widths.
      call PGPAGE
      call PGSLW(1)
      call PGSLS(1)
      call PGSCF(1)
      call PGSCH(2.5)
      call PGWINDOW(0.5,float(Global_neo)+0.5,ow(1)/2.,ow(1)*2.)
      call PGBOX('BCNST',0.,0,'BCNST',0.,0)
C Plot original object widths.
      call PGPT(Global_neo,xtemp1,save_ow,3)
C Plot object width line.
      call PGSLS(4)
      call PGLINE(Global_neo,xtemp1,ow)
      call PGSLS(1)

C Report overriding object width median if user requested.
      if (UserHalfWidth.gt.0.99) then
        rr4 = min(99.,max(-9.,UserHalfWidth))
        write(soun,'(a,f6.3)') 
     .  ' NOTE: By user request object half width=',rr4
        write(c80,'(a,f6.3)') 'User value=',rr4
        call PGMTEXT('B',-1.2,0.5,0.5,c80)
      endif

C Informative labeling.
      write(c80,'(a,i4,3x,2a)') 
     .  ' Obs#',min(9999,max(-999,Global_obsnum)),
     .     Current_ObsDate,'   Object Half Widths'
      call PGMTEXT('T',0.5,0.0,0.0,c80)
      call PGMTEXT('B',1.9,0.5,0.5,'Order Number')

C Y minimum and maximum.
      ymin=cent(1)
      ymax=cent(1)
      do ii=1,Global_neo
        if (cent(ii).lt.ymin) ymin=cent(ii)
        if (cent(ii).gt.ymax) ymax=cent(ii)
      enddo
      if ((ymax-ymin).lt.2.) then
        ymin = ymin - 1.
        ymax = ymax + 1.
      endif

C Plot centroids.
      call PGPAGE
      call PGWINDOW(0.5,float(Global_neo)+0.5,ymin,ymax)
      call PGBOX('BCNST',0.,0,'BCNST',0.,0)
C Plot original centroids
      call PGPT(Global_neo,xtemp1,save_cent,3)
C Plot centroid fit.
      call PGSLS(4)
      call PGLINE(Global_neo,xtemp1,cent)
      call PGSLS(1)
      call PGMTEXT('T',0.5,1.0,1.0,
     .  'Object Centroids (Relative to Star)')

C Plot profiles.
      do eon=1,Global_neo

C Object and Background boundaries.
        ob1 = sngl( sp1(2,eon) - eo(2,eon) )
        ob2 = sngl( sp2(2,eon) - eo(2,eon) )
        obc = (ob1 + ob2) / 2.
        bb1o= sngl( bk1o(2,eon) - eo(2,eon) )
        bb1i= sngl( bk1i(2,eon) - eo(2,eon) )
        bb2i= sngl( bk2i(2,eon) - eo(2,eon) )
        bb2o= sngl( bk2o(2,eon) - eo(2,eon) )

C Show position of centroid, object width, background level, and bckgnd ranges.
        call PGPAGE
        call PGSLW(1)
        call PGSLS(1)
        call PGSCF(1)
        call PGSCH(2.5)
        call EE_SetPlotBox(pd_n(eon),pd_xp(1,eon),pd_yp(1,eon))
        call PGQWIN(xmin,xmax,ymin,ymax)
C Draw profile.
        call PGLINE(pd_n(eon),pd_xp(1,eon),pd_yp(1,eon))
C Draw object width boundaries and background level used when finding centroid.
        xp(1)=max(ob1,xmin+0.1)
        xp(2)=xp(1)
        xp(3)=min(ob2,xmax-0.1)
        xp(4)=xp(3)
        xp(5)=xp(3)
        xp(6)=obc
        xp(7)=xp(6)
        yp(1)=ymax
        yp(2)=pd_bg(eon)
        yp(3)=pd_bg(eon)
        yp(4)=ymax
        yp(5)=pd_bg(eon)
        yp(6)=pd_bg(eon)
        yp(7)=ymax
        call PGSLS(4)
        call PGLINE(7,xp,yp)
C Plot background limits.
        yp(1) = ymin
        yp(2) = ymin + ( (ymax - ymin) * 0.2 )
        yp(3) = yp(2)
        yp(4) = ymin
        xp(1) = bb1i
        xp(2) = bb1i
        xp(3) = bb1o
        xp(4) = bb1o
        call PGLINE(4,xp,yp)
        xp(1) = bb2i
        xp(2) = bb2i
        xp(3) = bb2o
        xp(4) = bb2o
        call PGLINE(4,xp,yp)
        call PGSLS(1)
C Informative labels.
        k = nint(polyarroff(eo(0,eon),dfloat((sc+ec)/2)))
        c40=' '
C 0.191 arcseconds per pixel in the spatial direction.
C1234xxcr=1234xxbg=123xxfwhm=12345123456
C  23  cr=1023  bg= 27  fwhm= 2.43  0.71"
C
C Changed this... tab 11apr99...
C  23  cr=1023  bg= 27  fwhm= 0.71"
        write(c40,'(i2.2,a,i4,a,i3,a,f6.2,a)') eon,
     .            '  cr=',min(9999,max(-999,k)),
     .            '  bg=',min(999,max(-99,nint(pd_bg(eon)))),
     .          '  fwhm=',min(99.,max(0.,pd_fwhm(eon)*aspp_spa)),'"'
        call PGMTEXT('T',0.4,0.0,0.0,c40)
      enddo

C Close plot.
      call PGEND
C
      return
      end


C----------------------------------------------------------------------
C Get minimum sky variable out of pse.inc .
      real*4 function EE_GetSkyMin()
      implicit none
      include 'pse.inc'
      EE_GetSkyMin = PSE_SkyMin
      return
      end

C----------------------------------------------------------------------
C Get sky spacing variable out of pse.inc .
      real*4 function EE_GetSkySpacer()
      implicit none
      include 'pse.inc'
      EE_GetSkySpacer = PSE_SkySpacer
      return
      end


C----------------------------------------------------------------------
C Find RMS using median.
        real*4 function EE_FOW_bg_RMS(mdn,arr,narr)
        implicit none
        real*4 mdn,rms
        real*4 arr(*)
        integer*4 narr,ii
        rms = 0.
        do ii=1,narr
          rms = rms + ( (arr(ii) - mdn) * (arr(ii) - mdn) )
        enddo
        if (narr.gt.0) then
          rms = sqrt( rms / float(narr) )
        else
          rms = 1.
        endif
        EE_FOW_bg_RMS = rms
        return
        end
          

C----------------------------------------------------------------------
C Returns median background level outside of object.
C If slit is large compared to "ow", then find median on either side
C of "cent" and take the larger value.
C
      real*4 function EE_FOW_bg( n, xp, yp, arr, cent, ow_input, rms )
C
      implicit none
      integer*4 n          ! number of points in profile (input).
      real*4 xp(*),yp(*)   ! object profile (input).
      real*4 arr(*)        ! scratch.
      real*4 ow_input,cent ! object (half) width and centroid (input).
      real*4 rms           ! Root-Mean-Square of background (output).
C
      integer*4 i,k,k1,k2
      real*4 bg,bg1,bg2,ratio,bgw_min,bgw_max,ow,rms1,rms2
      real*4 EE_FOW_bg_RMS
C
C What is the ratio of the object width and slit width (or length).
      ow = ow_input
      ratio = ow / abs(xp(n) - xp(1))
C
C If ratio too large, then clip "effective" object width.
      if (ratio.gt.0.9) then
        ow = abs(xp(n) - xp(1)) * 0.9
      endif
C
C Single background.
      if (ratio.gt.0.3) then
        k=0
        do i=1,n
          if (abs(xp(i)-cent).gt.(ow*1.2)) then
            k=k+1
            arr(k) = yp(i)
          endif
        enddo
C If not enough points. Add points from edges.
        if (k.lt.3) then
          k=k+1
          arr(k) = yp(1)
          k=k+1
          arr(k) = yp(2)
          k=k+1
          arr(k) = yp(n)
          k=k+1
          arr(k) = yp(n-1)
        endif
        call find_median(arr,k,bg)
        rms = EE_FOW_bg_RMS(bg,arr,k)
C
C Two backgrounds.
      else
C Right side.
        bgw_min = ow*1.2          ! minimum and maximum background widths.
        bgw_max = ow*2.4
        if ((bgw_max - bgw_min).lt.3.) bgw_max = bgw_min + 3.
        k1=0
        do i=1,n
          if ( ((xp(i)-cent).gt.bgw_min).and.
     .         ((xp(i)-cent).lt.bgw_max) ) then
            k1=k1+1
            arr(k1) = yp(i)
          endif
        enddo
        call find_median(arr,k1,bg1)
        rms1 = EE_FOW_bg_RMS(bg1,arr,k1)
C Left side.
        k2=0
        do i=1,n
          if ( ((cent-xp(i)).gt.bgw_min).and.
     .         ((cent-xp(i)).lt.bgw_max) ) then
            k2=k2+1
            arr(k2) = yp(i)
          endif
        enddo
        call find_median(arr,k2,bg2)
        rms2 = EE_FOW_bg_RMS(bg2,arr,k2)
C Use larger of two backgrounds.
        if ((k1.gt.2).and.(k2.gt.2)) then
          bg = max(bg1,bg2)
          rms= max(rms1,rms2)
        else
          if (k1.le.2) then
            bg = bg2
            rms= rms2
          else
            bg = bg1
            rms= rms1
          endif
        endif
      endif
C
      EE_FOW_bg = bg
C
      return
      end


C----------------------------------------------------------------------
C Returns centroid given initial guess for object centroid.
C
      real*4 function EE_FOW_cent( n, xp, yp, cent, ow, bg )
C
      implicit none
      integer*4 n          ! number of points in profile (input).
      real*4 xp(*),yp(*)   ! object profile (input).
      real*4 ow,cent,bg    ! object (half) width and (initial) centroid 
                           !    and background level (input).
C
      include 'soun.inc'
C
      integer*4 i
      real*4 newcent,r1,r2,r
C
      r1=0.
      r2=0.
      do i=1,n
        if (abs(xp(i)-cent).lt.ow) then
          r  = max(0.,yp(i)-bg)
          r1 = r1 + (xp(i)*r)
          r2 = r2 + (r)
        endif
      enddo
      if (r2.gt.1.e-10) then
        newcent = r1 / r2
      else
C Use the initial guess if problem.
        newcent = cent
      endif
      EE_FOW_cent = newcent
C
      return
      end


C----------------------------------------------------------------------
C Rough guess at profile FWHM.
C
      real*4 function EE_FOW_fwhm(n,xp,yp,cent,ow,bg,peak)
C
      implicit none
      integer*4 n           ! number of points (input).
      real*4 xp(*),yp(*)    ! x,y data (input).
      real*4 cent,ow,bg     ! centroid, object (half) width, background (input).
      real*4 peak           ! peak of profile (output).
C
      real*4 xinc,area
      integer*4 ii
C
      peak=-1.e+30
      area=0.
      do ii=2,n
        if (abs(xp(ii)-cent).lt.ow) then
          if (ii.eq.1) then
            xinc = xp(ii+1) - xp(ii)
          else
            xinc = xp(ii) - xp(ii-1)
          endif
          if ((yp(ii)-bg).gt.peak) peak = yp(ii) - bg
          area = area + ((yp(ii) - bg)* xinc)
        endif
      enddo
      EE_FOW_fwhm = ( area / peak ) * 0.939437
C Output real peak.
      peak = peak + bg
      return
      end

C----------------------------------------------------------------------
C The object profile is in xp and yp.  
C
      subroutine EE_FindObjectWidth(n,xp,yp,ow,cent,fwhm,bg,
     .                              peak,mdnbg,rms,eon)
C
      implicit none
      integer*4 n          ! number of points in profile (input).
      real*4 xp(n),yp(n)   ! object profile (input).
      real*4 ow,cent       ! object (half) width and centroid (output).
      real*4 fwhm          ! FWHM (output)
      real*4 bg            ! background level (output).
      real*4 peak          ! peak of profile (output).
      real*4 mdnbg         ! median of whole profile (output).
      real*4 rms           ! RMS of background (outside object) (output).
      integer*4 eon        ! Echelle Order Number (input)
C
      include 'makee.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
      real*4 sum,tsum,objfrac,owlim,minow
      real*4 EE_FOW_bg,EE_FOW_cent,EE_FOW_fwhm
      integer*4 iter,ii

C Find median of whole profile.
      do ii=1,n
        arr(ii) = yp(ii)
      enddo
      call find_median(arr,n,mdnbg)

C Set object fraction.
      objfrac = EEFO_ObjectFraction
      
C Initial guess at object width and centroid.
      cent = (xp(1)+xp(n))/2.
      ow   = 0.3*abs(xp(1)-xp(n))
C Minimum Object Width.
      minow= 0.1*abs(xp(1)-xp(n))

C New background level, allow space between object edge & start of background.
      bg = EE_FOW_bg( n, xp, yp, arr, cent, ow, rms )
C New centroid.
      cent = EE_FOW_cent( n, xp, yp, cent, ow, bg )
C New centroid (again).
      cent = EE_FOW_cent( n, xp, yp, cent, ow, bg )
C New background (again).
      bg = EE_FOW_bg( n, xp, yp, arr, cent, ow, rms )

C ..............Three Iterations............
      DO iter = 1,3 
C FWHM guess.
        fwhm = EE_FOW_fwhm( n, xp, yp, cent, ow, bg, peak)
C New object width.
        ow = max(minow,fwhm*1.5)
C New centroid (again).
        cent = EE_FOW_cent( n, xp, yp, cent, ow, bg )
C New centroid (again).
        cent = EE_FOW_cent( n, xp, yp, cent, ow, bg )
C New background (again).
        bg = EE_FOW_bg( n, xp, yp, arr, cent, ow, rms )
      ENDDO

C Now find width which encloses "objfrac" fraction of the flux.
C Establish limit to width by allowing at least 1 pixel on each side for sky.
C NEW: Cannot be larger than 2 times "fwhm" object width (tab -02jun00).
      owlim = min( (cent-(xp(1)+1.)) , ((xp(n)-1.)-cent) )
      owlim = max((1.),min((2.*ow),(owlim)))

C Find total sum (to the object limits), not including 1 pixel on each side.
      call EE_IntegProf(n,xp,yp,bg,(cent-owlim),(cent+owlim),tsum)

C Find fractional sum within +/- ow .  Minimum (half) width of 1.01 pixels.
      sum = tsum
      ow  = owlim
      do while(((sum/tsum).gt.objfrac).and.(ow.gt.1.))
        ow = ow - 0.01
        call EE_IntegProf(n,xp,yp,bg,(cent-ow),(cent+ow),sum)
      enddo
C
      return
      end

C----------------------------------------------------------------------
C Integrate profile between r1 and r2.  We account for fractional pixels.
C Note the bounds are in "real" pixel units, such that if the bounds are
C between 3.0 and 5.0, the sum contains all of pixel 4, but only half of pixel
C 3 and half of pixel 5.
C Added "max(0.,(yp(i)-bg))" -tab 02jun00.
C 
      subroutine EE_IntegProf(n,xp,yp,bg,r1,r2,sum)
C
      implicit none
      integer*4 n,i
      real*4 xp(n),yp(n),bg,r1,r2,sum,hpw
      hpw = 0.5*(xp(n)-xp(1))/float(n-1)
      sum=0.
      do i=1,n
C Fractional pixel?
        if (abs(xp(i)-r1).lt.hpw) then
          sum = sum + max(0.,(yp(i)-bg)) * ((xp(i)+hpw)-r1)
        elseif (abs(xp(i)-r2).lt.hpw) then
          sum = sum + max(0.,(yp(i)-bg)) * (r2-(xp(i)-hpw))
C Pixel completely contained within the bounds?
        elseif ((xp(i).gt.r1).and.(xp(i).lt.r2)) then
          sum = sum + max(0.,(yp(i)-bg))
        endif
      enddo
      return
      end


C----------------------------------------------------------------------
C Calculate profile.  We use binning and median values.
C Start with SubSamp sampling over pixels, then interpolate between pixels
C to give a 0.1 pixel binning, then do a S/N dependent boxcar smoothing.
C
      subroutine EE_MakeProfile(sc,ec,sr,er,obj,eon)
C
      implicit none
      integer*4 sc,ec,sr,er,eon
      real*4 obj(sc:ec,sr:er)
C
      include 'makee.inc'
      include 'global.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
      integer*4 i,j,i1,i2,col,row,nla,nn,size,narr
      integer*4 maxla,ptr
      parameter(maxla=600100)
      real*4 px(maxla),py(maxla)
      real*4 trow,uprs,lors,xx,xlast,fr,sn,EE_getvar,x1,x2
C
C SubSamp/SampInc should be a whole number.
      real*4 SubSamp,SampInc
      parameter(SubSamp=0.3,SampInc=0.1)

C Estimate size needed for large array.
      if (((1+ec-sc)*(nint(Global_sw)+2)).gt.maxla) then
        write(soun,'(a)')
     .  'ERROR: EE_MP: Slitlength too big for arrays:'
        write(soun,'(a,i5,a,i5,a,f9.3)') 
     .  'ERROR: EE_MP: sc=',sc,'  ec=',ec,'  Global_sw=',Global_sw
        return
      endif
C First load the large array
      nla=0
      do col=sc,ec
        call EE_BackgroundLimits(eon,dfloat(col),trow,uprs,lors)
        do row=min(er,max(sr,nint(trow+uprs))),
     .         min(er,max(sr,nint(trow+lors)))
          if (obj(col,row).gt.-1.e+5) then
            nla=nla+1
            px(nla)=float(row)-trow
            py(nla)=obj(col,row)
          endif
        enddo
      enddo

C Number of bins in initial profile.
      nn = nint((lors-uprs)/SubSamp)+1
C Clear temporary array.
      do i=1,nn
        b(i)=0.
      enddo
C Median each sub-sampled bin.
C NOTE:   This is a slow loop (for wide slits like ESI) since a median
C NOTE:   is done for each sub-sample (every 0.3 pixels).
      do i=1,nn
        xx = uprs+(float(i-1)*SubSamp)
        narr=0
        do j=1,nla
          if (abs(px(j)-xx).lt.(SubSamp/2.)) then
            narr=narr+1
            arr(narr) = py(j)
          endif
        enddo
        if (narr.gt.0) call find_median(arr,narr,b(i))
      enddo
C Just in case, replace zero values with nearest non-zero value.
      do i=1,nn
        if (abs(b(i)).lt.1.e-10) then
          j=i
          if (i.lt.nn/2) then
            do while((abs(b(j)).lt.1.e-10).and.(j.lt.nn))
              j=j+1
            enddo
          else
            do while((abs(b(j)).lt.1.e-10).and.(j.gt.1))
              j=j-1
            enddo
          endif
          b(i)=b(j)
        endif
      enddo
C Convert back to (approximate) real digital numbers.
      do i=1,nn
        b(i)=b(i)*fltmn(eon)
      enddo

C Interpolate between bins.  Increment becomes SampInc.
C Profile moves to arr(..) array.
      xlast = uprs+((nn-1)*SubSamp) + 0.0001
      ptr= 1
      xx = uprs+((ptr-1)*SampInc)
      do while (xx.lt.xlast)
        i1 = int(((xx-uprs)/SubSamp)+0.0001) + 1
        i1 = max(1,min(nn,i1))
        i2 = min(nn,i1+1)
        x1 = uprs+((i1-1)*SubSamp)
        x2 = uprs+((i2-1)*SubSamp)
        fr = max(0.,min(1.,(xx-x1)/SubSamp))
        arr(ptr) = (b(i1)*(1.-fr)) + (b(i2)*fr)
        ptr= ptr + 1
        xx = uprs+((ptr-1)*SampInc)
      enddo
      nn = ptr - 1

C Determine S/N of profile and decide on boxcar size.
      xx=0.
      do i=1,nn
        if (arr(i).gt.xx) xx=arr(i)
      enddo
      sn = xx/sqrt(EE_getvar(xx,1.))
      if (sn.lt.1.5) then
        size=27
      elseif (sn.lt.4.5) then
        size=15
      elseif (sn.lt.8.5) then
        size=9
      else
        size=5
      endif

C Increase size for ESI.
      if (Global_ESI) size = size*2.3

C Profile moves back to b(..) array.
C Smooth array.
      call EE_Boxcar(nn,arr,b,size)

C Load up final profile.
      pparm(1,eon) = nn
      pparm(2,eon) = uprs
      pparm(3,eon) = 0.1
      do i=1,nn
        pval(i,eon)= b(i)
      enddo
C
      return
      end


C----------------------------------------------------------------------
C Find the row shifts for the background limits relative to the echelle
C bright star traces "eo(0:99,99)".  This uses bk1o(0:99,99) and bk2o(0:99,99).
C "eon"  is the echelle order number.
C "cc"   is the column where the shift is evaluated, the center column would
C        be normally used for this ( cc = dfloat(sc+ec)/2.d0 ).
C "trow" is the trace row center.
C "uprs" is the shift to the upper background limit (negative).
C "lors" is the shift to the lower background limit (positive).
C
      subroutine EE_BackgroundLimits(eon,cc,trow,uprs,lors)
      implicit none
      integer*4 eon
      real*8 cc,polyarroff,r8
      real*4 trow,uprs,lors
      include 'makee.inc'
      r8   = polyarroff(eo(0,eon),cc)
      uprs = sngl( polyarroff(bk1o(0,eon),cc) - r8 )
      lors = sngl( polyarroff(bk2o(0,eon),cc) - r8 )
      trow = sngl( r8 )
      return
      end

C----------------------------------------------------------------------
C Set window size for a PG plot.
      subroutine EE_SetPlotBox(n,x,y)
      implicit none
      integer*4 n,i
      real*4 x(n),y(n)
      real*4 xmin,xmax,ymin,ymax
      xmin=x(1)
      xmax=x(1)
      ymin=y(1)
      ymax=y(1)
      do i=1,n
        if (x(i).lt.xmin) xmin=x(i)
        if (x(i).gt.xmax) xmax=x(i)
        if (y(i).lt.ymin) ymin=y(i)
        if (y(i).gt.ymax) ymax=y(i)
      enddo
      xmin = xmin - (xmax-xmin)*0.01
      xmax = xmax + (xmax-xmin)*0.01
      ymin = ymin - (ymax-ymin)*0.01
      ymax = ymax + (ymax-ymin)*0.01
C If ymin is close to zero, make it zero.
      if ((ymin.gt.0.).and.((ymin/(ymax-ymin)).lt.0.1)) ymin=0.
      if (abs(ymax).lt.1.e-10) ymax=1.e-10
C Avoid overflow problems.
      if (abs(xmin-xmax).lt.1.e-10) then
        xmin = xmin - 1.e-10
        xmax = xmax + 1.e-10
      endif
      if (abs(ymin-ymax).lt.0.0001) then
        ymin = ymin - 0.0001
        ymax = ymax + 0.0001
      endif
C Check.
      if ( (xmin.gt.1.e+30).or.(xmin.lt.-1.e+30).or.
     .     (xmax.gt.1.e+30).or.(xmax.lt.-1.e+30).or.
     .     (ymin.gt.1.e+30).or.(ymin.lt.-1.e+30).or.
     .     (ymax.gt.1.e+30).or.(ymax.lt.-1.e+30) ) then
        print *,'ERROR: EE_SetPlotBox: ranges out of range.' 
      endif   
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBOX('BCNST',0.,0,'BCNST',0.,0)
      return
      end

C----------------------------------------------------------------------
C Find minimum, maximum, mean, and rms in an array.  Uses medians.
C
      subroutine EE_ArrStat(n,x,xmin,xmax,xmean,xrms,arr)
C
      implicit none
      integer*4 n,i
      real*4 x(n),xmin,xmax,xmean,xrms,arr(*)
      xmin=x(1)
      xmax=x(1)
      do i=1,n
        if (x(i).gt.xmax) xmax=x(i)
        if (x(i).lt.xmin) xmin=x(i)
        arr(i) = x(i)
      enddo
      call find_median(arr,n,xmean)
      do i=1,n
        arr(i) = ((x(i)-xmean)*(x(i)-xmean))
      enddo
      call find_median(arr,n,xrms)
      xrms = sqrt(xrms)
      return
      end

C----------------------------------------------------------------------
C Find background limits by probing the separated orders in the image.
C We use a 70% drop-off.
C We use the parameter "EEFB_Compensate" and "EEFB_OverlapCompensate" to make
C the background slightly smaller.
C
      subroutine EE_FindBack(sc,ec,sr,er,flat)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 flat(sc:ec,sr:er)
C
      include 'makee.inc'
      include 'global.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
      real*4 rrs,back,sum,top,r,split,rc,r1,r2,minsplit
      real*8 cc,polyval,polyarroff,comp,olcomp,r8
      integer*4 n,i,eon,skip,num,ord
      real*4 lowoff(mo),uppoff(mo)
C
C Initialize.
      top=0.

C Set compensation numbers.
      comp  = dble(EEFB_Compensate)
      olcomp= dble(EEFB_OverlapCompensate)
C
C Center column.
      cc = dfloat(sc+ec)/2.d0
C
C For speed, adjust how many pixels are summed along a trace.
      skip = 10
C
C First find background level between widely separated orders.
C "back" is actually a sum along a trace skipping every other pixel.
C NEW Note: "back" is now an average since "num" can vary... -tab may00
      minsplit = 1.5*Global_sw
7     continue
      n=0
      do eon=1,Global_neo-1
        call EE_OrderSplit(cc,eon,split)
        if (split.gt.minsplit) then
          rrs = split/2.
          call EE_OrderSum(sc,ec,sr,er,flat,eon,rrs,back,num,skip)
          n=n+1
          arr(n) = back/float(num)
        endif
      enddo
      if (n.lt.3) then
        if (minsplit.gt.1.11*Global_sw) then
          minsplit = 1.1*Global_sw
          goto 7
        endif
        write(soun,'(a)') 
     . 'ERROR: EE_FindBack: Orders too close together.'
        call exit(1)
      endif
      call find_median(arr,n,back)
      if (Global_ESI) back = arr(1)   ! For ESI, take lowest value.
      write(soun,'(a,1pe12.5)') 
     . 'NOTE: Background between widely separated orders: ',back
C
C Now determine background limits for individual orders, taking into
C account possible overlap.
C Find exact offsets for width of slits.
C Find offsets for well separated orders.
C Fit polynomial to offsets to extrapolate/interpolate to all orders.
C
      minsplit=Global_sw*1.2
      if (Global_ESI) minsplit = Global_sw*1.1
8     continue
C Zero arrays.
      do i=1,Global_neo
        lowoff(i)=0.
        uppoff(i)=0.
      enddo
C Look for 70% drop off points.
      n=0
      DO eon=1,Global_neo
        call EE_OrderSplit(cc,eon,split)
        if (split.gt.minsplit) then
C Find lower offset.
          rrs=0.
          do while(rrs.lt.(Global_sw*0.75))
            call EE_OrderSum(sc,ec,sr,er,flat,eon,rrs,r,num,skip)
            sum = (r / float(num)) - back
            if (rrs.lt.0.1) top=sum
            if (sum.lt.(top*0.7)) then
              lowoff(eon) = rrs
              rrs = Global_sw     ! to stop loop
            endif
            rrs=rrs+0.1
          enddo
C Find upper offset.
          rrs=0.
          do while(abs(rrs).lt.(Global_sw*0.75))
            call EE_OrderSum(sc,ec,sr,er,flat,eon,rrs,r,num,skip)
            sum = (r / float(num)) - back
            if (abs(rrs).lt.0.1) top=sum
            if (sum.lt.(top*0.7)) then
              uppoff(eon) = rrs
              rrs = -1.*Global_sw     ! to stop loop
            endif
            rrs=rrs-0.1
          enddo
          n=n+1
        endif
        write(soun,'(a,i3,3(a,f6.1))') 'NOTE: EEFB: EON=',eon,
     .  '  Slit Edge Lower=',lowoff(eon),'   Upper=',uppoff(eon),
     .  '   Width=',lowoff(eon)-uppoff(eon)
      ENDDO
C
C Give user warning.
      if ((n.lt.5).or.(n.lt.(Global_neo/3))) then
        if (minsplit.gt.Global_sw*1.1) then
          minsplit = Global_sw*1.08
          goto 8
        endif
        write(soun,'(a,i6)')
     .  'WARNING:EE_FindBack: too few well separ. orders:',n
      endif
C
C Fit Nth order offset polynomials and fill in uppoff and lowoff arrays.
      write(soun,'(a)') 'EEFB: Fitting background lower offsets.'
      ord=2
      n=0
      do i=1,Global_neo
        if (abs(lowoff(i)).gt.1.e-30) then
          n=n+1
          x8(n)=dfloat(i)
          y8(n)=dble(lowoff(i))
          w8(n)=1.d0
        endif
      enddo
C If too few points, decrease order.
      if (float(n)/float(Global_neo).lt.0.35) ord=ord-1
      call EEFB_Fit_and_Reject(n,ord)
      do i=1,Global_neo
        lowoff(i) = sngl(polyval(ord+1,coef,dfloat(i)))
      enddo
C
      write(soun,'(a)') 'EEFB: Fitting background upper offsets.'
      ord=2
      n=0
      do i=1,Global_neo
        if (abs(uppoff(i)).gt.1.e-30) then
          n=n+1
          x8(n)=dfloat(i)
          y8(n)=dble(uppoff(i))
          w8(n)=1.d0
        endif
      enddo
C If two few points, decrease order.
      if (float(n)/float(Global_neo).lt.0.35) ord=ord-1
      call EEFB_Fit_and_Reject(n,ord)
      do i=1,Global_neo
        uppoff(i) = sngl(polyval(ord+1,coef,dfloat(i)))
      enddo
C
C Create bk1o and bk2o, we will offset these below.
      do eon=1,Global_neo

C Load coeffecients for outer background (inner is set in EE_FindObject() ).
        do i=0,mpp
          bk1o(i,eon) = eo(i,eon)   ! outer
          bk2o(i,eon) = eo(i,eon)
        enddo

C Find bk1o. See if order above is overlapping. Extrapolate for first order.
C Add/subtract compensation (realistic background limits).
        call EE_OrderSplit(cc,eon,split)
        rc = sngl(polyarroff(eo(0,eon),cc))
        r1 = rc + uppoff(eon)
        if (eon.gt.1) then
          r2 = sngl(polyarroff(eo(0,eon-1),cc)) + lowoff(eon-1)
        else
          r2 = rc + lowoff(eon) - split
        endif
        if (r1.gt.r2) then
C Orders do not overlap.
          bk1o(2,eon) = bk1o(2,eon) + dble(r1-rc) + comp
        else
C Orders do overlap, use different compensation.
          bk1o(2,eon) = bk1o(2,eon) + dble(r2-rc) + olcomp
        endif

C Find bk2o. See if order below is overlapping. Extrapolate for last order.
        r1 = rc + lowoff(eon)
        if (eon.lt.Global_neo) then
          r2 = sngl(polyarroff(eo(0,eon+1),cc)) + uppoff(eon+1)
        else
          r2 = rc + uppoff(eon) + split
        endif
        if (r1.lt.r2) then
C Orders do not overlap.
          bk2o(2,eon) = bk2o(2,eon) + dble(r1-rc) - comp
        else
C Orders do overlap, use different compensation.
          bk2o(2,eon) = bk2o(2,eon) + dble(r2-rc) - olcomp
        endif

      enddo

C If the order is very wide, such as with ESI, squeeze in the background ranges.
C NEW... tab -May00
      if (Global_sw.gt.50.) then
        do eon=1,Global_neo
          r8 = ABS( polyarroff(bk1o(0,eon),(cc)) 
     .            - polyarroff(bk2o(0,eon),(cc)) )
          bk1o(2,eon) = bk1o(2,eon) + (r8 * 0.08)
          bk2o(2,eon) = bk2o(2,eon) - (r8 * 0.08)
        enddo
      endif
C
      return
      end


C----------------------------------------------------------------------
C Sum along an order.  a is the image, eon is echelle order number, rrs is the
C relative row shift, sum is the result, num is the number of pixels summed.
C Ignore masked pixels.
      subroutine EE_OrderSum(sc,ec,sr,er,a,eon,rrs,sum,num,inc)
      implicit none
      integer*4 sc,ec,sr,er,eon,row,col,inc,num
      real*4 a(sc:ec,sr:er),sum,rrs
      real*8 polyarroff
      include 'makee.inc'
      sum=0.
      num=0
      do col=sc,ec,inc
        row = nint( polyarroff(eo(0,eon),dfloat(col)) + dble(rrs) )
        if ((row.ge.sr).and.(row.le.er)) then
          if (a(col,row).gt.-1.e+5) then
            sum = sum + a(col,row)
            num = num + 1
          endif
        endif
      enddo
      return
      end

C----------------------------------------------------------------------
C Find the order separation near a given order, eon.  "cc" is center column.
C
      subroutine EE_OrderSplit(cc,eon,split)
C
      implicit none
      real*8 cc,polyarroff
      integer*4 eon,num
      real*4 cr,split
C
      include 'makee.inc'
      include 'global.inc'
      include 'soun.inc'
C
      cr = sngl(polyarroff(eo(0,eon),cc))
      split = 0.
      num = 0
      if (eon.gt.1) then
        split= split + abs( cr - sngl(polyarroff(eo(0,eon-1),cc)) )
        num = num + 1
      endif
      if (eon.lt.Global_neo) then
        split= split + abs( cr - sngl(polyarroff(eo(0,eon+1),cc)) )
        num = num + 1
      endif
      if (num.gt.0) then
        split = split / float(num)
      else
        write(soun,'(a)') 'ERROR in EE_OrderSplit.'
        split = 10.
      endif
      return
      end



C------------------------------------------------------------------------
C Find and fit echelle orders given a standard star image and and estimate
C for the slit width in pixels.
C Uses slit width (Global_sw), finds number of orders (Global_neo), 
C and echelle order polynomials (eo).

      subroutine EE_FindOrders_HIRES(sc,ec,sr,er,star)

      implicit none
      integer*4 sc,ec,sr,er     ! image dimensions (input)
      real*4 star(sc:ec,sr:er)  ! bright star data (input)
C
      include 'makee.inc'
      include 'scratch.inc'
      include 'global.inc'
      include 'soun.inc'
C
      real*8 eoleft(0:99,99),eoright(0:99,99)
      real*4 r
      integer*4 cc,row,col,narr,neoleft,neoright,k,kk,lc
      integer*4 Right_cc,Left_cc,wide
      character c40*40
      logical first

C For first time EE_TraceOrders_HIRES is called.
      first=.true.

C Right and left positions.
      Right_cc = (sc+ec)/2 + (ec-sc)/4
      Left_cc  = (sc+ec)/2 - (ec-sc)/4
      wide = 20

C Find orders down the left half and the right half of image.
C::::::::::::::::::::: RIGHT ::::::::::::::::::::::::
C Do a median mash on columns on the right half of image.
      cc = Right_cc
      do row=sr,er
        narr=0
        do col=cc-wide,cc+wide
          narr=narr+1
          arr(narr) = star(col,row)
        enddo
        call find_median(arr,narr,z(row))
      enddo
C Estimate background and subtract off.
      narr=0
      do row=sr,er
        narr=narr+1
        arr(narr)=z(row)
      enddo
      call find_median(arr,narr,r)
      do row=sr,er
        z(row) = z(row) - r
      enddo

C Find Peaks. Sets zpk().
      call EE_FindPeaks(sr,er,neoright)

C Open PGPLOT for trace fits which will be plotted in EE_TraceOrders_HIRES.
      kk=max(-1,min(999,Star_Obsnum))
      write(c40,'(a,i3.3,a)') 'trace-',kk,'.ps'
      write(soun,'(2a)') 'Writing PostScript plot file: ',c40(1:lc(c40))
      c40 = c40(1:lc(c40))//'/PS'
      call EE_OpGrid(neoright+1,k,kk)
      call PGBEGIN(0,c40,+1*k,+1*kk)   ! #### temporary?
      call PGASK(.false.)
C Now trace orders and fit polynomials.
C At each order, track the flux to the right and left and then fit a
C polynomial to the order.
      call EE_TraceOrders_HIRES(sc,ec,sr,er,star,cc,
     .                          neoright,eoright,first)
C::::::::::::::::::::::::::::::::::::::::::::::::::::
C:::::::::::::::::::::: LEFT ::::::::::::::::::::::::
C Do a median mash on columns on the left half of image.
      cc = Left_cc
      do row=sr,er
        narr=0
        do col=cc-wide,cc+wide
          narr=narr+1
          arr(narr) = star(col,row)
        enddo
        call find_median(arr,narr,z(row))
      enddo
C Estimate background and subtract off.
      narr=0
      do row=sr,er
        narr=narr+1
        arr(narr)=z(row)
      enddo
      call find_median(arr,narr,r)
      do row=sr,er
        z(row) = z(row) - r
      enddo
C Find Peaks. Sets zpk().
      call EE_FindPeaks(sr,er,neoleft)
C Now trace orders and fit polynomials.
C At each order, track the flux to the right and left and then fit a
C polynomial to the order.
      call EE_TraceOrders_HIRES(sc,ec,sr,er,star,cc,
     .                          neoleft,eoleft,first)
C Close plot file.
      call PGEND
C::::::::::::::::::::::::::::::::::::::::::::::::::::

C Load neo and eo(,). Find orders which are not common to both.
      call EE_LoadOrders(((sc+ec)/2),neoleft,neoright,eoleft,eoright)

      return
      end


C----------------------------------------------------------------------
C Load the array eo(*,*) by taking the union of eoleft and eoright.
C The array eo(*,*) and neo are in kee.inc .

      subroutine EE_LoadOrders(ccol,neoleft,neoright,eoleft,eoright)

      implicit none
      integer*4 ccol,neoleft,neoright
      real*8 eoleft(0:99,99),eoright(0:99,99)
C
      include 'makee.inc'
      include 'global.inc'
      include 'soun.inc'
C
      integer*4 eolc(99),eorc(99)
      integer*4 i,shift,j
      real*8 xc,polyarroff
      
C Clear arrays.
      do i=1,99
        eolc(i) = -1
        eorc(i) = -1
      enddo
C Skip rest if no left orders.
      if (neoleft.le.0) goto 222

C Special case.  Big difference between what is found on left and right.
      if (abs(neoleft-neoright).gt.10) then 
        write(soun,'(2a)') 'NOTE: Big difference in number',
     .            ' of orders found on left and right sides.'
        write(soun,'(a,i4,a,i4)') 
     .  'NOTE: neoleft=',neoleft,'   neoright=',neoright
        if (neoleft.lt.neoright) then
          write(soun,'(a)') 'NOTE: Use left side only.'
          Global_neo = neoleft          ! Load eo(*,*).
          do i=1,Global_neo
            do j=0,mpp
              eo(j,i) = eoleft(j,i)
            enddo
          enddo
        else
          write(soun,'(a)') 'NOTE: Use right side only.'
          Global_neo = neoright          ! Load eo(*,*).
          do i=1,Global_neo
            do j=0,mpp
              eo(j,i) = eoright(j,i)
            enddo
          enddo
        endif
        return      ! exit this routine here
      endif

C Load left centers and right centers.
      xc = dfloat(ccol)
      do i=1,neoleft
        eolc(i) = nint(polyarroff(eoleft(0,i),xc))
      enddo
      do i=1,neoright
        eorc(i) = nint(polyarroff(eoright(0,i),xc))
      enddo
C Find shift. Allow a maximum shift of two.
      shift = 9
      if (abs(eolc(1)-eorc(1)).lt.2) shift = 0
      if (abs(eolc(2)-eorc(1)).lt.2) shift = 1
      if (abs(eolc(3)-eorc(1)).lt.2) shift = 2
      if (abs(eolc(1)-eorc(2)).lt.2) shift = -1
      if (abs(eolc(1)-eorc(3)).lt.2) shift = -2
      if (abs(shift).gt.2) then
        write(soun,'(a,i9)') 
     . 'ERROR: EE_LoadOrders: this should not happen: shift=',shift
        call exit(1)
      endif
C Apply shift.
      if (shift.gt.0) then
        do i=(neoright+shift),(1+shift),-1
          do j=0,mpp
            eoright(j,i) = eoright(j,i-shift)
          enddo
        enddo
        do i=1,shift
          do j=0,mpp
            eoright(j,i) = -1
          enddo
        enddo
        neoright = neoright + shift
      elseif (shift.lt.0) then
        do i=(neoleft+abs(shift)),(1+abs(shift)),-1
          do j=0,mpp
            eoleft(j,i) = eoleft(j,i-abs(shift))
          enddo
        enddo
        do i=1,abs(shift)
          do j=0,mpp
            eoleft(j,i) = -1
          enddo
        enddo
        neoleft = neoleft + abs(shift)
      endif
C Re-Load left centers and right centers.
      if (shift.ne.0) then
        xc = dfloat(ccol)
        do i=1,neoleft
          eolc(i) = nint(polyarroff(eoleft(0,i),xc))
        enddo
        do i=1,neoright
          eorc(i) = nint(polyarroff(eoright(0,i),xc))
        enddo
      endif
C Check alignment.
      do i=1,max(neoleft,neoright)
        if ((eolc(i).gt.0).and.(eorc(i).gt.0)) then
          if ( (abs(eolc(i)-eorc(i)).gt.2) .or.
     .         ((eolc(i).lt.0).and.(eorc(i).lt.0)) )  then
            write(soun,'(a)')
     .        'ERROR: EE_LoadOrders: Misalignment of order tracings.'
          endif
        endif
      enddo
C Load eo(*,*).
222   continue
      Global_neo = max(neoleft,neoright)
      do i=1,Global_neo
        if (eolc(i).gt.0) then
          do j=0,mpp
            eo(j,i) = eoleft(j,i)
          enddo
        else
          do j=0,mpp
            eo(j,i) = eoright(j,i)
          enddo
        endif
      enddo
      return
      end



      
C------------------------------------------------------------------------
C Find and fit echelle orders given a standard star image and and estimate
C for the slit width in pixels.
C Uses slit width (Global_sw), finds number of orders (Global_neo), 
C and echelle order polynomials (eo).

      subroutine EE_FindOrders_ESI(sc,ec,sr,er,star)

      implicit none
      integer*4 sc,ec,sr,er     ! image dimensions (input)
      real*4 star(sc:ec,sr:er)  ! bright star data (input)
C
      include 'makee.inc'
      include 'scratch.inc'
      include 'global.inc'
      include 'soun.inc'
C
      real*8 eocenter(0:99,99)
      real*4 r
      integer*4 cc,row,col,narr,neocenter,k,kk,lc
      integer*4 Center_cc,wide,ii,jj
      character c40*40
      logical first

C For first time EE_TraceOrders_ESI is called.
      first=.true.

C Center position.
      Center_cc = (sc+ec)/2 + 1
      wide = 20

C Do a median mash on columns in the center of image.
      cc = Center_cc
      do row=sr,er
        narr=0
        do col=cc-wide,cc+wide
          narr=narr+1
          arr(narr) = star(col,row)
        enddo
        call find_median(arr,narr,z(row))
      enddo

C Estimate background and subtract off.
      narr=0
      do row=sr,er
        narr=narr+1
        arr(narr)=z(row)
      enddo
      call find_median(arr,narr,r)
      k = nint( float(narr) * 0.2 )
      k = min(narr,max(1,k))
      r = arr(k)
      do row=sr,er
        z(row) = z(row) - r
      enddo

C Find Peaks. Sets zpk().
      call EE_FindPeaks(sr,er,neocenter)

C Open PGPLOT for trace fits which will be plotted in EE_TraceOrders_ESI.
      kk=max(-1,min(999,Star_Obsnum))
      write(c40,'(a,i3.3,a)') 'trace-',kk,'.ps'
      write(soun,'(2a)') 'Writing PostScript plot file: ',c40(1:lc(c40))
      c40 = c40(1:lc(c40))//'/PS'
      call EE_OpGrid(neocenter+1,k,kk)
      call PGBEGIN(0,c40,+1*k,+1*kk)   ! #### temporary?
      call PGASK(.false.)

C Now trace orders and fit polynomials.
C At each order, track the flux to the right and left and then fit a
C polynomial to the order.
      call EE_TraceOrders_ESI(sc,ec,sr,er,star,cc,
     .                        neocenter,eocenter,first)

C Close plot file.
      call PGEND

C Load eo(*,*).
      Global_neo = neocenter
      do ii=1,Global_neo
        do jj=0,mpp
          eo(jj,ii) = eocenter(jj,ii)
        enddo
      enddo
C
      return
      end


C----------------------------------------------------------------------
C Using the order positions down a given column, determine all the
C order defining polynomials.  Note that it is assumed that the order tilt
C is less than 10.0 degrees (tan(10.0)=0.176).
C
      subroutine EE_TraceOrders_ESI(sc,ec,sr,er,star,cc,neotemp,
     .                              eotemp,first)

      implicit none
      integer*4 sc,ec,sr,er         ! image dimensions
      real*4 star(sc:ec,sr:er)      ! star image
      integer*4 cc                  ! center column for peaks (starting point)
      integer*4 neotemp             ! number of orders or peaks
      real*8 eotemp(0:99,99)        ! echelle order polynomials
      logical first                 ! flag for first time routine is called.
C
      include 'makee.inc'
      include 'global.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
      character c44*44
      integer*4 eon,npt,col,i,k,ii,nn,highpt,maxrej,nrej,dir
      integer*4 ncrow,lo_col,hi_col,ttorder
      real*4 crow(90)
      real*4 cent,gucent
      real*8 polyarroff,crat,polyvaloff,ttyoff
      real*8 wrms,wppd,high,r8,xoff,HighLimit,ttcoef(99),ttxoff,col8
      real*8 sum,wsum,xfac
      logical ok,plotit
C
      common /EETRACEORDERSBLK/ ncrow,crow
C
C X binning factor... An ESI 1x1 image has 4095 columns after processing...
      xfac = 1.d0
      ii = 1 + ec - sc
      if (ii.lt.3000) xfac = 2.d0

C High point reject stops when the high pixel residual is less than
C   the weighted RMS times "HighLimit". (was 9.0)
      HighLimit = 7.0

C Initialize on first call.
      if (first) then
        ncrow=0
        first=.false.
      endif

      DO eon=1,neotemp

C Find template ESI trace polynomial fits.
      call Load_ESI_Polynomial(eon,ttorder,ttcoef,ttxoff)

C Fill arrays.
      npt=0
      lo_col = +999000
      hi_col = -999000

C Go left and then right, stop if we hit the top or bottom of chip.
      do dir=-1,+1,2

C Initialize.
        col = cc

C Guess at centroid.
        gucent = zpk(eon)

C Loop until we hit edge.
        do while((col.gt.sc).and.(col.lt.ec))

C Find lowest and highest column considered in this trace fit.
          if (col.lt.lo_col) lo_col=col
          if (col.gt.hi_col) hi_col=col

C Calculate centroid.
          call EE_ImageCentroid(sc,ec,sr,er,star,col,gucent,0.15,
     .                          cent,ok)

C Include point if centroid is close enough to the guess or we do not yet
C have enough points.  Otherwise use template trace.
          npt=npt+1
          x8(npt)= dfloat(col)
          if ((ok).and.
     .       ((abs(cent-gucent).lt.4.0).or.(npt.lt.20))) then
            y8(npt)= dble(cent)
            w8(npt)= 1.d0
          else
            y8(npt)= dble(gucent)
            w8(npt)= 0.1d0
          endif

C Setup for guess at next point using template traces.
          ii = npt
          sum = 0.
          wsum= 0.
          do while((ii.gt.0).and.(abs(x8(ii)-x8(npt)).lt.40.))
            sum = sum + ( y8(ii) - 
     .         polyvaloff(ttorder+1,ttcoef,(x8(ii)*xfac),ttxoff) )
            wsum= wsum + 1.
            ii = ii - 1
          enddo
          ttyoff= sum / wsum

C Increment column towards the left (dir=-1) or right (dir=+1).
          col = col + dir
          col8 = dfloat(col)
          gucent = 
     .      polyvaloff(ttorder+1,ttcoef,(col8*xfac),ttxoff) + ttyoff

C Stop if we near top or bottom of image.
          if ( (abs(gucent-float(sr)).lt.(Global_sw/8)).or.
     .         (abs(gucent-float(er)).lt.(Global_sw/8)) ) col=sc

        enddo
      enddo

C Check number of points.
      if (npt.lt.20) then
        write(soun,'(a,i9)')
     .        'ERROR: EETO:1: Too few points found in trace:',npt
        call exit(1)
      endif

C Fit kth order polynomial.
      k = max(1,EETO_PolyOrder)
      nrej=0
      maxrej=npt/8
7     continue
      call poly_fit_gj(npt,x8,y8,w8,k,coef,xoff,0,ok)
      if (.not.ok) then
        write(soun,'(a)') 'ERROR: EE_TraceOrders: Fitting polynomial.'
        return
      endif

C Find WRMS.
      call poly_fit_gj_residuals(npt,x8,y8,w8,k,coef,xoff,
     .                           high,wrms,wppd)

C Find element number of highest residual.
      high=0.d0
      highpt=1
      do i=1,npt
        if (w8(i).gt.0.) then
          r8 = abs(y8(i) - polyvaloff(k+1,coef,x8(i),xoff))
          if (r8.gt.high) then
            high  = r8
            highpt= i
          endif
        endif
      enddo

C Go back?
      if ((w8(highpt).gt.0.).and.(high.gt.(HighLimit*wrms))
     .                      .and.(nrej.lt.maxrej)) then
        nrej=nrej+1
        w8(highpt)=0.d0
        goto 7
      endif

C Warn about maximum rejections.
      if (nrej.ge.maxrej) then
        write(soun,'(a)')
     .  'WARNING: EETO: Exceeded maximum # of rejected pixels.'
      endif

C Load coeffecients into eotemp.
      eotemp(0,eon)=xoff
      eotemp(1,eon)=dfloat(EETO_PolyOrder+1)
      do ii=1,nint(eotemp(1,eon))
        eotemp(ii+1,eon) = coef(ii-1)
      enddo

C Report.
      write(soun,'(a,i3,a,f7.3,a,f7.3,a,f7.3,a,i4)') 
     . 'NOTE: EETO: EON=',eon,'  High=',high,'  WRMS=',wrms,
     . '  WPPD=',wppd,'  NREJ=',nrej

C::::::::::::::::::::
C Plot fits.
C Get starting, ending, and center row position along trace.
      crat = polyarroff(eotemp(0,eon),dfloat((lo_col+hi_col)/2))
C Has this trace already been plotted?  Check center row.
      plotit=.true.
      if (ncrow.gt.0) then
        do i=1,ncrow
          if (abs((crow(i)-crat)).lt.5.) plotit=.false.
        enddo
      endif
      IF (plotit) THEN
C Record this center row in array.
      ncrow=ncrow+1
      crow(ncrow)=crat
C Fill arrays for plot, subtract off line going through end-points of fit.
      nn=0
      do ii=1,npt,5
        if (w8(ii).gt.0.) then
          nn=nn+1
          b(nn)  =sngl(x8(ii))
          arr(nn)=sngl( y8(ii) - polyarroff(eotemp(0,eon),x8(ii)) )
        endif
      enddo

C Add zeros at column ends to define start and end of plot.
      nn=nn+1
      b(nn)  = float(lo_col)
      arr(nn)= 0.
      nn=nn+1
      b(nn)  = float(hi_col)
      arr(nn)= 0.

C Plot trace data.
      call PGPAGE
      call PGSLW(1)
      call PGSLS(1)
      call PGSCF(1)
      call PGSCH(2.5)
      call EE_SetPlotBox(nn,b,arr)
      call PGPT(nn,b,arr,-1)
      call PG_HLine(float(lo_col),float(hi_col),0.)

C Write center row array number, center row, and WRMS on each plot.
      c44=' '
      if (ncrow.eq.1) then
        write(c44,'(i2.2,a,i4,a,f6.3,a,i4,a,i4)') 
     .         ncrow,'  cr=',min(9999,max(-1,nint(crat))),
     .         '  rms=',min(99.999,max(-1.,wrms)),
     .         '  Obs#',min(9999,max(-1,Global_obsnum)),
     .         '  S#',min(9999,max(-1,Star_Obsnum))
      elseif (ncrow.eq.2) then
        write(c44,'(i2.2,a,i4,a,f6.3,3x,a)')
     .         ncrow,'  cr=',min(9999,max(-1,nint(crat))),
     .         '  rms=',min(99.999,max(-1.,wrms)),
     .         Current_ObsDate
      else
        write(c44,'(i2.2,a,i4,a,f6.3)')
     .         ncrow,'   cr=',min(9999,max(-1,nint(crat))),
     .         '   rms=',min(99.999,max(-1.,wrms))
      endif
      call PGMTEXT('T',0.3,0.05,0.0,c44)

      ENDIF
C::::::::::::::::::::

      ENDDO                          ! DO eon=1,neotemp

      return
      end




C----------------------------------------------------------------------
C Using the order positions down a given column, determine all the
C order defining polynomials.  Note that it is assumed that the order tilt
C is less than 10.0 degrees (tan(10.0)=0.176).
C
      subroutine EE_TraceOrders_HIRES(sc,ec,sr,er,star,cc,neotemp,
     .                                eotemp,first)

      implicit none
      integer*4 sc,ec,sr,er         ! image dimensions
      real*4 star(sc:ec,sr:er)      ! star image
      integer*4 cc                  ! center column for peaks (starting point)
      integer*4 neotemp             ! number of orders or peaks
      real*8 eotemp(0:99,99)        ! echelle order polynomials
      logical first                 ! flag for first time routine is called.
C
      include 'makee.inc'
      include 'global.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
      character c44*44
      integer*4 eon,npt,col,i,k,ii,nn,highpt,maxrej,nrej,dir
      integer*4 ncrow,min_npt,OffEdge,lo_col,hi_col
      real*4 cent,gucent,crow(90)
      real*4 slopelimit
      real*8 polyarroff,srat,erat,crat,MM,BB,polyvaloff
      real*8 wrms,wppd,high,r8,xoff,HighLimit
      logical ok,plotit
C
      common /EETRACEORDERSBLK/ ncrow,crow
C
C Minimum points for trace guess fitting.
      min_npt = 15

C Ignore pixels near edge of image (HIRES only).
      OffEdge = 30

C Slope limit for echelle orders.
      slopelimit = 0.176       ! 10 degrees for HIRES

C High point reject stops when the high pixel residual is less than
C   the weighted RMS times "HighLimit".
      HighLimit = 4.0

C Initialize on first call.
      if (first) then
        ncrow=0
        first=.false.
      endif

      DO eon=1,neotemp

C Fill arrays.
      npt=0
      lo_col=+999000
      hi_col=-999000
C Go left and right, stop if we hit the top or bottom of chip.
      do dir=-1,+1,2
        col = cc
C Guess at centroid.
        gucent = zpk(eon)
        do while((col.gt.sc+OffEdge).and.(col.lt.ec-OffEdge))
C Calculate centroid.
          call EE_ImageCentroid(sc,ec,sr,er,star,col,gucent,0.25,
     .                          cent,ok)
C Include point if centroid is close enough to the guess or we do not yet
C have enough points.
          if (ok) then
            if ((abs(cent-gucent).lt.4.0).or.(npt.lt.20)) then
              npt=npt+1
              x8(npt)= dfloat(col)
              if (col.lt.lo_col) lo_col=col
              if (col.gt.hi_col) hi_col=col
              y8(npt)= dble(cent)
              w8(npt)= 1.d0
            endif
CCC#@#    else
CCC#@#    write(soun,'(a,i5)')'WARNING:1: Failed centroid at col=',col
          endif
C Guess at next point. Limit slope.
          if (npt.lt.min_npt) then
            gucent=cent
          else
            call EE_GuessTrace(npt,col,slopelimit,gucent,eon,min_npt)
          endif
C Increment column towards the left (dir=-1) or right (dir=+1).
          col=col+dir
C Are we near top or bottom of image?
          if ( (abs(gucent-float(sr)).lt.(Global_sw/8)).or.
     .         (abs(gucent-float(er)).lt.(Global_sw/8)) ) col=sc
        enddo
      enddo

C Check number of points.
      if (npt.lt.12) then
        write(soun,'(a,i9)')
     .        'ERROR: EETO:2: Too few points found in trace:',npt
        call exit(1)
      endif
C Fit lower polynomial order if partial echelle order.
      if (npt.lt.(ec-sc)/2.) then
        write(soun,'(2a,i6)')
     .         'NOTE: EE_TraceOrders: Partial eche',
     .         'lle order found.  No. of col.=',npt
        if (npt.lt.(ec-sc)/3.) then
          write(soun,'(a)') 
     .  'EE_TraceOrders: Decrease polynomial order to <=2.'
          k = max(1,min(2,EETO_PolyOrder))
        else
          write(soun,'(a)') 
     .  'EE_TraceOrders: Decrease polynomial order to <=3.'
          k = max(1,min(3,EETO_PolyOrder))
        endif
      else
        k = max(1,EETO_PolyOrder)
      endif
C Fit kth order polynomial.
      nrej=0
      maxrej=npt/8
7     continue
      call poly_fit_gj(npt,x8,y8,w8,k,coef,xoff,0,ok)
      if (.not.ok) then
        write(soun,'(a)') 'ERROR: EE_TraceOrders: Fitting polynomial.'
        return
      endif
C Find WRMS.
      call poly_fit_gj_residuals(npt,x8,y8,w8,k,coef,xoff,
     .                           high,wrms,wppd)

C Find element number of highest residual.
      high=0.d0
      highpt=1
      do i=1,npt
        if (w8(i).gt.0.) then
          r8 = abs(y8(i)-polyvaloff(k+1,coef,x8(i),xoff))
          if (r8.gt.high) then
            high  = r8
            highpt= i
          endif
        endif
      enddo
C Go back?
      if ((w8(highpt).gt.0.).and.(high.gt.(HighLimit*wrms))
     .                      .and.(nrej.lt.maxrej)) then
        nrej=nrej+1
        w8(highpt)=0.d0
        goto 7
      endif
C Warn about maximum rejections.
      if (nrej.ge.maxrej) then
        write(soun,'(a)')
     .  'WARNING: EETO: Exceeded maximum # of rejected pixels.'
      endif

C Load coeffecients into eotemp.
      eotemp(0,eon)=xoff
      eotemp(1,eon)=dfloat(EETO_PolyOrder+1)
      do ii=1,nint(eotemp(1,eon))
        eotemp(ii+1,eon)=coef(ii-1)
      enddo

C Report.
      write(soun,'(a,i3,a,f7.3,a,f7.3,a,i4)') 'NOTE: EETO: EON=',
     .           eon,'  High=',high,'  WRMS=',wrms,'  NRej=',nrej

C::::::::::::::::::::
C Plot fits.
C Get starting, ending, and center row position along trace.
      srat = polyarroff(eotemp(0,eon),dfloat(lo_col))
      erat = polyarroff(eotemp(0,eon),dfloat(hi_col))
      crat = polyarroff(eotemp(0,eon),dfloat((lo_col+hi_col)/2))
C Has this trace already been plotted?  Check center row.
      plotit=.true.
      if (ncrow.gt.0) then
        do i=1,ncrow
          if (abs((crow(i)-crat)).lt.5.) plotit=.false.
        enddo
      endif
      IF (plotit) THEN
C Record this center row in array.
      ncrow=ncrow+1
      crow(ncrow)=crat
C Line going through end-points:  y = MM x + BB
      MM   = (erat-srat)/dfloat(hi_col-lo_col)
      BB   = srat-(MM*dfloat(lo_col))
C Fill arrays for plot, subtract off line going through end-points of fit.
      nn=0
      do ii=1,npt,5
        if (w8(ii).gt.0.) then
          nn=nn+1
          b(nn)  =sngl(x8(ii))
          arr(nn)=sngl(y8(ii)-(MM*x8(ii)+BB))
        endif
      enddo
C Add zeros at column ends to define start and end of plot.
      nn=nn+1
      b(nn)  = float(lo_col)
      arr(nn)= 0.
      nn=nn+1
      b(nn)  = float(hi_col)
      arr(nn)= 0.
C Fill array with fit.
      if (Global_HIRES) then
        do ii=lo_col,hi_col,5
          nn=nn+1
          b(nn)  =float(ii)
          arr(nn)=sngl( polyarroff(eotemp(0,eon),dfloat(ii)) -
     .                  (MM*dfloat(ii)+BB) )
        enddo
      endif
C Plot trace data.
      call PGPAGE
      call PGSLW(1)
      call PGSLS(1)
      call PGSCF(1)
      call PGSCH(2.5)
      call EE_SetPlotBox(nn,b,arr)
      call PGPT(nn,b,arr,-1)
      call PG_HLine(float(lo_col),float(hi_col),0.)

C Write center row array number, center row, and WRMS on each plot.
      c44=' '
      if (ncrow.eq.1) then
        write(c44,'(i2.2,a,i4,a,f6.3,a,i4,a,i4)') 
     .         ncrow,'  cr=',min(9999,max(-1,nint(crat))),
     .         '  rms=',min(99.999,max(-1.,wrms)),
     .         '  Obs#',min(9999,max(-1,Global_obsnum)),
     .         '  S#',min(9999,max(-1,Star_Obsnum))
      elseif (ncrow.eq.2) then
        write(c44,'(i2.2,a,i4,a,f6.3,3x,a)')
     .         ncrow,'  cr=',min(9999,max(-1,nint(crat))),
     .         '  rms=',min(99.999,max(-1.,wrms)),
     .         Current_ObsDate
      else
        write(c44,'(i2.2,a,i4,a,f6.3)')
     .         ncrow,'   cr=',min(9999,max(-1,nint(crat))),
     .         '   rms=',min(99.999,max(-1.,wrms))
      endif
      call PGMTEXT('T',0.3,0.05,0.0,c44)

      ENDIF
C::::::::::::::::::::

      ENDDO                          ! DO eon=1,neotemp

      return
      end


C------------------------------------------------------------------------
C Use trace points collected so far to guess where centroid should be.
C "n" is the number of points in the x8,y8 arrays. "col" is the column number
C where the centroid should be guessed.  "limit" is the absolute value of the
C maximum slope allowed for the straight line (dy/dx).
C "cent" returns the centroid guess.
C
      subroutine EE_GuessTrace(n,col,limit,cent,eon,min_Sn)
C
      implicit none
      integer*4 n        ! number of points loaded into x8,y8 arrays (input)
      integer*4 col      ! column number where centroid esimated (input)
      real*4 limit       ! maximum slope (dy/dx) (input)
      real*4 cent        ! centroid guess (output)
      integer*4 eon      ! echelle order number (input)
      integer*4 min_Sn   ! minimum number of points needed for fit (input)
C
      include 'makee.inc'
      include 'scratch.inc'
      include 'global.inc'
      include 'soun.inc'
C
      real*8 aa,bb,S,Sx,Sy,Sxx,Sxy,col8,xlim,D,max_xlim
      integer*4 i,Sn
C
C Need larger limit for ESI.
      max_xlim = 500.
      if (Global_ESI) max_xlim = 1500.
C
C Column point.
      col8= dfloat(col)
C Fit points within 140 columns.
      xlim=140.d0
C Fit a straight line (see page 656 of Numerical Recipes, Press et al. 1992)
1     continue
      Sn  = 0
      Sx  = 0.d0
      Sy  = 0.d0
      Sxx = 0.d0
      Sxy = 0.d0
      do i=1,n
        if (abs(x8(i)-col8).lt.xlim) then
          Sx = Sx + x8(i)
          Sy = Sy + y8(i)
          Sxx= Sxx+ (x8(i)*x8(i))
          Sxy= Sxy+ (x8(i)*y8(i))
          Sn = Sn + 1
        endif
      enddo
C If not enough points, expand range.
      if (Sn.lt.min_Sn) then
        xlim=xlim+25.d0
        if (xlim.gt.max_xlim) then
          write(soun,'(a,i4,a,i5)') 
     .'ERROR:EE_GuessTrace: Not enough good centroid points. Order#',
     . eon,'  Column#',col
CCC       do jj=1,n
CCC         print '(3(f12.5))',x8(jj),y8(jj),w8(jj)
CCC       enddo
          return
        endif
        goto 1
      endif
C Equation is of the form:  y = a + bx
      S = dfloat(Sn)
      D = (S*Sxx) - (Sx*Sx)
      aa= ((Sxx*Sy)-(Sx*Sxy))/D
      bb= ( (S*Sxy)-(Sx*Sy) )/D
C Check slope.  If too big, refit with fixed slope.
      if (abs(sngl(bb)).gt.limit) then
        bb = min(dble(limit),max(-1.d0*dble(limit),bb)) 
        aa = (Sy-(bb*Sx))/S
      endif
C Guess at column "col".
      cent = sngl( aa + (bb*col8) )
      return
      end

C------------------------------------------------------------------------
C Image centroid. "gucent" is the guess at the centroid position evaluated at
C "col".  "centroid" returns the centroid.
C
C          Now deals with masked pixels.  -tab April 2000  
C
      subroutine EE_ImageCentroid(sc,ec,sr,er,star,col,gucent,
     .                                      slitfrac,centroid,ok)
C
      implicit none
      integer*4 sc,ec,sr,er       ! Dimensions of data image (input)
      real*4 star(sc:ec,sr:er)    ! Data image (input).
      integer*4 col               ! Column to use (input).
      real*4 gucent               ! Guess at the centroid (row space)(input)
      real*4 centroid             ! Computed centroid (output).
      real*4 slitfrac             ! Centroid Computation Width in terms of a
                                  !   fraction of slit width (e.g. 0.25 or 0.15)
      logical ok                  ! Successful? (output)
C
      integer*4 row,k0,k1,k2,width,count,slitlimit
      real*4 low,last_centroid
      real*8 r,r1,r2
      logical again
C
      include 'makee.inc'
      include 'global.inc'
C
C Initialize.
      k1   = 0
      k2   = 0
      width= 0
      count= 0
      last_centroid = gucent
C Iterate.
111   continue
      count=count+1
      k0   = nint(last_centroid)
C Are we at the edge of the image?
      if ((k0.le.sr).or.(k0.ge.er)) then
        ok=.false.
        return
      endif
C Expand width until we hit edge of image, masked pixel, or fraction of slit.
      slitlimit = nint( Global_sw * slitfrac )
      again = .true.
      do while(again)
        k1 = k0 - width
        k2 = k0 + width
        if ((k1.le.sr).or.(k2.ge.er)) then
          again = .false.
        elseif ( (star(col,k1).lt.-1.e+9).or.
     .       (star(col,k2).lt.-1.e+9) ) then
          again=.false.
        else
          width = width + 1
          if (width.gt.slitlimit) again=.false.
        endif
      enddo
C Enough rows to work with?
      if (width.lt.2) then
        ok=.false.
        return
      endif
C Compute centroid.
      low= max(0.,star(col,k1))
      do row=k1,k2
        if ( (star(col,row).lt.low).and.
     .       (star(col,row).gt.-1.e+9) ) then
          low=star(col,row)
        endif
      enddo
      low=max(0.,low)
      r1 = 0.d0
      r2 = 0.d0
      do row=k1,k2
        if (star(col,row).gt.-1.e+9) then
          r = dble(max(0.,star(col,row)-low))
          r1= r1 + (dfloat(row)*r)
          r2= r2 + (r)
        endif
      enddo
      centroid = sngl( r1 / max(1.d-20,r2) )
C Iterate?
      if (count.gt.5) then
        ok=.true. 
        return
      endif
      if (abs(centroid-last_centroid).gt.0.2) then
        last_centroid = centroid
        goto 111
      endif
      ok = .true.
      return
      end


C------------------------------------------------------------------------
C Determine slit width (Global_sw) in pixels using flat field image.
C z and b are scratch arrays.

      subroutine EE_FindSlitWidth(sc,ec,sr,er,flat)

      implicit none
      integer*4 sc,ec,sr,er            ! image dimensions (input)
      real*4 flat(sc:ec,sr:er)         ! flat field image (input)
C
      include 'makee.inc'
      include 'global.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
      integer*4 cc,row,col,i,j,k,narr,pkk,k2
      real*4 sum,wsum,pkval

C Initialize.
      pkk = 0
      k2  = 0
      pkval=0.
      narr= 0
      Global_sw = 0.

C Find slit width for several regions on image.
      DO cc=sc+200,ec-200,200
C Make vertical mash.
        do row=sr,er
          z(row) = 0.
          do col=cc-20,cc+20
            z(row) = z(row) + flat(col,row)
          enddo
          z(row) = z(row) / 21.
        enddo
C Correlation function.
        do k=1,er-sr
          b(k)= 0.
        enddo
        do i=sr,er
          do j=i+1,er
            k=j-i
            b(k) = b(k) + abs(z(i)-z(j))
          enddo
        enddo
C Determine first peak.
        k=5 
        do while(k.lt.er-sr)
          if (b(k).lt.b(k-1)) then
            pkk  = k-1
            pkval= b(k-1)
            k=er-sr
          else
            k=k+1
          endif
        enddo
C Find point where it drops by 9%.
        k=pkk
        do while(k.lt.er-sr)
          if (b(k).lt.pkval*0.9) then
            k2 = k-1
            k  = er-sr
          else
            k=k+1
          endif
        enddo
C Now find centroid.
        sum =0.
        wsum=0.
        do k=1,k2
          if (b(k).gt.pkval*0.9) then
            sum = sum + float(k)*(b(k)-(pkval*0.899))
            wsum=wsum + (b(k)-(pkval*0.899))
          endif
        enddo
        if (wsum.gt.1.e-20) then
          narr = narr + 1
          arr(narr) = sum / wsum
        endif
      ENDDO
      if (narr.lt.3) then
        write(soun,'(a,i6)') 
     .  'ERROR: EE_FindSlitWidth: Estimating slit width.  narr=',narr
        call exit(1)
      endif
      call find_median(arr,narr,Global_sw)
C Slight correction.
      Global_sw = Global_sw * 1.1
      return
      end

C----------------------------------------------------------------------
      subroutine EE_NearPeak(sr,er,z,slitwidth,pr)
      implicit none
      integer*4 sr,er,k,i1,i2,iter
      real*4 z(*),slitwidth,pr,cent,diff
      k    = nint(slitwidth/4)
      diff = 1.
      iter = 0
      do while((diff.gt.0.01).and.(iter.lt.5))
        i1 = min(er,max(sr,nint(pr)-k))
        i2 = min(er,max(sr,nint(pr)+k))
        call EE_Centroid(sr,er,z,i1,i2,cent)
        diff = abs(cent-pr)
        pr   = cent
        iter = iter + 1
      enddo
      return
      end 

C----------------------------------------------------------------------
C
      subroutine EE_Centroid(sr,er,z,i1,i2,centroid)
C
      implicit none
      integer*4 sr,er,i1,i2,i
      real*4 z(*),r,r1,r2,centroid
C
      include 'soun.inc'
C
      r1 = 0.
      r2 = 0.
      do i=i1,i2
        r = max(0.,z(i))
        r1 = r1 + (float(i)*r)
        r2 = r2 + (r)
      enddo 
      if ((r1.lt.1.e-10).or.(r2.lt.1.e-10)) then
        write(soun,'(a)') 'ERROR: EE_Centroid: Finding centroid.'
        centroid = (float(i1)+float(i2))/2.
      else
        centroid = r1/r2
      endif
      return
      end

C----------------------------------------------------------------------
C Given z() vertical median mash, finds zpk() the positions of the peaks.
C Both z() and zpk() are in kee.inc.
C
      subroutine EE_FindPeaks(sr,er,neotemp)
C
      implicit none
      integer*4 sr,er,neotemp
      real*4 hiz,peakrow
      integer*4 i,row
C
      include 'makee.inc'
      include 'global.inc'
      include 'scratch.inc'      ! This contains z(), and zpk().
C
C Locate first two orders.
C Find highest value within three slit widths.
      i = sr
      do row=sr+1,sr+nint(3.*Global_sw)
        if (z(row).gt.z(i)) i=row
      enddo
      hiz = 0.1 * z(i)
C Find first value >10% of high value, starting a half slit width in.
      row = sr + nint(0.5*Global_sw)
      do while(z(row).lt.hiz)
        row = row + 1
      enddo
      peakrow = float(row)
      call EE_NearPeak(sr,er,z,Global_sw,peakrow)
C If first order too close to edge, start a full slit width in.
      if (abs(peakrow-float(sr)).lt.(Global_sw/4)) then
        row = sr + nint(Global_sw)
        do while(z(row).lt.hiz)
          row = row + 1
        enddo
        peakrow = float(row)
        call EE_NearPeak(sr,er,z,Global_sw,peakrow)
      endif
      zpk(1) = peakrow
C Jump a half slit width and find next high value.
      row = nint(zpk(1)) + nint(0.5*Global_sw)
      do while(z(row).lt.hiz)
        row = row + 1
      enddo
      peakrow = float(row)
      call EE_NearPeak(sr,er,z,Global_sw,peakrow)
      zpk(2) = peakrow
C Find remaining orders by jumping an amount equal to last order
C separation plus two and a half percent.
      neotemp=2
      peakrow = zpk(neotemp) + (1.025*(zpk(neotemp)-zpk(neotemp-1)))
      do while(nint(peakrow).lt.(er-nint(Global_sw/4)))
        i = max(sr,min(er,nint(zpk(neotemp))))
        call EE_NearPeak(sr,er,z,Global_sw,peakrow)
        neotemp=neotemp+1
        zpk(neotemp) = peakrow
        peakrow  = peakrow + (1.025*(zpk(neotemp)-zpk(neotemp-1)))
      enddo
      return
      end

C----------------------------------------------------------------------
C Preform a boxcar smoothing on array a(..).  Result in array b(..).
C "size" should be an odd integer.
      subroutine EE_Boxcar(n,a,b,size)
      implicit none
      integer*4 n,size,num,j,half,i
      real*4 a(*),b(*),sum
      half = size/2
      do i=1,n
        sum=0.
        num=0
        do j=max(1,i-half),min(n,i+half)
          num=num+1
          sum=sum+a(j)
        enddo
        b(i)=sum/dfloat(max(1,num))
      enddo
      return
      end

C----------------------------------------------------------------------
      subroutine find_average(arr,n,average)
      implicit none
      integer*4 n,i
      real*4 arr(n),average
      average=0.
      do i=1,n
        average=average+arr(i)
      enddo
      average=average/float(max(1,n))
      return
      end




C-------------------------------------------------------------------
C Make wild guesses at parameters for a Gaussian-like distribution.
C
      subroutine gausslike_autoguess(p,h,x0)
C
      implicit none
      real*8 p,h,x0,dum1,dum2
C
      include 'scratch.inc'       ! common block with x8, y8, and w8.
C
      integer*4 n,i,j,narrb
      common /funkblk/ dum1,dum2,n
      real*4 arrb(9),r
C
C Initial guess.
      i = max(1,n/2)
      p = y8(i)
      x0= x8(i)
C Find group of five points with highest median.
      if (n.gt.5) then
        do i=3,n-2
          narrb=0
          do j=i-2,i+2
            if (w8(j).gt.0.) then
              narrb=narrb+1
              arrb(narrb) = sngl(y8(j))
            endif
          enddo
          if (narrb.gt.1) then
            call find_median(arrb,narrb,r)
            if (dble(r).gt.p) then
              p = dble(r)
              x0= x8(i)
            endif
          endif
        enddo
      endif
C Half of half width of region to be fitted.
      h = dabs(x8(1)-x8(n))/4.
      return
      end

C--------------------------------------------------------------------------
C Fit a Gaussian of the form:  y = peak * exp(-ln(2)*((x-xcenter)/hwhm)**2.).
C Points and weights (x8,y8,w8) will be in a common block in an include file.
C If peak, hwhm, or xcenter are non-zero they are assumed to be guesses for the
C peak of the Gaussian, Half-Width-Half-Maximum, and center point.
C If any of these are zero, the function will make its own guesses.
C
      logical function fit_gauss(npoints,peak,hwhm,xcenter)
C
      implicit none
      integer*4 npoints         ! number of points, x8(1..npts), etc.
      real*8 peak,hwhm,xcenter  ! Peak, HWHM, and center x value.
C
      include 'scratch.inc'       ! common block with x8, y8, and w8.
C
      logical amoeba_gate
      real*8 tiny,p,h,x0,dum1,dum2
      parameter(tiny=1.d-50)
C For AMOEBA.
      real*8 fit_gauss_funk   ! External function to minimize   (input)
      EXTERNAL fit_gauss_funk
      integer*4 npar          ! Number of parameters            (input)
      parameter(npar=3)
      real*8 par(npar)        ! Parameters (p,h,x0)             (input/output)
      real*8 parsl(npar)      ! Scale length for each parameter (input)
      real*8 resid            ! Residual (final value of FUNK)  (output)
C For use by fit_gauss_funk (function to be minimized).
      integer*4 n
      common /funkblk/ dum1,dum2,n
C
C Transfer parameters to variables.
      p = peak
      h = hwhm
      x0= xcenter
      n = npoints
C Auto-guess.
      call gausslike_autoguess(p,h,x0)
C Use user guesses if requested.
      if (dabs(peak).gt.tiny)    p = peak
      if (dabs(hwhm).gt.tiny)    h = hwhm
      if (dabs(xcenter).gt.tiny) x0= xcenter
C Load parameters.
      par(1) = p
      par(2) = h
      par(3) = x0
C Load scale lengths.
      parsl(1) = p*0.1d0
      parsl(2) = h*0.1d0
      parsl(3) = dabs(x8(1)-x8(n))/10.d0
C Fit Gaussian.
      fit_gauss = amoeba_gate(fit_gauss_funk,npar,par,parsl,resid)
      peak = par(1)
      hwhm = par(2)
      xcenter = par(3)
C     print '(a,3(1pe13.6))',
C    .  'Peak, FWHM, X-Center =',peak,hwhm*2.d0,xcenter
      return
      end

C-------------------------------------------------------------------
C Minimize the deviations from a Gaussian.
C
      real*8 function fit_gauss_funk(par)
C
      implicit none
      real*8 par(3)         ! p,h,x0
C
      include 'scratch.inc'      ! common block with x8, y8, and w8.
C
      integer*4 i
      real*8 mln2,sum,r,yv,dum1,dum2
      parameter(mln2=-0.693147181d0)          !  -ln(2)
      integer*4 n
      common /funkblk/ dum1,dum2,n
C
      sum = 0.d0
      do i=1,n
        r  = (x8(i)-par(3))/par(2)
        yv = par(1) * dexp(mln2*r*r)
        r  = y8(i)-yv
        sum = sum + (r*r*w8(i))
      enddo
      fit_gauss_funk = sum
      return
      end

C----------------------------------------------------------------------
C Calculate weight a sky pixel should have based on how it is contained
C within the background regions.
C
      subroutine EE_SkyWeight(rj,rb1,rb2,rb3,rb4,weight)
C
      implicit none
      real*4 rj,rb1,rb2,rb3,rb4,wt,rj1,rj2
      real*8 weight
C Bounds of pixel.
      rj1=rj-0.5
      rj2=rj+0.5
C Start with no weight.
      wt=0.
C Check first background boundary.
      if (rj1.lt.rb2) then
C Pixel enclosed by boundary?
        if ((rb1.le.rj1).and.(rb2.ge.rj2)) then
          wt = wt + (rj2-rj1)
C Boundary enclosed by pixel?
        elseif ((rj1.le.rb1).and.(rj2.ge.rb2)) then
          wt = wt + (rb2-rb1)
C Right boundary edge straddles pixel?
        elseif ((rj1.lt.rb2).and.(rj2.gt.rb2)) then
          wt = wt + (rb2-rj1)
C Left boundary edge straddles pixel?
        elseif ((rj1.lt.rb1).and.(rj2.gt.rb1)) then
          wt = wt + (rj2-rb1)
        endif
      endif
C Check second background boundary.
      if (rj2.gt.rb3) then
C Pixel enclosed by boundary?
        if ((rb3.le.rj1).and.(rb4.ge.rj2)) then
          wt = wt + (rj2-rj1)
C Boundary enclosed by pixel?
        elseif ((rj1.le.rb3).and.(rj2.ge.rb4)) then
          wt = wt + (rb4-rb3)
C Right boundary edge straddles pixel?
        elseif ((rj1.lt.rb4).and.(rj2.gt.rb4)) then
          wt = wt + (rb4-rj1)
C Left boundary edge straddles pixel?
        elseif ((rj1.lt.rb3).and.(rj2.gt.rb3)) then
          wt = wt + (rj2-rb3)
        endif
      endif
      weight = dble(wt)
      return
      end

C----------------------------------------------------------------------
C
      subroutine EEFB_Fit_and_Reject(n,ord)
C
      implicit none
      integer*4 n,ord
C
      include 'scratch.inc'
      include 'soun.inc'
C
      integer*4 i,nrej,highpt
      real*4 high,rr
      real*8 polyval
      logical ok
C###
C     real*4 xx(99),yy(99),ff(99)
C     real*4 xmin,xmax,ymin,ymax
C     integer*4 ii
C###
C
C Check.
      if (n.lt.ord+1) then
        print *,
     .  'ERROR: EEFB_Fit_and_Reject: too few points: n=',n,
     .  ' order=',ord
        call exit(0)
      endif
C
C (Re)Fit polynomial.
      nrej=0
1     continue
      do i=0,ord+2
        coef(i)=0.d0
      enddo
      call poly_fit_glls(n,x8,y8,w8,ord,coef,ok)
      if (.not.ok) write(soun,'(a)') 
     .  'ERROR: EEFB_Fit_and_Reject: Fitting polynomial.'
      if (.not.ok) call exit(1)
C Find high residual.
      high=0.
      highpt=0
      do i=1,n
        if (w8(i).gt.0.) then
          rr = sngl(abs( polyval(ord+1,coef,x8(i)) - y8(i) ))
          if (rr.gt.high) then
            high  = rr
            highpt= i
          endif
        endif
      enddo
C Reject if high residual larger than 3 pixels.
      if ((high.gt.3.).and.(n-nrej.gt.5)) then
        nrej=nrej+1
        w8(highpt)=0.d0
        write(soun,'(a,2i6,1pe16.7)') 
     .   'EEFB: Rejecting ',highpt,nint(x8(highpt)),sngl(y8(highpt))
        goto 1
      endif
      return
      end

C-------------------------------------------------------------------------
C Write spectra asp into row of image a.
      subroutine EE_Spectrum_to_Image(a,sc,ec,sr,er,row,asp)
      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er),asp(sc:ec)
      integer*4 col
      do col=sc,ec
        a(col,row) = asp(col)
      enddo
      return
      end
      
C----------------------------------------------------------------------
      subroutine PSE_SetProfpoly(pp)
      implicit none
      logical pp
      include 'pse.inc'
      profpoly = pp
      return
      end
 

C
C
C
CC-------------------------------------------------------------------------
CC
C      subroutine write_it
CC
C      implicit none
C      include 'makee.inc'
C      include 'global.inc'
CC
C      integer*4 k,i,j
CC
C      open(1,file='ee.put',status='unknown')
C      write(1,'(i9)') Global_neo
C      write(1,'(1pe16.8)') Global_sw
C      do i=1,Global_neo
C        write(1,'(7(1pe22.14))') (eo(k,i),k=1,7)
C        write(1,'(7(1pe22.14))') (bk1(k,i),k=1,7)
C        write(1,'(7(1pe22.14))') (bk2(k,i),k=1,7)
C        write(1,'(7(1pe22.14))') (sp1(k,i),k=1,7)
C        write(1,'(7(1pe22.14))') (sp2(k,i),k=1,7)
C        write(1,'(5(1pe22.14))') (pparm(k,i),k=1,5)
C        do j=1,nint(pparm(1,i))
C          write(1,'(1pe22.14)') pval(j,i)
C        enddo
C      enddo
C      close(1)
C      return
C      end
C        
CC-------------------------------------------------------------------------
CC
C      subroutine read_it
CC
C      implicit none
C      integer*4 k,i,j
CC
C      include 'makee.inc'
C      include 'global.inc'
CC
C      open(1,file='ee.get',status='old')
C      read(1,'(i9)') Global_neo
C      read(1,'(1pe16.8)') Global_sw
C      do i=1,Global_neo
C        read(1,'(7(1pe22.14))') (eo(k,i),k=1,7)
C        read(1,'(7(1pe22.14))') (bk1(k,i),k=1,7)
C        read(1,'(7(1pe22.14))') (bk2(k,i),k=1,7)
C        read(1,'(7(1pe22.14))') (sp1(k,i),k=1,7)
C        read(1,'(7(1pe22.14))') (sp2(k,i),k=1,7)
C        read(1,'(5(1pe22.14))') (pparm(k,i),k=1,5)
C        do j=1,nint(pparm(1,i))
C          read(1,'(1pe22.14)') pval(j,i)
C        enddo
C      enddo
C      close(1)
C      return
C      end
C        
C        
C        


C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C----------------------------------------------------------------------
C ECHELLE_EXTRACTION finds orders, slit width, background limits for each
C order, and spectrum limits.
C     subroutine EE(sc,ec,sr,er,obj,star,flat)
C    Inputs:
C sc,ec,sr,er = starting row, ending row, starting row, ending row; these
C               define the dimensions of the images, all images must have
C               the same dimensions.
C obj(*,*)    = flat-divided data image.
C flat(*,*)   = flat field image.
C star(*,*)   = sky image, blank on input, fitted sky image on output.
C   Outputs:
C None yet.
C     implicit none
C     integer*4 sc,ec,sr,er                            ! image boundaries
C     real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er)        ! data and flat images
C     real*4 star(sc:ec,sr:er)                         ! standard star image
C     include 'makee.inc'
C     return
C     end
C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

C----------------------------------------------------------------------
C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C THIS VERSION IS SPECIFIC TO HIRES WITH 1x2 BINNING:
C
C HIRES specific:
C  Use median boxes in sky region and an estimate of the profile shape to
C  eliminate large CRs. (see PSE_SkyMask)
C
C 1x2 binning specific:
C  Assume CRs spread out more in the row direction than the column direction.
C     (see PSE_SkyMask and PSE_ObjMask)
C
C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C
C PSF_SPECTRAL_EXTRACTION (PSE) extracts data from a spectrum where the
C center of the PSF in spatial direction changes gradually across the image.
C Assume the spectrum dispersion runs across columns.
C
      subroutine PSE(sc,ec,sr,er,obj,flat,             ! input/output images
     .               sp1,sp2,bk1o,bk2o,bk1i,bk2i,      ! inputs
     .               profparm,profval,                 ! parameters
     .               mode,                             ! PSE mode (see below)
     .               DoSum,                            ! Do Addtnl Summation
     .               sky,prof,                         ! scratch space
     .               objsp,skysp,varsp,sumsp,          ! output spectra
     .               OBJHEAD)

C
C    Inputs:
C sc,ec,sr,er = starting row, ending row, starting row, ending row; these
C               define the dimensions of the images, all images must have
C               the same dimensions.
C obj(*,*)    = flat-divided data image.
C flat(*,*)   = flat field image.
C sp1(*)      = defines the top boundary for object.
C sp2(*)      = defines the bottom boundary for object.
C bk1o(*)     = defines the top boundary for the background or sky.
C bk2o(*)     = defines the bottom boundary for sky.
C bk1i(*)     = defines the top inner boundary for the background or sky.
C bk2i(*)     = defines the bottom inner boundary for sky.
C Global_eperdn  = electrons per digital number.
C Global_rov     = read-out variance in DN^2.
C Global_obsnum  = current observation number.
C Global_exposure= current exposure time in seconds.
C Global_eon     = current echelle order number (bluest==1).
C profparm    = profile parameters: 1=# of pts, 2=first x value, 3=x increment.
C profval     = profile model (y values).
C mode        = (1) Full extraction.
C               (2) Mask CRs in sky and object, straight sum of object.
C               (3) Mask CRs in sky, straight sum of object.
C               (4) Straight sum of whole order (arc lamp).
C sky(*)      = scratch image used for sky fit, big enough to cover one order.
C prof(*)     = scratch image used for profile fit, big enough for one order.
C
C   [ sp1, sp2, bk1o, and bk2o are in form of a polynomial coeffecient array;
C     the zeroth element is the x or column offset (in the poly fit),
C     the first element is the number of coeffecients in the polynomial,
C     subsequent elements are the polymonial coeffecients, so the total
C     number of elements in the array should be bk1o(1)+1, and the order
C     of the polynomial is bk(1)-1; the function form is: row = func(col). ]
C   [ This form of "real space" boundaries is notably different than discrete
C     ranges.  A range from 3.0 to 5.0 would include all of row 4 and half of
C     row 3 and half of row 5. ]   
C 
C   Outputs:
C objsp(*)    = Object spectrum.
C skysp(*)    = Sky spectrum.
C varsp(*)    = Variance of object spectrum.
C sumsp(*)    = Summed flux object spectrum.
C
      implicit none
      integer*4 sc,ec,sr,er                        ! image boundaries
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er)    ! data and flat images
      real*8 sp1(*),sp2(*),bk1o(*),bk2o(*)         ! polynomials
      real*8 bk1i(*),bk2i(*)                       ! polynomials
      real*4 profparm(*),profval(*)                ! parameters
      real*4 sky(*),prof(*)                        ! scratch images
      real*4 objsp(sc:ec),skysp(sc:ec)             ! output spectra
      real*4 varsp(sc:ec),sumsp(sc:ec)             ! output spectra
      integer*4 mode                               ! PSE mode (see above)
      logical DoSum                                ! Additional Summation
C
      integer*4 ssr,ser,usc,uec,ii,col,back1,back2
      real*8 polyarroff
      logical ok
C
      include 'maxpix.inc'
      include 'pse.inc'
      include 'global.inc'
      include 'soun.inc'
C
      character*57600 OBJHEAD

C Clear output spectra.
      do ii=sc,ec
        objsp(ii)=0.
        skysp(ii)=0.
        varsp(ii)=0.
        sumsp(ii)=0.
      enddo

C Manipulate object profile.
      if ((mode.eq.1).or.(mode.eq.2).or.(mode.eq.3)) then
C Copy profile into common block array.
        PSE_pparm(1) = profparm(1)  ! number of values in profile model
        PSE_pparm(2) = profparm(2)  ! starting x value
        PSE_pparm(3) = profparm(3)  ! delta x (x incr. betw. array elements)
        PSE_pparm(4) = profparm(4)  ! centroid
        PSE_pparm(5) = profparm(5)  ! background level of profile.
        do ii=1,nint(PSE_pparm(1))
          PSE_pval(ii) = profval(ii)
        enddo
      endif

C Find usable columns in order range.
      call EE_order_column_range(bk1o,bk2o,sc,ec,sr,er,usc,uec,ok)

C Check.
      if ((usc.eq.0).and.(uec.eq.0)) then
         write(soun,'(a,i4)') 
     .  'WARNING: PSE: Not enough of an order to extract on order: ',
     .   Global_eon
         return
      endif

C Check.
      if ( ((ec-usc).lt.nint((0.1*(ec-sc)))).or.
     .     ((uec-sc).lt.nint((0.1*(ec-sc)))) ) then
        write(soun,'(a,i5,a,i5)') 
     .   'WARNING: PSE: Too few columns in order: usc=',usc,'  uec=',uec
        return
      endif

C Abort if no real usable spectrum.
      if ((uec-usc).lt.10) then
        write(soun,'(a)') 
     .    'WARNING: PSE: Spectrum too small-- ignoring.'
        return
      endif

C Return if there were any problems in EE_order_column_range.
      if (.not.ok) return

C Determine row boundaries for scratch images.
      ssr=er      ! reversed, will be reset below.
      ser=sr
      do col=usc,uec
        back1 = nint( polyarroff(bk1o,dfloat(col)) )
        back2 = nint( polyarroff(bk2o,dfloat(col)) )
        if (back1.lt.ssr) ssr = back1
        if (back2.lt.ssr) ssr = back2
        if (back1.gt.ser) ser = back1
        if (back2.gt.ser) ser = back2
      enddo
      ssr= max(sr,min(er,ssr))
      ser= max(sr,min(er,ser))

C Check scratch image size.
      if (abs((1+sc-ec)*(1+ser-ssr)) .gt. maxpixsub) then
        write(soun,'(a)') 
     .    'ERROR: PSE: Spectrum too large for allocated memory.'
        return
      endif

C Sky subtraction.
      IF ((mode.eq.1).or.(mode.eq.2).or.(mode.eq.3)) THEN
C Set PSE Sky Rejection Adjustment at 1.0 initially.
        PSESRA = 1.0
C First we check to see if too many cosmic rays and highly deviant pixels (CRs)
C will be rejected in PSE_SkyMask and increase PSESRA (PSE Sky Rejection
C Adjustment) accordingly.  This should be only rarely necessary.
        call PSE_SkyMaskCheck(usc,uec,sc,ec,sr,er,obj,flat,
     .                        sp1,sp2,bk1o,bk2o,bk1i,bk2i)
C Now we mask cosmic rays and deviant pixels (CRs) from sky.
        call PSE_SkyMask(usc,uec,sc,ec,sr,er,obj,flat,
     .                   sp1,sp2,bk1o,bk2o,bk1i,bk2i)
C Fit the sky.
        call PSE_SkyFit(usc,uec,sc,ec,sr,er,obj,flat,
     .                  sp1,sp2,bk1o,bk2o,bk1i,bk2i,skysp,sky,ssr,ser)
C Subtract off background from profile.
        call PSE_ProfileModelSkyFit(sc,ec,sp1,sp2,bk1o,bk2o,bk1i,bk2i)
      ENDIF

C Mask CRs in the object region. 
      IF ((mode.eq.1).or.(mode.eq.2)) THEN
        call PSE_ObjMask(usc,uec,sc,ec,sr,er,obj,flat,sp1,sp2,
     .                   sky,prof,ssr,ser,objsp)
      ENDIF

C A very simple object spectrum extraction.
      IF ((mode.eq.2).or.(mode.eq.3)) THEN
        write(soun,'(a)') 'Object flux straight summation...'
        call PSE_ObjSum(usc,uec,sc,ec,sr,er,obj,flat,sp1,sp2,
     .                  sky,ssr,ser,objsp,varsp)
      ENDIF

C Final object flux and variance calculations.
      IF (mode.eq.1) THEN
C Do additional straight summation.
        if (DoSum) then
          call PSE_ObjSum(usc,uec,sc,ec,sr,er,obj,flat,sp1,sp2,
     .                    sky,ssr,ser,sumsp,varsp)
        endif
C Clear variance.
        do ii=sc,ec
          varsp(ii)=0.
        enddo
        call PSE_ObjFlux(usc,uec,sc,ec,sr,er,obj,flat,sp1,sp2,
     .                   sky,prof,ssr,ser,objsp,varsp)
      ENDIF

C Wavelength lamp summation.
      IF (mode.eq.4) THEN
        call PSE_ArcSum(usc,uec,sc,ec,sr,er,obj,flat,bk1o,bk2o,objsp)
      ENDIF


      return
      end


C------------------------------------------------------------------------
C A very simple Arc Lamp spectrum extraction.
C Finds the median of each column between background boundaries.
C
      subroutine PSE_ArcSum(usc,uec,sc,ec,sr,er,obj,flat,
     .                      bk1o,bk2o,objsp)
C
      implicit none
      integer*4 usc,uec,sc,ec,sr,er
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er)
      real*8 bk1o(*),bk2o(*)
      real*4 objsp(sc:ec)
C
C Need arr(..) from include file.
C###  include 'pse.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
      integer*4 col,row,narr,num,i
      real*4 rb1,rb2,sum,median
C     integer*4 nrej,maxrej
C     real*4 sigs,PSE_getvar
      real*8 polyarroff
      logical warnyet,PSE_Masked
C
C Warning message flag.
      warnyet=.false.
C Clear spectra.
      do col=sc,ec
        objsp(col)=0.
      enddo
      DO col=usc,uec
C Determine object boundaries for this column.
        rb1 = max(float(sr+1),min(float(er-1),
     .        sngl(polyarroff(bk1o,dfloat(col)))))
        rb2 = max(float(sr+1),min(float(er-1),
     .        sngl(polyarroff(bk2o,dfloat(col)))))
C Warning.
        if ((.not.warnyet).and.(abs(rb1-rb2).lt.1.0)) then
C Do not give warning if we are at top or bottom of CCD.
          if ( ((er-rb2).gt.1.1).and.((rb1-sr).gt.1.1) ) then
            write(soun,'(a)') 
     .  'WARNING: PSE_ArcSum: Object width too small.'
            warnyet=.true.
          endif
        endif
C Find median in each column.
        narr=0
        do row=nint(rb1),nint(rb2)
          if (.not.PSE_Masked(obj(col,row))) then
            narr=narr+1
            arr(narr)=obj(col,row)
          endif
        enddo
        call find_median(arr,narr,median)
        objsp(col) = median
C Correct for flat field division.
        sum=0.
        num=0
        do row=nint(rb1),nint(rb2)
          do i=max(usc,col-3),min(uec,col+3)
            sum = sum + flat(i,row)
            num = num + 1
          enddo
        enddo
        objsp(col) = objsp(col)*(sum/float(max(1,num)))
      ENDDO
      return
      end


C----------------------------------------------------------------------
C This loads the EEPARMBLK common block in the file "kee.inc" using a 
C parameter header string read from a parameter file.
C
      subroutine EE_Parameters(parmhead)
      implicit none
      character*(*) parmhead
      include 'makee.inc'
      real*4 GetRealParm
C     character*80 GetCharParm

C
C     EE_MaskFile = GetCharParm('EE_MaskFile',parmhead) 
C     if (EE_MaskFile.eq.' ') 
C    .  print *,'WARNING-- No MaskFile will be used.'
C

      EETO_PolyOrder = nint(GetRealParm('EETO_PolyOrder',parmhead))
      call ParamCheck('EETO_PolyOrder',float(EETO_PolyOrder),0.,30.)

      EEFO_ObjectFraction = GetRealParm('EEFO_ObjectFraction',parmhead)
      call ParamCheck('EEFO_ObjectFraction',
     .  EEFO_ObjectFraction,0.1,0.99)

      EEFO_PolyOffset = nint(GetRealParm('EEFO_PolyOffset',parmhead))
      call ParamCheck('EEFO_PolyOffset',float(EEFO_PolyOffset),0.,7.)

      EEFB_Compensate = GetRealParm('EEFB_Compensate',parmhead)
      call ParamCheck('EEFB_Compensate',EEFB_Compensate,-30.,30.)

      EEFB_OverlapCompensate = GetRealParm('EEFB_OverlapCompensate',
     .  parmhead)
      call ParamCheck('EEFB_OverlapCompensate',
     .  EEFB_OverlapCompensate,-30.,30.)
C
C     EE_Eperdn = GetRealParm('EE_Eperdn',parmhead)
C     call ParamCheck('EE_Eperdn',EE_Eperdn,0.0001,1000.0)
C
C     EE_Ronoise = GetRealParm('EE_Ronoise',parmhead)
C     call ParamCheck('EE_Ronoise',EE_Ronoise,0.0001,1000.0)
C
      return
      end

C----------------------------------------------------------------------
C This loads the PSEPARMBLK common block in the file "pse.inc" using a 
C parameter header string read from a parameter file.
C
      subroutine PSE_Parameters(parmhead)
      implicit none
      character*(*) parmhead
C
      include 'pse.inc'
      include 'soun.inc'
C
      character*80 GetCharParm,wrd
      real*4 GetRealParm

      PSE_SkySpacer   = GetRealParm('PSE_SkySpacer',parmhead)
      call ParamCheck('PSE_SkySpacer',PSE_SkySpacer,-9.,9.)

      PSE_SkyMin      = GetRealParm('PSE_SkyMin',parmhead)
      call ParamCheck('PSE_SkyMin',PSE_SkyMin,0.,9.)

      PSE_SkyOrder    = nint(GetRealParm('PSE_SkyOrder',parmhead))
      call ParamCheck('PSE_SkyOrder',float(PSE_SkyOrder),-1.,5.)

      PSE_SkyRejLimOne= GetRealParm('PSE_SkyRejLimOne',parmhead)
      call ParamCheck('PSE_SkyRejLimOne',PSE_SkyRejLimOne,0.1,99.)

      PSE_SkyRejLimSat= GetRealParm('PSE_SkyRejLimSat',parmhead)
      call ParamCheck('PSE_SkyRejLimSat',PSE_SkyRejLimSat,0.1,99.)

      PSE_SkyRejLimTwo= GetRealParm('PSE_SkyRejLimTwo',parmhead)
      call ParamCheck('PSE_SkyRejLimTwo',PSE_SkyRejLimTwo,0.1,99.)

      PSEO_ProfInc    = GetRealParm('PSEO_ProfInc',parmhead)
      call ParamCheck('PSEO_ProfInc',PSEO_ProfInc,0.02,2.0)

      PSEO_ProfIncSlop= GetRealParm('PSEO_ProfIncSlop',parmhead)
      call ParamCheck('PSEO_ProfIncSlop',PSEO_ProfIncSlop,0.02,2.0)

      PSEOM_ProfMdnBox= nint(GetRealParm('PSEOM_ProfMdnBox',parmhead))
      call ParamCheck('PSEOM_ProfMdnBox',float(PSEOM_ProfMdnBox),5.,50.)

      PSE_ProOrder  = nint(GetRealParm('PSE_ProOrder',parmhead))
      call ParamCheck('PSE_ProOrder',float(PSE_ProOrder),-1.,7.)

      PSEOM_ProRejLim = GetRealParm('PSEOM_ProRejLim',parmhead)
      call ParamCheck('PSEOM_ProRejLim',PSEOM_ProRejLim,0.1,99.)

      PSEOM_ObjRejLim = GetRealParm('PSEOM_ObjRejLim',parmhead)
      call ParamCheck('PSEOM_ObjRejLim',PSEOM_ObjRejLim,0.1,99.)

      PSEOM_ObjRejLimBig = GetRealParm('PSEOM_ObjRejLimBig',parmhead)
      call ParamCheck('PSEOM_ObjRejLimBig',PSEOM_ObjRejLimBig,0.1,99.)

      PSEOM_ObjRejLimSat = GetRealParm('PSEOM_ObjRejLimSat',parmhead)
      call ParamCheck('PSEOM_ObjRejLimSat',PSEOM_ObjRejLimSat,0.1,99.)

      wrd = GetCharParm('PSE_SingleColumnSkyFit',parmhead)
      if (wrd.eq.' ') then
        write(soun,'(a)') 
     .  'ERROR: No PSE_SingleColumnSkyFit in param file.'
        call exit(1)
      endif
      PSE_SingleColumnSkyFit = (index(wrd,'F').eq.0)

      return
      end


C----------------------------------------------------------------------
C
      subroutine ParamCheck(s,r,rmin,rmax)
C
      implicit none
      character*(*) s
      real*4 r,rmin,rmax
C
      include 'soun.inc'
C
      if (r.lt.-32000.) then
        write(soun,'(3a)') 
     .  ' ERROR-- Could not find ',s,' in parameter file.'
        call exit(1)
      endif
      if ((r.lt.rmin).or.(r.gt.rmax)) then
        write(soun,'(2a,2(a,1pe11.3))')
     .      ' ERROR: ParamCheck: ',s,
     .      ' should be between',rmin,' and',rmax
        call exit(1)
      endif
      return
      end

C----------------------------------------------------------------------
      subroutine EE_MoreCards(header)
      implicit none
      character*(*) header
      character fdate*24
      include 'makee.inc'
      call  cheadset('MK_VERSN',MK_VERSION,header)
      call  cheadset('EE_VERSN',EE_VERSION,header)
      call  cheadset('EE_RDATE',fdate(),header)
      call  cheadset('EE_MASKF',EE_MaskFile,header)
      call inheadset('EE_PORDR',EETO_PolyOrder,header)
      call  fheadset('EE_OFRAC',dble(EEFO_ObjectFraction),header)
      call inheadset('EE_POFFS',EEFO_PolyOffset,header)
      call  fheadset('EE_CMPNS',dble(EEFB_Compensate),header)
      call  fheadset('EE_OLCMP',dble(EEFB_OverlapCompensate),header)
      return
      end
C----------------------------------------------------------------------
      subroutine PSE_MoreCards(header,pse_mode)
      implicit none
      character*(*) header
      integer*4 pse_mode
      include 'pse.inc'
      call inheadset('PSE_MODE',pse_mode,header)
      call  fheadset('PSE_SSPC',dble(PSE_SkySpacer),header)
      call  fheadset('PSE_SMIN',dble(PSE_SkyMin),header)
      call inheadset('PSE_SORD',PSE_SkyOrder,header)
      call  lheadset('PSE_SCSF',PSE_SingleColumnSkyFit,header)
      call  fheadset('PSE_SRLO',dble(PSE_SkyRejLimOne),header)
      call  fheadset('PSE_SRLS',dble(PSE_SkyRejLimSat),header)
      call  fheadset('PSE_PINC',dble(PSEO_ProfInc),header)
      call  fheadset('PSE_PSLP',dble(PSEO_ProfIncSlop),header)
      call  fheadset('PSE_PMBX',dble(PSEOM_ProfMdnBox),header)
      call inheadset('PSE_PORD',PSE_ProOrder,header)
      call  fheadset('PSE_PRJL',dble(PSEOM_ProRejLim),header)
      call  fheadset('PSE_ORJL',dble(PSEOM_ObjRejLim),header)
      call  fheadset('PSE_ORLB',dble(PSEOM_ObjRejLimBig),header)
      call  fheadset('PSE_ORLS',dble(PSEOM_ObjRejLimSat),header)
      return
      end


C----------------------------------------------------------------------
C Interpolate zero points.
C
      subroutine zero_interp(sc,ec,sr,er,a)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er),sum,b(9000)
      integer*4 i,j,num1,num2,i1,i2
C Do all rows.
      do j=sr,er
C Copy to temporary array.
        do i=sc,ec
          b(i)=a(i,j)
        enddo
C Search for a zero pixel.
        i1=sc
        do while(i1.le.ec)
          if (abs(b(i1)).lt.1.e-30) then
C Find the extent of the region of zero pixels.
            i2=i1+1
            do while((i2.le.ec).and.(abs(b(i2)).lt.1.e-30))
              i2=i2+1
            enddo
            i2=i2-1
C Find a mean value for local valid pixels.
            sum=0.
C Look for valid pixels to the left of region.
            i=i1-1
            num1=0
            do while((i.ge.sc).and.(num1.lt.10))
              if (abs(b(i)).gt.1.e-30) then
                num1=num1+1
                sum =sum +b(i)
              endif
              i=i-1
            enddo
C Look for valid pixels to the right of region.
            i=i2+1
            num2=0
            do while((i.le.ec).and.(num2.lt.10))
              if (abs(b(i)).gt.1.e-30) then
                num2=num2+1
                sum =sum +b(i)
              endif
              i=i+1
            enddo
            sum = sum / float(max(1,num1+num2))
C Fill in zero range.
            do i=i1,i2
              a(i,j)=sum
            enddo
          else
            i2=i1
          endif
          i1=i2+1
        enddo
      enddo
      return
      end

C----------------------------------------------------------------------
C Set zero points to -1.0
      subroutine Zero_to_Negative_One(sc,ec,sr,er,a)
      implicit none
      integer*4 sc,ec,sr,er,i,j
      real*4 a(sc:ec,sr:er)
      do i=sc,ec
        do j=sr,er
          if (abs(a(i,j)).lt.1.e-20) a(i,j)=-1.0
        enddo
      enddo
      return
      end

C----------------------------------------------------------------------
      subroutine EE_GetDate(objhead)
      implicit none
      character*(*) objhead
      include 'makee.inc'
      integer*4 day,mon,year,year4
      character*3 MONTH(12)
C
      data MONTH / 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
     .             'Aug', 'Sep', 'Oct', 'Nov', 'Dec' /
C
      call ReadFitsDate('DATE-OBS',objhead,year4,year,mon,day)
C
      Current_ObsDate=' '
      write(Current_ObsDate,'(i2.2,a3,i2.2)',err=9) 
     .   max(0,min(99,day)),MONTH(max(1,min(12,mon))),
     .   max(0,min(99,year))
9     return
      end

C----------------------------------------------------------------------
C For HIRES instrument, given header and ybin (inputs), output slit width in 
C arcseconds (swas) and slit width in pixels (swpx).   If problem, then swas
C will equal -1.
C
      subroutine LookupSlitWidth(header,ybin,swas,swpx)
C
      implicit none
      character*(*) header
      integer*4 ybin
      real*4 swas,swpx
      character s*80, chead*80
      logical EE_QueryHIRES
      swas=-1.
      if (.not.EE_QueryHIRES(header)) return
      s = chead('DECKNAME',header)
      if (s(1:3).eq.'B1 ') swas = 3.5
      if (s(1:3).eq.'B2 ') swas = 7.0
      if (s(1:3).eq.'B3 ') swas =14.0
      if (s(1:3).eq.'B4 ') swas =28.0
      if (s(1:3).eq.'B5 ') swas = 3.5
      if (s(1:3).eq.'C1 ') swas = 7.0
      if (s(1:3).eq.'C2 ') swas =14.0
      if (s(1:3).eq.'C3 ') swas =28.0
      if (s(1:3).eq.'C4 ') swas = 3.5
      if (s(1:3).eq.'C5 ') swas = 7.0
      if (s(1:3).eq.'D1 ') swas =14.0
      if (swas.gt.0.) then
C Slit width in pixels.
        swpx = ( swas / 0.191 ) / float(ybin)
      endif
      return
      end


C----------------------------------------------------------------------
      logical function Do_This_Order(eon,nueons,ueons)
      implicit none
      integer*4 eon,nueons,i
      integer*4 ueons(*)
      if (nueons.eq.0) then
        Do_This_Order=.true.
        return
      endif
      Do_This_Order=.false.
      do i=1,nueons
        if (ueons(i).eq.eon) Do_This_Order=.true.
      enddo
      return
      end

C----------------------------------------------------------------------
      subroutine EE_SubtractBias(a,bias,sc,ec,sr,er)
      implicit none
      integer*4 sc,ec,sr,er,i,j
      real*4 a(sc:ec,sr:er),bias(sc:ec)
      do i=sc,ec
        do j=sr,er
          a(i,j) = a(i,j) - bias(i)
        enddo
      enddo
      return
      end 

C----------------------------------------------------------------------
      subroutine EE_SubtractImage(a,b,sc,ec,sr,er)
      implicit none
      integer*4 sc,ec,sr,er,i,j
      real*4 a(sc:ec,sr:er),b(sc:ec,sr:er)
      do i=sc,ec
        do j=sr,er
          a(i,j) = a(i,j) - b(i,j)
        enddo
      enddo
      return
      end 

C----------------------------------------------------------------------
C Find background limits with no flat field image available.  The range is 
C estimated from the overall lookup slit width value only.  This is probably ok
C for arc lamps which have a quartz pinhole image for the trace, but NOT ok for
C other situations (normally the object will not be in the center of the order).
C
      subroutine EE_FindBackWithNoFlat(sc,ec,sr,er)
C
      implicit none
      integer*4 sc,ec,sr,er,i,eon
      real*4 split,rc,r1,r2
      real*8 cc,polyarroff,comp,olcomp
C
      include 'makee.inc'
      include 'global.inc'
C
C Set compensation numbers.
      comp  = dble(EEFB_Compensate)
      olcomp= dble(EEFB_OverlapCompensate)
C
C Center column.
      cc = dfloat(sc+ec)/2.d0
C
C Create bk1o and bk2o, we will offset these below.
      do eon=1,Global_neo
        do i=1,mpp
          bk1o(i,eon) = eo(i,eon)
          bk2o(i,eon) = eo(i,eon)
        enddo
C Find bk1o. See if order above is overlapping. Extrapolate for first order.
C Add/subtract compensation (realistic background limits).
        call EE_OrderSplit(cc,eon,split)
        rc = sngl(polyarroff(eo(0,eon),cc))
        r1 = rc - (Global_sw/2.)
        if (eon.gt.1) then
          r2 = sngl(polyarroff(eo(0,eon-1),cc)) + (Global_sw/2.)
        else
          r2 = rc + (Global_sw/2.) - split
        endif
        if (r1.gt.r2) then
C Orders do not overlap.
          bk1o(2,eon) = bk1o(2,eon) + dble(r1-rc) + comp
        else
C Orders do overlap, use different compensation.
          bk1o(2,eon) = bk1o(2,eon) + dble(r2-rc) + olcomp
        endif
C Find bk2. See if order below is overlapping. Extrapolate for last order.
        r1 = rc + (Global_sw/2.)
        if (eon.lt.Global_neo) then
          r2 = sngl(polyarroff(eo(0,eon+1),cc)) - (Global_sw/2.)
        else
          r2 = rc - (Global_sw/2.) + split
        endif
        if (r1.lt.r2) then
C Orders do not overlap.
          bk2o(2,eon) = bk2o(2,eon) + dble(r1-rc) - comp
        else
C Orders do overlap, use different compensation.
          bk2o(2,eon) = bk2o(2,eon) + dble(r2-rc) - olcomp
        endif
      enddo
C
      return
      end


C----------------------------------------------------------------------
C Makee flat averaging routine.
C Files must be contiguous.
C Creates new FITS files in current directory and "listfile".
C
      subroutine hires_flatave(nfi,fi,hi,echtol,xdtol,saturation,rawdir,
     .                         listfile,ok,a,b)
C
      implicit none
      integer*4 nfi           ! # of flat field files in fi() array (input)
      character*(*) fi(nfi)   ! Filenames of flat fields (input)
      real*4 hi(nfi)          ! High values (if 0, recalculates) (input)
      real*4 echtol           ! ECHANGL tolerance (input)
      real*4 xdtol            ! XDANGL tolerance (input)
      real*4 saturation       ! saturation level (input)
      character*(*) rawdir    ! Directory of RAW HIRES FITS files (input)
      character*(*) listfile  ! List of flat fields "flat.list" (input)
      logical ok              ! Successful? (output)
      real*4 a(*),b(*)        ! scratch arrays for images (scratch)
C
      include 'maxpix.inc'
      include 'verbose.inc'
      include 'soun.inc'
C
      character*80 file,chead,b_object,decker,b_decker
      character*80 lampname,b_lampname
      character*4 c4
      integer*4 i,lc,sc,ec,sr,er,nc,nr,i1,flatcount,fc
      integer*4 xbin,ybin,b_xbin,b_ybin,ifi
      integer*4 num,inhead,frameno
      character*57600 header,b_header
      real*4 echangl,xdangl,b_echangl,b_xdangl
      real*8 ot,b_ot

C Open new flat.list file.
      open(25,file=listfile,status='unknown')
C
C Find first good flat field FITS file.
      i1=0
10    continue
      i1=i1+1
      file=rawdir(1:lc(rawdir))//'/'//fi(i1)
      write(soun,'(2a)') 'hires_flatave: Reading: ',file(1:lc(file))
      call readfits(file,b,maxpix,b_header,ok)
      if (.not.ok) goto 901
      call GetDimensions(b_header,sc,ec,sr,er,nc,nr) 

C Grab information.
      call flatinfo(b,sc,ec,sr,er,
     .     b_header,b_echangl,b_xdangl,b_decker,
     .     b_lampname,b_ot,b_xbin,b_ybin,
     .     hi(i1),saturation,ok)
      if (.not.ok) goto 10
      num=1
      b_object = chead('OBJECT',b_header)
      frameno = inhead('FRAMENO',b_header)
      write(c4,'(i4)') frameno
      write(b_object,'(2a)') '<',c4(fc(c4):lc(c4))

C Initialize.
      flatcount=0

C Go through list.
      DO ifi=i1+1,nfi

C Read FITS file.
      file=rawdir(1:lc(rawdir))//'/'//fi(ifi)
      write(soun,'(2a)') 'hires_flatave: Reading: ',file(1:lc(file))
      call readfits(file,a,maxpix,header,ok)
      if (.not.ok) goto 901
      call GetDimensions(header,sc,ec,sr,er,nc,nr) 
C Grab information.
      call flatinfo(a,sc,ec,sr,er,
     .       header,echangl,xdangl,decker,lampname,ot,xbin,ybin,
     .       hi(ifi),saturation,ok)
      if (ok) then
C Add in if matching (if within 2 hours).
        if ( (abs(echangl-b_echangl).lt.echtol).and.
     .       (abs(xdangl-b_xdangl).lt.xdtol).and.
     .       (abs(ot-b_ot).lt.7200.).and.
     .       (xbin.eq.b_xbin).and.(ybin.eq.b_ybin).and.
     .       (decker.eq.b_decker) ) then
          do i=1,nc*nr
            b(i) = b(i) + a(i)
          enddo
          num=num+1
          frameno = inhead('FRAMENO',header)
          write(c4,'(i4)') frameno
          write(b_object,'(3a)') 
     .             b_object(1:lc(b_object)),'+',c4(fc(c4):lc(c4))
        else
C Average previous group.
          do i=1,nc*nr
            b(i) = b(i) / float(num)
          enddo
C Write out previous group.
          b_object = b_object(1:lc(b_object))//'> (average)'
          call cheadset('OBJECT',b_object,b_header)
          flatcount=flatcount+1
          write(file,'(a,i3.3,a)') 'flat',flatcount,'.fits'
          write(soun,'(4a)') 
     .  '   Writing: ',file(1:lc(file)),' : ',b_object(1:lc(b_object))
          call writefits(file,b,b_header,ok)
          if (.not.ok) goto 901
          write(25,'(a)') file(1:lc(file))
C Start new average image.
          do i=1,nc*nr
            b(i)=a(i)
          enddo
          num=1
          b_header= header
          b_object= chead('OBJECT',b_header)
          frameno = inhead('FRAMENO',b_header)
          write(c4,'(i4)') frameno
          write(b_object,'(2a)') '<',c4(fc(c4):lc(c4))
C Save values.
          b_echangl = echangl
          b_xdangl  = xdangl
          b_decker  = decker
          b_lampname= lampname
          b_ot      = ot
          b_xbin    = xbin
          b_ybin    = ybin
        endif
      endif
      ENDDO

C Write out last group.
      b_object = b_object(1:lc(b_object))//'> (average)'
      call cheadset('OBJECT',b_object,b_header)
      flatcount=flatcount+1
      write(file,'(a,i3.3,a)') 'flat',flatcount,'.fits'
      write(soun,'(4a)') 
     .  '   Writing: ',file(1:lc(file)),' : ',b_object(1:lc(b_object))
      call writefits(file,b,b_header,ok)
      if (.not.ok) goto 901
      write(25,'(a)') file(1:lc(file))

      ok=.true.
      close(25)   ! flat.list file.
      return

901   continue
      write(soun,'(2a)') 
     .  'ERROR: hires_flatave: Reading file: ',file(1:lc(file))
      ok=.false.
      close(25)   ! flat.list file.
      return
      end


C----------------------------------------------------------------------
C
      subroutine flatinfo(a,sc,ec,sr,er,
     .   header,echangl,xdangl,decker,lampname,ot,xbin,ybin,
     .   high,saturation,ok)
C
      implicit none
      integer*4 sc,ec,sr,er       ! dimensions (input).
      real*4 a(sc:ec,sr:er)       ! data image (input).
      character*(*) header        ! FITS image header (input).
      real*4 echangl,xdangl       ! echelle and cross disperser angles (output).
      character*(*) decker        ! Decker name (output).
      character*(*) lampname      ! Lamp name (output).
      real*8 ot                   ! Obs. Time in seconds (output).
      integer*4 xbin,ybin         ! X and Y binning factors (output).
      real*4 high                 ! high colum level (input).
      real*4 saturation           ! saturation level in DN (input).
      logical ok                  ! Is this a good flat field image? (output).
C
      include 'soun.inc'
C
      real*8 fhead
      character*80 chead,wrd
      real*4 arr(4100),hi,median
      integer*4 narr,k,row,col,c1,c2,lc
C
C Header values.
      lampname= chead('LAMPNAME',header)
      decker  = chead('DECKNAME',header)
      echangl = sngl(fhead('ECHANGL',header))
      xdangl  = sngl(fhead('XDANGL',header))
C Echo.
      write(soun,'(a,2f10.4,2x,a,2x,a)') '           : ',
     .  echangl,xdangl,decker(1:lc(decker)),lampname(1:lc(lampname))
C Only allow quartz lamps.
      wrd=lampname
      call lower_case(wrd)
      if (index(wrd,'quar').eq.0) then
        ok=.false.
        return
      endif
C Exclude pinhole decker.
      if (index(decker,'D5').gt.0) then
        ok=.false.
        return
      endif
C Observation time in seconds since 1/1/1970.
      call obs_time_seconds(header,ot)
C Binning factor.
      call GetBinning(header,xbin,ybin)
C
C Exclude saturated images.
      if (high.gt.0.) then
        if (high.gt.saturation) then
          ok=.false.
          return
        endif
      else
C Check for saturation in two places.
        do k=1,2
          hi=0.
          c1 = (k*(sc+ec)/3) - 8
          c2 = (k*(sc+ec)/3) + 8
          do row=sr,er
            narr=0
            do col=c1,c2
              narr=narr+1
              arr(narr)=a(col,row)
            enddo
            call find_median(arr,narr,median)
            if (median.gt.hi) hi=median
          enddo
          write(soun,'(a,i1,a,f11.1)') 
     .  '       Quartz high value #',k,' =',hi
          if (hi.gt.saturation) then
            ok=.false.
            return
          endif
        enddo
      endif
C
      ok=.true.
      return
      end


C----------------------------------------------------------------------
C Get obs. time in seconds since 1/1/1970.
C
      subroutine obs_time_seconds(header,ot)
C
      implicit none
      character*(*) header   ! FITS header (input).
      real*8 ot              ! Time in seconds since 1/1/1970 (output).
      real*4 valread
      integer*4 yy,mm,dd,hh,nn,ss
      character*80 wrd,chead,s
      integer*4 i,j,k,year4
C
      include 'soun.inc'
C
C Initial.
      yy=0
      mm=0
      dd=0
      hh=0
      nn=0
      ss=0
C Date.
      call ReadFitsDate('DATE-OBS',header,year4,yy,mm,dd)
C Check.
      if (yy.eq.0) then
        write(soun,'(a)')
     .  'ERROR:reading year in DATE-OBS header card. Use 1980.'
        yy=80
      endif
C Time.
      wrd = chead('UT',header)
      i = index(wrd,':')
      if (i.gt.0) then
        hh= nint(valread(wrd(1:i-1)))
        wrd(i:i)=' '
        j = index(wrd,':')
        if (j.gt.0) then
          nn= nint(valread(wrd(i+1:j-1)))
          ss= nint(valread(wrd(j+1:j+4)))
        endif
      endif

C Construct string.
      write(s,'(3(i2.2,a),3(i2.2,a))') 
     .  yy,'/',mm,'/',dd,' ',hh,':',nn,':',ss,' '

C Get seconds.
      call convert_time_string(s,k,1)
      ot = dfloat(k)

      return
      end


C----------------------------------------------------------------------
C Find usable column range for a given order.
C
      subroutine EE_order_column_range(bk1o,bk2o,sc,ec,sr,er,usc,uec,ok)
C
C bk1o(*)     = defines the top boundary for the background or sky.
C bk2o(*)     = defines the bottom boundary for sky.
C
      implicit none
      real*8 bk1o(*),bk2o(*)   ! polynomials
      integer*4 sc,ec,sr,er    ! dimensions of whole image.
      integer*4 usc,uec        ! usable starting column and ending column.
      logical ok               ! no problems?
C
      include 'global.inc'
      include 'soun.inc'
C
      real*8 refcol,rr,polyarroff
      integer*4 back1,back2
      logical again
C
C Find a portion of the spectrum with the slit completely on the image.
      ok    = .true.
      refcol= dfloat(ec-sc)/2.
      again = .true.
      usc   = 0
      uec   = 0
      rr = 1.
      do while (again)
        if ((nint(refcol+rr).ge.ec).or.(nint(refcol-rr).le.sc)) then
          ok=.false.
          return
        endif
C Looking right.
        back1 = nint( polyarroff(bk1o,(refcol+rr)) )
        back2 = nint( polyarroff(bk2o,(refcol+rr)) )
        if ( (back1.gt.sr).and.(back1.lt.er).and.
     .       (back2.gt.sr).and.(back2.lt.er) ) then
          refcol = refcol + rr
          again=.false. 
        endif
C Looking left.
        back1 = nint( polyarroff(bk1o,(refcol-rr)) )
        back2 = nint( polyarroff(bk2o,(refcol-rr)) )
        if ( (back1.gt.sr).and.(back1.lt.er).and.
     .       (back2.gt.sr).and.(back2.lt.er) ) then
          refcol = refcol - rr
          again=.false. 
        endif
        rr=rr+1.
      enddo  
C
C Determine usuable starting column for scratch images from "refcol".
      usc=nint(refcol)
      again=.true.
      do while ( (usc.ge.sc).and.(again) )
        back1 = nint( polyarroff(bk1o,dfloat(usc)) )
        back2 = nint( polyarroff(bk2o,dfloat(usc)) )
        if ( (back1.gt.sr).and.(back1.lt.er).and.
     .       (back2.gt.sr).and.(back2.lt.er) ) then
          usc = usc - 1
        else
          again=.false. 
        endif
      enddo
      usc = usc + 1
C
C Determine usuable ending column for scratch images.
      uec=nint(refcol)
      again=.true.
      do while ( (uec.le.ec).and.(again) )
        back1 = nint( polyarroff(bk1o,dfloat(uec)) )
        back2 = nint( polyarroff(bk2o,dfloat(uec)) )
        if ( (back1.gt.sr).and.(back1.lt.er).and.
     .       (back2.gt.sr).and.(back2.lt.er) ) then
          uec = uec + 1
        else
          again=.false. 
        endif
      enddo
      uec = uec - 1
C
      ok    = .true.
      return
      end

