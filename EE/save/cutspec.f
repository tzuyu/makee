C cutspec.f                                              tab  jun95
C
C Cut a segment out of a 1-D spectrum in rebinned format.
C
      implicit none
      integer*4 narg,i,lc,naxis1,inhead,nc
      character*57600 header,newhead
      character*80 arg(99),outfile,wrd,wrds(9)
      character*80 chead,infile
      integer*4 mx,my,maxpt
      parameter(mx=19000,my=99,maxpt=mx*my)
      real*4 a(maxpt)
      real*8 bluelimit,redlimit,bigpix1,bigpix2
      real*8 valread8,wave,crval1,cdelt1
      real*8 nwp,getx,fhead,aa,bb,disp
      logical ok,findarg,patch,loglin

C Get command line parameters and set defaults.
      call arguments(arg,narg,99)
      patch  = findarg(arg,narg,'-patch',':',wrd,i)
      outfile=' '
      if (findarg(arg,narg,'of=',':',wrd,i)) then
        outfile=wrd
        call AddFitsExt(outfile)
      endif
      bluelimit= 0.d0
      redlimit = 0.d0
      if (findarg(arg,narg,'wr=',':',wrds,i)) then
        if (i.eq.2) then
          bluelimit= valread8(wrds(1))
          redlimit = valread8(wrds(2))
        endif
        if ((i.ne.2).or.(bluelimit.lt.3000.).or.
     .                        (redlimit.lt.3000.)) then
          print *,'Error with wr= option.'
          call exit(1)
        endif
      endif

C If zero parameters or improper parameters, tell user syntax. 
      if ((narg.ne.1).or.(bluelimit.lt.1.e-30).or.(redlimit.lt.1.e-30)
     .       .or.(bluelimit.ge.redlimit).or.(outfile.eq.' ')) then
        print *,
     . 'Syntax: cutspec (1-D FITS spectrum file)  of=(output filename)'
        print *,'                   wr=bluewave:redwave  [-patch]'
        print *,' '
        print *,
     .  '  Cuts a segment out of 1-D FITS spectrum file given by'
        print *,'  the wr= parameter.'
        print *,' '
        print *,
     .  ' -patch : Interpolate over zero valued (masked) points.'
        print *,' '
        call exit(0)
      endif

C Set input filename, read data.
      infile = arg(1)
      call AddFitsExt(infile)
      print '(2a)',' Reading: ',infile(1:lc(infile))
      call readfits(infile,a,maxpt,header,ok)
      if (.not.ok) then
        print '(2a)',' Error reading : ',infile(1:lc(infile))
        call exit(1)
      endif

C Check dimensions.
      i = max(1,inhead('NAXIS2',header))
      if (i.gt.1) then
        print *,'ERROR- Must be a one-dimensional FITS image.'
        call exit(1)
      endif

C Check wavelength scale.
      wrd = chead('CTYPE1',header)
      call upper_case(wrd)
      if ((index(wrd,'LINEAR').eq.0).and.
     .                (index(wrd,'LAMBDA').eq.0)) then
        print *,'ERROR- Wavelength scale must be linear or log-linear.'
        call exit(1)
      endif

C Initialize wavelength scale.
      wave  = GetX(-1.d0,header)
      loglin= (inhead('DC-FLAG',header).eq.1)
      nc    = inhead('NAXIS1',header)

C Starting and ending pixel of new big spectrum.
      nwp=1.d0
      if (bluelimit.gt.GetX(1.d0,header)) then
        wave= GetX(nwp,header)
        do while(wave.lt.bluelimit)
          nwp = nwp+1.d0
          wave= GetX(nwp,header)
        enddo
      endif
      bigpix1 = nwp
      if (redlimit.lt.GetX(dfloat(nc),header)) then
        nwp = 1.d0
        wave= GetX(nwp,header)
        do while(wave.lt.redlimit)
          nwp = nwp+1.d0
          wave= GetX(nwp,header)
        enddo
        bigpix2 = nwp-1.d0
      else
        nwp = dfloat(nc)
        bigpix2 = nwp
      endif
      print '(a,2f11.4)',' Center of first and last pixels : ',
     .                   GetX(bigpix1,header),GetX(bigpix2,header)

C Set wavelength scale parameters.
      if (loglin) then
        aa  = fhead('CRVAL1',header)
        bb  = fhead('CDELT1',header)
        disp= 0.
      else
        aa  = 0.
        bb  = 0.
        disp= fhead('CDELT1',header)
      endif

C Set header card values.
      if (loglin) then
        crval1 = aa + (bb*(bigpix1-1.d0))
        cdelt1 = bb
        naxis1 = 1 + nint(bigpix2-bigpix1)
      else
        crval1 = GetX(bigpix1,header)
        cdelt1 = disp
        naxis1 = 1 + nint(bigpix2-bigpix1)
      endif
C Add new cards to new header.
      newhead = header
      call fheadset('CRVAL1',crval1,newhead)
      call fheadset('CDELT1',cdelt1,newhead)
      call cheadset('CTYPE1','LAMBDA',newhead)
      if (loglin) call inheadset('DC-FLAG',1,newhead)
      call inheadset('NAXIS1',naxis1,newhead)
      call inheadset('CRPIX1',1,newhead)
      call inheadset('NAXIS',1,newhead)
      call unfit('NAXIS2',newhead)
      call unfit('CRVAL2',newhead)
      call unfit('CDELT2',newhead)
      call unfit('CRPIX2',newhead)
      call cheadset('FILENAME',outfile,newhead)
      if (loglin) then
        call inheadset('WCSDIM',1,newhead)
        call cheadset('CTYPE1','LINEAR',newhead)
        call fheadset('CD1_1',cdelt1,newhead)
        call inheadset('LTM1_1',1,newhead)
        call cheadset('WAT0_001','system=equispec',newhead)
C Put in blank spaces for IRAF.
        i = index(newhead,'=equispec')
        newhead(i+9:i+9)=' '
        newhead(i+62:i+62)=char(39)
        call cheadset('WAT1_001',
     .       'wtype=linear label=Wavelength units=Angstroms',newhead)
C Put in blank spaces for IRAF.
        i = index(newhead,'units=Angstroms')
        newhead(i+15:i+15)=' '
        newhead(i+38:i+38)=char(39)
        call inheadset('DC-FLAG',1,newhead)
        call cheadset('APNUM1','1 1 1.0 1.0',newhead)
      endif
C Patch up zero pixel spots if requested.
      if (patch) call interp_zero_pts(1,nint(nwp),a)
C Write out spectrum.
      print '(2a)',' Writing: ',outfile(1:lc(outfile))
      call writefits(outfile,a(nint(bigpix1)),newhead,ok)
      if (.not.ok) goto 860
      print *,'All done.'
      stop
800   continue
      print *,'Error reading WV header card.'
      stop
850   print *,'Error- filename must have .fits extension.'
      stop
860   print *,'Error- reading or writing FITS file.'
      stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
C Interpolate zero points.
      subroutine interp_zero_pts(sc,ec,a)
      implicit none
      integer*4 sc,ec
      real*4 a(sc:ec),sum,b(90000)
      integer*4 i,num1,num2,i1,i2
C Copy to temporary array.
        do i=sc,ec
          b(i)=a(i)
        enddo
C Search for a zero pixel.
        i1=sc
        do while(i1.le.ec)
          if (abs(b(i1)).lt.1.e-30) then
C Find the extent of the region of zero pixels.
            i2=i1+1
            do while((i2.le.ec).and.(abs(b(i2)).lt.1.e-30))
              i2=i2+1
            enddo
            i2=i2-1
C Find a mean value for local valid pixels.
            sum=0.
C Look for valid pixels to the left of region.
            i=i1-1
            num1=0
            do while((i.ge.sc).and.(num1.lt.10))
              if (abs(b(i)).gt.1.e-30) then
                num1=num1+1
                sum =sum +b(i)
              endif
              i=i-1
            enddo
C Look for valid pixels to the right of region.
            i=i2+1
            num2=0
            do while((i.le.ec).and.(num2.lt.10))
              if (abs(b(i)).gt.1.e-30) then
                num2=num2+1
                sum =sum +b(i)
              endif
              i=i+1
            enddo
            sum = sum / float(max(1,num1+num2))
C Fill in zero range.
            do i=i1,i2
              a(i)=sum
            enddo
          else
            i2=i1
          endif
          i1=i2+1
        enddo

      return
      end
