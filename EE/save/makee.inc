C This file is makee.inc

C:::::::::::::::::::::::::
C Version of echelle extraction software "ee" (core routines).
      character*40 EE_VERSION
      parameter(EE_VERSION='3.4.1 : April 2001')

C:::::::::::::::::::::::::
C Version of makee package (all delivered programs).
      character*40 MK_VERSION
      parameter(MK_VERSION='3.4.0 : April 2001')



C###
      real*4 SkySlope
      parameter(SkySlope = 0.01)
C###


C:::::::::::::::::::::::::
C Semi-fixed parameters.  See "makee.param" for descriptions.
C
C Suggested values:
C     parameter(EETO_PolyOrder=4)
C     parameter(EEFO_ObjectFraction=0.98)
C     parameter(EEFO_PolyOffset=3)
C     parameter(EEFB_Compensate=1.0)
C     parameter(EEFB_OverlapCompensate=1.5)

      character*80 EE_MaskFile
      integer*4 EETO_PolyOrder, EEFO_PolyOffset
      real*4 EEFO_ObjectFraction, EEFB_Compensate
      real*4 EEFB_OverlapCompensate

      common / EEPARMBLK / EE_MaskFile,EETO_PolyOrder,EEFO_PolyOffset,
     .   EEFO_ObjectFraction, EEFB_Compensate, EEFB_OverlapCompensate


C:::::::::::::::::::::::::
C EE common blocks.
C Set maximum echelle orders, maximum polynomial parameters, and maximum
C pixels in either direction.  Set real*4 and real*8 scratch arrays.
C
      integer*4 mpp,mo,MK_mpv
      parameter(mpp=99,mo=99,MK_mpv=5000)
C
      character*57600 ObjectHeader    ! Current Object Header.
C
      character*80 uopfile            ! User Object Position file.
C
      character*8 Current_ObsDate
      real*8 eo(0:mpp,mo)
      real*8 bk1o(0:mpp,mo),bk2o(0:mpp,mo)
      real*8 bk1i(0:mpp,mo),bk2i(0:mpp,mo)
      real*8 sp1(0:mpp,mo),sp2(0:mpp,mo)
C
      real*4 fltmn(mo),pval(MK_mpv,mo),pparm(5,mo)
      real*4 UserHalfWidth,eeus_saturate
      integer*4 Star_Obsnum,arcmode
C
      logical oko(mo)
      logical trace

C User specified object center and positions: "User Object Position" (uop_).
      real*4 uop_c(mo)      ! center of object in an order
      real*4 uop_h(mo)      ! center of object in an order
      real*4 uop_b1o(mo)    ! background limit
      real*4 uop_b1i(mo)    ! background limit
      real*4 uop_b2i(mo)    ! background limit
      real*4 uop_b2o(mo)    ! background limit
      logical uop_exist     ! Have user object positions been specified?
C
      common / EEBLK57600 / ObjectHeader
C
      common / EEBLK80 / uopfile
C
      common / EEBLK8 / Current_ObsDate,
     .                  eo,bk1o,bk2o,bk1i,bk2i,sp1,sp2
C
      common / EEBLK4 / fltmn,pval,pparm,
     .                  UserHalfWidth,eeus_saturate,
     .                  Star_Obsnum,arcmode,uop_c,uop_h,
     .                  uop_b1o,uop_b1i,uop_b2i,uop_b2o
C
      common / EEBLK1 / oko,trace,uop_exist


C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C mpp          = maximum polynomial parameters (coeffecients)
C mo           = maximum number of echelle orders
C MK_mpv       = maximum number of profile values
C eo           = echelle order polynomials from bright star
C fltmn        = flat means for each order
C sp1,sp2      = spectrum defining polynomials
C bk1o,bk2o    = outer limits of background defining polynomials
C bk1i,bk2i    = inner limits of background defining polynomials
C pval         = profile model values
C pparm        = parameters for profile model values
C arcmode      = Mode for averaging together arclamp wavelength scales.
C UserHalfWidth  = User requested object half width in pixels.
C Current_ObsDate= String to mark PostScript plots.
C oko          = OK orders (for trace offsets computation).
C Star_Obsnum  = Current observation number for bright star used for trace
C trace        = general testing
C eeus_saturate= Saturation level in counts (default 62000.)
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C      Other notes:
C pparm(1,eon) = number of values in profile model
C pparm(2,eon) = starting x value
C pparm(3,eon) = delta x value (x increment between array elements)
C pparm(4,eon) = centroid.
C pparm(5,eon) = background level of profile.
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
