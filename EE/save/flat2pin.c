/* flat2pin.c                                   tab  oct01 */

#include "ctab.h"
#include "cmisc.h"
#include "fstuff.h"
#include "cufio.h"



/* ----------------------------------------------------------------------
  Create median column within radius.
*/
void f2p_cmc( float aa[], int nc, int nr, int col, int radius, float bb[] )
{
/**/
int col1,col2,ii,jj,pixno,narr;
float arr[90];
/**/

/* Create median column array. */
col1 = col - radius; if (col1 < 0) col1=0;
col2 = col + radius; if (col2 >= nc) col1=nc-1;
for (jj=0; jj<nr; ++jj) {
  narr=0;
  for (ii=col1; ii<=col2; ++ii) {
    pixno = ii + (jj * nc);
    arr[narr] = aa[pixno];
    ++narr;
  }
  bb[jj] = cfind_median( narr, arr );
}
return;
}



/* ----------------------------------------------------------------------
  Are the pixels from "row" to "row + slitw" consistent with a slit portion?
  If ok, return *ndev = 0;
*/
void f2p_check( float bb[], int nr, int row, int slitw, float *median, 
       int *ndev, float limit, int nlimrr, float limrr[100][3], float eperdn )
{
/**/
int ii,jj,narr;
float arr[900],erms,arms,sum;
float rr,uselimit;
/**/

/* Assign proper limit for this row. */
uselimit = limit;  /* default */
rr = row;
for (ii=0; ii<nlimrr; ++ii) {
  if ((rr >= limrr[ii][1])&&(rr <= limrr[ii][2])) { uselimit = limrr[ii][0]; }
}

/* Find median. */
narr=0;
for (jj=row; jj<=(row + slitw); ++jj) {
  if (jj < nr) {
    arr[narr] = bb[jj];
    ++narr;
  }
}
*median = cfind_median( narr, arr );

/* Estimate of RMS. */
erms = sqrt( *median / eperdn );

/* Actual RMS from median. */
sum=0.;
for (jj=row; jj<=(row + slitw); ++jj) {
  sum = sum + ( (bb[jj] - *median) * (bb[jj] - *median) );
}
arms = sqrt(( (sum) / ((float)narr) ));

/*
printf("#@#  row=%d  median=%12.3f  erms=%12.3f  arms=%12.3f\n",
             row,*median,erms,arms);
cpauseit();
*/

if (arms < (uselimit * erms)) { *ndev = 0; } else { *ndev = 100; }

return;
}




/* ----------------------------------------------------------------------
  Do statistics in current column at current row and ahead "ahead" rows.
*/
void f2p_stats( float bb[], int nr, int row, int ahead,
                float *median, int *ndev )
{
/**/
int jj,narr;
float sum,arr[900],rms,sigs;
/**/

/* Check. */
if (ahead > 100) {
  fprintf(stderr,"***ERROR: f2p_stats: ahead > 100... should not happen.\n");
  exit(0);
}
/* Find median. */
narr=0;
for (jj=row; jj<=(row+ahead); ++jj) {
  if (jj < nr) {
    arr[narr] = bb[jj];
    ++narr;
  }
}
*median = cfind_median( narr, arr );

/* Find RMS from median. */
sum=0.;
for (jj=row; jj<=(row+ahead); ++jj) {
  sum = sum + ( (bb[jj] - *median) * (bb[jj] - *median) );
}
rms = sqrt(( (sum) / ((float)narr) ));
/* Find number of points that deviate more than 4 sigma from RMS */
*ndev= 0;
for (jj=row; jj<=(row+ahead); ++jj) {
  sigs = ABS(( bb[jj] - *median )) / rms;
  if (sigs > 2.55) { *ndev = *ndev + 1; }
}
return;
}



/* ----------------------------------------------------------------------
  Do statistics in current column at current row and ahead "ahead" rows.
  This version checks out "nleadpix" pixels just ahead of "ahead" row.
  If *ndev equals nleadpix, then we have reached the edge of the slit.
*/
void f2p_stats2( float bb[], int nr, int row, int ahead,
                 int nleadpix, float *median, int *ndev )
{
/**/
int jj,narr;
float sum,arr[900],rms,sigs;
/**/

/* Check. */
if (ahead > 100) {
  fprintf(stderr,"***ERROR: f2p_stats2: ahead > 100... should not happen.\n");
  exit(0);
}
/* Find median. */
narr=0;
for (jj=row; jj<=(row+ahead); ++jj) {
  if (jj < nr) { arr[narr] = bb[jj]; ++narr; }
}
*median = cfind_median( narr, arr );
/* Find RMS from median. */
sum=0.;
for (jj=row; jj<=(row+ahead); ++jj) {
  sum = sum + ( (bb[jj] - *median) * (bb[jj] - *median) );
}
rms = sqrt(( (sum) / ((float)narr) ));

/* Check out "nleadpix" points beyond "ahead" row. */
*ndev= 0;
for (jj=(row+ahead+1); jj<=(row+ahead+nleadpix); ++jj) {
  if (jj < nr) {
    sigs = ABS(( bb[jj] - *median )) / rms;
    if (sigs > 2.55) { *ndev = *ndev + 1; }
  } else {
    *ndev = nleadpix;  /* stop if at edge */
  }
}

return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Collapse a flat image to a pinhole-type image.
*/
int main(int argc, char *argv[])
{
/**/
char arg[100][100];
char flatfile[100];
char pinfile[100];
char wrd[100];
/**/
const int headersize = 57600;
char header[ headersize ];
/**/
int ii,jj,narg,col1,col2,ndev,col,slitw,row2,row,narr;
int pixno,sc,ec,sr,er,nc,nr,frameno,testmark;
int nom_slitw,max_slitw,nlimrr,only_column;
/**/
float limrr[100][3];
/**/
const int maxaa = 4100 * 4100;
const int maxbb = 9000;
const int maxarr = maxaa / 10;
float *aa;
float *bb;
float *cc;
float *arr;
/**/
float limit, eperdn, median, back_cutoff, back_level;
/**/
/*
FILE *outfu;
*/

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Option. */

back_cutoff = 4003.;
if (cfindarg(arg,&narg,"cutoff=",':',wrd) == 1) back_cutoff = GLV(wrd,1);

testmark = 0;
if (cfindarg(arg,&narg,"-test",'s',wrd) == 1) testmark = 1;

frameno = -1;
if (cfindarg(arg,&narg,"frameno=",':',wrd) == 1) frameno = GLV(wrd,1);

nom_slitw = 30;
if (cfindarg(arg,&narg,"slit=",':',wrd) == 1) nom_slitw = GLV(wrd,1);

max_slitw = 80;
if (cfindarg(arg,&narg,"max_slit=",':',wrd) == 1) max_slitw = GLV(wrd,1);

limit = 6.0;
if (cfindarg(arg,&narg,"limit=",':',wrd) == 1) limit = GLV(wrd,1);

only_column = -1;
if (cfindarg(arg,&narg,"only=",':',wrd) == 1) only_column = GLV(wrd,1);

nlimrr = 0;
while( cfindarg(arg,&narg,"limrr=",':',wrd) == 1 ) {
  limrr[nlimrr][0] = GLV(wrd,1);
  limrr[nlimrr][1] = GLV(wrd,2);
  limrr[nlimrr][2] = GLV(wrd,3);
  ++nlimrr;
}

eperdn = 2.4;
if (cfindarg(arg,&narg,"eperdn=",':',wrd) == 1) eperdn = GLV(wrd,1);


/* Syntax. */
if ( narg != 2 ) {
  printf("Syntax: flat2pin (flat file) (pinhole-type file)\n");
  printf("         [frameno=]  [slit=]  [max_slit=]  [eperdn=]  [limit=]\n");
  printf("         [limrr=(limit),(row1),(row2)] [ limrr= ... ]\n");
  printf("         [only=]  [-test]  [cutoff=]\n");
  printf("\n");
  printf("  Collapse a flat image to a pinhole-type image.\n");
  printf("\n");
  printf("  Options:\n");
  printf("    frameno=  : change the FRAMENO and OBSNUM header card.\n");
  printf("    slit=     : nominal (minimum) slit width (def: 30).\n");
  printf("    max_slit= : maximum slit width (def: 80).\n");
  printf("    eperdn=   : electrons per digital number (def: 2.4)\n");
  printf("    limit=    : RMS ratio limit (actual to theory) (def: 6.0)\n");
  printf("    limrr=    : Assign specify row ranges to a given ratio limit.\n");
  printf("    only=     : Only do this column.\n");
  printf("    -test     : Set test marks on pinhole result image.\n");
  printf("    cutoff=   : Background cutoff level (def: 2 * backgnd median)\n");
  printf("\n");
  exit(0);
}

/* Allocate memory. */
aa = (float *)calloc(maxaa ,sizeof(float));
arr= (float *)calloc(maxarr,sizeof(float));
bb = (float *)calloc(maxbb ,sizeof(float));
cc = (float *)calloc(maxbb ,sizeof(float));

/* Filename. */
strcpy(flatfile,arg[1]);
strcpy(pinfile,arg[2]);

/* Echo */
printf("nom_slitw= %d \n",nom_slitw);
printf("max_slitw= %d \n",max_slitw);
printf("limit    = %6.2f \n",limit);
printf("nlimrr   = %d \n",nlimrr);
for (ii=0; ii<nlimrr; ++ii) {
  printf("%4d:%7.2f: %7.1f %7.1f\n",ii,limrr[ii][0],limrr[ii][1],limrr[ii][2]);
}

/* Read FITS image. */
printf("Reading %s .\n",flatfile);
creadfits( flatfile, aa, maxaa, header, headersize );
cGetDimensions(header,headersize,&sc,&ec,&sr,&er,&nc,&nr);
printf("Note: nc=%d  nr=%d\n",nc,nr);


/* Find background level. */
col1 = 0.1 * nc;
col2 = 0.9 * nc;
narr = 0;
for (col=col1; col<col2; col=col+10) {
  for (row=0; row<nr; ++row) {
    pixno = col + (row * nc);
    if (aa[pixno] < back_cutoff) {
      arr[narr] = aa[pixno];
      ++narr;
    }
  }
}
back_level  = 1.1 * cfind_median( narr, arr );
if (cnint((back_cutoff)) == 4003) {
  back_cutoff = 2.0 * back_level; 
} 
printf("Background level  is %11.3f\n",back_level);
printf("Background cutoff is %11.3f\n",back_cutoff);

/* Look at image column by column. */
col1=0; col2=nc-1;
if (only_column > -1) { 
  col1=only_column; 
  col2=only_column;
}
printf("Note: Looking at column range %d to %d .\n",col1,col2);
for (col=col1; col<=col2; ++col) {

  if (col%100 == 0) { printf("#@# col=%d\n",col); }

/* Create a "median" column using 11 adjacent columns. */
  f2p_cmc( aa, nc, nr, col, 5, bb );
  for(jj=0; jj<nr; ++jj) { cc[jj] = 0.; } /* Clear */


/* #@#
  printf("writing bb.xy ..............\n");
  outfu = fopen("bb.xy","w");
  for (jj=0; jj<nr; ++jj) { fprintf(outfu,"%d %12.2f\n",jj,bb[jj]); }
  fclose(outfu);
   #@# */

  
  row = 0;
  while (row < nr) {

/* Look at this pixel-- Is it within a slit? */
    if (bb[row] > back_cutoff) {

/* Look ahead at next "slitw" pixels-- all within a reasonable RMS? */
      slitw = nom_slitw;
      f2p_check( bb, nr, row, slitw, &median, &ndev,limit,nlimrr,limrr,eperdn);

      if ((ndev < 2)&&(median > back_cutoff)) {    /* within slit! */

/*
        printf("#@# within slit: row=%d  median=%12.3f\n",row,median);
*/

/* How far does slit extend? */
        row2 = row;
        slitw = nom_slitw;
        while((ndev < 2)&&((row2 + slitw) < nr)&&((row2 - row) <= max_slitw)){ 
          f2p_check(bb,nr,row2,slitw,&median,&ndev,limit,nlimrr,limrr,eperdn);
          row2 = row2 + 1;
        }

/* Upper row limit. */
        row2 = row2 + slitw - 1;

/* Record slit. */
        cc[row] = median;
        cc[row2]= 150000.;

/*
        printf("#@# FOUND slit: row=%d to row2=%d\n",row,row2);
        cpauseit();
*/

/* Set pinhole up. */
        if (testmark == 1) {
          pixno = col + ( row  * nc); aa[pixno] = 100000.;
          pixno = col + ( row2 * nc); aa[pixno] = 100000.;
          jj = cnint(( ((float)row + (float)(row2) ) / 2. ));
          pixno = col + ( jj    * nc); aa[pixno] = aa[pixno] + 20000.;
          pixno = col + ((jj-1) * nc); aa[pixno] = aa[pixno] + 18000.;
          pixno = col + ((jj+1) * nc); aa[pixno] = aa[pixno] + 18000.;
          pixno = col + ((jj-2) * nc); aa[pixno] = aa[pixno] + 15000.;
          pixno = col + ((jj+2) * nc); aa[pixno] = aa[pixno] + 15000.;
        } else {
          jj = cnint(( ((float)row + (float)(row2) ) / 2. ));
          pixno = col + ( jj    * nc); aa[pixno] = aa[pixno] + 200000.;
          pixno = col + ((jj-1) * nc); aa[pixno] = aa[pixno] + 180000.;
          pixno = col + ((jj+1) * nc); aa[pixno] = aa[pixno] + 180000.;
          pixno = col + ((jj-2) * nc); aa[pixno] = aa[pixno] + 150000.;
          pixno = col + ((jj+2) * nc); aa[pixno] = aa[pixno] + 150000.;
        }

/* jump past this slit. */
        row = row2 + 1;

      }
    }
    ++row;
  }


/* #@#
  printf("writing cc.xy ..............\n");
  outfu = fopen("cc.xy","w");
  for (jj=0; jj<nr; ++jj) { fprintf(outfu,"%d %12.2f\n",jj,cc[jj]); }
  fclose(outfu);
   #@# */


}


/* Add frame number. */
if (frameno > -1) { 
  cinheadset("FRAMENO",frameno,header,headersize);
  cinheadset("OBSNUM" ,frameno,header,headersize);
}

/* Lamp name. */
ccheadset("LAMPNAME" ,"none",header,headersize);

/* Clean up aa image. */
if (testmark == 0) {
  for (ii=0; ii<(nc*nr); ++ii) {
    if (aa[ii] < 100000.) { aa[ii]=1000.; } else { aa[ii]=aa[ii]-100000.; }
  }
}

/* Write out result image. */
printf("Writing %s .\n",pinfile);
cwritefits( pinfile, aa, header, headersize );

return(0);
}
