C imgplot.f
C
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C
C Plot a row, column, range of rows or columns with an average, sum, or median.
C
      implicit none
C
      include 'maxpix.inc'
C
      real*4 a(maxpix)
      integer*4 nc,nr,sc,sr,er,ec,narg,i
      integer*4 col,row,cols(2),rows(2)
      character header*57600
      logical ok
      character*80 arg(9),file,wrd,wrds(9),psfile
      real*4 valread,xs,xe,ys,ye
      logical findarg,dosum,domdn,hist,dorms

      call arguments(arg,narg,9)
      row= -9999
      if (findarg(arg,narg,'row=',',',wrd,i)) row=nint(valread(wrd))
      col= -9999
      if (findarg(arg,narg,'col=',',',wrd,i)) col=nint(valread(wrd))
      rows(1)= -9999
      if (findarg(arg,narg,'rows=',',',wrds,i)) then
        if (i.eq.2) then
          rows(1) = nint(valread(wrds(1)))
          rows(2) = nint(valread(wrds(2)))
        endif
      endif
      cols(1)= -9999
      if (findarg(arg,narg,'cols=',',',wrds,i)) then
        if (i.eq.2) then
          cols(1) = nint(valread(wrds(1)))
          cols(2) = nint(valread(wrds(2)))
        endif
      endif
      hist  = findarg(arg,narg,'-hist',':',wrd,i)
      dosum = findarg(arg,narg,'-sum',':',wrd,i)
      domdn = findarg(arg,narg,'-mdn',':',wrd,i)
      dorms = findarg(arg,narg,'-rms',':',wrd,i)
      psfile=' '
      if (findarg(arg,narg,'ps=',':',wrd,i)) psfile=wrd
C Defaults.
      xs=-9.e+30
      xe=-9.e+30
      ys=-9.e+30
      ye=-9.e+30
      if (findarg(arg,narg,'xs=',',',wrd,i)) xs = valread(wrd)
      if (findarg(arg,narg,'xe=',',',wrd,i)) xe = valread(wrd)
      if (findarg(arg,narg,'ys=',',',wrd,i)) ys = valread(wrd)
      if (findarg(arg,narg,'ye=',',',wrd,i)) ye = valread(wrd)
      if (narg.ne.1) then
        print *,'Syntax: imgplot (FITS file)'
        print *,' [row=n] [col=n] [rows=m,n] [cols=m,n] [-hist] [-sum]'
        print *,' [-mdn] [-rms] [ps=] [xs=v] [xe=v] [ys=v] [ye=v]'
        print *,'Plot part of image. OPTIONS:'
        print *,' xs=v  : define minimum x on plot.'
        print *,' xe=v  : define maximum x on plot.'
        print *,' ys=v  : define minimum y on plot.'
        print *,' ye=v  : define maximum y on plot.'
        print *,' row=n : plot row n.'
        print *,' col=n : plot column n.'
        print *,
     . ' rows=m,n : plot an average, sum, or median of rows m to n.'
        print *,
     . ' cols=m,n : plot an average, sum, or median of columns m to n.'
        print *,' -hist : do a histogram plot.'
        print *,' -sum  : plot a sum when using rows= or cols= .'
        print *,' -mdn  : plot a median when using rows= or cols= . '
        print *,' -rms  : plot a rms when using rows= or cols= . '
        print *,' ps=   : produce a PostScript plot file.'
        call exit(0)
      endif
      file = arg(1)
      call AddFitsExt(file)

C Read fits file...
      call readfits(file,a,maxpix,header,ok)
      if (.not.ok) call exit(1)
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
      call fixnans(a,nc*nr,0.)
      call imgplot(a,sc,ec,sr,er,header,file,row,col,rows,cols,
     .            hist,dosum,domdn,dorms,psfile,xs,xe,ys,ye)

      stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
      subroutine imgplot(a,sc,ec,sr,er,header,file,row,col,rows,cols,
     .                  hist,dosum,domdn,dorms,psfile,xs,xe,ys,ye)
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er),xs,xe,ys,ye
      character*(*) header
      character*(*) file
      character*(*) psfile
      logical hist,dosum,domdn,dorms
      integer*4 row,col,rows(2),cols(2)
      real*4 x(9000),y(9000),xmin,xmax,ymin,ymax,yhi,ylo,arr(9000),sum
      character*80 c80,chead,object,wrd
      character c40*40
      integer*4 ii,lc,jj,i,j,n,num,narr
C Object name.
      object = chead('OBJECT',header)
C Fill array.
      n=0
      if (row.ne.-9999) then
        write(c40,'(a,i4)') 'ROW=',row
        if ((row.lt.sr).or.(row.gt.er)) then
          print *,'ERROR: imgplot: Row (',row,') beyond bounds: ',sr,er
          return
        endif
        row=max(sr,min(er,row))
        do i=sc,ec
          n=n+1 
          x(n)=float(i)
          y(n)=a(i,row)
        enddo
      elseif (col.ne.-9999) then
        write(c40,'(a,i4)') 'COL=',col
        if ((col.lt.sc).or.(col.gt.ec)) then
          print *,'Error- col beyond bounds: ',sc,ec
          return
        endif
        do j=sr,er
          n=n+1 
          x(n)=float(j)
          y(n)=a(col,j)
        enddo
      elseif (cols(1).ne.-9999) then
        write(c40,'(a,i4,a,i4)') 'COLS=',cols(1),'-',cols(2)
        if ( (cols(1).lt.sc).or.(cols(1).gt.ec).or.
     .       (cols(1).lt.sc).or.(cols(1).gt.ec) ) then
          print *,'Error- cols beyond bounds: ',sc,ec
          return
        endif
        if (domdn) then
          do j=sr,er
            narr=0
            do i=max(sc,min(ec,cols(1))),max(sc,min(ec,cols(2)))
              narr=narr+1
              arr(narr)=a(i,j)
            enddo
            n=n+1
            x(n)=float(j)
            call find_median(arr,narr,y(n))
          enddo
          c40=c40(1:lc(c40))//' (median)'
        else
          do j=sr,er
            sum=0.
            num=0
            do i=max(sc,min(ec,cols(1))),max(sc,min(ec,cols(2)))
              num=num+1
              sum=sum+a(i,j)
            enddo
            n=n+1
            x(n)=float(j)
            if (dosum) then
              y(n)=sum
            else
              y(n)=sum/float(max(1,num))
            endif
            if (dorms) then
              sum=0.
              do i=max(sc,min(ec,cols(1))),max(sc,min(ec,cols(2)))
                sum = sum + ((a(i,j)-y(n))*(a(i,j)-y(n)))
              enddo
              y(n) = sqrt( sum / float(max(1,num)) )
            endif
          enddo
          if (dosum) then
            c40=c40(1:lc(c40))//' (sum)'
          elseif (dorms) then
            c40=c40(1:lc(c40))//' (R.M.S.)'
          else
            c40=c40(1:lc(c40))//' (average)'
          endif
        endif
      elseif (rows(1).ne.-9999) then
        write(c40,'(a,i4,a,i4)') 'ROWS=',rows(1),'-',rows(2)
        if ( (rows(1).lt.sr).or.(rows(1).gt.er).or.
     .       (rows(1).lt.sr).or.(rows(1).gt.er) ) then
          print *,'Error- rows beyond bounds: ',sr,er
          return
        endif
        if (domdn) then
          do i=sc,ec
            narr=0
            do j=max(sr,min(er,rows(1))),max(sr,min(er,rows(2)))
              narr=narr+1
              arr(narr)=a(i,j)
            enddo
            n=n+1
            x(n)=float(i)
            call find_median(arr,narr,y(n))
          enddo
          c40=c40(1:lc(c40))//' (median)'
        else
          do i=sc,ec
            sum=0.
            num=0
            do j=max(sr,min(er,rows(1))),max(sr,min(er,rows(2)))
              num=num+1
              sum=sum+a(i,j)
            enddo
            n=n+1
            x(n)=float(i)
            if (dosum) then
              y(n)=sum
            else
              y(n)=sum/float(max(1,num))
            endif
            if (dorms) then
              sum=0.
              do j=max(sr,min(er,rows(1))),max(sr,min(er,rows(2)))
                sum= sum + ((a(i,j)-y(n))*(a(i,j)-y(n)))
              enddo
              y(n) = sqrt( sum / float(max(1,num)) )
            endif
          enddo
          if (dosum) then
            c40=c40(1:lc(c40))//' (sum)'
          elseif (dorms) then
            c40=c40(1:lc(c40))//' (R.M.S.)'
          else
            c40=c40(1:lc(c40))//' (average)'
          endif
        endif
      else
        return
      endif
C Find ranges.
      xmin=x(1)-((x(n)-x(1))*0.01)
      xmax=x(n)+((x(n)-x(1))*0.01)
C Override.
      if (xs.gt.-1.e+29) xmin=xs
      if (xe.gt.-1.e+29) xmax=xe
      if (xmax.le.xmin) return
      yhi=-1.e+30
      ylo=+1.e+30
      do i=2,n
        if ((x(i).gt.xmin).and.(x(i).lt.xmax)) then
          if (y(i).lt.ylo) ylo=y(i)
          if (y(i).gt.yhi) yhi=y(i)
        endif
      enddo
C Just in case.
      if (abs(yhi-ylo).lt.1.e-28) then
        sum =((yhi+ylo)/2.)
        if (abs(sum).lt.1.e-28) then
          ymin= sum - 1.e-28
          ymax= sum + 1.e-28
        else
          ymin= sum - (sum*0.0001)
          ymax= sum + (sum*0.0001)
        endif
      else
        ymin=ylo-((yhi-ylo)*0.02)
        ymax=yhi+((yhi-ylo)*0.02)
      endif
C Override.
      if (xs.gt.-1.e+29) xmin=xs
      if (xe.gt.-1.e+29) xmax=xe
      if (ys.gt.-1.e+29) ymin=ys
      if (ye.gt.-1.e+29) ymax=ye
C Open device.
      if (psfile.ne.' ') then
        wrd = psfile(1:lc(psfile))//'/PS'
        call PGBEGIN(0,wrd,1,1)
        print '(2a)',' Output plot in : ',psfile(1:lc(psfile))
      else
        wrd = '/XDISP'
        call PGBEGIN(0,wrd,1,1)
      endif
      call PGASK(.false.)
      call PGVSTAND
      call PGSLW(1)
      call PGSCF(1)
      call PGSCH(1.4)
C Set window.
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBOX('BCNST',0.,0,'BCNST',0.,0)
C plot data.
      if (hist) then
        call PGBIN(n,x,y,.true.)
      else
        call PGLINE(n,x,y)
      endif
C Write various text stuff on plot, write filename on first plot.
      ii=index(file,'.fits') - 1
      if (ii.lt.1) ii=lc(file)
      ii=min(20,ii)
      jj=min(50,lc(object))
      write(c80,'(a,3x,a,3x,a)') file(1:ii),object(1:jj),c40(1:lc(c40))
      call PGMTEXT('T',0.5,0.1,0.,c80)
      if ((row.ne.-9999).or.(rows(1).ne.-9999)) then
        call PGMTEXT('B',2.0,0.5,0.5,'Column')
      else
        call PGMTEXT('B',2.0,0.5,0.5,'Row')
      endif
      call PGEND
      return
      end
