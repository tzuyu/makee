/* esirebinarc.c                                      tab  Jan01 */

#include "ctab.h"
#include "cmisc.h"
#include "fstuff.h"
#include "cpgplot.h"
#include "cpg-util.h"
#include "cufio.h"


extern int esi_rebinarc(char aafile[], char bbfile[], int binning);


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Rebin a calibrated arc lamp 2D spectrum.
*/
int main(int argc, char *argv[])
{
/**/
char arg[20][100];
char aafile[100];
char bbfile[100];
char wrd[100];
int binning,narg;
int ii;
/**/   

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Get argument value. */
binning=0;
if (cfindarg(arg,&narg,"bin=",':',wrd)==1) { binning = GetLineValue(wrd,1); }

/* Syntax. */
if ((narg < 1)||(binning < 1)) {
  printf("Syntax: esirebinarc (arclamp FITS file) (new arclamp file) (bin=n)\n");
  printf("\n");  
  printf("   Required: bin=n : rebin to n old pixels to 1 new pixel.\n");
  printf("\n");  
  exit(0);
}

/* Set files. */
strcpy(aafile,arg[1]);
cAddFitsExt(aafile);
if (narg > 1) {
  strcpy(bbfile,arg[2]);
  cAddFitsExt(bbfile);
} else {
  strcpy(bbfile,"none");
}

/* Call routine. */
ii = esi_rebinarc( aafile, bbfile, binning );
if (ii != 1) {
  fprintf(stderr,"***ERROR: esirebinarc:  Re-binning failed.\n");
  exit(0);
}

return(0);
}

