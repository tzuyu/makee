/* esiwave.c                                      tab  Sep00 */

#include "ctab.h"
#include "cmisc.h"
#include "fstuff.h"
#include "cpgplot.h"
#include "cpg-util.h"
#include "cufio.h"


extern int esiwave_apply( char aafile[], char bbfile[] );


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  esiwave: Applying a wavelength scale to an ESI arc lamp.
*/
int main(int argc, char *argv[])
{
/**/
char arg[500][100];
char aafile[100];
char bbfile[100];
int narg;
/**/   

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Syntax. */
if (narg < 1) {
  printf("Syntax: esiwave (arclamp FITS file) (2nd arclamp FITS file)\n");
  printf("\n");  
  exit(0);
}

/* Set files. */
strcpy(aafile,arg[1]);
cAddFitsExt(aafile);
if (narg > 1) {
  strcpy(bbfile,arg[2]);
  cAddFitsExt(bbfile);
} else {
  strcpy(bbfile,"none");
}

/* Apply wavelength scales. */
if ( esiwave_apply( aafile, bbfile ) != 1) {
  fprintf(stderr,"ERROR: esiwave: Applying wavelength scales.\n");
  return(0);
}

return(0);
}
