C codesum.f
C
      implicit none
      character*200 line
      character*80 arg(99),wrd,outfile
      integer*4 narg,i,k,lc,oun
      logical out,findarg
C
      call arguments(arg,narg,99)
      if (findarg(arg,narg,'of=',':',wrd,i)) then
        outfile=wrd
        oun=2
      else
        oun=6
      endif
      if (narg.lt.1) then
        print *,
     .   'Syntax: codesum (list of .f files) [of=(output filename)]'
        call exit(1)
      endif
      if (oun.ne.6) open(oun,file=outfile,status='unknown')
      do i=1,narg
        open(1,file=arg(i),status='old',iostat=k)
        if (k.ne.0) then
          print '(2a)',' Error opening: ',arg(i)(1:lc(arg(i)))
          call exit(1)
        endif
        out=.false.
1       continue
        read(1,'(a)',err=9,end=5) line
        if (line(1:10).eq.'CENDOFMAIN') then
          out=.true.
          write(oun,'(3a)') 'C ::::::::::::::::::: NEW FILE: ',
     .            arg(i)(1:lc(arg(i))),' :::::::::::::::::::'
        endif
        if (out) write(oun,'(a)') line(1:lc(line))
        goto 1
5       continue
        close(1)
      enddo
      if (oun.ne.6) close(oun)
      stop
9     continue
      print *,'Error reading input file.'
      stop
      end

       
