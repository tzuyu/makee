/* eeuc.c                                      tab  Sep00 */

#include "ctab.h"
#include "cmisc.h"
#include "fstuff.h"
#include "cpgplot.h"
#include "cpg-util.h"
#include "cufio.h"


/* EsiWave structure. */
#define EWmax 1000
struct EWstructure {
  int    num;     /* "EW.num[0]" is number of points. */
  double shift;   /* shift (either correlation shift or line shift). */
  double basepix; /* line pixel position in the base arclamp. */
  int    irow;    /* row number (order-1) in which a line appears. */
                  /* or the row number for a correlation shift. */
};
typedef struct EWstructure EWtype;


/* Raw ESI file data. */
struct RAWstructure {
  char file[100];    /* Raw file filename. */
  char object[100];  /* Object name. */
  char obstype[10];  /* Observation type: Object, Line, DmFlat, Bias, ... */
  char slmsknam[10]; /* Slit mask name */
  int CuAr;          /* CuAr lamp on? (1/0) (CU1) */    
  int Xenon;         /* Xenon lamp on? (1/0) (NE1) */    
  int exptime;       /* Exposure time in seconds. */
  int utime;         /* Universal time in seconds since 1970. */
  int obsnum;        /* Observation number. */
  int nc;            /* Number of columns. */
  int nr;            /* Number of rows. */
  int num;           /* RAW[0].num is number of files. */
};
typedef struct RAWstructure RAWtype;


/* ----------------------------------------------------------------------
  Given data from an arc spectrum and a filename of a base or reference arc
      spectrum FITS file, find pixel shifts for each order using 
      correlations.  The shift is the number of pixels any given arc
      emission line will be shifted in the "new" spectrum in pixels 
      relative to the "base" spectrum.  (So if the shift is -2.4 a line 
      "A" which appears at pixel position 438.7  in the "base" spectrum
      will appear at 436.3 in the "new" spectrum.)

  Input: aa        : New arc spectrum data.
         nc        : Number of columns.
         nr        : Number of rows.
         base_file : Base Arc spectrum FITS file.
 In/Out: EW        : EsiWave structure.
                .num   : number of points loaded so far (input/output)
                .shift : shift in correlation function.
                .irow  : row number for each shift.
Scratch: cc[]         : scratch image.
  Input: mp           : maximum pixels in scratch images.
Scratch: header[]     : scratch FITS header.
  Input: headersize   : maximum header size in bytes.

*/
void esiwave_CorrEW( float aa[], int nc,int nr, char base_file[], EWtype EW[], 
                      float cc[], int mp, char header[], int headersize)
{
/**/
int sc,ec,sr,er;
int nc_cc,nr_cc;
float xpeak,peak,fwhm;
int irow,icol,pixno,cos1,cos2,iok;
/**/
float aa1d[5000],cc1d[5000];
/**/

/* Read in files. */
creadfits( base_file, cc, mp, header, headersize );
cGetDimensions(header,headersize,&sc,&ec,&sr,&er,&nc_cc,&nr_cc);

/* Check. */
if ((nc != nc_cc)||(nr != nr_cc)) {
  fprintf(stderr,"ERROR: esiwave_CorrEW: dimensions do not match:%d %d %d %d\n",
                  nc,nr,nc_cc,nr_cc);
  return;
}

/* Column offset shift search range. */
cos1 = -200;
cos2 =  200;

/* Correlation each row. */
for (irow=0; irow<nr; ++irow) {

/* Load 1d spectra. */
  for (icol=0; icol<nc; ++icol) {
    pixno = icol + (irow * nc);
    aa1d[icol] = aa[pixno];       /* Note: these arrays will be changed. */
    cc1d[icol] = cc[pixno];
  }

/* Find best shift. */
  f2cautoidcorrelate_ (&nc,aa1d,cc1d,&cos1,&cos2,&xpeak,&peak,&fwhm,&iok); 

/* Reversal-- to be consistent with definition. */
  xpeak = -1. * xpeak;

/* Check. */
  if (iok != 1) {
    fprintf(stderr,"WARNING: esiwave_CorrEW: Finding correlation. %d\n",irow);
  } else {
    EW[ EW[0].num ].shift= xpeak;       /* Save values. */
    EW[ EW[0].num ].irow = irow;
    EW[0].num = EW[0].num + 1;
  }
}

return;
}


/* ----------------------------------------------------------------------
  Get coeffecients from header for an echelle order.
  Input: eon            : Echelle order number (first, bluest, is "1").
         header[]       : FITS file header.
         headersize     : Maximum header size in bytes.
 Output: coef[]         : Polynomial coeffecients.
         ncoef          : Number of coeffecients.
*/
void esiwave_get_coef( int eon, char header[], int headersize, 
                       double coef[], int *ncoef)
{
/**/
char name[10];
char wrd[100];
/**/
/* Read wavelength cards. */
sprintf(name,"WV_0_%2.2d",eon);
if (cgetpos(name,header,headersize) < 0) {
  fprintf(stderr,"ERROR: esiwave_get_coef: No wavelength in reference file.\n");
  *ncoef = 0;
  return;
}
cchead(name,header,headersize,wrd);
coef[0] = GLV(wrd,1);
coef[1] = GLV(wrd,2);
coef[2] = GLV(wrd,3);
coef[3] = GLV(wrd,4);
/* Next card. */
sprintf(name,"WV_4_%2.2d",eon);
cchead(name,header,headersize,wrd);
coef[4] = GLV(wrd,1);
coef[5] = GLV(wrd,2);
coef[6] = GLV(wrd,3);
coef[7] = GLV(wrd,4);
/* Number of coeffecients. */
*ncoef = 8;
if (ABS((coef[7])) < 1.e-40) { *ncoef = 7; }
return;
}


/* ----------------------------------------------------------------------
  Put coeffecients (0 to 7) into a header for an echelle order.
  Input: eon            : Echelle order number (first, bluest, is "1").
 In/Out: header[]       : FITS file header.
  Input: headersize     : Maximum header size in bytes.
         coef[]         : Polynomial coeffecients.
*/
void esiwave_put_coef( int eon, char header[], int headersize, double coef[] )
{
/**/
char name[10];
char wrd[100];
/**/
/* Write wavelength card. */
sprintf(name,"WV_0_%2.2d",eon);
sprintf(wrd,"%16.9e %16.9e %16.9e %16.9e",coef[0],coef[1],coef[2],coef[3]);
ccheadset(name,wrd,header,headersize);
/* Next card. */
sprintf(name,"WV_4_%2.2d",eon);
sprintf(wrd,"%16.9e %16.9e %16.9e %16.9e",coef[4],coef[5],coef[6],coef[7]);
ccheadset(name,wrd,header,headersize);
return;
}


/* ----------------------------------------------------------------------
  Find base arc file name.
  Input: header[]       : New arc FITS file header.
         headersize     : Maximum header size in bytes.
 Output: base_file[]    : Base arc FITS filename (from database).
         base_idfile[]  : Base arc line id list (from database).
*/
void esiwave_findbasefile( char header[], int headersize,
                           char base_file[], char base_idfile[], int xbin )
{
/**/
char wrd[100];
int CuAr,Xe,HgNe;
/**/
/* Which lamps are turned on? */
cchead("LAMPCU1",header,headersize,wrd);
if (strcmp(wrd,"on") == 0) { CuAr=1; } else { CuAr=0; }
cchead("LAMPNE1",header,headersize,wrd);
if (strcmp(wrd,"on") == 0) { Xe=1; } else { Xe=0; }
cchead("LAMPAR1",header,headersize,wrd);
if (strcmp(wrd,"on") == 0) { HgNe=1; } else { HgNe=0; }
/* Makee home directory. */
cGetMakeeHome( base_file );
strcpy(base_idfile,base_file);
/* Choose base file. */
if (CuAr == 1) {
  if (xbin == 1) { strcat(base_file,"ArcESI/Arc-CuAr.fits"); } else {
  if (xbin == 2) { strcat(base_file,"ArcESI/Arc-CuAr_1x2.fits"); } }
  strcat(base_idfile, "ArcESI/Arc-CuAr.ids");
} else {
if (Xe == 1) {
  if (xbin == 1) { strcat(base_file,"ArcESI/Arc-HgNeXe.fits"); } else {
  if (xbin == 2) { strcat(base_file,"ArcESI/Arc-HgNeXe_1x2.fits"); } }
  strcat(base_idfile, "ArcESI/Arc-HgNeXe.ids");
} else {
  fprintf(stderr,"ERROR: esiwave_findbasefile: Finding arc to match. Lamps: CuAr=%d Xe=%d HgNe=%d .\n",CuAr,Xe,HgNe);
  strcpy(base_file,"none");
  return;
}}
return;
}

/* ----------------------------------------------------------------------
  Given the data of an arc spectrum and the filename of base or reference
      arc spectrum FITS file, find pixel shifts for each order using known
      line ids.  Results give the number of pixels any given arc
      emission line will be shifted in the "new" spectrum in pixels 
      relative to the "base" spectrum.  (So if the shift is -2.4 a line 
      "A" which appears at pixel position 438.7  in the "base" spectrum
      will appear at 436.3 in the "new" spectrum.)

  Input: aa           : New arc spectrum data.
         nc           : Number of columns.
         nr           : Number of rows.
         CORRshift    : Correlations shift.
         base_idfile  : Base Arc spectrum line ID file.
  Input: xbin         : column binning factor (xbin=2 for 1x2 ESI raw data).
 In/Out: EW           : EsiWave structure (shift,fwhm,peak)
                  .num   : number of lines loaded so far (input/output)
                  .shift : shift of a line.
                  .irow  : row of a line.
                  .basepix : pixel position of line in base spectrum.
*/
void esiwave_LineEW( float aa[], int nc, int nr, double CORRshift,
                      char base_idfile[], EWtype EW[], int xbin )
{
/**/
char line[100];
double basepix,pix,cent,peak,fwhm,xfac;
int irow;
int sc,ec,sr,er,iok;
/**/
FILE *infu;
/**/
/* Initialize. */
xfac = xbin;
sc=0; sr=0; ec=nc-1; er=nr-1;
/* Open line id file. */
infu = fopen(base_idfile,"r");
/* Read each line. */
while (fgetline(line,infu) == 1) {
  if (GLV(line,6)  > 0.9) {
    irow    = GLV(line,2) - 1;
    basepix = GLV(line,3);
    if (xfac > 1.) {
      basepix = ( basepix / xfac ) - ( (xfac - 1.) / (2. * xfac) );
    }
    pix     = basepix + CORRshift;
    f2carccentroid_ (aa, &sc,&ec,&sr,&er, &irow, &pix,&cent,&peak,&fwhm, &iok);
    EW[ EW[0].num ].shift   = cent - basepix;
    EW[ EW[0].num ].irow    = irow;
    EW[ EW[0].num ].basepix = basepix;
    EW[0].num = EW[0].num + 1;
  }
}
/* Close file. */
fclose(infu);

return;
}


/* ----------------------------------------------------------------------
  Given one or two new arc spectrum files, find the appropriate base files,
    find the pixel shift, and apply the base wavelength to the new
    spectrum files.
  Also, a PostScript plot file is created showing the correlation
    functions and the line shifts fit.  Filename is derived by replacing
    ".fits" with "-fr.ps" in the aafile filename.

  Input: aafile : First new arc spectrum FITS file (input).
         bbfile : Second new arc spectrum FITS file (or "none") (input).

  Returns "1" if successful.
*/
int esiwave_apply( char aafile[], char bbfile[] )
{
/**/
const int headersize = 57600;
char aaheader[57600];
char bbheader[57600];
char ccheader[57600];
/**/
char base_aafile[100];
char base_aaidfile[100];
char base_bbfile[100];
char base_bbidfile[100];
char device[100];
char wrd[100];
/**/
const int mp = 99000;
float aa[99000],bb[99000],cc[99000];
/**/
EWtype EW[ EWmax ];
/**/
double arr8[ EWmax ];
/**/
double CORRshift;
/**/
float xmin,xmax,ymin,ymax,range;
/**/
int npt,iok,ord;
double x8[EWmax],y8[EWmax],w8[EWmax],r8[EWmax];
double base_coef[20],coef[20],xv,high,wrms;
/**/
int xbin,sc,ec,sr,er,ii,jj,nc,nr,eon,base_ncoef,base_ord;
/**/
float xxx,yyy;
double hi,diff;
int iihi,nrej,bbflag;
/**/

/* Create PS filename. */
strcpy(device,aafile);
ii = cindex(device,".fits");
if (ii < 0) { ii = clc(device) + 1; }
device[ii] = '\0';
strcat(device,"-fr.ps");
strcat(device,"/VPS");

/* Read data and header. */
creadfits( aafile, aa, mp, aaheader, headersize );

/* Get dimensions. */
cGetDimensions(aaheader,headersize,&sc,&ec,&sr,&er,&nc,&nr);

/* Check. */
if (nc < 1800) {
  fprintf(stderr,"ERROR: esiwave_apply: Expecting > 1800 columns in arc\n");
  fprintf(stderr,"ERROR: esiwave_apply:   spectrum, but found only %d .\n",nc);
  return(0);
}

/* X binning. */
if (nc > 3000) { xbin = 1; } else { xbin = 2; }

/* Initialize. */
EW[0].num = 0;

/* Find base arc filename. */
esiwave_findbasefile( aaheader, headersize, base_aafile, base_aaidfile, xbin);
if (strcmp(base_aafile,"none") == 0) {
  fprintf(stderr,"ERROR: esiwave_apply: No base file for %s .\n",aafile);
  return(0);
}

/* Second file flag. */
if (strcmp(bbfile,"none") == 0) { bbflag = 0; } else { bbflag = 1; }

/* Find correlation shift, load into arrays. */
esiwave_CorrEW( aa, nc, nr, base_aafile, EW, cc, mp, ccheader, headersize );

/* Second base arc filename. Add to shift value arrays. */
if (bbflag == 1) {
  creadfits( bbfile, bb, mp, bbheader, headersize );
  cGetDimensions(bbheader,headersize,&sc,&ec,&sr,&er,&ii,&jj);
  if ((ii != nc)||(jj != nr)) { 
    fprintf(stderr,
      "ERROR: esiwave_apply: dimensions do not match for %s and %s .\n",
       aafile,bbfile);
    return(0);
  }
  esiwave_findbasefile( bbheader, headersize, base_bbfile, base_bbidfile, xbin);
  if (strcmp(base_bbfile,"none") == 0) {
    fprintf(stderr,"ERROR: esiwave_apply: No base file for %s .\n",bbfile);
    return(0);
  }
  esiwave_CorrEW( bb, nc, nr, base_bbfile, EW, cc, mp, ccheader, headersize );
}

/* Check. */
if (EW[0].num < 3) {
  fprintf(stderr,"ERROR: esiwave_apply: Not enough (corr) shift points.\n");
  return(0);
}

/* Median the shifts. */
for (ii=0; ii<EW[0].num; ++ii) { arr8[ii] = EW[ii].shift; }
CORRshift = cfind_median8( EW[0].num, arr8 );

/* Start PS file. */
cpgbeg(0,device,1,2);
cpgsvp(0.1,0.9,0.1,0.9);

/* Find the plot range. */ 
xmin = EW[0].irow;
xmax = EW[0].irow;
ymin = EW[0].shift;
ymax = EW[0].shift;
for (ii=0; ii<EW[0].num; ++ii) {
  if (EW[ii].irow  < xmin) xmin = EW[ii].irow;
  if (EW[ii].irow  > xmax) xmax = EW[ii].irow;
  if ( ABS((EW[ii].shift - CORRshift)) < (10. / (double)xbin) ) {
    if (EW[ii].shift < ymin) ymin = EW[ii].shift;
    if (EW[ii].shift > ymax) ymax = EW[ii].shift;
  }
}
xmin = xmin + 0.5;  /* X's will be shifted +1 */
xmax = xmax + 1.5;
ymin = ymin - 0.01;
ymax = ymax + 0.01;
range= ymax - ymin;
ymin = ymin - (0.05*range);
ymax = ymax + (0.05*range);

/* Plot the CORR shift points. */
cpgpage();
cpgswin(xmin,xmax,ymin,ymax);
cpgbox("BCNST",0.,0,"BCNST",0.,0);
cpgsch(1.0);
cpgbbuf();
for (ii=0; ii<EW[0].num; ++ii) {
  xxx = EW[ii].irow + 1;
  yyy = EW[ii].shift;
  cpg_pt1(xxx,yyy,4);
}
cpgsls(4);
cpg_HLine( xmin, xmax, (float)CORRshift );
cpgsls(1);
cpgsch(1.5);
sprintf(wrd,"ESI Arc files: %s %s",aafile,bbfile);
cpgmtxt("T",1.0,0.5,0.5,wrd);
cpgmtxt("L",2.5,0.5,0.5,"Data Correlation Pixel Shift");
cpgmtxt("B",2.5,0.5,0.5,"Order Number");
cpgebuf();


/* Find shifts from known emission lines. */
EW[0].num = 0;
esiwave_LineEW( aa, nc, nr, CORRshift, base_aaidfile, EW, xbin );
if (bbflag == 1) {
  esiwave_LineEW( bb, nc, nr, CORRshift, base_bbidfile, EW, xbin );
}
/* Check. */
if (EW[0].num < 10) {
  fprintf(stderr,"***ERROR: esiwave_apply: Too few points: %d \n",EW[0].num);
  return(0);
}
/* Load fitting points. */
for (ii=0; ii<EW[0].num; ++ii) {
  x8[ii] = EW[ii].basepix;
  y8[ii] = EW[ii].shift;
  w8[ii] = 1.;
  r8[ii] = EW[ii].irow;
}

/* ............. Fit and reject ............ */
nrej = 0;
while (1) {

/* Fit a line. */
  ord = 1;
  iok = 0;
  npt = EW[0].num;
  f2cpolyfitglls_ ( &npt, x8, y8, w8, &ord, coef, &iok );
  if (iok != 1) {
    fprintf(stderr,"***ERROR: esiwave_apply: FIT failed.\n");
    return(0);
  }
  
/* Report residuals. */
  f2cpolyfitgllsresiduals_ ( &npt, x8, y8, w8, &ord, coef, &high, &wrms );
  
/* Reject the worse point. */
  hi   = 0.;
  iihi = -1;
  for (ii=0; ii<EW[0].num; ++ii) {
    if (w8[ii] > 0.) {
      diff = ABS(( y8[ii] - cpolyval( ord+1, coef, x8[ii] ) ));
      if (diff > hi) {
        iihi = ii;
        hi   = diff;
      }
    }
  }
/* Check. */
  if ((iihi < 0)||(hi == 0.)) {
    fprintf(stderr,"***ERROR: esiwave_apply: this should not happen.\n");
    return(0);
  }
/* Break? */
  if (hi < 0.4) break;    /* Threshold is +/-0.4 pixels. */
/* Reject. */
  w8[iihi] = 0.;
  ++nrej;
  
/* Break? */
  if (nrej > EW[0].num/2) {
    printf("WARNING: esiwave_apply: Rejected too many pts: nrej=%d  num=%d\n",
                nrej,EW[0].num);
    break;
  }

}

/* Plot the Arc line comparison shift points. */
xmin = +1.e+30;
xmax = -1.e+30;
ymin = +1.e+30;
ymax = -1.e+30;
for (ii=0; ii<EW[0].num; ++ii) {
  if (w8[ii] > 0.) {
    if (x8[ii] < xmin) xmin = x8[ii];
    if (x8[ii] > xmax) xmax = x8[ii];
    if (y8[ii] < ymin) ymin = y8[ii];
    if (y8[ii] > ymax) ymax = y8[ii];
  }
}
xmin = xmin - 10.;
xmax = xmax + 10.;
range= xmax - xmin;
xmin = xmin - (0.05*range);
xmax = xmax + (0.05*range);
ymin = ymin - 0.01;
ymax = ymax + 0.01;
range= ymax - ymin;
ymin = ymin - (0.05*range);
ymax = ymax + (0.05*range);
cpgpage();
cpgswin(xmin,xmax,ymin,ymax);
cpgbox("BCNST",0.,0,"BCNST",0.,0);
cpgsch(1.0);
cpgbbuf();
for (ii=0; ii<EW[0].num; ++ii) {
  xxx = x8[ii];
  yyy = y8[ii];
  if (w8[ii] > 0.) { 
    cpg_pt1(xxx,yyy,4);
  } else {
    cpg_pt1(xxx,yyy,5);
  }
}
cpgsls(4);
for (xv=xmin; xv<xmax; xv=xv+10.) {
  xxx = xv;
  yyy = cpolyval( ord+1, coef, xv );
  cpg_pt1(xxx,yyy,1);
}
cpgsls(1);
cpgsch(1.5);
cpgmtxt("L",2.5,0.5,0.5,"Arcline Comparison Pixel Shift");
cpgmtxt("B",2.5,0.5,0.5,"Column Number");
cpgebuf();

/* Close device. */
cpgend();


/* Read header from first comparison file. */
creadfitsheader( base_aafile, ccheader, headersize );

/* Re-fit polynomials for each row (echelle order). */
for (eon=1; eon<=nr; ++eon) {

/* Read reference scale polynomial. */
  esiwave_get_coef(eon,ccheader,headersize,base_coef,&base_ncoef);
  if (base_ncoef != 7) {
    fprintf(stderr,"ERROR: esiwave_apply: Must have 7 coeffecients.\n");
    return(0);
  }
  base_ord = base_ncoef - 1;
    
/* Load fit points. */
  npt=0;
  for (ii=0; ii<nc; ii=ii+10) {
    xv     = ii;                                  /* base frame pixel */
    y8[npt]= cpolyval(base_ncoef,base_coef,xv);   /* base frame wavelength */
    x8[npt]= xv + cpolyval(ord+1,coef,xv);        /* new frame pixel */
    ++npt;
  }

/* Re-fit polynomial. */
  f2cpolyfitglls_ ( &npt, x8, y8, w8, &base_ord, base_coef, &iok );
  if (iok != 1) {
    fprintf(stderr,"***ERROR: esiwave_apply: 2: FIT failed.\n");
    return(0);
  }
  
/* Report residuals. */
  f2cpolyfitgllsresiduals_ (&npt,x8,y8,w8,&base_ord,base_coef,&high,&wrms);
  if ((ABS((high)) > 0.1)||(wrms > 0.01)) {
    fprintf(stderr,"ERROR: esiwave_apply: Re-fit failed.\n");
    fprintf(stderr,"ERROR: eon=%2d  npt=%3d  high=%12.5e  wrms=%12.5e\n",
                           eon,npt,high,wrms);
    return(0);
  }
  
/* Write coeffecients into both new headers. */
  esiwave_put_coef(eon,aaheader,headersize,base_coef);
  if (bbflag == 1) {
    esiwave_put_coef(eon,bbheader,headersize,base_coef);
  }
}

/* Write new arc files. */
cwritefits( aafile, aa, aaheader, headersize );
if (bbflag == 1) {
  cwritefits( bbfile, bb, bbheader, headersize );
}

return(1);
}


/* ----------------------------------------------------------------------
  FORTRAN gateway to esiwave_apply.
*/
int esiwaveapply_ ( char aafile[], char bbfile[] )
{
int ii;
ii = esiwave_apply( aafile, bbfile );
return(ii);
}


/* ----------------------------------------------------------------------
  Convert a date and a time into seconds since 1970.
  Input: userdate  : User Date in format: YYYY-MM-DD, YYYY/MM/DD, or YYYY MM DD
         usertime  : User Time in format: HH:MM:SS.SS, HH MM SS, or HH MM SS.S
*/
int esipipe_seconds_since_1970( char userdate[], char usertime[] )
{
/**/
char thedate[100];
char thetime[100];
int ii,tt,doy,yy,mm,dd,hh,nn,ss;
/**/
const int nd[12]  ={ 0,31,59,90,120,151,181,212,243,273,304,334 };
const int ndly[12]={ 0,31,60,91,121,152,182,213,244,274,305,335 }; /* leap */
/**/
/* Copy */
strcpy( thedate, userdate );
strcpy( thetime, usertime );
/* Eliminate "-" characters. */
for (ii=0; ii<strlen(thedate); ++ii) { 
  if (thedate[ii] == '-') { thedate[ii] = ' '; }
}
/* Get year,month,day,hour,minute,second. Truncates the seconds. */
yy = GLV(thedate,1); mm = GLV(thedate,2); dd = GLV(thedate,3);
hh = GLV(thetime,1); nn = GLV(thetime,2); ss = GLV(thetime,3);
/* Count up years from 1970 to yy-1 */
tt = 0;
for (ii=1970; ii<(yy-1); ++ii) {
  if ((ii%4) == 0) {
    tt = tt + (366 * 24 * 3600);  /* leap year */
  } else {
    tt = tt + (365 * 24 * 3600);  /* non-leap year */
  }
}
/* Day of year before beginning of month. */
if ((yy%4) == 0) {
  doy = ndly[ mm - 1 ];    /* leap year */
} else {
  doy = nd[ mm - 1 ];      /* non-leap year */
}
/* Seconds since 1970. */
tt = tt + ss + (nn * 60) + (hh * 3600) + ((doy + dd - 1) * 24 * 3600);
return(tt);
}


/* ----------------------------------------------------------------------
  esipipe: Creating an ESI makee run script.
  Looks at all the files in a list.  Groups and adds the Dome Flats.
  Groups the arc lamps.  Matches all the objects with stars, flats, and
  arclamps to create "makee.script".
*/
int esipipe( char file1[], char file2[], char rawdir[], char extopt[],
             int flatresum )
{
/**/
const int maxfsf = 40;
char fsf[40][100];
int ii_fsf[40];
/**/
const int maxalp = 40;
int alp1[40];
int alp2[40];
/**/
char ut[100];
char date_obs[100];
char line[100];
char wrd[100];
char obstype0[100];
char slmsknam0[100];
/**/
int nc0,nr0,utime0;
int ff,kk,ii,jj,pp,flatlist[99];
int sc,ec,sr,er,nc,nr,nfsf,nalp;
int fflo,jjlo,diff,lo;
int iiTrace,iiFlat,iiArc; 
/**/ 
const int headersize = 57600; 
char header[57600];
char header2[57600];
/**/
const int maxaa = 9184000;   /* 2240 x 4100 */
float *aa;
float *bb;
/**/
const int maxobs = 400;
RAWtype RAW[400];
/**/   
FILE *infu;
FILE *outfu;
/**/   

/* Allocate memory. */
aa = (float *)calloc(maxaa,sizeof(float));
bb = (float *)calloc(maxaa,sizeof(float));

/* Read in raw file list and load in structure by reading headers. */
printf("NOTE: esipipe: Reading file list from %s .\n",file1);
infu = fopen(file1,"r");
if (infu == NULL) {
  fprintf(stderr,"ERROR: esipipe: Cannot open %s .\n",file1);
  exit(0);
}
ii = 0;
/* Each file. */
while( fgetline(line,infu) == 1 ) {
/* Raw FITS filename. */
  strcpy(RAW[ii].file,line);
  cAddFitsExt( RAW[ii].file );
  strcpy(wrd,rawdir);
  strcat(wrd,"/");
  strcat(wrd,RAW[ii].file);
/* Read in FITS header. */
  if ( creadfitsheader( wrd, header, headersize ) != 1 ) {
    fprintf(stderr,"ERROR: esipipe: Cannot read ESI file %s .\n",wrd);
    exit(0);
  }
/* Dimensions. */
  cGetDimensions(header,headersize,&sc,&ec,&sr,&er,&nc,&nr);
  RAW[ii].nc = nc;
  RAW[ii].nr = nr;
/* Values. */
  cchead( "OBJECT" , header, headersize, RAW[ii].object );
  cchead( "OBSTYPE", header, headersize, RAW[ii].obstype );
  cchead( "SLMSKNAM",header, headersize, RAW[ii].slmsknam );
  RAW[ii].exptime= cinhead("EXPOSURE", header, headersize );
  RAW[ii].obsnum = cinhead("OBSNUM"  , header, headersize );
  RAW[ii].CuAr = 0;
  RAW[ii].Xenon= 0;
  cchead("LAMPCU1",header,headersize,wrd);
  if (cindex(wrd,"on") > -1) { RAW[ii].CuAr = 1; }
  cchead("LAMPNE1",header,headersize,wrd);
  if (cindex(wrd,"on") > -1) { RAW[ii].Xenon = 1; }
/* Find seconds since 1970. */
  cchead( "UT", header, headersize, ut );
  cchead( "DATE-OBS", header, headersize, date_obs );
  RAW[ii].utime = esipipe_seconds_since_1970( date_obs, ut );
/* Increment and check. */
  ++ii;
  if (ii >= maxobs) {
    fprintf(stderr,"ERROR: esipipe: Too many files.  Limit is %d .\n",maxobs);
    exit(0);
  }
/* Delete if not echellete. */
  cchead("SYNOPFMT",header,headersize,wrd);
  if (cindex(wrd,"Echellette") < 0) { ii = ii - 1; }
}
RAW[0].num = ii;
/* Close file. */
fclose(infu);

/* ................Read in observation numbers of stars (given by user). */
printf("NOTE: esipipe: Reading star number list from %s .\n",file2);
infu = fopen(file2,"r");
if (infu == NULL) {
  fprintf(stderr,"ERROR: esipipe: Cannot open star number list %s .\n",file2);
  exit(0);
}
while( fgetline(line,infu) == 1 ) {
  kk = GLV(line,1);
/* Go through raw file list. */
  for (ii=0; ii<RAW[0].num; ++ii) {
    if (RAW[ii].obsnum == kk) strcpy(RAW[ii].obstype,"Trace");
  }
}
fclose(infu);

/* ...........................Find dome or int flat groups and sum them. */
ii=0;
nfsf=0;
while (ii < RAW[0].num) {
  if ((strcmp(RAW[ii].obstype,"DmFlat") == 0)||
      (strcmp(RAW[ii].obstype,"IntFlat") == 0)) {
    strcpy(obstype0,RAW[ii].obstype);
    strcpy(slmsknam0,RAW[ii].slmsknam);
    nc0    = RAW[ii].nc;
    nr0    = RAW[ii].nr;
    utime0 = RAW[ii].utime;
    ff=0;
    flatlist[ff] = ii;
    ++ff;
    ++ii;
    while ((ii < RAW[0].num)&&
         (RAW[ii].nc == nc0)&&(RAW[ii].nr == nr0)&&  /* Matching dimensions.*/
         (ABS((RAW[ii].utime - utime0)) < 3600)&&    /* <1 hr sep. */
         (strcmp(RAW[ii].obstype,obstype0) == 0)&&   /* same observation type.*/
         (strcmp(RAW[ii].slmsknam,slmsknam0) == 0) ) {   /* same slit mask */
      flatlist[ff] = ii;
      ++ff;
      ++ii;
    }
/* Echo. */
    printf("\n   Sum flats:\n");
    for (jj=0; jj<ff; ++jj) {
      kk = flatlist[jj];
      printf("%4d : %s :%5ds : %s\n",
        RAW[kk].obsnum,RAW[kk].obstype,RAW[kk].exptime,RAW[kk].object);
    }
/* Summed flat filename. */
    sprintf(fsf[nfsf],"Flatsum-%4.4d.fits",RAW[flatlist[0]].obsnum);
/* Record element array number of first flat in sum. */
    ii_fsf[nfsf] = flatlist[0];
/* Process flats and add this group to the flat reference list. */
/* Write the flat sum unless it already exists (or rewrite requested). */
    strcpy(wrd,rawdir);
    strcat(wrd,"/");
    strcat(wrd,fsf[nfsf]);
    if ((flatresum == 1)||(FileExist(wrd) == 0)) {
/* Read the first file. */
      kk = flatlist[0];
      strcpy(wrd,rawdir);
      strcat(wrd,"/");
      strcat(wrd,RAW[kk].file);
      creadfits(wrd,aa,maxaa,header,headersize);
/* Read the remaining files and add into first file. */
      for (jj=1; jj<ff; ++jj) {
        kk = flatlist[jj];
        strcpy(wrd,rawdir);
        strcat(wrd,"/");
        strcat(wrd,RAW[kk].file);
        creadfits(wrd,bb,maxaa,header2,headersize);
        for (pp=0; pp<(RAW[kk].nc * RAW[kk].nr); ++pp) {
          aa[pp] = aa[pp] + bb[pp];
        }
      }
/* Add header card, write out flat sum and load fsf list. */
      sprintf(wrd,"Flat Sum: %d",RAW[flatlist[0]].obsnum);
      for (jj=1; jj<ff; ++jj) { 
        sprintf(wrd,"%s+%d",wrd,RAW[flatlist[jj]].obsnum);
      }
      ccheadset("OBJECT",wrd,header,headersize);
      strcpy(wrd,rawdir);
      strcat(wrd,"/");
      strcat(wrd,fsf[nfsf]);
      printf("Writing flat sum file: %s .\n",wrd);
      cwritefits(wrd,aa,header,headersize);      
    } else {
      printf("Using pre-existing flat sum file: %s .\n",fsf[nfsf]);
    }
/* Increment and check. */
    ++nfsf;
    if (nfsf >= maxfsf) {
      fprintf(stderr,"ERROR: esipipe: Too many flat sum groups. (maxfsf=%d)\n",maxfsf);
      exit(0);
    }
    --ii;
  }
  ++ii;
}

/* ........................... Find arc lamp pairings. */
ii=0;
nalp=0;
while(ii < RAW[0].num) {
  if (strcmp(RAW[ii].obstype,"Line") == 0) {
/* Look for valid pairings. */
    jj=ii+1;
/* Xenon and then CuAr. */
    if ((RAW[ii].Xenon== 1)&&(RAW[ii].CuAr == 0)&&(RAW[ii].exptime > 2)&&
        (RAW[jj].CuAr == 1)&&(RAW[jj].Xenon== 0)&&(RAW[jj].exptime > 299)&&
        (ABS((RAW[ii].utime - RAW[jj].utime)) < 3600)) {
      alp1[nalp] = ii;
      alp2[nalp] = jj;
      ++nalp;
      ++ii;
    } else {
/* CuAr and then Xenon. */
    if ((RAW[ii].CuAr == 1)&&(RAW[ii].Xenon== 0)&&(RAW[ii].exptime > 299)&&
        (RAW[jj].Xenon== 1)&&(RAW[jj].CuAr == 0)&&(RAW[jj].exptime > 2)&&
        (ABS((RAW[ii].utime - RAW[jj].utime)) < 3600)) {
      alp1[nalp] = ii;
      alp2[nalp] = jj;
      ++nalp;
      ++ii;
    } else {
/* Single CuAr. */
    if ((RAW[ii].CuAr == 1)&&(RAW[ii].Xenon== 0)&&(RAW[ii].exptime > 299)) {
      alp1[nalp] = ii;
      alp2[nalp] = -1;
      ++nalp;
    } else {
/* Single Xenon. */
    if ((RAW[ii].Xenon== 1)&&(RAW[ii].CuAr == 0)&&(RAW[ii].exptime > 2)) {
      alp1[nalp] = ii;
      alp2[nalp] = -1;
      ++nalp;
    }}}}
  }
  ++ii;
  if (nalp >= maxalp) {
    fprintf(stderr,"ERROR: esipipe: Too many arc line pairs. (maxalp=%d)\n",maxalp);
    exit(0);
  }
}
if (nalp == 0) {
  fprintf(stderr,"ERROR: esipipe: Found no arc line pairs.\n");
  exit(0);
}

/* Echo. */
printf("\n   Arclamp pairs or singles: \n");
for (ii=0; ii<nalp; ++ii) {
  if (alp2[ii] > -1) {
    printf("%5d: %4d %4d\n",ii,RAW[alp1[ii]].obsnum,RAW[alp2[ii]].obsnum);
  } else {
    printf("%5d: %4d\n",ii,RAW[alp1[ii]].obsnum);
  }
}

/* Match objects with star, flat, and arc lamps.  Write to "makee.script". */
outfu = fopen("makee.script","w");
printf("\nNOTE: Writing 'makee.script'.\n\n");
fprintf(outfu,"# \n");
for (ii=0; ii<RAW[0].num; ++ii) {
if ((strcmp(RAW[ii].obstype,"Object")==0)||
    (strcmp(RAW[ii].obstype,"Trace") ==0)) {

/* Find best trace exposure for this object. */
  lo = 1073741824;
  jjlo = -1;
  for (jj=0; jj<RAW[0].num; ++jj) {
  if (strcmp(RAW[jj].obstype,"Trace") == 0) {
    diff = ABS((RAW[ii].utime - RAW[jj].utime));
    if (diff < lo) {
      lo = diff;
      jjlo = jj;
    }
  }}
  if (jjlo < 0) {
    fprintf(stderr,"ERROR: esipipe: No Trace Star exposures found.\n");
    exit(0);
  }
  iiTrace = jjlo;


/* Find best flat sum for this object. */
  lo = 1073741824;
  fflo = -1;
  for (ff=0; ff<nfsf; ++ff) {
    jj = ii_fsf[ff];
    if (strcmp(RAW[ii].slmsknam,RAW[jj].slmsknam) == 0) {
      diff = ABS((RAW[ii].utime - RAW[jj].utime));
      if (diff < lo) {
        lo = diff;
        fflo = ff;
      }
    }
  }
/* If no best flat found, relax slit mask constraint. */
  if (fflo < 0) {
    for (ff=0; ff<nfsf; ++ff) {
      jj = ii_fsf[ff];
      diff = ABS((RAW[ii].utime - RAW[jj].utime));
      if (diff < lo) {
        lo = diff;
        fflo = ff;
      }
    }
    printf("NOTE: Using flat with different slit for obs#%d\n",RAW[ii].obsnum);
  }
/* Error if no flat sum found, abort. */
  if (fflo < 0) {
    fprintf(stderr,"ERROR: esipipe: No flat sums found.\n");
    exit(0);
  }
  iiFlat = fflo;
    

/* Find best arc lamp pair for this object. */
  lo = 1073741824;
  fflo = -1;
  for (ff=0; ff<nalp; ++ff) {
    jj = alp1[ff];
    diff = ABS((RAW[ii].utime - RAW[jj].utime));
    if (diff < lo) {
      lo = diff;
      fflo = ff;
    }
  }
  if (fflo < 0) {
    fprintf(stderr,"ERROR: esipipe: No arc lamp pairs found.\n");
    exit(0);
  }
  iiArc = fflo;
    

/* Print execution command. */
  strcpy(wrd,RAW[ii].file);
  jj = cindex(wrd,".fits");
  if (jj > 0) { wrd[jj]='\0'; }
  strcat(wrd,".log");
  if (alp2[iiArc] > -1) {
    fprintf(outfu,"makee %s %s %s %s %s raw=%s log=%s %s\n",
                  RAW[ii].file,RAW[iiTrace].file,fsf[iiFlat],
                  RAW[alp1[iiArc]].file, RAW[alp2[iiArc]].file,
                  rawdir, wrd, extopt );
  } else {
    fprintf(outfu,"makee %s %s %s %s raw=%s log=%s %s\n",
                  RAW[ii].file,RAW[iiTrace].file,fsf[iiFlat],
                  RAW[alp1[iiArc]].file,
                  rawdir, wrd, extopt );
  }

}}
fclose(outfu);

free(aa); aa = NULL;
free(bb); bb = NULL;

return(0);
}


/* ----------------------------------------------------------------------
  FORTRAN gateway to esipipe() .
*/
int ffesipipe_ ( char file1[], char file2[], char rawdir[], char extopt[], 
                 int *flatresum )
{
int ii,jj;
jj = *flatresum;
ii = esipipe( file1, file2, rawdir, extopt, jj );
return(ii);
}



/* ----------------------------------------------------------------------
  Rebin a calibrated arc lamp 2D spectrum.
  Input:  aafile   : Input 2D arclamp FITS spectrum file.
          bbfile   : Output 2D arclamp FITS spectrum file.
          binning  : Re-binning value, e.g. 2,3,4,5,...
  Returns "1" if all ok.
*/
int esi_rebinarc(char aafile[], char bbfile[], int binning)
{
/**/
char wrd[100];
char line[100];
int npt,pix,iok,order,ncoef;
int ii,sc,ec,sr,er,nc,nr,col,row,pixno,newnc;
/**/   
double coef[99],xv,x8[9000],y8[9000],w8[9000],high,wrms;
/**/   
const int headersize = 57600;
char header[headersize];
/**/   
const int maxpix = 240000;
float aa[maxpix],sum;
/**/   

/* Read first file. */
printf("NOTE: Reading %s .\n",bbfile);
iok = creadfits(aafile,aa,maxpix,header,headersize);
if (iok != 1) {
  fprintf(stderr,"***ERROR: esi_rebinarc:  Cannot read %s .\n",aafile);
  return(0);
}

/* Dimensions. */
cGetDimensions(header,headersize,&sc,&ec,&sr,&er,&nc,&nr);

/* Rebin data. */
newnc = nc / binning;
printf("NOTE: Rebinning by %d. Old #cols=%d. New #cols=%d.\n",binning,nc,newnc);
for (row=0; row<nr; ++row) {
  for (col=0; col<nc; col=col+binning) {
    pixno = col + (row * nc);
    sum=0; for (ii=0; ii<binning; ++ii) { sum = sum + aa[(pixno + ii)]; }
    aa[((col / binning) + (row * newnc))] = sum;
  }
}

/* Reset dimensions. */
cinheadset("NAXIS1",newnc,header,headersize);
timegetstring( line );
sprintf(wrd,"Arc spectrum rebinned by %d on %s .",binning,line);
ccheadset("REBINARC",wrd,header,headersize);

/* Re-compute wavelength calibration. */
for (row=1; row<=nr; ++row) {

/* Read reference scale polynomial. */
  esiwave_get_coef(row,header,headersize,coef,&ncoef);

/* Re-assign points. */
  npt=0;
  for (pix=sc; pix<ec; pix=pix+10) {
    xv     = ( (double)pix ) + ( ((double)binning - 1.0) / 2.0 );
    y8[npt]= cpolyval( ncoef, coef, xv );
    x8[npt]= ( (double)pix ) / ( (double)binning );
    w8[npt]= 1.;
    ++npt;
  }

/* Re-fit polynomial. */
  order = 6;
  f2cpolyfitglls_ ( &npt, x8, y8, w8, &order, coef, &iok );
  if (iok != 1) {
    fprintf(stderr,"***ERROR: esi_rebinarc:  FIT failed.\n");
    return(0);
  }

/* Report residuals. */
  f2cpolyfitgllsresiduals_ (&npt,x8,y8,w8,&order,coef,&high,&wrms);
  printf("NOTE: esi_rebinarc:  row=%2d  npt=%3d  high=%12.5e  wrms=%12.5e\n",
                           row,npt,high,wrms);

/* Write coeffecients into new header. */
  esiwave_put_coef(row,header,headersize,coef);

}

/* Write new arc file. */
printf("NOTE: Writing %s .\n",bbfile);
cwritefits( bbfile, aa, header, headersize );

return(1);
}


