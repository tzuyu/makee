C autoid-util.f                       tab  March 1995

CENDOFMAIN

C----------------------------------------------------------------------
C This is the autoid program in the form of a subroutine.
C If running from within another program, you may want to set "-clobber".
C
      subroutine autoid_main(arg,narg,ok)
C
      implicit none
      character*(*) arg(*)
      integer*4 narg
      logical ok
C
      include 'autoid.inc'
      include 'verbose.inc'
      include 'soun.inc'
C
      integer*4 mpix,mpix2
      parameter( mpix = 201000, mpix2 = 2*mpix)
      real*4 valread,desat
      integer*4 file_number
      real*4 a(mpix2),b1(mpix),b2(mpix),x(mpix),r1,r2
      integer*4 i,j,k,lc,inhead,access,numord
      integer*4 sc,ec,sr,er,nc,nr,nc1,nr1,nc2,nr2,xbin,ybin
      integer*4 ros1,ros2,cos1,cos2,fitmode,expos2,reverse
      character*80 arcfile,aldbdir,idfile,cfpsfile,frpsfile,fresfile
      character*80 dbf,wrd,chead,files(2)
      character*5 filt2
      character*57600 header,headers(2)
      real*8 ech1,xd1,ech2,xd2,soft,fhead
      logical nops,writim,user_dbf
      logical findarg,clobber,file_is_open,nofit,listonly

C Specify ID file?
      idfile=' '
      if (findarg(arg,narg,'id=',':',wrd,i)) idfile=wrd
C Specify database file?
      dbf=' '
      if (findarg(arg,narg,'dbf=',':',wrd,i)) dbf=wrd
C Specify user database file?
      user_dbf=.false.
      if (findarg(arg,narg,'uf=',':',wrd,i)) then
        user_dbf=.true.
        dbf = wrd
      endif
C User override on Row Offset.
      UserRos = -999
      if (findarg(arg,narg,'ros=',':',wrd,i))UserRos=nint(valread(wrd))
      ForceRos = findarg(arg,narg,'-forceros',':',wrd,i)
C User override on Column Offset.
      UserCos = -999
      if (findarg(arg,narg,'cos=',':',wrd,i))UserCos=nint(valread(wrd))
C Change weak line limit.
      TooWeak = 50.
      if (findarg(arg,narg,'wll=',':',wrd,i)) TooWeak=valread(wrd)
C Change desaturation limit.
      desat=60000.
      if (findarg(arg,narg,'desat=',':',wrd,i)) desat=valread(wrd)
C Soften database requirement.
      soft = 1.d0
      if (findarg(arg,narg,'-soft',':',wrd,i)) soft=2.d0
C Special (unadvertised) option.
      if (findarg(arg,narg,'-verysoft',':',wrd,i)) soft=10.d0
C Overwrite any existing ID file.
      clobber = findarg(arg,narg,'-clobber',':',wrd,i)
C Verbose?
      verbose = findarg(arg,narg,'-verbose',':',wrd,i)
C Only list database file selection.
      listonly = findarg(arg,narg,'-listonly',':',wrd,i)
C Run final fitting routines.
      nofit   = findarg(arg,narg,'-nofit',':',wrd,i)
C Force use of 1997 and before database (unadvertised).
      Use97   = findarg(arg,narg,'-97',':',wrd,i)
C Fit mode.
      fitmode = 0
      if (findarg(arg,narg,'-fitonly1',':',wrd,i)) fitmode=1
      if (findarg(arg,narg,'-fitonly2',':',wrd,i)) fitmode=2
C Plot?
      nops = findarg(arg,narg,'-nops',':',wrd,i)
C Reverse plot order?
      reverse = +1
      if (findarg(arg,narg,'-reverse',':',wrd,i)) reverse = -1
C Echo syntax.
      if ((narg.ne.1).and.(narg.ne.2)) then
        print *,
     .  'Syntax: autoid (arclamp FITS file)  [2nd arc FITS file(s)]'
        print *,'    [id=file]  [dbf=file]  [uf=file]'
        print *,
     .  '    [ros=]  [cos=]  [-forceros]  [-reverse]  [-listonly]'
        print *,'    [-clobber]  [-verbose]  [-soft]  [wll=]  [-nops]'
        print *,'    [desat=]  [-nofit]  [-fitonly1]  [-fitonly2]'
        print *,
     .  '  AUTOmatically IDentify arc-lines.  Looks in database'
        print *,
     .  '  for a previously calibrated arclamp with a similar setup.'
        print *,'Options:'
        print *,
     .  '   id=file : ID file filename.  Default: (arc filename).ids .'
        print *,
     .  '  dbf=file : Explicitly specify one of the data base files of'
        print *,
     .  '             previously wavelength calibrated FITS files in'
        print *,
     .  '             $MAKEE_DIR/ArcLineDataBase/.  Default: guess.'
        print *,
     .  '   uf=file : Explicitly specify a user FITS file for the'
        print *,
     .  '             correlation instead of a data base file.'
        print *,
     .  '   ros=    : Guess row offset from DataBaseFile.'
        print *,
     .  '   cos=    : Guess column offset from DataBaseFile.'
        print *,
     .  '  -forceros: Force the program to use the "ros=" value.'
        print *,
     .  '  -clobber : Overwrite ID file if it exists.  Default: query.'
        print *,
     .  '  -verbose : Print lots of messages about line IDs.'
        print *,
     .  '  -soft    : Soften requirements for suitable database file.'
        print *,
     .  '   wll=    : Set Weak Line Limit (lines with peaks below this'
        print *,
     .  '             will be ignored.)  Default: 50.'
        print *,
     .  '  -nops    : Do not produce PS plots of correlation function'
        print *,
     .  '             (*-cf.ps) or final fit residuals (*-fr.ps).'
        print *,
     .  '  desat=   : Set saturated pixel level for desaturating'
        print *,
     .  '             columns which have bled due to strong Ar lines.'
        print *,
     .  '             Default: 60000.'
        print *,
     .  '    -nofit : Do not do final identifying and fitting.'
        print *,
     .  ' -fitonly1 : Only do final re-identifications and fitting.'
        print *,' -fitonly2 : Only do final fitting.'
        print *,' -reverse  : Reverse plot order (up,down,right,left).'
        print *,' -listonly : Only show database file selection.'
        print *,' '
        ok=.false.
        return
      endif

C Check verbose and -nops.
      if (verbose.and.(.not.nops)) then
       write(soun,'(a)')
     .  'NOTE: Turning off PostScript Plots with -verbose mode.'
       nops = .true.
      endif

C Check options.
      j=0
      if (nofit) j=j+1
      if (fitmode.gt.0) j=j+1
      if (j.gt.1) then
        write(soun,'(a)') 
     .  'Error- only specify one: -nofit,-fitonly1,-fitonly2.'
        ok=.false.
        return
      endif

C Initialize filenames for PostScript files and final residuals file.
      j=index(arg(1),'.fits')
      if (j.eq.0) then
        frpsfile = arg(1)(1:lc(arg(1)))//'-fr.ps'
        fresfile = arg(1)(1:lc(arg(1)))//'.res'
      else
        frpsfile = arg(1)(1:j-1)//'-fr.ps'
        fresfile = arg(1)(1:j-1)//'.res'
      endif
      if (nops) frpsfile=' '

C Get directory for arc line database.
      if (user_dbf) then
        aldbdir=' '
      else
        call GetMakeeHome(wrd)
        aldbdir = wrd(1:lc(wrd))//'/ArcLineDataBase/'
      endif

C Image number (to be incremented) must be 1 or 2.
      ino=0

C If two files, check to see that they both have the same angles...
C ...also get medians of images and switch order to put stronger lamp first.
      if (narg.gt.1) then
C
        arcfile = arg(1) 
        call AddFitsExt(arcfile)
        call readfits(arcfile,a,mpix2,header,ok)
        if (.not.ok) return
        call GetDimensions(header,sc,ec,sr,er,nc1,nr1)
        call find_median(a,nc1*nr1,r1)
        ech1= fhead('ECHANGL',header)
        xd1 = fhead('XDANGL',header)
C
        arcfile = arg(2) 
        call AddFitsExt(arcfile)
        call readfits(arcfile,a,mpix2,header,ok)
        if (.not.ok) return
        call GetDimensions(header,sc,ec,sr,er,nc2,nr2)
        call find_median(a,nc2*nr2,r2)
        ech2= fhead('ECHANGL',header)
        xd2 = fhead('XDANGL',header)
        write(soun,'(a,4f10.4)')
     .  'NOTE: autoid: Setup angles: ',ech1,xd1,ech2,xd2
C Same setup angles?
        if ((abs(ech1-ech2).gt.0.002).or.(abs(xd1-xd2).gt.0.005)) then
          write(soun,'(a)') 
     .  'ERROR: autoid: Echelle angles differ in two files.'
          ok=.false.
          return
        endif
C Same dimensions?
        write(soun,'(a,4i6)') 
     .  'NOTE: autoid: nc1,nr1,nc2,nr2=',nc1,nr1,nc2,nr2
        if ((nc1.ne.nc2).or.(nr1.ne.nr2)) then
          write(soun,'(a)') 
     .  'ERROR: autoid: Dimensions differ in two images:'
          ok=.false.
          return
        endif
C Swap images?
        write(soun,'(a,2f15.4)') 'NOTE: autoid: Image medians: ',r1,r2
        if (r2.gt.r1) then
          write(soun,'(a)')
     .  'NOTE: autoid: Swapping images to place stronger image first.'
          wrd   = arg(1)
          arg(1)= arg(2)
          arg(2)= wrd
        endif
      endif

C If "-listonly" then only show database selections.
      if (listonly) then
        do i=1,narg
          arcfile = arg(i)
          call AddFitsExt(arcfile)
          call read_fits_header(arcfile,header,ok)
          ech2  = fhead('ECHANGL',header)
          xd2   = fhead('XDANGL',header)
          wrd   = chead('LFILNAME',header)
          filt2 = wrd(1:5)
          call lower_case(filt2)
          expos2= inhead('EXPOSURE',header)
          k     = inhead('NAXIS2',header)
          write(soun,'(a)') '............................'
          write(soun,'(2a)') '  File     Echelle    XD     d',
     .                       'Col  dRow  Filt  Expos  dR   # of orders'
          write(soun,'(1x,a10,2f8.4,a7,a6,2x,a5,i4,a,i5)') 
     .           arcfile,ech2,xd2,
     .          '     - ','    - ',filt2,expos2,'     -  ',k
          call autoid_select_dbf(header,aldbdir,soft,dbf,Use97,ok)
          write(soun,'(a)') '............................'
        enddo
        return
      endif

C Skip to re-identifications and final fitting.
      if (fitmode.gt.0) goto 77

C Output file has not yet been opened.
      file_is_open = .false.

      DO file_number=1,narg

      write(soun,'(a)') ' '

C Read angles of input file.
      arcfile = arg(file_number) 
      call AddFitsExt(arcfile)
      call read_fits_header(arcfile,header,ok)
      if (.not.ok) return
C Get X binning.
      call GetBinning(header,xbin,ybin)
      if (xbin.gt.4) then
        write(soun,'(a,i4)') 
     .  'ERROR: Unable to ID arclamp with x binning of ',xbin
        ok=.false.
        return
      endif
C Angles.
      ech2 = fhead('ECHANGL',header)
      xd2  = fhead('XDANGL',header)
      wrd  = chead('LFILNAME',header)
      filt2= wrd(1:5)
      call lower_case(filt2)
      expos2 = inhead('EXPOSURE',header)
      i    = inhead('NAXIS2',header)
      write(soun,'(2a)') '  File     Echelle    XD     d',
     .                   'Col  dRow  Filt  Expos  dR   # of orders'
      write(soun,'(1x,a10,2f8.4,a7,a6,2x,a5,i4,a,i5)') 
     .           arcfile,ech2,xd2,
     .           '     - ','    - ',filt2,expos2,'     -  ',i

C Is lamp on?
      wrd = chead('LAMPNAME',header)
      if (wrd(1:5).ne.'ThAr1') then
        write(soun,'(2a)') 
     .  'SEVERE WARNING: Lamp name is not "ThAr1" :  ',wrd(1:lc(wrd))
      endif

C If user did not specify the database file, we need to guess.
      if (dbf.eq.' ') 
     .  call autoid_select_dbf(header,aldbdir,soft,dbf,Use97,ok)
      if (.not.ok) return

C Extra blank line for readability.
      write(soun,'(a)') ' '

C Check number of orders.
      numord = inhead('NAXIS2',header)
      if (numord.gt.99) then
        write(soun,'(a)') 'ERROR- too many orders.'
        ok=.false.
        return
      endif
      if (numord.lt.2) then
        write(soun,'(a)') 'ERROR- too few orders.'
        ok=.false.
        return
      endif

C Set up variables for autoid_ArcIDs subroutine.
      write(wrd,'(2a)') aldbdir(1:lc(aldbdir)),dbf(1:lc(dbf))
      call AddFitsExt(wrd)
      files(1) = wrd
      files(2) = arcfile
      ros1=-9999
      ros2=0
      cos1=-9999
      cos2=0
      ino=ino+1

C Correlation function plot PS filename.
      cfpsfile=' '
      if (.not.nops) then
        j=index(arcfile,'.fits')
        if (j.eq.0) then
          cfpsfile = arcfile(1:lc(arcfile))//'-cf.ps'
        else
          cfpsfile = arcfile(1:j-1)//'-cf.ps'
        endif
      endif

C Identify arc-lines.  IDs are in lines() array in common block.
      if (verbose) write(soun,'(a)') 'Running autoid_ArcIDs...'
      call autoid_ArcIDs(files,aldbdir,ros1,ros2,cos1,cos2,
     .                   xbin,cfpsfile,desat,ok)
      if (.not.ok) then
        write(soun,'(a)') 'ERROR in autoid_ArcIDs.'
        return
      endif

C Write ID file.
      if (file_is_open) then
        write(soun,'(2a)') 
     .  ' Appending to ID file: ',idfile(1:lc(idfile))
      else
C Form ID filename.
        if (idfile.eq.' ') then
          j=index(arcfile,'.fits')
          if (j.eq.0) then
            idfile = arcfile(1:lc(arcfile))//'-ids'
          else
            idfile = arcfile(1:j)//'ids'
          endif
        endif
C Does ID file already exist?
        if ((.not.clobber).and.(access(idfile,'r').eq.0)) then
          if (soun.eq.6) then
701         print '(3a,$)',
     .    ' Line ID file ',idfile(1:lc(idfile)),
     .    ' exists.  Overwrite? (1/0) : '
            read(5,*,err=701) j
            if (j.eq.0) then
              print *,'ERROR reading ID file- aborting.'
              ok=.false.
              return
            endif
          else
            write(soun,'(3a)')
     .  ' Line ID file ',idfile(1:lc(idfile)),' exists.  Overwriting.'
          endif
        endif
        open(37,file=idfile,status='unknown')
        write(soun,'(2a)') 
     .  ' Writing new ID file: ',idfile(1:lc(idfile))
        file_is_open=.true.
      endif
      do i=1,nlines
        write(37,'(a)') lines(i)(1:lc(lines(i)))
      enddo
      write(soun,'(i6,a)') nlines,' line IDs written to file.'
      write(soun,'(a,f6.1,a)') 
     .  ' An average of',float(nlines)/float(numord),
     .  ' line IDs per order.'

      ENDDO

      if (file_is_open) close(37)

C Next step: Re-ID lines and fit final scale... unless -nofit specified.
      if (nofit) then
        ok=.true.
        return
      endif

77    continue

      if (fitmode.gt.0) then
C Check ID file.
        if (idfile.eq.' ') then
C Form ID filename.
          arcfile=arg(1)
          call AddFitsExt(arcfile)
          j=index(arcfile,'.fits')
          if (j.eq.0) then
            idfile = arcfile(1:lc(arcfile))//'-ids'
          else
            idfile = arcfile(1:j)//'ids'
          endif
        endif
        if (access(idfile,'r').ne.0) then
          write(soun,'(2a)') 'Error- cannot find:',idfile(1:lc(idfile))
          ok=.false.
          return
        endif
      endif

C (Re)Read data from files.
C Note that the "desat" which applied to "autoid_ArcIDs" is not used below.
      files(1) = arg(1)
      call AddFitsExt(files(1))
      call readfits(files(1),b1,mpix,headers(1),ok)
      call GetDimensions(headers(1),sc,ec,sr,er,nc,nr)
      if (narg.gt.1) then
        files(2)=arg(2)
        call AddFitsExt(files(2))
        call readfits(files(2),b2,mpix,headers(2),ok)
      else
        files(2)=' '
      endif

C Automatic line-re-fitting and wavelength calibration fits.
      call autospim(b1,b2,a,x,sc,ec,sr,er,headers,files,
     .              idfile,writim,frpsfile,reverse,fresfile,fitmode)

C Write out new idfile.
      call WriteIDs(idfile)

C Write the new images out?  (Note that these will not have desat correction.)
      if (writim) then
        wrd=files(1)
        write(soun,'(2a)') ' Over-Writing: ',wrd(1:60)
        call writefits(wrd,a(1),headers(1),ok)
        if (files(2).ne.' ') then
          wrd=files(2)
          write(soun,'(2a)') ' Over-Writing: ',wrd(1:60)
          j = (nc*nr) + 1
          call writefits(wrd,a(j),headers(2),ok)
        endif
      endif

      ok=.true.
      return
      end


C----------------------------------------------------------------------
C Automatic line-re-fitting and wavelength calibration fits.
C
C In: b1,b2,sc,ec,sr,er,headers,files,idfile,frpsfile,reverse,fresfile,fitmode.
C Out: a,x,headers,writim.
C
C First image is in b1, second is in b2.  Dimensions for both are sc,ec,sr,er.
C FITS headers are in headers() and filenames are in files().  Arc line 
C identifications are in idfile, final residuals PostScript plot filename is
C frpsfile (use ' ' if you do not want a plot.)  The normal fitmode is 0, 1
C indicates that no pre-ids were done within this program, 2 indicates that
C no re-identifications and rejections should be done.
C
      subroutine autospim(b1,b2,a,x,sc,ec,sr,er,headers,files,
     .                 idfile,writim,frpsfile,reverse,fresfile,fitmode)

      implicit none
C Main parameters.
      integer*4 sc,ec,sr,er,fitmode
C Either +1 or -1, specifies order on multiple plot output (up-down,right-left).
      integer*4 reverse
      real*4 b1(sc:ec,sr:er),b2(sc:ec,sr:er)
      real*4 a(sc:ec,sr:er,1:2),x(sc:ec,sr:er)
      character*(*) headers(2)
      character*(*) files(2)
      character*(*) idfile,frpsfile,fresfile
      logical writim
C
      include 'spim1.inc'     ! Line ids, etc.
      include 'verbose.inc'
      include 'soun.inc'
C
C Other variables.
      integer*4 ima(2),nrej,npt,grade,grade_limit,soft
      logical ided(maxr),more,ok
      integer*4 ated(maxr),grade_by_own_row,jbest,bestgrade,ated_limit
      real*4 r1,r2,mdn_resid,mdrs(maxr),mdn_resid_cutoff
      real*4 arr1(maxr),arr2(maxr),arr3(maxr),arr4(maxr),arr0(maxr)
      real*4 mdn1,mdn2,mdn3,mdn4
      real*8 r81,r82,thres,orwt,wrms,high,fhead
      integer*4 i,ii,k,j,ino,lastchar,ord,inhead
      character*80 c80, ThArFile, chead
      character c9*9, c4*4

C Threshold to delete all lines.
      mdn_resid_cutoff=0.8
      if (verbose) write(soun,'(a,1pe12.3)') 
     .  'autospim: mdn_resid_cutoff=',mdn_resid_cutoff
      if (verbose) write(soun,'(2a)') 
     .  'frpsfile=',frpsfile(1:lastchar(frpsfile))

C How many files?
      numim=1
      if (files(2).ne.' ') numim=2

C Load a( ) and x( ) arrays.
      do i=sc,ec
        do j=sr,er
          a(i,j,1) = b1(i,j)
        enddo
      enddo
      do i=sc,ec
        do j=sr,er
          a(i,j,2) = b2(i,j)
        enddo
      enddo
      do i=sc,ec
        do j=sr,er
          x(i,j) = float(i)
        enddo
      enddo
C Set ThAr database filename.
      call GetMakeeHome(c80)
      ThArFile = c80(1:lastchar(c80))//'ThAr.dat'
C Zero line-ids.
      do ino=1,2
        do j=1,maxr
          nl(j,ino)=0
          do i=1,maxl
            lc(i,j,ino)=0.d0
            lw(i,j,ino)=0.d0
          enddo
        enddo
      enddo
C Read Thorium-Argon line list.
      open(1,file=ThArFile,status='old')
      nthar=0
11    continue
      read(1,*,end=55) r81,r82,i
      nthar       = nthar+1
      thar(nthar) = r81
      tharI(nthar)= r82
      tharR(nthar)= i
      goto 11
55    close(1)
C Read line ID list.
      if (idfile.eq.' ') then
        idfile='junk.ids'
      else
        call ReadIDs(idfile)
      endif
C Defaults.
      newli = .false.
      newim = .false.
      NoCentroid = .false.
      pgd  = .false.
C Set defaults for weak and saturated lines based on which image is weaker.
C Default if 2 is weaker than 1.
      weakline(1) = 70.
      weakline(2) = 500.
C Do not allow saturated lines in first image.
      satuline(1) = 64000.
C Allow saturated lines in second image.
      satuline(2) = 99000.
C List of images in order of strength.
      ima(1) = 1 
      ima(2) = 2
      if (numim.eq.1) then
C If only one image, allow saturated lines in first (and only) image.
        satuline(1) = 99000.
      else
C Which image is stronger?
        r1=0.
        r2=0.
        do i=sc,ec,10
          do j=sr,er
            r1=r1+a(i,j,1)
            r2=r2+a(i,j,2)
          enddo
        enddo
C Reverse defaults if 1 is weaker than 2.
        if (r1.lt.r2) then
          weakline(2) = 70.
          weakline(1) = 500.
          satuline(2) = 64000.
          satuline(1) = 99000.
          ima(1) = 2
          ima(2) = 1
        endif
      endif

C Skip to final fitting.
      if (fitmode.eq.2) goto 88

C Delete all lines from rows with less than 4 lines.
C This will force an identification of lines using other rows.
      do j=sr,er
        if (nl(j,1).lt.4) nl(j,1)=0
        if (nl(j,2).lt.4) nl(j,2)=0
        ided(j)=.false.
        ated(j)=0
        mdrs(j)=0.
      enddo

C Summary of grades.
      if (verbose) then
        write(soun,'(a)') 'Summary:1:'
        do j=sr,er
          i = grade_by_own_row(sc,ec,j)
          write(soun,'(a,i3,a,2i4,a,i2,a,l,a,i2,a,f5.2)')
     .          'Row=',j,'  # lines=',nl(j,1),nl(j,2),
     .          '   Grade=',i,'  ided=',ided(j),'  ated=',ated(j),
     .          '   mdrs=',mdrs(j)
        enddo
      endif

C ID (using own row) any "good" rows.
      grade_limit=6
8     continue
      if (verbose) write(soun,'(a,i4)') 
     .  ':::: grade_limit=',grade_limit
      do j=sr,er
      if (.not.ided(j)) then
        if (grade_by_own_row(sc,ec,j).ge.grade_limit) then
          do ii=1,numim
            if (verbose) write(soun,'(a,i5,a,i5)') 
     .  'autospim:2: IDing lines in row',j,'  im#',ii
            call IdentifyLines(sc,ec,sr,er,a,x,j,ima(ii),mdn_resid,ok)
            if (ii.eq.1) mdrs(j)=mdn_resid
            if ((.not.ok).and.verbose) write(soun,'(a,i5)') 
     .  'Error identifying lines, row',j
            if (mdn_resid.gt.1.) then
              if (verbose) write(soun,'(a,i5,a,i5,a,f9.4,a)') 
     .                'Problem with row',j,'  image',ii,
     .                '   ...deleting all lines (',mdn_resid,').'
              nl(j,ii)=0
            endif
          enddo
          if (ok.and.(nl(j,1)+nl(j,2).gt.0)) then
            thres = 0.4d0
            if (verbose) write(soun,'(a)') '  CleanFit...'
            call CleanFit(sc,ec,j,thres,.false.,ok)
            if ((.not.ok).and.verbose) write(soun,'(a,i5)') 
     .  'Error rejecting line IDs, row',j
          endif
          if (ok.and.(grade_by_own_row(sc,ec,j).ge.grade_limit)) then
            ided(j)=.true.
          else
            ided(j)=.false.
          endif
        endif
      endif
      enddo

C Check.
      if (verbose) then
        write(soun,'(a)') 'Summary:2:'
        do j=sr,er
          i = grade_by_own_row(sc,ec,j)
          write(soun,'(a,i3,a,2i4,a,i2,a,l,a,i2,a,f5.2)')
     .          'Row=',j,'  # lines=',nl(j,1),nl(j,2),
     .          '   Grade=',i,'  ided=',ided(j),'  ated=',ated(j),
     .          '   mdrs=',mdrs(j)
        enddo
      endif

C Check number of IDed rows.
      i=0
      do j=sr,er
        if (ided(j)) i = i + 1
      enddo
      if ((grade_limit.eq.6).and.(i.lt.6)) then
        grade_limit = grade_limit - 1
        goto 8
      elseif ((grade_limit.eq.5).and.(i.lt.4)) then
        grade_limit = grade_limit - 1
        goto 8
      elseif ((grade_limit.eq.4).and.(i.lt.4)) then
        write(soun,'(a)') 'ERROR- Not enough lines identified.'
        write(soun,'(a)') 
     .  '       Try using "spim" to ID more lines. Aborting.'
        call exit(1)
      endif

C ID all other lines using other rows.
      more=.true.
      DO WHILE(more)

C Check.
      if (verbose) then
        write(soun,'(a)') 'Summary:3:'
        do j=sr,er
          i = grade_by_own_row(sc,ec,j)
          call grade_by_other_rows(sr,er,j,ided,k)
          write(soun,'(a,i3,a,2i4,a,i2,a,i4,a,l,a,i2,a,f5.2)')
     .  'Row=',j,'  # lines=',nl(j,1),nl(j,2),
     .  '   Grade=',i,'  GBOR=',k,'  ided=',ided(j),'  ated=',ated(j),
     .  '   mdrs=',mdrs(j)
        enddo
      endif

C Make at least three attempts to ID all un-IDed rows.
C Find un-IDed row with best grade.
      jbest=-1
      bestgrade = -1
      ated_limit = 0
      do while((jbest.eq.-1).and.(ated_limit.le.2))
        do j=sr,er
          if ((.not.ided(j)).and.(ated(j).le.ated_limit)) then
            call grade_by_other_rows(sr,er,j,ided,grade)
            if (grade.gt.bestgrade) then
              jbest = j
              bestgrade = grade
            endif
          endif
        enddo
        ated_limit = ated_limit + 1
      enddo
      more = (jbest.ne.-1)
C
C Attempt to ID best unIDed row.
      if (more) then
        j = jbest
        if (verbose) then
          write(soun,'(a,i5,a,i5)') 
     .  'Attempt to ID (using other rows) row=',j,', attempt=',ated(j)
        endif
        soft=0
81      continue
        call IdentifyLinesUsingRows(sc,ec,sr,er,a,x,j,ima(1),soft,ok)
        if ((.not.ok).and.(numim.gt.1)) then
          if (verbose) write(soun,'(a,i5,a)') 
     .  'Error IDing row',j,' im#1 ....Try using im#2.'
          call IdentifyLinesUsingRows(sc,ec,sr,er,a,x,j,ima(2),soft,ok)
        endif
        if (.not.ok) then
          if (verbose) write(soun,'(a)') '...Failed attempt.'
          ated(j)=ated(j)+1
        else
C ID lines using own row.
          do ii=1,numim
            if (verbose) write(soun,'(a,i5,a,i5)') 
     .  'IDing lines using own row in row',j,'  im#',ii
            call IdentifyLines(sc,ec,sr,er,a,x,j,ima(ii),mdn_resid,ok)
            if (ii.eq.1) mdrs(j)=mdn_resid
C Non-fatal error.
            if ((.not.ok).and.verbose) write(soun,'(a)') 
     .  'Error ID-ing lines...'
C Fatal error.
            if (mdn_resid.gt.mdn_resid_cutoff) then 
              if (verbose) write(soun,'(a,f9.4,a)')  
     .          'Bad error: mdn_resid=',mdn_resid,', try cleaning...'
              call CleanFit(sc,ec,j,thres,.false.,ok)
              if (verbose) write(soun,'(a)') '...now re-ID lines.'
             call IdentifyLines(sc,ec,sr,er,a,x,j,ima(ii),mdn_resid,ok)
              if (ii.eq.1) mdrs(j)=mdn_resid
              if (mdn_resid.gt.mdn_resid_cutoff) then 
                nl(j,ii)=0
                if (verbose) write(soun,'(a,f9.4,a,2i5)')  
     .  'Bad error: mdn_resid=',mdn_resid,', deleting all lines:',j,ii
                ok=.false.
              else
                if (verbose) write(soun,'(a,f9.4)') 
     .  'New mdn_resid=',mdn_resid
              endif
            endif
          enddo
C Assess grade.
          if (ok.and.(grade_by_own_row(sc,ec,j).gt.6)) then
C Reject bad ID's.
            thres = 0.4d0
            if (verbose) write(soun,'(a,i5)') '  CleanFit for row ',j
            call CleanFit(sc,ec,j,thres,.false.,ok)
C Non-fatal error.
            if ((.not.ok).and.verbose) write(soun,'(a)') 
     .  'Error rejecting line IDs...'
          endif
C Assess grade again.
          if (ok.and.(grade_by_own_row(sc,ec,j).gt.6)) then
            ided(j) = .true.
          else
            ated(j) = ated(j)+1
          endif
        endif
C Second chance.
        if ((soft.eq.0).and.(.not.ok)) then
          soft=1
          ated(j) = ated(j)-1
          if (verbose) write(soun,'(a,i5)') 
     .  'Re-Attempt to ID (using other rows) row=',j
          goto 81
        endif
      endif

      ENDDO
            
C Make a desparate attempt to ID un-ID'ed rows.
      do j=sr,er
        if (.not.ided(j)) then
          do ii=1,numim
            call IdentifyLines(sc,ec,sr,er,a,x,j,ima(ii),mdn_resid,ok)
            if (mdn_resid.gt.mdn_resid_cutoff) nl(j,ii)=0
          enddo
        endif
      enddo

C Loop through all rows once more using other rows to ID (and reject points).
      if (verbose) then
        write(soun,'(a)') ' '
        write(soun,'(a)') 
     .  '::::::::: Final attempt at identifying lines. :::::::::'
        write(soun,'(a)') ' '
      endif

      do j=sr,er
C ID lines using other rows.
        do ii=1,numim
          if (verbose) then
            write(soun,'(a,i5,a,i5)') 
     .  'Identifying lines using other rows in row',j,'  image',ii
          endif
          call IdentifyLinesUsingRows(sc,ec,sr,er,a,x,j,ima(ii),0,ok)
          if ((.not.ok).and.verbose) then
            write(soun,'(a,i5)') 
     .  'Error id-ing lines (using other rows) in row',j
          endif
        enddo
        if (ok) then
C Reject bad ID's.
          thres = 0.4d0
          if (verbose) write(soun,'(a,i5)') '  CleanFit... row=',j
          call CleanFit(sc,ec,j,thres,.false.,ok)
          if ((.not.ok).and.verbose) write(soun,'(a)') 
     .  'Error in rejecting line IDs.'
        endif
      enddo

88    continue

C Final fit residuals PostScript plot.
      if (frpsfile.ne.' ') then
C One plot per row.
        i=1+er-sr
C Add one for header square.
        i=i+1
C Add two summary plots at end.
        i=i+2
        call OpGrid(i,j,k) 
        write(c80,'(2a)') frpsfile(1:lastchar(frpsfile)),'/PS' 
        write(soun,'(2a)') ' Writing PostScript plot file: ',
     .                             frpsfile(1:lastchar(frpsfile))
        call PGBEGIN(0,c80,reverse*j,reverse*k)
        call PGASK(.false.)
        call PGVSTAND
        call PGSLW(1)
        call PGSCF(1)
        call PGSCH(2.0)
C Plot first square giving info on Arc lamp.
        call PGPAGE
        call PGWINDOW(0.,1.,1.,0.)
        call PGBOX('BC',0.,0,'BC',0.,0)
        call GetFitsDate(headers(1),c9,4)
        call PGTEXT(0.1,0.1,c9)
        do ii=1,numim
          call PGTEXT(0.1,(0.00+(0.30*float(ii))),files(ii))
          i  = inhead('OBSNUM',headers(ii))  
          j  = inhead('EXPOSURE',headers(ii))  
          r81= fhead('ECHANGL',headers(ii))
          r82= fhead('XDANGL',headers(ii))
          c80= chead('LFILNAME',headers(ii))
          c4 = c80(1:4)
          write(c80,'(2i4,2a,2f9.4)',err=710) i,j,'s  ',c4,r81,r82
          call PGTEXT(0.1,(0.15+(0.30*float(ii))),c80)
710       continue
        enddo 
        call PGSCH(2.4)
      endif

      if (verbose) then
        write(soun,'(a)') ' '
        write(soun,'(a)') 
     .  '::::::::: Final wavelength scale fitting. :::::::::'
        write(soun,'(a)') ' '
      endif

C Do final wavelength scale fitting.
      open(34,file=fresfile,status='unknown')
      thres= 0.4d0
      orwt = 0.3d0
      k = 0
      do j=sr,er
        call FinalFit(sc,ec,sr,er,j,thres,orwt,headers,frpsfile,
     .                ord,wrms,high,npt,nrej,ok)
        if (.not.ok) write(soun,'(a,i5)') 'ERROR in final fit of row',j
        write(soun,'(a,i3,a,i2,a,f8.4,a,f8.4,a,i3,a,i3)')
     .        ' Final fit: Row=',j,'  Ord=',ord,'  WRMS=',wrms,
     .        '  High=',high,'  NPt=',npt,'  NRej=',nrej
        write(34,'(a,i3,a,i2,a,f8.4,a,f8.4,a,i3,a,i3)')
     .        ' Final fit: ROW=',j,'  ORD=',ord,'  WRMS=',wrms,
     .        '  HIGH=',high,'  NPT=',npt,'  NREJ=',nrej
        lrms(j)= wrms
        lord(j)= ord
        newli  = .true.
        k=k+1
        arr0(k)= float(j)
        arr1(k)= wrms
        arr2(k)= abs(high)
        arr3(k)= float(npt)
        arr4(k)= float(nrej)
      enddo

C Summary plots.

C RMS and High.
        call PGPAGE
        call PGWINDOW(float(sr-2),float(er+2),0.,0.41)
        call PGBOX('BCNST',0.,0,'BCNST',0.,0)
        call PGLINE(k,arr0,arr1)
        call PGPT(k,arr0,arr2,4)
        call PGSLS(2)
        call PG_HLine(float(sr-1),float(er+1),0.15)
        call PGSLS(1)
        call PGMTEXT('T',0.2,0.5,0.5,'WRMS and High Resid. vs. row#')

C Number of points, rejected.
        call PGPAGE
        r1=0.
        do i=1,k
          if (arr3(i).gt.r1) r1=arr3(i)
          if (arr4(i).gt.r1) r1=arr4(i)
        enddo
        call PGWINDOW(float(sr-2),float(er+2),-0.07*r1,r1*1.07)
        call PGBOX('BCNST',0.,0,'BCNST',0.,0)
        call PGLINE(k,arr0,arr3)
        call PGPT(k,arr0,arr4,4)
        call PGSLS(1)
        call PGMTEXT('T',0.2,0.5,0.5,'# of points used, rejected')

C Final report.
      call find_median(arr1,k,mdn1)
      call find_median(arr2,k,mdn2)
      call find_median(arr3,k,mdn3)
      call find_median(arr4,k,mdn4)
      write(soun,'(2x)')
      write(soun,'(a,a,3f6.2,a,3f6.2,a,3i3,a,3i2)') 'Lo,Mdn,Hi:',
     .      '  R=',arr1(1),mdn1,arr1(k),
     .      '  H=',arr2(1),mdn2,arr2(k),
     .      '  Pt=',nint(arr3(1)),nint(mdn3),nint(arr3(k)),
     .      '  Rj=',nint(arr4(1)),nint(mdn4),nint(arr4(k))
      write(soun,'(2x)')
      write(34,'(2x)')
      write(34,'(a,a,3f6.2,a,3f6.2,a,3i3,a,3i2)') 'Lo,Mdn,Hi:',
     .      '  R=',arr1(1),mdn1,arr1(k),
     .      '  H=',arr2(1),mdn2,arr2(k),
     .      '  Pt=',nint(arr3(1)),nint(mdn3),nint(arr3(k)),
     .      '  Rj=',nint(arr4(1)),nint(mdn4),nint(arr4(k))
      write(34,'(2x)')
C
      writim = .true.
      close(34)

C Close PS file.
      if (frpsfile.ne.' ') call PGEND

      return
      end


C----------------------------------------------------------------------
C Decide what is the best number of horizontal and vertical plots.
      subroutine OpGrid(n,h,v)
      implicit none
      integer*4 n,h,v
C This combination is guaranteed to fit.
      h=int(sqrt(float(n)-0.1))+1
      v=int(sqrt(float(n)-0.1))+1
C Try one less horizontal.
      if (((h-1)*v).ge.n) h=h-1
      return
      end


C---------------------------------------------------------------------- 
C Select the best template arc lamp spectrum.
C
      subroutine autoid_select_dbf(header,aldbdir,usoft,dbf,Use97,ok)
C
      implicit none
      character*(*) header   ! FITS header of input spectrum (input).
      character*(*) aldbdir  ! Arc line database directory (input).
      real*8 usoft           ! scale factor on shift limits (input)
      character*(*) dbf      ! Filename of best template arc file (output).
      logical Use97          ! Force use of 1997 and before database (input).
      logical ok             ! Success. (output)
C
      include 'verbose.inc'
      include 'soun.inc'
C
      integer*4 m
      parameter(m=900)
      real*8 echa(m),xda(m),xps(m),rs(m),sumnorm(m),ech0,xd0,soft
      real*4 arr(m)
      integer*4 expos(m),key(m),nord(m),n,bestkey,lc,i,j,mode,expos0
      integer*4 access,inhead,iter
      character*80 aldbf(m),wrd,chead,listfile
      character*5 filt(m),eqvfilt(m),eqvfilt0,filt0
      character*57600 temphead
      character c10*10, c3*3, c5*5
      real*8 fhead,r1,r2
C
C Set limits on x-pixel-shift and row-shift.
      real*8 xpslim,rslim
      parameter(xpslim=400.0,rslim=4.1)
C
C Copy scale factor.
      soft = usoft
C
C Assume the best.
      ok=.true.
C Get header cards.
      ech0= fhead('ECHANGL',header)
      xd0 = fhead('XDANGL',header)
      filt0= chead('LFILNAME',header)
      call lower_case(filt0)
      eqvfilt0 = filt0
      if (filt0(1:2).eq.'bg') eqvfilt0='clear'
      expos0 = inhead('EXPOSURE',header)
C Get mode and select appropriate database list.
      call autoid_ArcMode(header,aldbdir,mode)
      if (Use97) mode=-1
      if (mode.eq.-1) listfile= 'adrd97.list'
      if (mode.eq.0) listfile = 'adrd.list'
      if (mode.eq.1) listfile = 'aduv.list'
      if (mode.eq.2) listfile = 'adrd2o.list'
      if (mode.eq.3) listfile = 'aduv2o.list'
      listfile = aldbdir(1:lc(aldbdir))//listfile
C Does list file exist?
      if (access(listfile,'r').ne.0) then
        if ((mode.eq.-1).and.(.not.Use97)) then
          listfile = 'adrd.list'
          if (access(listfile,'r').ne.0) then
            write(soun,'(2a)') 
     .  'ERROR: List file does not exist: ',listfile(1:lc(listfile))
            write(soun,'(a)') 
     .  'ERROR: Neither adrd97.list nor adrd.list exist.'
            ok=.false.
            return
          else
            if (verbose) write(soun,'(a)') 
     .  'NOTE: Using adrd.list for pre-1998 data.'
          endif
        else
          write(soun,'(2a)') 
     .  'ERROR: List file does not exist: ',listfile(1:lc(listfile))
          ok=.false.
          return
        endif
      else
        if (verbose) then
          write(soun,'(2a)') 
     .  'Select database arclamp from: ',listfile(1:lc(listfile))
        endif
      endif

C Iterate on selection limits.
      iter=1
4     continue
      if (verbose) write(soun,'(a,i5)') 'asdbf: iteration=',iter
C Read parameters for database files from list.
      open(1,file=listfile,status='old') 
      n=0
11    continue
      read(1,'(i4,2x,a10,2f8.4,2x,a3,7x,2x,a5,i5)',end=15,err=810)
     .       i,c10,r1,r2,c3,c5,j
      call lower_case(c5)
      n=n+1
      aldbf(n)= ' '
      aldbf(n)(1:10) = c10
      echa(n) = r1
      xda(n)  = r2
      filt(n) = c5
      expos(n)= j
C Set equivalent filter.
      if (c5(1:2).eq.'bg') c5='clear'
      eqvfilt(n) = c5
C Estimate x-pixel-shift and row-shift.
      call echxdsh(r1,r2,ech0,xd0,xps(n),rs(n),mode,ok)
      if (.not.ok) return
C Is this a suitable file?
      if ((abs(xps(n)).gt.soft*xpslim).or.
     .     (abs(rs(n)).gt.soft*rslim)) then
        n=n-1
        goto 11
      endif
C More stringent criteria for first iteration.
      if (iter.eq.1) then
        if ( (eqvfilt0(1:lc(eqvfilt0)).ne.
     .          eqvfilt(n)(1:lc(eqvfilt(n)))).or.
     .       (expos0.ne.expos(n)) ) then
          n=n-1
          goto 11
        endif
      endif
C Quadrature estimate of shift.
      sumnorm(n)=sqrt( (xps(n)/xpslim)*(xps(n)/xpslim)
     .                 + (rs(n)/rslim)*(rs(n)/rslim) )
C Bonus for same filter.
      if(eqvfilt0(1:lc(eqvfilt0)).eq.eqvfilt(n)(1:lc(eqvfilt(n)))) then
        sumnorm(n)= sumnorm(n)/2.
C Bonus for same filter AND exposure.
        if (expos0.eq.expos(n)) sumnorm(n)= sumnorm(n)/1.5
      endif
      goto 11
15    continue
      close(1)
C Any suitable files?
      if (n.eq.0) then
        if (iter.eq.1) then
          iter=iter+1
          goto 4
        elseif (iter.ge.7) then
          write(soun,'(a)') 
     .  'ERROR- No suitable arc line database files found.'
          write(soun,'(a)') '       (You can try using -soft option.)'
          ok=.false.
          return
        else
          if (verbose) write(soun,'(a,1pe15.7)') 
     .  'asdbf: soften limits:',soft
          iter=iter+1
          soft=soft*1.2
          goto 4
        endif
      endif
C Sort by normalized sum of echelle angle shift and row position shift,load key.
      do i=1,n
        key(i)=i
        arr(i)=sngl(sumnorm(i))
      enddo
      call qcksrtkey(n,arr,key)
C Choose first key...
      bestkey=key(1)
C Report results.
      if (verbose) write(soun,'(3a)') 
     .      ' Suitable files (from ',listfile(1:lc(listfile)),' ):'
      do j=1,min(10,n)
        i=key(j)
C Try to read the header from all the suitable database files.
        wrd = aldbdir(1:lc(aldbdir))//aldbf(i)(1:lc(aldbf(i)))//'.fits'
        call read_fits_header(wrd,temphead,ok)
        if (.not.ok) then
          write(soun,'(3a)') 
     .  ' Error reading file: ',aldbf(i)(1:lc(aldbf(i))),'.fits'
          call exit(1)
        endif
C Number of orders.
        nord(i) = inhead('NAXIS2',temphead)
        if (verbose) then
          write(soun,'(1x,a10,2f8.4,f7.1,f6.1,2x,a5,i4,f8.4,i5)') 
     .  aldbf(i),echa(i),xda(i),xps(i),rs(i),filt(i),
     .  expos(i),sumnorm(i),nord(i)
        endif
      enddo
      write(soun,'(3a)') 
     .  'Best match (from ',listfile(1:lc(listfile)),' ):'
      i=bestkey
      write(soun,'(1x,a10,2f8.4,f7.1,f6.1,2x,a5,i4,f8.4,i5)') 
     .  aldbf(i),echa(i),xda(i),xps(i),rs(i),
     .  filt(i),expos(i),sumnorm(i),nord(i)
C Final result.
      dbf = aldbf(i)
      return
810   write(soun,'(a)') 
     .  'asdbf: Error involving arc-line database listing file.'
      ok=.false.
      return
      end

C----------------------------------------------------------------------
C Convert DD/MM/YY format to decimal year.  Y2K OK.
C  Input:    card :  card name for date such as "DATE-OBS".
C          header :  FITS header.
C Output: yearobs : decimal year, such as 1999.5264
C
      subroutine autoid_date2year(card,header,yearobs)
C
      implicit none
      character*(*) card
      character*(*) header
      real*4 yearobs
      integer*4 year,year2,month,day,NDAYS(12)
      data NDAYS / 0,31,59,90,120,151,181,212,243,273,304,334 /
      call ReadFitsDate(card,header,year,year2,month,day)
      yearobs = float(year) +  float(NDAYS(month) + day - 1)/365.25
      return
      end
  
C----------------------------------------------------------------------
C Find mode: 0=RED XD, 1=UV XD, 2=RED XD in 2nd order, 3=UV XD 2nd order.
C           -1=RED XD prior to December 31, 1997.
C
      subroutine autoid_ArcMode(header,aldbdir,mode)
      implicit none
      character*(*) header     ! FITS header (input).
      character*(*) aldbdir    ! arcline database directory (input).
      integer*4 mode           ! flag for data type (output).

      character*80 chead,xdispers,fil2name,wrd
      real*4 yearobs
      integer*4 lc,access
C
      xdispers = chead('XDISPERS',header)
      call upper_case(xdispers)
      if (lc(xdispers).lt.1) xdispers='RED'
      fil2name = chead('FIL2NAME',header)
      call upper_case(fil2name)
      if (lc(fil2name).lt.1) fil2name='CLEAR'
      call autoid_date2year('DATE-OBS',header,yearobs)
      if (yearobs.lt.1900.) yearobs=1998.1
C
      mode=0
      if (index(xdispers,'UV').gt.0) mode=mode+1
      if (index(fil2name,'CUSO').gt.0) mode=mode+2
      if ((mode.eq.0).and.(yearobs.lt.1998.)) mode=mode-1
C
C If mode=-1, but no adrd97.list file exists, use mode=0.
      if (mode.eq.-1) then
        wrd = aldbdir(1:lc(aldbdir))//'adrd97.list'
        if (access(wrd,'r').ne.0) mode=0
      endif
C
      return
      end

C----------------------------------------------------------------------
C Create line id list for 2nd file (new), using wavelength calibrated 1st
C file (reference).  The only output is lines(*),nlines via the common block.
C If ros1 and/or cos1 is equal to -9999, then a guess will be made for
C the shifts.
C
      subroutine autoid_ArcIDs(files,aldbdir,ros1,ros2,cos1,cos2,
     .                         xbin,cfpsfile,desat,ok)
C
      implicit none
      character*(*) files(2)             ! FITS files (input)
      character*(*) aldbdir              ! arclamp database directory (input)
      integer*4 ros1,ros2,cos1,cos2      ! row and column offsets (input)
      integer*4 xbin                     ! X binning factor (input)
      real*4 desat                       ! saturation limit (input)
      logical ok                         ! success? (output)
C
      include 'autoid.inc'
      include 'verbose.inc'
      include 'soun.inc'
C
      integer*4 asc,aec,asr,aer,bsc,bec,bsr,ber,i,j
      integer*4 nc,nr,inhead,nbad
      integer*4 col_os,col_ose,row_os,col_osr
      integer*4 obs(2),mode(2),lc
      real*8 fhead,echa(2),xda(2),xps,rs
      character*57600 header(2)
      character*80 lamp(2),lfil(2),chead,cfpsfile,wrd
      integer*4 mp
      parameter(mp=201000)
      real*4 a(mp),aw(mp),b(mp),c(mp)

C Assume the best.
      ok=.true.
C Read intensity reference file.
      call AddFitsExt(files(1))
      if (verbose) write(soun,'(2a)') 
     .  'Reading: ',files(1)(1:lc(files(1)))
      call readfits(files(1),a,mp,header(1),ok)
      if (.not.ok) then
        write(soun,'(2a)') 'Error reading fits file: ',files(1)
        return
      endif
      nc = inhead('NAXIS1',header(1))
      nr = inhead('NAXIS2',header(1))
      asc= inhead('CRVAL1',header(1))
      asr= inhead('CRVAL2',header(1))
      aec= asc + nc - 1
      aer= asr + nr - 1
C Desaturate bleeding columns.
      call arcdesat(a,c,asc,aec,asr,aer,desat,nbad)
      if (nbad.gt.0) write(soun,'(i6,2a)') nbad,
     .     ' bad (saturated and bleeding) pix',
     .     'els found in reference arclamp.'

C Create wavelengths reference data image.
      call load_aw(aw,asc,aec,asr,aer,header(1))

C Read new arc lamp fits file...
      call AddFitsExt(files(2))
      if (verbose) write(soun,'(2a)') 
     .  'Reading: ',files(2)(1:lc(files(2)))
      call readfits(files(2),b,mp,header(2),ok)
      if (.not.ok) then
        write(soun,'(2a)') 'Error reading fits file: ',files(2)
        return
      endif
      nc = inhead('NAXIS1',header(2))
      nr = inhead('NAXIS2',header(2))
      bsc= inhead('CRVAL1',header(2))
      bsr= inhead('CRVAL2',header(2))
      bec= bsc + nc - 1
      ber= bsr + nr - 1
C Desaturate bleeding columns.
      call arcdesat(b,c,bsc,bec,bsr,ber,desat,nbad)
      if (nbad.gt.0) write(soun,'(i6,a)') nbad,
     .     ' bad (saturated and bleeding) pixels found in new arclamp.'

C Bin to match.
      if (xbin.gt.1) then
        write(soun,'(a,i4)')'Rebinning reference arc to xbin =',xbin
        call autoid_bin_match( xbin,a,aw,asc,aec,asr,aer )
      endif

C Check things.
      write(soun,'(a)') 
     .  ' Obs#  Echelle  X.D.   Lamp    Filter  Filename'
C                  12345----+---12345678xx----+---12345678----+----1----+----2
      do i=1,2
        obs(i) = inhead('OBSNUM',header(i))
        echa(i)= fhead('ECHANGL',header(i))
        xda(i) = fhead('XDANGL',header(i))
        lamp(i)= chead('LAMPNAME',header(i))
        lfil(i)= chead('LFILNAME',header(i))
        J = index(files(i),'ArcLineDataBase')
        if (j.eq.0) j=1
        wrd = files(i)(j:)
        write(soun,'(i5,2f8.4,2x,2(a8),a40)')
     .         obs(i),echa(i),xda(i),lamp(i),lfil(i),wrd
C Find mode: -1,0=RED XD, 1=UV XD, 2=RED XD in 2nd order, 3=UV XD 2nd order.
        call autoid_ArcMode(header(i),aldbdir,mode(i))
      enddo

C Override.
      if (Use97) mode(2)=-1

C Must be same setup mode.
      if (mode(1).ne.mode(2)) then
        write(soun,'(a,2i5)') 
     .     ' ERROR-- setup modes differ between files:',mode(1),mode(2)
        ok=.false.
        return
      endif

C If the row offset or column offset has not been given, guess.
      if ((ros1.lt.-9998).or.(cos1.lt.-9998)) then
C Estimate x-pixel-shift and row-shift.
        call echxdsh(echa(1),xda(1),echa(2),xda(2),xps,rs,mode(1),ok)
        if (.not.ok) then
          write(soun,'(a)') 'ERROR determing echelle pixel offset.'
          return
        endif
        if (ros1.lt.-9998) then
          if (verbose) write(soun,'(a)') 'Guessing row offset.'
          ros1 = nint(rs) - 3
          ros2 = nint(rs) + 3
        endif
        if (cos1.lt.-9998) then
          if (verbose) write(soun,'(a)') 'Guessing column offset.'
          cos1 = nint(xps) - 120
          cos2 = nint(xps) + 120
        endif
      endif

C Let user override.
      if (UserRos.gt.-999) then
        if (verbose) write(soun,'(a)') 
     .  'User overrides row offset guess.'
        if (ForceRos) then
          ros1 = UserRos
          ros2 = UserRos
        else
          ros1 = UserRos - 2
          ros2 = UserRos + 2
        endif
      endif
      if (UserCos.gt.-999) then
        if (verbose) write(soun,'(a)') 
     .  'User overrides column offset guess.'
        cos1 = UserCos - 60
        cos2 = UserCos + 60
      endif

C Echo to user.
      if (verbose) then
        write(soun,'(2(a,i5))') 
     .  'Initial guess row offset    = ',ros1,' to',ros2
        write(soun,'(2(a,i5))') 
     .  'Initial guess column offset = ',cos1,' to',cos2
      endif
      if (cos2-cos1.lt.20) then
        write(soun,'(a)') 
     .  'ERROR- "cos=" range must be at least 20 pixels.'
        call exit(1)
      endif

C Find offsets.
      call autoid_FindOffsets(a,asc,aec,asr,aer,b,bsc,bec,bsr,ber,
     .       ros1,ros2,cos1,cos2,cfpsfile,col_os,col_ose,row_os,ok)
      if (verbose) write(soun,'(a,3i5)') 
     .  'Using: col_os,col_ose,row_os=',col_os,col_ose,row_os
      if (.not.ok) return

C Column offset range (within an order).
C This value is estimated from prior trials with HIRES data.
      col_osr = 20

C Find IDs.
      call Find_IDs(a,b,aw,asc,aec,asr,aer,bsc,bec,bsr,ber,
     .              row_os,col_os,col_osr)

999   return
      end


C----------------------------------------------------------------------
C Rebin reference images to match the binning of the new arclamp image.
C
      subroutine autoid_bin_match( xbin,a,aw,asc,aec,asr,aer )
C
      implicit none
      integer*4 xbin            ! x binning factor of new arclamp (input)
      real*4 a(*)               ! reference arclamp image (in/out)  
      real*4 aw(*)              ! reference arclamp wavelength image (in/out)  
      integer*4 asc,aec,asr,aer ! ref. image dimensions (in/out)
C
      integer*4 row,col,nr,nc,ii,pixno,newcol,nnc
      real*4 sum,wsum
C
      nc = 1 + aec - asc         ! old number of columns
      nr = 1 + aer - asr         ! old (and new) number of rows
      nnc= nc / xbin             ! number of new columns
      do row=1,nr
        do col=1,nc,xbin
C Sum up old data over binning width.
          sum =0.
          wsum=0.
          do ii=col,(col + xbin - 1)
            pixno = ii + ((row-1) * nc)
            sum = sum + a(pixno)
            wsum= wsum+ aw(pixno)
          enddo
C New data.
          newcol = 1 + ((col - 1) / xbin)
          if (newcol.le.nnc) then
            pixno = newcol + ((row-1) * nnc)
            a(pixno) = sum  / float(xbin)
            aw(pixno)= wsum / float(xbin)
          endif
        enddo
      enddo
C
      asc = asc / 2
      aec = asc + nnc - 1
C
      return
      end


C----------------------------------------------------------------------
C Find IDs.    BCOL = ACOL + COFF  ;  BROW = AROW + ROFF
C
      subroutine Find_IDs(a,b,aw,asc,aec,asr,aer,bsc,bec,bsr,ber,
     .  ros,coff,coffr)
C
      implicit none
      integer*4 bsc,bec,bsr,ber,asc,aec,asr,aer   ! dimensions (input).
      real*4 a(asc:aec,asr:aer)   ! reference spectrum (input).
      real*4 b(bsc:bec,bsr:ber)   ! new arc lamp data spectrum (input).
      real*4 aw(asc:aec,asr:aer)  ! reference spectrum wavelengths (input).
      integer*4 ros               ! best guess at row offset (input).
      integer*4 coff              ! best guess at column offset (input).
      integer*4 coffr             ! column offset range across an order (input).
C
      include 'autoid.inc'
      include 'verbose.inc'
C
      integer*4 maxdb,m
      parameter(maxdb=9000,m=9000)
      integer*4 nthar,tharR(maxdb)
      real*8 thar(maxdb),tharI(maxdb),r81,r82
      character*80 ThArFile,c80
      real*4 aa(m),aaww(m),bb(m)
      integer*4 i,n,brow,row,uasc,uaec,ubsc,ubec,lc

C Set ThAr database filename.
      call GetMakeeHome(c80)
      ThArFile = c80(1:lc(c80))//'ThAr.dat'

C Read Thorium-Argon line list.
      open(1,file=ThArFile,status='old')
      nthar=0
11    continue
      read(1,*,end=55) r81,r82,i
      nthar       = nthar+1
      thar(nthar) = r81
      tharI(nthar)= r82
      tharR(nthar)= i
      goto 11
55    close(1)

C Initialize lines(*) array.
      nlines=0

C Look at each row.
      DO brow=bsr,ber
      row=brow-ros
      if ((row.ge.asr).and.(row.le.aer)) then
C Find usuable range of values.
        i=asc
        do while((i.lt.aec).and.(abs(a(i,row)).lt.1.e-20))
          i=i+1
        enddo
        uasc=i
        i=aec
        do while((i.gt.asc).and.(abs(a(i,row)).lt.1.e-20))
          i=i-1
        enddo
        uaec=i
        i=bsc
        do while((i.lt.bec).and.(abs(b(i,brow)).lt.1.e-20))
          i=i+1
        enddo
        ubsc=i
        i=bec
        do while((i.gt.bsc).and.(abs(b(i,brow)).lt.1.e-20))
          i=i-1
        enddo
        ubec=i
C Transfer rows into temporary arrays.
        n=0
        do i=uasc,uaec
          n=n+1
          aa(n)  = a(i,row)
          aaww(n)= aw(i,row)
        enddo
        n=0
        do i=ubsc,ubec
          n=n+1
          bb(n) = b(i,brow)
        enddo
C Identify lines for this row and load them into lines(*) array.
        call idfind(aa,aaww,uasc,uaec,bb,ubsc,ubec,brow,
     .  coff,coffr,nthar,thar)
      endif
      ENDDO

      return
      end

C----------------------------------------------------------------------
C Identify lines for given row and load them into lines(*) array.
C
      subroutine idfind(a,aw,as,ae,b,bs,be,brow,coff,coffr,nthar,thar)
C
      implicit none
      integer*4 as,ae,bs,be,brow,coff,coffr,nthar
      real*4 a(as:ae),aw(as:ae),b(bs:be)
      real*8 thar(nthar)
C
      include 'autoid.inc'
      include 'verbose.inc'
      include 'soun.inc'
C
      real*4 cent,strength,fwhm
      integer*4 k1,k2,neari,neari_bs8,k,col,ols,ole,ncr,pcoff,pcoffr
      character c80*80
      logical ok

C Overlap (in terms of the a array.)
      ols = max(as,bs-coff)
      ole = min(ae,be-coff)
C Arc line wavelength limits.
      k1 = neari_bs8(dble(aw(ols)),nthar,thar)
      k2 = neari_bs8(dble(aw(ole)),nthar,thar)
C Look for all the lines.
      do k=k1,k2
C What column does this arc line correspond to?
        col = -1 + as + neari(sngl(thar(k)),1+ae-as,aw(as))
C Is the column within the bounds?
        if ((col.gt.ols).and.(col.lt.ole)) then
          pcoff = coff
          pcoffr= coffr
C Range in pixels for calculation 80% precentile in precise_shift().
          ncr = 50
C Find a more precise offset for the area of interest.
          call precise_shift(a,b,aw,as,ae,bs,be,col,ncr,pcoff,pcoffr)
C Find centroid near col+pcoff.
          cent = float(col+pcoff)
          call getcent(b,bs,be,cent,strength,fwhm)
C Check validity of line.
          ok=.true.
          if ((strength.lt.1.).and.(cent.lt.1.)) then
            if (verbose) write(soun,'(a)') 
     .  'WARNING: idfind: bad centroid.'
            ok=.false.
          elseif (strength.gt.62000.) then
            if (verbose) write(soun,'(a,f9.2)') 
     .  'WARNING: idfind: line saturated.  Peak=',strength
            ok=.false.
          elseif (strength.lt.TooWeak) then
            if (verbose) write(soun,'(a,f9.2)') 
     .  'WARNING: idfind: line too weak.  Peak=',strength
            ok=.false.
          elseif (fwhm.gt.5.5) then
            if (verbose) write(soun,'(a,f9.3)') 
     .  'WARNING: idfind: line too wide.  FWHM=',fwhm
            ok=.false.
          elseif (abs(cent-float(col+pcoff)).gt.3.) then
            if (verbose) then
              write(soun,'(a,3f8.2)')
     .  'WARNING: idfind: centroid too far from guess:',
     .  cent,float(col+pcoff),cent-float(col+pcoff)
            endif
            ok=.false.
          endif
          if ((.not.ok).and.(verbose)) then
            write(soun,'(a,f10.3,a,i4,a,i4)') 
     .  'WARNING: idfind: failure to ID',
     .  thar(k),' at col=',col+pcoff,'  row=',brow
          endif
          if (ok) then
C
C Add line to line id list.
C
CIm row     pixel    wavel. intensity   wt  resid.  WRMS  ord
C--1234----+----1----+----1----+----112345----+---12345678---
C 1 999 99000.528 99000.391 1234000.1 99.0 -99.000 -99.000 12
C
            c80=' '
            write(c80,'(i2,i4,2f10.3,f10.1,f5.1,f8.3,f8.3,i3)',err=9)
     .          ino,brow,
     .          min(99999.,max(-9.,cent)),
     .          min(99999.,max(-9.,thar(k))),
     .          min(9999999.,max(-9.,strength)),0.,0.,0.,0
            nlines=nlines+1
            lines(nlines)=c80

          endif
        endif
      enddo
      return
9     write(soun,'(a,f10.3)') 'Error writing line id:',thar(k)
      return
      end

C----------------------------------------------------------------------
C Find a more precise measure of the shift using ncr pixels on either
C side of the given column (col), given a guess for the column offset
C (pcoff) and for the range around that guess (pcoffr).
C
      subroutine precise_shift(a,b,aw,as,ae,bs,be,col,ncr,pcoff,pcoffr)
C
      implicit none 
      integer*4 as,ae,bs,be,col,ncr,pcoff,pcoffr
      real*4 a(as:ae),b(bs:be),aw(as:ae)
      integer*4 i,os,m,k,n,hios,hi
      parameter(m=9000)
      real*4 x(m),median,a80,b80
C
      include 'soun.inc'
C

C Find 80% value of a and b array.
      n=0
      do i=max(as,col-ncr),min(ae,col+ncr)
        n=n+1
        x(n)=a(i)
      enddo
      call find_median(x,n,median)
      a80 = x(nint(float(n)*0.8))
      n=0
      do i=max(bs,pcoff+col-ncr),min(be,pcoff+col+ncr)
        n=n+1
        x(n)=b(i)
      enddo
      call find_median(x,n,median)
      b80 = x(nint(float(n)*0.8))
C Find a better shift.
      hi=-1
      hios=0
      do os=pcoff-pcoffr,pcoff+pcoffr
        k=0
        do i=max(as,max(bs-os,col-ncr)),min(ae,min(be-os,col+ncr))
          if ((a(i).gt.a80).and.(b(i+os).gt.b80)) k=k+1
        enddo
        if (k.gt.hi) then
          hi  = k
          hios= os
        endif
      enddo
      pcoff = hios
      return
800   write(soun,'(a)') 'ERROR in precise_shift().'
      call exit(1)
      end

C----------------------------------------------------------------------
C Find line centroid.  Input: b,bs,be,cent (initial guess)
C                     Output: cent,strength,fwhm
C Failure is indicated by all output values equal to zero.
C
      subroutine getcent(b,bs,be,cent,strength,fwhm)

      implicit none
      integer*4 bs,be
      real*4 b(bs:be),cent,strength,fwhm
C
      include 'verbose.inc'
      include 'soun.inc'
C
      real*4 low,sum,wsum,cent_last,high,r4
      integer*4 iter,i,i1,i2

C Initialize iteration count.
      iter=1
C Try again.
1     continue
      cent_last = cent
C Find bounds. Center on cent.
      i1 = max(bs,nint(cent-3.))
      i2 = min(be,nint(cent+3.))
C Find background.
      low=b(i1)
      do i=max(bs,i1-3),min(be,i2+3)
        if (b(i).lt.low) low=b(i)
      enddo
C Find (2nd moment) centroid.
      sum=0.
      wsum=0.
      do i=i1,i2
        r4 = max(0.,b(i)-low)
        r4 = r4*r4
        sum = sum + (r4 * float(i))
        wsum= wsum+ (r4)
      enddo
C Make sure weight sum is greater than zero.
      if (wsum.lt.1.e-5) goto 8
C Centroid.
      cent = max(float(bs),min(float(be),sum/wsum))
C If too far from previous guess, try again.
      if (abs(cent-cent_last).gt.0.01) then
        iter=iter+1
C Check for convergence.
        if (iter.gt.9) then
          if (verbose) then
           write(soun,'(a,f9.2)') 
     .  'getcent: Trouble converging on line near pixel',cent
C          print '(2f12.3)',cent-2.,b(nint(cent-2.))
C          print '(2f12.3)',cent-1.,b(nint(cent-1.))
C          print '(2f12.3)',cent   ,b(nint(cent   ))
C          print '(2f12.3)',cent+1.,b(nint(cent+1.))
C          print '(2f12.3)',cent+2.,b(nint(cent+2.))
          endif
          goto 8
        endif
        goto 1
      endif
C Find strength of line.
      strength = b(nint(cent)) - low
C Find FWHM.
      sum=0.
      high=b(i1)
      do i=max(bs,i1-3),min(be,i2+3)
        if (b(i).gt.high) high=b(i)
        sum = sum + ( b(i) - low )
      enddo
      if (high.lt.1.e-10) goto 8
      fwhm=(0.939437)*sum/high
      return
8     continue
      cent=0.
      strength=0.
      fwhm=0.
      return
      end

C----------------------------------------------------------------------
C Given two sets of angles, give the xpixel shift and the row shift.
C
C Find mode: 0=RED XD, 1=UV XD, 2=RED XD in 2nd order, 3=UV XD 2nd order.
C           -1=RED XD prior to December 31, 1997.
C
      subroutine echxdsh(ech1,xd1,ech2,xd2,xps,rs,mode,ok)
C
      implicit none
      real*8 ech1,xd1,ech2,xd2,xps,rs
      integer*4 mode
      logical ok
      real*8 echxpix,xdorder
C
      include 'soun.inc'
C
C Assume the worst.
      ok=.false.
      if (mode.eq.3) then
        write(soun,'(a)') 
     .  'echxdsh: ERROR- Do not have pix function for UV XD 2nd order.'
        return
      elseif ((mode.gt.3).or.(mode.lt.-1)) then
        write(soun,'(a,i3)') 'echxdsh: ERROR- Unknown mode: ',mode
        return
      endif
C
      xps= echxpix(ech2,mode) - echxpix(ech1,mode)
      rs = xdorder(xd2,mode)  - xdorder(xd1,mode)
      ok = .true.
C
      return
      end

C----------------------------------------------------------------------
C Derived using UCSC Echelle Simulator.
C Return XPIX position of 5940.0 Angstroms (mode 0 and -1).
C Return XPIX position of 4400.0 Angstroms (mode 1).
C
      real*8 function echxpix(echangl,mode)
C
      implicit none
      real*8 echangl,x
      integer*4 mode
C
      include 'soun.inc'
C
      if ((mode.eq.0).or.(mode.eq.-1)) then
C Should this one have an offset?
        echxpix = 1031.498305d0 + (echangl*(-890.446067d0))
     .                 + (echangl*echangl*(21.491916527d0))
      elseif (mode.eq.1) then
        x = echangl - 0.05846d0
        echxpix =  980.732406d0 + (x*(-888.222573d0))
     .                 + (x*x*(19.114296716d0))
      elseif (mode.eq.2) then
        x = echangl - 0.03125d0
        echxpix =  1004.45878d0 + (x*(-888.048024d0))
     .                 + (x*x*(21.235309d0))
      else
        write(soun,'(a,i5)') 'echxpix: ERROR: Unknown mode=',mode
        echxpix = -1.e+20 * echangl
      endif
      return
      end

C----------------------------------------------------------------------
C Derived using UCSC Echelle Simulator.
C Return order# which crosses the bottom of the CCD at the middle column of the
C CCD with ECHANGL=0.0.
C
C Old: Mode=0: Return row shift in the bluest row relative to XD=0.000 .
C
      real*8 function xdorder(xdangl,mode)
C
      implicit none
      real*8 xdangl,x
      integer*4 mode
C
      include 'soun.inc'
C
      if ((mode.eq.0).or.(mode.eq.-1)) then
C
C  (old method) = (7.9818971448912d-02)
C    .          + (-23.554619751641d0  * xdangl)
C    .          + ( 5.3753141024723d0  * xdangl * xdangl)
C    .          + (-0.50422138257483d0 * xdangl * xdangl * xdangl)
C
        x = xdangl - 0.540525d0
        xdorder = ( 68.41180806d0 )
     .          + (-17.02928136d0 * x)
     .          + ( 4.285137307d0 * x * x)
     .          + (-1.159897353d0 * x * x * x)
     .          + ( 0.298400886d0 * x * x * x * x)
     .          + (-0.041293414d0 * x * x * x * x * x)

      elseif (mode.eq.1) then

C Old (from simulator).
C       x = xdangl - 0.782044d0
C       xdorder = ( 94.78266887d0 )
C    .          + (-20.45915127d0 * x)
C    .          + ( 4.482086311d0 * x * x)
C    .          + (-1.021025526d0 * x * x * x)
C    .          + ( 0.171811455d0 * x * x * x * x)

C New (using 7 points from real data taken May 1998 calibrated August 1998.)
        x = xdangl - 1.15400d0
        xdorder = ( 94.70065157d0 )
     .          + (-20.98344715d0 * x)
     .          + ( 4.078266837d0 * x * x)
     .          + (-0.784360445d0 * x * x * x)

      elseif (mode.eq.2) then

C From (Keck HQ) simulator points 7 measured August 25, 1998.
        x = xdangl - 1.731778d0
        xdorder = ( 105.4919135d0 )
     .          + (-20.25635883d0 * x)
     .          + ( 3.920380399d0 * x * x)
     .          + (-0.738225632d0 * x * x * x)

      else
        write(soun,'(a,i5)') 'xdorder: ERROR: Unknown mode=',mode
        xdorder = -1.e+20 * xdangl
      endif
      return
      end

C----------------------------------------------------------------------
C Load wavelength values into "image" for later reference.
C
      subroutine load_aw(aw,sc,ec,sr,er,header)
C
      implicit none
      integer*4 sc,ec,sr,er       ! dimensions (input)
      real*4 aw(sc:ec,sr:er)      ! wavelength image (output)
      character*(*) header        ! FITS header of reference arc (input)
      integer*4 i,j
      real*8 GetSpimWave,r8
C Initialize wavelength scale polynomials.
      r8 = GetSpimWave(header,0.d0,0)
C Load wavelengths into "image".
      do j=sr,er
        do i=sc,ec
          aw(i,j) = sngl(GetSpimWave(header,dfloat(i),j))
        enddo
      enddo
      return
      end


C----------------------------------------------------------------------
C Find offsets given a() and b() image data and a range guess.
C Uses correlation functions.  
C Set cfpsfile to blank if no PostScript file is requested.
C
C Sense of the shift:  BCOL = ACOL + COFF  ;  BROW = AROW + ROFF
C
      subroutine autoid_FindOffsets(a,asc,aec,asr,aer,
     .         b,bsc,bec,bsr,ber,
     .         ros1,ros2,cos1,cos2,cfpsfile,
     .         col_os,col_ose,row_os,ok)
C
      implicit none
C
      integer*4 asc,aec,asr,aer,bsc,bec,bsr,ber  ! dimensions (input).
      real*4 a(asc:aec,asr:aer)   ! template arc lamp data (input).
      real*4 b(bsc:bec,bsr:ber)   ! new arc lamp data (input).
      integer*4 ros1,ros2         ! row offset range guess (input).
      integer*4 cos1,cos2         ! column offset range guess (input).
      character*(*) cfpsfile      ! correlation function PS plot file (input).
C
      integer*4 col_os            ! derived column offset (output).
      integer*4 col_ose           ! RMS error in derived column offset (output).
      integer*4 row_os            ! derived row offset (output).
      logical ok                  ! success? (output).
C
      include 'verbose.inc'
      include 'soun.inc'
C
      include 'maxpix.inc'   ! sets maxpix, maxcol, and maxord.
C
      integer*4 key(maxord),roa_row(maxord)
      real*4 peaks(maxord),fwhms(maxord),xpeaks(maxord),arr(maxord)
      real*4 roa_coff(maxord),roa_rms(maxord),sum,rms,rmslo
      real*4 aa(maxcol),bb(maxcol)
      real*4 scfx(maxcol),scfy(maxcol)
      real*4 satur_a,satur_b,peak,fwhm,xpeak
      integer*4 anc,bnc,iros,brow1,brow2,i,brow,arow,nscf,num,narr
      integer*4 nsum,nroa,nh,nv,lc
      character*80 wrd

C Check row offset range.
      i = nint(float(min(ber-bsr,aer-asr))*0.7)
      if (ros2-ros1.gt.i) then
        write(soun,'(a,i5)') 
     . 'Find_Offsets: ERROR: row offset range is too large:',ros2-ros1
        ok=.false.
        return
      endif

C Number of columns.
      anc = 1+aec-asc
      bnc = 1+bec-bsc

C Determine saturation limit for each arc lamp.
      call autoid_FindSaturation(a,asc,aec,asr,aer,satur_a,ok)
      if (.not.ok) return
      call autoid_FindSaturation(b,bsc,bec,bsr,ber,satur_b,ok)
      if (.not.ok) return

C Echo.
      if (verbose) then
        print *,'asc,aec,asr,aer,anc=',asc,aec,asr,aer,anc
        print *,'bsc,bec,bsr,ber,bnc=',bsc,bec,bsr,ber,bnc
        print *,'ros1,ros2,cos1,cos2=',ros1,ros2,cos1,cos2
        print *,'cfpsfile=',cfpsfile
        print *,'satur_a,satur_b=',satur_a,satur_b
      endif

C Initialize row offset array.
      nroa=0

C Find best column shift, RMS, and correlation FWHM for each row offset guess.
      DO iros=ros1,ros2

C Find column shift for each order individually.
C Only orders which overlap with the template for this given row offset.
      brow1 = min(ber,max(bsr,asr+iros))
      brow2 = min(ber,max(bsr,aer+iros))

C Initialize.
      num=0
C Derive the correlation function for each overlapping row.
      do brow=brow1,brow2
        arow = brow - iros
        do i=asc,aec
          aa(1+i-asc) = a(i,arow)
        enddo
        do i=bsc,bec
          bb(1+i-bsc) = b(i,brow)
        enddo
        call autoid_correlate(anc,aa,satur_a,bnc,bb,satur_b,
     .                        cos1,cos2,xpeak,peak,fwhm,ok,
     .                        scfx,scfy,nscf,.false.)
        if (.not.ok) return
        num=num+1
        xpeaks(num)= xpeak
        peaks(num) = peak
        fwhms(num) = fwhm
        key(num)   = num
        if (verbose) write(soun,'(a,2i5,f12.3,1pe12.3,f10.3)') 
     .                     'brow:',brow,num,xpeak,peak,fwhm
      enddo
C Decide best xpeak (column offset) value and RMS in xpeak.
C First sort by FWHM.
      call qcksrt_subkey(num,fwhms,num,key)
C Keep only xpeaks from best FWHMs (best 90%).
      narr=0
      do i=1,nint(0.9*float(num))
        narr=narr+1
        arr(narr)=xpeaks(key(i))
      enddo
C Sort new xpeaks.
      call qcksrt(narr,arr)
C Average middle 50%.
      nsum=0
      sum =0.
      do i=nint(float(narr)*0.25),nint(float(narr)*0.75)
        nsum= nsum+ 1
        sum = sum + xpeaks(key(i))
      enddo
C RMS.
      if (nsum.gt.0) then
        sum = sum / float(nsum)
        rms=0.
        do i=nint(float(narr)*0.25),nint(float(narr)*0.75)
          rms = rms + ((sum-xpeaks(key(i)))*(sum-xpeaks(key(i))))
        enddo
        rms = sqrt( rms / float(nsum) )
C Load in row-offset-array (roa).
        nroa=nroa+1
        roa_row(nroa)  = iros
        roa_coff(nroa) = sum
        roa_rms(nroa)  = rms
        if (verbose) write(soun,'(a,2i5,2f12.3)') 
     .  '************nroa,row,coff,rms=',nroa,iros,sum,rms
      endif
      
      ENDDO

C Decide on best row offset and load output parameters.
C Note that we return a "five-sigma" RMS, since the true column offset
C across an order varies by much more than this RMS.
      rmslo=roa_rms(1)+1.
      do i=1,nroa
        if (roa_rms(i).lt.rmslo) then
          col_os = nint(roa_coff(i))
          col_ose= roa_rms(i)
          row_os = roa_row(i)
          rmslo  = roa_rms(i)
        endif
      enddo

C Echo.
      write(soun,'(a,9i7)')   'Row offset:',(roa_row(i),i=1,nroa)
      write(soun,'(a,9f7.1)') 'Col offset:',(roa_coff(i),i=1,nroa)
      write(soun,'(a,9f7.1)') '       RMS:',(roa_rms(i),i=1,nroa)
      write(soun,'(a,i7)')    '.....Best row offset=',row_os

C Make a correlation function plot file if requested.
      if (cfpsfile.ne.' ') then
        call EE_OpGrid(1+ber-bsr,nh,nv)
        wrd = cfpsfile(1:lc(cfpsfile))//'/PS'
        write(soun,'(2a)') ' Writing PostScript plot file: ',
     .                                 cfpsfile(1:lc(cfpsfile))
        call PGBEGIN(0,wrd,nh,nv)
        call PGVSTAND
        call PGASK(.false.)
        call PGSCH(2.0)
        iros=row_os
        brow1 = min(ber,max(bsr,asr+iros))
        brow2 = min(ber,max(bsr,aer+iros))
        do brow=brow1,brow2
          arow = brow - iros
          do i=asc,aec
            aa(1+i-asc) = a(i,arow)
          enddo
          do i=bsc,bec
            bb(1+i-bsc) = b(i,brow)
          enddo
          call autoid_correlate(anc,aa,satur_a,bnc,bb,satur_b,
     .                          cos1,cos2,xpeak,peak,fwhm,ok,
     .                          scfx,scfy,nscf,.true.)
          write(wrd,'(a,i3)') 'Row=',brow
          call PGMTEXT('T',0.3,0.5,0.5,wrd)
        enddo
        call PGEND
      endif

      ok=.true.
      return
      end


C----------------------------------------------------------------------
C See autoid_correlate() .
C
      subroutine f2cautoidcorrelate(nc,a,b,cos1,cos2,
     .                              xpeak,peak,fwhm,iok)

C
      implicit none
      integer*4 nc                 ! number of columns of arrays. (input)
      real*4 a(*),b(*)             ! arrays to correlate. (input)
      integer*4 cos1,cos2          ! column offset range to try. (input)
      real*4 xpeak,peak,fwhm       ! best offset, peak value, FWHM. (output)
      integer*4 iok                ! success? (1/0) (output).
C
      real*4 scfx(5000),scfy(5000) ! correlation spectrum.
      integer*4 nscf               ! number of points in corr. spec.
      logical makeplot             ! write out PGPLOT plot data.
      real*4 satur_a,satur_b       ! saturation values.
      integer*4 na,nb
      logical ok
C
C Defaults.
      satur_a = 61000
      satur_b = 61000
      na = nc
      nb = nc
      makeplot = .false.
C Routine.
      call autoid_correlate(na,a,satur_a,nb,b,satur_b,cos1,cos2,
     .                      xpeak,peak,fwhm,ok,scfx,scfy,nscf,
     .                      makeplot)
      if (ok) then
        iok = 1
      else
        iok = 0
      endif
      return
      end



C----------------------------------------------------------------------
C Compute peak and fwhm of correlation function.
C The arrays a and b will be modified, so use scratch arrays on input.
C
      subroutine autoid_correlate(na,a,satur_a,nb,b,satur_b,
     .                            cos1,cos2,xpeak,peak,fwhm,ok,
     .                            scfx,scfy,nscf,makeplot)
C
      implicit none
      integer*4 na,nb              ! dimensions of arrays. (input) 
      real*4 a(na),b(nb)           ! arrays to correlate. (input)
      real*4 satur_a,satur_b       ! saturation values. (input)
      integer*4 cos1,cos2          ! column offset range to try. (input)
      real*4 xpeak,peak,fwhm       ! best offset, peak value, FWHM. (output)
      logical ok                   ! success (output).
      real*4 scfx(*),scfy(*)       ! correlation spectrum (output).
      integer*4 nscf               ! number of points in corr. spec. (output).
      logical makeplot             ! write out PGPLOT plot data (input).
C
      include 'verbose.inc'
      include 'soun.inc'
C
      integer*4 coff,ipeak,i
      real*4 fudge,aback,bback,mwsh1,backscf,area
      parameter(fudge=0.95)
      real*4 xx(5000),yy(5000)
      real*4 ap(11),bp(11),rp(11),scalefactor,wt,rr

C First we determine a scale factor between the arrays, then we scale 
C the weaker image and clip the "saturated" values.
C
C Sort arrays. Find percentile values.
      do i=1,na
        xx(i) = a(i)
      enddo
      call qcksrt(na,xx)
      do i=1,9
        ap(i)= 
     .  xx(max(1,min(na,nint( float(na)*(0.90+(float(i-1)*0.01)) ))))
      enddo
      aback = xx(max(1,min(na,nint( float(na)*0.70 ))))
C b array.
      do i=1,nb
        xx(i) = b(i)
      enddo
      call qcksrt(nb,xx)
      do i=1,9
        bp(i) = 
     .  xx(max(1,min(nb,nint( float(nb)*(0.90+(float(i-1)*0.01)) ))))
      enddo
      bback = xx(max(1,min(nb,nint( float(nb)*0.70 ))))
C Find best percentile ratio.
      do i=1,9
        rp(i) = ap(i)/bp(i)
      enddo
      call qcksrt(9,rp)
      scalefactor = max(0.001,min(1000.,rp(5)))
C Increase weaker spectrum and clip at saturation level.
C Note that we clip at the saturation level of the OTHER spectrum.
      if (scalefactor.lt.1.) then
        do i=1,na
          a(i) = fudge* a(i) / scalefactor
          if (a(i).gt.satur_b) a(i)=satur_b
        enddo
        aback = fudge* aback / scalefactor
      else
        do i=1,na
          b(i) = b(i) * (scalefactor*fudge)
          if (b(i).gt.satur_a) b(i)=satur_a
        enddo
        bback = bback * (scalefactor*fudge)
      endif

C Make correlation spectrum.
      nscf=0
      peak=0.
      ipeak=0
      do coff=cos1,cos2
        nscf=nscf+1
        scfx(nscf) = float(coff)
        scfy(nscf) = mwsh1(a,1,na,b,1,nb,coff,aback,bback)
        yy(nscf)   = scfy(nscf)
        if (scfy(nscf).gt.peak) then
          ipeak=nscf
          peak =scfy(nscf)
        endif
      enddo
      call find_median(yy,nscf,backscf)
C Centroid and find FWHM near ipeak.
      area=0.
      xpeak=0.
      wt=0.
      do i=max(1,min(nscf,ipeak-10)),max(1,min(nscf,ipeak+10))
        rr = max(0., scfy(i) - backscf )
        area = area+ rr
        xpeak= xpeak+ (rr*scfx(i))
        wt   = wt  + rr
      enddo
      xpeak  = xpeak / max(1.e-20,wt)
      fwhm = ( area / peak ) *0.939437

C Just in case.
      if (abs(peak).lt.1.e-30) peak=1.e-30

      if (makeplot) then
        call PGPAGE
        call PGWINDOW(scfx(1),scfx(nscf),0.,peak)
        call PGBOX('BCNST',0.,0,'BCNST',0.,0)
        call PGLINE(nscf,scfx,scfy)
        call PGSLS(4)
        call PG_VLINE(0.,peak,xpeak)
        call PGSLS(1)
      endif

      ok = .true.
      return
      end


C----------------------------------------------------------------------
C Determine saturation level.  Should be about 64K.
C
      subroutine autoid_FindSaturation(a,sc,ec,sr,er,satur,ok)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er),satur,peak,x
      integer*4 i,j,nsat
      logical ok
C
      include 'soun.inc'
C
C Find peak.
      peak=0.
      do i=sc,ec
        do j=sr,er
          if (a(i,j).gt.peak) peak = a(i,j)
        enddo
      enddo
C Number of points within 3% of peak.
      x=peak*0.03
      nsat=0
      do i=sc,ec
        do j=sr,er
          if (abs(a(i,j)-peak).lt.x) nsat=nsat+1
        enddo
      enddo
C If not enough points close to saturation, assume that the peak is not
C the saturation level, use default.
      write(soun,'(a,f10.2,i5)') 
     . 'Saturation level, # of saturated pixels=',peak,nsat
      if (peak.lt.1.) then
        write(soun,'(a)') 
     . 'autoid_FindSaturation: ERROR: could not find saturation level.'
        ok=.false.
        return
      elseif (nsat.lt.7) then
        write(soun,'(a)') 'Assume saturation is 64600.'
        peak = 64600.
      elseif ((peak.lt.60000.).or.(peak.gt.70000)) then
        write(soun,'(a,f10.2)') 
     . 'WARNING: autoid_FindSaturation: unexpected saturation at:',peak
      endif
      satur = peak
      ok=.true.
      return
      end
       
C----------------------------------------------------------------------
C Multiple two 1-D arrays with pixel shift:  BCOL = ACOL + COFF
C
      real*4 function mwsh1(a,asc,aec,b,bsc,bec,coff,aback,bback)
C
      implicit none
      integer*4 asc,aec,bsc,bec,coff
      real*4 a(asc:aec),b(bsc:bec),aback,bback
      real*4 sum
      integer*4 col,olsc,olec
C Initialize.
      mwsh1 = 0.
C Starting and ending column in terms of a's columns.
      olsc = max(asc,bsc-coff)
      olec = min(aec,bec-coff)
      if ((olsc.gt.aec).or.(olec.lt.asc)) return
C Initialize.
      sum=0.
C Multiple with offset.
      do col=olsc,olec
        if ((a(col).gt.aback).and.(b(col+coff).gt.bback)) then
          sum = sum + (a(col)*b(col+coff))
        endif
      enddo
      mwsh1=sum
      return
      end


C----------------------------------------------------------------------
C Attempt to blank-out saturated, bleeding columns in HIRES arclamp. Input and
C output image is in a(), arr() is just a scratch image of the same size.
C "desat" is the limit defining saturated lines (usually about 64000.)
C
      subroutine arcdesat(a,arr,sc,ec,sr,er,desat,nbad)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er)
      real*4 arr(*)
      real*4 desat
      integer*4 nbad
C
      include 'soun.inc'
C
      integer*4 col,row,col1,col2,narr
      integer*4 ssr,ser,j,i,hij,satrows
      real*4 newdesat,back,sum,hisum
C
C Check.
      if (1+er-sr.lt.5) then
        write(soun,'(a,i5)') 
     .  'arcdesat: WARNING: too few orders to ID saturated col:',
     .  1+er-sr
        nbad=-1
        return
      endif

C Number of affected pixels.
      nbad=0
C First find the saturated lines.
      col=sc
      do while(col.lt.ec)
C Find contiguous sets of saturated rows.
        row=sr
        do while(row.lt.er)
          if (a(col,row).gt.desat) then
C Found a saturated sequence...
            ssr = row
            row=row+1
            do while((a(col,row).gt.desat).and.(row.le.er))
              row=row+1
            enddo
            ser = row-1
            satrows = 1+ser-ssr
C Is it long enough?  Need at least 3 rows (only 2 for the top or bottom).
            if ((ssr.eq.sr).or.(ser.eq.er)) satrows=satrows+1
            if (satrows.gt.2) then
C Hunt for actual saturated line.  Note that the center of the saturated
C line will be slightly to the left of where we are.
              hisum=-1.e+30
              hij  = ssr
              do j=ssr,ser
                sum=0.
                do i=max(sc,col-3),min(ec,col+5)
                  sum=sum+a(i,j)
                enddo
                if (sum.gt.hisum) then
                  hisum=sum
                  hij  =j
                endif
              enddo
C Find range of columns which are saturated within this arclamp line.
              col1=col
              do while((a(col1-1,hij).gt.desat).and.(col1-1.gt.sc))
                col1=col1-1
              enddo
              col2=col
              do while((a(col2+1,hij).gt.desat).and.(col2+1.lt.ec))
                col2=col2+1
              enddo
C "Typical" background level.
              narr=0
              do j=ssr,ser
                do i=sc,ec
                  narr=narr+1
                  arr(narr)=a(i,j)
                enddo
              enddo
              call find_median(arr,narr,back)
              back = min(back,desat-1.)            ! just in case.
C
C Purge the non-line saturated columns.  Replace with median for row.
C Go up and down from the actual line (the whole height of the image if
C necessary.)  Blank all contiguous saturated pixels.  Reduce requirement
C of saturation by 80%.
C
              newdesat = desat - ((desat-back)*0.8)
              do i=col1,col2
C Look up.
                j=hij+1
                do while((j.le.er).and.(a(i,j).gt.newdesat))
                  a(i,j)=back
                  j=j+1
                  nbad=nbad+1
                enddo
C Look down.
                j=hij-1
                do while((j.ge.sr).and.(a(i,j).gt.newdesat))
                  a(i,j)=back
                  j=j-1
                  nbad=nbad+1
                enddo
              enddo
            endif                ! enough rows saturated?
          else
            row=row+1
          endif                  ! col,row saturated?
        enddo                    ! row loop.
        col=col+1                ! next column to test.
      enddo                      ! column loop.
      return
      end


C----------------------------------------------------------------------
C Give a maximum of 2 points for each quarter of spectrum.
C So if grade > 6 there must be lines in each quarter.
C So if grade > 4 there must be lines in at least three of the quarters.
C So if grade > 2 there must be lines in at least two of the quarters.
C
      integer*4 function grade_by_own_row(sc,ec,j)
C
      implicit none
      integer*4 sc,ec,j
      include 'spim1.inc'
      integer*4 i,ii,k1(2),k2(2),k3(2),k4(2),i1,i2,i3,p
C
C Divide pixel range into fourths.
      i1=sc+((ec-sc)/4)
      i2=sc+(2*(ec-sc)/4)
      i3=sc+(3*(ec-sc)/4)
C Initialize
      do ii=1,2
        k1(ii)=0
        k2(ii)=0
        k3(ii)=0
        k4(ii)=0
      enddo
      do ii=1,numim
C Number of lines in first quarter.
        do i=1,nl(j,ii)
          p=nint(lc(i,j,ii))
          if (p.lt.i1) then
            k1(ii)=k1(ii)+1
          elseif (p.gt.i3) then
            k4(ii)=k4(ii)+1
          elseif (p.lt.i2) then
            k2(ii)=k2(ii)+1
          else
            k3(ii)=k3(ii)+1
          endif
        enddo
      enddo
      grade_by_own_row =
     .    min(2,max(k1(1),k1(2)))
     .  + min(2,max(k2(1),k2(2)))
     .  + min(2,max(k3(1),k3(2)))
     .  + min(2,max(k4(1),k4(2)))
      return
      end

C----------------------------------------------------------------------
C
      subroutine grade_by_other_rows(sr,er,j,ided,grade)
C
      implicit none
      include 'spim1.inc'
      integer*4 sr,er,j,grade,jj,k
      logical ided(maxr)
C Initialize.
      grade=0
C Give points for nearby IDed rows.
      do jj=sr,er
        if (ided(jj)) then
          grade = grade + max(0,6-abs(jj-j))
        endif
      enddo
      grade=grade*10
C Number of adjacent rows above.
      k=0
      jj=j+1
      do while((jj.le.er).and.(ided(jj)))
        jj=jj+1
      enddo
      k = k + ((jj-j)-1)
C Number of adjacent rows below.
      jj=j-1
      do while((jj.ge.sr).and.(ided(jj)))
        jj=jj-1
      enddo
      k = k + ((j-jj)-1)
C Bonus if more than 2 adjacent rows.
      if (k.gt.2) grade = grade + 1000 + k
C Try-breaker.
      grade = grade + max(nl(j,1),nl(j,2))
      return
      end
