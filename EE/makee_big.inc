C This file is makee_big.inc

C :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C :::::  CHANGE LINES IN "maxpix.inc" TO REDUCE MEMORY USAGE ::::::
C :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C
      include 'maxpix.inc'
C
      real*4 flat(maxpix),obj(maxpix)
C
      common /MAKEEBIGBLK/ flat,obj
C
C :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
