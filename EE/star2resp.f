C star2resp.f
C
      implicit none
      integer*4 sc,ec,sr,er
      integer*4 nc,nr,narg,inhead,i,row,n,ord,ii,bord
      character*115200 starhead
      character*80 arg(9),starfile,okefile,respfile,wv0,wv4
      character*80 chead,wrd
      character c7*7
      integer*4 maxoke,maxpt
      parameter(maxoke = 19000, maxpt = 201000)
      real*4 a(maxpt),value,okex(maxoke),okey(maxoke)
      real*8 coef(0:7),expos,disp,ech,xd
      real*8 polyval,fhead,wave,bcoef(0:7)
      real*8 x8(9000),y8(9000),w8(9000)
      integer*4 noke,getpos,neari_bs,lc,k
      logical ok,E17,findarg,silent

C Get command line parameters.
      call arguments(arg,narg,9)
      E17 = findarg(arg,narg,'-E17',':',wrd,i)
      silent = findarg(arg,narg,'-silent',':',wrd,i)
      if ((narg.ne.3).and.(narg.ne.2)) then
        print *,'Syntax: star2resp (Standard Star FITS input file)'
        print *,
     .  '                  (Response function FITS output file)'
        print *,
     .  '               [Oke fluxed FITS spectrum]  [-silent]  [-E17]'
        print *,
     .  '   Converts star from DN/pixel to DN/sec/Angstrom and divides'
        print *,
     .  '   this INTO the Oke spectrum:  resp = f(oke) / star .'
        print *,
     .  '   The star should have already been extinction corrected.'
        print *,
     .  ' Option: -E17 : Multiply response function by 1.e+17 .'
        print *,'         -silent : less verbose.'
        call exit(0)
      endif
      starfile= arg(1)
      call AddFitsExt(starfile)
      respfile= arg(2)
      call AddFitsExt(respfile)
      if (narg.eq.3) then
        okefile = arg(3)
        call AddFitsExt(okefile)
      else
        okefile = ' '
      endif

C Read standard star FITS file...
      call readfits(starfile,a,maxpt,starhead,ok)
      if (.not.ok) then
        print *,'Error reading fits file: ',starfile
        call exit(1)
      endif
      nc = inhead('NAXIS1',starhead)
      nr = inhead('NAXIS2',starhead)
      sc = inhead('CRVAL1',starhead)
      sr = inhead('CRVAL2',starhead)
      ec = sc + nc - 1
      er = sr + nr - 1
      print '(2a)','Star file= ',starfile(1:lc(starfile))
      wrd = chead('OBJECT',starhead)
C Look for oke file.
      if (okefile.eq.' ') then
        call WhatOSP(wrd,okefile)
        if (okefile.eq.' ') then
          print '(2a)','Could not find Oke spectrum file: ',
     .                  okefile(1:lc(okefile))
          print '(a)','   ...Enter on command line.'
          call exit(1)
        endif
      endif
      print '(3a)',wrd(1:lc(wrd)),'  using  ',okefile(1:lc(okefile))
      expos = dfloat(inhead('EXPOSURE',starhead))
      ech= fhead('ECHANGL',starhead)
      xd = fhead('XDANGL',starhead)
      print '(a,3f9.4)','ECHA,XDA,EXPOSURE= ',ech,xd,expos

C Read Oke spectrum file...
      call readfitsxy(okefile,okex,okey,maxoke,noke)

C Check header for flags.
      if (getpos('ATMEXTNC',starhead).eq.-1) then
        print *,'ERROR: No atmospheric extinction correction done.'
        call exit(1)
      endif

      if (.not.silent) print *,' Doing row : '

C Do each row.
      DO row=sr,er

      if (.not.silent) print *,row

C We use 7th order in HIRES reduced arc lamps, although they are really only
C 6th order polynomials for compatibility with Vista.
      ord = 7

C Extract coeffecients from starhead.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,starhead)
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) coef(0),coef(1),coef(2),coef(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,starhead)
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) coef(4),coef(5),coef(6),coef(7)

C Do each point.
      do i=sc,ec
C Convert to DN/s/Angstrom.
        disp= ( polyval(ord+1,coef,dfloat(i+1))
     .        - polyval(ord+1,coef,dfloat(i-1)) ) / 2.d0
        value = sngl( disp * expos ) 
        call DivideValue(a,sc,ec,sr,er,i,row,value)
C Find appropriate value in Oke spectrum.
        wave= polyval(ord+1,coef,dfloat(i))
        ii = neari_bs(sngl(wave),noke,okex)
C Fit a curve through a few Oke points.
        n=0
        do k=max(1,ii-3),min(noke,ii+3)
          n=n+1
          x8(n) = dble(okex(k))
          y8(n) = dble(okey(k))
          w8(n) = 1.d0
        enddo
        bord=2
        call poly_fit_glls(n,x8,y8,w8,bord,bcoef,ok)
        if (.not.ok) then
          print *,'Error fitting polynomial to Oke spectrum.'
          goto 999
        endif

C Get value and divide INTO.
        value = sngl(polyval(bord+1,bcoef,wave))
        call DivideIntoValue(a,sc,ec,sr,er,i,row,value)
      enddo

      ENDDO

      if (.not.silent) print *,'...done.'

C Multiply by 1.e+17.
      if (E17) call MultiplyE17(a,sc,ec,sr,er)

C Write out response FITS file.
      wrd = chead('OBJECT',starhead)
      i = index(wrd,'(Flux)')
      if (i.eq.0) then
        wrd = wrd(1:lc(wrd))//' (Error)'
      else
        wrd(i:i+9) ='(Response)'
      endif
      call cheadset('OBJECT',wrd,starhead)
      print '(2a)','Writing file: ',respfile(1:lc(respfile))
      call writefits(respfile,a,starhead,ok)
      if (.not.ok) then
        print *,'Error writing fits file: ',respfile
        call exit(1)
      endif
      
      print *,'All done.'
      goto 999
800   continue
      print *,'Error reading WV header card.'
      goto 999
805   continue
      print *,'Error writing WV header card.'
999   stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
      subroutine DivideValue(b,sc,ec,sr,er,col,row,value)
      implicit none
      integer*4 sc,ec,sr,er,col,row
      real*4 b(sc:ec,sr:er),value
      if (b(col,row).gt.1.e-10) then
        b(col,row) = b(col,row) / value
      else
        b(col,row) = 0.
      endif
      return
      end

C----------------------------------------------------------------------
      subroutine DivideIntoValue(b,sc,ec,sr,er,col,row,value)
      implicit none
      integer*4 sc,ec,sr,er,col,row
      real*4 b(sc:ec,sr:er),value
      if (b(col,row).gt.1.e-10) then
        b(col,row) = value / b(col,row)
      else
        b(col,row) = 0.
      endif
      return
      end

C----------------------------------------------------------------------
      subroutine MultiplyE17(a,sc,ec,sr,er)
      implicit none
      integer*4 sc,ec,sr,er,i,j
      real*4 a(sc:ec,sr:er)
      print *,'NOTE: Multiplying by 1.e+17 .'
      do i=sc,ec
        do j=sr,er
          a(i,j) = a(i,j) * 1.e+17
        enddo
      enddo
      return
      end

C----------------------------------------------------------------------
      subroutine WhatOSP(object,file)
      implicit none
      character*(*) object,file
      character*80 har,wrd
      integer*4 lc
      wrd = object
      call upper_case(wrd)
      call GetMakeeHome(har)

      if (index(wrd,'FEIGE 34').gt.0) then
        file = har(1:lc(har))//'ssdb/feige34_osp.fits'
      elseif (index(wrd,'FEIGE 67').gt.0) then
        file = har(1:lc(har))//'ssdb/feige67_osp.fits'
      elseif (index(wrd,'FEIGE 66').gt.0) then
        file = har(1:lc(har))//'ssdb/feige66_osp.fits'
      elseif (index(wrd,'FEIGE 110').gt.0) then
        file = har(1:lc(har))//'ssdb/feige110_osp.fits'

      elseif (index(wrd,'BD+28').gt.0) then
        file = har(1:lc(har))//'ssdb/bd_28d4211_osp.fits'
      elseif (index(wrd,'BD+33').gt.0) then
        file = har(1:lc(har))//'ssdb/bd_33d2642_osp.fits'

      elseif (index(wrd,'BD +28').gt.0) then
        file = har(1:lc(har))//'ssdb/bd_28d4211_osp.fits'
      elseif (index(wrd,'BD +33').gt.0) then
        file = har(1:lc(har))//'ssdb/bd_33d2642_osp.fits'

      elseif (index(wrd,'G191').gt.0) then
        file = har(1:lc(har))//'ssdb/g191b2b_osp.fits'
      elseif (index(wrd,'G 191').gt.0) then
        file = har(1:lc(har))//'ssdb/g191b2b_osp.fits'

      elseif (index(wrd,'HZ 44').gt.0) then
        file = har(1:lc(har))//'ssdb/hz44_osp.fits'
      elseif (index(wrd,'HZ44').gt.0) then
        file = har(1:lc(har))//'ssdb/hz44_osp.fits'
      else
        file = ' '
      endif
      return
      end
