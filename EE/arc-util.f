C arc-util.f                                    tab  1995-1998

CENDOFMAIN


C----------------------------------------------------------------------
C Create correlation spectrum.
C
      subroutine spcorr(a,asc,aec,asr,aer,b,bsc,bec,bsr,ber,
     .  centroid,plot)
C
      implicit none
      integer*4 asc,aec,asr,aer,bsc,bec,bsr,ber
      real*4 a(asc:aec,asr:aer)
      real*4 b(bsc:bec,bsr:ber)
      real*4 centroid
      integer*4 m
      parameter(m=9000)
      real*4 cosp(m),spsh(m)
      real*4 sum,wsum,low,r4,multwsh
      integer*4 n,i
      real*4 xx(m),yy(m)
      logical plot

C Correlation array.
      n  = 0
      do i=-20,+20
        n=n+1
        spsh(n) = dfloat(i)
        cosp(n) = multwsh(a,asc,aec,asr,aer,b,bsc,bec,bsr,ber,i,0)
      enddo
C Normalize.
      sum=0.
      do i=1,n
        sum=sum+cosp(n)
      enddo
      sum=sum/dfloat(n)
      do i=1,n
        cosp(i)=cosp(i)/sum
      enddo
C Low point.
      low=cosp(1)
      do i=2,n
        if (cosp(i).lt.low) low=cosp(i)
      enddo
C Centroid.
      sum =0.
      wsum=0.
      do i=1,n
        r4 = max(0.,cosp(i)-low)
        sum = sum + (spsh(i)*r4*r4)
        wsum= wsum+ (r4*r4)
      enddo
      centroid = sum/wsum 
      sum =0.
      wsum=0.
      do i=1,n
        if (abs(spsh(i)-centroid).lt.10.) then
          r4 = max(0.,cosp(i)-low)
          sum = sum + (spsh(i)*r4*r4)
          wsum= wsum+ (r4*r4)
        endif
      enddo
      centroid = sum/wsum 

      if (plot) then
        do i=1,n
          xx(i)=spsh(i)
          yy(i)=cosp(i)
        enddo
        call PG_SimpPlot(n,xx,yy,0,'pgdisp')
        call PG_SimpPlot(n,xx,yy,0,'go')
        call PG_SimpPlot(n,xx,yy,0,'stop')
      endif

      return
      end


C----------------------------------------------------------------------
C Multiple two images with pixel shift.
C    BCOL = ACOL + COFF  ;  BROW = AROW + ROFF
C
      real*4 function multwsh(a,asc,aec,asr,aer,b,bsc,bec,bsr,ber,
     .  coff,roff)
C
      implicit none
      integer*4 asc,aec,asr,aer
      integer*4 bsc,bec,bsr,ber
      integer*4 coff,roff
      real*4 a(asc:aec,asr:aer),b(bsc:bec,bsr:ber)
      real*4 sum
      integer*4 col,row,olsc,olec,olsr,oler
C
      include 'soun.inc'
C
C Check.
      multwsh = 0.
      if (roff.ne.0) then
        write(soun,'(a)') 'THIS VERSION REQUIRES roff=0.'
        call exit(1)
      endif
C Initialize.
      sum=0.
C Starting and ending column in terms of a's columns.
      olsc = max(asc,bsc-coff)
      olec = min(aec,bec-coff)
      if ((olsc.gt.aec).or.(olec.lt.asc)) return
C Starting and ending row in terms of a's rows.
      olsr = max(asr,bsr-roff)
      oler = min(aer,ber-roff)
      if ((olsr.gt.aer).or.(oler.lt.asr)) return
C Multiple with offset.
      do col=olsc,olec
        do row=olsr,oler
          sum = sum + (a(col,row)*b(col+coff,row))
        enddo
      enddo
      multwsh=sum
      return
      end


C----------------------------------------------------------------------
C Fit a polynomial to the extinction values between wv1 and wv2.
C
      subroutine GetExtinctionPoly(wv1,wv2,next,extx,exty,
     .                                          extord,extcoef)
C
      implicit none
      real*8 wv1,wv2
      integer*4 next,extord
      real*8 extx(next),exty(next)
      real*8 extcoef(*)
C
      include 'soun.inc'
C
      integer*4 i,n
      real*8 high,wrms,x8(900),y8(900),w8(900)
      logical ok
C
C Fill array between wv1 and wv2
      n=0
      do i=1,next
        if ((extx(i).gt.wv1).and.(extx(i).lt.wv2)) then
          n=n+1
          x8(n)=extx(i)
          y8(n)=exty(i)
          w8(n)=1.d0
        endif
      enddo
C Check
      if (n.lt.4) then
        write(soun,'(a,i5)') 
     .  'ERROR with extinction polynomial, number of points=',n
        call exit(1)
      endif
C Fit polynomial.
      extord=2
      call poly_fit_glls(n,x8,y8,w8,extord,extcoef,ok)
      if (.not.ok) then
        write(soun,'(a)') 
     .  'Error fitting polynomial extinction polynomial.'
        call exit(1)
      endif
C Check residuals.
      call poly_fit_glls_residuals(n,x8,y8,w8,extord,extcoef,high,wrms)
      if ((high.gt.0.001).or.(wrms.gt.0.0001)) then
        write(soun,'(a)') 
     .  'WARNING: High errors in extinction polynomial refit.'
        write(soun,'(a,2f15.7)') 
     .  'WARNING: EXT POLY High, WRMS = ',high,wrms
      endif
C
      return
      end


C----------------------------------------------------------------------
      subroutine DivideValue(b,sc,ec,sr,er,col,row,value)
      implicit none
      integer*4 sc,ec,sr,er,col,row
      real*4 b(sc:ec,sr:er),value
      b(col,row) = b(col,row) / value
      return
      end

C----------------------------------------------------------------------
      subroutine DivideErrValue(v,sc,ec,sr,er,col,row,value)
      implicit none
      integer*4 sc,ec,sr,er,col,row
      real*4 v(sc:ec,sr:er),value
      if (v(col,row).gt.0.) v(col,row) = v(col,row) / value
      return
      end

C----------------------------------------------------------------------
      subroutine SqrtVar(v,sc,ec,sr,er,col,row)
      implicit none
      integer*4 sc,ec,sr,er,col,row
      real*4 v(sc:ec,sr:er)
      if (v(col,row).gt.0.) v(col,row) = sqrt(v(col,row))
      return
      end

C----------------------------------------------------------------------
C Apply a pixel shift (ps) to the wavelength calibration in "header".
C
      subroutine PixelShift(header,ps)
C
      implicit none
      character*(*) header    ! FITS header (input/output)
      real*8 ps               ! Pixel Shift to apply (input)
C
      include 'soun.inc'
C
      integer*4 sc,ec,sr,er,nc,nr,row,i,n,ord
      character c7*7
      character*80 chead,wv0,wv4
      real*8 coef(0:20),bcoef(0:20),high,wrms,polyval
      real*8 x8(900),y8(900),w8(900)
      logical ok
C
C Dimensions.
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
C Do each row.
      DO row=sr,er
C Extract coeffecients.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,header)
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) coef(0),coef(1),coef(2),coef(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,header)
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) coef(4),coef(5),coef(6),coef(7)
C Fill array and apply pixel shift.
      n=0
      do i=sc,ec,10
        n=n+1
        x8(n)=dfloat(i) + ps
        y8(n)=polyval(7,coef,dfloat(i))
        w8(n)=1.d0
      enddo
C Refit polynomial.
      ord=6
      call poly_fit_glls(n,x8,y8,w8,ord,bcoef,ok)
      if (.not.ok) then
        write(soun,'(a)') 'Error refitting polynomial.'
        goto 999
      endif
C Check residuals.
      call poly_fit_glls_residuals(n,x8,y8,w8,ord,bcoef,high,wrms)
      if ((wrms.gt.0.00001).or.(high.gt.0.0001)) then
        write(soun,'(a,i5,2f15.7)') 
     .  ' Row, High, WRMS (Ang) = ',row,high,wrms
      endif
C Put coeffecients back into header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      write(wv0,'(4(1pe17.9))',err=805) 
     .  bcoef(0),bcoef(1),bcoef(2),bcoef(3)
      call cheadset(c7,wv0,header)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      write(wv4,'(4(1pe17.9))',err=805) 
     .  bcoef(4),bcoef(5),bcoef(6),bcoef(7)
      call cheadset(c7,wv4,header)
      ENDDO
      goto 999
800   continue
      write(soun,'(a)') 'Error reading WV header card.'
      goto 999
805   continue
      write(soun,'(a)') 'Error writing WV header card.'
999   return
      end


C----------------------------------------------------------------------
C Return best computed value for pixel shift derived from sky lines.
C Use device= "/XDISP" or "junk.ps/PS" or "none" (for no plot).
C
      real*8 function SkyShift(a,sc,ec,sr,er,header,objhead,skyfile,
     .                         minpk,minline,verbose,device,
     .                         rms,npt,nrej)
C
      implicit none
      integer*4 sc,ec,sr,er            ! Image dimensions (input)
      real*4 a(sc:ec,sr:er)            ! Data image (input)
      character*(*) header,objhead     ! Arc and Object FITS headers (input)
      character*(*) skyfile            ! Sky data filename (input)
      real*4 minpk                     ! Smallest peak to accept (input)
      integer*4 minline                ! Minimum # of lines needed (input)
      logical verbose                  ! Verbosity? (input)
      character*(*) device             ! Device name for PGPLOT plot (input)
      real*8 rms                       ! RMS of shift fit (output)
      integer*4 npt                    ! Number of lines used (output)
      integer*4 nrej                   ! Number of lines rejected (output)
C
      include 'soun.inc'
C
      integer*4 m
      parameter(m=9000)
      real*8 wv(m),shift(m),pk(m),wt(m)
      integer*4 wvn(m),rownum(m)
      real*4 xx(m),yy(m),ww(m)
      real*8 GetSpimWave,GetSpimPix
      real*8 wsum,mean,hi
      real*8 bwv(90),rwv(90),r1,r2,r3,r4,r5,r6,r7,r8,pix,fwhm,cent,peak
      integer*4 getpos,row,ns,i,n,i1,ihi
      logical ok

C Initialize wavelength = func(pixel) function.
      SkyShift=0.d0
      if (GetSpimWave(header,0.d0,0).lt.0.) then
        write(soun,'(a)') 'Error getting wavelength scale.'
        return
      endif
C Initialize pixel = func(wavelength) function.
      if (GetSpimPix(header,0.d0,0).lt.0.) then
        write(soun,'(a)') 'Error creating inverse wavelength scale.'
        return
      endif
C Must be air, non-heliocentric.
      if ( (getpos('HELIOCNT',header).ne.-1).or.
     .     (getpos('VACUUM',header).ne.-1) ) then
        write(soun,'(a)') 
     .  'Error- heliocentric or vacuum correction has been applied.'
        return
      endif
C Load wavelength ranges.
      do row=sr,er
        bwv(row) = GetSpimWave(header,dfloat(sc),row)
        rwv(row) = GetSpimWave(header,dfloat(ec),row)
      enddo
C Read in sky lines.
      open(1,file=skyfile,status='old')
      n=0
1     read(1,*,end=5) i1,r1,r2,r3,r4,r5,r6,r7,r8
      if (r4.gt.minpk) then
        n=n+1
        wv(n) = r1
        pk(n) = r4
      endif
      goto 1 
5     close(1)

C Look at each row for possible sky lines.
      ns=0
      do row=sr,er
        do i=1,n
          if ((wv(i).gt.bwv(row)).and.(wv(i).lt.rwv(row))) then
            pix = GetSpimPix(header,wv(i),row)
            if ((nint(pix).gt.sc+7).and.(nint(pix).lt.ec-7)) then
              call arc_centroid(a,sc,ec,sr,er,row,pix,cent,
     .                                            peak,fwhm,ok)
              if (ok) then
                ns=ns+1
                wvn(ns)  = i
                shift(ns)= cent - pix
                rownum(ns)= row
              endif
            endif
          endif
        enddo
      enddo
      if (ns.lt.minline) then
        write(soun,'(a,2(a,i4))') 'NOTE: No skyline shift applied.',
     .          ' # of lines found=',ns,'   Minimum required=',minline
        goto 810
      endif
C Set up weights. Initially use peak to find weights.
      do i=1,ns
        wt(i) = min(1.d0,max(0.d0,pk(wvn(i))/100.d0))
      enddo
C Calculate weighted mean.
2     continue
      mean=0.d0
      wsum=0.d0
      do i=1,ns
        mean = mean + shift(i)*wt(i)
        wsum = wsum+ wt(i)
      enddo
      if (wsum.lt.1.d-60) goto 800
      mean = mean/wsum
C RMS.
      rms=0.d0
      wsum=0.d0
      do i=1,ns
        if (wt(i).gt.0.) then
          rms = rms + (shift(i)-mean)*(shift(i)-mean)
          wsum= wsum+ 1.d0
        endif
      enddo
      if (wsum.lt.1.d-60) goto 800
C Minimum RMS must be 0.01 pixels.
      rms = max(0.01d0,sqrt(rms/wsum))
C Check number of lines left.
      npt=0
      do i=1,ns
        if (wt(i).gt.0.) npt=npt+1
      enddo
      nrej = ns - npt
C If we have enough lines left, see if we should throw out a line.
      if (npt.gt.minline) then
C Delete lines beyond 3 * rms.
        hi =0.d0
        ihi=0
        do i=1,ns
          if (wt(i).gt.0.) then
            r8 = abs( shift(i) - mean )
            if (r8.gt.hi) then
              hi = r8
              ihi= i
            endif
          endif
        enddo
        if ((hi/rms).gt.3.) then
          wt(ihi)=0.d0
          goto 2
        endif
      endif
C Report.
      if (verbose) then
        write(soun,'(a)') '*****Night sky line shift results:'
        write(soun,'(a)') '#   row  wavelength  peak   pix.shft weight'
        do i=1,ns
          write(soun,'(2i5,f11.3,f8.1,f8.2,f7.2)')
     .      i,rownum(i),wv(wvn(i)),pk(wvn(i)),shift(i),wt(i)
        enddo
      endif
      write(soun,'(a,f9.4,a,f8.4,a,i3,a,i3)')
     .               'Skyline shift=',mean,'  +/-',rms,
     .               '  Npt= ',npt,'  NRej=',nrej
      SkyShift = mean
C Plot.
      if (device(1:4).ne.'none') then
        do i=1,ns
          xx(i) = sngl(wv(wvn(i))) 
          yy(i) = sngl(shift(i)) 
          ww(i) = sngl(wt(i)) 
        enddo 
        call shiftplot(ns,xx,yy,ww,mean,rms,bwv(sr),rwv(er),
     .                        objhead,device) 
      endif

C###
C     open(1,file='skytest.xy',status='unknown')
C     do i=1,ns
C       write(1,'(2f12.6)') cc(i),yy(i)
C     enddo
C     close(1)
C###

      return
C Error.
800   continue
      write(soun,'(a)') 'Error in SkyShift, use zero shift.'
810   continue
      SkyShift = 0.d0
      rms = -1.d0
      return
      end


C----------------------------------------------------------------------
C See arc_centroid().
C
      subroutine f2carccentroid( a, sc, ec, sr, er, row,
     .                           pix, cent, peak, fwhm, iok )
C
      implicit none
      real*4 a(*)            ! data image (input)
      integer*4 sc,ec,sr,er  ! dimensions of data image (input)
      integer*4 row          ! row within image to look at (input)
      real*8 pix             ! guess at centroid (input)
      real*8 cent,peak,fwhm  ! centroid, peak, and FWHM (output)
      integer*4 iok          ! Success? (1/0) (output)
      logical ok
C
      call arc_centroid(a,sc,ec,sr,er,row,pix,cent,peak,fwhm,ok)
      if (ok) then
        iok = 1
      else
        iok = 0
      endif
      return
      end


C----------------------------------------------------------------------
C Input: a,sc,ec,sr,er,row,pix
C Output: cent,peak,fwhm
C
      subroutine arc_centroid(a,sc,ec,sr,er,row,pix,cent,peak,fwhm,ok)

      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er)
      real*8 pix,cent,peak,fwhm,last_cent,sum,wsum,r8
      real*4 low,arr(900),median
      integer*4 i,n,iter
      logical ok
C
      include 'soun.inc'
C
C Find background.
      n=0
      do i=max(sc,nint(pix)-40),min(ec,nint(pix)+40)
        n=n+1
        arr(n) = a(i,row)
      enddo
      call find_median(arr,n,median)
      low = arr(n/4)
C Find centroid.
      iter=0
      cent=pix
      last_cent=cent+1.d0
      do while((iter.lt.10).and.(abs(cent-last_cent).gt.0.01))
        iter=iter+1
        last_cent=cent
        sum = 0.d0
        wsum= 0.d0
        do i=max(sc,nint(cent)-5),min(ec,nint(cent)+5)
          r8  = dble(max(0.,a(i,row)-low))
          sum = sum + dfloat(i)*r8
          wsum= wsum+ r8
        enddo
        if (wsum.lt.1.d-30) goto 905
        cent = sum / wsum
      enddo 
C Find peak.
      peak=a(nint(cent),row)
      do i=max(sc,nint(cent)-5),min(ec,nint(cent)+5)
        if (a(i,row).gt.peak) peak=a(i,row)
      enddo 
C Find area.
      sum=0.d0
      do i=max(sc,nint(cent)-5),min(ec,nint(cent)+5)
        sum=sum+dble(max(0.,a(i,row)-low))
      enddo 
C Find FWHM.
      if ((sum.lt.1.d-30).or.(peak.lt.1.d-30)) then
        write(soun,'(a,2(1pe15.7))') 
     .  'WARNING: arc_centroid: problem finding FWHM, sum,peak=',
     .     sum,peak
        goto 905
      else
        fwhm=0.939437d0*sum/peak
      endif
      ok=.true.
      return
905   continue
      ok=.false.
      return
      end

C---------------------------------------------------------------------- 
C Plot pixel shifts from SkyShift function.
C
      subroutine shiftplot(n,x,y,w,mean,rms,blue,red,header,device)

      implicit none 
      integer*4 n 
      real*4 x(*),y(*),w(*)
      real*8 mean,rms,blue,red
      character*(*) header
      character*(*) device
      real*4 xmin,xmax,ymin,ymax,r,xx(9000),yy(9000)
      real*8 fhead,ech,xd
      integer*4 i,nn,expos,inhead,obsnum,lc
      character*80 chead,object,slmsknam
      character*200 wrd
      logical EE_QueryHIRES
C
      include 'soun.inc'
C
C Plot.
      write(soun,'(2a)') 'Writing to device: ',device(1:lc(device))
      call PGBEGIN(0,device,1,1)
      call PGASK(.false.)
      call PGPAGE
      ymin=y(1)
      ymax=y(1)
      do i=2,n
        if (y(i).lt.ymin) ymin=y(i)
        if (y(i).gt.ymax) ymax=y(i)
      enddo
      xmin = sngl(blue)
      xmax = sngl(red)
      r = max(ymax-ymin,1.e-10)
      ymin = ymin - 0.02*r
      ymax = ymax + 0.02*r
      r = max(xmax-xmin,1.e-10)
      xmin = xmin - 0.02*r
      xmax = xmax + 0.02*r
C Limit y range to 3 * RMS.
      ymin = max(ymin,sngl(mean-(3.*rms)))
      ymax = min(ymax,sngl(mean+(3.*rms)))
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBBUF
      call PGBOX('BCNST',0.,0,'BCNST',0.,0)
      call PGPT(n,x,y,3)
      nn=0
      do i=1,n
        if (w(i).lt.0.0001) then
          nn=nn+1
          xx(nn)=x(i)
          yy(nn)=y(i)
        endif
      enddo
      call PGSCI(2)
      call PGPT(nn,xx,yy,3)
      call PG_HLine(xmin,xmax,sngl(mean))
      call PGSLS(4)
      call PG_HLine(xmin,xmax,sngl(mean+rms))
      call PG_HLine(xmin,xmax,sngl(mean-rms))
      call PGSLS(1)
      call PGSCI(1)
C Write more info.
      object=  chead('OBJECT',header)
      expos = inhead('EXPOSURE',header)
      obsnum= inhead('OBSNUM',header)
      if ( EE_QueryHIRES(header) ) then
        ech   =  fhead('ECHANGL',header)
        xd    =  fhead('XDANGL',header)
        write(wrd,'(a,i6,a,i6,2f9.4)') object(1:min(30,lc(object))),
     .       expos,'s',obsnum,ech,xd
      else
        slmsknam = chead('SLMSKNAM',header)
        write(wrd,'(a,i6,a,i6,2x,a)') object(1:min(30,lc(object))),
     .       expos,'s',obsnum,slmsknam
      endif

      call PGMTEXT('B',2.5,0.5,0.5,wrd)
      call PGEBUF
      call PGEND
      return
      end


C----------------------------------------------------------------------
C
      subroutine arc2obj_main(arg,narg,ok)
C
      implicit none
      character*(*) arg(*)
      integer*4 narg
      logical ok
C
      include 'soun.inc'
C
      integer*4 sc,ec,sr,er,bsc,bec,bsr,ber
      integer*4 nc,nr,i,row,n,ord
      character*115200 objhead,archead,archead2,skyhead,varhead,sumhead
      character*80 chead,wv0,wv4,wrd
      character*80 objfile,arcfile,arcfile2,errfile
      character*80 varfile,skyfile,sumfile
      character c7*7
      integer*4 m
      parameter(m=200000)
      real*4 b(m),v(m),skydata(m),s(m)
      real*4 ech1,xd1,ech2,xd2,value,valread
      real*8 coef(0:7),high,wrms,polyval,bcoef(0:7),fhead,rms,skyshmea
      real*8 x8(9000),y8(9000),w8(9000)
      real*8 airmass,hvsf,r81,r82,wv1,wv2,wave,air2vac
      real*8 SkyShift,r8,expos
      real*8 extx(20000),exty(20000),extcoef(0:7)
      real*8 user_shift,valread8
      integer*4 next,extord,lc,getpos,npt,nrej,arcmode
      logical vacuum,helio,atmext,findarg,var
      logical hdr_vacuum,hdr_helio,hdr_atmext,sky
      logical sumfi,isky,card_exist,EE_QueryESI
      character*80 ExtinctionFile,SkyLineFile,device
      character*80 commline

C Initialize.
      hvsf=0.

C Command line string.
      commline = arg(1)(1:min(lc(arg(1)),80))
      do i=2,narg
        wrd = arg(i)
        call SubFitsExt(wrd)
        if (lc(commline)+lc(wrd).lt.80) then
          commline = commline(1:lc(commline))//' '//wrd(1:lc(wrd))
        endif
      enddo

C Look at command line parameters.
      var    = findarg(arg,narg,'-var',':',wrd,i)
      vacuum = (.not.findarg(arg,narg,'-novac',':',wrd,i))
      helio  = (.not.findarg(arg,narg,'-nohc',':',wrd,i))
      atmext = findarg(arg,narg,'-atm',':',wrd,i)
      sky    = findarg(arg,narg,'-sky',':',wrd,i)
      isky   = findarg(arg,narg,'-isky',':',wrd,i)
      sumfi  = findarg(arg,narg,'-sum',':',wrd,i)
      user_shift = -1.d+30
      if (findarg(arg,narg,'skyshift=',':',wrd,i)) 
     .  user_shift=valread8(wrd)
      device=' '
      if (findarg(arg,narg,'ps=',':',wrd,i)) device=wrd
      if (index(device,'.ps').gt.0) 
     .  device = device(1:lc(device))//'/PS'
      arcfile2=' '
      if (findarg(arg,narg,'bracket=',':',wrd,i)) then
        arcfile2=wrd
        call AddFitsExt(arcfile2)
      endif
      arcmode=0
      if (findarg(arg,narg,'arcmode=',':',wrd,i)) 
     .  arcmode = nint(valread(wrd))
C
      if ((narg.lt.2).or.(narg.gt.2)) then
        print *,
     .  'Syntax: arc2obj (Arclamp FITS file) (Object FITS file)'
        print *,
     .  '      [bracket=(2nd arclamp FITS file)]  [arcmode=]'
        print *,
     .  '      [-var] [-novac] [-nohc] [-atm] [-sky] [-isky] [-sum]'
        print *,'      [skyshift=n] [ps=]'
        print *,'  '
        print *,
     .  '   Copy wavelength scale from an arclamp to an object.'
        print *,'   Convert from air/observed to vacuum/heliocentric'
        print *,
     .  '   wavelengths and correct for atmospheric extinction.'
        print *,'  '
        print *,
     .  ' bracket=  : Average the wavelength scales in the first'
        print *,
     .  '              arclamp file with this second arclamp file.'
        print *,
     .  '        (for arclamp exposures which bracket object exposure.)'
        print *,
     .  ' arcmode=  : Arclamp wavelength scale averaging mode:'
        print *,
     .  '                0 : Average scales of 1st and 2nd arc',
     .  'lamps. (default)'
        print *,'                1 : Shift scale to 1st arclamp.'
        print *,'                2 : Shift scale to 2nd arclamp.'
        print *,
     .  '   -var    : Attempt to read Var-###.fits file, apply same'
        print *,
     .  '             corrections as to Flux-###.fits and convert to'
        print *,
     .  '             error array as Err-###.fits.'
        print *,
     .  '   -sky    : Shift to skylines. Use: Flux-### and Sky-###.'
        print *,
     .  '             (and put wavelength scale on Sky-### file.)'
        print *,
     .  '   -isky   : Interactive "-sky" (start pgdisp window first).'
        print *,
     .  '   -sum    : Apply Flux-### corrections to Sum-### file.'
        print *,
     .  '   -atm    : Apply atmospheric extinction correction.'
        print *,'   -novac  : No vacuum correction.'
        print *,'   -nohc   : No heliocentric correction.'
        print *,'skyshift=n : Apply sky shift of n pixels.'
        print *,
     .  '    ps=    : PostScript skyshift plot file ("none"=no plot.)'
        return
      endif
 
C Check.
      if ((arcmode.lt.0).or.(arcmode.gt.2)) then
        write(soun,'(a)') 'ERROR: arc2obj: arcmode must be 0, 1, or 2.'
        ok=.false.
        return
      endif
      if ((isky).and.(soun.ne.6)) then
        write(soun,'(a)')
     .  'ERROR: arc2obj: soun must be "6" in interactive mode.'
        call exit(1)
      endif
C Interactive sky shift.
      if (isky) sky=.true.

C Get Extinction filename.
      call GetMakeeHome(wrd)
      ExtinctionFile = wrd(1:lc(wrd))//'MaunaKea.Extinction'

C Set filenames.
      arcfile = arg(1)
      call AddFitsExt(arcfile)
      objfile = arg(2)
      call AddFitsExt(objfile)

C Do we need Sky or Variance file?
      if (var.or.sky) then
        if (objfile(1:5).ne.'Flux-') then
          write(soun,'(a)')
     .  'Need "Flux-*" object file with -var or -sky option.'
          return
        else
          varfile = 'Var'//objfile(5:)
          errfile = 'Err'//objfile(5:)
          skyfile = 'Sky'//objfile(5:)
          sumfile = 'Sum'//objfile(5:)
        endif
      endif

C Read first fits file... the Arclamp file.
      write(soun,'(2a)') 'Reading: ',arcfile(1:lc(arcfile))
      call readfits(arcfile,s,m,archead,ok)       ! s() is used as scratch.
      if (.not.ok) then
        write(soun,'(2a)') 'Error reading fits file: ',arcfile
        return
      endif
      call GetDimensions(archead,sc,ec,sr,er,nc,nr)
C Make sure archead has wavelength scale.
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',er
      if (getpos(c7,archead).eq.-1) then
        write(soun,'(a)') 
     .  'ar2obj:ERROR- Arclamp has no wavelength scale.'
        ok=.false.
        return
      endif
C Echo angles.
      write(soun,'(2a)') 'Arc file= ',arcfile(1:40)
      ech1= sngl(fhead('ECHANGL',archead))
      xd1 = sngl(fhead('XDANGL',archead))
      if (ech1.gt.-1000.) then
        write(soun,'(a,2f9.4)') 'ECHA,XDA= ',ech1,xd1
      endif

C Is this an ESI file?  Set sky line data base filename.
      if ( EE_QueryESI(archead) ) then
        SkyLineFile=wrd(1:lc(wrd))//'SkyLineDataBase/sky.results.ESI'
      else
        SkyLineFile=wrd(1:lc(wrd))//'SkyLineDataBase/sky.results'
      endif

C Echo.
      write(soun,'(2a)') 'NOTE: Skylines from: ',
     .                    SkyLineFile(1:lc(SkyLineFile))

C Read in second Arc file (if exists).
      if (arcfile2.ne.' ') then
        write(soun,'(2a)') 'Reading: ',arcfile2(1:lc(arcfile2))
        call readfits(arcfile2,s,m,archead2,ok)      ! s() is used as scratch.
C Average the two wavelength scales and put back into "archead".
        call a2o_AverageWaveScales(archead,archead2,arcmode,ok)
        if (.not.ok) return
      endif
C Read second fits file... the object file.
      write(soun,'(2a)') 'Reading: ',objfile(1:lc(objfile))
      call readfits(objfile,b,m,objhead,ok)
      if (.not.ok) then
        write(soun,'(2a)') 'Error reading fits file: ',objfile
        return
      endif
      expos = fhead('EXPOSURE',objhead)
      if (expos.lt.1.) expos=99000.d0
      call GetDimensions(objhead,bsc,bec,bsr,ber,nc,nr)
      if((sc.ne.bsc).or.(sr.ne.bsr).or.(ec.ne.bec).or.(er.ne.ber))then
        write(soun,'(a)')     
     .  'ar2obj:ERROR- image dimensions do not match.'
        write(soun,'(a,4i6)') ' sc,ec,sr,er   =',sc,ec,sr,er
        write(soun,'(a,4i6)') 'bsc,bec,bsr,ber=',bsc,bec,bsr,ber
        ok=.false.
        return
      endif
C Echo angles.
      write(soun,'(2a)') 'Second file= ',objfile(1:40)
      ech2= sngl(fhead('ECHANGL',objhead))
      xd2 = sngl(fhead('XDANGL',objhead))
      if (ech2.gt.-1000.) then
        write(soun,'(a,2f9.4)') 'ECHA,XDA= ',ech2,xd2
      endif

C Check echelle and cross disperser angle.
      if (ech2.gt.-1000.) then
        if (abs(ech1-ech2).gt.0.005) write(soun,'(a)') 
     .       'WARNING: ECHA values do not match.'
        if (abs( xd1- xd2).gt.0.005) write(soun,'(a)') 
     .       'WARNING:  XDA values do not match.'
        if ((abs(ech1-ech2).gt.0.02).or.(abs(xd1-xd2).gt.0.02)) then
          write(soun,'(a)') 
     .  'arc2obj: ERROR- ECHA and XDA values inconsistent-- aborting.'
          ok=.false.
          return
        endif
      endif

C Check sky shift.
      if (sky) then
        if (card_exist('SKYSHIFT',objhead)) then
          write(soun,'(a)') 
     .  'arc2obj:WARNING: sky shift already applied.'
          write(soun,'(a)') 
     .  'arc2obj:WARNING: turning off "-sky" option and continuing...'
          sky=.false.
        endif
      endif

C Read variance fits file...
      if (var) then
        write(soun,'(2a)') 'Reading: ',varfile(1:lc(varfile))
        call readfits(varfile,v,m,varhead,ok)
        if (.not.ok) then
          write(soun,'(2a)') 'Error reading fits file: ',varfile
          return
        endif
      endif

C Read "SUM" fits file...
      if (sumfi) then
        write(soun,'(2a)') 'Reading: ',sumfile(1:lc(sumfile))
        call readfits(sumfile,s,m,sumhead,ok)
        if (.not.ok) then
          write(soun,'(2a)') 'Error reading fits file: ',sumfile
          return
        endif
      endif

C Check header for flags.
      hdr_vacuum = (getpos('VACUUM',objhead).ne.-1)
      hdr_helio  = (getpos('HELIOCNT',objhead).ne.-1)
      hdr_atmext = (getpos('ATMEXTNC',objhead).ne.-1)
      ok=.true.
      if ((vacuum).and.(hdr_vacuum)) then
        write(soun,'(a)') 
     .  'arc2obj:ERROR: Vacuum correction already applied.'
        ok = .false.
      endif
      if ((helio).and.(hdr_helio)) then
        write(soun,'(a)') 
     .  'arc2obj:ERROR: Heliocentric correction already applied.'
        ok = .false.
      endif
      if ((atmext).and.(hdr_atmext)) then
        write(soun,'(a)') 
     .'arc2obj:ERROR: Atmospheric extinc. correction already applied.'
        ok = .false.
      endif
      if (.not.ok) return

C Attempt to shift the ArcLamp wavelength scale to sky data before applying.
      if (sky) then
        rms     =0.d0
        skyshmea=0.d0
        npt     =0
C Read in sky data.
        write(soun,'(2a)') 'Reading: ',skyfile(1:lc(skyfile))
        call readfits(skyfile,skydata,m,skyhead,ok)
        if (.not.ok) then
          write(soun,'(2a)') 'Error reading fits file: ',skyfile
          return
        endif
C Use arclamp header and sky data.
        if (isky.and.(user_shift.lt.-1.d+10)) then
          if (device.eq.' ') device='/XDISP'
          r8 = SkyShift(skydata,sc,ec,sr,er,archead,
     .                  objhead,SkyLineFile,
     .                  15.,5,.true.,device,rms,npt,nrej)
          skyshmea = r8
          print '(a,$)',' Use this shift? (1/0/-1=enter own) : '
          read(5,*) i
          if (i.eq.-1) then
            print '(a,$)',' Enter shift= '
            read(5,*) r8
          elseif (i.eq.0) then
            r8=0.d0
          endif
        elseif (user_shift.gt.-1.d+10) then
          r8 = user_shift
        else
          if (device.eq.' ') device='none'
          r8 = SkyShift(skydata,sc,ec,sr,er,archead,
     .                  objhead,SkyLineFile,
     .                  15.,5,.true.,device,rms,npt,nrej)
          skyshmea = r8
C Check sky line fit for problems.
          if (npt.lt.5) then
            write(soun,'(2a)')'arc2obj: NOTE: bad skyline fit--too fe',
     .          'w points--no shift applied.'
            r8=0.d0
          elseif (rms.gt.0.4) then
            write(soun,'(2a)')
     .       'arc2obj: NOTE: bad skyline fit--RMS>0.4--no shi',
     .       'ft applied.'
            r8=0.d0
          elseif (expos.lt.420.) then
            write(soun,'(2a)') 'arc2obj: NOTE: bad skyline fit--Expo',
     .       'sure<420s--no shift applied.'
            r8=0.d0
          elseif (rms.lt.0.) then
            write(soun,'(a)')
     .   'arc2obj: NOTE: bad skyline fit--RMS<0.0--no shift applied.'
            r8=0.d0
          elseif (rms/max(abs(r8),1.d-9).gt.4.) then
            write(soun,'(2a,1pe12.4)') 'arc2obj: NOTE: no skyline sh',
     .              'ift applied: RMS/Shift ratio is > 4:',rms/r8
            r8=0.d0
          endif
        endif
        if (abs(r8).gt.1.d-10) then
          write(soun,'(a,f10.4,a)') ' Applying skyline shift of',
     .              r8,' pixels to arclamp scale.'
          call PixelShift(archead,r8)
        else
          r8 = 0.d0
          write(soun,'(a)') 'arc2obj: NOTE: No skyline shift applied.'
        endif
        write(soun,'(a,f10.4,a,f10.4,a,f8.4,a,i4)') 
     .  'Sky Shift used=',r8,'   measured=',skyshmea,
     .  '   RMS=',rms,'  #Pt=',npt
        call fheadset('SKYSHIFT',r8,objhead)
        call fheadset('SKYSHMEA',skyshmea,objhead)
        call fheadset('SKYSHRMS',rms,objhead)
        call inheadset('SKYSHNPT',npt,objhead)
        if (sumfi) call fheadset('SKYSHIFT',r8,sumhead)
        if (sky) call fheadset('SKYSHIFT',r8,skyhead)
      endif

      if (atmext) then
C Read in extinction values.
        open(1,file=ExtinctionFile,status='old',iostat=i)
        if (i.ne.0) goto 850
        next=0
11      read(1,*,end=55) r81,r82
        next=next+1
        extx(next) = r81
        exty(next) = r82
        goto 11
55      close(1)
C Air mass.
        airmass = fhead('AIRMASS',objhead)
        if ((airmass.lt.1.).or.(airmass.gt.5.)) then
          write(soun,'(a,1pe12.4)') 
     .  'Error- AIRMASS FITS card missing or bad:',airmass
          return
        endif
      endif

C Heliocentric velocity correction scale factor.
      if (helio) then
        call get_hvsf(objhead,hvsf)
      endif

C Notes to user.
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',er
      if (getpos(c7,objhead).ne.-1) then
        write(soun,'(a)') 
     .  'NOTE: Object already has wavelength scale, will overwrite.'
      endif
      if ((.not.vacuum).and.(.not.helio)) then
        write(soun,'(a)') 
     .  'NOTE: No vacuum or heliocentric corrections will be done.'
      endif

C Shift polynomial solution and add wavelength cards to header of image 2.
      DO row=sr,er

C Initialize.
      do i=0,7
        coef(i) = 0.
        bcoef(i)= 0.
      enddo

C We use 7th order in HIRES reduced arc lamps, although they are really only
C 6th order polynomials for compatibility with Vista.
      ord = 7

C Extract coeffec. from arclamp (note archead has been changed if -sky used.)
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,archead)
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) coef(0),coef(1),coef(2),coef(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,archead)
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) coef(4),coef(5),coef(6),coef(7)

C Add coeffecients to sky header (never do vac or helio or atmext correction.)
      if (sky) then
        write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
        write(wv0,'(4(1pe17.9))',err=805) 
     .  coef(0),coef(1),coef(2),coef(3)
        call cheadset(c7,wv0,skyhead)
        write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
        write(wv4,'(4(1pe17.9))',err=805) 
     .  coef(4),coef(5),coef(6),coef(7)
        call cheadset(c7,wv4,skyhead)
        call cheadset('ARCSPFIL',arcfile,skyhead)
      endif
   
C Apply vacuum and/or helio corrections.
      if (vacuum.or.helio) then
C Fill array and apply wavelength correction.
        n=0
        do i=sc,ec,10
          n=n+1
          x8(n)= dfloat(i)
          wave = polyval(ord+1,coef,dfloat(i))
C Convert from air wavelengths to vacuum wavelengths.
          if (vacuum) wave = air2vac(wave)
C Convert to heliocentric.
          if (helio) wave = wave * hvsf
          y8(n)= wave
          w8(n)= 1.d0
        enddo
C Refit polynomial.  We choose ord=6 for compatibility with Vista.
        ord=6
        bcoef(7)=0.d0
        call poly_fit_glls(n,x8,y8,w8,ord,bcoef,ok)
        if (.not.ok) then
          write(soun,'(a)') 'Error refitting polynomial.'
          return
        endif
C Check residuals.
        call poly_fit_glls_residuals(n,x8,y8,w8,ord,bcoef,high,wrms)
        if ((wrms.gt.0.00001).or.(high.gt.0.0001)) then
          write(soun,'(a,i5,2f15.7)') 
     .  ' Row, High, WRMS (Ang) = ',row,high,wrms
        endif
      else
C Copy coeffecients.
        ord=7
        do i=0,7
          bcoef(i) = coef(i)
        enddo
      endif

C Add coeffecients to second header if necessary.
      if (vacuum.or.helio.or.((.not.hdr_vacuum).and.
     .                               (.not.hdr_helio))) then
        write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
        write(wv0,'(4(1pe17.9))',err=805) 
     .  bcoef(0),bcoef(1),bcoef(2),bcoef(3)
        call cheadset(c7,wv0,objhead)
        if (var) call cheadset(c7,wv0,varhead)
        if (sumfi) call cheadset(c7,wv0,sumhead)
        write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
        write(wv4,'(4(1pe17.9))',err=805) 
     .  bcoef(4),bcoef(5),bcoef(6),bcoef(7)
        call cheadset(c7,wv4,objhead)
        if (var) call cheadset(c7,wv4,varhead)
        if (sumfi) call cheadset(c7,wv4,sumhead)
      endif

C Convert variance to error.
      if (var) then
        do i=sc,ec
          call SqrtVar(v,sc,ec,sr,er,i,row)
        enddo
      endif
   
      if (atmext) then
C Fit extinction correction polynomial.
        wv1 = polyval(ord+1,bcoef,dfloat(sc))
        wv2 = polyval(ord+1,bcoef,dfloat(ec))
        call GetExtinctionPoly(wv1,wv2,next,extx,exty,extord,extcoef)
C Apply atmospheric extinction correction.
        do i=sc,ec
          wave = polyval(ord+1,bcoef,dfloat(i))
          value= sngl(polyval(extord+1,extcoef,wave)**airmass)
          call DivideValue(b,sc,ec,sr,er,i,row,value)
          if (sumfi) call DivideValue(s,sc,ec,sr,er,i,row,value)
          if (var) call DivideErrValue(v,sc,ec,sr,er,i,row,value)
        enddo
      endif

      ENDDO

C Add stuff to FITS header.
      if (vacuum) then
        write(soun,'(a)') 'Air to vacuum correction applied...'
        call cheadset('VACUUM  ',
     .       'Air to vacuum correction applied.',objhead)
        if (sumfi) call cheadset('VACUUM  ',
     .              'Air to vacuum correction applied.',sumhead)
      endif
      if (helio) then
        write(soun,'(a)') 'Heliocentric velocity correction applied...'
        call cheadset('HELIOCNT',
     .  'Heliocentric correction applied.',objhead)
        if (sumfi) call cheadset('HELIOCNT',
     .              'Heliocentric correction applied.',sumhead)
        r81 = hvsf*hvsf
        r82 = (2.997925d+5)*(r81-1.d0)/(r81+1.d0)
        call fheadset('HELIOVEL',r82,objhead)
        if (sumfi) call fheadset('HELIOVEL',r82,sumhead)
      endif
      if (atmext) then
        write(soun,'(a)')'Atmospheric extinction correction applied...'
        call cheadset('ATMEXTNC',
     .          'Atmospheric extinction correction applied.',objhead)
        if (sumfi) call cheadset('ATMEXTNC',
     .          'Atmospheric extinction correction applied.',sumhead)
      endif
C If we put a wavelength scale on, refer to the arc lamp.
      if (vacuum.or.helio.or.((.not.hdr_vacuum).and.
     .                               (.not.hdr_helio))) then
        call cheadset('ARCSPFIL',arcfile,objhead)
        if (sumfi) call cheadset('ARCSPFIL',arcfile,sumhead)
      endif
C Command line.
      call cheadset('ARC2OBJ',commline,objhead)

C Write out second FITS file... the object file.
      write(soun,'(2a)') 'Over-writing file: ',objfile(1:lc(objfile))
      call cheadset('FILENAME',objfile,objhead)
      call writefits(objfile,b,objhead,ok)
      if (.not.ok) then
        write(soun,'(2a)') 'Error writing fits file: ',objfile
        return
      endif
      if (sumfi) then
        write(soun,'(2a)')'Over-writing file: ',sumfile(1:lc(sumfile))
        call cheadset('FILENAME',sumfile,sumhead)
        call writefits(sumfile,s,sumhead,ok)
        if (.not.ok) then
          write(soun,'(2a)') 'Error writing fits file: ',sumfile
          return
        endif
      endif
      if (sky) then
        write(soun,'(2a)')'Over-writing file: ',skyfile(1:lc(skyfile))
        call cheadset('FILENAME',skyfile,skyhead)
        call writefits(skyfile,skydata,skyhead,ok)
        if (.not.ok) then
          write(soun,'(2a)') 'Error writing fits file: ',skyfile
          return
        endif
      endif

C Write out error FITS file.
      if (var) then
        wrd = chead('OBJECT',varhead)
        i = index(wrd,'(Variance)')
        if (i.eq.0) then
          wrd = wrd(1:lc(wrd))//' (Error)'
        else
          wrd(i:i+9) ='(Error)   '
        endif
        call cheadset('OBJECT',wrd,varhead)
        call cheadset('VAR2ARR',
     .          'Variance converted to errors.',varhead)
        write(soun,'(2a)') 'Writing file: ',errfile(1:lc(errfile))
        call cheadset('FILENAME',errfile,varhead)
        call writefits(errfile,v,varhead,ok)
        if (.not.ok) then
          write(soun,'(2a)') 
     .              'arc2obj: Error writing fits file: ',errfile
          return
        endif
      endif
      
      write(soun,'(a)') 'arc2obj: All done.'
      ok=.true.
      return
800   continue
      write(soun,'(a)') 'Error reading WV header card.'
      ok=.false.
      return
805   continue
      write(soun,'(a)') 'Error writing WV header card.'
      ok=.false.
      return
850   continue
      write(soun,'(2a)') 'Error could not find: ',ExtinctionFile
      write(soun,'(a)')
     .  ' You may have to edit the file arc2obj.f and change the'
      write(soun,'(a)')
     .  ' paramater "ExtinctionFile" to the appropriate value.'
      ok=.false.
      return
      end


C----------------------------------------------------------------------
C Find correlation shift between two arclamps and copy wavelength scale.
C
      subroutine arc2arc_main(arg,narg,ok)
C
      implicit none
      character*(*) arg(*)
      integer*4 narg
      logical ok
C
      include 'soun.inc'
C
      integer*4 asc,aec,asr,aer,bsc,bec,bsr,ber
      integer*4 nc,nr,inhead,i,row,n,ord
      character*115200 header(2)
      character*80 file(2),chead,wv0,wv4,wrd
      character c7*7
      integer*4 maxpt
      parameter(maxpt=201000)
      real*4 a(maxpt),b(maxpt),c(maxpt),centroid
      real*4 ech1,xd1,ech2,xd2,pxsh,valread
      real*8 shift,coef(0:8),high,wrms,polyval,bcoef(0:8),fhead
      real*8 x8(9000),y8(9000),w8(9000)
      logical desat,findarg,plot,have_pxsh

C Look at command line parameters.
      desat= findarg(arg,narg,'-desat',':',wrd,i)
      plot = findarg(arg,narg,'-plot',':',wrd,i)
      pxsh = 0.
      have_pxsh = findarg(arg,narg,'pxsh=',':',wrd,i)
      if (have_pxsh) pxsh=valread(wrd) 

C Syntax.
      if ((narg.lt.2).or.(narg.gt.2)) then
        print *,'Syntax: arc2arc (FITS file) (2nd FITS file)'
        print *,'                       [-desat] [-plot] [pxsh=]'
        print *,
     .  '   Find pixel shift between two reduced arclamp spec/images.'
        print *,'   Find pixel shift of #2 relative to #1.'
        print *,'   Copy wavelength scale from #1 to #2.'
        print *,
     .  '   It is assumed that the setups are similar (+/- 20 pixels.)'
        print *,'-desat : Blank out saturated and bleeding columns.'
        print *,'-plot  : Show correlation in PGDISP (XDISP) window.'
        print *,
     .  'pxsh=  : Specify pixel shift explicitly (bypass calculation.)'
        print *,'         Use "pxsh=0" to copy the wavelength scale.'
        ok=.false.
        return
      endif
      file(1) = arg(1)
      file(2) = arg(2)
      call AddFitsExt(file(1))
      call AddFitsExt(file(2))

C Read first fits file...
      call readfits(file(1),a,maxpt,header(1),ok)
      if (.not.ok) then
        write(soun,'(2a)') 'Error reading fits file: ',file(1)
        return
      endif
      nc = inhead('NAXIS1',header(1))
      nr = inhead('NAXIS2',header(1))
      asc= inhead('CRVAL1',header(1))
      asr= inhead('CRVAL2',header(1))
      aec= asc + nc - 1
      aer= asr + nr - 1
      if (desat.and.(.not.have_pxsh)) then
        call arcdesat(a,c,asc,aec,asr,aer,60000.,i)
        if (i.gt.0) write(soun,'(i5,a)') i,
     .   ' bad (saturated and bleeding) pixels found in first image.'
      endif
      write(soun,'(2a)') 'First file= ',file(1)(1:40)
      ech1= sngl(fhead('ECHANGL',header(1)))
      xd1 = sngl(fhead('XDANGL',header(1)))
      if (ech1.gt.-1000.) then
        write(soun,'(a,2f9.4)') 'ECHA,XDA= ',ech1,xd1
      endif
     
C Read second fits file...
      call readfits(file(2),b,maxpt,header(2),ok)
      if (.not.ok) then
        write(soun,'(2a)') 'Error reading fits file: ',file(2)
        return
      endif
      nc = inhead('NAXIS1',header(2))
      nr = inhead('NAXIS2',header(2))
      bsc= inhead('CRVAL1',header(2))
      bsr= inhead('CRVAL2',header(2))
      bec= bsc + nc - 1
      ber= bsr + nr - 1
      if ((asc.ne.bsc).or.(asr.ne.bsr).or.
     .              (aec.ne.bec).or.(aer.ne.ber)) then
        write(soun,'(a)') 'ERROR- image dimensions do not match.'
        write(soun,'(a,4i6)') 'asc,aec,asr,aer=',asc,aec,asr,aer
        write(soun,'(a,4i6)') 'bsc,bec,bsr,ber=',bsc,bec,bsr,ber
        ok=.false.
        return
      endif
      if (desat.and.(.not.have_pxsh)) then
        call arcdesat(b,c,bsc,bec,bsr,ber,60000.,i)
        if (i.gt.0) write(soun,'(i5,a)') i,
     .  ' bad (saturated and bleeding) pixels found in second image.'
      endif

      write(soun,'(2a)') 'Second file= ',file(2)(1:40)
      ech2= sngl(fhead('ECHANGL',header(2)))
      xd2 = sngl(fhead('XDANGL',header(2)))
      if (ech2.gt.-1000.) then
        write(soun,'(a,2f9.4)') 'ECHA,XDA= ',ech2,xd2
      endif

      if (ech2.gt.-1000.) then
        if (abs(ech1-ech2).gt.0.01) write(soun,'(a)') 
     .    'WARNING: ECHA values do not match.'
        if (abs( xd1- xd2).gt.0.01) write(soun,'(a)') 
     .    'WARNING:  XDA values do not match.'
      endif

C Create and centroid correlation spectrum.
      if (have_pxsh) then
        shift = dble(pxsh)
      else
        call spcorr(a,asc,aec,asr,aer,b,bsc,bec,bsr,ber,centroid,plot)
        shift = dble(centroid)
      endif
      write(soun,'(a,f11.6)') ' Shift in pixels=',shift

C Add pixel shift to header.
      if (abs(shift).gt.1.d-30) then
        call fheadset('PIXSHIFT',shift,header(2))
      endif

C Re-read the data in the second file if "desat" was applied.
      if (desat) call readfits(file(2),b,maxpt,header(2),ok)

C Note to user.
      if (abs(shift).lt.1.d-30) write(soun,'(a)') 
     .  ' Zero shift, no refitting necessary.'

C Shift polynomial solution and add wavelength cards to header of image 2.
      DO row=asr,aer

C Extract coeffecients.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,header(1))
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) coef(0),coef(1),coef(2),coef(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,header(1))
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) coef(4),coef(5),coef(6),coef(7)

C Apply shift.
      if (abs(shift).lt.1.d-30) then
        do i=0,7
          bcoef(i) = coef(i)
        enddo
      else
C Fill array and apply pixel shift.
        n=0
        do i=asc,aec,10
          n=n+1
          x8(n)=dfloat(i) + shift
          y8(n)=polyval(7,coef,dfloat(i))
          w8(n)=1.d0
        enddo
C Refit polynomial.
        ord=6
        call poly_fit_glls(n,x8,y8,w8,ord,bcoef,ok)
        if (.not.ok) then
          write(soun,'(a)') 'Error refitting polynomial.'
          return
        endif
C Check residuals.
        call poly_fit_glls_residuals(n,x8,y8,w8,ord,bcoef,high,wrms)
        if ((wrms.gt.0.00001).or.(high.gt.0.0001)) then
          write(soun,'(a,i5,2f15.7)') 
     .  ' Row, High, WRMS (Ang) = ',row,high,wrms
        endif
      endif

C Add coeffecients to second header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      write(wv0,'(4(1pe17.9))',err=805) 
     .  bcoef(0),bcoef(1),bcoef(2),bcoef(3)
      call cheadset(c7,wv0,header(2))
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      write(wv4,'(4(1pe17.9))',err=805) 
     .  bcoef(4),bcoef(5),bcoef(6),bcoef(7)
      call cheadset(c7,wv4,header(2))

      ENDDO

C Write out second FITS file.
      call writefits(file(2),b,header(2),ok)
      if (.not.ok) then
        write(soun,'(2a)') 'Error writing fits file: ',file(2)
        return
      endif
      
      write(soun,'(a)') 'arc2arc: All done.'
      ok=.true.
      return
800   continue
      write(soun,'(a)') 'Error reading WV header card.'
      ok=.false.
      return
805   continue
      write(soun,'(a)') 'Error writing WV header card.'
      ok=.false.
      return
      end


C----------------------------------------------------------------------
C Average wavelength scales from two arclamp files.
C Put resulting new scale in first FITS header.
C
      subroutine a2o_AverageWaveScales(header1,header2,arcmode,ok)
C
      implicit none
      character*(*) header1    ! First FITS header (input/output)
      character*(*) header2    ! Second FITS header (input)
      integer*4 arcmode        ! Mode for averaging wavelength scales (input).
C                              !  0 = average scales equally.  
C                              !  1 = shift scale on arc#2 to arc#1 position.
C                              !  2 = shift scale on arc#1 to arc#2 position.
      logical ok               ! Success? (output)
C
      include 'soun.inc'
C
      real*8 fhead,ech1,xda1,ech2,xda2
      real*8 coef1(0:7),coef2(0:7),coeff(0:7),pxsh
      integer*4 sc1,ec1,sr1,er1,nc1,nr1
      integer*4 sc2,ec2,sr2,er2,nc2,nr2,row,i
      character*80 chead,wv0,wv4
      character c7*7
C
C Make sure image sizes and angles match up.
      call GetDimensions(header1,sc1,ec1,sr1,er1,nc1,nr1)
      ech1 = fhead('ECHANGL',header1)
      xda1 = fhead('XDANGL' ,header1)
      call GetDimensions(header2,sc2,ec2,sr2,er2,nc2,nr2)
      ech2 = fhead('ECHANGL',header2)
      xda2 = fhead('XDANGL' ,header2)
      if ((sc1.ne.sc2).or.(ec1.ne.ec2).or.
     .           (sr1.ne.sr2).or.(er1.ne.er2)) then
        write(soun,'(a)') 
     .  'ERROR: arc2obj: Two arclamp dimensions do not match.'
        ok=.false.
        return
      endif
C Generous angle constraints.
      if ((abs(ech1-ech2).gt.0.01).or.(abs(xda1-xda2).gt.0.01)) then
        write(soun,'(a)') 
     .  'ERROR: arc2obj: Setup angles between arclamps do not match.'
        ok=.false.
        return
      endif
C
C.................................
C Average each row.
      IF (arcmode.eq.0) THEN
      DO row=sr1,er1
C Extract coeffecients from first header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,header1)
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) 
     .  coef1(0),coef1(1),coef1(2),coef1(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,header1)
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) 
     .  coef1(4),coef1(5),coef1(6),coef1(7)
C Extract coeffecients from second header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,header2)
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) 
     .  coef2(0),coef2(1),coef2(2),coef2(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,header2)
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) 
     .  coef2(4),coef2(5),coef2(6),coef2(7)
C Average.
      do i=0,7
        coef1(i) = ( coef1(i) + coef2(i) ) / 2.d0
      enddo
C Write coeffecients back to first header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      write(wv0,'(4(1pe17.9))',err=800) 
     .  coef1(0),coef1(1),coef1(2),coef1(3)
      call cheadset(c7,wv0,header1)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      write(wv4,'(4(1pe17.9))',err=800) 
     .  coef1(4),coef1(5),coef1(6),coef1(7)
      call cheadset(c7,wv4,header1)
      ENDDO
      ENDIF
C
C.................................
C Shift scales.
      IF ((arcmode.eq.1).or.(arcmode.eq.2)) THEN

C Find Shift.
      if (arcmode.eq.1) then
        call a2o_findpixelshift(header2,header1,pxsh)
      else
        call a2o_findpixelshift(header1,header2,pxsh)
      endif
C
      DO row=sr1,er1
C Extract coeffecients from first header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,header1)
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) 
     .  coef1(0),coef1(1),coef1(2),coef1(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,header1)
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) 
     .  coef1(4),coef1(5),coef1(6),coef1(7)
C Extract coeffecients from second header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,header2)
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) 
     .  coef2(0),coef2(1),coef2(2),coef2(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,header2)
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) 
     .  coef2(4),coef2(5),coef2(6),coef2(7)
C Apply Shift and load into coeff().
      if (arcmode.eq.1) then
        call a2o_applypixelshift(sc1,ec1,coef2,coeff,pxsh)
      else
        call a2o_applypixelshift(sc1,ec1,coef1,coeff,pxsh)
      endif
C Write coeffecients back to first header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      write(wv0,'(4(1pe17.9))',err=800) 
     .  coeff(0),coeff(1),coeff(2),coeff(3)
      call cheadset(c7,wv0,header1)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      write(wv4,'(4(1pe17.9))',err=800) 
     .  coeff(4),coeff(5),coeff(6),coeff(7)
      call cheadset(c7,wv4,header1)
      ENDDO
C
      ENDIF
C
      ok=.true.
      return
C
800   continue
      write(soun,'(a)') 
     .  'ERROR: arc2obj: reading or writing wavelength values.'
      ok=.false.
      return
      end


C----------------------------------------------------------------------
C Find pixel shift from "1" to "2".
C This will be used to shift the polynomial scale in "1" to "2".
C
      subroutine a2o_findpixelshift(header1,header2,pxsh)
C
      implicit none
      character*(*) header1,header2   ! FITS headers (input)
      real*8 pxsh                     ! Pixel shift (output)
C
      include 'soun.inc'
      include 'maxpix.inc'
      real*8 p(maxord*(maxcol/20))
C
      real*8 polyval,x,disp
      real*8 coef1(0:7),coef2(0:7)
      integer*4 col,row,n,sc,ec,sr,er,nc,nr
      character*80 wv0,wv4,chead
      character c7*7
C
C Dimensions.
      call GetDimensions(header1,sc,ec,sr,er,nc,nr)
C Initial.
      n=0
C Go through each order.
      do row=sr,er
C Extract coeffecients from first header.
        write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
        wv0 = chead(c7,header1)
        if (wv0.eq.' ') goto 800
        read(wv0,'(4(1pe17.9))',err=800) 
     .  coef1(0),coef1(1),coef1(2),coef1(3)
        write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
        wv4 = chead(c7,header1)
        if (wv4.eq.' ') goto 800
        read(wv4,'(4(1pe17.9))',err=800) 
     .  coef1(4),coef1(5),coef1(6),coef1(7)
C Extract coeffecients from second header.
        write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
        wv0 = chead(c7,header2)
        if (wv0.eq.' ') goto 800
        read(wv0,'(4(1pe17.9))',err=800) 
     .  coef2(0),coef2(1),coef2(2),coef2(3)
        write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
        wv4 = chead(c7,header2)
        if (wv4.eq.' ') goto 800
        read(wv4,'(4(1pe17.9))',err=800) 
     .  coef2(4),coef2(5),coef2(6),coef2(7)
C Every 20th pixel.
        do col=sc+20,ec-20,20
          n=n+1
          x   = dfloat(col) 
          disp= 
     .  ( polyval(7,coef1,x+1.d0)-polyval(7,coef1,x-1.d0) ) /2.d0
          p(n)= ( polyval(7,coef1,x) - polyval(7,coef2,x) ) /disp
        enddo
      enddo
C Median.
      call find_median8(p,n,pxsh)
C
      return
800   continue
      write(soun,'(a)') 
     .  'ERROR: a2o_findpixelshift: reading wavelength scale.'
      write(soun,'(a)') 
     .  'ERROR: a2o_findpixelshift: using zero pixel shift.'
      pxsh=0.d0
      return
      end


C----------------------------------------------------------------------
C Apply pixel shift to coefa() and copy to coeff().
C
      subroutine a2o_applypixelshift(sc,ec,coefa,coeff,pxsh)
C
      implicit none
      integer*4 sc,ec        ! Starting and ending columns (input)
      real*8 coefa(0:7)      ! (input)
      real*8 coeff(0:7)      ! (output)
      real*8 pxsh            ! (input)
C
      include 'scratch.inc'
      include 'soun.inc'
C
      real*8 high,wrms,polyval
      integer*4 i
      logical ok
C
C Copy.
      do i=0,7
        coeff(i) = coefa(i)
      enddo
C Set up points.
      do i=1,500
        x8(i) = dfloat(sc) + ( (dfloat(i)/500.d0) * dfloat(ec-sc) )
        y8(i) = polyval(7,coeff,x8(i))
        w8(i) = 1.d0
      enddo
C Apply shift to points.
      do i=1,500
        x8(i) = x8(i) + pxsh
      enddo
C Refit forward polynomial.
      call poly_fit_glls(500,x8,y8,w8,6,coeff,ok)
      coeff(7) = 0.d0
C Check residuals.
      call poly_fit_glls_residuals(500,x8,y8,w8,7,coeff,high,wrms)
      if ((high.gt.0.00001).or.(wrms.gt.0.000001)) then
        write(soun,'(2a,2(1pe15.6))') 
     .      'WARNING: a2o_applypixelshift: High errors for',
     .      'ward polynomial fit:',high,wrms
        pxsh=0.d0
        return
      endif
      return
      end


