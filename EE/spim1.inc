C spim1.inc
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C mpp  = maximum polynomial parameters (coeffecients).
C mo   = maximum number of echelle orders.
C maxl = maximum number of lines in a given row.
C maxr = maximum number of rows (orders).
C maxdb= maximum number of lines if ThAr database.
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      integer*4 maxl,maxr,mpp,mo,maxdb
      parameter(maxl=500,maxr=200,mpp=19,mo=99,maxdb=9000)
C Scratch arrays for polynomial fits.
      real*8 x8(maxl),y8(maxl),w8(maxl),coef(0:20),mcoef(0:20,maxr)
      real*8 x8b(maxl),y8b(maxl),w8b(maxl)
C Polynomial coeffecients defining original echelle order traces.
      real*8 co(mpp,mo)
C Thorium-Argon database.
      real*8 thar(maxdb),tharI(maxdb)
C Line IDs and other.
      real*8 lc(maxl,maxr,2),lw(maxl,maxr,2),ls(maxl,maxr,2)
      real*8 lres(maxl,maxr,2),lwgt(maxl,maxr,2),ssf,lrms(maxr)
      real*4 arr(maxdb),mres(maxr)
      real*4 weakline(2),satuline(2)
      integer*4 nl(maxr,2),nthar,mord(maxr),tharR(maxdb),lord(maxr)
      integer*4 nautokey,numim
      logical newli,newim,NoCentroid,vt100,hist,hide,pgd
      logical spim1_HIRES,spim1_ESI,spim1_HIRES2
      character*1 autokey(999)
      common /spimblk/ x8,y8,w8,x8b,y8b,w8b,coef,
     .      mcoef,co,thar,tharI,
     .      lc,lw,ls,lres,lwgt,ssf,lrms,
     .      arr,mres,weakline,satuline,
     .      nl,nthar,mord,tharR,lord,nautokey,numim,
     .      newli,newim,NoCentroid,vt100,hist,hide,pgd,
     .      spim1_HIRES,spim1_ESI,spim1_HIRES2,
     .      autokey
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C x8,y8,w8,x8b,y8b,w8b,coef,mcoef  = scratch space for polynomial fitting.
C co(,) = defines row pixel position in original image.
C thar  = array of Thorium-Argon line wavelengths in air.
C tharI = estimated relative intensity of lines.
C tharR = rating of lines indicating how good a fit they tend to yield.
C lc    = line center in pixels.
C lw    = line wavelength (in air).
C ls    = line strength (DN value at pixel centroid).
C lrms  = WRMS for the final polynomial fit for a given row.
C lord  = order used for the final polynomial fit for a given row.
C ssf   = strength scaling factor (towards clear filter value).
C arr() = scratch space.
C mres  = median residual in polynomial fit for a given row.
C mord  = polynomial order used in fit for a given row.
C nl    = number of lines in a given row and image.
C nthar = number of ThAr lines in the database.
C newli = lines have been added to line array (need to write out data at end.)
C newim = write the image out at end since we have added new polynomial
C         coeffecients to the header.
C NoCentroid = When centroiding on a line, do not centroid, just use initial
C              position.  Used for lines close to each other.
C vt100 = draw plots appropriate for a VT-100 with retrographics.
C hist  = plot in histogram mode.
C hide  = hide secondary image.
C twod  = do a two dimensional fit to wavelengths/pixels (experimental).
C numim = number of images (1 or 2).
C pgd   = PGPLOT Display flag.  If .false., no plotting in certain routines.
C spim1_HIRES = Is this HIRES data?
C spim1_ESI   = Is this ESI data?
C autokey = Key strokes stored in array which saves having to type the sequence
C weakline(i) = minimum peak level to accept weak line in image "i".
C satuline(i) = maximum peak level to accept line (saturation) in image "i".
C modified = If data has been changed re-write the image data.
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
