C biw.f          Baseline-Interpolate-Window an image      tab Mar00
C
C----------------------------------------------------------------------
C
      implicit none
C
      include 'maxpix.inc'
      include 'global.inc'
      include 'soun.inc'
C
      real*4 aa(maxpix)
      real*4 bb(maxpix)
      real*4 eperdn,ronoise
      integer*4 mp
      parameter(mp=9000)
      real*4 bias(mp)
      integer*4 i,narg,lc,maskmode
      integer*4 sc,ec,sr,er,nc,nr,xbin,ybin
      character*115200 header,biashead
      character*80 arg(9),infile,outfile,wrd,maskfile,biasfile
      logical ok,findarg,select,EE_QueryHIRES,EE_QueryESI,EE_QueryHIRES2
      logical biasmode
C

C Standard output unit number.
      soun = 6

C Command line arguments.
      call arguments(arg,narg,9)

C Masking file.
      maskfile=' '
      if (findarg(arg,narg,'maskfile=',':',wrd,i)) maskfile = wrd

C Select a new mask file?
      select = findarg(arg,narg,'-select','s',wrd,i)

C Interpolate or mask?
      if (findarg(arg,narg,'-mask','s',wrd,i)) then
        maskmode = 1
      else
        maskmode = 0
      endif

C Subtract bias?
      biasmode = findarg(arg,narg,'-bias','s',wrd,i)

C ESI interpolation.
      if (findarg(arg,narg,'-esi','s',wrd,i)) maskmode = 2

C Syntax.
      if (narg.ne.2) then
        print *,'Syntax:  biw  (input FITS file)  (output FITS file)'
        print *,'          [-select]  [-mask]  [-esi]  [maskfile=]'
        print *,'          [-bias]'
        print *,' '
        print *,'     Baseline-Interpolate-Window an image.'
        print *,'     Also "fixes" Keck header cards.'
        print *,'Options:'
        print *,'   -select    : Select a new mask file.'
        print *,'   maskfile=  : Use this maskfile.'
        print *,
     .   '    -mask     : Mask bad columns, instead of interpolating.'
        print *,
     .   '    -esi      : Mask bad columns then ESI interpolate.'
        print *,'   -bias      : Lookup and subtract bias after BIW.'
        print *,' '
        call exit(0)
      endif

C Read files.
      infile = arg(1)
      call AddFitsExt(infile)
      outfile= arg(2)
      call AddFitsExt(outfile)
      call readfits(infile,aa,maxpix,header,ok)
      if (.not.ok) goto 801

C Which instrument?
      Global_HIRES = EE_QueryHIRES(header)
      Global_ESI   = EE_QueryESI(header)
      Global_HIRES2= EE_QueryHIRES2(header)

C Fix Keck cards.
      eperdn=-1.
      ronoise=-1.
      call FixKeckCards(outfile,header,eperdn,ronoise)

C Dimensions.
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
      print '(a,4i5)',' Old dimensions (sc,ec,sr,er) : ',sc,ec,sr,er

C Binning.
      call GetBinning(header,xbin,ybin)
      print '(a,i3,a,i3)',' Binning:  xbin=',xbin,'   ybin=',ybin

C Select a new mask file.
      if (select) then
        call mk_select_maskfile(header,maskfile)
        print '(2a)','New mask file: ',maskfile(1:lc(maskfile))
      endif

C Baseline-Interpolate(or mask)-Window (and rotate if necessary).
      call biw(aa,bb,header,maskfile,maskmode,sc,ec,sr,er,ok)
      print '(a,4i5)',' New dimensions (sc,ec,sr,er) : ',sc,ec,sr,er

C Bias subtraction?
      if (biasmode) then
        call EE_GetBiasFile(header,biasfile)
        print *,'Reading bias file: ',biasfile(1:lc(biasfile))
        call readfits(biasfile,bias,mp,biashead,ok) 
        print *,'Subtracting bias.'
        call EE_SubtractBias(aa,bias,sc,ec,sr,er)
      endif

C Write file.
      print '(2a)',' Writing : ',outfile(1:lc(outfile))
      call writefits(outfile,aa,header,ok)
      if (.not.ok) goto 801

      stop
801   print *,'Error writing/reading FITS image.'
      stop
      end

