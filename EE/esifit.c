/* esifit.c                                      tab  Sep00 */

#include "ctab.h"
#include "cmisc.h"
#include "fstuff.h"
#include "cpgplot.h"
#include "cpg-util.h"
#include "cufio.h"

/* Maximum number of points per order and maximum number of orders. */
#define maxlpo  901
#define maxord   99
#define maxpt   901
#define maxpoly  99
#define maxpxpt 901
#define maxpix  90100

/* . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . */
/* Global variables                                                */

int POtwo;          /* polynomial order for second iteration. */
int POthree;        /* polynomial order for third (cross_wave) iteration. */
float sch;          /* Set character height. */
float svp;          /* Set viewport percentage. */
int sym;            /* Symbol number for points, show points if sym>0. */
int nh,nv;          /* number of windows in multi-plot. */
int sls;            /* Set line style, 0=points, 1=solid, 2=3=dashes, 4=dots */
int slw;            /* Set line width. */
int verbose;        /* More messages. */
int hist;           /* Histogram mode? */
int dots;           /* Show dots instead of colors. */
int glls;           /* Use glls instead of gj for poly fitting. */

const int headersize = 115200;
char header120[ 115200 ];
char header121[ 115200 ];

float aa120[ maxpix ];
float aa121[ maxpix ];

double ids_pix[ maxord ][ maxlpo ];
double ids_wav[ maxord ][ maxlpo ];
double ids_wgt[ maxord ][ maxlpo ];   /* weight for fitting */
int    ids_num[ maxord ];
int    ids_maxord;

int    DeLinear;

double cross_wave[ maxord ][ maxpxpt ];   /* For cross echelle order fits. */

float xmin,xmax;    /* Horizontal view range. */
float ymin,ymax;    /* Vertical view range. */

float xmin0 =    0.;
float xmax0 = 4000.;

char title[100];    /* Title. */

char psfile[100];   /* PostScript filename. */
char device[100];   /* Device name. */

float xmin,xmax,ymin,ymax;

int nn1,nn2,nn3,nn4;
float xx1[maxpt],yy1[maxpt],zz1[maxpt];
float xx2[maxpt],yy2[maxpt],zz2[maxpt];
float xx3[maxpt],yy3[maxpt],zz3[maxpt];
float xx4[maxpt],yy4[maxpt],zz4[maxpt];

/* for fitting. */
int PolyOrder;
double x8[maxpt],y8[maxpt],w8[maxpt],coef[maxpoly];
double xoff;

/* . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . */


/* ----------------------------------------------------------------------
  Find dispersion given current global variables.
*/
double esifit_dispersion()
{
/**/
double sum,wsum,xv,disp;
/**/
/* Find dispersion. */
sum =0.;
wsum=0.;
for (xv=xmin0; xv<xmax0; xv=xv+2.) {
  sum = sum + ( cpolyval(PolyOrder+1,coef,1.+xv-xoff) - 
                cpolyval(PolyOrder+1,coef,   xv-xoff) );
  wsum= wsum+ 1.;
}
disp = sum / wsum;
return(disp);
}


/* ----------------------------------------------------------------------
  Fit an n-dimensional line to an order.
  Input: eon    : Echelle order number 
                    (Note that eon starts with 0 within esifit.c)
*/
void esifit_poly_fit( int eon, double *pixwrms )
{
/**/
int ii,order,npt,forcexoff,iok;
double high,wrms,wppd,disp;
/**/

/* Clear. */
for (ii=0; ii<maxpoly; ++ii) { coef[ii] = 0.; }

/* Load fitting variables. */
for (ii=0; ii<ids_num[eon]; ++ii) {
  x8[ii] = ids_pix[eon][ii];
  y8[ii] = ids_wav[eon][ii];
  w8[ii] = ids_wgt[eon][ii];
}
npt = ids_num[eon];

/* Polynomial fit. */
order = PolyOrder;
xoff  = 0.;
forcexoff = 0;
iok   = 0;
if (glls == 1) {
  f2cpolyfitglls_ ( &npt, x8, y8, w8, &order, coef, &iok );
  xoff = 0.;
} else {
  f2cpolyfitgj_ ( &npt, x8, y8, w8, &order, coef, &xoff, &forcexoff, &iok );
}

/* Find Ang/pixel. */
disp = esifit_dispersion();

/* Report residuals. */
f2cpolyfitgjresiduals_ ( &npt, x8, y8, w8, &order, coef, &xoff, &high, &wrms, &wppd );
printf("### eon=%3d  order=%2d  disp=%8.4f  high=%9.4f  wrms=%8.4f  (Pixels)\n",
            eon,order,disp,high/disp,wrms/disp);
*pixwrms = wrms/disp;

return;
}

/* ----------------------------------------------------------------------
  Reject lines from an order line id fit.
  Input: eon       : Echelle order number (0 is first).
         threshold : Threshold in pixels.
 Output: nrej      : Number of points rejected.
*/
void esifit_poly_reject( int eon, double threshold, int *nrej )
{
/**/
int ii;
double disp,xv,yv;
/**/

/* Find Ang/pixel. */
disp = esifit_dispersion();

/* Check each point and reject if necessary. */
*nrej=0;
for (ii=0; ii<ids_num[eon]; ++ii) {
  xv = ids_pix[eon][ii];
  yv = cpolyval(PolyOrder+1,coef,xv-xoff);
  if ((ABS((yv - ids_wav[eon][ii]))/disp) > threshold) { 
    ids_wgt[eon][ii] = 0.;
    ++*nrej;
  }
}

return;
}


/* ----------------------------------------------------------------------
  Read in line IDs.
  Input: idfile    : Line ID file, e.g. "Arc-120.ids"
         clear     : If "1" clear the ID list.
*/
void esifit_read_IDs( char idfile[], int clear )
{
/**/
int eon,ii;
char line[100];
/**/
FILE *infu;
/**/

/* Clear */
if (clear == 1) {
  ids_maxord = 0;
  for (eon=0; eon<maxord; ++eon) {
    ids_num[eon] = 0;
    for (ii=0; ii<maxlpo; ++ii) {
      ids_pix[eon][ii] = 0.;
      ids_wav[eon][ii] = 0.;
      ids_wgt[eon][ii] = 0.;
    }
  }
}

/* Open file. */
infu = fopen(idfile,"r");

/* Read. */
while (fgetline(line,infu) == 1) {

  eon = GLV(line,2) - 1;
  ii  = ids_num[eon];
  if (eon > ids_maxord) ids_maxord = eon;
  ids_pix[eon][ii] = GLV(line,3);
  ids_wav[eon][ii] = GLV(line,4);
  ids_wgt[eon][ii] = GLV(line,6);
  if (ids_wgt[eon][ii] != 1.) {
    printf("Weight not 1: %s\n",line);
  }
  ++ids_num[eon];

}

/* Close file. */
fclose(infu);

/* Check. */
if (ids_maxord < 1) {
  fprintf(stderr,"***ERROR: esifit_read_IDS: No lines found in %s .\n",idfile);
  exit(0);
}

return;
}


/* ----------------------------------------------------------------------
  Plot points and much more...
*/
void esifit_multi_plot_one()
{
/**/
char wrd[100];
int eon,ii,nrej;
double xv,threshold,pixwrms;
double yyy,mmm,bbb,xxA,xxB,yyA,yyB;
float yrange;
/**/

/* Polynomial fit order. */
PolyOrder = 3;

printf("### ................ one ................. PolyOrder=%d \n",PolyOrder);

/* Set. */
nh = 3;
nv = 4;

/* Open PG device. */
printf("### Plotting to device: %s .\n",device);
cpgbeg(0,device,nh,nv);

/* Setup. */
cpgsch(sch);
cpgslw(slw);
cpgsci(1);

/* Plot each order. */
for (eon=0; eon<=ids_maxord; ++eon) {

/* X range. */
  xmin = xmin0 - 100.;
  xmax = xmax0 + 100.;

/* Fit points. */
  esifit_poly_fit( eon, &pixwrms );

/* Reject points. */
  threshold = 3.;
  esifit_poly_reject( eon, threshold, &nrej );

/* Re-fit. */
  if (nrej > 0) {
    printf("###     ......... threshold=%7.2f  nrej=%d\n",threshold,nrej);
    esifit_poly_fit( eon, &pixwrms );
  }

/* Load all points. */
  nn1 = 0;
  for (ii=0; ii<ids_num[eon]; ++ii) {
    xx1[nn1] = ids_pix[eon][ii];
    yy1[nn1] = ids_wav[eon][ii];
    ++nn1;
  }

/* Load fit curve points. */
  nn2 = 0;
  for (xv=xmin; xv<xmax; xv = xv + 10.) {
    xx2[nn2] = xv;
    yy2[nn2] = cpolyval(PolyOrder+1,coef,xv-xoff);
    ++nn2;
  }

/* Zero weight points. */
  nn3=0;
  for (ii=0; ii<ids_num[eon]; ++ii) {
    if (ids_wgt[eon][ii] < 1.e-20) {
      xx3[nn3] = ids_pix[eon][ii];
      yy3[nn3] = ids_wav[eon][ii];
      ++nn3;
    }
  }

/* All new points. */
  if (DeLinear == 1) {
/* End points. */
    xxA =    0.;
    xxB = 4000.;
    yyA = cpolyval(PolyOrder+1,coef,   0.-xoff);
    yyB = cpolyval(PolyOrder+1,coef,4000.-xoff);
    mmm = (yyB - yyA)/(xxB - xxA);
    bbb = yyA;
    for (ii=0; ii<nn1; ++ii) {
      yyy = (mmm * xx1[ii]) + bbb;
      yy1[ii] = yy1[ii] - yyy; 
    }
    for (ii=0; ii<nn2; ++ii) {
      yyy = (mmm * xx2[ii]) + bbb;
      yy2[ii] = yy2[ii] - yyy; 
    }
    for (ii=0; ii<nn3; ++ii) {
      yyy = (mmm * xx3[ii]) + bbb;
      yy3[ii] = yy3[ii] - yyy; 
    }
  }

/* Initial y range. */
  ymin = yy1[0];
  ymax = yy1[0];

/* Y range. */
  for (ii=0; ii<nn1; ++ii) {
    if (yy1[ii] < ymin) ymin = yy1[ii];
    if (yy1[ii] > ymax) ymax = yy1[ii];
  }
  for (ii=0; ii<nn2; ++ii) {
    if (yy2[ii] < ymin) ymin = yy2[ii];
    if (yy2[ii] > ymax) ymax = yy2[ii];
  }
  yrange = ymax - ymin;
  if (yrange < 1.e-20) yrange = 1.e-20;
  ymin = ymin - (yrange*0.1);
  ymax = ymax + (yrange*0.1);

/* New plot window box. */
  cpgpage();
  cpgswin(xmin,xmax,ymin,ymax);
  cpgbox("BCNST",0.,0,"BCNST",0.,0);

/* Plot all points. */
  cpgpt(nn1,xx1,yy1,5);

/* Bad points. */
  cpgsci(2);
  cpgpt(nn3,xx3,yy3,24);
  cpgsci(1);

/* Curve fit points. */
  cpgsci(3);
  cpgline(nn2,xx2,yy2);
  cpgsci(1);

/* Title bar. */
  sprintf(wrd,"Order#%3d",eon);
  cpgmtxt("T",0.3,0.5,0.5,wrd);

/* Add more points based on fit. */
  for (xv=xmin; xv<xmax; xv = xv + 10.) {
    ii = ids_num[eon];
    ids_pix[eon][ii] = xv;
    ids_wav[eon][ii] = cpolyval(PolyOrder+1,coef,xv-xoff);
    ids_wgt[eon][ii] = 0.01;
    ++ids_num[eon];
  }

}

/* Close PG device. */
cpgend();

return;
}


/* ----------------------------------------------------------------------
  Plot points and much more...
*/
void esifit_multi_plot_two()
{
/**/
char wrd[100];
int eon,ii,nrej;
double xv,threshold,disp,pixwrms;
float yrange;
/**/
char card[100];
char line[100];
/**/

/* Polynomial fit order. */
PolyOrder = POtwo;

printf("### ................ two ................. PolyOrder=%d \n",PolyOrder);

/* Set. */
nh = 3; nv = 4;
/*
nh = 1; nv = 1;
*/

/* Open PG device. */
printf("### Plotting to device: %s .\n",device);
cpgbeg(0,device,nh,nv);

/* Setup. */
cpgsch(sch);
cpgslw(slw);
cpgsci(1);

/* Plot each order. */
for (eon=0; eon<=ids_maxord; ++eon) {

/* X range. */
  xmin = xmin0 - 100.;
  xmax = xmax0 + 100.;

/* Fit points. */
  esifit_poly_fit( eon, &pixwrms );

/* Reject points. */
  threshold = 30.;
  esifit_poly_reject( eon, threshold, &nrej );

/* Re-fit. */
  if (nrej > 0) {
    printf("###     ......... threshold=%7.2f  nrej=%d\n",threshold,nrej);
    esifit_poly_fit( eon, &pixwrms );
  }

/* Dispersion. */
  disp = esifit_dispersion();

/* Load all points. */
  nn1 = 0;
  for (ii=0; ii<ids_num[eon]; ++ii) {
    xv       = ids_pix[eon][ii];
    xx1[nn1] = xv;
    yy1[nn1] = ( ids_wav[eon][ii] - cpolyval(PolyOrder+1,coef,xv-xoff) )/disp;
/*
    if (ABS((yy1[nn1])) > 0.3) {
    if ((ids_wgt[eon][ii] > 0.02)||(ids_wgt[eon][ii] < 1.e-10)) {
      printf("eon=%2d: xx=%9.3f  yy=%9.3f  wgt=%9.5f\n",
              eon,xx1[nn1],yy1[nn1],ids_wgt[eon][ii]);
    }
    }
*/
    ++nn1;
  }

/* Load all weight=1 points. */
  nn4 = 0;
  for (ii=0; ii<ids_num[eon]; ++ii) {
    if (ids_wgt[eon][ii] > 0.99) {
      xv       = ids_pix[eon][ii];
      xx4[nn4] = xv;
      yy4[nn4] = ( ids_wav[eon][ii] - cpolyval(PolyOrder+1,coef,xv-xoff) )/disp;
      ++nn4;
    }
  }

/* Zero weight points. */
  nn3=0;
  for (ii=0; ii<ids_num[eon]; ++ii) {
    if (ids_wgt[eon][ii] < 1.e-20) {
      xv       = ids_pix[eon][ii];
      xx3[nn3] = xv;
      yy3[nn3] = ( ids_wav[eon][ii] - cpolyval(PolyOrder+1,coef,xv-xoff) )/disp;
      ++nn3;
    }
  }

/* Initial y range. */
  ymin = yy1[0];
  ymax = yy1[0];

/* Y range. */
  for (ii=0; ii<nn1; ++ii) {
    if (yy1[ii] < ymin) ymin = yy1[ii];
    if (yy1[ii] > ymax) ymax = yy1[ii];
  }
  yrange = ymax - ymin;
  if (yrange < 1.e-20) yrange = 1.e-20;
  ymin = ymin - (yrange*0.1);
  ymax = ymax + (yrange*0.1);

/* Override. */
  ymin = -1.0;
  ymax = +1.0;

/* New plot window box. */
  cpgpage();
  cpgswin(xmin,xmax,ymin,ymax);
  cpgbox("BCNST",0.,0,"BCNST",0.,0);

/* Plot all points. */
  cpgpt(nn1,xx1,yy1,1);

/* Normal points. */
  cpgsci(3);
  cpgpt(nn4,xx4,yy4,5);
  cpgsci(1);

/* Bad points. */
  cpgsci(2);
  cpgpt(nn3,xx3,yy3,24);
  cpgsci(1);

/* Horizontal line. */
  cpgsls(4);
  cpg_HLine(xmin,xmax,0.);
  cpgsls(1);

/* Title bar. */
  sprintf(wrd,"PO=%2d  Order#%3d  WRMS=%7.3f",PolyOrder,eon,pixwrms);
  cpgmtxt("T",0.3,0.5,0.5,wrd);

/* Load cross_wave points. */
  for (ii=0; ii<41; ++ii) {
    xv = ii * 100.;
    cross_wave[eon][ii] = cpolyval(PolyOrder+1,coef,xv-xoff);
  }

/* Save first polynomial coeffecients (max poly order = 7) into FITS header. */
  sprintf(card,"WV_0_%2.2d",eon+1);
  sprintf(line,"%16.9e %16.9e %16.9e %16.9e",coef[0],coef[1],coef[2],coef[3]);
  ccheadset( card, line, header120, headersize );
  ccheadset( card, line, header121, headersize );

  sprintf(card,"WV_4_%2.2d",eon+1);
  sprintf(line,"%16.9e %16.9e %16.9e %16.9e",coef[4],coef[5],coef[6],coef[7]);
  ccheadset( card, line, header120, headersize );
  ccheadset( card, line, header121, headersize );

}

/* Close PG device. */
cpgend();

return;
}



/* ----------------------------------------------------------------------
  Plot cross points and much more...
*/
void esifit_multi_plot_three()
{
/**/
char wrd[100];
int eon,ii,pxpt,npt,forcexoff,order,iok;
float yrange;
double high,wrms,wppd;
/**/

/* Polynomial fit order. */
PolyOrder = POthree;

printf("### ................ three ............... PolyOrder=%d \n",PolyOrder);

/* Set. */
nh = 6;
nv = 7;

/* Open PG device. */
printf("### Plotting to device: %s .\n",device);
cpgbeg(0,device,nh,nv);

/* Setup. */
cpgsch(sch);
cpgslw(slw);
cpgsci(1);

/* Plot each pixel point. */
for (pxpt=0; pxpt<41.; ++pxpt) {

/* Load points. */
  npt=0;
  for (eon=0; eon<ids_maxord; ++eon) {
    x8[npt] = eon;
    y8[npt] = cross_wave[eon][pxpt];
    w8[npt] = 1.;
    ++npt;
  }

/* Fit points. */
  order = PolyOrder;
  xoff  = 0.;
  forcexoff = 0;
  iok   = 0;
  if (glls == 1) {
    f2cpolyfitglls_ ( &npt, x8, y8, w8, &order, coef, &iok );
    xoff = 0.;
  } else {
    f2cpolyfitgj_ ( &npt, x8, y8, w8, &order, coef, &xoff, &forcexoff, &iok );
  }

/* Report residuals. */
  f2cpolyfitgjresiduals_ (&npt,x8,y8,w8, &order, coef, &xoff, &high, &wrms, &wppd );
  printf("### pxpt=%3d  order=%2d  high=%9.4f  wrms=%8.4f (Angstroms)\n",
              pxpt,order,high,wrms);

/* Load data points. */
  nn1 = 0;
  for (eon=0; eon<ids_maxord; ++eon) {
    xx1[nn1] = x8[eon];
    yy1[nn1] = y8[eon];
    ++nn1;
  }

/* Load curve points. */
  nn2 = 0;
  for (eon=0; eon<ids_maxord; ++eon) {
    xx2[nn2] = x8[eon];
    yy2[nn2] = cpolyval(PolyOrder+1,coef,x8[eon]-xoff);
    ++nn2;
  }

/* x range. */
  xmin = -1.;
  xmax = 10.;

/* Initial y range. */
  ymin = yy1[0];
  ymax = yy1[0];

/* Y range. */
  for (ii=0; ii<nn1; ++ii) {
    if (yy1[ii] < ymin) ymin = yy1[ii];
    if (yy1[ii] > ymax) ymax = yy1[ii];
  }
  for (ii=0; ii<nn1; ++ii) {
    if (yy2[ii] < ymin) ymin = yy2[ii];
    if (yy2[ii] > ymax) ymax = yy2[ii];
  }
  yrange = ymax - ymin;
  if (yrange < 1.e-20) yrange = 1.e-20;
  ymin = ymin - (yrange*0.1);
  ymax = ymax + (yrange*0.1);

/* New plot window box. */
  cpgpage();
  cpgswin(xmin,xmax,ymin,ymax);
  cpgbox("BCNST",0.,0,"BCNST",0.,0);

/* Plot data points. */
  cpgpt(nn1,xx1,yy1,5);

/* Plot fit curve. */
  cpgsci(3);
  cpgline(nn2,xx2,yy2);
  cpgsci(1);

/* Title bar. */
  sprintf(wrd,"PO=%2d  pxpt=%4d",PolyOrder,pxpt);
  cpgmtxt("T",0.3,0.5,0.5,wrd);

}

/* Close PG device. */
cpgend();

return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  PGPLOT ploting program for line IDS.
*/
int main(int argc, char *argv[])
{
/**/
char arg[500][100];
char wrd[100];
char idfile[100];
int narg;
/**/   

/* Hard wire */
glls = 1;

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Options. */
sym=-1;
if (cfindarg(arg,&narg,"sym=",':',wrd)==1) { sym = GetLineValue(wrd,1); }
sym = MAX(-1,MIN(99,sym));

sls= 1;
if (cfindarg(arg,&narg,"sls=",':',wrd)==1) { sls = GetLineValue(wrd,1); }
sls = MAX(0,MIN(4,sls));

slw= 1;
if (cfindarg(arg,&narg,"slw=",':',wrd)==1) { slw = GetLineValue(wrd,1); }
slw = MAX(1,MIN(99,slw));

sch= 3.0;
if (cfindarg(arg,&narg,"sch=",':',wrd)==1) { sch = GetLineValue(wrd,1); }
sch = MAX(0.1,MIN(99.,sch));

POtwo = 6;
if (cfindarg(arg,&narg,"po2=",':',wrd)==1) { POtwo = GetLineValue(wrd,1); }

POthree = 2;
if (cfindarg(arg,&narg,"po3=",':',wrd)==1) { POthree = GetLineValue(wrd,1); }

strcpy(psfile,"none");
if (cfindarg(arg,&narg,"ps=",':',wrd)==1) { strcpy(psfile,wrd); }

/*
svp= 0.1;
if (cfindarg(arg,&narg,"svp=",':',wrd)==1) { svp = GetLineValue(wrd,1); }
svp = MAX(0.001,MIN(0.999,svp));
nh = 1;
if (cfindarg(arg,&narg,"nh=",':',wrd)==1) { nh = GetLineValue(wrd,1); }
nh = MAX(1,nh);
nv = 1;
if (cfindarg(arg,&narg,"nv=",':',wrd)==1) { nv = GetLineValue(wrd,1); }
nv = MAX(1,nv);
binfac = 1;
if (cfindarg(arg,&narg,"bin=",':',wrd)==1) { binfac = GetLineValue(wrd,1); }
binfac = MAX(1,binfac);
hist=0;
if (cfindarg(arg,&narg,"-hist", 's',wrd)==1) { hist = 1; }
dots=0;
if (cfindarg(arg,&narg,"-dots", 's',wrd)==1) { dots = 1; }
fixzero = 0;
if (cfindarg(arg,&narg,"-fixzero", 's',wrd)==1) { fixzero = 1; }
verbose = 0;
if (cfindarg(arg,&narg,"-verbose",'s',wrd)==1) { verbose = 1; }
if (cfindarg(arg,&narg,"-v"      ,'s',wrd)==1) { verbose = 1; }
*/


/* Syntax. */
if (narg < 2) {
  printf("Syntax: esifit (line ID file) (2nd line ID file)\n");
  printf("\n");  
  printf("Options:\n");
  printf("   list=  : Filename of file with list of data files.\n");
  printf("    sym=  : Symbol for points (default is -1 (points)).\n");
  printf("    sch=  : Set character height (default is 1.8).\n");
  printf("    slw=  : Set line width (default is 1).\n");
  printf("    sls=  : Set line style (0=don't connect) (default is 1).\n");
  printf("    ps=   : PS file.\n");
/*
  printf("    svp=  : Set viewport fraction (def:0.1 =.1,.9,.1,.9)\n");
  printf("    ps=   : PostScript filename (default is 'xdisp').\n");
  printf("    xmin= : Set x minimum on plot.\n");
  printf("    xmax= : Set x maximum on plot.\n");
  printf("    ymin= : Set y minimum on plot.\n");
  printf("    ymax= : Set y maximum on plot.\n");
  printf("    bin=  : Apply this binning factor before plotting.\n");
  printf("    nh=   : Number of plots on a page horizontally.\n");
  printf("    nv=   : Number of plots on a page vertically.\n");
  printf(" (note: if nh*nv > 1, then each file will be plotted separately.)\n");
  printf("  -dots   : Plot overlapping spectra in dots/dashes, NOT colors.\n");
  printf("  -quit   : Quit, with no interactive mode.\n");
  printf("  -nozero : Do not show zero line on plots.\n");
  printf("  -hist   : Histogram drawing mode (default: connect dots).\n");
  printf("  -fixzero: Fix y-minimum at zero.\n");
  printf("  -match  : Match comparison files to primary by median scaling.\n");
  printf("  -verbose: More messages.\n");
*/
  printf("\n");  
  exit(0);
}

/* Read FITS files. */
printf("### Reading fits files Arc-120.fits and Arc-121.fits ...\n");
creadfits( "Arc-120.fits", aa120, maxpix, header120, headersize);
creadfits( "Arc-121.fits", aa121, maxpix, header121, headersize);

printf("### glls=%d\n",glls);

/* Set file. */
strcpy(idfile,arg[1]);

/* Read file. */
esifit_read_IDs( idfile, 1 );

/* Set file. */
strcpy(idfile,arg[2]);

/* Read file. */
esifit_read_IDs( idfile, 0 );

/* Show points after subtracting a straight line through fit endpoints. */
DeLinear = 1;
 
/* Plot points. */
strcpy(device,"/XDISP");
strcpy(device,"j_one.ps/CPS");
esifit_multi_plot_one();

/* Plot points and fit and write header cards... */
if (strcmp(psfile,"none") == 0) {
  strcpy(device,"/XDISP");
} else  {
  strcpy(device,psfile);
  strcat(device,"/CPS");
}
strcpy(device,"j_two.ps/CPS");
esifit_multi_plot_two();

/* Plot cross_wave... */
printf("### skipping step 3...\n");
/*
esifit_multi_plot_three();
*/

/* Write back FITS files. */
printf("### Writing fits files Arc-120.fits and Arc-121.fits ...\n");
cwritefits( "Arc-120.fits", aa120, header120, headersize);
cwritefits( "Arc-121.fits", aa121, header121, headersize);

return(0);
}

