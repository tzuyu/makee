C This file is scratch.inc
C
C:::::::::::::::::::::::::
C Scratch variables.
C
      integer*4 mpix
      parameter(mpix=9000)
      real*8 x8(mpix),y8(mpix),w8(mpix),sw8(mpix),coef(0:99)
      real*8 x8b(mpix),y8b(mpix),w8b(mpix),coefb(0:99)
      real*4 arr(mpix),b(mpix),bx(mpix),z(mpix),zpk(mpix)
C
      common / SCRATCHBLK / x8,y8,w8,sw8,coef,x8b,y8b,w8b,coefb,
     .                      arr,b,bx,z,zpk
C
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C   mpix  =  Maximum number of points in fits, normally we should need no more
C            than the number of columns in an image.
C   x8,y8,w8,sw8 = scratch arrays, usually used for fitting polynomials.
C   x8b,y8b,w8b  = scratch arrays, usually used for fitting polynomials.
C   coef,coefb   = coeffecients for polynomials
C   arr          = scratch array, usually used for sorting.
C   b,bx,z,zpk   = more scratch arrays.
C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C
