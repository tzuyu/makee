C This file is global.inc

C:::::::::::::::::::::::::
C Global variables for makee routines.
C
      real*4 Global_trace_rms(100) ! trace RMS values.
      real*4 Global_eperdn       ! electrons per digital number
      real*4 Global_rov          ! readout variance in counts
      real*4 Global_exposure     ! exposure time in seconds
      real*4 Global_sw           ! current estimate of slit width
      real*4 Global_sw_flat      ! current estimate of slit width on flat
      real*4 Global_sw_adj       ! adjustment of slit (object vs. flat)
      real*4 Global_CR_Frac      ! Cosmic Ray Fraction for current order
      real*4 Global_psemra       ! Adjustable masking fractor(current ord)
      integer*4 Global_obsnum    ! observation number of object
      integer*4 Global_eon       ! current echelle order number
      integer*4 Global_neo       ! current number of echelle orders
      integer*4 Global_xbin      ! current x binning factor (columns)
      integer*4 Global_ybin      ! current y binning factor (rows)
      integer*4 Global_ccdloc    ! HIRES2 CCDLOC number (1,2,3)
      integer*4 Global_sky_rej   ! Sky pixels masked for current order
      integer*4 Global_obj_rej   ! Object pixels masked for current order
      logical Global_HIRES       ! Is this HIRES?
      logical Global_ESI         ! Is this ESI?
      logical Global_HIRES2      ! Is this HIRES2 (2004)?
C
      common / GLOBALBLK /
     .            Global_trace_rms,
     .            Global_eperdn, Global_rov, Global_exposure, 
     .            Global_sw, Global_sw_flat, Global_sw_adj,
     .            Global_CR_Frac, Global_psemra,
     .            Global_obsnum, Global_eon, Global_neo,
     .            Global_xbin, Global_ybin, Global_ccdloc,
     .            Global_sky_rej, Global_obj_rej,
     .            Global_HIRES, Global_ESI, Global_HIRES2

