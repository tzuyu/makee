C logsum.f

      implicit none
      character*80 arg(99),line,line2,object,star,flat
      character*80 objname,wrd,mode,logfile
      integer*4 narg,lc,eon,primary,adjacent,additional,high,low,ii
      integer*4 minexpos
      real*4 expfrac,actfrac,valread
      real*8 fgetlinevalue
      logical findarg,format94,format98,isobj

C Get parameters.
      call arguments(arg,narg,99)
      format94 = findarg(arg,narg,'-94',':',wrd,ii)
      format98 = findarg(arg,narg,'-98',':',wrd,ii)
      minexpos = 180
      if (findarg(arg,narg,'minexpos=',':',wrd,ii)) then
        minexpos=nint(valread(wrd))
      endif
C
      if (narg.ne.1) then
        print *,
     .  'Syntax: logsum ( "all" or (file with list of log filenames) )'
        print *,'                      [minexpos= ]   [-94] [-98]  '
        print *,'  '
        print *,'   Summarizes output from makee log files.'
        print *,'  '
        print *,'   minexpos= : Minimum exposure time for inclusion.'
        print *,'     -98 : Use old (1998) format for log files.'
        print *,'     -94 : Use old (1994) format of output.'
        call exit(0)
      endif

C New program.
      if ((.not.format98).and.(.not.format94)) then
        if (arg(1)(1:3).eq.'all') then
          call mk_logsum_list(minexpos,' ')
        else
          call mk_logsum_list(minexpos,arg(1))
        endif
        stop
      endif

CCC
C OLD PROGRAM BELOW:
CCC

C Go through individual log files.
      open(33,file=arg(1),status='old')

111   read(33,'(a)',end=555) logfile

C Initialize.
      eon       =-1
      primary   =-1
      adjacent  =-1
      additional=-1
      high      =-1
      low       =-1
      expfrac   = 0. 
      actfrac   = 0. 

C Open original log file.
      open(1,file=logfile,status='old')
      objname=' '
      isobj=.false.

C Echo out files used.
1     continue
      read(1,'(a)',end=5,err=9) line
      line2 = line
      call lower_case(line2)
      if ((index(line2,'warning').gt.0).or.
     .                  (index(line2,'error').gt.0)) then
        print '(a)',line(1:lc(line))
      endif
      if (index(line,'Object file').gt.0) object=line 
      if (index(line,'Star file').gt.0) star=line 
      if (index(line,'Flat file').gt.0) flat=line 
      if (index(line,'Mode').gt.0) mode=line 
      if ((index(line,'XDANGL').gt.0).and.(objname.eq.' ')) then
        objname=line(37:76)
      endif
      if (index(line,'Writing').gt.0) goto 2
      goto 1 
2     continue
C Divider.
      print '(4a)','   ::::::::::: ',logfile(1:lc(logfile)),
     .             ' ::::::::::: ',objname(1:lc(objname))
      object= object((index(object,'=')+2):)
      star  = star((index(star,'=')+2):)
      flat  = flat((index(flat,'=')+2):)
      mode  = mode((index(mode,'=')+3):)
      print '(2a,2x,a,2x,a)','Object,star,flat: ',object(1:lc(object)),
     .                       star(1:lc(star)),flat(1:lc(flat))
      read(1,'(a)',end=5,err=9) line
      line2 = line
      call lower_case(line2)
3     continue
      if ((index(line2,'warning').gt.0).or.
     .                   (index(line2,'error').gt.0)) then
        print '(a)',line(1:lc(line))
      endif
C Summarize an order.
      if (index(line,'****************').gt.0) then
        eon = nint( fgetlinevalue(line(60:65),1) )
        read(1,'(a)',end=5,err=9) line
        line2 = line
        call lower_case(line2)
        do while(index(line,'**************').eq.0)
          if ((index(line2,'warning').gt.0).or.
     .                        (index(line2,'error').gt.0)) then
            print '(a)',line(1:lc(line))
          elseif (mode(1:1).ne.'4') then
            if (line(1:7).eq.'Expecte') then
              expfrac = fgetlinevalue(line(27:35),1)
            elseif (line(1:7).eq.'  Actua') then
              actfrac = fgetlinevalue(line(27:35),1)
            elseif (index(line,'primary').gt.0) then
              primary = nint( fgetlinevalue(line(44:49),1) )
            elseif (index(line,'adjacent').gt.0) then
              adjacent = nint( fgetlinevalue(line(44:49),1) )
            elseif (index(line,'additional').gt.0) then
              additional = nint( fgetlinevalue(line(44:49),1) )
            elseif (index(line,'high pix').gt.0) then
              high = nint( fgetlinevalue(line(44:49),1) )
            elseif (index(line,'low pix').gt.0) then
              low = nint( fgetlinevalue(line(44:49),1) )
            endif
          endif
          read(1,'(a)',end=5,err=9) line
          line2 = line
          call lower_case(line2)
        enddo
        if (mode(1:1).ne.'4') then
          if (eon.eq.1) then
             if (format94) then
             print '(a)',
     .  ' Ord#   ExpFrac ActFrac  Prim   Adj   Add  High   Low'
             else
             print '(a)',
     .  ' Ord#   ActFrac/ExpFrac  Prim   Adj   Add  High   Low'
             endif
C                         1234 : 12345678----+---123456----+-123456----+-123456
C                         1234 : 12345678901xxxxx123456----+-123456----+-123456
          endif
          if (format94) then
            print '(i4,a,2f8.4,5(i6))
     .         ',eon,' : ',expfrac,actfrac,primary,
     .         adjacent,additional,high,low
          else
            print '(i4,a,f11.3,5x,5(i6))',eon,' : ',
     .         actfrac/max(0.0001,expfrac),primary,
     .         adjacent,additional,high,low
          endif
          isobj=.true.
        endif
      else
        read(1,'(a)',end=5,err=9) line
        line2 = line
        call lower_case(line2)
      endif
      goto 3
5     continue
      if (isobj) then
      if (format94) then
        print '(i4,a,2f8.4,5(i6))',eon,' : ',expfrac,actfrac,primary,
     .     adjacent,additional,high,low
      else
        print '(i4,a,f11.3,5x,5(i6))',eon,' : ',
     .     actfrac/max(0.0001,expfrac),primary,
     .     adjacent,additional,high,low
      endif
      endif
      goto 500
9     continue
      print *,'Error reading log file: ',logfile(1:lc(logfile))
500   continue
      close(1)
      goto 111

555   continue
      close(33)
      stop
      end

