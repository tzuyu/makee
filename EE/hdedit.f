C ------------------------------------------------------------------------------
C Edit FITS header.
C
      implicit none
C
      include 'maxpix.inc'
C
      real*4 a(maxpix)
C
      integer*4 i,narg,lc,num,k,ios
      real*4 valread
      real*8 r8
      integer*4 headersize
      parameter(headersize=115200)
      character header*115200
      character*80 arg(9999),card,file,wrd,wrds(3),type,name,value,tf
      logical ok,view,findarg,novi,modified,logic,have_card,nuc

      call arguments(arg,narg,9999)
      view = findarg(arg,narg,'-view',':',wrd,i)
      novi = findarg(arg,narg,'-novi',':',wrd,i)
      nuc  = findarg(arg,narg,'-nuc',':',wrd,i)
      have_card = findarg(arg,narg,'card=','~',wrds,i)
      if (have_card) then
        if (i.ne.3) then
          print *,
     . 'ERROR: hdedit: You must have type,name, and value in card='
          call exit(1)
        endif
        type = wrds(1)
        call upper_case(type)
        name = wrds(2)
        if (.not.nuc) call upper_case(name)
        value= wrds(3)
        if ((lc(name).lt.1).or.(lc(name).gt.8)) then
          print *,
     .  'ERROR: hdedit: Name must have between 1 and 8 characters.'
          call exit(1)
        endif
      endif
      if (narg.eq.0) then
        print *,'Syntax:  hdedit  (FITS file(s))  [-view] [-novi]'
        print *,'  ..         [card=type~name~value [-nuc]]'
        print *,
     .  '    Read FITS file, edit header (execute vi on a temporary'
        print *,
     .  '    file /tmp/hdedit.temp), write FITS file.  The header is'
        print *,'    "cleaned up" before writing it back out.'
        print *,' -view : view only do not write back out.'
        print *,' -novi : use an editor other than vi.'
        print *,
     .  ' card= : Change or add header card; type = I, F, L, C, or D'
        print *,
     .  '         for integer, real, logical, character, and delete.'
        print *,
     .  '         If card= is given, the editor is not executed.'
        print *,
     .  ' -nuc  : No upper case, allows card name to have lower case.'
        call exit(0)
      endif

C Change card only.
      if (have_card) then
      DO num=1,narg

      file = arg(num)
      call AddFitsExt(file)
C Read FITS file.
      if (view) then
        print '(2a)',' Reading header from: ',file(1:lc(file))
        call readfitsheader(file,header,ok)
      else
        print '(2a)',' Reading: ',file(1:lc(file))
        call readfits(file,a,maxpix,header,ok)
      endif
      if (.not.ok) goto 801
C Clean up header.
      call nice_header(header)
C Add/change card.
      if (type(1:1).eq.'D') then
        call unfit(name,header)
      elseif (type(1:1).eq.'I') then
        i = nint(valread(value))
        call inheadset(name,i,header)
      elseif (type(1:1).eq.'F') then
        r8 = dble(valread(value))
        call fheadset(name,r8,header)
      elseif (type(1:1).eq.'L') then
        logic = ((index(value,'T').gt.0).or.(index(value,'t').gt.0))
        call lheadset(name,logic,header)
      elseif (type(1:1).eq.'C') then
        if (value(1:1).eq.char(39)) value = value(2:)
        k = lc(value)
        if (value(k:k).eq.char(39)) value(k:k) = ' '
        call cheadset(name,value,header)
      else
        print '(2a)',' Unknown type = ',type(1:lc(type))
      endif
C Clean up header.
      call nice_header(header)
C Write out FITS file.      
      if (.not.view) then
        print '(2a)','  Over-writing file : ',file(1:lc(file))
        call writefits(file,a,header,ok)
        if (.not.ok) goto 804
      else
        print *,'View only-- not re-writing file.'
      endif

      ENDDO
      goto 900
      endif

      DO num=1,narg

      file = arg(num)
      call AddFitsExt(file)
C Read FITS file.
      if (view) then
        print '(2a)',' Reading header from: ',file(1:lc(file))
        call readfitsheader(file,header,ok)
      else
        print '(2a)',' Reading: ',file(1:lc(file))
        call readfits(file,a,maxpix,header,ok)
      endif
      if (.not.ok) goto 801
C Clean up header.
      call nice_header(header)
C Write header cards to a temporary file.
      i=0
      tf='/tmp/hdedit.temp'
4     continue
      open(1,file=tf,status='new',iostat=ios)
      if (ios.ne.0) then
        if (i.eq.9) then
          print *,
     . 'ERROR creating temp file: "hdedit.temp" or "/tmp/hdedit.temp".'
          call exit(1)
        endif
        tf='hdedit.temp'
        i=9
        goto 4
      endif
      i=1
      do while((i.lt.headersize-81).and.(header(i:i+3).ne.'END '))
        card = header(i:i+79)
        write(1,'(a)',err=802) card(1:lc(card))
        i=i+80
      enddo
      if (i.lt.headersize-81) then
        card = header(i:i+79)
        write(1,'(a)',err=802) card(1:lc(card))
      else
        goto 805
      endif
      close(1)
C Edit header.
      if (novi) then
        print '(3a)',
     .  'Edit the file ',tf(1:lc(tf)),' with your favorite editor.' 
        print '(a,$)',' Hit return when done.'
        read(5,'(a)') wrd
      else
        call system('vi '//tf(1:lc(tf)))
      endif
C Has header been modified?
      modified=.false.
      card = ' '
      open(1,file=tf,status='old')
      i=1
      do while((i.lt.headersize-81).and.(card(1:4).ne.'END '))
        card = ' '
        read(1,'(a)',err=803,end=55) card
        if (card.ne.header(i:i+79)) modified=.true.
        i=i+80
      enddo
55    close(1)
      if (i.ge.headersize-81) goto 806
C Modified?
      if (.not.modified) then
        if (.not.view) print *,
     .  'No change to header-- not re-writing file.'
        open(1,file=tf,status='old')
        close(1,status='delete')
        goto 700
      endif
C Clear header and read header cards back from temporary file. 
      header = ' '
      card = ' '
      open(1,file=tf,status='old')
      i=1
      do while((i.lt.headersize-81).and.(card(1:4).ne.'END '))
        card = ' '
        read(1,'(a)',err=803,end=5) card
        header(i:i+79) = card
        i=i+80
      enddo
5     close(1,status='delete')
      if (i.ge.headersize-81) goto 806
C Clean up header.
      call nice_header(header)
C Write out FITS file.      
      if (.not.view) then
        print '(2a)','  Over-writing file : ',file(1:lc(file))
        call writefits(file,a,header,ok)
        if (.not.ok) goto 804
      else
        print *,'View only-- not re-writing file.'
      endif
700   continue

      ENDDO

      goto 900
801   print *,'ERROR: hdedit: Reading FITS image.'
      goto 900
802   print *,
     .  'ERROR: hdedit: Writing temporary header file: ',tf(1:lc(tf))
      goto 900
803   print *,
     .  'ERROR: hdedit: Reading temporary header file: ',tf(1:lc(tf))
      goto 900
804   print *,
     .  'ERROR: hdedit: Writing FITS image.'
      goto 900
805   print *,
     .  'ERROR: hdedit: Header is too big for this program.'
      goto 900
806   print *,
     .  'ERROR: hdedit: Could not find END card in new header.'
900   continue
      call system('/bin/rm -f '//tf(1:lc(tf)))

      stop
      end

