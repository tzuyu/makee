C combine1xy.f                                       tab  may95 mar97 jun97
C
C Combine 1 D spectra by rebinning.  Input spectrum is an x-y pair file.
C
      implicit none
      integer*4 sc,ec,narg,i,lc,access,nim,naxis1,npix
      character*115200 newhead
      character*80 arg(99),outfile,wrd,wrds(9),file
      character*80 chead,newobject,object,dfile(99),efile(99)
      character*600 combine
      integer*4 mx
      parameter(mx=99000)
      real*4 ff(mx),fe(mx),a(mx),ae(mx),fv,ev,wt
      real*8 bluelimit,redlimit,bigpix1,bigpix2,x8(mx),getpix
      real*8 valread8,wave,crval1,cdelt1,waveblue,wavered
      real*8 pix1,pix2,NewWave,nwp,hi_nwp,blwv,rdwv
      logical ok,findarg,nozero,lgi,patch

C$$$$$$$$$$$
      real*8 zero,disp,aa,bb
      logical loglin,nwinit
      common /NWB/ zero,disp,aa,bb,loglin,nwinit
C$$$$$$$$$$$

C Get command line parameters and set defaults.
      call arguments(arg,narg,99)
      combine=' '
      do i=1,narg
        combine = combine(1:lc(combine))//' '//arg(i)
      enddo
      nozero=.true.
      lgi    = findarg(arg,narg,'-lgi',':',wrd,i)
      loglin = findarg(arg,narg,'-loglin',':',wrd,i)
      patch  = findarg(arg,narg,'-patch',':',wrd,i)
      zero=1000.d0
      if (findarg(arg,narg,'zero=',':',wrd,i)) zero=valread8(wrd)
      disp=0.d0
      if (findarg(arg,narg,'disp=',':',wrd,i)) disp=valread8(wrd)
      outfile=' '
      if (findarg(arg,narg,'of=',':',wrd,i)) outfile=wrd
      call AddFitsExt(outfile)
      bluelimit= 0.d0
      redlimit = 0.d0
      if (findarg(arg,narg,'wr=',':',wrds,i)) then
        if (i.eq.2) then
          bluelimit= valread8(wrds(1))
          redlimit = valread8(wrds(2))
        endif
        if ((i.ne.2).or.(bluelimit.lt.1000.).or.
     .                       (redlimit.lt.1000.)) then
          print *,'Error with wr= option.'
          call exit(1)
        endif
      endif

C If zero parameters or improper parameters, tell user syntax. 
      if (narg.eq.0) then
        print *,
     .  'Syntax: combine1xy (ASCII x,y pair file) [file 2] [file 3] ...'
        print *,
     .  '                of=(output filename)  [wr=bluewave:redwave]'
        print *,'                [-loglin]  (disp=d)  [zero=w]  [-lgi]'
        print *,'                [-patch]'
        print *,' '
        print *,'  Combines 1 D spectra by rebinning and averaging.'
        print *,'  The error file filenames should be in the following'
        print *,'  acceptable format:  *.xy,*e.xy .' 
        print *,'Options:'
        print *,
     .  ' -loglin : use a log-linear relation. In this case, disp= is'
        print *,
     .  '           equal to the velocity width of each bin in km/s .'
        print *,
     .  ' disp=d  : align such that the Angstroms per pixel is "d".'
        print *,
     .  '           (Default is the average pixel width for each row.)'
        print *,
     .  ' zero=w  : specifies that the spectra should be aligned such'
        print *,
     .  '           the wavelength "w" would be at the center of a bin.'
        print *,
     .  '           (Default value is 1000.)'
        print *,
     .  ' -lgi    : Do lagrangian (area polynomial) interpolation (not'
        print *,
     .  '           recommended.)'
        print *,
     .  ' -patch  : Interpolate over zero valued (masked) points.'
        print *,' '
        call exit(0)
      endif

C Check dispersion.
      if (disp.lt.1.e-10) then
        print *,'ERROR- you must specify disp= .'
        call exit(1)
      endif

C Echo to user.
      print '(a,f10.4,f10.6)',
     .  ' Zero wavelength and dispersion= ',zero,disp
      if (loglin) print *,'Dispersion is in kilometers per second.'
      if (bluelimit.gt.0.) print '(a,2f12.4)',
     .  ' User specified wavelength limits : ',bluelimit,redlimit

C Load filenames.
      nim=0
      do while(narg.gt.nim)
        nim=nim+1
        dfile(nim) = arg(nim)
C Look for error file.
        efile(nim)=' '
        i = index(dfile(nim),'.xy')
        if ((efile(nim).eq.' ').and.(i.gt.0)) then
          file = dfile(nim)(1:i-1)//'e.xy'
          if (access(file,'r').eq.0) efile(nim)=file
        endif
        if (efile(nim).eq.' ') then
          print '(2a)',' Could not find error file for ',
     .                   dfile(nim)(1:lc(dfile(nim)))
          call exit(1)
        endif
      enddo
      print *,'Number of 1 D spectra=',nim
C
C In the final arrays the center of the first element is given by "zero".
C For a linear scale    :  wavelength = zero + disp*(pixel-1)
C For a log-linear scale:  wavelength = 10**( azero + bdisp*(pixel-1) )
C where azero=log10(zero) and bdisp=( ( disp in km/s )  / ( c * ln(10) ) ).
C
C Initialize.
      nwinit=.false.
      wave = NewWave(0.d0)
C Zero final arrays.
      do i=1,mx
        ff(i)=0.
        fe(i)=0.
      enddo

C Go through each spectrum and build up final arrays ff() and fe().
      hi_nwp=0.d0
      DO i=1,nim

C Read in the flux and error files.
        print '(2a)',' Reading : ',dfile(i)(1:lc(dfile(i)))
        call readxy(dfile(i),x8,a,npix)
        sc = 1
        ec = npix
        print '(2a)',' Reading : ',efile(i)(1:lc(efile(i)))
        call readxy(efile(i),x8,ae,npix)
        if (npix.ne.ec) then
          print *,
     .  'Error- number of pixels in error file does not match data.'
          call exit(1)
        endif
C Set wavelength limits in x,y,pair file.
        blwv = x8(2)
        rdwv = x8(npix-1)

C Go through each pixel in new array.
        nwp = 1.d0
        wave= NewWave(nwp)
        do while(wave.lt.rdwv) 
          if (wave.gt.blwv) then
            if ((wave.gt.blwv).and.(wave.lt.rdwv)) then
C Pixel boundaries.
              waveblue= NewWave(nwp-0.5d0)
              wavered = NewWave(nwp+0.5d0)
C Get pixel positions in x,y pair files.
              pix1    = getpix(npix,x8,waveblue)
              pix2    = getpix(npix,x8,wavered)
              if ((nint(pix1).ge.sc).and.(nint(pix2).le.ec)) then
          call linear_fluxave(a,sc,ec,1,1,1,pix1,pix2,fv,nozero,lgi)
          call linear_errave(ae,sc,ec,1,1,1,pix1,pix2,ev,nozero,lgi)
                if (ev.gt.0.) then
                  wt = 1.0 / (ev*ev)
                  ff(nint(nwp)) = ff(nint(nwp)) + wt*fv
                  fe(nint(nwp)) = fe(nint(nwp)) + wt
                endif
              endif
            endif
          endif
          nwp = nwp + 1.d0
          wave= NewWave(nwp)
        enddo
        if (nwp.gt.hi_nwp) hi_nwp=nwp
      ENDDO

C Divide by weight sum.
      do i=1,nint(hi_nwp)
        if ((fe(i).gt.0.).and.(abs(ff(i)).gt.0.)) then
          ff(i) = ff(i) / fe(i)
          fe(i) = sqrt( 1. / fe(i) )
        else
          ff(i) = 0.
          fe(i) =-1.
        endif
      enddo

C Starting and ending pixel of new big spectrum.
      if (bluelimit.gt.0.) then
        nwp=1.d0
        wave = NewWave(nwp)
        do while(wave.lt.bluelimit)
          nwp=nwp+1.d0
          wave = NewWave(nwp)
        enddo
        bigpix1 = nwp
        nwp=1.d0
        wave = NewWave(nwp)
        do while(wave.lt.redlimit)
          nwp=nwp+1.d0
          wave = NewWave(nwp)
        enddo
        bigpix2 = nwp-1.d0
      else
        nwp=1.d0
        do while((abs(ff(nint(nwp))).lt.1.e-30).and.(nint(nwp).lt.mx))
          nwp=nwp+1.d0
        enddo
        bigpix1 = nwp
        nwp=dfloat(mx)
        do while((abs(ff(nint(nwp))).lt.1.e-30).and.(nwp.gt.1.d0))
          nwp=nwp-1.d0
        enddo
        bigpix2 = nwp
      endif
      if (bigpix2.lt.bigpix1) then
        print *,'bigpix1=',bigpix1
        print *,'bigpix2=',bigpix2
        print *,'Error combining spectra. bigpix2.lt.bigpix1.'
        call exit(1)
      endif
C Set header card values.
      if (loglin) then
        crval1 = aa + (bb*(bigpix1-1.d0))
        cdelt1 = bb
        naxis1 = 1 + nint(bigpix2-bigpix1)
      else
        crval1 = NewWave(bigpix1)
        cdelt1 = disp
        naxis1 = 1 + nint(bigpix2-bigpix1)
      endif
C Make new header.
      call make_basic_header(newhead,sc,npix,1,1)
C Add new cards to new header.
      call cheadset('OBJECT',outfile,newhead)
      call fheadset('CRVAL1',crval1,newhead)
      call fheadset('CDELT1',cdelt1,newhead)
      call cheadset('CTYPE1','LAMBDA',newhead)
      if (loglin) call inheadset('DC-FLAG',1,newhead)
      call inheadset('NAXIS1',naxis1,newhead)
      call inheadset('CRPIX1',1,newhead)
      call inheadset('NAXIS',1,newhead)
      call unfit('NAXIS2',newhead)
      call unfit('CRVAL2',newhead)
      call unfit('CDELT2',newhead)
      call unfit('CRPIX2',newhead)
      call cheadset('FILENAME',outfile,newhead)
C Add command line to header.
      i = lc(combine)
      wrd = combine(001:060)
      call cheadset('COMBINE1',wrd,newhead)
      if (i.gt.60) then
        wrd = combine(061:120)
        call cheadset('COMBINE2',wrd,newhead)
      endif
      if (i.gt.120) then
        wrd = combine(121:180)
        call cheadset('COMBINE3',wrd,newhead)
      endif
      if (i.gt.180) then
        wrd = combine(181:240)
        call cheadset('COMBINE4',wrd,newhead)
      endif
      if (i.gt.240) then
        wrd = combine(241:300)
        call cheadset('COMBINE5',wrd,newhead)
      endif
 
      if (loglin) then
        call inheadset('WCSDIM',1,newhead)
        call cheadset('CTYPE1','LINEAR',newhead)
        call fheadset('CD1_1',cdelt1,newhead)
        call inheadset('LTM1_1',1,newhead)
        call cheadset('WAT0_001','system=equispec',newhead)
C Put in blank spaces for IRAF.
        i = index(newhead,'=equispec')
        newhead(i+9:i+9)=' '
        newhead(i+62:i+62)=char(39)
        call cheadset('WAT1_001',
     .       'wtype=linear label=Wavelength units=Angstroms',newhead)
C Put in blank spaces for IRAF.
        i = index(newhead,'units=Angstroms')
        newhead(i+15:i+15)=' '
        newhead(i+38:i+38)=char(39)
        call inheadset('DC-FLAG',1,newhead)
        call cheadset('APNUM1','1 1 1.0 1.0',newhead)
      endif
C Patch up zero pixel spots if requested.
      if (patch) call interp_zero_pts(1,nint(nwp),ff)
      do i=1,nint(nwp)
        if (fe(i).lt.1.e-31) fe(i)=-1.0
      enddo

C Write out flux spectrum.
      print '(2a)',' Writing flux : ',outfile(1:lc(outfile))
      call writefits(outfile,ff(nint(bigpix1)),newhead,ok)
      if (.not.ok) goto 860
C Write out error file.
      object = chead('OBJECT',newhead)
C New object name
      write(newobject,'(2a)') object(1:lc(object)),' (Error)'
      call cheadset('OBJECT',newobject,newhead)
C Form new filename.
      i=index(outfile,'.fits')
      if (i.eq.0) goto 850
      write(outfile,'(2a)') outfile(1:i-1),'e.fits'
      call cheadset('FILENAME',outfile,newhead)
C Write out error.
      print '(2a)',' Writing error: ',outfile(1:lc(outfile))
      call writefits(outfile,fe(nint(bigpix1)),newhead,ok)
      if (.not.ok) goto 860

      print *,'All done.'
      stop
800   continue
      print *,'Error reading WV header card.'
      stop
850   print *,'Error- filename must have .fits extension.'
      stop
860   print *,'Error reading or writing FITS file.'
      stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
C Find flux (per unit original bin) between the pixel values pix1 and pix2.
C
      subroutine linear_fluxave(a,sc,ec,sr,er,row,pix1,pix2,
     .  bval,nozero,lgi)
C
      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er),bval
      real*8 pix1,pix2
      logical nozero,lgi,more
      real*8 Y0,Ym,Yp,x1,x3,sum,wsum,area,b0,b1
      integer*4 i,i0,i1,i2

C Check for zero pixels.
      if (nozero) then
        more = .true.
        do i=nint(pix1),nint(pix2)
          if (abs(a(i,row)).lt.1.e-30) more = .false.
        enddo
        if (.not.more) then
          bval = 0.
          return
        endif
      endif

C Sum and weight.
      sum = 0.d0
      wsum= 0.d0
C Whole pixels.
      i1 = nint(pix1) + 1
      i2 = nint(pix2) - 1
      if (i2.ge.i1) then
        do i=i1,i2
          sum = sum + dble(a(i,row))
          wsum= wsum+ 1.d0
        enddo
      endif

      IF (lgi) THEN

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))
      Ym = dble(a(max(sc,i0-1),row))
      Yp = dble(a(min(ec,i0+1),row))
      x1 = dfloat(i0) - 1.d0
      x3 = dfloat(i0) + 1.d0
      call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
      sum = sum + area
      wsum= wsum+ (b1-b0)
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))
        Ym = dble(a(max(sc,i0-1),row))
        Yp = dble(a(min(ec,i0+1),row))
        x1 = dfloat(i0) - 1.d0
        x3 = dfloat(i0) + 1.d0
        call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
        sum = sum + area
        wsum= wsum+ (b1-b0)
      endif

      ELSE

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))
      sum = sum + (b1-b0)*Y0
      wsum= wsum+ (b1-b0)
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))
        sum = sum + (b1-b0)*Y0
        wsum= wsum+ (b1-b0)
      endif

      ENDIF

C Average flux per unit original bin.
      bval = sngl( sum / wsum )

      return
      end


C----------------------------------------------------------------------
C Find flux error (per unit original bin) between the pixel values pix1 and
C pix2.  The variance is used in calculating the new error.
C
C The simple derivation of the error on  F = sum( Wi Ai ) / sum( Ai ) would be
C   var(F) = sum( Wi * Wi * var(Ai) ) / [ sum( Wi ) * sum( Wi ) ] , however,
C in cases of fractional pixels this underestimates the error since it neglects
C the correlation between adjacent bins.  We use the following equation:
C   var(F) = sum( Wi * var(Ai) ) / [ sum( Wi ) * sum( Wi ) ] , which is
C incorrect but probably a better estimate of the true error. Note that in
C the case of whole pixels the equations are the same.
C
      subroutine linear_errave(a,sc,ec,sr,er,row,pix1,pix2,
     .  bval,nozero,lgi)
C
      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er),bval
      real*8 pix1,pix2
      logical nozero,lgi,more
      real*8 Y0,Ym,Yp,x1,x3,sum,wsum,area,b0,b1,wt
      integer*4 i,i0,i1,i2

C Check for zero or negative pixels.
      if (nozero) then
        more = .true.
        do i=nint(pix1),nint(pix2)
          if (a(i,row).lt.1.e-30) more = .false.
        enddo
        if (.not.more) then
          bval = -1.
          return
        endif
      endif
C Sum and weight.
      sum = 0.d0
      wsum= 0.d0
C Whole pixels.
      i1 = nint(pix1) + 1
      i2 = nint(pix2) - 1
      if (i2.ge.i1) then
        do i=i1,i2
          sum = sum + (dble(a(i,row))*dble(a(i,row)))
          wsum= wsum+ 1.d0
        enddo
      endif

      IF (lgi) THEN

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))*dble(a(i0,row))
      Ym = dble(a(max(sc,i0-1),row))*dble(a(max(sc,i0-1),row))
      Yp = dble(a(min(ec,i0+1),row))*dble(a(min(ec,i0+1),row))
      x1 = dfloat(i0) - 1.d0
      x3 = dfloat(i0) + 1.d0
      call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
      sum = sum + area
      wsum= wsum+ (b1-b0)
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))*dble(a(i0,row))
        Ym = dble(a(max(sc,i0-1),row))*dble(a(max(sc,i0-1),row))
        Yp = dble(a(min(ec,i0+1),row))*dble(a(min(ec,i0+1),row))
        x1 = dfloat(i0) - 1.d0
        x3 = dfloat(i0) + 1.d0
        call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
        sum = sum + area
        wsum= wsum+ (b1-b0)
      endif

      ELSE

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))*dble(a(i0,row))
      wt = (b1-b0)
      sum = sum + wt*Y0
      wsum= wsum+ wt

C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))*dble(a(i0,row))
        wt = (b1-b0)
        sum = sum + wt*Y0
        wsum= wsum+ wt
      endif

      ENDIF

C Average flux error per unit original bin.
      if ((wsum.gt.0.).and.(sum.gt.0.)) then
        bval = sngl( sqrt( sum ) / wsum )
      else
        bval = -1.0
      endif

      return
      end


C----------------------------------------------------------------------
C Given three bin fluxes, Ym,Y0,Yp, and the x values at the center of
C the outer bins, x1,x3, find the flux or area between two x boundary
C values, b0 and b1. These boundaries should be contained within the limits
C of the middle bin (inclusive.)
C
C This routine uses lgi interpolation (i.e. polynomial interpolation) by
C finding a parabola which matches the area of the three bins across each bin.
C
C Imagine the center of the bins being at x = -1.0, 0.0, and +1.0, neglecting
C any non-linearity from x1 through x3, and the three point fluxes are
C Ym, Y0, and, Yp.  The solution to a parabola which goes through the three
C points ( y = c0 + c1*x + c2*x*x ) would be c0 = Y0 , c1 = (Yp-Ym)/2, and
C c2 = (Ym+Yp)/2 - Y0 .
C
C However, we want a parabola such that the area under the curve within the
C region of each bin matches the flux in that bin.  The solution to such a
C parabola ( y = a0 + a1*x + a2*x*x ) would be:  a1 = (Yp-Ym)/2 = c1 , 
C a2 = (Ym+Yp)/2 - Y0 = c2, and  a0 = Y0 - (a2/12) = c0 - (c2/12) .
C
      subroutine lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)

      implicit none
      real*8 Ym,Y0,Yp,x1,x3,b0,b1,area
      real*8 a0,a1,a2,n0,n1

C Coeffecients.
      a1 = (Yp-Ym)/2.d0
      a2 = ((Ym+Yp)/2.d0) - Y0
      a0 = Y0 - (a2/12.d0)

C Boundaries in terms of new x coordinates.
      n0 = ( 2.d0*(b0-x1)/(x3-x1) ) - 1.d0
      n1 = ( 2.d0*(b1-x1)/(x3-x1) ) - 1.d0

C Check (temporary?).
      if ((n1.lt.n0).or.(n0.lt.-0.501).or.(n1.gt.0.501)) then
        print *,'ERROR in lgi_align -- bad limits.'
        call exit(1)
      endif

C Integrate polynomial between n0 and n1.
      area = ( (a0*n1) + (a1*n1*n1/2.d0) + (a2*n1*n1*n1/3.d0) ) 
     .     - ( (a0*n0) + (a1*n0*n0/2.d0) + (a2*n0*n0*n0/3.d0) )

      return
      end


C----------------------------------------------------------------------
C Add cards to make this image compatible with IRAF WCS Multispec format.

      subroutine IRAF_Cards(header,crvl,cdlt,tnp)

      implicit none
      character*(*) header
      real*8 crvl(*),cdlt(*)
      integer*4 tnp(*)
      character s*5760, c12*12, wrd*200, c8*8
      integer*4 nr,inhead,row,i,k,lc,ptr,nc
C$$$$$$$$$$$
      real*8 zero,disp,aa,bb
      logical loglin,nwinit
      common /NWB/ zero,disp,aa,bb,loglin,nwinit
C$$$$$$$$$$$
      if (loglin) then
        call inheadset('DC-FLAG',1,header)
      endif
      nc = inhead('NAXIS1',header)
      nr = inhead('NAXIS2',header)
C Not for IRAF, just adding easy to read cards.
      do row=1,nr
        write(c8,'(a,i2.2)') 'CRVL1_',row
        call fheadset(c8,crvl(row),header)
        write(c8,'(a,i2.2)') 'CDLT1_',row
        call fheadset(c8,cdlt(row),header)
      enddo
C Must take out CRPIX cards for IRAF to work.
      call unfit('CRPIX1',header)
      call unfit('CRPIX2',header)
C This is for IRAF.
      call inheadset('WCSDIM',2,header)
      call cheadset('CTYPE1','MULTISPE',header)
      call cheadset('CTYPE2','MULTISPE',header)
      call fheadset('CD1_1',1.d0,header)
      call fheadset('CD2_2',1.d0,header)
      call fheadset('LTM1_1',1.d0,header)
      call fheadset('LTM2_2',1.d0,header)
C Note we need to use cheadset_fill, otherwise IRAF doesn't work.
      call cheadset_fill('WAT0_001','system=multispec',header)
      call cheadset_fill('WAT1_001',
     .   'wtype=multispec label=Wavelength units=Angstroms',header)
      s = ' '
      s = 'wtype=multispec'
      do row=1,nr
        if (row.gt.99) then
          write(c12,'(a,i3,a)') ' spec',row,' = "'
        elseif (row.gt.9) then
          write(c12,'(a,i2,a)') ' spec',row,' = " '
        else
          write(c12,'(a,i1,a)') ' spec',row,' = "  '
        endif
        k=nc
        if (tnp(row).gt.0) k=tnp(row)
        if (loglin) then
          write(wrd,'(a,i3,i3,i2,f18.11,f18.15,i6,f3.0,2f6.1,a)')
     .       c12(1:lc(c12)),row,row,1,crvl(row),cdlt(row),k,0.,
     .       float(row),float(row),'"'
        else
          write(wrd,'(a,i3,i3,i2,f18.11,f18.15,i6,f3.0,2f6.1,a)')
     .       c12(1:lc(c12)),row,row,0,crvl(row),cdlt(row),k,0.,
     .       float(row),float(row),'"'
        endif
        s = s(1:lc(s))//wrd(1:lc(wrd))
      enddo
      k = lc(s)
      ptr = 1
      i=0
      do while(ptr.le.k)
        i=i+1
        write(c8,'(a,i3.3)') 'WAT2_',i
C Note we need to use cheadset_fill, otherwise IRAF doesn't work.
        call cheadset_fill(c8,s(ptr:ptr+67),header)
        ptr=ptr+68
      enddo
      return
      end

C----------------------------------------------------------------------
C Set starting wavelength and dispersion.
      subroutine set_wave_disp(wave,nwp,crval1,cdelt1,crvl,cdlt,row)
      implicit none
      real*8 wave,crval1,cdelt1,crvl(*),cdlt(*),nwp
      integer*4 row
C$$$$$$$$$$$
      real*8 zero,disp,aa,bb
      logical loglin,nwinit
      common /NWB/ zero,disp,aa,bb,loglin,nwinit
C$$$$$$$$$$$
      if (loglin) then
        crval1 = aa + (bb*(nwp-1.d0))
        cdelt1 = bb
      else
        crval1 = wave
        cdelt1 = disp
      endif
      crvl(row) = crval1
      cdlt(row) = cdelt1
      return
      end




C----------------------------------------------------------------------
      real*8 function NewWave(pix)
      implicit none
      real*8 pix
C$$$$$$$$$$$
      real*8 zero,disp,aa,bb
      logical loglin,nwinit
      common /NWB/ zero,disp,aa,bb,loglin,nwinit
C$$$$$$$$$$$
      real*8 c,ln10
      parameter(c=2.99792458d+5)
      parameter(ln10=2.302585093d0)
      if (.not.nwinit) then
        if (loglin) then
          aa = log10(zero)
          bb = disp / (c*ln10)
        endif
        nwinit=.true.
      endif
      if (loglin) then
        NewWave = 10.d0 ** ( aa + ( bb * (pix - 1.d0) ) )
      else
        NewWave = zero + ( disp * (pix - 1.d0) ) 
      endif
      return
      end
      
C----------------------------------------------------------------------
C Interpolate zero points.
      subroutine interp_zero_pts(sc,ec,a)
      implicit none
      integer*4 sc,ec
      real*4 a(sc:ec),sum,b(90000)
      integer*4 i,num1,num2,i1,i2
C Copy to temporary array.
        do i=sc,ec
          b(i)=a(i)
        enddo
C Search for a zero pixel.
        i1=sc
        do while(i1.le.ec)
          if (abs(b(i1)).lt.1.e-30) then
C Find the extent of the region of zero pixels.
            i2=i1+1
            do while((i2.le.ec).and.(abs(b(i2)).lt.1.e-30))
              i2=i2+1
            enddo
            i2=i2-1
C Find a mean value for local valid pixels.
            sum=0.
C Look for valid pixels to the left of region.
            i=i1-1
            num1=0
            do while((i.ge.sc).and.(num1.lt.10))
              if (abs(b(i)).gt.1.e-30) then
                num1=num1+1
                sum =sum +b(i)
              endif
              i=i-1
            enddo
C Look for valid pixels to the right of region.
            i=i2+1
            num2=0
            do while((i.le.ec).and.(num2.lt.10))
              if (abs(b(i)).gt.1.e-30) then
                num2=num2+1
                sum =sum +b(i)
              endif
              i=i+1
            enddo
            sum = sum / float(max(1,num1+num2))
C Fill in zero range.
            do i=i1,i2
              a(i)=sum
            enddo
          else
            i2=i1
          endif
          i1=i2+1
        enddo

      return
      end

C----------------------------------------------------------------------
C Set error value to -1 for all data pixels which are zero.
      subroutine ErrorMaskForZero(d,e,sc,ec,sr,er)
      implicit none
      integer*4 sc,ec,sr,er,i,j
      real*4 d(sc:ec,sr:er),e(sc:ec,sr:er)
      do i=sc,ec
        do j=sr,er
          if (abs(d(i,j)).lt.1.e-29) e(i,j)=-1.0
        enddo
      enddo
      return
      end

C----------------------------------------------------------------------
      subroutine readxy(file,x8,y,npix)
      implicit none
      character*(*) file
      real*8 x8(*),r81,r82
      real*4 y(*)
      integer*4 npix
      npix=0
      open(33,file=file,status='old')
1     read(33,*,end=5) r81,r82
      npix=npix+1
      x8(npix)= r81
      y(npix) = sngl(r82)
      goto 1
5     close(33)
      return
      end
C----------------------------------------------------------------------
      real*8 function getpix(npix,x8,wave)
      implicit none
      integer*4 npix,neari_bs8,i
      real*8 x8(*),wave,d
      i = neari_bs8(wave,npix,x8)
      if ((i.le.1).or.(i.ge.npix)) then
        print *,'Error-- too close to edge of spectrum: ',i,npix
        getpix = -1.d0
        return
      endif
      d = ( x8(i+1) - x8(i-1) ) / 2.d0
      getpix = dfloat(i) + (wave-x8(i))/d
      return
      end
