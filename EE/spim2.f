C spim2.f                                               tab May-September 1995
C
C Multiple order, multiple image viewing of polynomial wavelength scale spectra.
C
      implicit none
      integer*4 narg,inhead,access,row
      integer*4 n,col,im,lc,i,j,k,order,naa
      character*80 arg(99),wrd,wrds(9),chead,file,card1,card2
      character c7*7
      real*4 r1,r2,r3,r4,r5,cd(9,2),cdall1,cdall2,valread
      real*8 r8,polyval,high,wrms,c,hvsf,hvsf2,fhead
      parameter(c=2.997925d+5)
      logical ok,findarg,nocheck,card_exist,noerr
C
      include 'spim2.inc'
C
      integer*4 maxpt
      parameter(maxpt = mpt*mrw)
      real*4 a(maxpt)

C Initialize.
      do i=1,mim
        nrw(i)= 0
        sc(i) = 0
        ec(i) = 0
        nc(i) = 0
        scb(i)= 0
        ecb(i)= 0
        ncb(i)= 0
        do j=1,mrw
          do k=1,mpt
            f(k,j,i)  = 0.
            e(k,j,i)  = 0.
            w(k,j,i)  = 0.
            fb(k,j,i) = 0.
            eb(k,j,i) = 0.
            wb(k,j,i) = 0.
            fo(k,j,i) = 0.
            eo(k,j,i) = 0.
            wo8(k,j,i)= 0.d0
          enddo
        enddo
      enddo
      bighead=' '
      ncbig  = 0
      do i=1,9
        cd(i,1)=0.
        cd(i,2)=0.
      enddo
      cdall1 =-1.
      cdall2 =-1.

C Minimum and maximum ratio rejection limits.  When dividing by the 
C big template spectrum, some values are rejected if they are below or
C above this level (adjusted for median of division).
      minratrej = -1.
      maxratrej = 1000.
      divart    = 0

C Order used with "v" option.
      v_order = 8

C Zero logical indicators for atmospheric features star division.
      do i=1,mrw
        StarDivDone(i)=.false.
      enddo

C Get command line parameters.
      call arguments(arg,narg,99)
      nocheck = findarg(arg,narg,'-nocheck',':',wrd,i)
      noerr   = findarg(arg,narg,'-noerr',':',wrd,i)
      bigfile = ' '
      if (findarg(arg,narg,'big=',':',wrd,i)) bigfile= wrd
      if (findarg(arg,narg,'cd=', ':',wrds,i)) then
        if (i.ne.2) then 
          print *,'Error in cd= option.'
          call exit(1)
        endif
        cdall1 = valread(wrds(1))
        cdall2 = valread(wrds(2))
      endif
C Correct Deviants options.
      call CD_ops(arg,narg,cd)
C Atmospheric division star.
      AtmIm=0
      if (findarg(arg,narg,'-star',':',wrd,i)) AtmIm=2
      if (narg.eq.0) then
        print *,'Syntax: spim2 (file root) [(2nd file root) [(3r',
     .                                         'd file root)] ... ]'
        print *,
     .  '            [big=(FITS file)]  [-star]  [-nocheck]  [-noerr]'
        print *,
     .  '            [cd=median box:threshold] (for all images)'
        print *,
     .  '            [cd1= ...] [cd2= ...] (for specific images)'
        print *,
     .  '   Multiple order, multiple image viewing of polynomial'
        print *,'   wavelength scale spectra.  file = (root).fits'
        print *,'   Error file = (root)e.fits or E-(subroot).fits'
        print *,
     .  ' big=  : A large wavelength-span spectrum to overplot.'
        print *,
     .  ' -star : The second file listed is to be used for division of'
        print *,
     .  '         of atmospheric absorption features in first file.'
        print *,
     .  ' cd=, cd1=, ... cd9= : Run "Correct Deviants" routine.'
        print *,
     .  ' -nocheck : do not prompt before writing out modified images.'
        print *,' -noerr   : do not try to load error files.'
        call exit(0)
      endif

C Load big file.
      if (bigfile.ne.' ') then
        call AddFitsExt(bigfile)
        call rw_bigspec(1)
      endif

C Load filename roots.
      nim=0
      do while(narg.gt.nim)
        nim = nim + 1
        if (nim.gt.mim) then
          print *,'ERROR -- You can only have',mim,' images.' 
          call exit(1)
        endif
        dfile(nim) = arg(nim)
        call AddFitsExt(dfile(nim))
C Look for error file.
        if (noerr) then
          efile(nim)=' '
          goto 88
        endif
        efile(nim)=' '
        i = index(dfile(nim),'Flux-')
        if ((efile(nim).eq.' ').and.(i.gt.0)) then
          file = 'Err'//dfile(nim)(i+4:)
          if (access(file,'r').eq.0) efile(nim)=file
        endif
        i = index(dfile(nim),'FF-')
        if ((efile(nim).eq.' ').and.(i.gt.0)) then
          file = 'FE'//dfile(nim)(i+2:)
          if (access(file,'r').eq.0) efile(nim)=file
        endif
        i = index(dfile(nim),'F-')
        if ((efile(nim).eq.' ').and.(i.gt.0)) then
          file = 'E'//dfile(nim)(i+1:)
          if (access(file,'r').eq.0) efile(nim)=file
        endif
        i = index(dfile(nim),'.fits')
        if ((efile(nim).eq.' ').and.(i.gt.0)) then
          file = dfile(nim)(1:i-1)//'e.fits'
          if (access(file,'r').eq.0) efile(nim)=file
        endif
        if (efile(nim).eq.' ') then
          print '(2a)',' Could not find error file for ',
     .                   dfile(nim)(1:lc(dfile(nim)))
          call exit(1)
        endif
88      continue
      enddo
      print *,'Number of images=',nim

C Set "cd" values if cdall set.
      if (cdall1.gt.0.) then
        do i=1,nim
          cd(i,1)=cdall1
          cd(i,2)=cdall2
        enddo
      endif

C There must be two images with AtmIm .
      if ((AtmIm.gt.0).and.(nim.ne.2)) then
        print *,
     .  'Error-- There must be two images when using -star option.'
        call exit(1)
      endif

C Read in sets of FITS files...
      do im=1,nim
C Read flux data.
        print '(2a)',' Reading : ',dfile(im)(1:lc(dfile(im)))
        file = dfile(im)
        call readfits(file,a,maxpt,header(im),ok)
        if (.not.ok) goto 950
C Initialize counting of a pixels.
        naa=0
C Transfer header for convenience.
        temphead= header(im)
C Number of rows.
        nrw(im) = inhead('NAXIS2',temphead)
        if (nrw(im).gt.mrw) then
          print *,'ERROR- Maximum number of rows is: ', mrw
          call exit(1)
        endif
C Get column range.
        sc(im) = inhead('CRVAL1',temphead)
        nc(im) = inhead('NAXIS1',temphead)
        ec(im) = sc(im) + nc(im) - 1
        scb(im)= sc(im)
        ncb(im)= nc(im)
        ecb(im)= ec(im)
C Check number of columns.
        if (nc(im).gt.mpt) then
          print *,'ERROR- Maximum number of columns is: ', mpt
          call exit(1)
        endif
C Start counting pixels in a().
        n=0
        do row=1,nrw(im)
C Get polynomial coeffecients.
          write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
          card1 = chead(c7,temphead)
          write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
          card2 = chead(c7,temphead)
          if ((card1.eq.' ').or.(card2.eq.' ')) goto 805
          read(card1,'(4(1pe17.9))',err=805)
     .         cof(0,row,im),cof(1,row,im),cof(2,row,im),cof(3,row,im)
          read(card2,'(4(1pe17.9))',err=805)
     .         cof(4,row,im),cof(5,row,im),cof(6,row,im),cof(7,row,im)
          ord(row,im)=7
C Record blue and red wavelength limits for convenience.
          blwv(row,im)=
     .  sngl(polyval(ord(row,im)+1,cof(0,row,im),dfloat(sc(im))))
          rdwv(row,im)=
     .  sngl(polyval(ord(row,im)+1,cof(0,row,im),dfloat(ec(im))))
C Derive inverse polynomial coeffecients.
          order=12
7         continue
          n=0
          do col=sc(im),ec(im)
            n = n + 1
            x8(n) = polyval(ord(row,im)+1,cof(0,row,im),dfloat(col)) 
            y8(n) = dfloat(col)
            z8(n) = 1.d0
          enddo
          call poly_fit_glls(n,x8,y8,z8,order,icof(0,row,im),ok)
          if (.not.ok) then
            if (order.lt.20) then
              order=order+1
              goto 7
            else
              print '(a,2i5,2f8.2)',
     .  ' Row,Order,High,WRMS=',row,order,high,wrms
              print *,'Error fitting inverse polynomials.'
              call exit(1)
            endif
          endif
         call poly_fit_glls_residuals(n,x8,y8,z8,order,
     .                              icof(0,row,im),high,wrms)
          if ((high.gt.3.0).or.(wrms.gt.0.8)) then
            if (order.lt.18) then
              order=order+1
              goto 7
            else
              print '(a,2i5,2f8.2)',
     .  ' Row,Order,High,WRMS=',row,order,high,wrms
              print *,'Error fitting inverse polynomials.'
              call exit(1)
            endif
          endif
          if ((high.gt.1.0).or.(wrms.gt.0.3)) then
            print '(a,2i5,2f8.2)',
     .  ' Row,Order,High,WRMS=',row,order,high,wrms
          endif
C Save order of inverse polynomial.
          iord(row,im)=order
C Load data arrays.
          do col=sc(im),ec(im)
            naa=naa+1
            f(col,row,im)  = a(naa)
            fo(col,row,im) = a(naa)
            fb(col,row,im) = a(naa)
            r8 = polyval(ord(row,im)+1,cof(0,row,im),dfloat(col))
            w(col,row,im)  = sngl(r8)
            wo8(col,row,im)= r8
            wb(col,row,im) = sngl(r8)
          enddo
        enddo

C Read error data.
        if (efile(nim).ne.' ') then
          print '(2a)',' Reading : ',efile(im)(1:lc(efile(im)))
          file = efile(im)
          call readfits(file,a,maxpt,eheader(im),ok)
          if (.not.ok) goto 950
C Initialize counting of a() array pixels.
          naa=0
C Transfer header for convenience.
          temphead = eheader(im)
C Number of rows.
          i = inhead('NAXIS2',temphead)
          if (i.ne.nrw(im)) then
            print *,
     .  'Number of rows in data and error files do not match.'
            call exit(1)
          endif
C Get column range.
          i = inhead('CRVAL1',temphead)
          if (i.ne.sc(im)) then
            print *,
     .  'Starting column in data and error files do not match.'
            call exit(1)
          endif
C Start counting pixels in a().
          do row=1,nrw(im)
C Load data arrays.
            do col=sc(im),ec(im)
              naa=naa+1
              e(col,row,im) = a(naa)
              eo(col,row,im)= a(naa)
              eb(col,row,im)= a(naa)
            enddo
          enddo
        endif
      enddo

C Load atmospheric line data.
      if (AtmIm.gt.0) then
        print *,'Loading atmospheric line data...'
        if (.not.card_exist('HELIOVEL',header(1))) then
          print *,'ERROR- Cannot find HELIOVEL card in first file.'
          call exit(1)
        endif
        hvsf = fhead('HELIOVEL',header(1))
        print '(a,f9.5)',
     .  ' Object heliocentric velocity correction=',hvsf
        hvsf = dsqrt( (1.d0+(hvsf/c)) / (1.d0-(hvsf/c)) )
        call GetMakeeHome(wrd)
        wrd = wrd(1:lc(wrd))//'/atmabs.dat'
        open(33,file=wrd,status='old')
        naal=0
11      read(33,*,end=55) r1,r2,r3,r4,r5
C Shift wavelengths to match object velocity correction.
        r1 = sngl( dble(r1) * hvsf )
        r2 = sngl( dble(r2) * hvsf )
        naal=naal+1
        aablu(naal)= r1
        aared(naal)= r2
        aarsi(naal)= r3
        goto 11
55      close(33)
C Shift wavelengths of Atm. Star to match velocity of object.
        if (.not.card_exist('HELIOVEL',header(AtmIm))) then
          print *,'ERROR- Cannot find HELIOVEL card in second file.'
          call exit(1)
        endif
        hvsf2 = fhead('HELIOVEL',header(AtmIm))
        print '(a,f9.5)',
     .  '   Star heliocentric velocity correction=',hvsf2
        hvsf2 = dsqrt( (1.d0+(hvsf2/c)) / (1.d0-(hvsf2/c)) )
        do j=1,nrw(AtmIm)
          do i=sc(AtmIm),ec(AtmIm)
            r8 = wo8(i,j,AtmIm)
            r8 = ( r8 / hvsf2 ) * hvsf
            w(i,j,AtmIm)  = sngl(r8)
            wo8(i,j,AtmIm)= r8
            wb(i,j,AtmIm) = sngl(r8)
          enddo
        enddo
      endif

C Initially no modifications.
      do im=1,nim
        modified(im) = .false.
      enddo

C Set the error array to -1 where the flux is exactly zero.
      do im=1,nim
        if (efile(im).ne.' ') then
          do row=1,nrw(im)
            j=0
            do i=sc(im),ec(im)
              if ((e(i,row,im).lt.1.e-30).or.
     .                    (abs(f(i,row,im)).lt.1.e-30)) then
                if (abs(e(i,row,im)+1.).gt.1.e-30) then
                  j=j+1
                  e(i,row,im) = -1.
                  eo(i,row,im)= -1.
                  eb(i,row,im)= -1.
                endif
              endif
            enddo
          enddo
        endif
      enddo

C Initial binning.
      bin = 1

C Load binned array.
      print *,'Current binning = ',bin

C Primary image.
      prim = 1

C If Correct Deviants option selected, then run non-interactively.
      if (cd(1,1).gt.0.) then
        do i=1,nim
          if (cd(i,1).gt.0.) then
            prim=i
            j =nint(cd(1,1))
            r1=cd(1,2)
            print *,'Correct deviants in image: ',prim,j,r1
            call CorrectDeviants(prim,j,r1,0)
            modified(prim)=.true.
            print *,'All done rejecting deviant pixels.'
          endif
        enddo
        goto 77
      endif

C Interactive plotting.
      row=1
      call spim2(row)

77    continue

C Write sets of modified FITS files out.
      do im=1,nim
        if (modified(im)) then
          print *,'Image',im,' has been modified.'
          if (nocheck) then
            i=1
          else
78          print '(a,$)',
     .  ' Do you want to overwrite original files? (1/0) : '
            read(5,*,err=78) i 
            if ((i.ne.0).and.(i.ne.1)) goto 78
          endif
          if (i.eq.1) then
C Data array.
            i=0
            do row=1,nrw(im)
              do col=sc(im),ec(im)
                i=i+1
                a(i)=f(col,row,im)
              enddo
            enddo
            file = dfile(im)
            print '(2a)',' Writing : ',file(1:lc(file))
            call writefits(file,a,header(im),ok)
            if (.not.ok) goto 960
C Error array.
            i=0
            do row=1,nrw(im)
              do col=sc(im),ec(im)
                i=i+1
                a(i)=e(col,row,im)
              enddo
            enddo
            file = efile(im)
            print '(2a)',' Writing : ',file(1:lc(file))
            call writefits(file,a,eheader(im),ok)
            if (.not.ok) goto 960
          endif
        endif
      enddo

      stop
805   print *,'Error getting polynomial wavelength scale.'
      call exit(1)
950   print *,'Error reading: ',file(1:lc(file))
      call exit(1)
960   print *,'Error writing: ',file(1:lc(file))
      call exit(1)
      end

CENDOFMAIN
