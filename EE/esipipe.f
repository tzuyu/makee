C
C esipipe.f                                                    tab  Oct00
C
C Create a makee/ESI script.
C
      implicit none
C
      integer*4 ii,narg,lc,flatresum
      character*80 arg(9),file1,file2,rawdir,extopt,wrd
      logical findarg

C Command line arguments.
      call arguments(arg,narg,9)

      rawdir = './'
      if (findarg(arg,narg,'raw=',':',wrd,ii)) rawdir = wrd

      flatresum = 0
      if (findarg(arg,narg,'-flatresum','s',wrd,ii)) flatresum = 1
     
      extopt = ' '
      if (narg.gt.2) then
        extopt = arg(3)
        narg = narg - 1
      endif

C Syntax.
      if (narg.ne.2) then
        print *,'Syntax: esipipe (file with list of raw FITS files)'
        print *,'              (file with list of star frame numbers)'
        print *,'           [ raw= ]  ["makee extra options"]'
        print *,'           [ -flatresum]'
        print *,' '
        print *,'    Create a makee script file for ESI raw data.'
        print *,' '
        print *,' -flatresum : Resum the flats even if the sums exist.'
        print *,' '
        call exit(0)
      endif

C Files.
      file1 = arg(1)
      file2 = arg(2)

      ii = lc(file1)
      file1 = file1(1:ii)//char(0)

      ii = lc(file2)
      file2 = file2(1:ii)//char(0)

      ii = lc(rawdir)
      rawdir = rawdir(1:ii)//char(0)

      ii = lc(rawdir)
      extopt = extopt(1:ii)//char(0)

C Call esipipe.
      call ffesipipe(file1,file2,rawdir,extopt,flatresum)

      stop
      end
