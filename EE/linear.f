C linear.f    Parse a 2-D spectra image and linearize the spectra.
C    tab Nov94,Mar95 
C
      implicit none
      integer*4 nc,nr,sc,sr,er,ec,narg,inhead,row,i,j,k,lc,rowbeg,rowend
      integer*4 enc,enr,esc,esr,np,urow,lastrow,lastcol
      character*115200 header,newhead
      character*80 arg(9),fullfile,newfile,errfile,wrd
      character*80 chead,newobject,object,table
      character c7*7
      integer*4 mx,my,maxpt
      parameter(mx=19000,my=99,maxpt=mx*my)
      real*4 a(maxpt),ae(maxpt),d(mx,my),de(mx,my),b(mx),be(mx)
      real*4 valread,sum
      real*8 crvl(my),cdlt(my),r1,r2,tsw(my),tdp(my)
      integer*4 tnp(my),getpos
      logical ok,findarg,onedim,nozero,lgi,listdisp,image
      real*8 valread8,scw,ecw,wave,crval1,cdelt1,hidisp,lodisp
      real*8 udisp,pix1,pix2,GetSpimWave,GetSpimPix,NewWave,nwp
      real*8 waveblue,wavered
C$$$$$$$$$$$
      real*8 zero,disp,aa,bb
      logical loglin,nwinit
      common /NWB/ zero,disp,aa,bb,loglin,nwinit
C$$$$$$$$$$$
C
      include 'soun.inc'        ! Standard output unit number.
C
C Initialize.
      soun = 6
      nwinit=.false.
C
C Get command line parameters and set defaults.
      call arguments(arg,narg,9)
      onedim = findarg(arg,narg,'-1d',':',wrd,i)
      nozero = findarg(arg,narg,'-nozero',':',wrd,i)
      lgi    = findarg(arg,narg,'-lgi',':',wrd,i)
      image  = findarg(arg,narg,'-image',':',wrd,i)
      listdisp = findarg(arg,narg,'-listdisp',':',wrd,i)
      loglin = findarg(arg,narg,'-loglin',':',wrd,i)
      table  = ' '
      if (findarg(arg,narg,'table=',':',wrd,i)) table = wrd
      errfile=' '
      if (findarg(arg,narg,'ef=',':',wrd,i)) then
        errfile= wrd
        call AddFitsExt(errfile)
      endif
      zero=3000.d0
      if (findarg(arg,narg,'zero=',':',wrd,i)) zero = valread8(wrd)
      udisp=0.d0
      if (findarg(arg,narg,'disp=',':',wrd,i)) udisp = valread8(wrd)
      urow=0
      if (findarg(arg,narg,'row=',':',wrd,i)) urow=nint(valread(wrd))
      if ((table.ne.' ').and.loglin) then
        print *,'ERROR- you cannot use table= and -loglin together.'
        call exit(1)
      endif
      if ((abs(udisp).lt.1.e-20).and.(loglin)) then
        print *,'ERROR- you must specify disp= with -loglin.'
        call exit(1)
      endif

C If zero parameters or improper parameters, tell user syntax. 
      if (narg.ne.1) then
        print *,'Syntax: linear (wavelength calib. FITS file)'
        print *,'          [-image] [-loglin]'
        print *,
     .  '          [disp=d]  [zero=w]  [-1d]  [-nozero]  [ef=filename]'
        print *,
     .  '          [row=]  [-listdisp]  [-lgi]  [table=filename]'
        print *,
     .  '  Produces linear wavelength calibrated FITS spectra.  Input:'
        print *,
     .  '  file.fits  Output (each row): file-##.fits where ## is the'
        print *,'  row number.  Output (whole image): file-L.fits .'
        print *,'Options:'
        print *,
     . '  -image : Create a 2 dimensional image of linear spectra with'
        print *,'           IRAF compatible header cards.'
        print *,
     .  ' -loglin : use a log-linear relation. In this case, disp= is'
        print *,
     .  '           equal to the velocity width of each bin in km/s .'
        print *,
     .  '  disp=d : align such that the Angstroms per pixel is "d".'
        print *,
     .  '           (Default is the average pixel width for each row.)'
        print *,
     .  '  zero=w : specifies that the spectra should be aligned such'
        print *,
     . '           the wavelength "w" would be at the center of a bin.'
        print *,'           (Default value is 3000.)'
        print *,
     .  '    -1d  : Write out one dimensional spectra, i.e. NAXIS=1.' 
        print *,
     .  ' -nozero : Ignore zero values, if any pixel within the new'
        print *,
     .  '           bin is zero, the new bin flux will be zero.'
        print *,
     . '   ef=   : Error file. Align this data in a consistent matter.'
        print *,
     . '           Any new bin which contains a zero or negative pixel'
        print *,
     .  '           will be set to -1.'
        print *,
     .  '  row=   : Only produce spectrum file for specified row.'
        print *,
     .  '-listdisp: List default dispersion for all rows, no writing.'
        print *,
     .  '  -lgi   : Do lagrangian (area polynomial) interpolation (not'
        print *,'           recommended.)'
        print *,
     . ' table=  : Specify alignment for row(s) in a file.  The format'
        print *,
     .  '           is: (row#) (starting wavelength) (dispersion)'
        print *,'           (number of pixels) . One entry per line.'
        print *,'  '
        call exit(0)
      endif
C Full 2-D spectra image.
      fullfile = arg(1)
      call AddFitsExt(fullfile)
C Echo to user.
      print '(a,f10.4,f10.6)',
     .  'Zero wavelength and dispersion= ',zero,udisp
      if (loglin) print *,'Dispersion is in kilometers per second.'
C Read fits file.
      print '(2a)',' Reading: ',fullfile(1:lc(fullfile))
      call readfits(fullfile,a,maxpt,header,ok)
      if (.not.ok) goto 860
      nc = max(0,inhead('NAXIS1',header))
      nr = max(0,inhead('NAXIS2',header))
      if ((nc.eq.0).or.(nr.eq.0)) then
        print *,'ERROR-- Image must be two dimensional.'
        call exit(1)
      endif
      sc = inhead('CRVAL1',header)
      sr = inhead('CRVAL2',header)
      ec = sc + nc - 1
      er = sr + nr - 1
      object = chead('OBJECT',header)

C Table values.
      do i=1,my
        tsw(i)=0.d0
        tdp(i)=0.d0
        tnp(i)=0
      enddo
C Read from file.
      if (table.ne.' ') then
        open(1,file=table,status='old')
1       read(1,*,end=5) row,r1,r2,i
        if ((row.lt.1).or.(row.gt.my)) then
          print *,'ERROR- Row in table out of range.  # of rows=',nr
          call exit(1)
        endif 
        tsw(row) = r1
        tdp(row) = r2
        tnp(row) = i
        goto 1
5       close(1)
      endif
C Echo to user.
      do row=sr,er
        if (tnp(row).gt.0) then
          print '(i5,2f22.15,i6)',row,tsw(row),tdp(row),tnp(row)
        endif
      enddo
   
C Read error file.
      if (errfile.ne.' ') then
        print '(2a)',' Reading: ',errfile(1:lc(errfile))
        call readfits(errfile,ae,maxpt,header,ok)
        if (.not.ok) goto 860
        enc = max(0,inhead('NAXIS1',header))
        enr = max(0,inhead('NAXIS2',header))
        esc = inhead('CRVAL1',header)
        esr = inhead('CRVAL2',header)
        if ((nc.ne.enc).or.(nr.ne.enr).or.
     .               (sc.ne.esc).or.(sr.ne.esr)) then
          print *,'ERROR: File dimensions do not match.'
          call exit(1)
        endif
      endif

C Initialize wavelength = func(pixel) function.
      if (GetSpimWave(header,0.d0,0).lt.0.) then
        print *,'Error getting wavelength scale.'
        call exit(1)
      endif
C Initialize pixel = func(wavelength) function.
      if (GetSpimPix(header,0.d0,0).lt.0.) then
        print *,'Error creating inverse wavelength scale.'
        call exit(1)
      endif

C Starting and stopping rows.
      if (urow.ne.0) then
        if ((urow.lt.sr).or.(urow.gt.er)) then
          print *,'ERROR- User specified row is out of bounds.'
          print *,'  The row bounds are: ',sr,er
          call exit(1)
        endif
        rowbeg = urow
        rowend = urow
      else
        rowbeg = sr
        rowend = er
      endif
C List only.
      if (listdisp) then
        do row=rowbeg,rowend
          hidisp = -1.d+60
          lodisp = +1.d+60
          do i=sc,ec,10
            disp = GetSpimWave(header,dfloat(i+1),row)
     .           - GetSpimWave(header,dfloat(i),row)
            if (disp.gt.hidisp) hidisp=disp
            if (disp.lt.lodisp) lodisp=disp
          enddo
          scw = GetSpimWave(header,dfloat(sc),row)
          ecw = GetSpimWave(header,dfloat(ec),row)
          disp = (ecw-scw) / dfloat(ec-sc)
          print '(a,i3,a,2f11.4,f10.6,a,2f8.4,f6.2)',' Row',row,' :', 
     .          scw,ecw,disp,'   Dispersion range:',
     .          lodisp,hidisp
        enddo
        stop
      endif

C Check for polynomial wavelength scale.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',sr
      if (getpos(c7,header).eq.-1) then
        print *,'ERROR- Input file must have polynomial scale.'
        call exit(1)
      endif

C Copy header to new header and remove unneccessary cards.
      newhead = header
      do row=sr,er
        write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
        call unfit(c7,newhead)
        write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
        call unfit(c7,newhead)
        write(c7,'(a,i1,a,i2.2)') 'CO_',0,'_',row
        call unfit(c7,newhead)
        write(c7,'(a,i1,a,i2.2)') 'CO_',4,'_',row
        call unfit(c7,newhead)
      enddo

C Comment on interpolation scheme in header.
      if (loglin) then
        call cheadset('LININTRP',
     .'Simple (flat bins) interpolation to log-linear scale.',newhead)
      elseif (lgi) then
        call cheadset('LININTRP',
     .'Lagrangian (polynomial) interpolation to linear scale.',newhead)
      else
        call cheadset('LININTRP',
     . 'Simple (flat bins) interpolation to linear scale.',newhead)
      endif

C Clear two dimensional array.
      do i=1,mx
        do j=1,my
          d(i,j) =0.d0
          de(i,j)=0.d0
        enddo
      enddo

C Do each row.
      row=rowbeg
      DO while(row.le.rowend)

C New object name
      if (.not.image) then
        write(newobject,'(a,a,i2.2,a)')
     .       object(1:lc(object)),' [',max(-9,min(99,row)),']'
        call cheadset('OBJECT',newobject,newhead)
      endif
C Form new filename.
      i=index(fullfile,'.fits')
      if (i.eq.0) goto 850
      write(newfile,'(a,a,i2.2,a)') fullfile(1:i-1),'-',row,'.fits'
C First and last wavelength.
      scw = GetSpimWave(header,dfloat(sc),row)
      ecw = GetSpimWave(header,dfloat(ec),row)
C Set dispersion.
      if (udisp.gt.0.) then
        disp = udisp
      elseif (tnp(row).gt.0) then
        disp = tdp(row)
      else
        disp = (ecw-scw) / dfloat(ec-sc)
        if (image) then
          print '(a,i3,a,2f11.4,f11.7)',' Row',row,' :',scw,ecw,disp
        else
          print '(a,i3,a,2f11.4,f11.7,2a)',
     .          ' Row',row,' :',scw,ecw,disp,
     .          '    Writing: ',newfile(1:lc(newfile))
        endif
      endif
C First bin central wavelength.
      nwp = 0.d0
      wave= NewWave(nwp)
      do while(wave.lt.scw)
        nwp = nwp + 1.d0
        wave= NewWave(nwp)
      enddo
C If first wavelength specified, override value of wave. 
      if (tnp(row).gt.0) wave = tsw(row)
C Begin counting new pixels.
      np = 0 
C Do each new bin.
      do while(wave.lt.ecw)
C Pixel boundaries.
77      continue
        wave    = NewWave(nwp)
        waveblue= NewWave(nwp-0.5d0)
        wavered = NewWave(nwp+0.5d0)
        if (waveblue.lt.scw) then
          nwp=nwp+1.d0
          goto 77
        endif
        pix1 = GetSpimPix(header,waveblue,row)
        pix2 = GetSpimPix(header,wavered,row)
        if ((nint(pix1).ge.sc).and.(nint(pix2).le.ec)) then
          np = np + 1
          if (np.eq.1) then
            call set_wave_disp(wave,nwp,crval1,cdelt1,crvl,cdlt,row)
          endif
          call linear_fluxave(a,sc,ec,sr,er,row,
     .                             pix1,pix2,b(np),nozero,lgi)
          if (errfile.ne.' ') then
            call linear_errave(ae,sc,ec,sr,er,row,pix1,pix2,be(np),lgi)
          endif
C If starting wavelength was specified we allow zero pixels prior to data.
        elseif (tnp(row).gt.0) then
          np = np + 1  
          if (np.eq.1) then
            call set_wave_disp(wave,nwp,crval1,cdelt1,crvl,cdlt,row)
          endif
          b(np) = 0.
          be(np)= 0.
        endif
C Next bin.
        nwp = nwp + 1.d0
        wave= NewWave(nwp)
      enddo
C Store data in 2-D array.
      if (image) then
        if (np.gt.mx) then
          print *,'Error- Number of pixels exceeded bounds of array.'
          print *,'You will need to increase the "mx" parameter.'
          call exit(1)
        endif
        if (row.gt.my) then
          print *,'Error- Number of rows exceeded bounds of array.'
          print *,'You will need to increase the "my" parameter.'
          call exit(1)
        endif
        do i=1,np
          d(i,row) = b(i)
          de(i,row)= be(i)
        enddo
      else
C Writing one-dimensional spectrum. Add new cards to new header.
        call fheadset('CRVAL1',crval1,newhead)
        call fheadset('CDELT1',cdelt1,newhead)
        call cheadset('CTYPE1','LAMBDA',newhead)
        if (loglin) call inheadset('DC-FLAG',1,newhead)
        call inheadset('NAXIS1',np,newhead)
        call inheadset('CRPIX1',1,newhead)
        call inheadset('ORIG_ROW',row,newhead)
        if (onedim) then
          call inheadset('NAXIS',1,newhead)
          call unfit('NAXIS2',newhead)
          call unfit('CRVAL2',newhead)
          call unfit('CDELT2',newhead)
          call unfit('CRPIX2',newhead)
        else
          call inheadset('NAXIS',2,newhead)
          call inheadset('NAXIS2',1,newhead)
          call inheadset('CRVAL2',0,newhead)
          call inheadset('CDELT2',1,newhead)
          call inheadset('CRPIX2',1,newhead)
        endif
C Write out a single row as a spectrum.
        call cheadset('FILENAME',newfile,newhead)
        call writefits(newfile,b,newhead,ok)
        if (.not.ok) goto 860
C Write out error file.
        if (errfile.ne.' ') then
C New object name
          write(newobject,'(a,a,i2.2,a)')
     .      object(1:lc(object)),' (Error) [',max(-9,min(99,row)),']'
          call cheadset('OBJECT',newobject,newhead)
C Form new filename.
          i=index(errfile,'.fits')
          if (i.eq.0) then
            print *,'Extension on error file must be .fits .'
            call exit(1)
          endif
          write(newfile,'(a,a,i2.2,a)') errfile(1:i-1),'-',row,'.fits'
C Write out a single row as a spectrum.
          call cheadset('FILENAME',newfile,newhead)
          call writefits(newfile,be,newhead,ok)
          if (.not.ok) goto 860
        endif
      endif
C Increment row number.
      row=row+1

      ENDDO

C Write out two dimensional image.
      if (image) then
C Window image to smallest possible box.
        lastrow=0
        do j=1,my
          sum=0.
          do i=1,mx
            sum=sum+abs(d(i,j))
          enddo
          if (sum.gt.0.) lastrow=j
        enddo
        lastcol=0
        do j=1,my
          do i=lastcol+1,mx
            if (abs(d(i,j)).gt.0.) lastcol=i
          enddo
        enddo
C If any rows specified to have more columns, increase lastcol.
        do j=1,my
          if (tnp(j).gt.lastcol) lastcol=tnp(j)
        enddo
C Copy image into scratch image.
        do j=1,my
          do i=1,mx
            k=((j-1)*mx) + i
            a(k) = d(i,j)
            ae(k)= de(i,j)
          enddo
        enddo
C New object name.
        if (loglin) then
          write(newobject,'(2a)') object(1:lc(object)),' (Log-Linear)'
        else
          write(newobject,'(2a)') object(1:lc(object)),' (Linear)'
        endif
        call cheadset('OBJECT',newobject,newhead)
C New filename.
        i=index(fullfile,'.fits') 
        if (i.eq.0) goto 850
        write(newfile,'(a,a)') fullfile(1:i-1),'-L.fits'
C Put maximum dimensions in header.
        call inheadset('CRVAL1',1,newhead)
        call inheadset('CRVAL2',1,newhead)
        call inheadset('NAXIS1',mx,newhead)
        call inheadset('NAXIS2',my,newhead)
C Window the image.
        call window_fits(a,newhead,1,lastcol,1,lastrow,ok)
C Add IRAF "FITS" cards to header.
        call IRAF_Cards(newhead,crvl,cdlt,tnp)
C Write out image.
        print '(2a)',' Writing: ',newfile(1:lc(newfile))
        call cheadset('FILENAME',newfile,newhead)
        call writefits(newfile,a,newhead,ok)
        if (.not.ok) goto 860
C Write out error file.
        if (errfile.ne.' ') then
C New object name
          write(newobject,'(2a)') newobject(1:lc(newobject)),' (Error)'
          call cheadset('OBJECT',newobject,newhead)
C Form new filename.
          i=index(newfile,'Flux-')
          j=lc(newfile)
          if (i.gt.0) then
            newfile = 'Err'//newfile(i+4:j)
          else
            newfile = 'Err-'//newfile(1:j)
          endif
C Put maximum dimensions in header.
          call inheadset('CRVAL1',1,newhead)
          call inheadset('CRVAL2',1,newhead)
          call inheadset('NAXIS1',mx,newhead)
          call inheadset('NAXIS2',my,newhead)
C Window the error image.
          call window_fits(ae,newhead,1,lastcol,1,lastrow,ok)
C Add IRAF "FITS" cards to header.
          call IRAF_Cards(newhead,crvl,cdlt,tnp)
C Write out image.
          print '(2a)',' Writing: ',newfile(1:lc(newfile))
          call cheadset('FILENAME',newfile,newhead)
          call writefits(newfile,ae,newhead,ok)
          if (.not.ok) goto 860
        endif
      endif

      print *,'All done.'
      stop
800   continue
      print *,'Error reading WV header card.'
      stop
850   print *,'Error- filename must have .fits extension.'
      stop
860   print *,'Error reading or writing FITS file.'
      stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
C Find flux (per unit original bin) between the pixel values pix1 and pix2.
C
      subroutine linear_fluxave(a,sc,ec,sr,er,row,pix1,pix2,
     .  bval,nozero,lgi)
C
      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er),bval
      real*8 pix1,pix2
      logical nozero,lgi,more
      real*8 Y0,Ym,Yp,x1,x3,sum,wsum,area,b0,b1
      integer*4 i,i0,i1,i2

C Check for zero pixels.
      if (nozero) then
        more = .true.
        do i=nint(pix1),nint(pix2)
          if (abs(a(i,row)).lt.1.e-30) more = .false.
        enddo
        if (.not.more) then
          bval = 0.
          return
        endif
      endif
C Sum and weight.
      sum = 0.d0
      wsum= 0.d0
C Whole pixels.
      i1 = nint(pix1) + 1
      i2 = nint(pix2) - 1
      if (i2.ge.i1) then
        do i=i1,i2
          sum = sum + dble(a(i,row))
          wsum= wsum+ 1.d0
        enddo
      endif

      IF (lgi) THEN

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))
      Ym = dble(a(max(sc,i0-1),row))
      Yp = dble(a(min(ec,i0+1),row))
      x1 = dfloat(i0) - 1.d0
      x3 = dfloat(i0) + 1.d0
      call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
      sum = sum + area
      wsum= wsum+ (b1-b0)
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))
        Ym = dble(a(max(sc,i0-1),row))
        Yp = dble(a(min(ec,i0+1),row))
        x1 = dfloat(i0) - 1.d0
        x3 = dfloat(i0) + 1.d0
        call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
        sum = sum + area
        wsum= wsum+ (b1-b0)
      endif

      ELSE

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))
      sum = sum + (b1-b0)*Y0
      wsum= wsum+ (b1-b0)
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))
        sum = sum + (b1-b0)*Y0
        wsum= wsum+ (b1-b0)
      endif

      ENDIF

C Average flux per unit original bin.
      bval = sngl( sum / wsum )

      return
      end


C----------------------------------------------------------------------
C Find flux error (per unit original bin) between the pixel values pix1 and
C pix2.  The variance is used in calculating the new error.
C
C The simple derivation of the error on  F = sum( Wi Ai ) / sum( Ai ) would be
C   var(F) = sum( Wi * Wi * var(Ai) ) / [ sum( Wi ) * sum( Wi ) ] , however,
C in cases of fractional pixels this underestimates the error since it neglects
C the correlation between adjacent bins.  We use the following equation:
C   var(F) = sum( Wi * var(Ai) ) / [ sum( Wi ) * sum( Wi ) ] , which is
C incorrect but probably a better estimate of the true error. Note that in
C the case of whole pixels the equations are the same.
C
      subroutine linear_errave(a,sc,ec,sr,er,row,pix1,pix2,bval,lgi)
      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er),bval
      real*8 pix1,pix2
      logical lgi,more
      real*8 Y0,Ym,Yp,x1,x3,sum,wsum,area,b0,b1,wt
      integer*4 i,i0,i1,i2

C Check for zero or negative pixels.
      more = .true.
      do i=nint(pix1),nint(pix2)
        if (a(i,row).lt.1.e-30) more = .false.
      enddo
      if (.not.more) then
        bval = -1.
        return
      endif
C Sum and weight.
      sum = 0.d0
      wsum= 0.d0
C Whole pixels.
      i1 = nint(pix1) + 1
      i2 = nint(pix2) - 1
      if (i2.ge.i1) then
        do i=i1,i2
          sum = sum + (dble(a(i,row))*dble(a(i,row)))
          wsum= wsum+ 1.d0
        enddo
      endif

      IF (lgi) THEN

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))*dble(a(i0,row))
      Ym = dble(a(max(sc,i0-1),row))*dble(a(max(sc,i0-1),row))
      Yp = dble(a(min(ec,i0+1),row))*dble(a(min(ec,i0+1),row))
      x1 = dfloat(i0) - 1.d0
      x3 = dfloat(i0) + 1.d0
      call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
      sum = sum + area
      wsum= wsum+ (b1-b0)
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))*dble(a(i0,row))
        Ym = dble(a(max(sc,i0-1),row))*dble(a(max(sc,i0-1),row))
        Yp = dble(a(min(ec,i0+1),row))*dble(a(min(ec,i0+1),row))
        x1 = dfloat(i0) - 1.d0
        x3 = dfloat(i0) + 1.d0
        call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
        sum = sum + area
        wsum= wsum+ (b1-b0)
      endif

      ELSE

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))*dble(a(i0,row))
      wt = (b1-b0)
      sum = sum + wt*Y0
      wsum= wsum+ wt
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))*dble(a(i0,row))
        wt = (b1-b0)
        sum = sum + wt*Y0
        wsum= wsum+ wt
      endif

      ENDIF

C Average flux error per unit original bin.
      bval = sngl( sqrt( sum ) / wsum )

      return
      end


C----------------------------------------------------------------------
C Given three bin fluxes, Ym,Y0,Yp, and the x values at the center of
C the outer bins, x1,x3, find the flux or area between two x boundary
C values, b0 and b1. These boundaries should be contained within the limits
C of the middle bin (inclusive.)
C
C This routine uses lgi interpolation (i.e. polynomial interpolation) by
C finding a parabola which matches the area of the three bins across each bin.
C
C Imagine the center of the bins being at x = -1.0, 0.0, and +1.0, neglecting
C any non-linearity from x1 through x3, and the three point fluxes are
C Ym, Y0, and, Yp.  The solution to a parabola which goes through the three
C points ( y = c0 + c1*x + c2*x*x ) would be c0 = Y0 , c1 = (Yp-Ym)/2, and
C c2 = (Ym+Yp)/2 - Y0 .
C
C However, we want a parabola such that the area under the curve within the
C region of each bin matches the flux in that bin.  The solution to such a
C parabola ( y = a0 + a1*x + a2*x*x ) would be:  a1 = (Yp-Ym)/2 = c1 , 
C a2 = (Ym+Yp)/2 - Y0 = c2, and  a0 = Y0 - (a2/12) = c0 - (c2/12) .
C
      subroutine lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)

      implicit none
      real*8 Ym,Y0,Yp,x1,x3,b0,b1,area
      real*8 a0,a1,a2,n0,n1

C Coeffecients.
      a1 = (Yp-Ym)/2.d0
      a2 = ((Ym+Yp)/2.d0) - Y0
      a0 = Y0 - (a2/12.d0)

C Boundaries in terms of new x coordinates.
      n0 = ( 2.d0*(b0-x1)/(x3-x1) ) - 1.d0
      n1 = ( 2.d0*(b1-x1)/(x3-x1) ) - 1.d0

C Check (temporary?).
      if ((n1.lt.n0).or.(n0.lt.-0.501).or.(n1.gt.0.501)) then
        print *,'ERROR in lgi_align -- bad limits.'
        call exit(1)
      endif

C Integrate polynomial between n0 and n1.
      area = ( (a0*n1) + (a1*n1*n1/2.d0) + (a2*n1*n1*n1/3.d0) ) 
     .     - ( (a0*n0) + (a1*n0*n0/2.d0) + (a2*n0*n0*n0/3.d0) )

      return
      end


C----------------------------------------------------------------------
C Add cards to make this image compatible with IRAF WCS Multispec format.

      subroutine IRAF_Cards(header,crvl,cdlt,tnp)

      implicit none
      character*(*) header
      real*8 crvl(*),cdlt(*)
      integer*4 tnp(*)
      character s*5760, c12*12, wrd*200, c8*8
      integer*4 nr,inhead,row,i,k,lc,ptr,nc
C$$$$$$$$$$$
      real*8 zero,disp,aa,bb
      logical loglin,nwinit
      common /NWB/ zero,disp,aa,bb,loglin,nwinit
C$$$$$$$$$$$
      if (loglin) then
        call inheadset('DC-FLAG',1,header)
      endif
      nc = inhead('NAXIS1',header)
      nr = inhead('NAXIS2',header)
C Not for IRAF, just adding easy to read cards.
      do row=1,nr
        write(c8,'(a,i2.2)') 'CRVL1_',row
        call fheadset(c8,crvl(row),header)
        write(c8,'(a,i2.2)') 'CDLT1_',row
        call fheadset(c8,cdlt(row),header)
      enddo
C Must take out CRPIX cards for IRAF to work.
      call unfit('CRPIX1',header)
      call unfit('CRPIX2',header)
C This is for IRAF.
      call inheadset('WCSDIM',2,header)
      call cheadset('CTYPE1','MULTISPE',header)
      call cheadset('CTYPE2','MULTISPE',header)
      call fheadset('CD1_1',1.d0,header)
      call fheadset('CD2_2',1.d0,header)
      call fheadset('LTM1_1',1.d0,header)
      call fheadset('LTM2_2',1.d0,header)
C Note we need to use cheadset_fill, otherwise IRAF doesn't work.
      call cheadset_fill('WAT0_001','system=multispec',header)
      call cheadset_fill('WAT1_001',
     .        'wtype=multispec label=Wavelength units=Angstroms',header)
      s = ' '
      s = 'wtype=multispec'
      do row=1,nr
        if (row.gt.99) then
          write(c12,'(a,i3,a)') ' spec',row,' = "'
        elseif (row.gt.9) then
          write(c12,'(a,i2,a)') ' spec',row,' = " '
        else
          write(c12,'(a,i1,a)') ' spec',row,' = "  '
        endif
        k=nc
        if (tnp(row).gt.0) k=tnp(row)
        if (loglin) then
          write(wrd,'(a,i3,i3,i2,f18.11,f18.15,i6,f3.0,2f6.1,a)')
     .       c12(1:lc(c12)),row,row,1,crvl(row),cdlt(row),k,0.,
     .       float(row),float(row),'"'
        else
          write(wrd,'(a,i3,i3,i2,f18.11,f18.15,i6,f3.0,2f6.1,a)')
     .       c12(1:lc(c12)),row,row,0,crvl(row),cdlt(row),k,0.,
     .       float(row),float(row),'"'
        endif
        s = s(1:lc(s))//wrd(1:lc(wrd))
      enddo
      k = lc(s)
      ptr = 1
      i=0
      do while(ptr.le.k)
        i=i+1
        write(c8,'(a,i3.3)') 'WAT2_',i
C Note we need to use cheadset_fill, otherwise IRAF doesn't work.
        call cheadset_fill(c8,s(ptr:ptr+67),header)
        ptr=ptr+68
      enddo
      return
      end

C----------------------------------------------------------------------
C Set starting wavelength and dispersion.
      subroutine set_wave_disp(wave,nwp,crval1,cdelt1,crvl,cdlt,row)
      implicit none
      real*8 wave,crval1,cdelt1,crvl(*),cdlt(*),nwp
      integer*4 row
C$$$$$$$$$$$
      real*8 zero,disp,aa,bb
      logical loglin,nwinit
      common /NWB/ zero,disp,aa,bb,loglin,nwinit
C$$$$$$$$$$$
      if (loglin) then
        crval1 = aa + (bb*(nwp-1.d0))
        cdelt1 = bb
      else
        crval1 = wave
        cdelt1 = disp
      endif
      crvl(row) = crval1
      cdlt(row) = cdelt1
      return
      end

C----------------------------------------------------------------------
      real*8 function NewWave(pix)
      implicit none
      real*8 pix
C$$$$$$$$$$$
      real*8 zero,disp,aa,bb
      logical loglin,nwinit
      common /NWB/ zero,disp,aa,bb,loglin,nwinit
C$$$$$$$$$$$
      real*8 c,ln10
      parameter(c=2.99792458d+5)
      parameter(ln10=2.302585093d0)
      if (.not.nwinit) then
        if (loglin) then
          aa = log10(zero)
          bb = disp / (c*ln10)
        endif
        nwinit=.true.
      endif
      if (loglin) then
        NewWave = 10.d0 ** ( aa + ( bb * (pix - 1.d0) ) )
      else
        NewWave = zero + ( disp * pix ) 
      endif
      return
      end
      
