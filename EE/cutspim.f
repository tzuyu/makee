C cutspim.f                                                    tab  July95
C
C Cut (or window) a spectrum-in-an-image FITS file (output from EE.)
C
      implicit none
      integer*4 maxpix
      parameter(maxpix=5290000)  
      integer*4 i,narg,inhead,lc,nc,nr,j,new_sr,new_er,row
      integer*4 sc,ec,sr,er
      real*4 a(maxpix),e(maxpix)
      real*4 valread
      character*115200 header,eheader
      character*80 file,arg(9),wrd,efile,dfile,chead,wrds(9)
      character*80 co(99,2),wv(99,2)
      character c8*8
      logical ok,findarg,have_co,have_wv,card_exist,he
C
      include 'soun.inc'
C
C Standard output unit number.
      soun=6
C
C Command line arguments.
      call arguments(arg,narg,9)
      new_sr = 0
      new_er = 0
      if (findarg(arg,narg,'rows=',':',wrds,i)) then
        if (i.eq.2) then
          new_sr = nint(valread(wrds(1)))
          new_er = nint(valread(wrds(2)))
        endif
      endif
      efile = ' '
      if (findarg(arg,narg,'ef=',':',wrd,i)) efile = wrd

C Syntax.
      if ((narg.ne.1).or.(new_er.eq.0)) then
        print *,
     .  'Syntax:  cutspim  (FITS file)  rows=(first row):(last row) '
        print *,'                       [ef=filename] '
        print *,'  Window a 2-D FITS file (output from EE).'
        print *,'    rows= : Specify first and last row for new image.'
        print *,'    ef=   : Error FITS file.'
        print *,'  THIS WILL OVERWRITE ORIGINAL FILES.'
        call exit(0)
      endif

C Read files.
      dfile = arg(1)
      call AddFitsExt(dfile)
      call readfits(dfile,a,maxpix,header,ok)
      if (.not.ok) goto 801
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
      he = (efile.ne.' ')
      if (he) then
        call AddFitsExt(efile)
        call readfits(efile,e,maxpix,eheader,ok)
        if (.not.ok) goto 802
        i = max(1,inhead('NAXIS1',eheader))
        j = max(1,inhead('NAXIS2',eheader))
        if ((i.ne.nc).or.(j.ne.nr)) goto 910
      endif
      print '(a,4i5)',
     .  ' Old dimensions (sc,ec,sr,er) : ',sc,ec,sr,er
      print '(a,4i5)',
     .  ' New dimensions (sc,ec,sr,er) : ',sc,ec,new_sr,new_er
      if ( (new_sr.lt.sr).or.(new_sr.gt.er).or.
     .     (new_er.lt.sr).or.(new_er.gt.er).or.
     .     (new_er.lt.new_sr) ) then
        print *,'Error specifying new row range.'
        call exit(1)
      endif

C First store all the trace and wavelength polynomial coeffecients.
C Remove cards.
      write(c8,'(a,i2.2)') 'CO_0_',sr
      have_co = card_exist(c8,header)
      if (have_co) then
        do row=sr,er
          write(c8,'(a,i2.2)') 'CO_0_',row
          co(row,1) = chead(c8,header)
          call unfit(c8,header)
          if (he) call unfit(c8,eheader)
          write(c8,'(a,i2.2)') 'CO_4_',row
          co(row,2) = chead(c8,header)
          call unfit(c8,header)
          if (he) call unfit(c8,eheader)
        enddo
      endif
      write(c8,'(a,i2.2)') 'WV_0_',sr
      have_wv = card_exist(c8,header)
      if (have_wv) then
        do row=sr,er
          write(c8,'(a,i2.2)') 'WV_0_',row
          wv(row,1) = chead(c8,header)
          call unfit(c8,header)
          if (he) call unfit(c8,eheader)
          write(c8,'(a,i2.2)') 'WV_4_',row
          wv(row,2) = chead(c8,header)
          call unfit(c8,header)
          if (he) call unfit(c8,eheader)
        enddo
      endif

C Now window the image.
      call window_fits(a,header,sc,ec,new_sr,new_er,ok)

C Window error image.
      if (he) call window_fits(e,eheader,sc,ec,new_sr,new_er,ok)

C Force rows to start at 1.  Shift new_sr to 1.
      call inheadset('CRVAL2',1,header)
      if (he) call inheadset('CRVAL2',1,eheader)

C Put cards back in.
      if (have_co) then
        do row=new_sr,new_er
          write(c8,'(a,i2.2)') 'CO_0_',1+row-new_sr
          call cheadset(c8,co(row,1),header)
          if (he) call cheadset(c8,co(row,1),eheader)
          write(c8,'(a,i2.2)') 'CO_4_',1+row-new_sr
          call cheadset(c8,co(row,2),header)
          if (he) call cheadset(c8,co(row,2),eheader)
        enddo
      endif
      if (have_wv) then
        do row=new_sr,new_er
          write(c8,'(a,i2.2)') 'WV_0_',1+row-new_sr
          call cheadset(c8,wv(row,1),header)
          if (he) call cheadset(c8,wv(row,1),eheader)
          write(c8,'(a,i2.2)') 'WV_4_',1+row-new_sr
          call cheadset(c8,wv(row,2),header)
          if (he) call cheadset(c8,wv(row,2),eheader)
        enddo
      endif

C Write files.
      print '(2a)',' Writing : ',dfile(1:lc(dfile))
      call writefits(dfile,a,header,ok)
      if (.not.ok) goto 801
      if (he) then
        print '(2a)',' Writing : ',efile(1:lc(efile))
        call writefits(efile,e,header,ok)
        if (.not.ok) goto 801
      endif

      stop
801   print *,'Error writing/reading FITS image: ',file(1:lc(file))
      stop
802   print *,'Error writing/reading FITS image: ',efile(1:lc(efile))
      stop
910   print *,
     .  'Error-- dimensions of error array do not match data array.'
      stop
      end

