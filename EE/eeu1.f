C eeu1.f                                tab  1995-1999

CENDOFMAIN

C----------------------------------------------------------------------
C MAuna-kea Keck Echelle Extraction for the HIRES CCD spectrograph.
C
C For 1x2 binning set size to >= 2303 x 1024 ~= 2360000.
C For 1x1 binning set size to >= 2303 x 2048 ~= 4720000.
C
C For current maximum image size see maxpix.inc file.
C Maximum image size is set by maxpix parameter in "makee_big.inc".
C
      subroutine makee(arg,narg,ok)
C
      implicit none
      character*(*) arg(*)
      integer*4 narg
      logical ok
C
      include 'soun.inc'
C
      include 'makee.inc'
C
      include 'scratch.inc'
C
      include 'global.inc'
C
C Allocates big images, sets "maxpix", "maxcol", and "maxord".
C Sets the big arrays: flat(maxpix) and obj(maxpix).
C
      include 'makee_big.inc'
C
      real*4 sky(maxpixsub),prof(maxpixsub)      ! used in PSE for extraction.
C
      real*4 aao(maxcol*maxord),aas(maxcol*maxord)
      real*4 aav(maxcol*maxord),aasum(maxcol*maxord)
      real*4 tbl_Flux(maxcol*maxord),tbl_Var(maxcol*maxord)
      real*4 tbl_Flat(maxcol*maxord),tbl_Sky(maxcol*maxord)
      real*4 tbl_Sum(maxcol*maxord),tbl_Arc(maxcol*maxord)
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8
C
      real*4 objsp(maxcol),skysp(maxcol),varsp(maxcol)
      real*4 sumsp(maxcol),bias(maxcol)
      real*4 pd_xp(MK_mpv,mo),pd_yp(MK_mpv,mo),pd_zp(MK_mpv,mo)
C
      real*4 eperdn,ronoise,scale_factor
      real*4 valread,r1,r2,sw_flat_lookup,orig_scale_factor,usw
      real*4 UserEperdn,UserRonoise
      real*4 koa_global(100,100)
C
      integer*4 sc,sr,nc,nr,ec,er,lc,firstchar,bias_sc,bias_ec
      integer*4 inhead,pse_mode,access,orig_pse_mode,Flux_Obsnum
      integer*4 ii,jj,nn,fftimeget,eon,i,j,nueons,Pass,esiwaveapply
      integer*4 xbin,ybin,ueons(99),ntrg,usc,uec,ccdloc
      integer*4 pd_n(mo)
C
      character*115200 header,temphead
      character*5760 parmhead
      character*200 c200
      character*80 trg(99)
      character*80 objfile,starfile,flatfile,file
      character*80 biasfile,saveflatfile
CCC   character*80 savestarfile
      character*80 arcfile1,arcfile2,arc1add,arc2add
      character*80 wrd,parmfile,CommentString,wrds(99),object,chead
      character*80 wrd1,wrd2,fluxspfile,arcspfile1,arcspfile2
      character*80 arcspfile1a,arcspfile2a
      character*80 varspfile,skyspfile,flatspfile
      character*80 sumspfile,newobject,s2nspfile
      character*80 rawfile,rawdir,dnspfile
      character*80 observer,printer,logfile
      character*8 ExtractionType
      character fdate*24,c10*10,c7*7
C
      logical whole_thing,findarg,test,nozero,DoSum,DoAtm,nox
      logical Do_This_Order,noskyshift,DoWave,nobias,lesspr
      logical card_exist,options,NoHC,NoVac
      logical SetNewFilename_HIRES2
      logical KOA_PRODUCTS
C
C profval  = profile model.
C profparm = profile parameters: 1=num of pts, 2=first x value, 3=x increment
C                                4=centroid,  5=background
      real*4 profval(MK_mpv),profparm(5)
      real*8 Tsp1(mpp+1),Tsp2(mpp+1)
      real*8 Tbk1o(mpp+1),Tbk2o(mpp+1)
      real*8 Tbk1i(mpp+1),Tbk2i(mpp+1)
C
      real*8 fhead,user_shift,valread8,xc,polyarroff
      integer*4 eoc(99)
C
      real*8 tbl_sp(0:mpp,mo),tbl_eo(0:mpp,mo),rr8    !  table echelle orders -tab 16nov07
      integer*4 tbl_mc(4)
      integer*4 tsc,tec,tsr,ter,tnc,tnr,koatblproducts
C
C#@#  real*8 old_row,old_col
C#@#  integer*4 naxis1,naxis2,pp
C

C Defaults.
      DoSum        = .true.
      UserEperdn   = -1.
      UserRonoise  = -1.
      printer      = ' '
      pse_mode     = 1
      user_shift   = -1.d+30
      usw          = 0.
      UserHalfWidth= 0.
      parmfile     = ' '
      biasfile     = ' '
      rawdir       = ' '
      newobject    = ' '
      observer     = ' '
      logfile      = ' '
      BlueOrders   = 909
      do ii=1,mo
        oko(ii) =.true.
      enddo
      Global_ccdloc= 0
      Global_HIRES2= .false.
      Global_HIRES = .false.
      Global_ESI   = .false.
      KOA_PRODUCTS = .false.

C Output log file.
      if (findarg(arg,narg,'log=',':',wrd,i)) then
        logfile=wrd
        open(42,file=logfile,status='unknown')
        soun=42
      else
        soun = 6
      endif
C Grab optional parameters.
      if (findarg(arg,narg,'observer=','A',wrd,i)) observer = wrd
      call tilda_to_blank(observer)
      if (findarg(arg,narg,'object=','A',wrd,i)) newobject = wrd
      call tilda_to_blank(newobject)
      if (findarg(arg,narg,'raw=','A',wrd,i)) 
     .  rawdir = wrd(1:lc(wrd))//'/'
      if (findarg(arg,narg,'eperdn=','A',wrd,i)) 
     .  UserEperdn=valread(wrd)
      if (findarg(arg,narg,'ronoise=','A',wrd,i)) 
     .  UserRonoise=valread(wrd)
      if (findarg(arg,narg,'lpr=','A',wrd,i)) printer=wrd
      lesspr = findarg(arg,narg,'-lesspr','A',wrd,i)
      if (findarg(arg,narg,'mode=','A',wrd,i)) then
        pse_mode = max(1,min(4,nint(valread(wrd))))
      endif
      if (findarg(arg,narg,'skyshift=','A',wrd,i)) 
     .  user_shift = valread8(wrd)
      if (findarg(arg,narg,'BlueOrders=','A',wrd,i)) 
     .  BlueOrders = nint(valread(wrd))
C CCDLOC (CCD location or mosaic number) for raw HIRES2 FITS file.
      if (findarg(arg,narg,'ccdloc=','A',wrd,i)) 
     .                  Global_ccdloc=valread(wrd)
C CCDLOC by CCD color.
      if (findarg(arg,narg,'ccd=','A',wrd,i)) then
        if (index(wrd,'blue' ).gt.0) Global_ccdloc = 1
        if (index(wrd,'green').gt.0) Global_ccdloc = 2
        if (index(wrd,'red'  ).gt.0) Global_ccdloc = 3
        if (Global_ccdloc.eq.0) then
          if (wrd.eq.'b') Global_ccdloc = 1
          if (wrd.eq.'g') Global_ccdloc = 2
          if (wrd.eq.'r') Global_ccdloc = 3
        endif
        if (Global_ccdloc.eq.0) then
          if (wrd.eq.'1') Global_ccdloc = 1
          if (wrd.eq.'2') Global_ccdloc = 2
          if (wrd.eq.'3') Global_ccdloc = 3
        endif
      endif
C Slit length.
      if (findarg(arg,narg,'sl=','A',wrd,i)) usw=valread(wrd)
C User object half width.
      if (findarg(arg,narg,'hw=','A',wrd,i)) then
        r1=valread(wrd)
        if ((r1.lt.1.).or.(r1.gt.99.)) then
          write(soun,'(a)') 
     .  'ERROR:  hw= value must be between 1. and 99.'
          ok=.false.
          return
        endif
        UserHalfWidth=r1
      endif
C Arc lamp wavelength scale averaging mode.
      arcmode = 0
      if (findarg(arg,narg,'arcmode=','A',wrd,i))
     .  arcmode=nint(valread(wrd))
C Additional arc lamps.
      arc1add = ' '
      if (findarg(arg,narg,'arc1add=','A',wrd,i))arc1add=wrd(1:lc(wrd))
      arc2add = ' '
      if (findarg(arg,narg,'arc2add=','A',wrd,i))arc2add=wrd(1:lc(wrd))
C Parameter file.
      if (findarg(arg,narg,'pf=','A',wrd,i)) parmfile=wrd(1:lc(wrd))
C Bias file.
      if (findarg(arg,narg,'bias=','A',wrd,i)) biasfile=wrd(1:lc(wrd))
C Which orders to extract.
      nueons = 0
      if (findarg(arg,narg,'order=',',',wrds,i)) then
        nueons = i
        do i=1,nueons
          ueons(i) = nint(valread(wrds(i)))
        enddo
      endif
C Orders to use for centroid trace offsets.
      if (findarg(arg,narg,'oko=',',',wrds,nn)) then
        do ii=1,mo
          oko(ii) = .false.
        enddo
        do ii=1,nn
          jj = nint(valread(wrds(ii)))
          if ((jj.ge.1).and.(jj.le.mo)) oko(jj)=.true.
        enddo
      endif
C Load User Object Postions if requested.
      uop_exist = .false.
      if (findarg(arg,narg,'uop=',':',wrd,i)) then
        uopfile = wrd
        call EE_load_uop()
      endif
C Comment string.
      if (findarg(arg,narg,'comm=','A',wrd,i)) then
        CommentString = wrd
      else
        CommentString = ' '
      endif
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8

C Switches.
      KOA_PRODUCTS= findarg(arg,narg,'-koa_products','A',wrd,i).or.
     .              findarg(arg,narg,'-koa','A',wrd,i)
      whole_thing = (.not.findarg(arg,narg,'-notwhole','A',wrd,i))
      test        = findarg(arg,narg,'-test','A',wrd,i)
      DoAtm       = findarg(arg,narg,'-atm','A',wrd,i)
      nox         = findarg(arg,narg,'-nox','A',wrd,i)
      NoVac       = findarg(arg,narg,'-novac','A',wrd,i)
      NoHC        = findarg(arg,narg,'-nohc','A',wrd,i)
      nozero      = (.not.findarg(arg,narg,'-showmask','A',wrd,i))
      noskyshift  = findarg(arg,narg,'-noskyshift','A',wrd,i)
      trace       = findarg(arg,narg,'-trace','A',wrd,i)
      nobias      = findarg(arg,narg,'-nobias','A',wrd,i)
      options     = findarg(arg,narg,'-options','A',wrd,i).or.
     .              findarg(arg,narg,'-help','A',wrd,i)

C Show plot of profile polynomial fits.
      if (findarg(arg,narg,'-profpoly','A',wrd,i)) then
        call PSE_SetProfpoly(.true.)
      else
        call PSE_SetProfpoly(.false.)
      endif

C Check parameters, give syntax.
      if ((narg.lt.2).or.(narg.gt.5).or.(options)) then
        print *,'Syntax: makee (object file) (star file) (flat file)'
        print *,'                    [arc file]  [2nd arc file]'
        print *,' '
        print *,
     . '   [arc1add=(FITS file)]  [arc2add=(FITS file)]  [arcmode=]'
        print *,'   [mode=]  [raw=]  [bias=(file)]  [-nobias]  [lpr=]'
        print *,
     . '   [eperdn=]  [ronoise=]  [pf=(file)]  [order=n,n,..] [comm=s]'
        print *,
     . '   [sl=] [hw=] [-atm] [-novac] [-noskyshift] [skyshift=]'
        print *,
     . '   [-nohc]  [-showmask]  [object=]  [observer=]  [log=]'
        print *,'   [oko=n,n,...]  [-nox]  [uop=]  [ccd=]'
        print *,'   [ BlueOrders= ]  [ -koa | -koa_products ]'
        print *,' '
        print *,'   [-options] (show description of options)'
        print *,'   [-help   ] (show description of options)'
        print *,' '
        if (options) then
        print *,' object file : Object FITS file to be reduced.'
        print *,' star file   : Bright star FITS file to define trace.'
        print *,' flat file   : Flat field FITS file.'
        print *,' arc file    : Arclamp FITS file (optional).'
        print *,' 2nd arc file: Second Arclamp FITS file (optional).'
        print *,' '
        print *,
     . ' mode=      : 1 = Full extraction (default).'
        print *,
     . '              2 = Mask CRs but do straight sum of object.'
        print *,
     . '              3 = Mask CRs in sky only,straight sum of object.'
        print *,
     . '              4 = Median of whole order.'
        print *,' '
        print *,' raw=dir   : Directory path for raw FITS files.'
        print *,
     . '            (You can also set environment variable MAKEERAW,'
        print *,
     .  '               or do "ln -s (directory name) raw".)'
        print *,
     .  ' bias=file : FITS filename of bias (short dark) image.'
        print *,' -nobias   : Do not apply bias correction.'
        print *,
     .  ' pf=file   : Use this parameter file instead of "makee.param".'
        print *,
     .  'order=n,n,.. : Only extract orders n,n,.. where the bluest'
        print *,
     .  'oko=n,n,..   : Only use these orders for trace centroids.'
        print *,
     .  'uop=file     : Load User Object Positions data file.'
        print *,
     .  '      -nox : No Extraction (exit after profile computation).'
        print *,
     .  ' BlueOrders=  : Use this many orders for HIRES2 CCD#1 .'
        print *,
     .  '      ccd= : CCD location number for HIRES2 data.'
        print *,
     .  '             (This can be 1, 2, or 3, or blue, green, or red,'
        print *,
     .  '              or b, g, or r.)'
        print *,
     .  '  -showmask: Set pixels in masked (bad) regions to zero in'
        print *,
     .  '             reduced spectra. These regions will always appear'
        print *,
     .  '             as -1.0 in the variance spectra.'
        print *,
     .  '  comm=s   : Add the comment "s" to each output spectrum.'
        print *,
     .  '  sl=v     : Give estimate of slit length in pixels.'
        print *,
     .  '  hw=v     : Give object half width in pixels explicitly.'
        print *,
     .  '  lpr=     : Send output to this printer.'
        print *,
     .  ' -lesspr   : Send less output to printer (just Flux-*).'
        print *,
     .  ' -atm      : Apply atmospheric extinction correction.'
        print *,
     . ' -novac     : No vacuum wavelength scale correction (air wave.)'
        print *,
     . ' -nohc      : No heliocentric wavelength correction.'
        print *,
     .  '-noskyshift: Do not apply automated skyline shifted scale.'
        print *,
     .  ' skyshift=v: Override automated skyline wavelength scale '
        print *,
     .  '             shift, set shift to v pixels.'
        print *,
     .  '  object=  : Reset "OBJECT" name to this string (~=blank).'
        print *,
     .  ' observer= : Reset "OBSERVER" name to this string (~=blank).'
        print *,
     .  '    log=   : Filename for log file for output messages.'
        print *,
     .  ' arc1add=  : Arclamp exposure to provide additional lines to'
        print *,
     .  '             wavelength calibration of first arclamp.'
        print *,
     .  ' arc2add=  : Arclamp exposure to provide additional lines to'
        print *,
     .  '             wavelength calibration of second arclamp.'
        print *,
     .  ' arcmode=  : Arclamp wavelength scale averaging mode:'
        print *,
     .  '                0 : Average scales of 1st,2nd arclamps(def.).'
        print *,
     .  '                1 : Shift scale to 1st arclamp.'
        print *,
     .  '                2 : Shift scale to 2nd arclamp.'
        print *,
     .  '  eperdn=  : Inverse gain (electrons per digital number).'
        print *,
     .  '  ronoise= : Readout noise in electrons.'
        print *,
     .  '   { If "eperdn=" or "ronoise=" is not given, use (1) values'
        print *,
     .  '     in header, (2) values in "eperdn.dat" and "ronoise.dat",'
        print *,
     .  '     or (3) default values.'
        print *,
     .  ' '
        print *,
     .  'The following are usually only used for testing purposes:'
        print *,
     .  '  -profpoly : Write out profile polynomial fit plots.'
        print *,
     .  '  -test     : Write out prior/post masking test images.'
        print *,
     .  '  -notwhole : Read ee.get file for trace information.'
        print *,
     .  '  -trace    : General testing...'
        endif
        print '(2a)','              MAKEE VERSION= ',MK_VERSION
CCC     print '(2a)','                 EE_VERSION= ',EE_VERSION
        ok=.false.
        return
      endif


      write(soun,'(2x)')
      write(soun,'(2a)') 
     . '........ MAKEE: Keck Echelle Reduction Program ....... '
      write(soun,'(2x)')

C Set "raw" directory path.
      if (rawdir.eq.' ') then
        call getenv('MAKEERAW',wrd)
        if (wrd.ne.' ') then
          rawdir = wrd(1:lc(wrd))//'/'
        elseif (access('raw','r').eq.0) then
          rawdir = './raw/'
        else
          rawdir = './'
        endif
      endif

C .... Check for HIRES2 data ....
C Set object file filename.
      rawfile = arg(1)
      call SetRawFilename(rawfile,rawdir,ok)
C Is this the HIRES2 (2004) mosaic CCD instrument?
      call readfits_header_rawhires2(rawfile,header,ok) 
      wrd = chead('DETECTOR',header)
      if (index(wrd,'Mosaic').gt.0) Global_HIRES2 = .true.
      if (.not.Global_HIRES2) then
        if (card_exist('TEMPDET3',header)) Global_HIRES2 = .true.
      endif
      if (Global_HIRES2) then
        write(soun,'(a)') 'This is a HIRES2 raw FITS file.'
        if (Global_ccdloc.eq.1) wrd='blue'
        if (Global_ccdloc.eq.2) wrd='green'
        if (Global_ccdloc.eq.3) wrd='red'
        write(soun,'(a,i2,3a)') 
     .  '  Use CCD mosaic location (CCDLOC) number is ',
     .   Global_ccdloc,' (',wrd(1:lc(wrd)),')'
      else 
        write(soun,'(a)') 'This is not a HIRES2 raw FITS file.'
      endif

C Process HIRES2 raw file.
      if (Global_HIRES2) then

        if ((Global_ccdloc.lt.1).or.(Global_ccdloc.gt.3)) then
          write(soun,'(2a)') 
     .    'ERROR: makee: Must specify ccdloc=1, 2, or 3 (or ccd',
     .    '=blue,green,red) for a HIRES2 raw FITS file.'
          ok=.false.
          return
        endif

C Set variable.
        ccdloc = Global_ccdloc

C Object file.
        rawfile = arg(1)
        call SetRawFilename(rawfile,rawdir,ok)
        if(SetNewFilename_HIRES2(rawfile,ccdloc,temphead,arg(1)))then
          call cappendnull(rawfile,lc(rawfile))
          write(soun,'(5a)') 'Read raw object file ',
     .      rawfile(1:lc(rawfile)),', write ',arg(1)(1:lc(arg(1))),' .'
          ii = 0
          jj = 1
          call ffchires2readwrite(rawfile,ccdloc,ii,jj)
        else
          write(soun,'(3a)') 'Object file ',arg(1)(1:lc(arg(1))),
     .     ' is already a single CCD file, no need to re-write file.'
        endif

C Star file.
        rawfile = arg(2)
        call SetRawFilename(rawfile,rawdir,ok)
        if(SetNewFilename_HIRES2(rawfile,ccdloc,temphead,arg(2)))then
          call cappendnull(rawfile,lc(rawfile))
          write(soun,'(5a)') 'Read raw star file  ',
     .      rawfile(1:lc(rawfile)),', write ',arg(2)(1:lc(arg(2))),' .'
          ii = 0
          jj = 1
          call ffchires2readwrite(rawfile,ccdloc,ii,jj)
        else
          write(soun,'(3a)') 'Star file ',arg(2)(1:lc(arg(2))),
     .     ' is already a single CCD file, no need to re-write file.'
        endif

C Flat file.
        rawfile = arg(3)
        call SetRawFilename(rawfile,rawdir,ok)
        if(SetNewFilename_HIRES2(rawfile,ccdloc,temphead,arg(3)))then
          call cappendnull(rawfile,lc(rawfile))
          write(soun,'(5a)') 'Read raw flat file  ',
     .      rawfile(1:lc(rawfile)),', write ',arg(3)(1:lc(arg(3))),' .'
          ii = 0
          jj = 1
          call ffchires2readwrite(rawfile,ccdloc,ii,jj)
        else
          write(soun,'(3a)') 'Flat file ',arg(3)(1:lc(arg(3))),
     .     ' is already a single CCD file, no need to re-write file.'
        endif

C Arc file 1.
        if (narg > 3) then
          rawfile = arg(4)
          call SetRawFilename(rawfile,rawdir,ok)
          if(SetNewFilename_HIRES2(rawfile,ccdloc,temphead,arg(4)))then
            call cappendnull(rawfile,lc(rawfile))
            write(soun,'(5a)') 'Read raw arc1 file  ',
     .      rawfile(1:lc(rawfile)),', write ',arg(4)(1:lc(arg(4))),' .'
            ii = 0
            jj = 1
            call ffchires2readwrite(rawfile,ccdloc,ii,jj)
          else
            write(soun,'(3a)') 'Arc1 file ',arg(4)(1:lc(arg(4))),
     .       ' is already a single CCD file, no need to re-write file.'
          endif
        endif

C Arc file 2.
        if (narg > 4) then
          rawfile = arg(5)
          call SetRawFilename(rawfile,rawdir,ok)
          if(SetNewFilename_HIRES2(rawfile,ccdloc,temphead,arg(5)))then
            call cappendnull(rawfile,lc(rawfile))
            write(soun,'(5a)') 'Read raw arc2 file  ',
     .      rawfile(1:lc(rawfile)),', write ',arg(5)(1:lc(arg(5))),' .'
            ii = 0
            jj = 1
            call ffchires2readwrite(rawfile,ccdloc,ii,jj)
          else
            write(soun,'(3a)') 'Arc2 file ',arg(5)(1:lc(arg(5))),
     .       ' is already a single CCD file, no need to re-write file.'
          endif
        endif

C BIAS file. biasfile
        if (biasfile.ne.' ') then
          rawfile = biasfile
          call SetRawFilename(rawfile,rawdir,ok)
          if(SetNewFilename_HIRES2(
     .                rawfile,ccdloc,temphead,biasfile))then
            call cappendnull(rawfile,lc(rawfile))
            write(soun,'(5a)') 'Read raw BIAS file  ',
     .        rawfile(1:lc(rawfile)),', write ',biasfile(1:lc(biasfile)),
            ii = 0
            jj = 1
            call ffchires2readwrite(rawfile,ccdloc,ii,jj)
          else
            write(soun,'(3a)') 'BIAS file ',biasfile(1:lc(biasfile)),
     .       ' is already a single CCD file, no need to re-write file.'
          endif
        endif
        
      endif
        
C Which instrument?  HIRES[2] or ESI?
      objfile = arg(1)
      call SetRawFilename(objfile,rawdir,ok)
      call readfits_header(objfile,header,ok)
      if (.not.ok) then
        write(soun,'(2a)') 'ERROR: makee: reading FITS file',
     .                objfile(1:lc(objfile))
        ok=.false.
        return
      endif

C This will set Global_HIRES[2] or Global_ESI or print error and abort.
      call EE_CheckInstrument( header )

C Banner with date.
      write(soun,'(2x)') 
      if (Global_HIRES) then
        write(soun,'(2a)') 
     . '... MAKEE: Keck HIRES Reduction .......... ',fdate()
      elseif (Global_HIRES2) then
        write(soun,'(3a)') 
     . '... MAKEE: Keck HIRES2 (Mosaic 20',
     . '04) Reduction .......... ',fdate()
      elseif (Global_ESI) then
        write(soun,'(2a)') 
     . '... MAKEE: Keck ESI Reduction .......... ',fdate()
      else
        write(soun,'(2a)') 
     . '... MAKEE: Keck ??? Reduction .......... ',fdate()
      endif
      write(soun,'(2x)') 
      wrds(1)=MK_VERSION
      wrds(2)=EE_VERSION
      write(soun,'(2a)') 'MAKEE VERSION= ',wrds(1)(1:lc(wrds(1)))
CCC  .          '         EE_VERSION= ',wrds(2)(1:lc(wrds(2)))
      write(soun,'(2x)') 

C Check.
      if (Global_ESI) then
        if (arcmode.ne.0) then
          write(soun,'(a)') '- - - - - - - - - - - - - - - - - - - -'
          write(soun,'(a)')'ERROR: Arcmode must be zero for ESI.'
          write(soun,'(a)')
     .'NOTE: With ESI, arclamps are always averaged.'
          write(soun,'(a)') '- - - - - - - - - - - - - - - - - - - -'
          return
        endif
        if ((arc1add.ne.' ').or.(arc2add.ne.' ')) then
          write(soun,'(a)') '- - - - - - - - - - - - - - - - - - - -'
          write(soun,'(a)') 
     .'ERROR: You cannot specify "arc1add=" or "arc2add=" with ESI.'
          write(soun,'(a)') 
     .'NOTE: With ESI, you should specify two arclamps, one with CuAr'
          write(soun,'(a)') 
     .'NOTE:   and the other with Xe (or Xe + HgNe).'
          write(soun,'(a)') '- - - - - - - - - - - - - - - - - - - -'
          return
        endif
      endif

C Check.
      if ((arcmode.lt.0).or.(arcmode.gt.2)) then
        write(soun,'(2a)') 'ERROR: makee: arcmode must be 0, 1, or 2.'
        ok=.false.
        return
      endif

C Set default parameter file value from MAKEE_DIR environment variable.
      if (parmfile.eq.' ') then
        call GetMakeeHome(wrd)
        parmfile = wrd(1:lc(wrd))//'makee.param'
        if (Global_HIRES) then
          parmfile = wrd(1:lc(wrd))//'makee.HIRES.param'
        endif
        if (Global_HIRES2) then
          parmfile = wrd(1:lc(wrd))//'makee.HIRES2.param'
        endif
        if (Global_ESI) then
          parmfile = wrd(1:lc(wrd))//'makee.ESI.param'
        endif
      endif

C Check the makee.param parameter file.
      if (access(parmfile,'r').ne.0) then
        write(soun,'(2a)') 'ERROR: Cannot access parameter file: ',
     .           parmfile(1:lc(parmfile))
        write(soun,'(a)') 
     .  'You may need to set MAKEE_DIR environment variable.'
        ok=.false.
        return
      endif

C Read parameter file into one big string.
      call ReadParmFile(parmfile,parmhead)

C Set necessary EE parameters.
      call EE_Parameters(parmhead)

C Set necessary PSE parameters.
      call PSE_Parameters(parmhead)

C First pass.
      Pass=1

C Extraction mode.
      orig_pse_mode = pse_mode

C DoSum only works with pse_mode=1.
      if (DoSum.and.(pse_mode.ne.1)) then
        write(soun,'(2a)') 
     .    'Note that the "Sum-###.fits" fi',
     .    'le is only generated with mode=1.'
        DoSum = .false.
      endif

C Clear arrays.
      Global_neo     =0
      Global_sw      =0.
      Global_sw_flat =0.
      Global_sw_adj  =0.
      Global_obsnum  =0
      do i=0,mpp
      do j=1,mo
        eo(i,j)  =0.d0
        sp1(i,j) =0.d0
        sp2(i,j) =0.d0
        bk1o(i,j)=0.d0
        bk2o(i,j)=0.d0
        bk1i(i,j)=0.d0
        bk2i(i,j)=0.d0
      enddo
      enddo
      do i=1,maxcol*maxord
        aao(i)=0.
        aasum(i)=0.
        aas(i)=0.
        aav(i)=0.
      enddo
      do i=1,maxcol
        bias(i)=0.
      enddo
      bias_sc = 0
      bias_ec = 0

C Blank filenames.
      fluxspfile =' '
      flatspfile =' '
      skyspfile =' '
      s2nspfile  =' '
      dnspfile   =' '
      arcspfile1 =' '
      arcspfile2 =' '
      arcspfile1a=' '
      arcspfile2a=' '
      sumspfile  =' '



C...............OBJECT FILE (read header only)..........................

C Set filename of object FITS data file.
      objfile = arg(1)
      call SetRawFilename(objfile,rawdir,ok)
      if (.not.ok) then
       write(soun,'(2a)') 
     .  'ERROR: Could not find object file: ',objfile(1:lc(objfile))
       return
      endif

C Grab the object header to get binning and set maskfile, eperdn, and ronoise.
      call readfits_header(objfile,ObjectHeader,ok)
      if (.not.ok) then
        write(soun,'(a)') 'ERROR: Reading object header.'
        return
      endif

C Observation number?
      Global_obsnum = inhead('FRAMENO',ObjectHeader)

C Set Current_ObsDate.
      call EE_GetDate(ObjectHeader)

C Check image size.
      call GetDimensions(ObjectHeader,sc,ec,sr,er,nc,nr)
      if (nc*nr.gt.maxpix) then
        write(soun,'(a,i6,a,i7)') 
     .             'ERROR: Image too big :',nc*nr,'   Max:',maxpix
        ok=.false.
        return
      endif

C Set observation number string "cob" (append CCD number for HIRES2).
      if (ccdloc.gt.0) then
        if (Global_obsnum > 999) then
          write(cob,'(i4.4,a,i1)') 
     .       max(0,min(9999,Global_obsnum)),'_',ccdloc
        else
          write(cob,'(i3.3,a,i1)') 
     .       max(0,min(999,Global_obsnum)),'_',ccdloc
        endif
      else
        if (Global_obsnum > 999) then
          write(cob,'(i4.4)') max(0,min(9999,Global_obsnum))
        else
          write(cob,'(i3.3)') max(0,min(999,Global_obsnum))
        endif
      endif

C Binning, etc.
      call GetBinning(ObjectHeader,xbin,ybin)
      call mk_select_eperdn_ronoise(xbin,ybin,UserEperdn,UserRonoise,
     .                   ObjectHeader,eperdn,ronoise)
      call mk_select_maskfile(ObjectHeader,EE_MaskFile)
      if (access(EE_MaskFile,'r').ne.0) then
        write(soun,'(2a)') 'WARNING: Cannot find mask file: ',
     .                      EE_MaskFile(1:lc(EE_MaskFile))
        write(soun,'(a)') 'WARNING: Not using bad column masking.'
        EE_MaskFile=' '
      endif

C Other files (read and check headers)...................
      call mk_set_other_files(arg,narg,nobias,biasfile,xbin,ybin,
     .    rawdir,objfile,ObjectHeader,header,starfile,flatfile,
     .    arcfile1,arcfile2,arc1add,arc2add,ok)
      if (.not.ok) then
        write(soun,'(a)') 'ERROR: Finding or checking other files.'
        return
      endif

C Fix cards in object header.
      call FixKeckCards(biasfile,ObjectHeader,eperdn,ronoise)

C Change some parmfile values
      if (Global_HIRES2) then
        call PSE_HIRES2_Parameter_Change( xbin )
      endif

C Echo.
      write(soun,'(2x)') 
      write(soun,'(a)') 'Object to extract:' 
      call mk_object_info_lines(objfile,ObjectHeader)
      write(soun,'(2x)') 

C Order to extract.
      if (nueons.eq.0) then
        write(soun,'(a,i2,a)') 
     .  'Mode= ',pse_mode,'   Extract all orders.'
      else
        write(soun,'(a,i2)')   'Mode= ',pse_mode
        do i=1,nueons
          write(soun,'(a,i4)') 'Extract order=',ueons(i)
        enddo
      endif


C Echo masking data file and bias file.
      write(soun,'(2a)') 
     .  'Masking file: ',EE_MaskFile(1:lc(EE_MaskFile))

      write(soun,'(2a)') 
     .  '   Bias file: ',biasfile(1:lc(biasfile))



C.........BIAS IMAGE...........
C Read bias FITS file (use obj() array, temporarily).
      if (biasfile.ne.' ') then
        write(soun,'(a)') '............'
        write(soun,'(2a)') 'Reading bias : ',biasfile(1:lc(biasfile))
        call readfits(biasfile,obj,maxpix,header,ok)
        if (.not.ok) then
          write(soun,'(2a)')
     .  'ERROR: Reading Bias file: ',biasfile(1:lc(biasfile))
          return
        endif
C Is this a 1D processed mush?
        call GetDimensions(header,sc,ec,sr,er,nc,nr)
        bias_sc = sc
        bias_ec = ec
        if (nr.eq.1) then
          if (.not.card_exist('BIW',header)) then
            write(soun,'(2a)') 
     .  'ERROR: 1D bias file has not been processed.'
            ok=.false.
            return
          endif
          write(soun,'(2a)') 
     .  'Using 1 dimensional processed bias image.'
          do i=1,nc
            bias(i)=obj(i)
          enddo
        else
C Warning.
          if (Global_ESI) then
            write(soun,'(2a)') 'WARNING: Bias image input ',
     .           'not recommended with ESI-- use default instead.'
          endif
C If necessary, fix header cards and Baseline-Interpolate-Window.
          call FixKeckCards(biasfile,header,eperdn,ronoise)
          call biw(obj,flat,header,EE_MaskFile,0,sc,ec,sr,er,ok)
          if (.not.ok) return
C Bad pixel masking.
          if (EE_MaskFile.ne.' ') then
            write(soun,'(a)') 'Masking bad regions...'
            call Mask_Bad_Regions(obj,sc,ec,sr,er,EE_MaskFile)
          endif
C Mush bias image (replace columns with median values).
          call GetDimensions(header,sc,ec,sr,er,nc,nr)
          call CB_Mush(obj,sc,ec,sr,er,arr,bias)
          bias_sc = sc
          bias_ec = ec
C Write out Mush image.
          call inheadset('NAXIS2',1,header) 
          call inheadset('CRVAL2',0,header)
          call cheadset('CTYPE1','PIXEL',header)
          write(soun,'(a)') 'Writing : bias1d.fits'
          call writefits('bias1d.fits',bias,header,ok)
        endif
        wrd = chead('DATE-OBS',header)
        write(soun,'(2a)') 
     .  'Bias image observation date: ',wrd(1:lc(wrd))
      else
        write(soun,'(a)') 'WARNING: makee: Using zero bias image.'
      endif



C..................FLAT IMAGE....................
      IF (flatfile.ne.' ') THEN
C
      write(soun,'(a)') '............'
      write(soun,'(2a)') 'Reading flat : ',flatfile(1:lc(flatfile))
      call readfits(flatfile,flat,maxpix,header,ok)
      if (.not.ok) return
C Check for quartz.
      if (Global_HIRES.or.Global_HIRES2) then
        wrd = chead('LAMPNAME',header)
        call lower_case(wrd)
        if (index(wrd,'quar').eq.0) then
          write(soun,'(a)') 
     .    'WARNING : WARNING : WARNING : WARNING : WARNING'
          write(soun,'(3a)') 'WARNING: LAMPNAME = ',wrd(1:lc(wrd)),
     .                       ' in flat field image.'
          write(soun,'(a)') 
     .    'WARNING : WARNING : WARNING : WARNING : WARNING'
        endif
      endif
      if (Global_ESI) then
        wrd = chead('OBSTYPE',header)
        if ((index(wrd,'DmFlat').eq.0).and.
     .      (index(wrd,'IntFlat').eq.0)) then
          write(soun,'(a)') 
     .    'WARNING : WARNING : WARNING : WARNING : WARNING'
          write(soun,'(2a)') 'WARNING: Flat field image is not',
     .                     ' a "DmFlat" or "IntFlat" observation type.'
          write(soun,'(a)') 
     .    'WARNING : WARNING : WARNING : WARNING : WARNING'
        endif
      endif
C If necessary, fix header cards and Baseline-Interpolate-Window.
      call FixKeckCards(flatfile,header,eperdn,ronoise)
C Baseline-Interpolate-Window.
      if (Global_ESI) then
        write(soun,'(a)') 'NOTE: Using special interpolation for ESI.'
        call biw(flat,obj,header,EE_Maskfile,2,sc,ec,sr,er,ok)
      else
        call biw(flat,obj,header,EE_MaskFile,0,sc,ec,sr,er,ok)
      endif
      call GetBinning(header,xbin,ybin)
      if (.not.ok) return
C If necessary, subtract Bias (uses dimensions, does not change them.)
      if ((inhead('BIAS_SUB',header).ne.1).and.(biasfile.ne.' ')) then
        write(soun,'(a)') 'Subtracting bias from image...'
        call EE_SubtractBias(flat,bias,sc,ec,sr,er)
      endif
      FlatHeader = header
C
      ELSE
C
C Read object as a flat field (but set values to 1.0).
      write(soun,'(a)') '............'
      write(soun,'(2a)') 
     .          'Reading object as place holder for zero flat : ',
     .          objfile(1:lc(objfile))
      call readfits(objfile,flat,maxpix,header,ok)
      if (.not.ok) return
      FlatHeader = header
C If necessary, fix header cards and Baseline-Interpolate-Window.
      call FixKeckCards(objfile,header,eperdn,ronoise)
C Baseline-Interpolate-Window.
      if (Global_ESI) then
        write(soun,'(a)') 'NOTE: Using special interpolation for ESI.'
        call biw(flat,obj,header,EE_Maskfile,2,sc,ec,sr,er,ok)
      else
        call biw(flat,obj,header,EE_MaskFile,0,sc,ec,sr,er,ok)
      endif
      call GetBinning(header,xbin,ybin)
C Set to zero.
      nc = 1 + ec - sc
      nr = 1 + er - sr
      do ii=1,(nc*nr)
        flat(ii) = 1.
      enddo
      FlatHeader = header
C
      ENDIF
      write(soun,'(a)') '............'


C.................SLIT LENGTH.................
C Find slit length (Global_sw).
      if (usw.gt.0.) then
        Global_sw = usw
        write(soun,'(a)') 'Using slit length from command line.'
      elseif (flatfile.eq.' ') then
        call LookupSlitWidth(header,ybin,r1,r2,starfile,temphead)
        if (r1.gt.0.) then
          Global_sw = r2 
        else
          write(soun,'(a)') 
     .  'ERROR: Could not guess slit length from FITS header.'
          write(soun,'(a)') 
     .  'ERROR: Do not have flat field to estimate slit.'
          write(soun,'(a)') 
     .  'ERROR: Cannot continue.'
          ok=.false.
          return
        endif
      else
        write(soun,'(a)') 'Estimate slit length from flat field...'
        call GetDimensions(FlatHeader,sc,ec,sr,er,nc,nr) 
        call EE_FindSlitWidth(sc,ec,sr,er,flat)
      endif
      write(soun,'(a,f8.3)') 'Estimate for slit length (px) = ',
     .                        Global_sw
C Check slit length (Global_sw).
      call LookupSlitWidth(FlatHeader,ybin,r1,r2,starfile,temphead)
      sw_flat_lookup = r2
      if (r1.gt.0.) then
        write(soun,'(a,f8.3)') ' Slit length lookup (px) =',r2
C Changed this from 0.65 and 1.50 to 0.80 and 1.25.......... -tab 04dec07
        if ((r2/Global_sw.lt.0.80).or.(r2/Global_sw.gt.1.25)) then   
          write(soun,'(a)') 
     .  'WARNING: Estimated slit length appears incorrect.'
          write(soun,'(a)') 
     .  'WARNING: Using lookup value.'
          Global_sw = r2
        endif
      endif
C Modification... -tab 09apr2007
      Global_sw_flat = Global_sw
      Global_sw_adj  = 0.
C Compare against lookup slit on object header.
      call LookupSlitWidth(ObjectHeader,ybin,r1,r2,starfile,temphead)
      if (r1.gt.0) then
        write(soun,'(a,f8.3)') ' Object header slit lookup (px) =',r2
C Changed this from 0.65 and 1.50 to 0.80 and 1.25.......... -tab 04dec07
        if ((r2/Global_sw.lt.0.80).or.(r2/Global_sw.gt.1.25)) then
          Global_sw = r2
          Global_sw_adj = (sw_flat_lookup - r2) / 2.
          write(soun,'(a)') 
     .  'WARNING: Object header slit length appears different.'
          write(soun,'(a)') 
     .  'WARNING: Use two different slit lengths (in pixels):'
          write(soun,'(a,f8.3)') '   Global_sw_flat=',Global_sw_flat
          write(soun,'(a,f8.3)') '   Global_sw     =',Global_sw
          write(soun,'(a,f8.3)') '   Global_sw_adj =',Global_sw_adj
        endif
      endif

C..........SAVE FLAT IMAGE...............
      ii = fftimeget()
      write(saveflatfile,'(a,i10.10,i6.6,i3.3,a)')
     .  'junk-flat_',ii,Global_obsnum,pse_mode,'.fits'
      write(soun,'(2a)') 'Saving flat image... ',
     .                          saveflatfile(1:lc(saveflatfile))
      call writefits(saveflatfile,flat,header,ok)

C.................STAR IMAGE.........................
C Read star.  Use obj() to read data, temporarily.
      write(soun,'(a)') '............'
      write(soun,'(2a)') 'Reading star : ',starfile(1:lc(starfile))
      call readfits(starfile,obj,maxpix,header,ok)
      if (.not.ok) return
      Star_Obsnum = inhead('FRAMENO',header)
C If necessary, fix header cards and Baseline-Interpolate-Window.
      call FixKeckCards(starfile,header,eperdn,ronoise)
C Baseline-Interpolate-Window.
      if (Global_ESI) then
        write(soun,'(a)') 'NOTE: Using special interpolation for ESI.'
        call biw(obj,flat,header,EE_Maskfile,2,sc,ec,sr,er,ok)
      else
        write(soun,'(2a)') 'NOTE: Not interpolating or masking bad',
     .                     ' columns on trace star image.'
        call biw(obj,flat,header,' ',0,sc,ec,sr,er,ok)
      endif
      call GetBinning(header,xbin,ybin)
      if (.not.ok) return
C If necessary, subtract Bias (uses dimensions, does not change them.)
      if ((inhead('BIAS_SUB',header).ne.1).and.(biasfile.ne.' ')) then
        write(soun,'(a)') 'Subtracting bias from image...'
        call EE_SubtractBias(obj,bias,sc,ec,sr,er)
      endif

C..........SAVE STAR IMAGE...............
C#@#
C     ii = fftimeget()
C     write(savestarfile,'(a,i10.10,i6.6,i3.3,a)')
C    .  'junk-star_',ii,Global_obsnum,pse_mode,'.fits'
C     write(soun,'(2a)') 'Saving star image... ',
C    .                   savestarfile(1:lc(savestarfile))
C     call writefits(savestarfile,obj,header,ok)
C#@#



C............TRACE ORDERS.....................
C Find eo(..,..) and neo from bright star.
C NOTE: EE_FindOrders requires "Global_sw", so we cannot put the star reading
C       before the flat reading.  Thus, we need to save-recover-save-recover
C       the flat image to disk (annoying).
      write(soun,'(a)') 'Finding and fitting order traces...'
      if (BlueOrders.ne.909) then
        write(soun,'(a,i3,a)') 'NOTE: Try to trace ',BlueOrders,
     .              ' orders from blue CCD of HIRES2.'
      endif
      call GetDimensions(header,sc,ec,sr,er,nc,nr) 
      if ((Global_HIRES2).and.(Global_ccdloc.eq.1)) then
        call EE_FindOrders_HIRES2_1(sc,ec,sr,er,obj)
      elseif (Global_HIRES.or.Global_HIRES2) then
        call EE_FindOrders_HIRES(sc,ec,sr,er,obj)
C#@#
C       print *,'#@# ... Global_neo=',Global_neo
C       do jj=1,Global_neo
C       print *,'#@# ... jj=',jj
C       do ii=0,10
C         print '(a,1pe15.7,a,i4,a,i4)',
C    .     '#@# eo(ii,jj)=',eo(ii,jj),'  ii=',ii,'  jj=',jj
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8
C       enddo
C       enddo
C#@#
      else
        call EE_FindOrders_ESI(sc,ec,sr,er,obj)
      endif
      write(soun,'(a,i4)') 'Number of echelle orders =',Global_neo
C
C Check.
      if (Global_neo.gt.100) then
        write(soun,'(a,i5,a)') 
     .   'ERROR: Too many orders found:',Global_neo,' (max=100).'
        call exit(0)
      endif
C
C Report User Object Positions.
      if (uop_exist) then
        write(soun,'(a)')'..... User Object Positions ("uop=") .....'
        write(soun,'(a)')
     .    'Order  Center  Halfwidth  Background Limits (pixels)'
        do ii=1,Global_neo
          if (uop_h(ii).gt.0.) then
            write(soun,'(i3,6(f10.2))') ii,uop_c(ii),uop_h(ii),
     .          uop_b1o(ii),uop_b1i(ii),uop_b2i(ii),uop_b2o(ii)
          endif
        enddo
        write(soun,'(a)')'.............'
      endif

C Check oko().
      jj=0
      do ii=1,Global_neo
        if (oko(ii)) jj=jj+1
      enddo
      if (jj.lt.2) then
        write(soun,'(2a)') 
     .     'ERROR: Must have at least 2 good orders ("oko=")',
     .     ' for trace centroids.'
        call exit(0)
      endif

C..........RECOVER FLAT IMAGE...............
      write(soun,'(2a)') 'Recovering flat image... ',
     .                          saveflatfile(1:lc(saveflatfile))
      call readfits(saveflatfile,flat,maxpix,header,ok)


C..............BACKGROUND RANGES.................
C Find bk1o(..,..), bk2o(..,..) from flat field or just slit width.
      call GetDimensions(header,sc,ec,sr,er,nc,nr) 
      if (flatfile.ne.' ') then
        write(soun,'(a)') 'Estimating background limits...'
        call EE_FindBack(sc,ec,sr,er,flat)
      else
        write(soun,'(a)') 'Estimating background limits with no flat...'
        call EE_FindBackWithNoFlat(sc,ec,sr,er)
      endif

C..............DELETE SMALL ORDERS...................
C Check order column range using bk1o() and bk2o().
C Test each order. Delete order if too small.
      eon=1
      do while(eon.le.Global_neo)
C Transfer arrays.
        do i=0,mpp
          Tbk1o(i+1)= bk1o(i,eon)
          Tbk2o(i+1)= bk2o(i,eon)
        enddo
C Check usable order column range.
        call EE_order_column_range(Tbk1o,Tbk2o,sc,ec,sr,er,usc,uec,ok)
C Delete order if problem or if column range is too small.
        if ((.not.ok).or.((uec-usc).lt.20)) then
          write(soun,'(a,i4,a,a,i4,a)') 
     .       'NOTE: Deleting order number',eon,' (too small) ',
     .       '[usable columns=',uec-usc,']'
          call EE_delete_echelle_order(eon)
        endif
        eon=eon+1
      enddo

C/* Echo, store. */
C     print *,'NOTE: Global_neo=',Global_neo,sc,ec,xc
      xc = dfloat(((sc + ec) / 2))
      do i=1,Global_neo
        eoc(i) = nint(polyarroff(eo(0,i),xc))
      enddo

C Extraction type.
      if (pse_mode.eq.4) then
        ExtractionType = 'Arc'
      else
        ExtractionType = 'Object'
      endif

C Re-scale the flat and divide the object by flat.
      if (flatfile.ne.' ') then
        call EE_FlatScale(sc,ec,sr,er,flat,scale_factor)
        orig_scale_factor = scale_factor
        write(soun,'(a,1pe15.6)')
     .       ' Flat field scale factor=',scale_factor
        call EE_ImageDivideConst(sc,ec,sr,er,flat,scale_factor)
      else
C Set flat to 1.0 .
        write(soun,'(a)') 'NOTE: Set flat field to 1.0'
        orig_scale_factor = 1.0
        do i=1,maxpix
          flat(i) = 1.
        enddo
      endif

C We need means of flat in each echelle order.
      call EE_FlatMeans(sc,ec,sr,er,flat)


C..........SAVE FLAT IMAGE...............
      write(soun,'(2a)') 'Saving flat image... ',
     .                    saveflatfile(1:lc(saveflatfile))
      call writefits(saveflatfile,flat,header,ok)


C + + + + + + + + + + + + + + + + + + + + + + + +
C Restart point for other extractions.
1111  continue
C + + + + + + + + + + + + + + + + + + + + + + + +


C.............OBJECT IMAGE..................
C Read object FITS file.  ( "ObjectHeader" was loaded earlier. )
      write(soun,'(a)') '............'
      write(soun,'(2a)') 'Reading object : ',objfile(1:lc(objfile))
      call readfits(objfile,obj,maxpix,header,ok)
C#@#
C     if (Pass.eq.1) then
C     ii = ( 685 + (1092 * 1070) ) + 1
C     obj(ii) = 9000.0
C     call writefits('raw.fits',obj,header,ok)
C     endif
C#@#
     
      if (.not.ok) return

C#@#
C     if (Pass.eq.1) then
C     naxis1 = inhead('NAXIS1',header);
C     naxis2 = inhead('NAXIS2',header);
C     print *,'naxis1=',naxis1,' naxis2=',naxis2
C     ii = 200
C     jj = 200
C     pp = 1 + ii + (jj * naxis1)
C     obj(pp) = 9900.
C     ii = 201
C     jj = 200
C     pp = 1 + ii + (jj * naxis1)
C     obj(pp) = 9910.
C     ii = 200
C     jj = 201
C     pp = 1 + ii + (jj * naxis1)
C     obj(pp) = 9901.
C     ii = 201
C     jj = 201
C     pp = 1 + ii + (jj * naxis1)
C     obj(pp) = 9911.
C     ii = 202
C     jj = 201
C     pp = 1 + ii + (jj * naxis1)
C     obj(pp) = 9921.
C     ii = 202
C     jj = 202
C     pp = 1 + ii + (jj * naxis1)
C     obj(pp) = 9922.
C     ii = 202
C     jj = 201
C     pp = 1 + ii + (jj * naxis1)
C     obj(pp) = 9921.
C     ii = 203
C     jj = 201
C     pp = 21 + ii + (jj * naxis1)
C     obj(pp) = 9931.
C     ii = 201
C     jj = 203
C     pp = 1 + ii + (jj * naxis1)
C     obj(pp) = 9913.
C     do ii=195,200
C     do jj=195,200
C       pp = 1 + ii + (jj * naxis1)
C       obj(pp) = 9000.
C     enddo
C     enddo
C     do ii=190,200
C     do jj=300,310
C       pp = 1 + ii + (jj * naxis1)
C       obj(pp) = 9000.
C     enddo
C     enddo
C     do ii=300,320
C     do jj=180,200
C       pp = 1 + ii + (jj * naxis1)
C       obj(pp) = 9000.
C     enddo
C     enddo
C     do ii=300,340
C     do jj=300,340
C       pp = 1 + ii + (jj * naxis1)
C       obj(pp) = 9000.
C     enddo
C     enddo
C     do ii=200,300
C     do jj=200,300
C       pp = 1 + ii + (jj * naxis1)
C       obj(pp) = 9000.
C     enddo
C     enddo
C     print *,'#@# writing raw-1.fits'
C     call inheadset('CRVAL1',0,header)
C     call inheadset('CRVAL2',0,header)
C     call inheadset('CRPIX1',1,header)
C     call inheadset('CRPIX2',1,header)
C     call make_basic_header( temphead, 0, naxis1, 0, naxis2 )
C     call writefits('raw-1.fits',obj,temphead,ok)
C     endif
C#@#

C Remove these from Object Header.
      call unfit('BZERO',ObjectHeader)
      call unfit('BSCALE',ObjectHeader)

C If arc lamp pass, load "ObjectHeader".
      if (Pass.ne.1) ObjectHeader = header
C If necessary, fix header cards and Baseline-Interpolate-Window.
      call FixKeckCards(objfile,ObjectHeader,eperdn,ronoise)
C Baseline-Interpolate-Window (will mask bad regions later).
      if (Global_ESI) then
        write(soun,'(a)') 'NOTE: Using special interpolation for ESI.'
        call biw(obj,flat,ObjectHeader,EE_MaskFile,2,sc,ec,sr,er,ok)
      else
        call biw(obj,flat,ObjectHeader,EE_MaskFile,0,sc,ec,sr,er,ok)
      endif
      call GetBinning(ObjectHeader,xbin,ybin)
      if (.not.ok) return

C Re-fix keck cards.
      call FixKeckCards(objfile,ObjectHeader,eperdn,ronoise)

C If necessary, subtract Bias (uses dimensions, does not change them.)
      if ((inhead('BIAS_SUB',ObjectHeader).ne.1).and.
     .                                   (biasfile.ne.' ')) then
C Check bias dimensions.
        if ((bias_sc.ne.sc).or.(bias_ec.ne.ec)) then
          write(soun,'(a)') 
     .  'ERROR: bias dimensions do not match object dim.'
          ok=.false.
          return
        endif
        write(soun,'(a)') 'Subtracting bias from image...'
        call EE_SubtractBias(obj,bias,sc,ec,sr,er)
      endif
C Write version string in header as well as all the makee.param parameters.
      call EE_MoreCards(ObjectHeader)
      call PSE_MoreCards(ObjectHeader,pse_mode)
C Object name.
      if (newobject.ne.' ') 
     .  call cheadset('OBJECT',newobject,ObjectHeader)
      object = chead('OBJECT',ObjectHeader)
C New observer name.
      if (observer.ne.' ') 
     .  call cheadset('OBSERVER',observer,ObjectHeader)
C Add MAKEE argument string to object header.
      call makeearg_string(ObjectHeader,UserEperdn,UserRonoise,printer,
     .       pse_mode,user_shift,usw,parmfile,biasfile,rawdir,
     .       newobject,observer,logfile,whole_thing,test,DoAtm,
     .       NoVac,NoHC,nozero,noskyshift,nobias)
C Set gain and read-out noise.
      eperdn   = sngl(fhead('EPERDN',ObjectHeader))
      ronoise  = sngl(fhead('RONOISE',ObjectHeader))
      Global_exposure = float(inhead('EXPOSURE',ObjectHeader))
      if (Global_exposure.lt.1.) write(soun,'(a)') 
     .  'ERROR: makee: No exposure time.'
      Global_eperdn = eperdn
      Global_rov    = (ronoise/eperdn)*(ronoise/eperdn)
      Global_obsnum = inhead('OBSNUM',ObjectHeader)
      if (Pass.eq.1) Flux_Obsnum = Global_obsnum
C Get UT date (set Current_ObsDate).
      call EE_GetDate(ObjectHeader)
      write(soun,'(a,i5,2a)') ' Observation number=',
     . max(-9,min(9999,Global_obsnum)),'   UT date = ',Current_ObsDate
C Bad pixel masking.
      if (EE_MaskFile.ne.' ') then
        write(soun,'(a)') 'Masking bad regions...'
        call Mask_Bad_Regions(obj,sc,ec,sr,er,EE_MaskFile)
      endif

C Save binning factors for later reference.
      call GetBinning(ObjectHeader,xbin,ybin)
      Global_xbin = xbin
      Global_ybin = ybin
      write(soun,'(a,i3,a,i3,a)') 
     .   'NOTE: Object x,y binning is ',xbin,' x',ybin,' .'

C..........RECOVER FLAT IMAGE...............
      write(soun,'(2a)') 'Recovering flat image... ',
     .                          saveflatfile(1:lc(saveflatfile))
      call readfits(saveflatfile,flat,maxpix,header,ok)

C#@#
C     if (Pass.eq.1) then
C     print *,'### Writing... objnodiv.fits...'
C     call writefits('objnodiv.fits',obj,ObjectHeader,ok)
C     endif
C     if (Pass.eq.2) then
C     print *,'### Writing... flatnodiv.fits...'
C     call writefits('flatnodiv.fits',obj,ObjectHeader,ok)
C     endif
C     if (Pass.eq.3) then
C     print *,'### Writing... arcnodiv.fits...'
C     call writefits('arcnodiv.fits',obj,ObjectHeader,ok)
C     endif
C#@#

C..........DIVIDE FLAT FIELD................
      write(soun,'(a)') 'Dividing object by flat field...'
      call EE_ImageDivide(sc,ec,sr,er,obj,flat)

C#@#
c     if (Pass.eq.1) then
C     print *,'### Writing... objdiv.fits...'
C     call writefits('objdiv.fits',obj,ObjectHeader,ok)
C     print *,'### BEFORE: FIND THE OBJECT PROFILE AND BOUNDS..'
C     endif
C#@#

C..........FIND THE OBJECT PROFILE AND BOUNDS................
C Find sp1(..,..) and sp2(..,..) and pval and pparm.
      if (pse_mode.ne.4) then
        write(soun,'(a)') ' Determine object profile and boundaries...'
        call EE_FindObject(sc,ec,sr,er,obj,pd_xp,pd_yp,pd_zp,pd_n)   ! changed -tab 28nov07
      endif

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8
C Put in trace solution coeffecients.  -tab 06nov07
      if (Pass.eq.1) then
C Save some cards.
        call inheadset('MK_NEO'  ,Global_neo    ,ObjectHeader)
        call inheadset('MK_XBIN' ,Global_xbin   ,ObjectHeader)
        call inheadset('MK_YBIN' ,Global_ybin   ,ObjectHeader)
        call inheadset('MK_CCD'  ,Global_ccdloc ,ObjectHeader)

        rr8 = Global_sw
        call fheadset( 'MK_SW'   ,rr8 ,ObjectHeader)
        rr8 = Global_sw_flat
        call fheadset( 'MK_SWFLT',rr8 ,ObjectHeader)
        rr8 = Global_sw_adj
        call fheadset( 'MK_SWADJ',rr8 ,ObjectHeader)
        rr8 = orig_scale_factor
        call fheadset('MK_FSCAL' ,rr8 ,ObjectHeader)
        call fheadset('FLATSCAL' ,rr8 ,ObjectHeader);

        call lheadset( 'MK_ESI'  ,Global_ESI    ,ObjectHeader)
        call lheadset( 'MK_HRS'  ,Global_HIRES  ,ObjectHeader)
        call lheadset( 'MK_HRS2' ,Global_HIRES2 ,ObjectHeader)

C Load table echelle order solutions.
        do i=1,Global_neo
          write(c7,'(a,i1,a,i2.2)') 'CO_',0,'_',i
          write(wrd,'(4(1pe17.9))') sp1(0,i), sp1(1,i),
     .            ((sp1(2,i) + sp2(2,i))/2.), sp1(3,i)
          call cheadset(c7,wrd,ObjectHeader)
          write(c7,'(a,i1,a,i2.2)') 'CO_',4,'_',i
          write(wrd,'(4(1pe17.9))') sp1(4,i),sp1(5,i),sp1(6,i),sp1(7,i)
          call cheadset(c7,wrd,ObjectHeader)
C Save coeffecients for center of primary object.
          tbl_sp(0,i) = sp1(0,i)
          tbl_sp(1,i) = sp1(1,i)
          tbl_sp(2,i) = (sp1(2,i) + sp2(2,i))/2.
          tbl_sp(3,i) = sp1(3,i)
          tbl_sp(4,i) = sp1(4,i)
          tbl_sp(5,i) = sp1(5,i)
          tbl_sp(6,i) = sp1(6,i)
          tbl_sp(7,i) = sp1(7,i)
C Save coeffecients for trace solution.
          tbl_eo(0,i) = eo(0,i)
          tbl_eo(1,i) = eo(1,i)
          tbl_eo(2,i) = eo(2,i)
          tbl_eo(3,i) = eo(3,i)
          tbl_eo(4,i) = eo(4,i)
          tbl_eo(5,i) = eo(5,i)
          tbl_eo(6,i) = eo(6,i)
          tbl_eo(7,i) = eo(7,i)
        enddo

C Save MK_CLIP.
        wrd = chead('MK_CLIP',ObjectHeader)
        read(wrd,'(4(i7))') tbl_mc(1),tbl_mc(2),tbl_mc(3),tbl_mc(4)


C#@#
C     i = 2000
C     rr8 = polyarroff(tbl_eo(0,1),dfloat(i))
C     print *,'order=1 ... col=',i,' rr8=',rr8
C     rr8 = polyarroff(tbl_eo(0,2),dfloat(i))
C     print *,'order=2 ... col=',i,' rr8=',rr8
C     rr8 = polyarroff(tbl_eo(0,3),dfloat(i))
C     print *,'order=3 ... col=',i,' rr8=',rr8
C
C     i = 1969
C     rr8 = polyarroff(tbl_eo(0,3),dfloat(i))
C     print *,'order=3 ... col=',i,' rr8=',rr8
C     print *,'tbl_mc(1)=',tbl_mc(1)
C     print *,'tbl_mc(2)=',tbl_mc(2)
C     print *,'tbl_mc(3)=',tbl_mc(3)
C     print *,'tbl_mc(4)=',tbl_mc(4)
C
C     old_row = ( float(i) - float(tbl_mc(3)) ) + 1.0
C     old_col = ( float(tbl_mc(2)) - rr8 ) - 1.0
C     print *,'NEW ord=3 .. old_col=',old_col,'  old_row=',old_row
C
C     rr8 = polyarroff(tbl_eo(0,4),dfloat(i))
C     print *,'order=4 ... col=',i,' rr8=',rr8
C     rr8 = polyarroff(tbl_eo(0,5),dfloat(i))
C     print *,'order=5 ... col=',i,' rr8=',rr8
C     rr8 = polyarroff(tbl_eo(0,9),dfloat(i))
C     print *,'order=9 ... col=',i,' rr8=',rr8
C#@#


      endif

C Exit here if requested.
      if (nox) then
        write(soun,'(a)')'Exiting prior to extractions as requested.'
        call exit(0)
      endif


C..........PREPARE TO EXTRACT...........................
C Echo.

      write(soun,'(a,i3)') 'pse_mode=',pse_mode

      write(soun,'(a,f6.2,i4,2f7.3,i3)') 
     .      ' slit width, neo, eperdn, ronoise, pse_mode = ',
     .      Global_sw,Global_neo,eperdn,ronoise,pse_mode
C Test only.
      if (test) then
        write(soun,'(a)') 
     .  'Write flat-divided pre-masking image "testOne.fits".'
        file='testOne.fits'
        header=ObjectHeader
        call cheadset('OBJECT','testOne',header)
        call writefits(file,obj,header,ok)
      endif


C Echo.
      if (pse_mode.eq.4) write(soun,'(a)') 
     .  'Extracting whole slit spectrum...'

C Set global variables.
      Global_eperdn  = eperdn
      Global_rov     = (ronoise/eperdn)*(ronoise/eperdn)


C..........EXTRACT EACH ECHELLE ORDER REQUESTED................
      DO eon=1,Global_neo
      IF (Do_This_Order(eon,nueons,ueons)) THEN

C Transfer arrays.
      do i=0,mpp
        Tsp1(i+1) = sp1(i,eon)
        Tsp2(i+1) = sp2(i,eon)
        Tbk1o(i+1)= bk1o(i,eon)
        Tbk2o(i+1)= bk2o(i,eon)
        Tbk1i(i+1)= bk1i(i,eon)
        Tbk2i(i+1)= bk2i(i,eon)
      enddo

C#@#
C     print *,'#@# ..........eon=',eon
C     print *,'sp1(2,eon)=',sp1(2,eon)
C     print *,'sp2(2,eon)=',sp2(2,eon)
C     print *,'bk1o(2,eon)=',bk1o(2,eon)
C     print *,'bk1i(2,eon)=',bk1i(2,eon)
C     print *,'bk2i(2,eon)=',bk2i(2,eon)
C     print *,'bk2o(2,eon)=',bk2o(2,eon)
C#@#

C Transfer profile.
      profparm(1)=pparm(1,eon)
      profparm(2)=pparm(2,eon)
      profparm(3)=pparm(3,eon)
      profparm(4)=pparm(4,eon)
      profparm(5)=pparm(5,eon)

      do i=1,nint(profparm(1))
        profval(i) = pval(i,eon)
      enddo

C Echo.
      write(soun,'(a,i3,a,i3)')
     .'****************************** Extracting echelle order number',
     . eon,' of',Global_neo

C Set global variable.
      Global_eon     = eon

C Extract spectrum for this order.
      call PSE(sc,ec,sr,er,obj,flat,
     .         Tsp1,Tsp2,Tbk1o,Tbk2o,Tbk1i,Tbk2i,profparm,profval,
     .         pse_mode,DoSum,sky,prof,
     .         objsp,skysp,varsp,sumsp)

C Save some variables on first pass only. 
      if (Pass.eq.1) then
        koa_global( 4,eon) = Global_CR_Frac   ! added -tab 26nov07
        koa_global( 5,eon) = Global_psemra
        koa_global( 6,eon) = Global_sky_rej
        koa_global( 7,eon) = Global_obj_rej
        koa_global( 8,eon) = sp1(2,eon)
        koa_global( 9,eon) = sp2(2,eon)
        koa_global(10,eon) = bk1o(2,eon)
        koa_global(11,eon) = bk1i(2,eon)
        koa_global(12,eon) = bk2i(2,eon)
        koa_global(13,eon) = bk2o(2,eon)
      endif

C Make sky and variance pixels zero if object pixel is zero.
      do i=sc,ec
        if (abs(objsp(i)).lt.1.e-30) then
          skysp(i)=0.
          varsp(i)=0.
        endif
      enddo

C Write spectrum images.
      call EE_Spectrum_to_Image(aao,sc,ec,1,Global_neo,eon,objsp)
      if (pse_mode.ne.4) then
        call EE_Spectrum_to_Image(aasum,sc,ec,1,Global_neo,eon,sumsp)
        call EE_Spectrum_to_Image(aav,sc,ec,1,Global_neo,eon,varsp)
        call EE_Spectrum_to_Image(aas,sc,ec,1,Global_neo,eon,skysp)
      endif

      ENDIF
      ENDDO

C Write out image with CRs masked.
      if (test) then
        file='testTwo.fits'
        header=ObjectHeader
        call cheadset('OBJECT','testTwo',header)
        write(soun,'(a)') 'Writing post-masking image "testTwo.fits".'
        call writefits(file,obj,header,ok)
      endif
C#@#
C     print *,'#@# after testTwo.fits write.'
C     call pauseit()
C#@#

C Change starting row and number of rows.
      call inheadset('CRVAL2',1,ObjectHeader)
      call inheadset('NAXIS2',Global_neo,ObjectHeader)

CCC### C Write order polynomial coeffecients to FITS header.
CCC    C
CCC    C THIS NO LONGER WORKS WITH NEW HIGHER ORDER TRACE FITS...
CCC    C
CCC    CCC   call OLD_EE_CenterOfOrder_to_header(ObjectHeader,pse_mode)
CCC### C

C Write images.
C
C Write arc lamp.
      if (ExtractionType.eq.'Arc') then
        wrd = object(1:lc(object))//' (Arc Lamp)'
        call cheadset('OBJECT',wrd,ObjectHeader)
        file= 'Arc-'//cob(1:lc(cob))//'.fits'
        i = index(file,'.fits')
        if (CommentString.eq.' ') then
          file=file(1:i-1)//'.fits'
        else
          file=file(1:i-1)//CommentString(1:lc(CommentString))//'.fits'
        endif
        if (Pass.eq.3) then
          arcspfile1 = file
        elseif (Pass.eq.4) then
          arcspfile2 = file
        elseif (Pass.eq.5) then
          arcspfile1a= file
        elseif (Pass.eq.6) then
          arcspfile2a= file
        endif
        write(soun,'(2a)') 'Writing : ',file(1:lc(file))
        call writefits(file,aao,ObjectHeader,ok)
C Save Arc- lamp spectrum_image.
        call GetDimensions(ObjectHeader,tsc,tec,tsr,ter,tnc,tnr)
        do i=1,tnc*tnr
          tbl_Arc(i) = aao(i)
        enddo
C
C Write flat lamp spectrum.
      elseif (ExtractionType.eq.'Flat') then
        wrd = object(1:lc(object))//' (Flat Spectrum)'
        call cheadset('OBJECT',wrd,ObjectHeader)
        file= 'Flat-'//cob(1:lc(cob))//'.fits'
        i = index(file,'.fits')
        if (CommentString.eq.' ') then
          file=file(1:i-1)//'.fits'
        else
          file=file(1:i-1)//CommentString(1:lc(CommentString))//'.fits'
        endif
        flatspfile=file
        write(soun,'(2a)') 'Writing : ',file(1:lc(file))
        call writefits(file,aao,ObjectHeader,ok)
C Save Flat- lamp spectrum_image.
        call GetDimensions(ObjectHeader,tsc,tec,tsr,ter,tnc,tnr)
        do i=1,tnc*tnr
          tbl_Flat(i) = aao(i)
        enddo
C 
C Write object spectrum.  (Interpolate zeros unless requested not to.)
      else
        if (nozero) call zero_interp(sc,ec,1,Global_neo,aao)
        wrd = object(1:lc(object))//' (Flux)'
        call cheadset('OBJECT',wrd,ObjectHeader)
        if (CommentString.eq.' ') then
          file='Flux-'//cob(1:lc(cob))//'.fits'
        else
          file='Flux-'//cob(1:lc(cob))//
     .          CommentString(1:lc(CommentString))//'.fits'
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----
        endif
        fluxspfile = file
        write(soun,'(2a)') 'Writing : ',file(1:lc(file))
        call writefits(file,aao,ObjectHeader,ok)
C Save Flux- spectrum_image.
        call GetDimensions(ObjectHeader,tsc,tec,tsr,ter,tnc,tnr)
        do i=1,tnc*tnr
          tbl_Flux(i) = aao(i)
        enddo
C Write variance. Set all zero pixels to -1.0 to help identify masked pixels.
        call Zero_to_Negative_One(sc,ec,1,Global_neo,aav)
        wrd = object(1:lc(object))//' (Variance)'
        call cheadset('OBJECT',wrd,ObjectHeader)
        if (CommentString.eq.' ') then
          file='Var-'//cob(1:lc(cob))//'.fits'
        else
          file='Var-'//cob(1:lc(cob))//
     .         CommentString(1:lc(CommentString))//'.fits'
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----
        endif
        varspfile=file   ! added -tab 10oct2007
        write(soun,'(2a)') 'Writing : ',file(1:lc(file))
        call writefits(file,aav,ObjectHeader,ok)
C Save Var- spectrum_image.
        call GetDimensions(ObjectHeader,tsc,tec,tsr,ter,tnc,tnr)
        do i=1,tnc*tnr
          tbl_Var(i) = aav(i)
        enddo
C Write sky.
        wrd = object(1:lc(object))//' (Sky)'
        call cheadset('OBJECT',wrd,ObjectHeader)
        if (CommentString.eq.' ') then
          file='Sky-'//cob(1:lc(cob))//'.fits'
        else
          file='Sky-'//cob(1:lc(cob))//
     .         CommentString(1:lc(CommentString))//'.fits'
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----
        endif
        skyspfile=file      ! added 10oct2007
        write(soun,'(2a)') 'Writing : ',file(1:lc(file))
        call writefits(file,aas,ObjectHeader,ok)
C Save Sky- spectrum_image.
        call GetDimensions(ObjectHeader,tsc,tec,tsr,ter,tnc,tnr)
        do i=1,tnc*tnr
          tbl_Sky(i) = aas(i)
        enddo

C Summed spectrum. Interpolate zeros unless requested not to.
        if (DoSum) then
          if (nozero) call zero_interp(sc,ec,1,Global_neo,aasum)
          wrd = object(1:lc(object))//' (Summed Flux)'
          call cheadset('OBJECT',wrd,ObjectHeader)
          if (CommentString.eq.' ') then
           file='Sum-'//cob(1:lc(cob))//'.fits'
          else
           file='Sum-'//cob(1:lc(cob))//
     .           CommentString(1:lc(CommentString))//'.fits'
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----
          endif
          sumspfile = file
          write(soun,'(2a)') 'Writing : ',file(1:lc(file))
          call writefits(file,aasum,ObjectHeader,ok)
C Save Sum- spectrum_image.
          call GetDimensions(ObjectHeader,tsc,tec,tsr,ter,tnc,tnr)
          do i=1,tnc*tnr
            tbl_Sum(i) = aasum(i)
          enddo
        endif
      endif

C Echo date.
      write(soun,'(a)') fdate()

C End of pass.
      Pass = Pass + 1

C.....................................
C Extract flat field image (like Arc). 
      if (Pass.eq.2) then
        if (flatfile.ne.' ') then
          write(soun,'(2x)')
          write(soun,'(a)')
     .   '.....  Extracting Flat Field Spectrum  ...........'
          write(soun,'(2x)')
          objfile=flatfile
          do i=1,maxcol*maxord
            aao(i)=0.
          enddo
          pse_mode=4
          ExtractionType='Flat'
          goto 1111
        endif
      endif

C.....................................
C Extract arc lamp (fourth file) if it is specified.
      if (Pass.eq.3) then
        if (arcfile1.ne.' ') then
          write(soun,'(2x)')
          write(soun,'(a)') 
     .  '.....  Extracting Arc Lamp Spectrum  ...........'
          write(soun,'(2x)')
          objfile=arcfile1
          do i=1,maxcol*maxord
            aao(i)=0.
          enddo
          pse_mode=4
          ExtractionType='Arc'
          goto 1111
        endif
      endif

C.....................................
C Extract second arc lamp (fifth file) if it is specified.
      if (Pass.eq.4) then
        if (arcfile2.ne.' ') then
          write(soun,'(2x)')
          write(soun,'(a)') 
     .  '.....  Extracting Second Arc Lamp Spectrum  ...........'
          write(soun,'(2x)')
          objfile=arcfile2
          do i=1,maxcol*maxord
            aao(i)=0.
          enddo
          pse_mode=4
          ExtractionType='Arc'
          goto 1111
        else
C Try Pass #5 : Additional arclamp to arclamp #1.
          if (arc1add.ne.' ') Pass=Pass+1
        endif
      endif

C.....................................
C Extract additional arc lamp for 1st arclamp.
      if (Pass.eq.5) then
        if (arc1add.ne.' ') then
          write(soun,'(2x)')
          write(soun,'(2a)') 
     .      '.....  Extracting Spectrum of Add',
     .      'itional Arc Lamp to Arc#1 .......'
          write(soun,'(2x)')
          objfile=arc1add
          do i=1,maxcol*maxord
            aao(i)=0.
          enddo
          pse_mode=4
          ExtractionType='Arc'
          goto 1111
        else
C Try Pass #6 : Additional arclamp to arclamp #2.
          if (arc2add.ne.' ') Pass=Pass+1
        endif
      endif

C.....................................
C Extract additional arc lamp for 2nd arclamp.
      if (Pass.eq.6) then
        if (arc2add.ne.' ') then
          write(soun,'(2x)')
          write(soun,'(a)') 
     .       '.....  Extracting Spectrum of Add',
     .       'itional Arc Lamp to Arc#2 .......'
          write(soun,'(2x)')
          objfile=arc2add
          do i=1,maxcol*maxord
            aao(i)=0.
          enddo
          pse_mode=4
          ExtractionType='Arc'
          goto 1111
        endif
      endif

C Wavelength Calibration Flag.
      DoWave = ((nueons.eq.0).and.(arcfile1.ne.' '))
C.....................................
C HIRES Wavelength Calibration (only if all orders were extracted).
      IF (DoWave) THEN

      IF (Global_HIRES.or.Global_HIRES2) THEN

C Calibrate Arc#1............
      if (arcspfile1.ne.' ') then
        write(soun,'(a)') 'Calibrate Arc lamp #1...............'
        trg(1)=arcspfile1
        trg(2)='-clobber'
        trg(3)=' '
        ntrg=2
C Use additional arc lamp to add more arclines to the fit.
        if (arcspfile1a.ne.' ') then
          ntrg=ntrg+1
          trg(ntrg)=arcspfile1a
        endif
        write(wrd,'(4(a,2x))') 'autoid',trg(1)(1:lc(trg(1))),
     .                                  trg(2)(1:lc(trg(2))),
     .                                  trg(3)(1:lc(trg(3)))
        write(soun,'(2x)')
        write(soun,'(a)') wrd(1:lc(wrd))
        call autoid_main(trg,ntrg,ok)
        write(soun,'(2x)')
        if (.not.ok) then
          write(soun,'(a)') 'NOTE: Try again with the -soft option.'
          trg(3)='-soft'
          trg(4)=' '
          ntrg=3
          write(wrd,'(4(a,2x))') 'autoid',trg(1)(1:lc(trg(1))),
     .                                    trg(2)(1:lc(trg(2))),
     .                                    trg(3)(1:lc(trg(3))),
     .                                    trg(4)(1:lc(trg(4)))
          write(soun,'(2x)')
          write(soun,'(a)') wrd(1:lc(wrd))
          call autoid_main(trg,ntrg,ok)
          write(soun,'(2x)')
        endif
        write(soun,'(a)') 
     .  '.................................................'
      endif

C Calibrate Arc#2............
      if (arcspfile2.ne.' ') then
        write(soun,'(a)') 'Calibrate Arc lamp #2...............'
        trg(1)=arcspfile2
        trg(2)='-clobber'
        trg(3)=' '
        ntrg=2
C Use additional arc lamp to add more arclines to the fit.
        if (arcspfile2a.ne.' ') then
          ntrg=ntrg+1
          trg(ntrg)=arcspfile2a
        endif
        write(wrd,'(4(a,2x))') 'autoid',trg(1)(1:lc(trg(1))),
     .                                  trg(2)(1:lc(trg(2))),
     .                                  trg(3)(1:lc(trg(3)))
        write(soun,'(2x)')
        write(soun,'(a)') wrd(1:lc(wrd))
        call autoid_main(trg,ntrg,ok)
        write(soun,'(a)') 
     .  '.................................................'
        write(soun,'(2x)')
      endif

      ELSEIF (Global_ESI) THEN

C ESI Wavelength Calibration (only if all orders were extracted).
C Copy filenames.
        wrd1 = arcspfile1
        write(soun,'(2a)') 'Compute wavelength scale for ',
     .                      arcspfile1(1:lc(arcspfile1))
        if (arcspfile2.ne.' ') then
          write(soun,'(2a)') ' ... and include ',
     .                        arcspfile2(1:lc(arcspfile2))
          wrd2 = arcspfile2
        else
          wrd2 = 'none'
        endif
C Append NULL to arcfile filaname string.
        ii = lc(wrd1)
        wrd1(ii+1:ii+1) = char(0)
        ii = lc(wrd2)
        wrd2(ii+1:ii+1) = char(0)
C Apply wavelength scales.
        ii = esiwaveapply( wrd1, wrd2 )             ! "c" routine
C Cancel calibration if error has occurred.
        if (ii.ne.1) then
          write(soun,'(a)') 
     .     'ERROR: With wavelength calibration in esiwaveapply.'
          DoWave = .false.
        endif

      ENDIF          !   IF (Global_HIRES.or.Global_HIRES2) THEN ...

C Put wavelength scale on object file.......
      IF ((fluxspfile.ne.' ').and.(DoWave)) THEN

C Run arc2obj -sky on Flux-, Var-, Arc-, Sky- files.
      do i=1,12
        arg(i)=' '
      enddo
C Two required "Command" line arguments.
      trg(1)=arcspfile1
      trg(2)=fluxspfile
      ntrg=2
C Bracketting arclamps with arcmode.
      if (arcspfile2.ne.' ') then
        ntrg=ntrg+1
        trg(ntrg)='bracket='//arcspfile2(1:lc(arcspfile2))
        ntrg=ntrg+1
        write(trg(ntrg),'(a,i1)') 'arcmode=',arcmode
      endif
C Atmospheric correction.
      if (DoAtm) then
        ntrg=ntrg+1
        trg(ntrg)='-atm'
      endif
C No vacuum wavelength scale.
      if (NoVac) then
        ntrg=ntrg+1
        trg(ntrg)='-novac'
      endif
C No heliocentric wavelength correction.
      if (NoHC) then
        ntrg=ntrg+1
        trg(ntrg)='-nohc'
      endif
C Sky shifting.
      if (.not.noskyshift) then
        ntrg=ntrg+1
        trg(ntrg)='-sky'
        if (user_shift.gt.-1.d+10) then
          write(c10,'(1pe10.3)') user_shift
          ntrg=ntrg+1
          write(trg(ntrg),'(2a)') 
     .  'skyshift=',c10(firstchar(c10):lc(c10))
        endif
      endif
C Variance.
      ntrg=ntrg+1
      trg(ntrg)='-var'
C PostScript plot.
      i = index(fluxspfile,'.fits')
      ntrg=ntrg+1
      trg(ntrg)='ps='//fluxspfile(1:i-1)//'-skyshift.ps'
C Sum-*.fits spectrum.
      if (sumspfile.ne.' ') then
        ntrg=ntrg+1
        trg(ntrg)='-sum'
      endif

C NOTE: The Err-*.fits file is created on this next step.  -tab 10oct2007

C Execute.
      write(c200,'(13(a,2x))')'arc2obj',trg(1)(1:lc(trg(1))),
     .                                  trg(2)(1:lc(trg(2))),
     .                                  trg(3)(1:lc(trg(3))),
     .                                  trg(4)(1:lc(trg(4))),
     .                                  trg(5)(1:lc(trg(5))),
     .                                  trg(6)(1:lc(trg(6))),
     .                                  trg(7)(1:lc(trg(7))),
     .                                  trg(8)(1:lc(trg(8))),
     .                                  trg(9)(1:lc(trg(9))),
     .                                  trg(10)(1:lc(trg(10))),
     .                                  trg(11)(1:lc(trg(11))),
     .                                  trg(12)(1:lc(trg(12)))
      write(soun,'(a)') c200(1:lc(c200))
      write(soun,'(2x)')
      call arc2obj_main(trg,ntrg,ok)
      write(soun,'(a)') 
     .  '.....................................................'
      write(soun,'(2x)')

C Copy wavelength scale to Sum- file.
      if (sumspfile.ne.' ') then
        ntrg=3
        trg(1)=fluxspfile
        trg(2)=sumspfile
        trg(3)='pxsh=0'
        write(c200,'(4(a,2x))') 'arc2arc',trg(1)(1:lc(trg(1))),
     .                                    trg(2)(1:lc(trg(2))),
     .                                    trg(3)(1:lc(trg(3)))
        write(soun,'(a)') c200(1:lc(c200))
        call arc2arc_main(trg,ntrg,ok)
        write(soun,'(2x)')
      endif

      ENDIF    ! (fluxspfile.ne.' ')...

      ENDIF    ! (DoWave)...


C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8

C................FINAL OUTPUT..........

C Write KOA tables.  Added -tab 10oct2007
      if (KOA_PRODUCTS) then
        write(soun,'(a)') 'NOTE: Writing KOA tables.'
C Grab final object header with wavelength scale.
        write(soun,'(2a)') 'Reading final Flux- file ',
     .                      fluxspfile(1:lc(fluxspfile))
        call readfits_header( fluxspfile, header, ok ) 
C Grab final arc header with wavelength scale and other info.
        if (ok) then
          write(soun,'(2a)') 'Reading final Arc- file ',
     .                        arcspfile1(1:lc(arcspfile1))
          call readfits_header( arcspfile1, temphead, ok ) 
          if (.not.ok) then
            temphead = header
            ok = .true.
          endif
        endif
        if (ok) then
          do i=1,Global_neo
            koa_global(3,i) = Global_trace_rms(i)
          enddo
          koa_global(1,1) = Global_eperdn
          koa_global(2,1) = MK_mpv
          ok = (koatblproducts( header, temphead, tbl_Flux, tbl_Var, 
     .          tbl_Flat, tbl_Sky, tbl_Sum, tbl_Arc,  mpp, tbl_eo, 
     .          tbl_sp, koa_global, pd_xp, pd_yp, pd_zp, pd_n ).eq.1)
        endif

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8
C#@#  real*8 tbl_eo(0:mpp,mo)
C     integer*4 tbl_mc(4)
C     real*4 tbl_Flux(maxcol*maxord),tbl_Var(maxcol*maxord)
C     real*4 tbl_Flat(maxcol*maxord),tbl_Sky(maxcol*maxord)
C     real*4 tbl_Sum(maxcol*maxord),tbl_Arc(maxcol*maxord)
C#@#  character*115200 header,temphead

      endif

C Write other plots, etc.
      IF (fluxspfile.ne.' ') THEN

      IF (flatspfile.ne.' ') THEN

C Create DN- file.
      ntrg=5
      trg(1)='mul'
      trg(2)=fluxspfile
      trg(3)=flatspfile
      dnspfile = 'DN-'//fluxspfile(6:lc(fluxspfile))
      trg(4)= dnspfile
      trg(5)= '-clobber'
      write(wrd,'(6(a,2x))')  'opim',trg(1)(1:lc(trg(1))),
     .                               trg(2)(1:lc(trg(2))),
     .                               trg(3)(1:lc(trg(3))),
     .                               trg(4)(1:lc(trg(4))),
     .                               trg(5)(1:lc(trg(5)))
      write(soun,'(a)') wrd(1:lc(wrd))
      call opim_main(trg,ntrg,ok)

C Scale DN- file.
      write(wrd,'(1pe14.7)') max(-1.,orig_scale_factor)
      if (wrd(1:1).eq.' ') wrd(1:1)='+'
      ntrg=4
      trg(1)= 'div'
      trg(2)= dnspfile
      trg(3)= 'const='//wrd(1:lc(wrd))
      trg(4)= '-clobber'
      write(wrd,'(5(a,2x))') 'opim',trg(1)(1:lc(trg(1))),
     .                              trg(2)(1:lc(trg(2))),
     .                              trg(3)(1:lc(trg(3))),
     .                              trg(4)(1:lc(trg(4)))
      write(soun,'(a)') wrd(1:lc(wrd))
      call opim_main(trg,ntrg,ok)

C Rename DN- file.
      call readfits(dnspfile,flat,maxpix,header,ok)
      wrd =chead('OBJECT',header)
      wrd = '[DN] '//wrd(1:lc(wrd))
      call cheadset('OBJECT',wrd,header)
      call writefits(dnspfile,flat,header,ok)

C Run spim0 on DN- file.
      ntrg=5
      trg(1)=dnspfile
      trg(2)='min=0.'
      trg(3)='bin=2'
      i = index(dnspfile,'.fits')
      trg(4)='ps='//dnspfile(1:i-1)//'.ps'
      if (KOA_PRODUCTS) then
        trg(5)='coflag=3'
      else 
        trg(5)='coflag=0'
      endif
      write(wrd,'(6(a,2x))') 'spim0',trg(1)(1:lc(trg(1))),
     .                               trg(2)(1:lc(trg(2))),
     .                               trg(3)(1:lc(trg(3))),
     .                               trg(4)(1:lc(trg(4))),
     .                               trg(5)(1:lc(trg(5)))
      write(soun,'(a)') wrd(1:lc(wrd))
      call spim0_main(trg,ntrg,ok)
      write(soun,'(2x)')

      ENDIF     ! (flatspfile.ne.' ')...

C Create S/N file.
      ntrg=5
      trg(1)='div'
      trg(2)=fluxspfile
      trg(3)= 'Err-'//fluxspfile(6:lc(fluxspfile))
      s2nspfile = 's2n-'//fluxspfile(6:lc(fluxspfile))
      trg(4)= s2nspfile
      trg(5)= '-clobber'
      write(wrd,'(6(a,2x))')  'opim',trg(1)(1:lc(trg(1))),
     .                               trg(2)(1:lc(trg(2))),
     .                               trg(3)(1:lc(trg(3))),
     .                               trg(4)(1:lc(trg(4))),
     .                               trg(5)(1:lc(trg(5)))
      if (access(trg(3),'r').eq.0) then
        write(soun,'(a)') wrd(1:lc(wrd))
        call opim_main(trg,ntrg,ok)
      endif

C Clip S/N file.
      ntrg=4
      trg(1)='min'
      trg(2)=s2nspfile
      trg(3)= 'const=0.'
      trg(4)= '-clobber'
      write(wrd,'(5(a,2x))') 'opim',trg(1)(1:lc(trg(1))),
     .                              trg(2)(1:lc(trg(2))),
     .                              trg(3)(1:lc(trg(3))),
     .                              trg(4)(1:lc(trg(4)))
      if (access(s2nspfile,'r').eq.0) then
        write(soun,'(a)') wrd(1:lc(wrd))
        call opim_main(trg,ntrg,ok)
C Rename S/N file.
        call readfits(s2nspfile,flat,maxpix,header,ok)
        wrd =chead('OBJECT',header)
        wrd = '[S/N] '//wrd(1:lc(wrd))
        call cheadset('OBJECT',wrd,header)
        call writefits(s2nspfile,flat,header,ok)
      endif

C Run spim0 on s2n- file.
      ntrg=5
      trg(1)=s2nspfile
      trg(2)='min=0.'
      trg(3)='bin=2'
      i = index(s2nspfile,'.fits')
      trg(4)='ps='//s2nspfile(1:i-1)//'.ps'
      if (KOA_PRODUCTS) then
        trg(5)='coflag=2'
      else 
        trg(5)='coflag=0'
      endif
      write(wrd,'(6(a,2x))') 'spim0',trg(1)(1:lc(trg(1))),
     .                               trg(2)(1:lc(trg(2))),
     .                               trg(3)(1:lc(trg(3))),
     .                               trg(4)(1:lc(trg(4))),
     .                               trg(5)(1:lc(trg(5)))
      if (access(s2nspfile,'r').eq.0) then
        write(soun,'(a)') wrd(1:lc(wrd))
        call spim0_main(trg,ntrg,ok)
        write(soun,'(2x)')
      endif

C Run spim0 on Flux- file.
      if (fluxspfile.eq.' ') fluxspfile=arcspfile1
      ntrg=5
      trg(1)=fluxspfile
      trg(2)='minp=-0.08'
      trg(3)='bin=2'
      i = index(fluxspfile,'.fits')
      trg(4)='ps='//fluxspfile(1:i-1)//'.ps'
      if (KOA_PRODUCTS) then
        trg(5)='coflag=1'
      else 
        trg(5)='coflag=0'
      endif
      write(wrd,'(6(a,2x))') 'spim0',trg(1)(1:lc(trg(1))),
     .                               trg(2)(1:lc(trg(2))),
     .                               trg(3)(1:lc(trg(3))),
     .                               trg(4)(1:lc(trg(4))),
     .                               trg(5)(1:lc(trg(5)))

      write(soun,'(a)') wrd(1:lc(wrd))
      call spim0_main(trg,ntrg,ok)
      write(soun,'(a)') 
     .  'NOTE: spectrum plot binning factor is 2 pixels.'
      write(soun,'(2x)')

C Run rhh on Flux- file.
      ntrg=1
      trg(1)=fluxspfile
      if (DoWave) then
        ntrg=ntrg+1
        trg(ntrg)='-wave'
      endif
      write(wrd,'(3(a,2x))') 'rhh',trg(1)(1:lc(trg(1))),
     .                             trg(2)(1:lc(trg(2)))
      write(soun,'(a)') wrd(1:lc(wrd))
      call rhh_main(trg,ntrg,ok)
      write(soun,'(2x)')

      ENDIF  ! (fluxspfile.ne.' ')...

C Print files.
      if (lc(printer).gt.1) then

        if (.not.lesspr) then
          write(wrd,'(3a)') 'trace-',cob(1:lc(cob)),'.ps'
          wrd = 
     .  'lpr -h -P'//printer(1:lc(printer))//'  '//wrd(1:lc(wrd))
          write(soun,'(a)') wrd(1:lc(wrd))
          call system(wrd)
        endif

        if (.not.lesspr) then
          write(wrd,'(3a)') 'profiles-',cob(1:lc(cob)),'.ps'
          wrd = 
     .  'lpr -h -P'//printer(1:lc(printer))//'  '//wrd(1:lc(wrd))
          write(soun,'(a)') wrd(1:lc(wrd))
          call system(wrd)
        endif

        if ((.not.lesspr).and.(DoWave)) then
          i = index(arcspfile1,'.fits')
          wrd = 'lpr -h -P'//printer(1:lc(printer))//
     .          '  '//arcspfile1(1:i-1)//'-fr.ps'
          write(soun,'(a)') wrd(1:lc(wrd))
          call system(wrd)
        endif

        if (fluxspfile.ne.' ') then
          i = index(fluxspfile,'.fits')
          wrd = 'lpr -h -P'//printer(1:lc(printer))//
     .          '  '//fluxspfile(1:i-1)//'.ps'
          write(soun,'(a)') wrd(1:lc(wrd))
          call system(wrd)
        endif

C
C       if ((.not.lesspr).and.(dnspfile.ne.' ')) then
C         i = index(dnspfile,'.fits')
C         wrd = 'lpr -h -P'//printer(1:lc(printer))//
C    .          '  '//dnspfile(1:i-1)//'.ps'
C         print '(a)',wrd(1:lc(wrd))
C         call system(wrd)
C       endif
C

        if ((.not.lesspr).and.(s2nspfile.ne.' ')) then
          i = index(s2nspfile,'.fits')
          wrd = 'lpr -h -P'//printer(1:lc(printer))//
     .          '  '//s2nspfile(1:i-1)//'.ps'
          write(soun,'(a)') wrd(1:lc(wrd))
          call system(wrd)
        endif

      endif

C Echo date.
      write(soun,'(a)') fdate()
      write(soun,'(a)') 'makee: All done.'

C Close log file.
      if (soun.ne.6) close(soun)

C Delete saved flat file.
      call delete_file(saveflatfile,ok)
      if (.not.ok) return

C Successful return.
      ok=.true.
      return
      end

C----------------------------------------------------------------------
C Set the raw filename for makee using the rawdir and fits extension.
C
      subroutine SetRawFilename(file,rawdir,ok)
C
      implicit none
      character*(*) file,rawdir
      logical ok
      character*80 f1,f2,f3,f4
      integer*4 lc,access
C First choice, file prepended by raw directory and appended with .fits .
      f1=file
      call AddFitsExt(f1)
      if (rawdir.ne.'./') then
        f1 = rawdir(1:lc(rawdir))//f1(1:lc(f1))
      endif
C Second choice, file appended with .fits extension.
      f2=file
      call AddFitsExt(f2)
C Third choice, basic filename.
      f3=file
C Fourth choice, file prepended by raw directory.
      f4=file
      f4 = rawdir(1:lc(rawdir))//f4(1:lc(f4))
      ok=.true.
      if (access(f1,'r').eq.0) then
        file = f1
      elseif (access(f2,'r').eq.0) then
        file = f2
      elseif (access(f3,'r').eq.0) then
        file = f3
      elseif (access(f4,'r').eq.0) then
        file = f4
      else
        ok=.false.
      endif
      return
      end


C------------------------------------------------------------------------
C A very simple object spectrum extraction.
C
      subroutine PSE_ObjSum(usc,uec,sc,ec,sr,er,obj,flat,sp1,sp2,
     .                      sky,ssr,ser,objsp,varsp)
C
      implicit none
      integer*4 usc,uec,sc,ec,sr,er
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er)
      real*8 sp1(*),sp2(*)
      integer*4 ssr,ser
      real*4 sky(sc:ec,ssr:ser),objsp(sc:ec),varsp(sc:ec)
C
C Need arr(..) from include file.
      include 'scratch.inc'
      include 'soun.inc'
C###  include 'pse.inc'
C
      integer*4 col,row,i,j,np,i1,i2,j1,j2,nmask,npix,nmsk
      real*4 rb1,rb2,vsum,sum,ssv,var,PSE_getvar,PSE_PixFrac,fr
      real*8 polyarroff
      logical warnyet,PSE_Masked,badcol

C Warning message flag.
      warnyet=.false.
       
C Clear spectra.
      do col=sc,ec
        objsp(col)=0.
        varsp(col)=0.
      enddo

      nmask=0
      DO col=usc,uec

C We will check for columns with a large fraction of masked pixels.
      badcol=.false.
C Determine object boundaries for this column.
      rb1= max(float(ssr),min(float(ser),
     .           sngl(polyarroff(sp1,dfloat(col)))))
      rb2= max(float(ssr),min(float(ser),
     .           sngl(polyarroff(sp2,dfloat(col)))))
C Warning.
      if ((.not.warnyet).and.(abs(rb1-rb2).lt.1.0)) then
        write(soun,'(a)') 
     .  'WARNING: PSE_ObjSum: Object width too small.'
        warnyet=.true.
      endif
C Calculate total flux and error for each column (account for partial pixels).
      sum =0.
      vsum=0.
C Check for a large fraction of masked pixels.
      nmsk=0
      npix=0
      do row=max(ssr,nint(rb1)-1),min(ser,nint(rb2)+1)
        npix=npix+1
        if (PSE_Masked(obj(col,row))) nmsk=nmsk+1
      enddo
      if ((float(nmsk)/float(max(1,npix))).gt.0.5) then
        badcol=.true.
      else
        do row=max(ssr,nint(rb1)-1),min(ser,nint(rb2)+1)
C Get sky-subtracted value and variance.
          ssv = obj(col,row) - sky(col,row)
          var = PSE_getvar(obj(col,row),flat(col,row))
C Masked pixel?  We must replace masked pixel with an appropriate value.
          if (PSE_Masked(ssv)) then
            nmask=nmask+1
            i1=max(sc,col-2)
            i2=min(ec,col+2)
            j1=max(sr,row-2)
            j2=min(er,row+2)
            np=0
            do i=i1,i2
            do j=j1,j2
              if (.not.PSE_Masked(obj(i,j))) then
                np=np+1
                arr(np) = obj(i,j) - sky(i,j)
              endif
            enddo
            enddo
            if (np.gt.0) then
              call find_median(arr,np,ssv)
            else
              badcol=.true.
              ssv=0.
            endif
            var = PSE_getvar((ssv+sky(col,row)),flat(col,row))
          endif
C Get fraction of pixel to include and accumulate flux.
          fr = PSE_PixFrac(row,rb1,rb2,col,sp1,sp2,(1+ec-sc))
          sum = sum + (fr*ssv)
C How should we accumulate variance?  This slightly overestimates variance
C (it should be fr*fr*var) but avoids unsightly periodic patterns.
          vsum= vsum+ (fr*var)
        enddo
      endif
C Store sum in object spectrum.
      if (badcol) then
        objsp(col) = 0.
        varsp(col) = 0.
      else 
        objsp(col) = sum
        varsp(col) = vsum
      endif
      ENDDO
C Warn user about pixel interpolations.
      if (nmask.gt.0) then
        write(soun,'(a,i6,a)') 
     .      'PSE_ObjSum: Had to deal with',nmask,' masked pixels.'
      endif

      return
      end


C----------------------------------------------------------------------
C Given two row points (rp1 and rp2), returns the flux ratio between them
C using the profile model: flux(rp1)/flux(rp2).  We also need the column (col)
C object boundaries polynomials (sp1,sp2) and the number of columns in image.
C
      real*4 function PSE_PixInterp(rp1,rp2,col,sp1,sp2,nc)
C
      implicit none
      real*4 rp1,rp2
      integer*4 col,nc
      real*8 sp1(*),sp2(*)
C
      include 'pse.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
      real*4 p1,p2,r1,r2,sn,cent,PSE_getvar
      real*8 polyarroff,polyval
      integer*4 i,i1,i2,np,ord,nn
      logical ok
C
C Center of profile at this column.
      cent=sngl((
     .     polyarroff(sp1,dfloat(col)) + polyarroff(sp2,dfloat(col))
     .     ) / 2.d0)
C Number of points in profile model.
      nn = nint(PSE_pparm(1))
C Convert row points to profile units.
      p1 = (rp1-cent)+PSE_pparm(4)
      p2 = (rp2-cent)+PSE_pparm(4)
C Signal-to-noise of profile at midpoint.
      i  = min(nn,max(1,nint((((p1+p2)/2.)-PSE_pparm(2))/
     .                                    PSE_pparm(3))+1))
      sn = PSE_pval(i)/sqrt(PSE_getvar(PSE_pval(i),1.))
C True signal-to-noise.
      sn = sqrt(float(nc))*sn
C Decide what order to use when fitting profile model.
      if (sn.gt.140.) then
        ord = 4
      elseif (sn.gt.40.) then
        ord = 3
      else
        ord = 2
      endif
C Fill array for fit.
C Note we have to use x8b,y8b,.., since x8,y8,w8 are used outside routine.
C Limits for points used in fit, use an extra half pixel on either side.
      r1 = min(p1,p2)
      r2 = max(p1,p2)
      i1 = nint(((r1-0.5)-PSE_pparm(2))/PSE_pparm(3))+1
      i2 = nint(((r2+0.5)-PSE_pparm(2))/PSE_pparm(3))+1
      np = 0
      do i=max(1,min(nn,i1)),max(1,min(nn,i2))
        np     = np + 1
        x8b(np)= dble( PSE_pparm(2)+(float(i-1)*PSE_pparm(3)) )
        y8b(np)= dble( PSE_pval(i) )
        w8b(np)= 1.d0
      enddo
C Be sure there are enough points for choosen order.
      if (np.lt.3) then
        PSE_PixInterp = 1.
        return
      endif
      ord = min(ord,max(0,np-2))
C Fit polynomial.
      call poly_fit_glls(np,x8b,y8b,w8b,ord,coefb,ok)
      if (.not.ok) write(soun,'(a)') 
     .  'ERROR: PSE_PixInterp: Fitting polynomial.'
C Find ratio between the two points.
      r1 = sngl(polyval(ord+1,coefb,dble(p1)))
      r2 = sngl(polyval(ord+1,coefb,dble(p2)))
      if ((r1.gt.1.e-6).and.(r2.gt.1.e-6)) then
        PSE_PixInterp = r1/r2
      else
        PSE_PixInterp = 1.
      endif
      return
      end


C----------------------------------------------------------------------
C Calculates the fraction of pixel flux which should be included in summation.
C Calculates the ratio of the flux between pix1 and rowpos and the flux
C between pix1 and pix2 in the profile model.  "col" is the current column
C number and sp1 and sp2 are the object-range-defining polynomials.  "nc" is
C the number of columns in the image.

      real*4 function PSE_PixFrac(row,rb1,rb2,col,sp1,sp2,nc)
      implicit none
      integer*4 row,col,nc
      real*4 rb1,rb2
      real*8 sp1(*),sp2(*)
C
      include 'pse.inc'
      include 'soun.inc'
      include 'scratch.inc'
C
      real*4 p1,p2,rp,sn,pix1,pix2,rb,cent
      real*4 PSE_getvar
      real*8 PSE_IntegPoly,polyarroff,r8
      integer*4 i,i1,i2,np,ord,nn
      logical ok
C Pixel edges.
      pix1 = float(row) - 0.5
      pix2 = float(row) + 0.5
C Pixel straddles first boundary.
      if ((pix1.lt.rb1).and.(pix2.gt.rb1)) then
        rb = rb1
C Pixel straddles second boundary.
      elseif ((pix1.lt.rb2).and.(pix2.gt.rb2)) then
        rb = rb2
C Pixel fully enclosed within boundaries.
      elseif ((pix1.ge.rb1).and.(pix2.le.rb2)) then
        PSE_PixFrac = 1.0
C Exit function.
        return
C Pixel fully outside of boundaries.
      else
        PSE_PixFrac = 0.0
C Exit function.
        return
      endif
C Pixel straddles boundary, so calculate partial flux.
C Center of profile at this column.
      cent = sngl((
     .       polyarroff(sp1,dfloat(col)) +
     .       polyarroff(sp2,dfloat(col))
     .       ) / 2.d0)
C Number of points in profile model.
      nn = nint(PSE_pparm(1))
C Convert parameters to profile units.
      p1 = (pix1-cent)+PSE_pparm(4)
      p2 = (pix2-cent)+PSE_pparm(4)
      rp =   (rb-cent)+PSE_pparm(4)
C Signal-to-noise at rp point.
      i  = min(nn,max(1,nint((rp-PSE_pparm(2))/PSE_pparm(3))+1))
      sn = PSE_pval(i)/sqrt(PSE_getvar(PSE_pval(i),1.))
C True signal-to-noise.
      sn = sqrt(float(nc))*sn
C Decide what order to use when fitting profile model.
      if (sn.gt.140.) then
        ord = 4
      elseif (sn.gt.40.) then
        ord = 3
      else
        ord = 2
      endif
C Fill array for fit, include one half pixel on either side of pixel.
      i1 = nint(((p1-0.5)-PSE_pparm(2))/PSE_pparm(3))+1
      i2 = nint(((p2+0.5)-PSE_pparm(2))/PSE_pparm(3))+1
      np = 0
      do i=min(nn,max(1,i1)),min(nn,max(1,i2))
        np     = np + 1
        x8(np) = dble( PSE_pparm(2)+(float(i-1)*PSE_pparm(3)) )
        y8(np) = dble( PSE_pval(i) )
        w8(np) = 1.d0
      enddo
C Any points?
      if (np.lt.1) then
        write(soun,'(a)')
     .  'ERROR: PSE_PixFrac: Fitting polynomial (severe error).'
        PSE_PixFrac=0.
        return
      endif
C Constrain order.
      ord=min(ord,np-1)
C Fit polynomial.
      call poly_fit_glls(np,x8,y8,w8,ord,coef,ok) 
      if (.not.ok) then
        write(soun,'(a)')
     .  'ERROR: PSE_PixFrac: Fitting polynomial (severe).'
        PSE_PixFrac=0.
        return
      endif
C Analytically integrate polynomial.
      r8 = PSE_IntegPoly(ord,coef,dble(p1),dble(p2))
      if (abs(sngl(r8)).gt.1.e-10) then
        if ((pix1.lt.rb1).and.(pix2.gt.rb1)) then
          PSE_PixFrac= 
     .  sngl( PSE_IntegPoly(ord,coef,dble(rp),dble(p2)) / r8 )
        else
          PSE_PixFrac= 
     .  sngl( PSE_IntegPoly(ord,coef,dble(p1),dble(rp)) / r8 )
        endif
      else
        PSE_PixFrac = 1.
      endif
      PSE_PixFrac = max(0.,min(1.,PSE_PixFrac))
      return
      end
C----------------------------------------------------------------------
C Analytically integrate a polynomial between x1 and x2.
      real*8 function PSE_IntegPoly(ord,coef,x1,x2)
      implicit none
      integer*4 ord,i
      real*8 coef(*),x1,x2,sum1,sum2
      sum1=0.d0
      do i=ord+1,1,-1
        sum1 = x1*( (coef(i)/dfloat(i)) + sum1 )
      enddo
      sum2=0.d0
      do i=ord+1,1,-1
        sum2 = x2*( (coef(i)/dfloat(i)) + sum2 )
      enddo
      PSE_IntegPoly = sum2 - sum1
      return
      end


C----------------------------------------------------------------------
C Append _#.fits to HIRES2 file.
      subroutine EE_append_ccdloc( rawfile, ccdloc, wrd )
      implicit none
      integer*4 ccdloc,ii
      character*(*) rawfile
      character*(*) wrd
C
      ii = index(rawfile,'.fits')
      if (ii.gt.0) then
        write(wrd,'(a,a,i1,a)') rawfile(1:ii-1),'_',ccdloc,'.fits'
      else
        wrd = rawfile
      endif
C
      return
      end


C------------------------------------------------------------------------
C Mask CRs in object region.  Uses a profile model derived from polynomials
C fit to a normalized profile in each column.  This normalized profile comes 
C from a median of a subbox along a row (in order to avoid CRs.)
C Account for fractional pixels at boundaries of spectral region.
C
      subroutine PSE_ObjMask(usc,uec,sc,ec,sr,er,obj,flat,sp1,sp2,
     .                       sky,prof,ssr,ser,objsp)
C
      implicit none
C
      integer*4 usc,uec,sc,ec,sr,er
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er)
      real*8 sp1(*),sp2(*)
      integer*4 ssr,ser
      real*4 sky(sc:ec,ssr:ser),prof(sc:ec,ssr:ser),objsp(sc:ec)
C
      include 'soun.inc'
      include 'scratch.inc'
      include 'global.inc'
      include 'pse.inc'
C
C Variables in pse.inc used in this subroutine:
C    x8,y8,w8,coef for polynomial fits
C    pfcd,rv,pv,pff for profile modeling
C
C Variables in global.inc used in this subroutine:
C    Global_HIRES, Global_obsnum, Global_eon, Global_xbin, Global_ybin,
C    Global_exposure
C
      integer*4 k1,k2,nn
      character c40*40, c9*9
      integer*4 col,row,npf,i,j,ii,np,ord,maxrej,hidevpt
      integer*4 i1,i2,j1,j2,nrej,nrejlow,nhi,nlo,losii,ncoef
      real*4 rb1,rb2,sum,rj,ow,maxow,rrs,cent,fr,losep,msn
      real*4 v1,fit,diff,sig,r1,r2,r3,r4,hidev,dev,siglim
      real*4 PSE_PixFrac,PSE_getvar,PSE_PixInterp
      real*8 polyval,polyarroff,xv
      logical warnyet,iterate,PSE_Masked,ok

C     real*4 rb3,rb4,skysig
C     integer*4 rowbnd(4)

C Set value.
      siglim = 2.5

C Warning message flag.
      warnyet=.false.
C Clear profile image and object spectrum.
      do col=sc,ec 
        objsp(col)=0.
        do row=ssr,ser
          prof(col,row)=0.
        enddo
      enddo
C Clear coeffecients for polynomial fits.
      do ii=1,mth
        do i=0,19
          pff(i,ii)=0.d0
        enddo
      enddo

C Check increment.
      if (PSEO_ProfInc.gt.0.7) then
        write(soun,'(a)') 'PSE_ObjMask: PSEO_ProfInc must be <0.7'
      endif
       
C:::::::::::: CREATE INITIAL PROFILE IMAGE ::::::::
C We avoid CRs by taking medians in the dispersion direction.  This creates
C a rather bad object spectrum, but is good enough for determining the object
C point spread function (profile).
C
C Profile values represent the normalized flux per unit pixel at a given row
C value (rather than an "infinite sampling" flux value).  We calculate profile
C points beyond the object boundaries, but we normalize using the flux within
C the boundaries.  This means that any given obj-sky pixel value divided by a
C profile value (near the center of that pixel) is equivalent to an estimate
C of the flux contained within the object boundaries.
C

C Create profile image "prof" using "ProfMdnBox" pixel medians.
      DO col=usc,uec
C Determine object boundaries for this column.
      rb1 = max(float(ssr),min(float(ser),
     .              sngl(polyarroff(sp1,dfloat(col)))))
      rb2 = max(float(ssr),min(float(ser),
     .              sngl(polyarroff(sp2,dfloat(col)))))
C Are we off the end of the CCD?
      IF( ((rb2-float(ssr)).lt.1.0).or.((float(ser)-rb1).lt.1.0) )THEN
C Set profile to zero for this column.
        do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
          prof(col,row)=0.
        enddo
      ELSE
C Warning.
      if ((.not.warnyet).and.(abs(rb1-rb2).lt.1.0)) then
        write(soun,'(a)') 
     .  'WARNING: PSE_ObjMask: Object width too small.'
        warnyet=.true.
      endif
C Calculate total flux (between boundaries) for each row.
C Account for partial pixels at the edges.
      sum=0.
      do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
C Get sky-subtracted profile value: prof(col,row).
C The size of the median box is set by PSEOM_ProfMdnBox in pse.inc .
        call PSE_ProfMdn(usc,uec,sc,ec,sr,er,obj,ssr,ser,
     .  sky,prof,col,row)
C Masked pixel?  If so, we will have to throw out entire column.
        if (PSE_Masked(prof(col,row))) then
          call PSE_Mask(objsp(col),1.)
        else 
          fr = PSE_PixFrac(row,rb1,rb2,col,sp1,sp2,(1+ec-sc))
C Accumulate sum, at this point prof(,) is equivalent to obj(,) minus sky(,).
          sum = sum + (fr*prof(col,row))
        endif
      enddo
      if (sum.lt.1.e-20) call PSE_Mask(objsp(col),1.)
C Column mask?
      if (.not.PSE_Masked(objsp(col))) then
C Store sum of profile in object spectrum.
        objsp(col) = sum
C Set "normalized" profile values, note generous increase in the row limits.
        do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
          prof(col,row) = prof(col,row) / sum
        enddo
      endif

      ENDIF
      ENDDO

C:::::::::::: SET PARAMETERS FOR POLYNOMIAL FITS ::::::::::::::::
C Define profile-centroid-distance array: pfcd(..) .
C Find maximum object width (maxow).
      maxow = 0.
      do col=usc,uec
        xv = dfloat(col)
        ow = abs(sngl(polyarroff(sp1,xv)-polyarroff(sp2,xv)))
        if (ow.gt.maxow) maxow=ow
      enddo
C Increase so we encompass the whole region.
      maxow = maxow + 2.
C Divide up range in terms of distance relative to center.
      npf=0
      do rrs=(maxow/-2.),((maxow/2.)+(PSEO_ProfInc/2.)),(PSEO_ProfInc)
        npf=npf+1
        pfcd(npf)=rrs
      enddo

C::::::::: FIT PROFILE POLYNOMIALS :::::::::::::::::::
C Using the crude profile image (of non-masked columns) just created, we
C attempt to fit polynomials in the dispersion direction.  We (usually)
C over-sample by fitting many more polynomials than there are pixels in the
C spatial direction.  We only use pixels whose centers are close to the
C polynomial row-point, since we want to avoid interpolation inaccuracies
C which occur near the peak of the profile.

C Fit profile polynomials: pff(..) .
      ord = PSE_ProOrder
C Fit each thread.
      do ii=1,npf
C Fill arrays. Allow for masks.
        np=0
        do col=usc,uec
          if (.not.PSE_Masked(objsp(col))) then
C Determine row point.
            xv  =dfloat(col)
            cent= abs(sngl(polyarroff(sp1,xv)+polyarroff(sp2,xv)))/2.
            rj  = cent+pfcd(ii)
C Is this point within PSEO_ProfIncSlop of center of a pixel?
            if (abs(rj-nint(rj)).lt.(PSEO_ProfIncSlop)) then
C Interpolate with profile model.
              row = nint(rj)
              if (((row-1).gt.ssr).and.((row+1).lt.ser)) then
                r1 = PSE_PixInterp(float(row),rj,col,sp1,sp2,(1+ec-sc))
                r2 = prof(col,row)/min(10.,max(0.1,r1))
C Fill array with interpolated value.
                np = np+1
                x8(np)= dfloat(col)
                y8(np)= dble(r2)
C Calculate original object+sky counts in order to get variance. (ObjMask)
                sum= objsp(col)
C Find profile variance and use inverse variance as weight.
                v1 = PSE_getvar(obj(col,row),flat(col,row))/(sum*sum)
                w8(np) = 1.d0 / dble(v1)
              endif
            endif
          endif
        enddo
C Fit polynomial.  Iterate, rejecting up to "maxrej" pixels
        maxrej= ( np / 10 )
        nrej  = 0
        iterate= .true.
        do while(iterate.and.(nrej.lt.maxrej))
          iterate=.false.
C Choose order by S/N if PSE_ProOrder < 0 . (ObjMask)
          if (PSE_ProOrder.lt.0) then
            call PSE_poly_fit_bysn(np,x8,y8,w8,ncoef,coef,msn,ok)
            if (.not.ok) then
              write(soun,'(a)') 
     .  'ERROR: ObjMask: Fitting (bysn) polynomial.'
            endif
            ord=ncoef-1
            write(c9,'(f6.1,i3)') 
     .  max(0.,min(9999.,msn)),max(0,min(99,ord))
          else
            call poly_fit_glls(np,x8,y8,w8,ord,coef,ok)
            if (.not.ok) write(soun,'(a)') 
     .  'ERROR: ObjMask: Fitting polynomial.'
            c9=' '
          endif
          hidev=0.
          hidevpt=0
          do i=1,np
            if (w8(i).gt.0.) then
              fit = sngl( polyval(ord+1,coef,x8(i)) )
              sig = sqrt(1./sngl(w8(i)))
              diff= sngl(y8(i))-fit
              dev = abs(diff/sig)
              if (dev.gt.hidev) then
                hidev = dev
                hidevpt = i
              endif
            endif
          enddo
          if (hidev.gt.PSEOM_ProRejLim*PSEMRA) then
            w8(hidevpt) = 0.d0
            nrej=nrej+1
            iterate=.true.
          endif
        enddo
        if (nrej.gt.maxrej/2) then
         write(soun,'(a,i4,a,i4,a,i4)') 
     .  ' Rejecting',min(9999,max(-999,nrej)),
     .         ' out of',min(9999,max(-999,np)),
     .         ' points in profile fit#',min(9999,max(-999,ii))
        endif
        if (nrej.ge.maxrej) then
          write(soun,'(a,i4)')
     .    ' NOTE: PSE_ObjMask: Maximum rejections in profile fit#',
     .    min(9999,max(-999,ii))
        endif
C Load coeffecients.
        pff(1,ii)=dfloat(ord+1)
        do i=2,ord+2
          pff(i,ii)=coef(i-2)
        enddo

C...............
C Show polynomials.                   (ObjMask)
        IF (profpoly) THEN
C Open plot.
        if (ii.eq.1) then
          k1=max(-1,min(999,Global_obsnum))
          k2=max(-1,min(99,Global_eon))
          c40=' '
          write(c40,'(a,I3.3,a,I2.2,a)') 
     .  'profpoly_om_',k1,'-',k2,'.ps/PS'
          write(soun,'(2a)') 'Writing PostScript plot file: ',c40
          call EE_OpGrid(npf,k1,k2)
          call PGBEGIN(0,c40,+1*k1,k2)
          call PGASK(.false.)
          call PGSLW(1)
          call PGSLS(1)
          call PGSCF(1)
          call PGSCH(2.5)
        endif
        call PGPAGE     
        nn=0
        do i=1,np
          nn=nn+1 
          b(nn)  =sngl(x8(i))
          arr(nn)=sngl(y8(i))
        enddo
        call EE_ArrStat(nn,arr,r1,r2,r3,r4,bx)

C Just in case.
        if (abs(r1-r2).lt.1.e-10) then
          r1 = ((r1+r2)/2.) - 1.e-10
          r2 = ((r1+r2)/2.) + 1.e-10
        endif

C Plot data.
        call PGWINDOW(b(1),b(nn),r1,r2)
        call PGBOX('BCNST',0.,0,'BCNST',0.,0)
        call PGPT(nn,b,arr,-1)  
        nn=0
        do i=sc,ec,30
          nn=nn+1 
          b(nn)  =float(i)
          arr(nn)=sngl(polyarroff(pff(0,ii),dfloat(i)))
        enddo
C Plot fit.
        call PGLINE(nn,b,arr)
        c40=' '
        if (ii.eq.1) then
          write(c40,'(i3,f7.2,a,i3,a,i4)') ii,pfcd(ii),
     .              '   Ord#',min(999,max(-1,Global_eon)),
     .              '   Obs#',min(9999,max(-1,Global_obsnum))
        else
          if (pfcd(ii).gt.0.) then
            if (pfcd(ii).lt.10.) then
              write(c40,'(i3,a,f4.2)') ii,'  +',pfcd(ii)
            else
              write(c40,'(i3,a,f5.2)') ii,' +',pfcd(ii)
            endif
          else
            write(c40,'(i3,f7.2)') ii,pfcd(ii)
          endif
        endif
        call PGMTEXT('T',0.3,0.01,0.0,c40)
        call PGMTEXT('B',-1.1,0.03,0.0,c9)
C Close plot.
        if (ii.eq.npf) call PGEND
        ENDIF
C...............

      enddo

C:::::: TRANSFER PROFILE FIT POLYNOMIALS TO PROFILE IMAGE ::::::
C We now create a much better profile image using the (over-sampled) profile
C fit polynomials.  Copy fit to profile, leave any values "defined as zero".
      DO col=usc,uec
C Create profile at each column. Determine boundaries for this column.
      xv  = dfloat(col)
      r1  = sngl(polyarroff(sp1,xv))
      r2  = sngl(polyarroff(sp2,xv))
      cent= (r1+r2)/2.
      rb1 = max(float(ssr),min(float(ser),r1))
      rb2 = max(float(ssr),min(float(ser),r2))
C Set row-values (rv) and profile-values (pv)
      do ii=1,npf
        rv(ii) = cent + pfcd(ii)
        pv(ii) = sngl(polyarroff(pff(0,ii),xv))
      enddo
C Set each row.
      do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
        rj=float(row)
C Clear profile value.
        prof(col,row)=0.
C Is the pixel center inside the polynomial fits row-value range?
        if ((rj.gt.rv(1)).and.(rj.lt.rv(npf))) then
C Find closest thread.
          losep=abs(rv(1)-rj)
          losii=1
          do ii=2,npf
            if (abs(rv(ii)-rj).lt.losep) then
              losep=abs(rv(ii)-rj)
              losii=ii
            endif
          enddo
C Use profile to interpolate to row center.
          r1 = PSE_PixInterp(rj,rv(losii),col,sp1,sp2,(1+ec-sc))
          prof(col,row) = pv(losii)*min(10.,max(0.1,r1))
C Set small or negative values to zero.
          if (prof(col,row).lt.1.e-20) prof(col,row)=0.
        endif
      enddo
      ENDDO

C::::::: CREATE NEW OBJECT TOTALS :::::::::::::::::::::::::::::::
C Using the profile fits we can create a much more accurate object spectrum,
C while still avoiding (most) CRs.
C Make new and improved sky-subtracted sum, put in objsp(..).
C Note that (by using the profile model) each pixel in a column is an estimate
C   for the total flux in that column.
C We use all the pixels in a 3 column by N row box centered on the object
C   centroid in a given column (where N is the number of object pixels within
C   that column minus one), to find a median value for the total flux.
C We look for and ignore very deviant pixels before finding the median.
C We also decide whether we might be looking at a sharp emiss/abs line, in
C   which case we only use a 1 column by N row box.

      DO col=usc,uec
C Define column range for median box (may be reduced for sharp line).
      i1 = max(sc,col-1)
      i2 = min(ec,col+1)
C Define row bounds for median box.
      r1 = sngl(polyarroff(sp1,dfloat(col)))
      r2 = sngl(polyarroff(sp2,dfloat(col)))
      cent = (r1+r2)/2.
      j1 = nint(r1)+1
      j2 = nint(r2)-1
      if ((j2-j1).lt.3) then
        j1=j1-1
        j2=j2+1
      endif
      j1 = min(ser,max(ssr,j1))
      j2 = min(ser,max(ssr,j2))
C Decide whether we are on top of a very sharp, strong abs/emiss line.
      nhi=0
      nlo=0 
      i  =col
      do j=j1,j2
        if ((obj(i,j).gt.0.).and.(obj(i1,j).gt.0.).and.
     .                                  (obj(i2,j).gt.0.)) then
          r1 =(obj(i,j)+obj(i1,j)+obj(i2,j))/3.
          sig=PSE_getvar(r1,flat(i,j))
          if ( (((obj(i,j)-obj(i1,j))/sig).gt.siglim).and.
     .         (((obj(i,j)-obj(i2,j))/sig).gt.siglim) ) nhi=nhi+1
          if ( (((obj(i1,j)-obj(i,j))/sig).gt.siglim).and.
     .         (((obj(i2,j)-obj(i,j))/sig).gt.siglim) ) nlo=nlo+1
        endif
      enddo
C Make sure its not a big off center CR.
      j=max(ssr,min(ser,nint(cent)))
      if ((obj(i,j).gt.obj(i,j1)).and.(obj(i,j).gt.obj(i,j2)).and.
     .    (obj(i,j).gt.0.).and.(obj(i,j1).gt.0.).and.
     .                              (obj(i,j2).gt.0.)) then
C If 4 out of 5 pixels agree, we have a line.
        if (nhi.ge.abs(j2-j1)) then
          write(soun,'(a,2i6)') 
     . 'NOTE: PSE_ObjMask: Sharp, strong emission line at (r,c)=',
     . j,col
          i1=col
          i2=col
        endif
        if (nlo.ge.abs(j2-j1)) then
          write(soun,'(a,2i6)')
     . 'NOTE: PSE_ObjMask: Sharp, strong absorption line at (r,c)=',
     .  j,col
          i1=col
          i2=col
        endif
      endif
C Load array.
      np=0
      do i=i1,i2 
      do j=j1,j2 
        if ((prof(i,j).gt.0.).and.(.not.PSE_Masked(obj(i,j)))) then
          np=np+1
          arr(np) = (obj(i,j)-sky(i,j))/prof(i,j)
        endif
      enddo
      enddo
C Find median.
      if (np.lt.1) then
        call PSE_Mask(objsp(col),1.)
      else
        call find_median(arr,np,objsp(col))
C Look for deviant pixels (twice the size of the Big Limit) and redo median.
C Use the current objsp(col) for all pixels.
        np=0
        do i=i1,i2
        do j=j1,j2
          if ((prof(i,j).gt.0.).and.(.not.PSE_Masked(obj(i,j)))) then
            r1 = obj(i,j)-sky(i,j)
            r2 = prof(i,j)*objsp(col)
CCC Added the sky to the variance computation...  -tab 20jun00
CCC         r3 = sqrt(PSE_getvar( r2            ,flat(i,j)))
            r3 = sqrt(PSE_getvar((r2 + sky(i,j)),flat(i,j)))
            sig= (r1-r2)/r3
            if (sig.lt.(2.*PSEOM_ObjRejLimBig*PSEMRA)) then
              np=np+1
              arr(np)=r1/prof(i,j)
            endif
          endif
        enddo
        enddo
        if (np.lt.1) then
          objsp(col)=0.
        else
          call find_median(arr,np,objsp(col))
        endif
      endif 
      ENDDO

C:::::::: MASKING CRs :::::::::::::::::::::::::::
C With a fully defined profile image we can now search for and mask CRs.
C Look for CRs, search for satellites near big CRs.
      nrej=0
      nrejlow=0
      DO col=usc,uec
      IF (.not.PSE_Masked(objsp(col))) THEN

C Determine object boundaries for this column.
      rb1 = max(float(ssr),min(float(ser),
     .               sngl(polyarroff(sp1,dfloat(col)))))
      rb2 = max(float(ssr),min(float(ser),
     .               sngl(polyarroff(sp2,dfloat(col)))))
      do row=max(ssr,nint(rb1)),min(ser,nint(rb2))
        if (.not.PSE_Masked(obj(col,row))) then
          r1 = obj(col,row)-sky(col,row)
          r2 = prof(col,row)*objsp(col)
CCC Added the sky to the variance computation...  -tab 20jun00
CCC       r3 = sqrt(PSE_getvar( r2                ,flat(col,row)))
          r3 = sqrt(PSE_getvar((r2 + sky(col,row)),flat(col,row)))
          sig= (r1-r2)/r3
C Mask deviant pixels.
          if (sig.gt.PSEOM_ObjRejLim*PSEMRA) then
            call PSE_Mask(obj(col,row),sig)
            nrej=nrej+1
          elseif (abs(sig).gt.PSEOM_ObjRejLim*PSEMRA) then
            call PSE_Mask(obj(col,row),sig)
            nrejlow=nrejlow+1
          endif
C If this was a BIG CR, look for satellites.
          if (sig.gt.(PSEOM_ObjRejLimBig*PSEMRA)) then
C Adjacent satellites. Mask if > ObjRejLimSat , but < ObjRejLimBig .
            do i=max(usc,col-1),min(uec,col+1)
            do j=max(ssr,row-1),min(ser,row+1)
              r1 = obj(i,j)-sky(i,j)
              r2 = prof(i,j)*objsp(i)
CCC Added the sky to the variance computation...  -tab 20jun00
CCC           r3 = sqrt(PSE_getvar( r2            ,flat(i,j)))
              r3 = sqrt(PSE_getvar((r2 + sky(i,j)),flat(i,j)))
              sig= (r1-r2)/r3
              if ( (.not.PSE_Masked(obj(i,j))).and.
     .             (sig.gt.PSEOM_ObjRejLimSat*PSEMRA).and.
     .             (sig.lt.(PSEOM_ObjRejLimBig*PSEMRA)) ) then
                call PSE_Mask(obj(i,j),sig)
                nrej=nrej+1
              endif
            enddo
            enddo
C Distant satellites.  Mask if > ObjRejLimSat , < ObjRejLimBig , and the pixel
C between it and the central pixel has been masked.
C Added a binning check for distant satellite CR pixels.   -tab 21jun00
            IF ((Global_ybin/Global_xbin).eq.2) THEN
            do ii=-1,1,2
              i  = col+(2*ii)
              r1 = obj(i,row)-sky(i,row)
              r2 = prof(i,row)*objsp(i)
CCC Added the sky to the variance computation...  -tab 20jun00
CCC           r3 = sqrt(PSE_getvar( r2              ,flat(i,row)))
              r3 = sqrt(PSE_getvar((r2 + sky(i,row)),flat(i,row)))
              sig= (r1-r2)/r3
              if ( (.not.PSE_Masked(obj(i,row))).and.
     .             (PSE_Masked(obj(col+ii,row))).and.
     .             ( sig.gt.PSEOM_ObjRejLimSat*PSEMRA ).and.
     .             (sig.lt.(PSEOM_ObjRejLimBig*PSEMRA)) ) then
                call PSE_Mask(obj(i,row),sig)
                nrej=nrej+1
              endif
            enddo
            ENDIF
          endif
        endif        
      enddo 

      ENDIF
      ENDDO 
C
C Echo info to user.
      write(soun,'(a,2i6)') 
     . '                Object Pixels Masked (high,low):',nrej,nrejlow
      Global_obj_rej = nrej + nrejlow  ! added -tab 26nov07
C
      return
      end

C------------------------------------------------------------------------
C Create final object spectrum and variance.
C Fit polynomials to model profile, ignore masked pixels.
C Account for fractional pixels at boundaries of spectral region.

      subroutine PSE_ObjFlux(usc,uec,sc,ec,sr,er,obj,flat,sp1,sp2,
     .                       sky,prof,ssr,ser,objsp,varsp)

      implicit none
      integer*4 usc,uec,sc,ec,sr,er
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er)
      real*8 sp1(*),sp2(*)
      integer*4 ssr,ser
      real*4 sky(sc:ec,ssr:ser),prof(sc:ec,ssr:ser)
      real*4 objsp(sc:ec),varsp(sc:ec)
C Save object spectrum.
      real*4 obj_est(1:9000),flat_est(1:9000),obj_est_temp(1:9000)

C Variables in pse.inc used in this subroutine:
C    x8,y8,w8,coef for polynomial fits.
C    pfcd,rv,pv,pff for profile modeling.
C
      include 'pse.inc'
      include 'scratch.inc'
      include 'global.inc'
      include 'soun.inc'
C
      integer*4 col,row,npf,i,ii,np,ord,nn,losii,ncoef
      integer*4 npx,nmk,k1,k2
      real*4 rb1,rb2,sum,ow,maxow,rrs,cent,fr,wsum,vsum,wt,losep
      real*4 v1,PSE_getvar,r1,r2,r3,r4,pfi,obi,voi,rj,msn
      real*4 PSE_PixFrac,PSE_PixInterp
      real*8 polyarroff,xv
      logical PSE_Masked,ok
      character c40*40,c9*9

C Initial.
      sum=0.
      vsum=0.

C Clear profile image and object spectrum.
      do col=sc,ec 
        objsp(col)=0.
        do row=ssr,ser
          prof(col,row)=0.
        enddo
      enddo
C Clear coeffecients for polynomial fits.
      do ii=1,mth
        do i=1,19
          pff(i,ii)=0.d0
        enddo
      enddo

C:::::::::::: CREATE PROFILE IMAGE ::::::::
C Profile values represent the normalized flux per unit pixel at a given row
C value (rather than an "infinite sampling" flux value).  We calculate profile
C points beyond the object boundaries, but we normalize using the flux within
C the boundaries.  This means that any given obj-sky pixel value divided by a
C profile value (near the center of that pixel) is equivalent to an estimate
C of the flux contained within the object boundaries.

C Create profile image, ignore masked pixels.
      DO col=usc,uec
C Determine object boundaries for this column.
      rb1= max(float(ssr),min(float(ser),
     .           sngl(polyarroff(sp1,dfloat(col)))))
      rb2= max(float(ssr),min(float(ser),
     .           sngl(polyarroff(sp2,dfloat(col)))))
C Are we off the end of the CCD?
      IF ( ((rb2-float(ssr)).lt.1.0).or.
     .               ((float(ser)-rb1).lt.1.0) ) THEN
C Set profile to zero for this column.
        do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
          prof(col,row)=0.
        enddo
      ELSE
C Calculate total flux (between boundaries) for each row.
C Account for partial pixels at the edges.
        sum=0.
        do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
C Masked pixel?  If so, we will have to throw out entire column.
          if (PSE_Masked(obj(col,row))) then
            call PSE_Mask(objsp(col),1.)
          else
C Accumulate sum.
            fr = PSE_PixFrac(row,rb1,rb2,col,sp1,sp2,(1+ec-sc))
            sum= sum + (fr*(obj(col,row)-sky(col,row)))
          endif
        enddo
C If sum is less than or equal to a small number, throw out the entire column.
        if (sum.lt.1.e-20) call PSE_Mask(objsp(col),1.)
        if (.not.PSE_Masked(objsp(col))) then
C Store sum of profile in object spectrum.
          objsp(col) = sum
C Set "normalized" profile values, note generous increase in the row limits.
          do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
            prof(col,row) = (obj(col,row)-sky(col,row)) / sum
          enddo
        endif
      ENDIF
      ENDDO

C:::::::: SET PARAMETERS FOR POLYNOMIAL FITS :::::::::::::: 
C Define profile-centroid-distance array: pfcd(..) .
C First find maximum object width (maxow).
      maxow = 0.
      do col=usc,uec
        xv = dfloat(col)
        ow = abs(sngl(polyarroff(sp1,xv)-polyarroff(sp2,xv)))
        if (ow.gt.maxow) maxow=ow
      enddo
C Increase so we encompass the whole region.
      maxow = maxow + 2.
C Divide up range in terms of distance relative to centroid.
      npf=0
      do rrs=(maxow/-2.),((maxow/2.)+(PSEO_ProfInc/2.)),(PSEO_ProfInc)
        npf=npf+1
        pfcd(npf)=rrs
      enddo

C::::::::: FIT PROFILE POLYNOMIALS :::::::::::::::::::
C Using the profile image (of non-masked columns) we attempt to fit polynomials
C in the dispersion direction.  We (usually) over-sample by fitting many more
C polynomials than there are pixels in the spatial direction.  We only use
C pixels whose centers are close to the polynomial row-point, since we want
C to avoid interpolation inaccuracies which occur near the peak of the profile.

C Fit profile polynomials: pff(..) .
      ord   = PSE_ProOrder
C Fit each thread.
      do ii=1,npf
C Fill arrays. Allow for masks.
        np=0
        do col=usc,uec
          if (.not.PSE_Masked(objsp(col))) then
C Determine row point.
            xv  = dfloat(col)
            cent= abs(sngl(polyarroff(sp1,xv)+polyarroff(sp2,xv)))/2.
            rj  = cent+pfcd(ii)
C Is this point within PSEO_ProfIncSlop of center of a pixel?
            if (abs(rj-nint(rj)).lt.(PSEO_ProfIncSlop)) then
C Interpolate with profile model.
              row = nint(rj)
              if (((row-1).gt.ssr).and.((row+1).lt.ser)) then
                r1 = PSE_PixInterp(float(row),rj,col,sp1,sp2,(1+ec-sc))
                r2 = prof(col,row)/min(10.,max(0.1,r1))
C Fill array with interpolated value.
                np = np+1
                x8(np)= dfloat(col)
                y8(np)= dble(r2)
C Calculate original object+sky counts in order to get variance.
                sum= objsp(col)
C Find profile variance and use inverse variance as weight.
                v1 = PSE_getvar(obj(col,row),flat(col,row))/(sum*sum)
                w8(np) = 1.d0 / dble(v1)
              endif
            endif
          endif
        enddo
C Fit polynomial.  Choose order by S/N if PSE_ProOrder < 0 .
        if (PSE_ProOrder.lt.0) then
          call PSE_poly_fit_bysn(np,x8,y8,w8,ncoef,coef,msn,ok)
          if (.not.ok) write(soun,'(a)')
     .  'ERROR: PSE_ObjFlux: Fitting(bysn)polynomial.'
          ord=ncoef-1
          write(c9,'(f6.1,i3)') 
     .  max(0.,min(9999.,msn)),max(0,min(99,ord))
        else
          call poly_fit_glls(np,x8,y8,w8,ord,coef,ok)
          if (.not.ok) write(soun,'(a)') 
     .  'ERROR: PSE_ObjFlux: Fitting polynomial.'
          c9 = ' '
        endif
C Load coeffecients.
        pff(1,ii)=dfloat(ord+1)
        do i=2,ord+2
          pff(i,ii)=coef(i-2)
        enddo

C...............
C Show polynomials.                         (ObjFlux)
        IF (profpoly) THEN
C Open plot.
        if (ii.eq.1) then
          k1=max(-1,min(999,Global_obsnum))
          k2=max(-1,min(99,Global_eon))
          c40=' '
          write(c40,'(a,I3.3,a,I2.2,a)') 'profpoly_',k1,'-',k2,'.ps/PS'
          write(soun,'(2a)') 'Writing PostScript plot file: ',c40
          call PGBEGIN(0,c40,1,1)
          call PGASK(.false.)
          call PGSLW(1)
          call PGSLS(1)
          call PGSCF(1)
          call PGSCH(2.5)
        endif
        call PGPAGE     
        nn=0
        do i=1,np
          nn=nn+1 
          b(nn)  =sngl(x8(i))
          arr(nn)=sngl(y8(i))
        enddo
        call EE_ArrStat(nn,arr,r1,r2,r3,r4,bx)
C Just in case.
        if (abs(r1-r2).lt.1.e-10) then
          r1 = ((r1+r2)/2.) - 1.e-10
          r2 = ((r1+r2)/2.) + 1.e-10
        endif

        call PGWINDOW(b(1),b(nn),r1,r2)
        call PGBOX('BCNST',0.,0,'BCNST',0.,0)
C Plot data.
        call PGPT(nn,b,arr,-1)  
        nn=0
        do i=sc,ec,30
          nn=nn+1 
          b(nn)  =float(i)
          arr(nn)=sngl(polyarroff(pff(0,ii),dfloat(i)))
        enddo
C Plot fit.
        call PGLINE(nn,b,arr)
        c40=' '
        if (ii.eq.1) then
          write(c40,'(i3,f7.2,a,i3,a,i4)') ii,pfcd(ii),
     .              '   Ord#',min(999,max(-1,Global_eon)),
     .              '   Obs#',min(9999,max(-1,Global_obsnum))
        else
          if (pfcd(ii).gt.0.) then
            if (pfcd(ii).lt.10.) then
              write(c40,'(i3,a,f4.2)') ii,'  +',pfcd(ii)
            else
              write(c40,'(i3,a,f5.2)') ii,' +',pfcd(ii)
            endif
          else
            write(c40,'(i3,f7.2)') ii,pfcd(ii)
          endif
        endif
        call PGMTEXT('T',0.3,0.01,0.0,c40)
        call PGMTEXT('B',-1.1,0.03,0.0,c9)
C Close plot.
        if (ii.eq.npf) call PGEND
        ENDIF
C...............

      enddo

C:::::: TRANSFER PROFILE FIT POLYNOMIALS TO PROFILE IMAGE ::::::
C Create an essentially noiseless profile image using the polynomial fits.
C Remember the profile values are in "normalized" flux per pixel.
C We interpolate between closest threads. We require values to be non-negative.
C
C Copy fit to profile image.
      DO col=usc,uec
C Determine boundaries for this column.
      xv  = dfloat(col)
      r1  = sngl(polyarroff(sp1,xv))
      r2  = sngl(polyarroff(sp2,xv))
      cent= (r1+r2)/2.
      rb1 = max(float(ssr),min(float(ser),r1))
      rb2 = max(float(ssr),min(float(ser),r2))
C Set row-values (rv) and profile-values (pv)
      do ii=1,npf
        rv(ii) = cent + pfcd(ii)
        pv(ii) = sngl(polyarroff(pff(0,ii),xv))
      enddo
C Set each row.
      do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
        rj=float(row)
C Clear profile value.
        prof(col,row)=0.
C Is the pixel center outside the polynomial fits row-value range?
        if ((rj.gt.rv(1)).and.(rj.lt.rv(npf))) then
C Find closest thread.
          losep=abs(rv(1)-rj)
          losii=1
          do ii=2,npf
            if (abs(rv(ii)-rj).lt.losep) then
              losep=abs(rv(ii)-rj)
              losii=ii
            endif
          enddo
C Use profile to interpolate to row center.
          r1 = PSE_PixInterp(rj,rv(losii),col,sp1,sp2,(1+ec-sc))
          prof(col,row) = pv(losii)*min(10.,max(0.1,r1))
C Set small or negative values to zero.
          if (prof(col,row).lt.1.e-20) prof(col,row)=0.
        endif
      enddo
      ENDDO

C:::: RE-NORMALIZE NEW PROFILE:::::
C Make sure the new profile is consistent with the requirement that each
C profile value represents the flux value at that point divided by the total
C flux between the row boundaries.
C
      DO col=usc,uec
C Determine object boundaries for this column.
      rb1 = max(float(ssr),min(float(ser),
     .            sngl(polyarroff(sp1,dfloat(col)))))
      rb2 = max(float(ssr),min(float(ser),
     .            sngl(polyarroff(sp2,dfloat(col)))))
C Are we off the end of the CCD?
      IF( ((rb2-float(ssr)).lt.1.0).or.((float(ser)-rb1).lt.1.0) )THEN
C Set profile to zero for this column.
        do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
          prof(col,row)=0.
        enddo
      ELSE
C Calculate total flux (between boundaries) for each row.
C Account for partial pixels at the edges.
        sum=0.
        do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
          fr = PSE_PixFrac(row,rb1,rb2,col,sp1,sp2,(1+ec-sc))
          sum= sum + (fr*prof(col,row))
        enddo
C If sum is less than or equal to a small number, throw out the entire column.
        if (sum.lt.1.e-10) then
          do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
            prof(col,row)=0.
          enddo
        else
C Set "re-normalized" profile values.
          do row=max(ssr,nint(rb1)-2),min(ser,nint(rb2)+2)
            prof(col,row) = prof(col,row) / sum
          enddo
        endif
      ENDIF
      ENDDO

C::::::::: NEXT-TO-FINAL FLUX CALCULATION ::::::::::::::::::::::::::::::::::
C Estimate object flux and flat field level beneath object for each column
C in order to get a better variance estimate for the final flux calculation.
      DO col=usc,uec
C Determine object range row boundaries.
      rb1 = max(float(ssr),min(float(ser),
     .            sngl(polyarroff(sp1,dfloat(col)))))
      rb2 = max(float(ssr),min(float(ser),
     .            sngl(polyarroff(sp2,dfloat(col)))))
C Calculate flux.
      sum =0.
      wsum=0.
      do row=max(ssr,nint(rb1)+1),min(ser,nint(rb2)-1)
        if ((.not.PSE_Masked(obj(col,row))).and.
     .                       (prof(col,row).gt.0.)) then
          fr = PSE_PixFrac(row,rb1,rb2,col,sp1,sp2,(1+ec-sc))
C Consider only whole pixels.
          if (fr.gt.0.999) then
            pfi = prof(col,row)
            obi = (obj(col,row)-sky(col,row))
            voi = PSE_getvar(obj(col,row),flat(col,row))
            wt  = fr*pfi*pfi/voi
            sum = sum + (wt*(obi/pfi))
            wsum= wsum+ (wt)
          endif
        endif
      enddo
      if (wsum.gt.0.) then
        obj_est(col) = sum / wsum
      else
        obj_est(col) = 0.
      endif
C PLEASE NOTE: Since obj_est is only used for variance calculations,
C PLEASE NOTE:   we make sure that this value is also non-negative.
      obj_est(col) = max(0.,obj_est(col))
C Calculate flat field level.
      sum =0.
      wsum=0.
      do row=max(ssr,nint(rb1)+1),min(ser,nint(rb2)-1)
        sum = sum + flat(col,row)
        wsum= wsum+ 1.
      enddo
      if (wsum.gt.0.) then
        flat_est(col) = sum / wsum
      else
        flat_est(col) = 
     .           flat( col, max(ssr,min(ser,nint((rb1+rb2)/2.))) )
      endif
      ENDDO

C PLEASE NOTE: Here we avoid having zero values in obj_est().
C...Replace zero object estimates with estimates from surrounding pixels...
      DO col=usc,uec
        obj_est_temp(col)=obj_est(col)
      ENDDO
      DO col=usc,uec
        if (obj_est(col).lt.1.e-30) then
          sum =0.
          wsum=0.
          do i=max(usc,col-2),min(uec,col+2)
            if (obj_est_temp(i).gt.0.) then
              sum =sum +obj_est_temp(i)
              wsum=wsum+1.
            endif
          enddo
          if (wsum.gt.0.) then
            obj_est(col) = sum / wsum
          else
            sum =0.
            wsum=0.
            do i=max(usc,col-4),min(uec,col+4)
              if (obj_est_temp(i).gt.0.) then
                sum =sum +obj_est_temp(i)
                wsum=wsum+1.
              endif
            enddo
            if (wsum.gt.0.) obj_est(col) = sum / wsum
          endif
        endif
      ENDDO

C:::::: FINAL FLUX AND VARIANCE CALCULATION AT EACH COLUMN :::::::::::::::
C Given the re-normalized profile image, each pixel is a measure of the total
C flux in each column between the object boundaries. The weights are the
C inverse variance of that measure.  Ignore masked pixels.  Reduce the weight
C for the fractional pixel at the object boundary.
C
C Clear object and variance spectra.
      do col=sc,ec 
        objsp(col)=0.
        varsp(col)=0.
      enddo
C Find flux for each column.
      DO col=usc,uec
C Determine object range row boundaries.
      rb1 = max(float(ssr),min(float(ser),
     .            sngl(polyarroff(sp1,dfloat(col)))))
      rb2 = max(float(ssr),min(float(ser),
     .            sngl(polyarroff(sp2,dfloat(col)))))
C Determine if there are enough unmasked pixels to bother calculating flux.
      npx=0
      nmk=0
      do row=max(ssr,nint(rb1)+1),min(ser,nint(rb2)-1)
        npx=npx+1
        if (PSE_Masked(obj(col,row))) nmk=nmk+1
      enddo
      if ((npx.gt.2).and.((npx-nmk).lt.2)) then
        wsum=-1.
      else
C Initialize for final summation.
        sum =0.
        wsum=0.
        vsum=0.
C Consider each appropriate row for this column.
        do row=max(ssr,nint(rb1)),min(ser,nint(rb2))
C Only include non-masked, positive profile pixels.
          if ((.not.PSE_Masked(obj(col,row))).and.
     .                       (prof(col,row).gt.0.)) then
C Find fraction of this pixel.
C Although we can use any pixel with a profile value, we choose to use only
C those pixels which are included (at least in part) within the boundaries.
C Weight the fractional pixels a bit less.
            fr = PSE_PixFrac(row,rb1,rb2,col,sp1,sp2,(1+ec-sc))
            if (fr.gt.0.) then
              pfi = prof(col,row)
              obi = (obj(col,row)-sky(col,row))
              voi = PSE_getvar((pfi*obj_est(col))+sky(col,row),
     .                                                flat_est(col))
              wt  = fr*pfi*pfi/voi
              sum = sum + (wt*(obi/pfi))
              wsum= wsum+ (wt)
              vsum= vsum+ (wt*fr)
            endif
          endif
        enddo
      endif
C Optimally weighted sum.
      if (wsum.gt.0.) then
        objsp(col) = sum / wsum
        varsp(col) = (vsum/wsum) / wsum
      else
        objsp(col) = 0.
        varsp(col) = 0.
      endif
      ENDDO

      return
      end

C----------------------------------------------------------------------
      subroutine PSE_Mask(value,scale)
      implicit none
      real*4 value,scale
      value = -1.e+10 * max(0.001,min(1000.,abs(scale)))
      return
      end
      
C----------------------------------------------------------------------
      logical function PSE_Masked(value)
      implicit none
      real*4 value
      if (value.lt.-1.e+5) then
        PSE_Masked = .true.
      else
        PSE_Masked = .false.
      endif
      return
      end
      
C----------------------------------------------------------------------
C Decide what is the best number of horizontal and vertical plots.
      subroutine EE_OpGrid(n,h,v)
      implicit none
      integer*4 n,h,v
C This combination is guaranteed to fit.
      h=int(sqrt(float(n)-0.1))+1 
      v=int(sqrt(float(n)-0.1))+1 
C Try one less horizontal.
      if (((h-1)*v).ge.n) h=h-1
      return
      end

C-------------------------------------------------------------------------
C Polynomial fitting using Vista's Generalized Least Squares fitting (GLLS).
C This version selects the polynomial order based on the S/N of the data.
C Input:
C   np      = number of points in data arrays.               
C   x8,y8,w8= x and y values and weight for each data point.
C
C Output: 
C   ncoef   = polynomial order plus one, order can range from 0 to 19,
C   coef    = order+1 polynomial coeffecients ( c(0), c(1), c(2), ... )
C   msn     = median signal-to-noise
C
      subroutine PSE_poly_fit_bysn(np,x8,y8,w8,ncoef,coef,msn,ok)

      implicit none
C
      include 'global.inc'
C
      real*8 x8(*),y8(*),w8(*),coef(*)
      integer*4 np,order,i,ncoef,mo
      integer*4 nseg,narr,num,k,kk
      parameter(nseg=10)
      real*4 arr(nseg),msn
      parameter(mo=19)
      real*8 aa(0:mo,0:mo),dcoef(0:mo),chisq,sum,wsum,mean,rms
      logical GLLS,ok
      EXTERNAL FPOLY

C Estimate median S/N. This assumes points are ordered by x values.
      kk = max(1,int(float(np)/float(nseg)))
      narr=0
      do k=1,nseg
        mean=0.d0
        num =0
        do i=max(1,(k-1)*kk),min(np,(k*kk))
          mean= mean + y8(i)
          num = num + 1
        enddo
        mean = max(0.d0,mean/dfloat(max(1,num)))
        rms =0.d0
        do i=max(1,(k-1)*kk),min(np,(k*kk))
          rms = rms + ((y8(i)-mean)*(y8(i)-mean))
        enddo
        rms = sqrt(rms/dfloat(max(1,num)))
        narr = narr+1
        if (rms.lt.1.d-6) then
          arr(narr) = 0.
        else
          arr(narr) = sngl(mean/rms)
        endif
      enddo
      call find_median(arr,narr,msn)
C Choose order.
      if (Global_ESI) then         ! choices for ESI only.
        if (msn.lt.0.2) then
          order=0
        elseif (msn.lt.0.8) then
          order=1
        elseif (msn.lt.2.0) then
          order=2
        elseif (msn.lt.3.0) then
          order=3
        elseif (msn.lt.7.0) then
          order=4
        elseif (msn.lt.15.0) then
          order=5
        elseif (msn.lt.30.0) then
          order=6
        else
          order=7
        endif
      else                         ! all others including HIRES
        if (msn.lt.0.3) then
          order=0
        elseif (msn.lt.1.0) then
          order=1
        elseif (msn.lt.3.0) then
          order=2
        elseif (msn.lt.10.0) then
          order=3
        elseif (msn.lt.30.0) then
          order=4
        else
          order=5
        endif
      endif
C Number of coeffecients is the polynomial order plus one.
      ncoef = order + 1
      do i=1,ncoef
        coef(i) = 0.d0
      enddo
C Fit flat line.
      if (order.lt.1) then
        sum =0.d0
        wsum=0.d0
        do i=1,np
          sum =sum + (y8(i)*w8(i))
          wsum=wsum+ (w8(i))
        enddo
        if (wsum.gt.1.d-30) then
          coef(1) = sum / wsum
          ok = .true.
        else
          coef(1) = 0.d0
          ok = .false.
        endif
      else
C Fit polynomial.
        ok = GLLS(x8,y8,w8,np,coef,ncoef,dcoef,aa,chisq,FPOLY,.true.)
      endif
      return
      end


C------------------------------------------------------------------------
C Fit the sky range within the profile model and subtract this fit from the
C profile model.  The fit should be done in the same manner as in PSE_SkyFit
C to get an accurate relative profile.
C
      subroutine PSE_ProfileModelSkyFit(sc,ec,sp1,sp2,
     .                                  bk1o,bk2o,bk1i,bk2i)
C
      implicit none
      integer*4 sc,ec
      real*8 bk1o(*),bk2o(*),bk1i(*),bk2i(*),sp1(*),sp2(*)
      real*4 rb1,rb2,rb3,rb4,cent,balance
      integer*4 i,i1,i2,i3,i4,nn,np,skyord
C
      include 'pse.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
      real*8 polyarroff,polyval,xv
      logical ok

C Determine row boundaries for center column.
      xv  = dfloat(sc+ec)/2.d0
      rb1 = sngl( polyarroff(bk1o,xv) )
      rb2 = sngl( polyarroff(bk1i,xv) )
      rb3 = sngl( polyarroff(bk2i,xv) )
      rb4 = sngl( polyarroff(bk2o,xv) )
      cent= (rb2+rb3)/2.
C Make sure there is enough sky on both sides.
      if ((rb2-rb1).lt.PSE_SkyMin) rb2= rb1 + PSE_SkyMin
      if ((rb4-rb3).lt.PSE_SkyMin) rb3= rb4 - PSE_SkyMin
C Convert these to profile model units.
      rb1 = (rb1-cent)+PSE_pparm(4)  
      rb2 = (rb2-cent)+PSE_pparm(4)  
      rb3 = (rb3-cent)+PSE_pparm(4)  
      rb4 = (rb4-cent)+PSE_pparm(4)  
C Convert to array elements.
      i1 = nint((rb1-PSE_pparm(2))/PSE_pparm(3))+1
      i2 = nint((rb2-PSE_pparm(2))/PSE_pparm(3))+1
      i3 = nint((rb3-PSE_pparm(2))/PSE_pparm(3))+1
      i4 = nint((rb4-PSE_pparm(2))/PSE_pparm(3))+1
      nn = nint(PSE_pparm(1))
      i1 = min(nn,max(1,i1))
      i2 = min(nn,max(1,i2))
      i3 = min(nn,max(1,i3))
      i4 = min(nn,max(1,i4))
C Sky region balance.
      balance = (rb2-rb1)/max(1.e-10,(rb4-rb3))
C Choose sky polynomial fit order.
      if (PSE_SkyOrder.ge.0) then
        skyord = PSE_SkyOrder
      else
C Program's choice is order = 1, ...but check for unbalanced sky regions.
        skyord = 1
        if ((balance.gt.2.).or.(balance.lt.0.5)) skyord=0
      endif
      write(soun,'(a,f7.3,a,i2,a)') 
     .      ' Sky areas ratio: ',max(-99.,min(999.,balance)),
     .      '.   Sky polynomial order: ',max(-1,min(99,skyord)),'.'
C Fill arrays for fit.
      np=0
      do i=i1,i2
        np=np+1
        x8(np) = dble(PSE_pparm(2)+(float(i-1)*PSE_pparm(3)))
        y8(np) = dble(PSE_pval(i))
        w8(np) = 1.d0
      enddo
      do i=i3,i4
        np=np+1
        x8(np) = dble(PSE_pparm(2)+(float(i-1)*PSE_pparm(3)))
        y8(np) = dble(PSE_pval(i))
        w8(np) = 1.d0
      enddo
C Fit sky.
      call poly_fit_glls(np,x8,y8,w8,skyord,coef,ok)
      if (.not.ok) write(soun,'(a)') 
     .      'ERROR: PSE_ProfileModelSkyFit: Fitting polynomial.'
C Subtract sky fit from profile model.
      do i=1,nn
        xv      = dble(PSE_pparm(2)+(float(i-1)*PSE_pparm(3)))
        PSE_pval(i) = PSE_pval(i) - sngl( polyval(skyord+1,coef,xv) )
      enddo 
      return
      end



C------------------------------------------------------------------------
C
      real*4 function PSE_NEW_SkyBckColoff( rowoff )
C
      implicit none
      real*4 rowoff
C
      include 'makee.inc'
C
      PSE_NEW_SkyBckColOff = rowoff * ( SkySlope )
C
      return
      end



C------------------------------------------------------------------------
C
C NEW sky fit... try using a tilted sky fit...
C
C Fit the sky, assume all the bad pixels have been masked by PSE_SkyMask.
C "PSE_SkySpacer" is the distance between the edge of object or spectrum range
C and the inside edge of the background range (the distance separated the
C object pixels from the background pixels).  "PSE_SkyMin" is the minimum
C amount of background used on each side of the object.  "PSE_SkyMin" overrides
C "PSE_SkySpacer" such that the sky will cut into the object if necessary.
C
      subroutine PSE_NEW_SkyFit(usc,uec,sc,ec,sr,er,obj,flat,sp1,sp2,
     .                          bk1o,bk2o,bk1i,bk2i,skysp,sky,ssr,ser)
C
      implicit none
      integer*4 usc,uec,sc,ec,sr,er             ! image boundaries (input)
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er) ! data and flat images (input)
      real*8 bk1o(*),bk2o(*),sp1(*),sp2(*)      ! polynomials (input)
      real*8 bk1i(*),bk2i(*)                    ! polynomials (input)
C
      integer*4 ssr,ser                         ! sub-image row range (input)
      real*4 skysp(sc:ec)                       ! sky spectrum (output)
      real*4 sky(sc:ec,ssr:ser)                 ! sky sub-image (output)
C
      real*4 rb1,rb2,rb3,rb4,rj,spcr,skywidth1,skywidth2
      real*4 ob1,ob2
      real*4 sum,wsum,wt_col_a,wt_col_b,rr,bcp
      real*4 PSE_NEW_SkyBckColOff
      integer*4 col_a,col_b
      real*8 polyarroff,xv,weight
      integer*4 j,jj,row,col,rowbnd(4)
      logical PSE_Masked,DoSky(2)
C
      include 'pse.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
C Decide which sky side we should (or use both sides).
      xv = dfloat(sc+ec)/2.d0
      skywidth1 = sngl( polyarroff(bk1i,xv) - polyarroff(bk1o,xv) )
      skywidth2 = sngl( polyarroff(bk2o,xv) - polyarroff(bk2i,xv) )
      call PSE_SkyWidthCheck(skywidth1,skywidth2,DoSky)

C Clear sky image.
      do col=sc,ec
        do row=ssr,ser
          sky(col,row)=0.
        enddo
        skysp(col)=0.
      enddo

C Do each column separately.
      DO col=usc,uec

C Determine boundaries for this column.
      xv = dfloat(col)
      rb1= sngl( polyarroff(bk1o,xv) )
      rb2= sngl( polyarroff(bk1i,xv) )
      rb3= sngl( polyarroff(bk2i,xv) )
      rb4= sngl( polyarroff(bk2o,xv) )
      ob1= sngl( polyarroff(sp1,xv) )
      ob2= sngl( polyarroff(sp2,xv) )
      spcr= (ob1 + ob2) / 2.
      ob1= max(float(ssr),min(float(ser),ob1))
      ob2= max(float(ssr),min(float(ser),ob2))
C Make sure there is enough sky on both sides.
      if ((rb2-rb1).lt.PSE_SkyMin) rb2= rb1 + PSE_SkyMin
      if ((rb4-rb3).lt.PSE_SkyMin) rb3= rb4 - PSE_SkyMin
      rb1 = max(float(ssr),min(float(ser),rb1))
      rb2 = max(float(ssr),min(float(ser),rb2))
      rb3 = max(float(ssr),min(float(ser),rb3))
      rb4 = max(float(ssr),min(float(ser),rb4))
C Integer-ized boundaries.
      rowbnd(1)= nint(rb1)
      rowbnd(2)= nint(rb2)
      rowbnd(3)= nint(rb3)
      rowbnd(4)= nint(rb4)

C Are we within the boundaries of the CCD?
      IF ( ((rb2-float(sr)).gt.1.0).and.((float(er)-rb3).gt.1.0) ) THEN

C Compute sky pixels in object region.
      do row=int(ob1),int(ob2+1)

C Find pixels to average in background regions.
        sum = 0.
        wsum= 0.
        do jj=0,2,2
          if (DoSky(1+(jj/2))) then
            do j=max(sr,rowbnd(1+jj)-1),min(er,rowbnd(2+jj)+1)
              rj = float(j)
C Assign fractional weight to pixels which straddle background boundaries.
              call EE_SkyWeight(rj,rb1,rb2,rb3,rb4,weight)
              if (PSE_Masked(obj(col,j))) weight = 0.

C Sum up fractions of pixels in appropriate columns given col,row and j....

C Background Column Position...
              bcp = float(col) + PSE_NEW_SkyBckColOff(float(j - row))

C Two columns to weight and sum.
              col_a = int(bcp)
              col_b = col_a + 1
              wt_col_a = 1.0 - (bcp - float(col_a))
              wt_col_b = 1.0 - (float(col_b) - bcp)
              rr=(wt_col_a * obj(col_a,j))+(wt_col_b * obj(col_b,j))

C Background sum and weight sum.
              sum = sum + ( weight * rr )
              wsum= wsum+ ( weight )

            enddo
          endif
        enddo
        if (wsum.gt.0.) then
          sky(col,row) = sum / wsum
        else
          write(soun,'(a)')' ERROR: PSE_SkyFit: no sky points.'
        endif

      enddo

C Load sky spectrum.
      skysp(col) = sky(col,nint(spcr))

      ENDIF
      ENDDO
C
      return
      end



C------------------------------------------------------------------------
C Fit the sky, assume all the bad pixels have been masked by PSE_SkyMask.
C "PSE_SkySpacer" is the distance between the edge of object or spectrum range
C and the inside edge of the background range (the distance separated the
C object pixels from the background pixels).  "PSE_SkyMin" is the minimum
C amount of background used on each side of the object.  "PSE_SkyMin" overrides
C "PSE_SkySpacer" such that the sky will cut into the object if necessary. 
C
      subroutine PSE_SkyFit(usc,uec,sc,ec,sr,er,obj,flat,sp1,sp2,
     .                      bk1o,bk2o,bk1i,bk2i,skysp,sky,ssr,ser)

      implicit none
      integer*4 usc,uec,sc,ec,sr,er                    ! image boundaries
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er)        ! data and flat images
      real*8 bk1o(*),bk2o(*),sp1(*),sp2(*)             ! polynomials
      real*8 bk1i(*),bk2i(*)                           ! polynomials
      real*4 skysp(sc:ec)                              ! output spectrum
      integer*4 ssr,ser                                ! scratch row range
      real*4 sky(sc:ec,ssr:ser)                        ! scratch/output image

      real*4 rb1,rb2,rb3,rb4,rj,skysig,balance,skywidth1,skywidth2
      real*8 polyarroff,polyval,xv,weight
      integer*4 row,col,rowbnd(4),col1,col2,ord,np,i,j,jj,nsl,skyord
      logical PSE_Masked,ok,DoSky(2)
C
      include 'pse.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
C Decide which sky side we should (or use both sides).
      xv = dfloat(sc+ec)/2.d0
      skywidth1 = sngl( polyarroff(bk1i,xv) - polyarroff(bk1o,xv) )
      skywidth2 = sngl( polyarroff(bk2o,xv) - polyarroff(bk2i,xv) )
      call PSE_SkyWidthCheck(skywidth1,skywidth2,DoSky)

C Clear sky image.
      do col=sc,ec 
        do row=ssr,ser
          sky(col,row)=0.
        enddo
      enddo

C Number of night sky lines seen.
      nsl=0

C Do each column separately.
      DO col=usc,uec

C Determine boundaries for this column.
      xv = dfloat(col)
      rb1= sngl( polyarroff(bk1o,xv) )
      rb2= sngl( polyarroff(bk1i,xv) )
      rb3= sngl( polyarroff(bk2i,xv) )
      rb4= sngl( polyarroff(bk2o,xv) )
C Make sure there is enough sky on both sides.
      if ((rb2-rb1).lt.PSE_SkyMin) rb2= rb1 + PSE_SkyMin
      if ((rb4-rb3).lt.PSE_SkyMin) rb3= rb4 - PSE_SkyMin
      rb1 = max(float(ssr),min(float(ser),rb1))
      rb2 = max(float(ssr),min(float(ser),rb2))
      rb3 = max(float(ssr),min(float(ser),rb3))
      rb4 = max(float(ssr),min(float(ser),rb4))
C Integer-ized boundaries.
      rowbnd(1)= nint(rb1)
      rowbnd(2)= nint(rb2)
      rowbnd(3)= nint(rb3)
      rowbnd(4)= nint(rb4)

C Choose sky polynomial fit order.
      if (PSE_SkyOrder.ge.0) then
        skyord = PSE_SkyOrder
      else
C Program's choice is order = 1, ...but check for unbalanced sky regions.
        skyord = 1
        balance = (rb2-rb1)/max(1.e-10,(rb4-rb3))
        if ((balance.gt.2.).or.(balance.lt.0.5)) skyord=0
C Use skyord=0, if one side is not being used.
        if ((.not.DoSky(1)).or.(.not.DoSky(2))) skyord=0
      endif

C Are we off the end of the CCD?
      IF ( ((rb2-float(sr)).lt.1.0).or.((float(er)-rb3).lt.1.0) ) THEN

      do row=max(ssr,rowbnd(1)-1),min(ser,rowbnd(4)+1)
        sky(col,row) = 0.
      enddo
      skysp(col) = 0.

      ELSE

C Choose number of adjacent columns to use when fitting this particular column.
      if (PSE_SingleColumnSkyFit) then
        col1 = col
        col2 = col
        ord  = skyord
      else
C Try to detect possible nearby strong sky-emission-line.
C If we are near a sky-line, use fewer columns.
        call PSE_NearSky(sc,ec,sr,er,obj,flat,rowbnd,col,-3,3,skysig)
C Strong Line.
        if (skysig.gt.20.) then
          col1 = col
          col2 = col
          ord  = max(0,skyord-1)
          nsl=nsl+1
C Not so strong line.
        elseif (skysig.gt.2.5) then
          col1 = max(usc,col-1)
          col2 = min(uec,col+1)
          ord  = max(0,skyord-1)
          nsl=nsl+1
        else
          col1 = max(usc,col-2)
          col2 = min(uec,col+2)
          ord  = skyord
        endif
      endif

C Fill arrays for fit.  Use pixels from columns between col1 and col2.
      np=0
      do i=col1,col2
        do jj=0,2,2
        if (DoSky(1+(jj/2))) then
          do j=max(sr,rowbnd(1+jj)-1),min(er,rowbnd(2+jj)+1)
            rj = float(j)
            if (.not.PSE_Masked(obj(i,j))) then
C Assign fractional weight to pixels which straddle background boundaries.
              call EE_SkyWeight(rj,rb1,rb2,rb3,rb4,weight)
              if (weight.gt.0.) then
                np=np+1
                x8(np)=dble(rj)
                y8(np)=dble(obj(i,j))
C Re-adjust weight for distance from column:  1 / abs(i-col)*2
                if (i.ne.col) then
                  w8(np) = weight / ( dfloat(2*abs(i-col)) )
                else
                  w8(np) = weight
                endif
              endif
            endif
          enddo
        endif
        enddo
      enddo
      if (np.gt.(ord+1)) then
C Fit and load sky.
        call poly_fit_glls(np,x8,y8,w8,ord,coef,ok)
        if (.not.ok) write(soun,'(a)')
     .  ' ERROR: PSE_SkyFit: Fitting polynomial.'
        do row=max(ssr,rowbnd(1)-1),min(ser,rowbnd(4)+1)
          sky(col,row) = sngl( polyval(ord+1,coef,dfloat(row)) )
        enddo

C Load sky spectrum.
        skysp(col) = sngl( polyval(ord+1,coef,dble((rb2+rb3)/2.)) )

      else
C If all the pixels are masked use previous column's values.
        if (col.gt.sc) then
          if (np.gt.0) then
            write(soun,'(a,i4,a,i3,a,a)') 
     .  ' PSE_SkyFit: NOTE: Found column (',
     .  min(9999,col),') with too few pixels (',min(999,np),') ',
     .  '[use sky from previous column].'
          endif
C Changed 'min(sc,' to 'max(sc,'... this was apparently an error.  -tab 08may2007
          do row=max(ssr,rowbnd(1)-1),min(ser,rowbnd(4)+1)
            sky(col,row) = sky(max(sc,col-1),row)
          enddo
          skysp(col) = skysp(max(sc,col-1))
        else
          do row=max(ssr,rowbnd(1)-1),min(ser,rowbnd(4)+1)
            sky(col,row) = 0.
          enddo
          skysp(col) = 0.
        endif
      endif

      ENDIF

      ENDDO

      if (nsl.gt.0) then
        write(soun,'(i6,a)') nsl,
     .     ' columns affected by nearby night sky lines during SkyFit.'
      endif

      return
      end

C----------------------------------------------------------------------
C Check sky widths and decide whether to do both sides or just one side.
C
      subroutine PSE_SkyWidthCheck(skywidth1,skywidth2,DoSky)
C
      implicit none
      real*4 skywidth1,skywidth2   ! input
      logical DoSky(2)             ! output
      real*4 EE_GetSkyMin,sm
C
      include 'soun.inc'
C
C Check.
      if ((skywidth1.le.0.).or.(skywidth2.le.0.)) then
        write(soun,'(a)')
     .  'SEVERE ERROR: No sky width. This should not happen.'
        write(soun,'(a,f12.6,a,f12.6)') 
     .  'NOTE: skywidth1=',skywidth1,'   skywidth2=',skywidth2
        call exit(1)
      endif
C Which sky side to do? or both?
      sm = EE_GetSkyMin()
      DoSky(1) = .false.
      DoSky(2) = .false.
C Select larger side.
      if (skywidth1.ge.skywidth2) DoSky(1) = .true.
      if (skywidth2.ge.skywidth1) DoSky(2) = .true.
C But do smaller side also if it is large enough.
      if (skywidth1.gt.(sm+0.1))  DoSky(1) = .true.
      if (skywidth2.gt.(sm+0.1))  DoSky(2) = .true.
C And do both sides if both widths are small.
      if ( (skywidth1.lt.(sm+0.1)).and.(skywidth2.lt.(sm+0.1)) ) then
        DoSky(1) = .true.
        DoSky(2) = .true.
      endif
C
      return
      end

C------------------------------------------------------------------------
C Here we estimate the number of CRs per pixel that PSE_SkyMask would find.
C If this number greatly exceeds the number expected (about 1% of pixels per
C hour) then we will increase PSEMRA .  We keep increasing this until
C we get a reasonable number rejected.
C
      subroutine PSE_SkyMaskCheck(usc,uec,sc,ec,sr,er,obj,flat,
     .  sp1,sp2,bk1o,bk2o,bk1i,bk2i)

      implicit none
      integer*4 usc,uec,sc,ec,sr,er                    ! image boundaries
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er)        ! data and flat images
      real*8 sp1(*),sp2(*),bk1o(*),bk2o(*),bk1i(*),bk2i(*)   ! polynomials
      real*4 PSE_getvar,rr,back,expfrac,actfrac
      real*4 rb1,rb2,rb3,rb4,cent,pbm,skysig,skywidth1,skywidth2
      integer*4 i1,i2,i3,i4,nn,ii
      real*8 polyarroff,xv
      integer*4 rowbnd(4),jj,col,col1,col2,i,j,n,nrej,npix
      logical PSE_Masked,DoSky(2)
C
      include 'pse.inc'
      include 'global.inc'
      include 'scratch.inc'
      include 'soun.inc'
C
C Decide which sky side we should (or use both sides).
      xv = dfloat(sc+ec)/2.d0
      skywidth1 = sngl( polyarroff(bk1i,xv) - polyarroff(bk1o,xv) )
      skywidth2 = sngl( polyarroff(bk2o,xv) - polyarroff(bk2i,xv) )
      call PSE_SkyWidthCheck(skywidth1,skywidth2,DoSky)
C
C In order to use the profile to estimate background levels across a column,
C we need to estimate a median level for the background using ranges which
C are similar to those used below for each individual column.
      xv = dfloat(sc+ec)/2.d0
      rb1 =  sngl(polyarroff(bk1o,xv)-0.5d0)
      rb2 =  sngl(polyarroff(bk1i,xv)+0.5d0)
      rb3 =  sngl(polyarroff(bk2i,xv)-0.5d0)
      rb4 =  sngl(polyarroff(bk2o,xv)+0.5d0)
      cent= sngl((polyarroff(sp1,xv)+polyarroff(sp2,xv))/2.d0)
C Convert these numbers to profile units.
      rb1 = (rb1-cent)+PSE_pparm(4)
      rb2 = (rb2-cent)+PSE_pparm(4)
      rb3 = (rb3-cent)+PSE_pparm(4)
      rb4 = (rb4-cent)+PSE_pparm(4)
C Convert to array elements.
      i1 = nint((rb1-PSE_pparm(2))/PSE_pparm(3))+1
      i2 = nint((rb2-PSE_pparm(2))/PSE_pparm(3))+1
      i3 = nint((rb3-PSE_pparm(2))/PSE_pparm(3))+1
      i4 = nint((rb4-PSE_pparm(2))/PSE_pparm(3))+1
      nn = nint(PSE_pparm(1))
      i1 = min(nn,max(1,i1))
      i2 = min(nn,max(1,i2))
      i3 = min(nn,max(1,i3))
      i4 = min(nn,max(1,i4))
C Find "profile background median" pbm.
      n=0
      if (DoSky(1)) then
        do i=i1,i2
          n=n+1
          arr(n)=PSE_pval(i)
        enddo
      endif
      if (DoSky(2)) then
        do i=i3,i4
          n=n+1
          arr(n)=PSE_pval(i)
        enddo
      endif
      call find_median(arr,n,pbm)

C Repeat from this point if PSEMRA needed to be increased.
1     continue

C Do each column separately.
      nrej=0
      npix=0
      DO col=usc,uec
C Determine row boundaries for this column: rowbnd(1..4)
      xv = dfloat(col)
      rowbnd(1) = max(sr,min(er,nint(polyarroff(bk1o,xv)-0.5d0)))
      rowbnd(2) = max(sr,min(er,nint(polyarroff(bk1i,xv)+0.5d0)))
      rowbnd(3) = max(sr,min(er,nint(polyarroff(bk2i,xv)-0.5d0)))
      rowbnd(4) = max(sr,min(er,nint(polyarroff(bk2o,xv)+0.5d0)))
      cent = sngl((polyarroff(sp1,xv)+polyarroff(sp2,xv))/2.d0)
C Try to detect nearby strong sky-emission-line.
C If we are near a sky-line, derive median including data only +/-1 column.
      call PSE_NearSky(sc,ec,sr,er,obj,flat,rowbnd,col,-3,3,skysig)
      if (skysig.gt.40.) then
C Skip this column.
        goto 3
      elseif (skysig.gt.15.) then
        col1 = col
        col2 = col
      elseif (skysig.gt.2.5) then
        col1 = max(usc,col-1)
        col2 = min(uec,col+1)
      else
        col1 = max(usc,col-2)
        col2 = min(uec,col+2)
      endif
C Find median of background.
      n=0
      do i=col1,col2
        do jj=0,2,2
        if (DoSky(1+(jj/2))) then
          do j=rowbnd(1+jj),rowbnd(2+jj)
            if (.not.PSE_Masked(obj(i,j))) then
              n=n+1
              arr(n) = obj(i,j)
            endif
          enddo
        endif
        enddo
      enddo
      call find_median(arr,n,back)
C Look for and mask CRs.
      do jj=0,2,2
      if (DoSky(1+(jj/2))) then
        do j=rowbnd(1+jj),rowbnd(2+jj)
          if (.not.PSE_Masked(obj(col,j))) then
C What is the expected background level for this particular pixel?
            if ((pbm.gt.0.).and.(back.gt.0.)) then
              ii = nint(((PSE_pparm(4)+float(j)-cent)
     .                          -PSE_pparm(2))/PSE_pparm(3))+1
              ii = max(1,min(nint(PSE_pparm(1)),ii))
              if (PSE_pval(ii).gt.0.) then
                rr = PSE_pval(ii)*back/pbm
              else
                rr = 0.
              endif
            else
              rr = 0.
            endif
            rr = (obj(col,j)-rr)
     .         / sqrt(PSE_getvar(obj(col,j),flat(col,j)))
C Should we reject?
            if (rr.gt.(PSE_SkyRejLimOne*PSEMRA)) then
              nrej=nrej+1
            endif
            npix = npix + 1
          endif
        enddo
      endif
      enddo
3     continue
      ENDDO

C See if too many CRs would be rejected.
      if (Global_HIRES.or.Global_HIRES2) then
        expfrac = (Global_exposure + 60.)/ 360000.
      elseif (Global_ESI) then
        expfrac = (Global_exposure + 60.)/1362000.
      else
        expfrac = 9.
      endif
      actfrac = float(nrej)/float(max(1,npix))
C Echo.
      write(soun,'(a,f8.5,a,f8.5)') 
     .   'Cosmic Ray Fraction: expected=',min(99.,max(-9.,expfrac)),
     .   '  actual=',min(99.,max(-9.,actfrac))
      Global_CR_Frac = actfrac / expfrac    ! added -tab 26nov07
C Check.
      if ((actfrac/expfrac.gt.5.).and.(PSEMRA.lt.10.)) then
        PSEMRA=PSEMRA*1.5
        write(soun,'(a,f6.1,a)') 'This is a factor of',
     .    min(9999.,max(0.,actfrac/expfrac)),' larger than expected.'
        write(soun,'(a,1pe15.6)') 
     .  'Repeating and increasing PSEMRA to ',PSEMRA
        goto 1
      endif
      Global_psemra = PSEMRA     ! added -tab 26nov07
C
      return
      end

C------------------------------------------------------------------------
C Here we want to compensate for radiation events (CRs) in the background.
C We "mask" (call PSE_Mask) all bad pixels.
C We check for masked by pixels by calling PSE_Masked.
C
C This routine uses medians with large sections of sky to eliminate
C large radiation events.
C
      subroutine PSE_SkyMask(usc,uec,sc,ec,sr,er,obj,flat,
     .  sp1,sp2,bk1o,bk2o,bk1i,bk2i)

      implicit none
      integer*4 usc,uec,sc,ec,sr,er                    ! image boundaries
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er)        ! data and flat images
      real*8 sp1(*),sp2(*),bk1o(*),bk2o(*)             ! polynomials
      real*8 bk1i(*),bk2i(*)                           ! polynomials
      real*4 PSE_getvar,rr,back,cent,pbm
      real*4 rb1,rb2,rb3,rb4,skysig,skywidth1,skywidth2
      real*8 polyarroff,xv
      integer*4 rowbnd(4),jj,col,col0,col1,col2,i,j,n,k
      integer*4 nrej,srej,ii
      integer*4 i1,i2,i3,i4,nn,prispm,adjspm
      logical PSE_Masked,DoSky(2)
C
      include 'pse.inc'
      include 'global.inc'        ! added -tab 26nov07
      include 'scratch.inc'
      include 'soun.inc'
C
C
C Decide which sky side we should (or use both sides).
      xv = dfloat(sc+ec)/2.d0
      skywidth1 = sngl( polyarroff(bk1i,xv) - polyarroff(bk1o,xv) )
      skywidth2 = sngl( polyarroff(bk2o,xv) - polyarroff(bk2i,xv) )
      call PSE_SkyWidthCheck(skywidth1,skywidth2,DoSky)
C
C In order to use the profile to estimate background levels across a column,
C we need to estimate a median level for the background using ranges which
C are similar to those used below for each individual column.
      xv = dfloat(sc+ec)/2.d0
      rb1 =  sngl(polyarroff(bk1o,xv)-0.5d0)
      rb2 =  sngl(polyarroff(bk1i,xv)+0.5d0)
      rb3 =  sngl(polyarroff(bk2o,xv)-0.5d0)
      rb4 =  sngl(polyarroff(bk2o,xv)+0.5d0)
      cent= sngl((polyarroff(sp1,xv)+polyarroff(sp2,xv))/2.d0)
C Convert these numbers to profile units.
      rb1 = (rb1-cent)+PSE_pparm(4)
      rb2 = (rb2-cent)+PSE_pparm(4)
      rb3 = (rb3-cent)+PSE_pparm(4)
      rb4 = (rb4-cent)+PSE_pparm(4)
C Convert to array elements.
      i1 = nint((rb1-PSE_pparm(2))/PSE_pparm(3))+1
      i2 = nint((rb2-PSE_pparm(2))/PSE_pparm(3))+1
      i3 = nint((rb3-PSE_pparm(2))/PSE_pparm(3))+1
      i4 = nint((rb4-PSE_pparm(2))/PSE_pparm(3))+1
      nn = nint(PSE_pparm(1))
      i1 = min(nn,max(1,i1))
      i2 = min(nn,max(1,i2))
      i3 = min(nn,max(1,i3))
      i4 = min(nn,max(1,i4))
C Find "profile background median" pbm.
      n=0
      if (DoSky(1)) then
        do i=i1,i2
          n=n+1
          arr(n)=PSE_pval(i)
        enddo
      endif
      if (DoSky(2)) then
        do i=i3,i4
          n=n+1
          arr(n)=PSE_pval(i)
        enddo
      endif
      call find_median(arr,n,pbm)

C :::::::: FIRST PASS ::::::::::::::::
C Do each column separately.
      nrej=0
      srej=0

      DO col=usc,uec

C Determine row boundaries for this column: rowbnd(1..4)
      xv = dfloat(col)
      rowbnd(1) = max(sr,min(er,nint(polyarroff(bk1o,xv)-0.5d0)))
      rowbnd(2) = max(sr,min(er,nint(polyarroff(bk1i,xv)+0.5d0)))
      rowbnd(3) = max(sr,min(er,nint(polyarroff(bk2i,xv)-0.5d0)))
      rowbnd(4) = max(sr,min(er,nint(polyarroff(bk2o,xv)+0.5d0)))
      cent = sngl((polyarroff(sp1,xv)+polyarroff(sp2,xv))/2.d0)
C Try to detect nearby strong sky-emission-line.
C If we are near a sky-line, derive median including data only 1 column back.
      call PSE_NearSky(sc,ec,sr,er,obj,flat,rowbnd,col,-3,-99,skysig)
      if (skysig.gt.40.) then
C Skip this column.
        goto 3 
      elseif (skysig.gt.15.) then
        col0 = col
      elseif (skysig.gt.2.5) then
        col0 = max(usc,col-1)
      else
        col0 = max(usc,col-3)
      endif
C Find median of background.
      n=0
      do i=col0,col
        do jj=0,2,2
        if (DoSky(1+(jj/2))) then
          do j=rowbnd(1+jj),rowbnd(2+jj)
            if (.not.PSE_Masked(obj(i,j))) then
              n=n+1
              arr(n) = obj(i,j)
            endif
          enddo
        endif
        enddo
      enddo
      call find_median(arr,n,back)
C Look for and mask CRs.
      do jj=0,2,2
      if (DoSky(1+(jj/2))) then
        do j=rowbnd(1+jj),rowbnd(2+jj)
          if (.not.PSE_Masked(obj(col,j))) then
C What is the expected background level for this particular pixel?
            if ((pbm.gt.0.).and.(back.gt.0.)) then
              ii = nint(((PSE_pparm(4)+float(j)-cent)
     .                      -PSE_pparm(2))/PSE_pparm(3))+1
              ii = max(1,min(nint(PSE_pparm(1)),ii))
              if (PSE_pval(ii).gt.0.) then
                rr = PSE_pval(ii)*back/pbm
              else
                rr = 0.
              endif
            else
              rr = 0.
            endif
            rr = (obj(col,j)-rr)
     .         / sqrt(PSE_getvar(obj(col,j),flat(col,j)))
C Should we reject?
            if (rr.gt.(PSE_SkyRejLimOne*PSEMRA)) then
              call PSE_Mask(obj(col,j),rr)
              nrej=nrej+1
C If CR is greater than twice the rejection limit, search for "satellites".
              if (rr.gt.(PSE_SkyRejLimOne*PSEMRA*2.)) then
            call PSESMO_Sat(sc,ec,sr,er,obj,flat,col,j,rowbnd,
     .  back,pbm,cent,k)
            srej=srej+k
              endif   
            endif
          endif
        enddo
      endif
      enddo
3     continue
      ENDDO
C Save for later.
      prispm = nrej
      adjspm = srej

C Do the second pass only if PSEMRA <= 2 ... -tab 10apr2007
      IF (PSEMRA.gt.2.0) THEN
        write(soun,'(a)') 
     .  'NOTE: Skip additional sky masking if PSEMRA > 2.0.'
        nrej = -1;
      ELSE

C :::::::: SECOND PASS ::::::::::::::::
C Do each column separately.
C In this version we use a median box centered on the column, rather than
C one which only uses other columns behind the column.
      nrej=0
C#@#  nrejlow=0
      DO col=usc,uec
C Determine row boundaries for this column: rowbnd(1..4) .
      xv = dfloat(col)
      rowbnd(1) = max(sr,min(er,nint(polyarroff(bk1o,xv)-0.5d0)))
      rowbnd(2) = max(sr,min(er,nint(polyarroff(bk1i,xv)+0.5d0)))
      rowbnd(3) = max(sr,min(er,nint(polyarroff(bk2i,xv)-0.5d0)))
      rowbnd(4) = max(sr,min(er,nint(polyarroff(bk2o,xv)+0.5d0)))
      cent = sngl((polyarroff(sp1,xv)+polyarroff(sp2,xv))/2.d0)
C Try to detect nearby strong sky-emission-line. If we are near a sky-line,
C derive median including data only 1 column on either side.
      call PSE_NearSky(sc,ec,sr,er,obj,flat,rowbnd,col,-3,3,skysig)
      if (skysig.gt.40.) then
C Skip this column.
        goto 33
      elseif (skysig.gt.15.) then
        col1 = col
        col2 = col
      elseif (skysig.gt.2.5) then
        col1 = max(usc,col-1)
        col2 = min(uec,col+1)
      else
        col1 = max(usc,col-2)
        col2 = min(uec,col+2)
      endif
C Find median of background within box centered on column.
      n=0
      do i=col1,col2
        do jj=0,2,2
        if (DoSky(1+(jj/2))) then
          do j=rowbnd(1+jj),rowbnd(2+jj)
            if (.not.PSE_Masked(obj(i,j))) then
              n=n+1
              arr(n) = obj(i,j)
            endif
          enddo
        endif
        enddo
      enddo
      call find_median(arr,n,back)
C Look for and mask deviant pixels.  We consider both high and low pixels.
      do jj=0,2,2
      if (DoSky(1+(jj/2))) then
C Only look for additional pixels if background includes more than 1 pixel.
        if (rowbnd(2+jj).gt.rowbnd(1+jj)) then
          do j=rowbnd(1+jj),rowbnd(2+jj)
            if (.not.PSE_Masked(obj(col,j))) then
C What is the expected background level for this particular pixel?
              if ((pbm.gt.0.).and.(back.gt.0.)) then
                ii =  nint(((PSE_pparm(4)+float(j)-cent)
     .                         -PSE_pparm(2))/PSE_pparm(3))+1
                ii = max(1,min(nint(PSE_pparm(1)),ii))
                if (PSE_pval(ii).gt.0.) then
                  rr = PSE_pval(ii)*back/pbm
                else
                  rr = 0.
                endif
              else
                rr = 0.
              endif
C #@#
C             rr2= obj(col,j) - rr
C #@#
              rr = abs(obj(col,j)-rr)
     .           / sqrt(PSE_getvar(obj(col,j),flat(col,j)))
C Should we reject?
              if (rr.gt.(PSE_SkyRejLimTwo*PSEMRA)) then
                call PSE_Mask(obj(col,j),rr)
                nrej=nrej+1
C#@#
C               if (rr2.lt.0.) then
C                 nrejlow = nrejlow + 1
C               endif
C#@#
              endif
            endif
          enddo
        endif
      endif
      enddo
33    continue
      ENDDO
      ENDIF
C Echo.
      write(soun,'(a,3i6)') 
     . 'Sky Pixels Masked (primary,adjacent,additional):',
     .  prispm,adjspm,nrej
      Global_sky_rej = prispm + adjspm + nrej    ! added -tab 26nov07

C     print *,'#@# nrejlow=',nrejlow

      return
      end

C----------------------------------------------------------------------
C Look for satellites of big radiation event.  If satellite is also a big
C event, then do not reject (it will be rejected later).
C
      subroutine PSESMO_Sat(sc,ec,sr,er,obj,flat,col,row,rowbnd,
     .  back,pbm,cent,k)
C
      implicit none
      integer*4 sc,ec,sr,er,col,row,rowbnd(4)
      real*4 obj(sc:ec,sr:er),flat(sc:ec,sr:er),back,pbm,cent
      real*4 PSE_getvar,rr
      integer*4 ioff,joff,i,j,k,ii
      logical PSE_Masked
      include 'pse.inc'
C Initialize.
      j=0
C Count number of pixels masked.
      k=0
C Test adjacent pixels.
C Note that the middle point (ioff=joff=0) has already been rejected.
      do joff=-1,1
        if (row.le.rowbnd(2)) j=max(rowbnd(1),min(rowbnd(2),row+joff))
        if (row.ge.rowbnd(3)) j=max(rowbnd(3),min(rowbnd(4),row+joff))
        do ioff=-1,1
          i=max(sc,min(ec,col+ioff))
          if (.not.PSE_Masked(obj(i,j))) then
C What is the expected background level for this particular pixel?
            if ((pbm.gt.0.).and.(back.gt.0.)) then
              ii = nint(((PSE_pparm(4)+float(j)-cent)
     .                    -PSE_pparm(2))/PSE_pparm(3))+1
              if (PSE_pval(ii).gt.0.) then
                rr = PSE_pval(ii)*back/pbm
              else
                rr = 0.
              endif
            else
              rr = 0.
            endif
            rr = (obj(i,j)-rr) / sqrt(PSE_getvar(obj(i,j),flat(i,j)))
            if ((rr.gt.(PSE_SkyRejLimSat*PSEMRA)).and.
     .          (rr.lt.(PSE_SkyRejLimOne*PSEMRA*2.))) then
              call PSE_Mask(obj(i,j),rr)
              k=k+1
            endif
          endif
        enddo
      enddo
C Check pixels two away from big CR.
C Here we only reject a pixel if the pixel between it and the big CR has been
C rejected.  This just means that we require that the effect of the big CR
C must lessen as we move further away.
      j=row
      do ioff=-2,2,4
        i=max(sc,min(ec,col+ioff))
        if (.not.PSE_Masked(obj(i,j))) then
C What is the expected background level for this particular pixel?
          if ((pbm.gt.0.).and.(back.gt.0.)) then
            ii = nint(((PSE_pparm(4)+float(j)-cent)
     .                   -PSE_pparm(2))/PSE_pparm(3))+1
            if (PSE_pval(ii).gt.0.) then
              rr = PSE_pval(ii)*back/pbm
            else
              rr = 0.
            endif
          else
            rr = 0.
          endif
          rr = (obj(i,j)-rr) / sqrt(PSE_getvar(obj(i,j),flat(i,j)))
          if ( (rr.gt.(PSE_SkyRejLimSat*PSEMRA)).and.
     .         (rr.lt.(PSE_SkyRejLimOne*PSEMRA*2.)).and.
     .         (PSE_Masked(obj((col+(ioff/2)),j))) ) then
            call PSE_Mask(obj(i,j),rr)
            k=k+1
          endif
        endif
      enddo
      return
      end


C----------------------------------------------------------------------
C Check arc lamps.  Are lamps on?
C
      logical function mk_check_arc(header)
C
      implicit none
      include 'soun.inc'
      character*(*) header
      logical ok
      character*80 wrd,chead
      ok = .false.
      wrd = chead('LAMPCU1',header)
      if (index(wrd,'on').gt.0) ok=.true.
      wrd = chead('LAMPNE1',header)
      if (index(wrd,'on').gt.0) ok=.true.
      wrd = chead('LAMPAR1',header)
      if (index(wrd,'on').gt.0) ok=.true.
      mk_check_arc = ok
      if (.not.ok) then
        write(soun,'(a)') 'ERROR: Arclamps not turned on.'
      endif
      return
      end


C----------------------------------------------------------------------
C Set the filenames for bias, star, flat, and arc.
C Compare files to object file.
C Sets header cards in "objhead" header string.
C
      subroutine mk_set_other_files(arg,narg,nobias,biasfile,xbin,ybin,
     .  rawdir,objfile,objhead,header,starfile,flatfile,arcfile1,
     .  arcfile2,arc1add,arc2add,ok)
C
      implicit none
      character*(*) arg(*)      ! Command line arguments (input): arg(1)=object,
C                               !    arg(2)=star, arg(3)=flat, arg(4)=arc lamp.
      integer*4 narg            ! number of arguments (input).
      logical nobias            ! Do not use bias image (input).
      character*(*) biasfile    ! BIAS filename (input/output).
      integer*4 xbin,ybin       ! Binning factors, column and row (input).
      character*(*) rawdir      ! Directory for raw images (input).
      character*(*) objfile     ! Object raw image filename (input).
      character*(*) objhead     ! FITS header for object file (input/output).
      character*(*) header      ! Scratch FITS header (scratch).
      character*(*) starfile    ! Star (trace) raw image file (output).
      character*(*) flatfile    ! Flat raw image file (output).
      character*(*) arcfile1    ! First arc lamp raw image file (output).
      character*(*) arcfile2    ! Second arc lamp raw image file (output).
      character*(*) arc1add     ! Addit. arc to arc#1 (raw image file) (in/out)
      character*(*) arc2add     ! Addit. arc to arc#2 (raw image file) (in/out)
      logical ok                ! Success? (input).
C
      character*200 wrd
      character*80 chead,deck,obstype,slmsknam
      real*8 oecha,echa,oxda,xda,fhead
      integer*4 osc,oec,osr,oer,onc,onr,sc,ec,sr,er,nc,nr,lc
      logical mk_check_arc,EE_GetBiasFile
C
      include 'global.inc'
      include 'soun.inc'
C

C Warn if non-standard binning.  Changed this.  -tab 11feb08
      if ((xbin.gt.4).or.(ybin.gt.4)) then
        write(soun,'(a,i5,i5)') 
     .  'WARNING: Non-standard binning, col,row=',xbin,ybin
        write(soun,'(a)') 
     .  'WARNING: Program not tested for this binning.'
      endif

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8

C BIASFILE.............................
      if (nobias) then
        biasfile=' '
      elseif (Global_HIRES2) then
        if (access(biasfile,'r').ne.0) then
          biasfile=' '
          write(soun,'(2a)') 'WARNING: No default bias fi',
     .                       'le exists for HIRES2.'
        endif
      else
C Build bias file filename.
        if (.not.EE_GetBiasFile(objhead,biasfile)) then
          write(soun,'(2a)') 
     .    'ERROR: makee: Could not find bias file: ',
     .    biasfile(1:lc(biasfile))
          ok=.false.
          return
        endif
      endif
C Set in header.
      call cheadset('BIASFILE',biasfile,objhead)

C OBJECT.................................
      call GetDimensions(objhead,osc,oec,osr,oer,onc,onr)
C Check for two dimensions.
      if ((onc.lt.2).or.(onr.lt.2)) then
        write(soun,'(a)') 'ERROR-- RAW images must be two dimensional.'
        ok=.false.
        return
      endif
      if (Global_HIRES.or.Global_HIRES2) then
        oecha= fhead('ECHANGL',objhead)
        oxda = fhead('XDANGL',objhead)
        deck = chead('DECKNAME',objhead)
        write(soun,'(a,2f8.4,2x,a,4i6,3x,a)') ' OBJECT:',
     .   oecha,oxda,deck(1:4),
     .   osc,oec,osr,oer,objfile(1:lc(objfile))
        call makee_file_header_info_string(objfile,objhead,wrd)
        call cheadset('OBJFILE',wrd,objhead)
      endif
      if (Global_ESI) then
        obstype = chead('OBSTYPE',objhead)
        slmsknam= chead('SLMSKNAM',header)
        write(soun,'(a,4i6,3x,a,2x,a,2x,a)') ' OBJECT:',
     .     osc,oec,osr,oer,objfile(1:lc(objfile)),
     .     obstype(1:7),slmsknam(1:4)
        call cheadset('OBJFILE',objfile,objhead)
        wrd = chead('SYNOPFMT',objhead)
        if (index(wrd,'Echellette').eq.0) then
          write(soun,'(a)') 'ERROR-- OBJECT file not echellette.'
          ok=.false.
          return
        endif
      endif

C STARFILE.............................
      starfile= arg(2)
      call SetRawFilename(starfile,rawdir,ok)
      if (ok) call readfits_header(starfile,header,ok) 
      if (.not.ok) then
        write(soun,'(2a)') 
     .  'ERROR: Could not read star file: ',starfile(1:lc(starfile))
        return
      endif
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
      if (Global_HIRES.or.Global_HIRES2) then
        echa = fhead('ECHANGL',header)
        xda  = fhead('XDANGL',header)
        deck = chead('DECKNAME',header)
        write(soun,'(a,2f8.4,2x,a,4i6,3x,a)') 
     .  '   STAR:',echa,xda,deck(1:4),
     .         sc,ec,sr,er,starfile(1:lc(starfile))
        call makee_file_header_info_string(starfile,header,wrd)
        call cheadset('STARFILE',wrd,objhead)
        call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                      echa, xda, sc, ec, sr, er, ok)
        if (.not.ok) return
      endif
      if (Global_ESI) then
        obstype = chead('OBSTYPE',header)
        slmsknam= chead('SLMSKNAM',header)
        write(soun,'(a,4i6,3x,a,2x,a,2x,a,2x,a)') '   STAR:',
     .         sc,ec,sr,er,starfile(1:lc(starfile)),
     .         obstype(1:7),slmsknam(1:4)
        call cheadset('STARFILE',starfile,objhead)
        call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                      echa, xda, sc, ec, sr, er, ok)
        if (.not.ok) return
        wrd = chead('SYNOPFMT',header)
        if (index(wrd,'Echellette').eq.0) then
          write(soun,'(a)') 'ERROR-- STAR TRACE file not echellette.'
          ok=.false.
          return
        endif
      endif

C FLATFILE.............................
      IF (narg.ge.3) THEN
        flatfile= arg(3)
        call SetRawFilename(flatfile,rawdir,ok)
        if (ok) call readfits_header(flatfile,header,ok)
        if (.not.ok) then
         write(soun,'(2a)') 
     .  'ERROR: Could not read flat: ',flatfile(1:lc(flatfile))
         return
        endif
        call GetDimensions(header,sc,ec,sr,er,nc,nr)
        if (Global_HIRES.or.Global_HIRES2) then
          echa = fhead('ECHANGL',header)
          xda  = fhead('XDANGL',header)
          deck = chead('DECKNAME',header)
          write(soun,'(a,2f8.4,2x,a,4i6,3x,a)') 
     .    '   FLAT:',echa,xda,deck(1:4),
     .           sc,ec,sr,er,flatfile(1:lc(flatfile))
          call makee_file_header_info_string(flatfile,header,wrd)
          call cheadset('FLATFILE',wrd,objhead)
          call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                    echa, xda, sc, ec, sr, er, ok)
          if (.not.ok) return
        endif
        if (Global_ESI) then
          obstype = chead('OBSTYPE',header)
          slmsknam= chead('SLMSKNAM',header)
          write(soun,'(a,4i6,3x,a,2x,a,2x,a)') '   FLAT:',
     .           sc,ec,sr,er,flatfile(1:lc(flatfile)),
     .           obstype(1:7),slmsknam(1:4)
          call cheadset('FLATFILE',flatfile,objhead)
          call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                    echa, xda, sc, ec, sr, er, ok)
          if (.not.ok) return
          wrd = chead('SYNOPFMT',header)
          if (index(wrd,'Echellette').eq.0) then
            write(soun,'(a)') 'ERROR-- FLAT file not echellette.'
            ok=.false.
            return
          endif
        endif
      ELSE
        flatfile=' '
        write(soun,'(2a)') 
     .  'WARNING: makee: Continuing with no flat field file.'
        write(soun,'(2a)') 
     .  'WARNING: makee: This is not recommended unless a'
        write(soun,'(2a)') 
     .  'WARNING: makee: pinhole quartz is used for trace.'
      ENDIF

C FIRST ARCFILE.............................
      IF (narg.ge.4) THEN
        arcfile1= arg(4)
        call SetRawFilename(arcfile1,rawdir,ok)
        if (ok) call readfits_header(arcfile1,header,ok)
        if (.not.ok) then
        write(soun,'(2a)')
     .  'ERROR: Could not read arc: ',arcfile1(1:lc(arcfile1))
        return
        endif
        call GetDimensions(header,sc,ec,sr,er,nc,nr)
        if (Global_HIRES.or.Global_HIRES2) then
          echa = fhead('ECHANGL',header)
          xda  = fhead('XDANGL',header)
          deck = chead('DECKNAME',header)
          write(soun,'(a,2f8.4,2x,a,4i6,3x,a)') 
     .    '    ARC:',echa,xda,deck(1:4),
     .           sc,ec,sr,er,arcfile1(1:lc(arcfile1))
          call makee_file_header_info_string(arcfile1,header,wrd)
          call cheadset('ARCFILE',wrd,objhead)
          call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                    echa, xda, sc, ec, sr, er, ok)
          if (.not.ok) return
        endif
        if (Global_ESI) then
          obstype = chead('OBSTYPE',header)
          slmsknam= chead('SLMSKNAM',header)
          write(soun,'(a,4i6,3x,a,2x,a,2x,a)') 
     .    '    ARC:',sc,ec,sr,er,arcfile1(1:lc(arcfile1)),
     .         obstype(1:7),slmsknam(1:4)
          call cheadset('ARCFILE',arcfile1,objhead)
          call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                    echa, xda, sc, ec, sr, er, ok)
          if (.not.ok) return
          wrd = chead('SYNOPFMT',header)
          if (index(wrd,'Echellette').eq.0) then
            write(soun,'(a)') 'ERROR-- First ARC file not echellette.'
            ok=.false.
            return
          endif
          ok = mk_check_arc(header)
          if (.not.ok) return
        endif
      ELSE
        arcfile1=' '
      ENDIF

C SECOND ARCFILE.............................
      IF (narg.ge.5) THEN
        arcfile2= arg(5)
        call SetRawFilename(arcfile2,rawdir,ok)
        if (ok) call readfits_header(arcfile2,header,ok)
        if (.not.ok) then
         write(soun,'(2a)')
     .  'ERROR:Could not read arc: ',arcfile2(1:lc(arcfile2))
         return
        endif
        call GetDimensions(header,sc,ec,sr,er,nc,nr)
        if (Global_HIRES.or.Global_HIRES2) then
          echa = fhead('ECHANGL',header)
          xda  = fhead('XDANGL',header)
          deck = chead('DECKNAME',header)
          write(soun,'(a,2f8.4,2x,a,4i6,3x,a)') 
     .  '   ARC2:',echa,xda,deck(1:4),
     .         sc,ec,sr,er,arcfile2(1:lc(arcfile2))
          call makee_file_header_info_string(arcfile2,header,wrd)
          call cheadset('ARCFILE2',wrd,objhead)
          call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                    echa, xda, sc, ec, sr, er, ok)
          if (.not.ok) return
        endif
        if (Global_ESI) then
          obstype = chead('OBSTYPE',header)
          slmsknam= chead('SLMSKNAM',header)
          write(soun,'(a,4i6,3x,a,2x,a,2x,a)') 
     .  '   ARC2:',sc,ec,sr,er,arcfile2(1:lc(arcfile2)),
     .         obstype(1:7),slmsknam(1:4)
          call cheadset('ARCFILE2',arcfile2,objhead)
          call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                    echa, xda, sc, ec, sr, er, ok)
          if (.not.ok) return
          wrd = chead('SYNOPFMT',header)
          if (index(wrd,'Echellette').eq.0) then
            write(soun,'(a)') 'ERROR-- Second ARC file not echellette.'
            ok=.false.
            return
          endif
          ok = mk_check_arc(header)
          if (.not.ok) return
        endif
      ELSE
        arcfile2=' '
      ENDIF

C ADDITIONAL ARC TO ARC#1........
      IF (arc1add.ne.' ') THEN
        call SetRawFilename(arc1add,rawdir,ok)
        if (ok) call readfits_header(arc1add,header,ok)
        if (.not.ok) then
         write(soun,'(2a)')
     .     'ERROR:Could not read arc: ',arc1add(1:lc(arc1add))
         return
        endif
        call GetDimensions(header,sc,ec,sr,er,nc,nr)
        if (Global_HIRES.or.Global_HIRES2) then
          echa = fhead('ECHANGL',header)
          xda  = fhead('XDANGL',header)
          deck = chead('DECKNAME',header)
          write(soun,'(a,2f8.4,2x,a,4i6,3x,a)') 
     .       'ARC1ADD:',echa,xda,deck(1:4),
     .         sc,ec,sr,er,arc1add(1:lc(arc1add))
          call makee_file_header_info_string(arc1add,header,wrd)
          call cheadset('ARC1ADD',wrd,objhead)
          call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                    echa, xda, sc, ec, sr, er, ok)
          if (.not.ok) return
        endif
        if (Global_ESI) then
          obstype = chead('OBSTYPE',header)
          slmsknam= chead('SLMSKNAM',header)
          write(soun,'(a,2f8.4,2x,a,4i6,3x,a,2x,a,2x,a)') 
     .       'ARC1ADD:',echa,xda,deck(1:4),
     .       sc,ec,sr,er,arc1add(1:lc(arc1add)),
     .       obstype(1:7),slmsknam(1:4)
          call cheadset('ARC1ADD',arc1add,objhead)
          call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                    echa, xda, sc, ec, sr, er, ok)
          if (.not.ok) return
        endif
      ENDIF

C ADDITIONAL ARC TO ARC#2........
      IF (arc2add.ne.' ') THEN
        call SetRawFilename(arc2add,rawdir,ok)
        if (ok) call readfits_header(arc2add,header,ok)
        if (.not.ok) then
         write(soun,'(2a)')
     .     'ERROR:Could not read arc: ',arc2add(1:lc(arc2add))
         return
        endif
        call GetDimensions(header,sc,ec,sr,er,nc,nr)
        if (Global_HIRES.or.Global_HIRES2) then
          echa = fhead('ECHANGL',header)
          xda  = fhead('XDANGL',header)
          deck = chead('DECKNAME',header)
          write(soun,'(a,2f8.4,2x,a,4i6,3x,a)') 
     .       'ARC2ADD:',echa,xda,deck(1:4),
     .         sc,ec,sr,er,arc2add(1:lc(arc2add))
          call makee_file_header_info_string(arc2add,header,wrd)
          call cheadset('ARC2ADD',wrd,objhead)
          call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                    echa, xda, sc, ec, sr, er, ok)
          if (.not.ok) return
        endif
        if (Global_ESI) then
          obstype = chead('OBSTYPE',header)
          slmsknam= chead('SLMSKNAM',header)
          write(soun,'(a,2f8.4,2x,a,4i6,3x,a,2x,a,2x,a)') 
     .       'ARC2ADD:',echa,xda,deck(1:4),
     .       sc,ec,sr,er,arc2add(1:lc(arc2add)),
     .       obstype(1:7),slmsknam(1:4)
          call cheadset('ARC2ADD',arc2add,objhead)
          call mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                    echa, xda, sc, ec, sr, er, ok)
          if (.not.ok) return
        endif
      ENDIF

      return
      end

 
C----------------------------------------------------------------------
C Check existence of bias file and (if not there) attempt to get
C bias (1-D) filename and check existence.
C Returns ".true." if successful. 
C
      logical function EE_GetBiasFile(header,biasfile)
C
      implicit none
      character*(*) header    ! FITS header (input).
      character*(*) biasfile  ! Bias filename (input/output).
C 
      character*80 biasroot,wrd 
      integer*4 numamps,lc,inhead,xbin,ybin,access
      integer*4 year4,year2,month,day
      logical EE_QueryHIRES,EE_QueryESI 
      logical EE_QueryHIRES2
C
C If bias file not there, create the default bias filename.
      if ((biasfile.eq.' ').or.(access(biasfile,'r').ne.0)) then
C Root name.
        if (EE_QueryHIRES(header)) then
          biasroot = 'biasHIRES_'
        elseif (EE_QueryHIRES2(header)) then
          biasroot = 'biasHIRES2_'
        elseif (EE_QueryESI(header)) then
          biasroot = 'biasESI_'
        else
          print *,'ERROR: EE_GetBiasFile: Unknown instrument.'
          call exit(0)
        endif
C Number of amplifiers during readout. 
        numamps = inhead('NUMAMPS',header)
C Binning.
        call GetBinning(header,xbin,ybin)
C Default "makee" bias file.
        call GetMakeeHome(wrd)
        if (numamps.eq.1) then
          write(biasfile,'(3a,i1,a,i1,a)') wrd(1:lc(wrd)),'/',
     .      biasroot(1:lc(biasroot)),xbin,'x',ybin,'.fits'
        else
          write(biasfile,'(3a,i1,a,i1,a)') wrd(1:lc(wrd)),'/',
     .      biasroot(1:lc(biasroot)),xbin,'x',ybin,'_2amp.fits'
        endif
C
C Use newer HIRES bias file.
        if (EE_QueryHIRES(header)) then
        if ((xbin.eq.1).and.(ybin.eq.2).and.(numamps.eq.1)) then
          call ReadFitsDate('DATE-OBS',header,year4,year2,month,day)
          if (year4.ge.1999) then
            write(biasfile,'(3a,i1,a,i1,a)') wrd(1:lc(wrd)),'/',
     .        biasroot(1:lc(biasroot)),xbin,'x',ybin,'-jun00.fits'
          endif
        endif
        endif
C
      endif
C
C Check existence.
      if (access(biasfile,'r').eq.0) then
        EE_GetBiasFile = .true.
      else
        EE_GetBiasFile = .false.
      endif
C 
      return
      end
 

C----------------------------------------------------------------------
C Check angles and dimensions between object and other file.
C
      subroutine mk_check_file(oecha,oxda,osc,oec,osr,oer,
     .                          echa, xda, sc, ec, sr, er, ok)
C
      implicit none
      real*8 oecha,oxda,echa,xda
      integer*4 osc,oec,osr,oer,sc,ec,sr,er
      logical ok
C
      include 'global.inc'
      include 'soun.inc'
C
      if (Global_HIRES.or.Global_HIRES2) then
        if ( (abs(oecha-echa).gt.0.02).or.(abs(oxda-xda).gt.0.02) ) then
          write(soun,'(a)') 
     .    'ERROR- Echelle or Cross Disperser angles differ.'
          ok=.false.
          return
        endif
        if ( (abs(oecha-echa).gt.0.0021).or.
     .                    (abs(oxda-xda).gt.0.0021) ) then
          write(soun,'(a)') 
     .    'WARNING- Echelle or Cross Disperser angles differ slightly.'
        endif
      endif
C
      if ( (osc.ne.sc).or.(oec.ne.ec).or.(osr.ne.sr).or.
     .                                      (oer.ne.er) ) then
        write(soun,'(a)') 'ERROR- Image dimensions not identical.'
        ok=.false.
        return
      endif
C
      ok=.true.
      return
      end


C----------------------------------------------------------------------
C Make a string with some useful information.
C
      subroutine makee_file_header_info_string(file,header,s)
C
      implicit none
      character*(*) file   ! FITS filename (input).
      character*(*) header ! Keck HIRES FITS header (input).
      character*(*) s      ! string with info (output).
C
      character*80 chead,c1,c2,c3,c4
      integer*4 inhead,i1,i2,i,lc
      real*8 fhead,f1,f2
C
      i1=inhead('FRAMENO',header)
      i2=inhead('EXPOSURE',header)
      if (i2.lt.0) then           ! added exptime -tab 20nov07
        i2=inhead('EXPTIME',header)
      endif
C
      c1= chead('LAMPNAME',header)
      c2= chead('LFILNAME',header)
      c3= chead('FIL1NAME',header)
      c4= chead('DECKNAME',header)
C
      f1= fhead('ECHANGL',header)
      f2= fhead('XDANGL',header)
C
C Filename.
      i = index(file,'.fits')
      if (i.eq.0) i=lc(file)+1
      s = ' '
      s(max(1,i-19):i-1) = file(max(1,i-19):i-1)
      s = s(1:19)//':'
C Other stuff.
      write(s,'(2a,2i5,3(1x,a),2f8.4,x,a)') 
     .   s(1:19),':',i1,i2,c1(1:5),c2(1:5),c3(1:5),f1,f2,c4(1:3)
C
CVOFFSET1= '----+----1----+----2----+----3----+----4----+----5----+----6----+---
C           1234567890123456789: 1234 1234 12345 12345 12335 12.1234 12.1234 123
C           ./raw1raw/hires0023:   45  300 quart clear kv318  0.4982 -1.3489 C12
      return
      end


C----------------------------------------------------------------------
C Write two informative lines about this HIRES or ESI FITS file.
C
      subroutine mk_object_info_lines(fitsfile,header)
C
      implicit none
      character*(*) fitsfile   ! FITS filename (input)
      character*(*) header     ! FITS header (input)
C
      include 'global.inc'
      include 'soun.inc'
C
      real*4 echa,xda,valread,slit
      real*8 fhead,ra,dec
      integer*4 j,k,lc,xbin,ybin,fc
      integer*4 frameno,inhead,exposure,revindex
      integer*4 hh,nn,ss,year4,yy,mm,dd,equinox
      character*80 chead,lampname,deckname,fil1name,lfilname
      character*80 object,shutter,hatch,wrd,ts,coord
      character c9*9, c7a*7, c7b*7, cd*2
      logical lhead,DarkObs,card_exist

C Find binning factors.
      call GetBinning(header,xbin,ybin)
      xbin = max(0,min(9,xbin))
      ybin = max(0,min(9,ybin))

C Various HIRES cards.
      if (Global_HIRES.or.Global_HIRES2) then
        deckname = chead('DECKNAME',header)
        call upper_case(deckname)
        lampname = chead('LAMPNAME',header)
        call lower_case(lampname)
        lfilname = chead('LFILNAME',header)
        call lower_case(lfilname)
        fil1name = chead('FIL1NAME',header)
        call lower_case(fil1name)
        wrd= chead('XDISPERS',header)
        call lower_case(wrd)
        cd = wrd(fc(wrd):fc(wrd)+1)
        echa= max(-99.,min(999.,sngl(fhead('ECHANGL',header))))
        xda = max(-99.,min(999.,sngl(fhead('XDANGL',header))))
        slit= max(-99.,min(999.,sngl(fhead('SLITWID',header))))
C Hatch.
        if (lhead('HATOPEN',header)) then
          hatch='open'
        else
          hatch='closed'
        endif
C Shutter.
        if (DarkObs(header)) then
          shutter='closed'
        else
          shutter='open'
        endif
      endif

C More cards.
      object   = chead('OBJECT',header)
      frameno  =inhead('FRAMENO',header)
      exposure =inhead('EXPOSURE',header)

C Coordinate name.
      equinox = inhead('EQUINOX',header)
      ra = fhead('RA',header)
      dec= fhead('DEC',header)
      if (equinox.eq.1950) then
        call write_radec(ra,dec,c9,2)
      elseif (equinox.eq.2000) then
        call precess(2000.d0,1950.d0,ra,dec)
        call write_radec(ra,dec,c9,2)
      else
        call precess(2000.d0,1950.d0,ra,dec)
        call write_radec(ra,dec,c9,2)
      endif
      coord = c9

C Date and time string.
      call ReadFitsDate('DATE-OBS',header,year4,yy,mm,dd)

C Time.
      wrd = chead('UT',header)
      if (.not.card_exist('UT',header)) then
        if (card_exist('UTC',header)) then
          wrd = chead('UTC',header)
        endif
      endif
      j = index(wrd,':')
      hh = nint(valread(wrd(1:j-1)))
      wrd(j:j)=' '
      k = index(wrd,':')
      nn = nint(valread(wrd(j+1:k-1)))
      ss = nint(valread(wrd(k+1:k+5)))
      write(ts,'(i2.2,a,i2.2,a,i2.2,1x,i2.2,a,i2.2,a,i2.2)')
     .    yy,'/',mm,'/',dd,hh,':',nn,':',ss

C Abreviated filename.
      k = max(0,revindex(fitsfile,'/'))
      wrd = fitsfile(k+1:lc(fitsfile))

      if (Global_ESI) then

        write(soun,'(a,2x,a,2x,a,i5,i6,a)')
     .     object(1:20),coord(1:9),
     .     ts(1:17),frameno,exposure,'s'

        write(soun,'(a,2x,i1,a,i1,4x,2a)')
     .     'Binning: ',xbin,'x',ybin,'    File: ',wrd(1:lc(wrd))

      elseif (Global_HIRES.or.Global_HIRES2) then

C Echelle and XD angles.
        write(c7a,'(f7.4)') echa
        write(c7b,'(f7.4)') xda
        if (c7a(1:1).eq.' ') c7a(1:1)='+'
        if (c7b(1:1).eq.' ') c7b(1:1)='+'
C Write line.
        write(soun,'(a,2x,a,2x,a,i6,i6,a,2x,a)')
     .     object(1:20),coord(1:9),
     .     ts(1:17),frameno,exposure,'s',lampname(1:5)

        write(soun,'(5(2x,a),f6.2,3(2x,a),2x,i1,a,i1,2x,a)')
     .     lfilname(1:5),fil1name(1:5),
     .     c7a,c7b,
     .     deckname(1:3),slit,
     .     shutter(1:4),hatch(1:4),cd,xbin,'x',ybin,wrd(1:lc(wrd))

      else
        write(soun,'(a)') '.............UNKNOWN INSTRUMENT..........'

      endif


      return
      end


C----------------------------------------------------------------------
C Put makee arguments into header cards...
C
      subroutine makeearg_string(header,UserEperdn,UserRonoise,printer,
     .  pse_mode,user_shift,usw,parmfile,biasfile,rawdir,
     .  newobject,observer,logfile,whole_thing,test,DoAtm,
     .  NoVac,NoHC,nozero,noskyshift,nobias)
C
      implicit none
C
      character*(*) header
      real*4 UserEperdn,UserRonoise
      character*(*) printer,parmfile,biasfile
      character*(*) rawdir,newobject,observer,logfile
      integer*4 pse_mode
      real*8 user_shift
      real*4 usw
      logical whole_thing,test,DoAtm,NoVac,NoHC
      logical nozero,noskyshift,nobias
C
      include 'makee.inc'
      include 'soun.inc'
C
      character*200 s,wrd
      integer*4 lc,count,ii
      logical okoflags,firstoko
C
      count=1
      wrd=' ' 
      s  =' '
C
      if (UserEperdn.gt.-0.01) then
        write(wrd,'(a,1pe10.3)',err=9) 'eperdn=',UserEperdn
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (UserRonoise.gt.-0.01) then
        write(wrd,'(a,1pe10.3)',err=9) 'ronoise=',UserRonoise
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (printer.ne.' ') then
        write(wrd,'(2a)',err=9) 'lpr=',printer(1:lc(printer))
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (parmfile.ne.' ') then
        write(wrd,'(2a)',err=9) 'pf=',parmfile(1:lc(parmfile))
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (biasfile.ne.' ') then
        write(wrd,'(2a)',err=9) 'bias=',biasfile(1:lc(biasfile))
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (rawdir.ne.' ') then
        write(wrd,'(2a)',err=9) 'raw=',rawdir(1:lc(rawdir))
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (newobject.ne.' ') then
        write(wrd,'(2a)',err=9) 'object=',newobject(1:lc(newobject))
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (observer.ne.' ') then
        write(wrd,'(2a)',err=9) 'observer=',observer(1:lc(observer))
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (logfile.ne.' ') then
        write(wrd,'(2a)',err=9) 'log=',logfile(1:lc(logfile))
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (pse_mode.ne.-1) then
        write(wrd,'(a,i2.2)',err=9) 'mode=',pse_mode
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (user_shift.gt.-1.e+29) then
        write(wrd,'(a,1pe10.3)',err=9) 'skyshift=',user_shift
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (usw.gt.0.) then
        write(wrd,'(a,1pe10.3)',err=9) 'sl=',usw
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (UserHalfWidth.gt.0.) then
        write(wrd,'(a,1pe10.3)',err=9) 'hw=',UserHalfWidth
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (uop_exist) then
        write(wrd,'(a,a)',err=9) 'uop=',uopfile(1:lc(uopfile))
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (.not.whole_thing) then
        wrd='-notwhole'
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (test) then
        wrd='-test'
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (DoAtm) then
        wrd='-atm'
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (NoVac) then
        wrd='-novac'
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (NoHC) then
        wrd='-nohc'
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (.not.nozero) then
        wrd='-showmask'
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (noskyshift) then
        wrd='-noskyshift'
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      if (nobias) then
        wrd='-nobias'
        call makeearg_string_write(header,count,s,wrd)
      endif
C
      okoflags=.false.
      do ii=1,mo
        if (.not.oko(ii)) okoflags=.true.
      enddo
      if (okoflags) then
        firstoko=.true.
        wrd = 'oko='
        do ii=1,mo
          if (oko(ii)) then
            if(.not.firstoko)write(wrd,'(2a)')wrd(1:lc(wrd)),','
            firstoko=.false.
            if (ii.lt.10) then
              write(wrd,'(a,i1)') wrd(1:lc(wrd)),ii
            else
              write(wrd,'(a,i2)') wrd(1:lc(wrd)),ii
            endif
          endif
        enddo
        call makeearg_string_write(header,count,s,wrd)
      endif
C
C Finish it off.
      wrd=' '
      wrd(90:90)='X'
      call makeearg_string_write(header,count,s,wrd)
C
      return
9     continue
      write(soun,'(a)')'WARNING: problem writing MAKEE### header cards.'
      return
      end

C----------------------------------------------------------------------
C Add a word to makeearg string and write out if necessary.
C
      subroutine makeearg_string_write(header,count,s,wrd)
C
      implicit none
      character*(*) header
      integer*4 count
      character*(*) s
      character*(*) wrd
      character*8 card
      integer*4 lc
C Write out string to header.
      if ( (lc(s)+lc(wrd)).gt.66 ) then
        write(card,'(a,I3.3)') 'MAKEE',count
        count=count+1
        call cheadset(card,s,header)
        s=' '
        s=wrd(1:lc(wrd))
      else
C Add word to string.
        s = s(1:lc(s))//' '//wrd(1:lc(wrd))
      endif
      return
      end


C----------------------------------------------------------------------
C Load user object positions file ("uopfile").
C
      subroutine EE_load_uop( )
C
      implicit none
      include 'makee.inc'
      include 'soun.inc'
C
      integer*4 ii,jj,kk,lc,nw,nv,numback,access
      character*500 line
      character*50 wrds(50)
      real*8 vv(50),fgetlinevalue,r8,r81,r82
      logical eons(mo)
C
C Check file.
      if (access(uopfile,'r').ne.0) then
        write(soun,'(2a)') 
     . 'ERROR: "uop=" file does not exist:',uopfile(1:lc(uopfile))
        call exit(0)
      endif
C
C Clear arrays.
      do ii=1,mo
        uop_c(ii)  =  0.
        uop_h(ii)  = -1.
        uop_b1o(ii)= -900.
        uop_b1i(ii)= -900.
        uop_b2i(ii)= -900.
        uop_b2o(ii)= -900.
        eons(ii)   = .true.
      enddo
      numback=0
C Open file.
      open(1,file=uopfile,status='old')
1     continue
      read(1,'(a)',end=5) line
C Load data values. 
      call EE_get_words(line,wrds,nw)
      do ii=1,nw
C Load active order numbers.
        if (index(wrds(ii),'o=').gt.0) then
          do jj=1,mo
            eons(jj)=.false.
          enddo
          call EE_get_values(wrds,vv,nv)
          do jj=1,nv
            kk = nint(vv(jj))
            if ((kk.ge.1).and.(kk.le.mo)) eons(kk) = .true.
          enddo
          numback=0
C Load center position.
        elseif (index(wrds(ii),'c=').gt.0) then
          r8 = fgetlinevalue(wrds(ii),1)
          if (r8.gt.-1.e+10) then
            do jj=1,mo
              if (eons(jj)) uop_c(jj) = r8
            enddo
          endif
C Load background range.
        elseif (index(wrds(ii),'b=').gt.0) then
          r81 = fgetlinevalue(wrds(ii),1)
          r82 = fgetlinevalue(wrds(ii),2)
          if ((r81.gt.-1.e+10).and.(r82.gt.-1.e+10)) then
            if (numback.eq.0) then
              do jj=1,mo
                if (eons(jj)) then
                  uop_b1o(jj) = r81
                  uop_b1i(jj) = r82
                endif
              enddo
            else
              do jj=1,mo
                if (eons(jj)) then
                  uop_b2i(jj) = r81
                  uop_b2o(jj) = r82
                endif
              enddo
            endif
            numback=numback+1
          endif
C Load halfwidth value.
        elseif (index(wrds(ii),'h=').gt.0) then
          r8 = fgetlinevalue(wrds(ii),1)
          if (r8.gt.0.001) then
            do jj=1,mo
              if (eons(jj)) uop_h(jj) = r8
            enddo
          endif
        endif
      enddo
      goto 1 
5     continue
      close(1)
      uop_exist = .true.
      return
      end

C----------------------------------------------------------------------
C Get words out of a line.
C
      subroutine EE_get_words(line,wrds,nw)
C
      implicit none
      character*(*) line
      character*50 wrds(50)
      integer*4 nw,ii,iw,lc
      logical load
      load=.false.
      nw=0
      iw=0
      do ii=1,lc(line)
C ASCII #40 is "(".  "sp" is below #40
        if (ichar(line(ii:ii)).gt.39) then
          if (load) then
            iw=iw+1
            wrds(nw)(iw:iw) = line(ii:ii)
          else
            load=.true.
            nw=nw+1
            iw=1
            wrds(nw)='                                             '
            wrds(nw)(iw:iw) = line(ii:ii)
          endif
        else
          load=.false.
        endif
      enddo
      return
      end

C----------------------------------------------------------------------
C Get values out of a word.
C
      subroutine EE_get_values(wrd,vv,nv)
C
      implicit none
      character*(*) wrd
      integer*4 nv,ii
      real*8 vv(*),r8,fgetlinevaluequiet
      logical more
C
      more=.true.
      ii=0
      do while (more)
        ii=ii+1
        r8 = fgetlinevaluequiet(wrd,ii)
        if (r8.gt.-1.e+20) then
          vv(ii) = r8
        else
          more=.false.
        endif
      enddo
      nv = ii - 1
      return
      end
 
C----------------------------------------------------------------------
C Delete an echelle order from the list.
C
      subroutine EE_delete_echelle_order(eon)
C
      implicit none
      integer*4 eon,i,j
      include 'makee.inc'
      include 'global.inc'
      do i=0,mpp
      do j=eon,mo-1
        eo(i,j)  =eo(i,j+1)
        sp1(i,j) =sp1(i,j+1)
        sp2(i,j) =sp1(i,j+1)
        bk1o(i,j)=bk1o(i,j+1)
        bk2o(i,j)=bk2o(i,j+1)
        bk1i(i,j)=bk1i(i,j+1)
        bk2i(i,j)=bk2i(i,j+1)
      enddo
      enddo
      Global_neo = Global_neo - 1
      return
      end


C----------------------------------------------------------------------
C Create a new filename for HIRES2 data.  If .false. then the current 
C filename is already a single CCD file (based on the filename only).
C So we do not need to call ffchires2readwrite().
C
      logical function SetNewFilename_HIRES2( 
     .                   rawfile, ccdloc, temphead, newfile )
C
      implicit none
      character*(*) rawfile
      integer*4 ccdloc
      character*(*) temphead
      character*(*) newfile
C
      integer*4 ii,lc,inhead
      logical ok
      character*20 ext
C
C Does file have the _#.fits extension?
      write(ext,'(a,i1,a)') '_',ccdloc,'.fits'
      if (index(rawfile,ext).gt.0) then
        newfile = rawfile
        SetNewFilename_HIRES2 = .false.
        return
      endif
C
C Is file a single CCD file?
      call readfits_header_rawhires2(rawfile,temphead,ok) 
      ii = inhead('CCDLOC',temphead)
      if (ii.eq.ccdloc) then
        newfile = rawfile
        SetNewFilename_HIRES2 = .false.
        return
      endif
C
      ii = index(rawfile,'.fits')
      newfile = rawfile(1:ii-1)//ext(1:lc(ext))
      SetNewFilename_HIRES2 = .true.
C
      return
      end

