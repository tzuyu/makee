/* HIRES2_readwrite.c                  tab  Sep04  Apr05 */


#include "ctab.h"
#include "cmisc.h"
#include "cufio.h"


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Read HIRES 2004 raw data file and write individual images.
*/
int main(int argc, char *argv[])
{
/**/
char arg[500][100];
char wrd[100];
/**/
int narg,mode,verbose,quiet;
/**/

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Options. */
mode = 4;
if (cfindarg(arg,&narg,"mode=",':',wrd)==1) { mode = GetLineValue(wrd,1); }

verbose = 0;
if (cfindarg(arg,&narg,"-verbose",'s',wrd)==1) { verbose = 1; }
if (cfindarg(arg,&narg,"-v"      ,'s',wrd)==1) { verbose = 1; }

quiet = 0;
if (cfindarg(arg,&narg,"-quiet",'s',wrd)==1) { quiet = 1; }

/* Syntax. */
if (narg != 1) {
  printf("Syntax: HIRES2_readwrite (raw HIRES 2004 FITS file) [ mode= ]\n");
  printf("                [ -v | -verbose ]   [ -quiet ]\n");
  printf("\n");  
  printf("Options:\n");
  printf("   mode=  :  0 : write out _0.fits only.\n");
  printf("          :  1 : write out _1.fits only.\n");
  printf("          :  2 : write out _2.fits only.\n");
  printf("          :  3 : write out _3.fits only.\n");
  printf("          :  4 : write out all files (default).\n");
  printf("          :  5 : write out all files, except _0.fits.\n");
  printf("          : -1 : information only.\n");
  printf("\n");  
  exit(0);
}

/* Run routine. */
chires2_readwrite( arg[1], mode, verbose, quiet );

return(0);
}

