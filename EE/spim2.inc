C spim2.inc
C
C Maximum dimensions.
      integer*4 mpt       ! Max. points (over a single order)
      integer*4 mptbig    ! Max. points in big arrays (over many orders)
      integer*4 mrw,mim   ! Max. rows, images.
      integer*4 mord      ! Max. orders.
      integer*4 marr      ! Max. entries in misc. arrays.
C
      parameter(mord=20,mpt=4100,mrw=48,mim=5,mptbig=90000,marr=9000)
C...................
C
      character*115200 header(mim)   ! FITS headers.
      character*115200 eheader(mim)  ! FITS headers (error arrays)
      character*115200 temphead      ! Temporary FITS header.
      character*115200 bighead       ! Big spectrum FITS header.
C
      common /SPIM2BLK115200/ header,eheader,temphead,bighead
C...................
C
      character*80 dfile(mim)    ! Data FITS file filenames.
      character*80 efile(mim)    ! Data FITS (error) file filenames.
      character*80 bigfile       ! FITS filename for big spectrum.
C
      common /SPIM2BLK80/ dfile,efile,bigfile
C...................
C
      real*8 wo8(mpt,mrw,mim)     ! orig. wavelengths (8 byte version)
      real*8 cof(0:mord,mrw,mim)  ! polynomial coeffecients of wavel. scale
      real*8 icof(0:mord,mrw,mim) ! inverse poly. coeff. wavescale.
      real*8 x8(mptbig)           ! scratch array (x values) used for fits,etc.
      real*8 y8(mptbig)           ! scratch array (y values) used for fits,etc.
      real*8 z8(mptbig)           ! scratch array (z values) used for fits,etc.
C
      common /SPIM2BLK8/ wo8,cof,icof,x8,y8,z8
C...................
C
      real*4 ff(mptbig)      ! flux values for big spectrum.
      real*4 ww(mptbig)      ! wavelength values for big spectrum.
      real*4 ffb(mptbig)     ! flux values for big spectrum (binned).
      real*4 wwb(mptbig)     ! wavelength values for big spectrum (binned).
C
      real*4 f(mpt,mrw,mim)  ! flux values (each pt, each order, each image).
      real*4 e(mpt,mrw,mim)  ! error values
      real*4 w(mpt,mrw,mim)  ! wavelengths.
      real*4 fb(mpt,mrw,mim) ! flux values (binned)
      real*4 eb(mpt,mrw,mim) ! error values (binned)
      real*4 wb(mpt,mrw,mim) ! wavelengths values (binned)
      real*4 fo(mpt,mrw,mim) ! original flux values.
      real*4 eo(mpt,mrw,mim) ! original error values.
C
      real*4 ys(mrw)         ! scaling values which will be applied to each row.
      real*4 arr(marr)       ! scratch array.
      real*4 blwv(mrw,mim)   ! blue wavelength limits.
      real*4 rdwv(mrw,mim)   ! red wavelength limits.
      real*4 xex             ! x extension fraction (for plotting).
C
      real*4 ObjAir          ! Airmass value for object (could change).
      real*4 StarAir         ! Airmass value for star (could change).
      real*4 StarPS          ! Pixel Shift to apply to star to line up w/Object.
C
      real*4 aablu(marr)     ! Atm. Abs. blue limit  (set other regions to 1.0
C                            !                        in star before division).
      real*4 aared(marr)     ! Atm. Abs. red limit.
      real*4 aarsi(marr)     ! Atm. Abs. residual intensity (of absorp. line).
C
      real*4 xmin,xmax,ymin,ymax   ! plotting ranges.
      real*4 minratrej,maxratrej   ! minimum and maximum ratios (Match_To_Big)
C
      common /SPIM2BLK4/ ff,ww,ffb,wwb,f,e,w,fb,eb,wb,fo,eo,ys,arr,
     .                   blwv,rdwv,xex,ObjAir,StarAir,StarPS,
     .                   aablu,aared,aarsi,xmin,xmax,ymin,ymax,
     .                   minratrej,maxratrej
C...................
C
      integer*4 sc(mim)   ! starting column for each image.
      integer*4 ec(mim)   ! ending column for each image.
      integer*4 nc(mim)   ! number of columns for each image.
      integer*4 scb(mim),ecb(mim),ncb(mim)   ! (binned)
      integer*4 ncbig,ncbigb   ! number of columns in big spectrum and (binned)
      integer*4 nrw(mim)  ! number of rows in image.
      integer*4 nim,prim  ! number of images and current primary image number.
      integer*4 bin       ! binning factor for binned arrays.
      integer*4 nptbig    ! number of points in big spectrum.
      integer*4 AtmIm     ! Atmospheric division star flag.
      integer*4 naal      ! Number of atmospheric abs. lines in table.
      integer*4 v_order   ! polynomial fit order for 'v' option.
      integer*4 ord(mrw,mim)   ! poly. orders for forward fits.
      integer*4 iord(mrw,mim)  ! poly. orders for inverse fits.
      integer*4 divart    ! add artificial points automatically (Match_To_Big)
C
      common /SPIM2BLKI4/ sc,ec,nc,scb,ecb,ncb,ncbig,ncbigb,
     .                    nrw,nim,prim,bin,
     .                    nptbig,AtmIm,naal,v_order,ord,iord,divart
C...................
C
      logical modified(mim)    ! Has image been modified?
      logical StarDivDone(mrw) ! Has Atm. Abs. division been done to row?
      logical ShowBin          ! Show binned arrays (spectrums)?
      logical hide             ! Hide other spectra?
      logical hidebits         ! Hide bits of other adjacent orders?
      logical hideorig         ! Hide original (unmodified) spectrum?
      logical hist             ! Plot as histogram format (i.e. show bin edges)?
      logical yfix             ! Fix y range on plots?
      logical yfixzero         ! Fix lower y range at zero?
C
      common /SPIM2BLKL/ modified,StarDivDone,ShowBin,hide,
     .                   hidebits,hideorig,
     .                   hist,yfix,yfixzero
C...................


C OLD...
C RejectNearMask                  : With cd= option, reject only if near 

