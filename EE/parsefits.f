C parsefits.f                                            tab
C
      implicit none
      integer*4 nc,nr,sc,sr,er,ec,narg,row,i,n,lc
      integer*4 ord,k,rownum,npix,inhead
      character*115200 header,newhead
      character*80 arg(9),fullfile,newfile,chead,newobject,object
      character*80 wv0,wv4,LP0,LP1,IP0,IP1,wrd
      character c7*7
      integer*4 maxpt
      parameter(maxpt=201000)
      real*4 a(maxpt),valread
      real*8 coef(0:20),P(0:20),IP(0:20),CW,CP,wvll,valread8
      real*8 x8(9000),y8(9000),w8(9000),polyval,high,wrms,getx
      logical ok,findarg,onedim,xy

C Get command line parameters.
      call arguments(arg,narg,9)
      xy     = findarg(arg,narg,'-xy',':',wrd,i)
      onedim = findarg(arg,narg,'-1d',':',wrd,i)
      wvll=0.d0
      if (findarg(arg,narg,'wvll=',':',wrd,i)) wvll=valread8(wrd)
      rownum=0
      if (findarg(arg,narg,'row=',':',wrd,i)) rownum=nint(valread(wrd))
      if (narg.ne.1) then
        print *,
     .  'Syntax: parsefits (pixel or wavelength calib. FITS file)'
        print *,'                  [-1d]  [row=]  [-xy]  [wvll=]'
        print *,
     .  '  Produces Vista-compatible individual pixel or wavelength'
        print *,
     .  '  calibrated FITS spectra.  Input: filename.fits  Output:'
        print *,
     .  '  filename-###.fits where ### is the row/order number.'
        print *,'OPTIONS:'
        print *,
     .  '   -xy  : Write out as ASCII X-Y pair file.'
        print *,
     .  '  wvll= : Only write rows with wavelengths greater than this.'
        print *,
     .  '   -1d  : Write out as one dimensional (NAXIS=1) image.' 
        print *,'  row=  : Only write out file for row number given.'
        call exit(0)
      endif
      fullfile = arg(1)
      call AddFitsExt(fullfile)

C Read fits file...
      call readfits(fullfile,a,maxpt,header,ok)
      if (.not.ok) then
        print *,'Error reading fits file: ',fullfile
        call exit(1)
      endif
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
      if ((nc.eq.0).or.(nr.eq.0)) then
        print *,'ERROR-- Images must be two dimensional.'
        call exit(1)
      endif
      object = chead('OBJECT',header)

C Copy header to new header and remove unneccessary cards.
      newhead = header
      do row=sr,er
        write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
        call unfit(c7,newhead)
        write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
        call unfit(c7,newhead)
        write(c7,'(a,i1,a,i2.2)') 'CO_',0,'_',row
        call unfit(c7,newhead)
        write(c7,'(a,i1,a,i2.2)') 'CO_',4,'_',row
        call unfit(c7,newhead)
      enddo

C Extract wavelength scale and convert to Vista format.
      DO row=sr,er
      IF ((rownum.eq.0).or.(rownum.eq.row)) THEN

C Extract coeffecients.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,header)
      if (wv0.eq.' ') goto 700
      read(wv0,'(4(1pe17.9))',err=800) coef(0),coef(1),coef(2),coef(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,header)
      if (wv4.eq.' ') goto 700
      read(wv4,'(4(1pe17.9))',err=800) coef(4),coef(5),coef(6),coef(7)
C Fill array and apply pixel shift around center pixel.
      CP = dfloat((sc+ec)/2)
      n=0
      do i=sc,ec,10
        n=n+1
        x8(n)=dfloat(i) - CP
        y8(n)=polyval(7,coef,dfloat(i))
        w8(n)=1.d0
      enddo
C Refit polynomial.
      ord=6
      call poly_fit_glls(n,x8,y8,w8,ord,P,ok)
      if (.not.ok) then
        print *,'Error refitting polynomial.'
        goto 999
      endif
C Check residuals.
      call poly_fit_glls_residuals(n,x8,y8,w8,ord,P,high,wrms)
      if ((wrms.gt.0.0001).or.(abs(high).gt.0.0005)) then
        print '(a,i5,2f15.7)',' Row, High, WRMS (Ang) = ',row,high,wrms
      endif
C Fit inverse polynomial.
      CW = polyval(7,coef,CP)
      n=0
      do i=sc,ec,10
        n=n+1
        x8(n)=polyval(7,coef,dfloat(i)) - CW
        y8(n)=dfloat(i)
        w8(n)=1.d0
      enddo
      ord=6
      call poly_fit_glls(n,x8,y8,w8,ord,IP,ok)
      if (.not.ok) then
        print *,'Error refitting polynomial.'
        goto 999
      endif
C Check residuals.
      call poly_fit_glls_residuals(n,x8,y8,w8,ord,IP,high,wrms)
      if ((wrms.gt.0.005).or.(high.gt.0.02)) then
        print '(a,i5,2f15.7)',' Row, High, WRMS (Pix) = ',row,high,wrms
      endif
C Write header cards.
      write(LP0,'(1PE15.7,3(1PE16.7))') CP,P(0),P(1),P(2)
      write(LP1,'(1PE15.7,3(1PE16.7))') P(3),P(4),P(5),P(6)
      write(IP0,'(1PE15.7,3(1PE16.7))') CW,IP(0),IP(1),IP(2)
      write(IP1,'(1PE15.7,3(1PE16.7))') IP(3),IP(4),IP(5),IP(6)
C Add new cards to new header.
      call inheadset('LAMORD',7,newhead)
      call cheadset('CTYPE1','POLY_LAMBDA',newhead)
      call cheadset('LPOLY0',LP0,newhead)
      call cheadset('LPOLY1',LP1,newhead)
      call cheadset('IPOLY0',IP0,newhead)
      call cheadset('IPOLY1',IP1,newhead)

C Pixel scale only starts here.
700   continue
      write(newobject,'(a,a,i2.2,a)') 
     .  object(1:lc(object)),' [',min(99,row),']'
      call cheadset('OBJECT',newobject,newhead)
      call inheadset('NAXIS2',1,newhead)
      call inheadset('CRVAL2',0,newhead)
      call inheadset('ROWORD',row,newhead)
      if (onedim) then
        call unfit('NAXIS2',newhead)
        call unfit('CRVAL2',newhead)
        call unfit('CDELT2',newhead)
        call unfit('CRPIX2',newhead)
        call inheadset('NAXIS',1,newhead)
      endif

C Get polynomial scale.
      npix = max(0,inhead('NAXIS1',newhead))
      if (getx(-1.d0,newhead).lt.0.) goto 800
      do i=1,npix
        x8(i) = getx(dfloat(i),newhead)
      enddo

C Skip files with bluer wavelengths.
      IF (x8(npix).gt.wvll) THEN

      if (xy) then
C Write out x-y file.
        i=index(fullfile,'.fits')
        if (i.eq.0) then
          print *,'Extension must be .fits .'
          call exit(1)
        endif
        write(newfile,'(a,a,i3.3,a)') fullfile(1:i-1),'-',row,'.xy'
        k = 1 + (nc*(row-1))
        open(33,file=newfile,status='unknown')
        do i=1,npix
          write(33,'(f10.4,1pe14.6)') x8(i),a(k+i-1)
        enddo
        close(33)
      else
C Write out a single row as a spectrum.
        i=index(fullfile,'.fits')
        if (i.eq.0) then
          print *,'Extension must be .fits .'
          call exit(1)
        endif
        write(newfile,'(a,a,i3.3,a)') fullfile(1:i-1),'-',row,'.fits'
        k = 1 + (nc*(row-1))
        call writefits(newfile,a(k),newhead,ok)
        if (.not.ok) then
          print *,'Error writing fits file: ',newfile
          call exit(1)
        endif
      endif

      ENDIF

      ENDIF
      ENDDO

      print *,'All done.'
      goto 999
800   continue
      print *,'Error reading WV header card.'
      goto 999
999   stop
      end

