C ------------------------------------------------------------------------------
C Bin (average) a linear wavelength or pixel scale FITS image.
C
      implicit none
C
      include 'maxpix.inc'
C
      real*4 a(maxpix),e(maxpix)
      real*4 a2(maxpix),e2(maxpix)
      integer*4 i,narg,inhead,lc,bin,nc,nr,j,nnc,nnr,binc,binr
      real*4 valread
      character*115200 header,eheader
      character*80 file,arg(9),wrd,efile,dfile,chead
      real*8 fhead,crval1,cdelt1
      logical ok,findarg,nozero,sumit

      call arguments(arg,narg,9)
      bin =0
      binr=0
      binc=0
      if (findarg(arg,narg,'bin=',':',wrd,i))  
     .  bin = max(0,nint(valread(wrd)))
      if (findarg(arg,narg,'binr=',':',wrd,i)) 
     .  binr= max(0,nint(valread(wrd)))
      if (findarg(arg,narg,'binc=',':',wrd,i)) 
     .  binc= max(0,nint(valread(wrd)))
      efile = ' '
      if (findarg(arg,narg,'ef=',':',wrd,i)) efile = wrd
      nozero = findarg(arg,narg,'-nozero',':',wrd,i)
      sumit  = findarg(arg,narg,'-sum',':',wrd,i)
      if (narg.ne.1) then
        print *,'Syntax:  binim  (FITS file)  bin=n  binc=n  binr=m'
        print *,'                     [ef=filename] [-nozero] [-sum]'
        print *,
     .  '    Bin a FITS file image or spectrum.  This will bin a pixel'
        print *,
     .  '    scale image or spectrum or a linear wavelength scale'
        print *,'    spectrum.  Overwrites original file(s).'
        print *,' '
        print *,'Options: bin=n  : Bin in both columns and rows by n.'
        print *,'         binc=n : Bin in columns by n.'
        print *,'         binr=m : Bin in rows by m.'
        print *,'         ef=filename :  Error FITS file.'
        print *,
     .  '         -nozero     :  Interpolate over zero regions.'
        print *,
     .  '         -sum        :  Sum pixels instead of averaging.'
        call exit(0)
      endif

C Check bin option.
      if ((bin.eq.0).and.(binr.eq.0).and.(binc.eq.0)) then
        print *,'Error: You must specify bin=, binr=, or binc= .'
        call exit(0)
      endif

C Set filenames, read file.
      dfile = arg(1)
      file  = dfile
      call AddFitsExt(file)
      call readfits(file,a,maxpix,header,ok)
      if (.not.ok) goto 801
      nc = max(1,inhead('NAXIS1',header))
      nr = max(1,inhead('NAXIS2',header))
      if (efile.ne.' ') then
        file = efile
        call AddFitsExt(file)
        call readfits(file,e,maxpix,eheader,ok)
        i = max(1,inhead('NAXIS1',eheader))
        j = max(1,inhead('NAXIS2',eheader))
        if ((i.ne.nc).or.(j.ne.nr)) goto 910
      else
        eheader = header
        do i=1,(nc*nr)
          e(i)=1.
        enddo
      endif

C bin= option overrides binc= and binr= .
      if (bin.gt.0) then
        binr=bin
        binc=bin
      endif
      if (binc.lt.1) binc=1
      if (binr.lt.1) binr=1

C New number of columns and rows.
      nnc = nc/binc
      nnr = nr/binr

C Bin the image in the column and/or row direction.  Copy into a2 and e2.
      call BinIm(a,e,nc,nr,a2,e2,nnc,nnr,binc,binr,sumit)

C Fix number of columns.
      call inheadset('NAXIS1',nnc,header)
      call inheadset('NAXIS1',nnc,eheader)

C Fix number of rows.
      call inheadset('NAXIS2',nnr,header)
      call inheadset('NAXIS2',nnr,eheader)

C Interpolate over zero pixels in data array.
      if (nozero) call fits_zero_interp(a2,1,nnc,1,nnr,10)

C Change cards if linear wavelength scale.
      wrd = chead('CTYPE1',header)
      call upper_case(wrd)
      if (wrd.eq.'LAMBDA') then
        crval1 = fhead('CRVAL1',header)
        cdelt1 = fhead('CDELT1',header)
        crval1 = crval1 + ((dfloat(bin-1)*cdelt1)/2.d0)
        cdelt1 = cdelt1 * dfloat(bin)
        call fheadset('CRVAL1',crval1,header)
        call fheadset('CDELT1',cdelt1,header)
        call fheadset('CRVAL1',crval1,eheader)
        call fheadset('CDELT1',cdelt1,eheader)
      elseif ((wrd.ne.'PIXEL').and.(wrd.ne.' ')) then
        print *,'WARNING-- Unknown type:',wrd(1:lc(wrd))
      endif

      file = dfile
      call AddFitsExt(file)
      print '(2a)','  Over-writing file : ',file(1:lc(file))
      call writefits(file,a2,header,ok)
      if (.not.ok) goto 801

      if (efile.ne.' ') then
        file = efile
        call AddFitsExt(file)
        print '(2a)','  Over-writing file : ',file(1:lc(file))
        call writefits(file,e2,header,ok)
        if (.not.ok) goto 801
      endif

      stop
801   print *,'Error writing/reading FITS image: ',file(1:lc(file))
      stop
910   print *,
     .  'Error-- dimensions of error array do not match data array.'
      stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
C Bin an image.  a() e() are old data and error images with nc columns
C and nr rows.  a2() e2() are new data and error images with nnc columns
C and nnr rows.  binc is the column binning.  binr is the row binning.  
C If "sumit" is true then sum pixels instead of averaging.
C
      subroutine BinIm(a,e,nc,nr,a2,e2,nnc,nnr,binc,binr,sumit)
C
      implicit none
      integer*4 nc,nr,nnc,nnr,binc,binr
      real*4 a(nc,nr),e(nc,nr)
      real*4 a2(nnc,nnr),e2(nnc,nnr),sum,wsum,wt
      integer*4 row,col,i,j,row2,col2
      logical sumit
      row2=0
      do row=1,(1+nr-binr),binr
        row2=row2+1
        col2=0
        do col=1,(1+nc-binc),binc
          col2=col2+1
          sum = 0.
          wsum= 0.
          do i=col,col+binc-1
            do j=row,row+binr-1
              if (e(i,j).gt.0.) then
                if (sumit) then
                  sum = sum + a(i,j)
                  wsum= wsum+ e(i,j)*e(i,j)
                else
                  wt  = 1. / (e(i,j)*e(i,j))
                  sum = sum + wt*a(i,j)
                  wsum= wsum+ wt
                endif
              endif
            enddo
          enddo
          if (wsum.gt.0.) then
            if (sumit) then
              a2(col2,row2) = sum
              e2(col2,row2) = sqrt( wsum )
            else
              a2(col2,row2) = sum / wsum
              e2(col2,row2) = sqrt( 1. / wsum )
            endif
          else
            a2(col2,row2)= 0.
            e2(col2,row2)=-1.
          endif
        enddo
      enddo
      return
      end


