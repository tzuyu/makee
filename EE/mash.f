C mash.f
C
C Mash a range of rows or columns with an average, sum, or median.
C
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C
      implicit none
C
      include 'maxpix.inc'
C
      real*4 a(maxpix)
C
      real*4 y(9000)
      integer*4 nc,nr,sc,sr,er,ec,narg,i,n,lc
      integer*4 cols(2),rows(2)
      character header*115200
      logical ok
      character*80 arg(9),infile,outfile,wrd(9),object,chead,c80
      real*4 valread
      logical findarg,dosum,domdn

      call arguments(arg,narg,9)
      rows(1)= -9999
      if (findarg(arg,narg,'rows=',',',wrd,i)) then
        if (i.eq.2) then
          rows(1) = nint(valread(wrd(1)))
          rows(2) = nint(valread(wrd(2)))
        endif
      endif
      cols(1)= -9999
      if (findarg(arg,narg,'cols=',',',wrd,i)) then
        if (i.eq.2) then
          cols(1) = nint(valread(wrd(1)))
          cols(2) = nint(valread(wrd(2)))
        endif
      endif
      dosum = findarg(arg,narg,'-sum',':',wrd,i)
      domdn = findarg(arg,narg,'-mdn',':',wrd,i)
C Print syntax.
      if (narg.ne.2) then
        print *,'Syntax: mash (input FITS image) (output FITS spectrum)'
        print *,'                   [rows=m,n] [cols=m,n] [-sum] [-mdn]'
        print *,'Mash part of image. OPTIONS:'
        print *,
     .  ' rows=m,n : mash an average, sum, or median of rows m to n.'
        print *,
     .  ' cols=m,n : mash an average, sum, or median of columns m to n.'
        print *,' -sum  : plot a sum when using rows= or cols= .'
        print *,' -mdn  : plot a median when using rows= or cols= . '
        print *,'   (default is average)'
        call exit(0)
      endif
      infile = arg(1)
      call AddFitsExt(infile)
      outfile= arg(2)
      call AddFitsExt(outfile)

C Check.
      if ((cols(1).eq.-9999).and.(rows(1).eq.-9999)) then
        print *,'Error-- you must specify either columns or rows.'
        call exit(1)
      endif
      if ((cols(1).ne.-9999).and.(rows(1).ne.-9999)) then
        print *,'Error-- you must specify only columns or rows.'
        call exit(1)
      endif

C Read fits file...
      call readfits(infile,a,maxpix,header,ok)
      if (.not.ok) goto 992
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
      print *,'sc,ec,sr,er,nc,nr=',sc,ec,sr,er,nc,nr
      call fixnans(a,nc*nr,0.)
      call imgmash(a,sc,ec,sr,er,rows,cols,dosum,domdn,y,n)
      call inheadset('NAXIS2',1,header)
      object = chead('OBJECT',header)
C Mode.
      c80='(average)'
      if (dosum) c80='(sum)'
      if (domdn) c80='(median)'
C Changes for a column mash.
      if (rows(1).eq.-9999) then
        if (n.ne.nr) goto 991
        call inheadset('NAXIS1',nr,header)
        call inheadset('CRVAL1',sr,header) 
        write(object,'(a,a,i4,a,i4,1x,a,a)') 
     .           object(1:lc(object)),'[ cols=',
     .           cols(1),':',cols(2),c80(1:lc(c80)),' ]'
      else
        if (n.ne.nc) goto 991
        write(object,'(a,a,i4,a,i4,1x,a,a)') 
     .           object(1:lc(object)),'[ rows=',
     .           rows(1),':',rows(2),c80(1:lc(c80)),' ]'
      endif
      call cheadset('OBJECT',object,header)
      call writefits(outfile,y,header,ok)
      if (.not.ok) goto 992
      stop
991   continue
      print *,'Error- THIS SHOULD NEVER HAPPEN.'
      print *,
     .  'Error- number of mashed points does not match image dimension.'
      print *,'Error- THIS SHOULD NEVER HAPPEN.'
      stop
992   continue
      print *,'Error- reading or writing FITS file.'
      stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
C Mash an image in rows or columns into a 1-dim array y(*).
C
      subroutine imgmash(a,sc,ec,sr,er,rows,cols,dosum,domdn,y,n)
C
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er),y(*)
      logical dosum,domdn
      integer*4 rows(2),cols(2)
      real*4 sum,arr(9000)
      integer*4 i,j,n,num,narr
C Fill array.
      n=0
      if (cols(1).ne.-9999) then
        write(6,'(a,i4,a,i4)') 'COLS=',cols(1),'-',cols(2)
        if ( (cols(1).lt.sc).or.(cols(1).gt.ec).or.
     .       (cols(1).lt.sc).or.(cols(1).gt.ec) ) then
          print *,'Error- cols beyond bounds: ',sc,ec
          return
        endif
        if (domdn) then
          do j=sr,er
            narr=0
            do i=max(sc,min(ec,cols(1))),max(sc,min(ec,cols(2)))
              narr=narr+1
              arr(narr)=a(i,j)
            enddo
            n=n+1
            call find_median(arr,narr,y(n))
          enddo
        else
          do j=sr,er
            sum=0.
            num=0
            do i=max(sc,min(ec,cols(1))),max(sc,min(ec,cols(2)))
              num=num+1
              sum=sum+a(i,j)
            enddo
            n=n+1
            if (dosum) then
              y(n)=sum
            else
              y(n)=sum/float(max(1,num))
            endif
          enddo
        endif
      elseif (rows(1).ne.-9999) then
        write(6,'(a,i4,a,i4)') 'ROWS=',rows(1),'-',rows(2)
        if ( (rows(1).lt.sr).or.(rows(1).gt.er).or.
     .       (rows(1).lt.sr).or.(rows(1).gt.er) ) then
          print *,'Error- rows beyond bounds: ',sr,er
          return
        endif
        if (domdn) then
          do i=sc,ec
            narr=0
            do j=max(sr,min(er,rows(1))),max(sr,min(er,rows(2)))
              narr=narr+1
              arr(narr)=a(i,j)
            enddo
            n=n+1
            call find_median(arr,narr,y(n))
          enddo
        else
          do i=sc,ec
            sum=0.
            num=0
            do j=max(sr,min(er,rows(1))),max(sr,min(er,rows(2)))
              num=num+1
              sum=sum+a(i,j)
            enddo
            n=n+1
            if (dosum) then
              y(n)=sum
            else
              y(n)=sum/float(max(1,num))
            endif
          enddo
        endif
      endif
      return
      end

