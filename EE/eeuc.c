/* eeuc.c                                      tab  Sep00 */

#include "ctab.h"
#include "cmisc.h"
#include "fstuff.h"
#include "cpgplot.h"
#include "cpg-util.h"
#include "cufio.h"


/* EsiWave structure. */
#define EWmax 1000
struct EWstructure {
  int    num;     /* "EW.num[0]" is number of points. */
  double shift;   /* shift (either correlation shift or line shift). */
  double basepix; /* line pixel position in the base arclamp. */
  int    irow;    /* row number (order-1) in which a line appears. */
                  /* or the row number for a correlation shift. */
};
typedef struct EWstructure EWtype;


/* Raw ESI file data. */
struct RAWstructure {
  char file[100];    /* Raw file filename. */
  char object[100];  /* Object name. */
  char obstype[10];  /* Observation type: Object, Line, DmFlat, Bias, ... */
  char slmsknam[10]; /* Slit mask name */
  int CuAr;          /* CuAr lamp on? (1/0) (CU1) */    
  int Xenon;         /* Xenon lamp on? (1/0) (NE1) */    
  int exptime;       /* Exposure time in seconds. */
  int utime;         /* Universal time in seconds since 1970. */
  int obsnum;        /* Observation number. */
  int nc;            /* Number of columns. */
  int nr;            /* Number of rows. */
  int num;           /* RAW[0].num is number of files. */
};
typedef struct RAWstructure RAWtype;





/* ----------------------------------------------------------------------
  Given data from an arc spectrum and a filename of a base or reference arc
      spectrum FITS file, find pixel shifts for each order using 
      correlations.  The shift is the number of pixels any given arc
      emission line will be shifted in the "new" spectrum in pixels 
      relative to the "base" spectrum.  (So if the shift is -2.4 a line 
      "A" which appears at pixel position 438.7  in the "base" spectrum
      will appear at 436.3 in the "new" spectrum.)

  Input: aa        : New arc spectrum data.
         nc        : Number of columns.
         nr        : Number of rows.
         base_file : Base Arc spectrum FITS file.
 In/Out: EW        : EsiWave structure.
                .num   : number of points loaded so far (input/output)
                .shift : shift in correlation function.
                .irow  : row number for each shift.
Scratch: cc[]         : scratch image.
  Input: mp           : maximum pixels in scratch images.
Scratch: header[]     : scratch FITS header.
  Input: headersize   : maximum header size in bytes.

*/
void esiwave_CorrEW( float aa[], int nc,int nr, char base_file[], EWtype EW[], 
                      float cc[], int mp, char header[], int headersize)
{
/**/
int sc,ec,sr,er;
int nc_cc,nr_cc;
float xpeak,peak,fwhm;
int irow,icol,pixno,cos1,cos2,iok;
/**/
float aa1d[5000],cc1d[5000];
/**/

/* Read in files. */
creadfits( base_file, cc, mp, header, headersize );
cGetDimensions(header,headersize,&sc,&ec,&sr,&er,&nc_cc,&nr_cc);

/* Check. */
if ((nc != nc_cc)||(nr != nr_cc)) {
  fprintf(stderr,"ERROR: esiwave_CorrEW: dimensions do not match:%d %d %d %d\n",
                  nc,nr,nc_cc,nr_cc);
  return;
}

/* Column offset shift search range. */
cos1 = -200;
cos2 =  200;

/* Correlation each row. */
for (irow=0; irow<nr; ++irow) {

/* Load 1d spectra. */
  for (icol=0; icol<nc; ++icol) {
    pixno = icol + (irow * nc);
    aa1d[icol] = aa[pixno];       /* Note: these arrays will be changed. */
    cc1d[icol] = cc[pixno];
  }

/* Find best shift. */
  f2cautoidcorrelate_ (&nc,aa1d,cc1d,&cos1,&cos2,&xpeak,&peak,&fwhm,&iok); 

/* Reversal-- to be consistent with definition. */
  xpeak = -1. * xpeak;

/* Check. */
  if (iok != 1) {
    fprintf(stderr,"WARNING: esiwave_CorrEW: Finding correlation. %d\n",irow);
  } else {
    EW[ EW[0].num ].shift= xpeak;       /* Save values. */
    EW[ EW[0].num ].irow = irow;
    EW[0].num = EW[0].num + 1;
  }
}

return;
}


/* ----------------------------------------------------------------------
  Get coeffecients from header for an echelle order.
  Input: eon            : Echelle order number (first, bluest, is "1").
         header[]       : FITS file header.
         headersize     : Maximum header size in bytes.
 Output: coef[]         : Polynomial coeffecients.
         ncoef          : Number of coeffecients.
*/
void esiwave_get_coef( int eon, char header[], int headersize, double coef[], int *ncoef)
{
/**/
char name[10];
char wrd[100];
/**/
/* Read wavelength cards. */
sprintf(name,"WV_0_%2.2d",eon);
if (cgetpos(name,header,headersize) < 0) {
  fprintf(stderr,"ERROR: esiwave_get_coef: No wavelength in reference file.\n");
  *ncoef = 0;
  return;
}
cchead(name,header,headersize,wrd);
coef[0] = GLV(wrd,1);
coef[1] = GLV(wrd,2);
coef[2] = GLV(wrd,3);
coef[3] = GLV(wrd,4);
/* Next card. */
sprintf(name,"WV_4_%2.2d",eon);
cchead(name,header,headersize,wrd);
coef[4] = GLV(wrd,1);
coef[5] = GLV(wrd,2);
coef[6] = GLV(wrd,3);
coef[7] = GLV(wrd,4);
/* Number of coeffecients. */
*ncoef = 8;
if (ABS((coef[7])) < 1.e-40) { *ncoef = 7; }
return;
}


/* ----------------------------------------------------------------------
  Put coeffecients (0 to 7) into a header for an echelle order.
  Input: eon            : Echelle order number (first, bluest, is "1").
 In/Out: header[]       : FITS file header.
  Input: headersize     : Maximum header size in bytes.
         coef[]         : Polynomial coeffecients.
*/
void esiwave_put_coef( int eon, char header[], int headersize, double coef[] )
{
/**/
char name[10];
char wrd[100];
/**/
/* Write wavelength card. */
sprintf(name,"WV_0_%2.2d",eon);
sprintf(wrd,"%16.9e %16.9e %16.9e %16.9e",coef[0],coef[1],coef[2],coef[3]);
ccheadset(name,wrd,header,headersize);
/* Next card. */
sprintf(name,"WV_4_%2.2d",eon);
sprintf(wrd,"%16.9e %16.9e %16.9e %16.9e",coef[4],coef[5],coef[6],coef[7]);
ccheadset(name,wrd,header,headersize);
return;
}


/* ----------------------------------------------------------------------
  Find base arc file name.
  Input: header[]       : New arc FITS file header.
         headersize     : Maximum header size in bytes.
 Output: base_file[]    : Base arc FITS filename (from database).
         base_idfile[]  : Base arc line id list (from database).
*/
void esiwave_findbasefile( char header[], int headersize,
                           char base_file[], char base_idfile[], int xbin )
{
/**/
char wrd[100];
int CuAr,Xe,HgNe;
/**/
/* Which lamps are turned on? */
cchead("LAMPCU1",header,headersize,wrd);
if (strcmp(wrd,"on") == 0) { CuAr=1; } else { CuAr=0; }
cchead("LAMPNE1",header,headersize,wrd);
if (strcmp(wrd,"on") == 0) { Xe=1; } else { Xe=0; }
cchead("LAMPAR1",header,headersize,wrd);
if (strcmp(wrd,"on") == 0) { HgNe=1; } else { HgNe=0; }
/* Makee home directory. */
cGetMakeeHome( base_file );
strcpy(base_idfile,base_file);
/* Choose base file. */
if (CuAr == 1) {
  if (xbin == 1) { strcat(base_file,"ArcESI/Arc-CuAr.fits"); } else {
  if (xbin == 2) { strcat(base_file,"ArcESI/Arc-CuAr_1x2.fits"); } }
  strcat(base_idfile, "ArcESI/Arc-CuAr.ids");
} else {
if (Xe == 1) {
  if (xbin == 1) { strcat(base_file,"ArcESI/Arc-HgNeXe.fits"); } else {
  if (xbin == 2) { strcat(base_file,"ArcESI/Arc-HgNeXe_1x2.fits"); } }
  strcat(base_idfile, "ArcESI/Arc-HgNeXe.ids");
} else {
  fprintf(stderr,"ERROR: esiwave_findbasefile: Finding arc to match. Lamps: CuAr=%d Xe=%d HgNe=%d .\n",CuAr,Xe,HgNe);
  strcpy(base_file,"none");
  return;
}}
return;
}

/* ----------------------------------------------------------------------
  Given the data of an arc spectrum and the filename of base or reference
      arc spectrum FITS file, find pixel shifts for each order using known
      line ids.  Results give the number of pixels any given arc
      emission line will be shifted in the "new" spectrum in pixels 
      relative to the "base" spectrum.  (So if the shift is -2.4 a line 
      "A" which appears at pixel position 438.7  in the "base" spectrum
      will appear at 436.3 in the "new" spectrum.)

  Input: aa           : New arc spectrum data.
         nc           : Number of columns.
         nr           : Number of rows.
         CORRshift    : Correlations shift.
         base_idfile  : Base Arc spectrum line ID file.
  Input: xbin         : column binning factor (xbin=2 for 1x2 ESI raw data).
 In/Out: EW           : EsiWave structure (shift,fwhm,peak)
                  .num   : number of lines loaded so far (input/output)
                  .shift : shift of a line.
                  .irow  : row of a line.
                  .basepix : pixel position of line in base spectrum.
*/
void esiwave_LineEW( float aa[], int nc, int nr, double CORRshift,
                      char base_idfile[], EWtype EW[], int xbin )
{
/**/
char line[100];
double basepix,pix,cent,peak,fwhm,xfac;
int irow;
int sc,ec,sr,er,iok;
/**/
FILE *infu;
/**/
/* Initialize. */
xfac = xbin;
sc=0; sr=0; ec=nc-1; er=nr-1;
/* Open line id file. */
infu = fopen(base_idfile,"r");
/* Read each line. */
while (fgetline(line,infu) == 1) {
  if (GLV(line,6)  > 0.9) {
    irow    = GLV(line,2) - 1;
    basepix = GLV(line,3);
    if (xfac > 1.) {
      basepix = ( basepix / xfac ) - ( (xfac - 1.) / (2. * xfac) );
    }
    pix     = basepix + CORRshift;
    f2carccentroid_ (aa, &sc,&ec,&sr,&er, &irow, &pix,&cent,&peak,&fwhm, &iok);
    EW[ EW[0].num ].shift   = cent - basepix;
    EW[ EW[0].num ].irow    = irow;
    EW[ EW[0].num ].basepix = basepix;
    EW[0].num = EW[0].num + 1;
  }
}
/* Close file. */
fclose(infu);

return;
}


/* ----------------------------------------------------------------------
  Given one or two new arc spectrum files, find the appropriate base files,
    find the pixel shift, and apply the base wavelength to the new
    spectrum files.
  Also, a PostScript plot file is created showing the correlation
    functions and the line shifts fit.  Filename is derived by replacing
    ".fits" with "-fr.ps" in the aafile filename.

  Input: aafile : First new arc spectrum FITS file (input).
         bbfile : Second new arc spectrum FITS file (or "none") (input).

  Returns "1" if successful.
*/
int esiwave_apply( char aafile[], char bbfile[] )
{
/**/
const int headersize = 115200;
char aaheader[115200];
char bbheader[115200];
char ccheader[115200];
/**/
char base_aafile[100];
char base_aaidfile[100];
char base_bbfile[100];
char base_bbidfile[100];
char device[100];
char wrd[100];
/**/
const int mp = 99000;
float aa[99000],bb[99000],cc[99000];
/**/
EWtype EW[ EWmax ];
/**/
double arr8[ EWmax ];
/**/
double CORRshift;
/**/
float xmin,xmax,ymin,ymax,range;
/**/
int npt,iok,ord;
double x8[EWmax],y8[EWmax],w8[EWmax],r8[EWmax];
double base_coef[20],coef[20],xv,high,wrms;
/**/
int xbin,sc,ec,sr,er,ii,jj,nc,nr,eon,base_ncoef,base_ord;
/**/
float xxx,yyy;
double hi,diff;
int iihi,nrej,bbflag;
/**/

/* Create PS filename. */
strcpy(device,aafile);
ii = cindex(device,".fits");
if (ii < 0) { ii = clc(device) + 1; }
device[ii] = '\0';
strcat(device,"-fr.ps");
strcat(device,"/VPS");

/* Read data and header. */
creadfits( aafile, aa, mp, aaheader, headersize );

/* Get dimensions. */
cGetDimensions(aaheader,headersize,&sc,&ec,&sr,&er,&nc,&nr);

/* Check. */
if (nc < 1800) {
  fprintf(stderr,"ERROR: esiwave_apply: Expecting > 1800 columns in arc\n");
  fprintf(stderr,"ERROR: esiwave_apply:   spectrum, but found only %d .\n",nc);
  return(0);
}

/* X binning. */
if (nc > 3000) { xbin = 1; } else { xbin = 2; }

/* Initialize. */
EW[0].num = 0;

/* Find base arc filename. */
esiwave_findbasefile( aaheader, headersize, base_aafile, base_aaidfile, xbin);
if (strcmp(base_aafile,"none") == 0) {
  fprintf(stderr,"ERROR: esiwave_apply: No base file for %s .\n",aafile);
  return(0);
}

/* Second file flag. */
if (strcmp(bbfile,"none") == 0) { bbflag = 0; } else { bbflag = 1; }

/* Find correlation shift, load into arrays. */
esiwave_CorrEW( aa, nc, nr, base_aafile, EW, cc, mp, ccheader, headersize );

/* Second base arc filename. Add to shift value arrays. */
if (bbflag == 1) {
  creadfits( bbfile, bb, mp, bbheader, headersize );
  cGetDimensions(bbheader,headersize,&sc,&ec,&sr,&er,&ii,&jj);
  if ((ii != nc)||(jj != nr)) { 
    fprintf(stderr,
      "ERROR: esiwave_apply: dimensions do not match for %s and %s .\n",
       aafile,bbfile);
    return(0);
  }
  esiwave_findbasefile( bbheader, headersize, base_bbfile, base_bbidfile, xbin);
  if (strcmp(base_bbfile,"none") == 0) {
    fprintf(stderr,"ERROR: esiwave_apply: No base file for %s .\n",bbfile);
    return(0);
  }
  esiwave_CorrEW( bb, nc, nr, base_bbfile, EW, cc, mp, ccheader, headersize );
}

/* Check. */
if (EW[0].num < 3) {
  fprintf(stderr,"ERROR: esiwave_apply: Not enough (corr) shift points.\n");
  return(0);
}

/* Median the shifts. */
for (ii=0; ii<EW[0].num; ++ii) { arr8[ii] = EW[ii].shift; }
CORRshift = cfind_median8( EW[0].num, arr8 );

/* Start PS file. */
cpgbeg(0,device,1,2);
cpgsvp(0.1,0.9,0.1,0.9);

/* Find the plot range. */ 
xmin = EW[0].irow;
xmax = EW[0].irow;
ymin = EW[0].shift;
ymax = EW[0].shift;
for (ii=0; ii<EW[0].num; ++ii) {
  if (EW[ii].irow  < xmin) xmin = EW[ii].irow;
  if (EW[ii].irow  > xmax) xmax = EW[ii].irow;
  if ( ABS((EW[ii].shift - CORRshift)) < (10. / (double)xbin) ) {
    if (EW[ii].shift < ymin) ymin = EW[ii].shift;
    if (EW[ii].shift > ymax) ymax = EW[ii].shift;
  }
}
xmin = xmin + 0.5;  /* X's will be shifted +1 */
xmax = xmax + 1.5;
ymin = ymin - 0.01;
ymax = ymax + 0.01;
range= ymax - ymin;
ymin = ymin - (0.05*range);
ymax = ymax + (0.05*range);

/* Plot the CORR shift points. */
cpgpage();
cpgswin(xmin,xmax,ymin,ymax);
cpgbox("BCNST",0.,0,"BCNST",0.,0);
cpgsch(1.0);
cpgbbuf();
for (ii=0; ii<EW[0].num; ++ii) {
  xxx = EW[ii].irow + 1;
  yyy = EW[ii].shift;
  cpg_pt1(xxx,yyy,4);
}
cpgsls(4);
cpg_HLine( xmin, xmax, (float)CORRshift );
cpgsls(1);
cpgsch(1.5);
sprintf(wrd,"ESI Arc files: %s %s",aafile,bbfile);
cpgmtxt("T",1.0,0.5,0.5,wrd);
cpgmtxt("L",2.5,0.5,0.5,"Data Correlation Pixel Shift");
cpgmtxt("B",2.5,0.5,0.5,"Order Number");
cpgebuf();


/* Find shifts from known emission lines. */
EW[0].num = 0;
esiwave_LineEW( aa, nc, nr, CORRshift, base_aaidfile, EW, xbin );
if (bbflag == 1) {
  esiwave_LineEW( bb, nc, nr, CORRshift, base_bbidfile, EW, xbin );
}
/* Check. */
if (EW[0].num < 10) {
  fprintf(stderr,"***ERROR: esiwave_apply: Too few points: %d \n",EW[0].num);
  return(0);
}
/* Load fitting points. */
for (ii=0; ii<EW[0].num; ++ii) {
  x8[ii] = EW[ii].basepix;
  y8[ii] = EW[ii].shift;
  w8[ii] = 1.;
  r8[ii] = EW[ii].irow;
}

/* ............. Fit and reject ............ */
nrej = 0;
while (1) {

/* Fit a line. */
  ord = 1;
  iok = 0;
  npt = EW[0].num;
  f2cpolyfitglls_ ( &npt, x8, y8, w8, &ord, coef, &iok );
  if (iok != 1) {
    fprintf(stderr,"***ERROR: esiwave_apply: FIT failed.\n");
    return(0);
  }
  
/* Report residuals. */
  f2cpolyfitgllsresiduals_ ( &npt, x8, y8, w8, &ord, coef, &high, &wrms );
  
/* Reject the worse point. */
  hi   = 0.;
  iihi = -1;
  for (ii=0; ii<EW[0].num; ++ii) {
    if (w8[ii] > 0.) {
      diff = ABS(( y8[ii] - cpolyval( ord+1, coef, x8[ii] ) ));
      if (diff > hi) {
        iihi = ii;
        hi   = diff;
      }
    }
  }
/* Check. */
  if ((iihi < 0)||(hi == 0.)) {
    fprintf(stderr,"***ERROR: esiwave_apply: this should not happen.\n");
    return(0);
  }
/* Break? */
  if (hi < 0.4) break;    /* Threshold is +/-0.4 pixels. */
/* Reject. */
  w8[iihi] = 0.;
  ++nrej;
  
/* Break? */
  if (nrej > EW[0].num/2) {
    printf("WARNING: esiwave_apply: Rejected too many pts: nrej=%d  num=%d\n",
                nrej,EW[0].num);
    break;
  }

}

/* Plot the Arc line comparison shift points. */
xmin = +1.e+30;
xmax = -1.e+30;
ymin = +1.e+30;
ymax = -1.e+30;
for (ii=0; ii<EW[0].num; ++ii) {
  if (w8[ii] > 0.) {
    if (x8[ii] < xmin) xmin = x8[ii];
    if (x8[ii] > xmax) xmax = x8[ii];
    if (y8[ii] < ymin) ymin = y8[ii];
    if (y8[ii] > ymax) ymax = y8[ii];
  }
}
xmin = xmin - 10.;
xmax = xmax + 10.;
range= xmax - xmin;
xmin = xmin - (0.05*range);
xmax = xmax + (0.05*range);
ymin = ymin - 0.01;
ymax = ymax + 0.01;
range= ymax - ymin;
ymin = ymin - (0.05*range);
ymax = ymax + (0.05*range);
cpgpage();
cpgswin(xmin,xmax,ymin,ymax);
cpgbox("BCNST",0.,0,"BCNST",0.,0);
cpgsch(1.0);
cpgbbuf();
for (ii=0; ii<EW[0].num; ++ii) {
  xxx = x8[ii];
  yyy = y8[ii];
  if (w8[ii] > 0.) { 
    cpg_pt1(xxx,yyy,4);
  } else {
    cpg_pt1(xxx,yyy,5);
  }
}
cpgsls(4);
for (xv=xmin; xv<xmax; xv=xv+10.) {
  xxx = xv;
  yyy = cpolyval( ord+1, coef, xv );
  cpg_pt1(xxx,yyy,1);
}
cpgsls(1);
cpgsch(1.5);
cpgmtxt("L",2.5,0.5,0.5,"Arcline Comparison Pixel Shift");
cpgmtxt("B",2.5,0.5,0.5,"Column Number");
cpgebuf();

/* Close device. */
cpgend();


/* Read header from first comparison file. */
creadfitsheader( base_aafile, ccheader, headersize );

/* Re-fit polynomials for each row (echelle order). */
for (eon=1; eon<=nr; ++eon) {

/* Read reference scale polynomial. */
  esiwave_get_coef(eon,ccheader,headersize,base_coef,&base_ncoef);
  if (base_ncoef != 7) {
    fprintf(stderr,"ERROR: esiwave_apply: Must have 7 coeffecients.\n");
    return(0);
  }
  base_ord = base_ncoef - 1;
    
/* Load fit points. */
  npt=0;
  for (ii=0; ii<nc; ii=ii+10) {
    xv     = ii;                                  /* base frame pixel */
    y8[npt]= cpolyval(base_ncoef,base_coef,xv);   /* base frame wavelength */
    x8[npt]= xv + cpolyval(ord+1,coef,xv);        /* new frame pixel */
    ++npt;
  }

/* Re-fit polynomial. */
  f2cpolyfitglls_ ( &npt, x8, y8, w8, &base_ord, base_coef, &iok );
  if (iok != 1) {
    fprintf(stderr,"***ERROR: esiwave_apply: 2: FIT failed.\n");
    return(0);
  }
  
/* Report residuals. */
  f2cpolyfitgllsresiduals_ (&npt,x8,y8,w8,&base_ord,base_coef,&high,&wrms);
  if ((ABS((high)) > 0.1)||(wrms > 0.01)) {
    fprintf(stderr,"ERROR: esiwave_apply: Re-fit failed.\n");
    fprintf(stderr,"ERROR: eon=%2d  npt=%3d  high=%12.5e  wrms=%12.5e\n",
                           eon,npt,high,wrms);
    return(0);
  }
  
/* Write coeffecients into both new headers. */
  esiwave_put_coef(eon,aaheader,headersize,base_coef);
  if (bbflag == 1) {
    esiwave_put_coef(eon,bbheader,headersize,base_coef);
  }
}

/* Write new arc files. */
cwritefits( aafile, aa, aaheader, headersize );
if (bbflag == 1) {
  cwritefits( bbfile, bb, bbheader, headersize );
}

return(1);
}





/* ----------------------------------------------------------------------
  Compute profile data for KOA tables.
*/
void koa_profile_data( int nn, float xx[], float yy[], float cent, float objw, float sp1, float sp2, float bk1o, float bk1i, float bk2i, float bk2o, 
                       float *fwhm, float *peak, float *area, float *back1, float *back2, float *skew, float *fracflux )
{
/**/
int ii;
float spcent;
float s1,s2,b1o,b1i,b2i,b2o;
float anum,b1num,b2num,px_per_pt,pnum;
float bg,total,wt,tnum;
float xmean;
/**/
double skew8,area8,xmean8,xx8,yy8,xrms8,value8;
/**/

/* Set limits in real pixel space. */
spcent = (sp1 + sp2) / 2.;
s1  = cent + (sp1 -spcent);
s2  = cent + (sp2 -spcent);
b1o = cent + (bk1o-spcent);
b1i = cent + (bk1i-spcent);
b2i = cent + (bk2i-spcent);
b2o = cent + (bk2o-spcent);

/* Compute background on each side. */
*area = 0.;  *back1 = 0.;  *back2 = 0.;
anum=0.; b1num=0.; b2num=0.; *peak=0.; total=0.; tnum=0.;
px_per_pt = 0.; pnum=0.;  xmean = 0.;
for (ii=0; ii<nn; ++ii) {
  if ((xx[ii] >= s1 )&&(xx[ii] <= s2 )) { 
    *area  = *area  + yy[ii];    
    anum = anum + 1.; 
    if (yy[ii] > *peak) *peak = yy[ii];
    xmean = xmean + (xx[ii] * yy[ii]);
  }
  if ((xx[ii] >= b1o)&&(xx[ii] <= b1i)) { *back1 = *back1 + yy[ii];  b1num= b1num+ 1.; }
  if ((xx[ii] >= b2i)&&(xx[ii] <= b2o)) { *back2 = *back2 + yy[ii];  b2num= b2num+ 1.; }
  total = total + yy[ii];  tnum  = tnum + 1.;
  if (ii > 0) { px_per_pt = px_per_pt + (xx[ii] - xx[ii-1]);  pnum = pnum + 1.; }
}

/* Skew as defined from Numerical Recipes (use double precision). */
skew8 = 0.;
area8 = *area;
xmean8= xmean;
if (area8 > 0.) { 
  xmean8 = xmean8 / area8; 
  xrms8 = 0.;
  for (ii=0; ii<nn; ++ii) {
    if ((xx[ii] >= s1 )&&(xx[ii] <= s2 )) { 
      xx8=xx[ii];  yy8=yy[ii];
      xrms8 = xrms8 + (yy8 * (xx8 - xmean8) * (xx8 - xmean8));
    }
  }
  if (xrms8 > 0.) {
    xrms8 = sqrt(( xrms8 / area8 ));
    skew8 = 0.;
    for (ii=0; ii<nn; ++ii) {
      if ((xx[ii] >= s1 )&&(xx[ii] <= s2 )) { 
        xx8=xx[ii];  yy8=yy[ii];
        value8= (xx8 - xmean8) / xrms8;
        skew8 = skew8 + (yy8 * value8 * value8 * value8);
      }
    }
    skew8 = skew8 / area8;
  }
}
*skew = skew8;

wt = 0.;
if (b1num > 0.) { *back1 = *back1 / b1num;  wt = wt + 1.; }
if (b2num > 0.) { *back2 = *back2 / b2num;  wt = wt + 1.; }
if (wt > 0.) { bg = (*back1 + *back2 ) / wt; } else { bg = 0.; }

*area = *area - (anum * bg);
total = total - (tnum * bg);
*peak = *peak - bg;

if (*peak > 0.) { *fwhm = ( *area / *peak ) * 0.939437; } else { *fwhm = 0.;     }
if (total > 0.) { *fracflux = *area / total;            } else { *fracflux = 0.; }

if (pnum > 0.) { px_per_pt = px_per_pt / pnum; } else { px_per_pt = 1.; }
*area = *area  * px_per_pt;    /* adjust for real pixels. */
*fwhm = *fwhm  * px_per_pt;

return;
}




/* ----------------------------------------------------------------------
  Write KOA table products.
  Input: objhead    : 'ObjectHeader' for final Flux-???.fits output file.
         archead    : Header from arc lamp.
         Flux[]     : Flux spectrum_image.
         Var[]      : Variance spectrum_image.
         Flat[]     : Flat spectrum_image.
         Sky[]      : Sky spectrum_image.
         Sum[]      : Sum spectrum_image.
         Arc[]      : Arc spectrum_image.
         mpp        : maximum polynomial parameters (coeffecients + )
         tbl_eo     : echelle orders polynomial solutions for trace
         tbl_sp     : echelle orders polynomial solutions for primary object
         global[]   : Global variables:  Global_eperdn, MK_mpv, ...
         xpro[]     : X values for spatial profile: 'xpro(MK_mpv,mo)'  (mo=99)
         ypro[]     : Y values for spatial profile: 'ypro(MK_mpv,mo)'
         zpro[]     : Y values for spatial profile (in raw, unsmoothed format).
         npro[]     : number of points in spatial profile: 'npro(mo)'

  Returns "1" if successful.
*/
int koa_tbl_products( char objhead[], char archead[], float Flux[], float Var[], float Flat[], float Sky[], 
                      float Sum[], float Arc[], int mpp, double tbl_eo[], double tbl_sp[],
                      float global[], float xpro[], float ypro[], float zpro[], int npro[] )
{
/**/
int eon,mk_ccd,ip,kk,neo,ncoef[99],trc_ncoef[99];
int ptr,pixno,col,row,ii,jj,nn,sc,ec,sr,er,nc,nr,frameno;
int WaveOK,wv_ncoef,narr8,MK_mpv;
int true_order,ech_num,min_col,max_col;
int ok = 1;
/**/
const int max_ech_num = 200;
int ech_ord[max_ech_num];
double ech_wav[max_ech_num];
/**/
double exptime,echangl,col_clip,row_clip,s2n,wave,err;
double gain,img_col,img_row,raw_col,raw_row,vSky;
double mk_fscal,rr8,arr8[9000];
double trc_xoff[99],trc_coef[99][20];
double xoff[99],coef[99][20];
double wv_coef[99];
double min_wave,max_wave;
double eperdn,aspp_spa,ave_dsp,num_dsp,last_wave;
double trc_lin,trc_fit,trc_cen,trc_wgt;
double ech_coef[20],xv,yv;
/**/
float wfit_rms,wfit_high,wfit_npt,wfit_nrej;
float fwhm,peak,area,back1,back2,skew,fracflux;
float sp1,sp2,bk1o,bk1i,bk2i,bk2o;
float spcent,cent,objw;
float b1o,b1i,b2i,b2o;
float xx[2000],yy[2000],zz[2000];
/**/
const int maxid = 2000;
int nai,ai_row[maxid];
double ai_pix[maxid], ai_wav[maxid], ai_int[maxid], ai_wgt[maxid], ai_res[maxid];
double wave_obj,disp,wave_res,wave_fit;
/**/
char fns[20],rns[20];
char wrd[200];
char tblfile[90];
char hdrfile[90];
char hdrfitsfile[90];
char arcidfile[90];
char card[90];
char line[200];
char name[10];
/**/
FILE *infu;
FILE *tblfu;
FILE *hdrfu;
FILE *ffu;
/**/
const int headersize = 115200; 
char header[headersize];
char temphead[headersize];
/**/

/* Header values. */
mk_fscal= cfhead( "MK_FSCAL", objhead, headersize );
mk_ccd  = cinhead("MK_CCD"  , objhead, headersize );
frameno = cinhead("FRAMENO" , objhead, headersize );
neo     = cinhead("MK_NEO"  , objhead, headersize );
echangl = cfhead( "ECHANGL" , objhead, headersize );
exptime = cfhead( "EXPTIME" , objhead, headersize );
aspp_spa= cfhead( "ASPP_SPA", objhead, headersize );
if (exptime < 0.0001) exptime= cfhead("EXPOSURE", objhead, headersize );
if (exptime < 0.0001) exptime= 1.;
cchead("MK_CLIP",objhead,headersize,wrd);
col_clip = GLV(wrd,2);   /* ending column value on new image */
row_clip = GLV(wrd,3);   /* beginning row value on new image */
cGetDimensions(objhead,headersize,&sc,&ec,&sr,&er,&nc,&nr);
cchead("ARCSPFIL",objhead,headersize,wrd);
strcpy(arcidfile,"");
ii = cindex(wrd,".fits"); if (ii > 0) { wrd[ii]='\0'; strcat(wrd,".ids"); strcpy(arcidfile,wrd); }

/* Check. */
if (neo != nr) {
  fprintf(stderr,"***ERROR: Number of rows does not match number of echelle orders.\n");
  exit(1);
}


/* Copy header (so we can write to it). */
for (ii=0; ii<headersize; ++ii) { header[ii] = objhead[ii]; }

/* Delete some cards. */
cunheadset("NAXIS1"  ,header,headersize);
cunheadset("NAXIS2"  ,header,headersize);
cunheadset("PCOUNT"  ,header,headersize);
cunheadset("GCOUNT"  ,header,headersize);
cunheadset("EXTNAME" ,header,headersize);
cunheadset("CRPIX1" ,header,headersize);
cunheadset("CRPIX2" ,header,headersize);
cunheadset("CRVAL1" ,header,headersize);
cunheadset("CRVAL2" ,header,headersize);
cunheadset("CSYER1" ,header,headersize);
cunheadset("CSYER2" ,header,headersize);
cunheadset("CRDER1" ,header,headersize);
cunheadset("CRDER2" ,header,headersize);
cunheadset("CUNIT1" ,header,headersize);
cunheadset("CUNIT2" ,header,headersize);
cunheadset("CTYPE1" ,header,headersize);
cunheadset("CTYPE2" ,header,headersize);

/* Add or change a couple cards. */
cinheadset2("NAXIS",0,header,headersize,"number of data axes");

/* Special insertation: clheadset2("EXTEND",'T',header,headersize,"FITS dataset may contain extensions"); */
for (ii=0; ii<headersize; ++ii) temphead[ii] = ' ';
ii = cindex(header,"NAXIS   =");
substrcpy(header,0,ii+79,temphead,0);
strcpy(wrd,"EXTEND  =                    T / FITS dataset may contain extensions                             ");
substrcpy(wrd,0,79,temphead,ii+80);
substrcpy(header,ii+80,(headersize - 81),temphead,ii+160);
for (ii=0; ii<headersize; ++ii) header[ii] = temphead[ii];

/* Read arc lamp IDs. */
nai = 0;
if (FileExist(arcidfile) == 1) {
  infu = fopen_read(arcidfile);
  while (fgetline(line,infu) == 1) {
    ai_row[nai] = cvalread( line,  4,  7 );
    ai_pix[nai] = cvalread( line,  8, 17 );
    ai_wav[nai] = cvalread( line, 18, 27 );
    ai_int[nai] = cvalread( line, 28, 37 );
    ai_wgt[nai] = cvalread( line, 38, 42 );
    ai_res[nai] = cvalread( line, 43, 50 );
    ++nai;
  }
  fclose(infu);
}


/* Lookup table for echelle order numbers. */
ech_coef[0]=  3.793393504074e+04;
ech_coef[1]= -1.691947926353e+03;
ech_coef[2]=  4.104033570687e+01;
ech_coef[3]= -5.849819101632e-01;
ech_coef[4]=  4.903132630801e-03;
ech_coef[5]= -2.239403711573e-05;
ech_coef[6]=  4.303363563994e-08;
ech_coef[7]=  0.000000000000e+00;
ech_num = 0;
for (xv=30.; xv<120.; xv=xv+1.) {
  yv = cpolyval( 7, ech_coef, xv );
  ech_ord[ech_num] = xv;
  ech_wav[ech_num] = yv;
  ++ech_num;
  if (ech_num > (max_ech_num - 2)) {
    fprintf(stderr,"***ERROR: Too many entries for ech_ord[] and ech_wav[].\n");
    exit(1);
  }
}

/* Set global variables */
pixno = 0 + ((0) * 100);  eperdn = global[pixno];
pixno = 1 + ((0) * 100);  MK_mpv = global[pixno];


/* Load echelle solution polynomial coeffecients (for primary object). */
for (row=sr; row<=er; ++row) {
  ncoef[row-sr]=0;  xoff[row-sr]=0.; coef[row-sr][0]=0.;
  for (ip=0; ip<=9; ++ip) {
    kk = ip + ((row - sr) * (mpp + 1));
    if (ip == 0) { xoff[row-sr] = tbl_sp[kk]; } else {
    if (ip == 1) { ncoef[row-sr]= tbl_sp[kk]; } else {
      coef[row-sr][(ip - 2)] = tbl_sp[kk];
    }}
  }
}


/* Load echelle solution polynomial coeffecients (for trace). */
for (row=sr; row<=er; ++row) {
  trc_ncoef[row-sr]=0;  trc_xoff[row-sr]=0.; trc_coef[row-sr][0]=0.;
  for (ip=0; ip<=9; ++ip) {
    kk = ip + ((row - sr) * (mpp + 1));
    if (ip == 0) { trc_xoff[row-sr] = tbl_eo[kk]; } else {
    if (ip == 1) { trc_ncoef[row-sr]= tbl_eo[kk]; } else {
      trc_coef[row-sr][(ip - 2)] = tbl_eo[kk];
    }}
  }
}






/* Frame number string. */
if (frameno < 1000) { sprintf(fns,"%3.3d",frameno); } else {sprintf(fns,"%d",frameno); }


/* . . . . . Create a table for each order. . . . . . . */
for (row=sr; row<=er; ++row) {

/* Object position relative to trace and width. */
  sprintf(card,"OBJPOS%2.2d",row);
  cchead(card,header,headersize,wrd);
  cent = GLV(wrd,1);
  objw = GLV(wrd,2);

/* Do wavelength cards exist for this row? */
  sprintf(name,"WV_0_%2.2d",row);
  if (cgetpos(name,header,headersize) < 0) { WaveOK = 0; } else { WaveOK = 1; }

/* Read reference scale polynomial. */
  if (WaveOK == 1) {
    esiwave_get_coef(row,header,headersize,wv_coef,&wv_ncoef);
  }

/* Open output files. */
  if (nr < 100) { sprintf(rns,"%2.2d",row); } else {sprintf(rns,"%3.3d",row); }
  sprintf(hdrfile,"KOA-%s_%d-%s_hdr.txt",fns,mk_ccd,rns);
  sprintf(hdrfitsfile,"KOA-%s_%d-%s_hdr.fits",fns,mk_ccd,rns);
  hdrfu = fopen_write(hdrfile);




/* . . . . FLUX DATA TABLE . . . . . */

  sprintf(tblfile,"KOA-%s_%d-%s_flux.tbl",fns,mk_ccd,rns);
  tblfu = fopen_write(tblfile);

/*                12345678 12345678 12345678 12345678 1234567890 12345678901234 12345678901234 12345678901234 12345678901234 12345678901234 12345678901234 12345678901234 */
  fprintf(tblfu,"| col    | row    |raw_col |raw_row | wave     | Flux         | Error        | Background   | Sig_to_Noise | Flat         | Arc_Lamp     | Sum_Flux     |\n");

/* Each column. */
  min_col  =  99999;
  max_col  = -99999;
  min_wave =  99999.;
  max_wave = -99999.;
  wave     = -1.;
  last_wave= -1.;
  ave_dsp  = 0.;
  num_dsp  = 0.;
  for (col=sc; col<=ec; ++col) {

    pixno   = (col-sc) + ( (row-sr) * nc );
    img_col = (double)col;
    img_row = cpolyval( ncoef[row-sr], coef[row-sr], img_col - xoff[row-sr] );
    raw_row = ( img_col - row_clip ) + 1.0;
    raw_col = ( col_clip - img_row ) - 1.0;
    if (WaveOK == 1) {
      wave    = cpolyval( wv_ncoef, wv_coef, img_col );
      if (last_wave > 0.) {
        ave_dsp = ave_dsp + (wave - last_wave);
        num_dsp = num_dsp + 1.;
      }
      last_wave = wave;
    } else {
      wave = -1.;
    }

/* Error and S/N. */
    if (Var[pixno] > 0.) { err = sqrt(( Var[pixno] )); } else { err = -1.; }
    if (err > 0.)        { s2n = Flux[pixno] / err;    } else { s2n = -1.; }

/* Minimum and maximum non-masked column. */
    if ((err > 0.)&&(col < min_col)) { min_col = col; min_wave = wave; }
    if ((err > 0.)&&(col > max_col)) { max_col = col; max_wave = wave; }

/* Sky in counts per column within object width. */
    vSky = Sky[pixno] * objw;

    fprintf(tblfu," %8.2f %8.2f %8.2f %8.2f %10.4f %14.7e %14.7e %14.7e %14.7e %14.7e %14.7e %14.7e \n",
      img_col, img_row, raw_col, raw_row, wave, Flux[pixno], err, vSky, s2n, Flat[pixno], Arc[pixno], Sum[pixno] );

  }
  if (num_dsp > 0.) { ave_dsp = ave_dsp / num_dsp; } else { ave_dsp = -1.; }
  fclose(tblfu);

/* . . . . */




/* . . . . PROFILE DATA TABLE . . . . . */

/* Load profile. */
  jj = row - sr;
  nn=0;
  for (ii=0; ii<npro[jj]; ++ii) {
    pixno = ii + ( jj * MK_mpv );
    xx[nn] = xpro[pixno];
    yy[nn] = ypro[pixno];
    zz[nn] = zpro[pixno];
    ++nn;
  }
/* Open file. */
  sprintf(tblfile,"KOA-%s_%d-%s_profile.tbl",fns,mk_ccd,rns);
  tblfu = fopen_write(tblfile);
/*               1234567890123456 1234567890123 1234567890123 */
  fprintf(tblfu,"| relative_pixel | median_flux | smooth_flux |\n");
/* Write each profile point. */
  for (ii=0; ii<nn; ++ii) { fprintf(tblfu,"%16.4f %13.4f %13.4f \n",xx[ii],zz[ii],yy[ii]); }
/* Close file. */
  fclose(tblfu);

/* . . . . */




/* . . . . TRACE DATA TABLE . . . . . */

  sprintf(tblfile,"KOA-%s_%d-%s_trace.tbl",fns,mk_ccd,rns);
  tblfu = fopen_write(tblfile);
/*                12345678 12345678 12345678 12345678 1234567890 1234567890 1234567890 123456789012 123456789012 123456 */
  fprintf(tblfu,"| col    | row    |raw_col |raw_row | centroid | fit      | cent_fit |cent_linear | fit_linear |weight|\n");

/* Read trace data file. */
  if (mk_ccd == 0) { sprintf(wrd,"trace-%s.dat",fns);           } 
              else { sprintf(wrd,"trace-%s_%d.dat",fns,mk_ccd); } 
  if (FileExist(wrd) == 0) {
    fprintf(stderr,"***ERROR:ktp: Cannot find trace data file '%s'.\n",wrd);
    exit(1);
  }
  infu = fopen_read(wrd);
  while (fgetline(line,infu) == 1) {  if (line[0] != '|') {
    eon = GLV(line,1); 
    if (eon == row) {
      img_col = GLV(line,3); 
      trc_lin = GLV(line,4); 
      trc_fit = GLV(line,5); 
      trc_cen = GLV(line,6); 
      trc_wgt = GLV(line,7); 
      img_row = trc_fit;         /* here, "image row" is the position of the trace star, not the object. */
      raw_row = ( img_col - row_clip ) + 1.0;
      raw_col = ( col_clip - img_row ) - 1.0;
      fprintf(tblfu," %8.2f %8.2f %8.2f %8.2f %10.4f %10.4f %10.4f %12.4f %12.4f %6.2f \n",
         img_col,img_row,raw_col,raw_row,trc_cen,trc_fit,(trc_cen - trc_fit),(trc_cen - trc_lin),(trc_fit - trc_lin),trc_wgt);
    }
  }}
  fclose(infu);
  fclose(tblfu);

/* . . . . */



/* . . . . WAVELENGTH ID DATA TABLE . . . . . */
  if (WaveOK == 1) {
    sprintf(tblfile,"KOA-%s_%d-%s_arcids.tbl",fns,mk_ccd,rns);
    tblfu = fopen_write(tblfile);
/*                123456789 123456789 123456789 123456789 1234567890 1234567890 1234567890 1234567890 123456789 1234567 12345678 1234567 */
    fprintf(tblfu,"| col_cen | row     | raw_col | raw_row | wave_exp | wave_fit | wave_obj | wave_res | col_res |weight | peak   | disp  |\n");
/*                12345.123 12345.123 12345.123 12345.123 12345.1234 12345.1234 12345.1234 1234.12345 12345.123 1234.12 123456.1 12.1234  */
/* Go through each line id in this row. */
    for (ii=0; ii<nai; ++ii) {  if (ai_row[ii] == row) {
      col     = cnint(( ai_pix[ii] ));
      pixno   = (col-sc) + ( (row-sr) * nc );
      img_col = ai_pix[ii];
      img_row = cpolyval( ncoef[row-sr], coef[row-sr], img_col - xoff[row-sr] );
      raw_row = ( img_col - row_clip ) + 1.0;
      raw_col = ( col_clip - img_row ) - 1.0;
      wave_obj= cpolyval( wv_ncoef, wv_coef, img_col );
      disp    = ( (cpolyval( wv_ncoef, wv_coef, (img_col + 1.0) )) - wave_obj );
      wave_res= ai_res[ii] * disp;
      wave_fit= ai_wav[ii] + wave_res;
      fprintf(tblfu," %9.3f %9.3f %9.3f %9.3f %10.4f %10.4f %10.4f %10.5f %9.3f %7.2f %8.1f %7.4f \n",
      img_col,img_row,raw_col,raw_row, ai_wav[ii], wave_fit, wave_obj, wave_res, ai_res[ii], ai_wgt[ii], ai_int[ii], disp );
    }}
    fclose(tblfu);
  }

/* . . . . */



/* . . . . ADD HEADER CARDS . . . . . . . */


/* Transfer global variables. */
  pixno = 3 + ((row - sr) * 100);  cfheadset2( "REJ_FRAC",global[pixno],header,headersize,"rejection fraction (actual/expected)",1);
  pixno = 4 + ((row - sr) * 100);  cfheadset2( "REJ_ADJ" ,global[pixno],header,headersize,"adjustment to rejection threshold",1);
  pixno = 5 + ((row - sr) * 100);  cinheadset2("REJ_SKY" ,global[pixno],header,headersize,"masked pixels in sky region");
  pixno = 6 + ((row - sr) * 100);  cinheadset2("REJ_OBJ" ,global[pixno],header,headersize,"masked pixels in object region");
  pixno = 7 + ((row - sr) * 100);  sp1 = global[pixno];
  pixno = 8 + ((row - sr) * 100);  sp2 = global[pixno]; 
  pixno = 9 + ((row - sr) * 100);  bk1o= global[pixno];  
  pixno =10 + ((row - sr) * 100);  bk1i= global[pixno];  
  pixno =11 + ((row - sr) * 100);  bk2i= global[pixno];  
  pixno =12 + ((row - sr) * 100);  bk2o= global[pixno];  

/* Compute data about current profile for header cards. */
  koa_profile_data( nn, xx, yy, cent, objw, sp1, sp2, bk1o, bk1i, bk2i, bk2o, &fwhm, &peak, &area, &back1, &back2, &skew, &fracflux );

/* Find 'true' echelle order number (from simulator). */
  if ((echangl > -0.8)&&(echangl < 0.8)) { wave = (min_wave + max_wave) / 2.; } else {
  if (echangl < -0.8) {                    wave = max_wave;                   } else {
  if (echangl >  0.8) {                    wave = min_wave;                   }      }}
  jj = cneari( wave, ech_num, ech_wav );
  true_order = ech_ord[jj];

/* Row (echelle order) specific information. */
  cinheadset2("INDX_ORD" ,row          ,header,headersize,"index of echelle order (internal)");
  cinheadset2("ECHL_ORD" ,true_order   ,header,headersize,"true echelle order number");
  cfheadset2( "TRAC_OFF" ,cent         ,header,headersize,"centroid relative to trace (pixels)",1);
  pixno = 2 + ((row - sr) * 100);  
  cfheadset2( "TRAC_RMS" ,global[pixno],header,headersize,"trace RMS (pixels)",1);

/* Trace polynomial solutions. */
  jj = row - sr;
  sprintf(wrd," %16.9e %16.9e %16.9e %16.9e",   trc_xoff[jj],(float)trc_ncoef[jj],trc_coef[jj][0],trc_coef[jj][1]); ccheadset("TRC_FIT1",wrd,header,headersize );
  sprintf(wrd," %16.9e %16.9e %16.9e %16.9e",trc_coef[jj][2],     trc_coef[jj][3],trc_coef[jj][4],trc_coef[jj][5]); ccheadset("TRC_FIT2",wrd,header,headersize );
  sprintf(wrd," %16.9e %16.9e %16.9e %16.9e",       xoff[jj],    (float)ncoef[jj],    coef[jj][0],    coef[jj][1]); ccheadset("OBJ_FIT1",wrd,header,headersize );
  sprintf(wrd," %16.9e %16.9e %16.9e %16.9e",    coef[jj][2],         coef[jj][3],    coef[jj][4],    coef[jj][5]); ccheadset("OBJ_FIT2",wrd,header,headersize );

  spcent = (sp1 + sp2) / 2.;
  b1o = cent + (bk1o-spcent);
  b1i = cent + (bk1i-spcent);
  b2i = cent + (bk2i-spcent);
  b2o = cent + (bk2o-spcent);
  cfheadset2( "BK1O_OFF" ,b1o ,header,headersize,"backgrnd limit 1 (outside) rel. to trace (px)",1);
  cfheadset2( "BK1I_OFF" ,b1i ,header,headersize,"backgrnd limit 1 (inside) rel. to trace (px)",1);
  cfheadset2( "BK2I_OFF" ,b2i ,header,headersize,"backgrnd limit 2 (inside) rel. to trace (px)",1);
  cfheadset2( "BK2O_OFF" ,b2o ,header,headersize,"backgrnd limit 2 (outside) rel. to trace (px)",1);

  cfheadset2( "HOBJW_PX" ,objw               ,header,headersize,"half object width (pixels)",1);
  cfheadset2( "OBJW_PX"  ,(2.*objw)          ,header,headersize,"object width (pixels)",1);
  cfheadset2( "OBJW_AS"  ,(2.*objw*aspp_spa) ,header,headersize,"object width (arcseconds)",1);

  sprintf(card,"WV_0_%2.2d",row); cchead(card,header,headersize,wrd);
  ccheadset( "WAVEFIT1" ,wrd   ,header,headersize);
  sprintf(card,"WV_4_%2.2d",row); cchead(card,header,headersize,wrd);
  ccheadset( "WAVEFIT2" ,wrd   ,header,headersize);

  cfheadset2( "PFWHM_PX" , fwhm            ,header,headersize,"profile FWHM in pixels",1);
  cfheadset2( "PFWHM_AS" ,(fwhm * aspp_spa),header,headersize,"profile FWHM in arcsec",1);
  cfheadset2( "PSKEW"    , skew            ,header,headersize,"profile Skew",1);
  cfheadset2( "BCKGND_1" , back1           ,header,headersize,"background 1 in counts per pixel",1);
  cfheadset2( "BCKGND_2" , back2           ,header,headersize,"background 2 in counts per pixel",1);
  cfheadset2( "FRACFLUX" , fracflux        ,header,headersize,"fractional flux extracted from profile",1);

/* Find median S/N. */
  narr8 = 0;
  for (col=sc; col<=ec; ++col) {
    pixno = (col-sc) + ( (row-sr) * nc );
    if (Var[pixno] > 0.) {
      arr8[narr8] = Flux[pixno] / sqrt((Var[pixno]));
      ++narr8;
    }
  }
  rr8 = cfind_median8( narr8, arr8 );
  cfheadset2( "MDN_S2N"  ,rr8,header,headersize,"median signal-to-noise per column",1);

  cfheadset2( "MIN_WAVE",min_wave,header,headersize,"minimum wavelength (usable data)",1);
  cfheadset2( "MAX_WAVE",max_wave,header,headersize,"maximum wavelength (usable data)",1);
  cinheadset2("MIN_COL" ,min_col ,header,headersize,"minimum column (usable data)");
  cinheadset2("MAX_COL" ,max_col ,header,headersize,"maximum column (usable data)");
  cfheadset2( "AVE_DSP" ,ave_dsp ,header,headersize,"average dispersion (Angstroms/pixel)",1);

  img_col = ( (double)min_col + (double)max_col ) / 2.;
  cfheadset2( "CEN_COL" ,img_col ,header,headersize,"central column for this order",1);
  img_row = cpolyval( ncoef[row-sr], coef[row-sr], img_col - xoff[row-sr] );
  cfheadset2( "CEN_ROW" ,img_row ,header,headersize,"image row at central column for this order",1);

  img_col = ( (double)sc + (double)ec ) / 2.;
  cfheadset2( "MID_COL" ,img_col ,header,headersize,"image middle column for this order",1);
  img_row = cpolyval( ncoef[row-sr], coef[row-sr], img_col - xoff[row-sr] );
  cfheadset2( "MID_ROW" ,img_row ,header,headersize,"image row at middle column for this order",1);

/* Get arclamp header info. */
  sprintf(card,"WVFIT_%2.2d",row);
  cchead(card,archead,headersize,wrd);
  wfit_rms = GLVQ(wrd,2);  if (wfit_rms  < -99999.) wfit_rms = -99.;
  wfit_high= GLVQ(wrd,3);  if (wfit_high < -99999.) wfit_high= -99.;
  wfit_npt = GLVQ(wrd,4);  if (wfit_npt  < -99999.) wfit_npt = -99.;
  wfit_nrej= GLVQ(wrd,5);  if (wfit_nrej < -99999.) wfit_nrej= -99.;
  cfheadset2( "WFIT_RMS",wfit_rms        ,header,headersize,"wavelength fit RMS",1);
  cfheadset2( "WFIT_HI" ,wfit_high       ,header,headersize,"wavelength fit high residual",1);
  cinheadset2("WFIT_NUM",cnint(wfit_npt) ,header,headersize,"number of arclines used in wavelength fit");
  cinheadset2("WFIT_NRJ",cnint(wfit_nrej),header,headersize,"number of arclines rejected in wavelength fit");
  sprintf(card,"WV_0_%2.2d",row); cchead(card,archead,headersize,wrd); ccheadset("ARC_FIT1",wrd,header,headersize );
  sprintf(card,"WV_4_%2.2d",row); cchead(card,archead,headersize,wrd); ccheadset("ARC_FIT2",wrd,header,headersize );

  gain = -999.;
  if (global[0] > 0.) gain = 1. / global[0];
  cfheadset2( "MK_GAIN"  ,gain ,header,headersize,"MAKEE gain used (inverse of e-/DN)",1);


/* Write out FITS header string. */
  ffu = fopen(hdrfitsfile,"w");
  if (crw_fitshead( header, 1, ffu, headersize ) == 0) {
    fprintf(stderr,"***ERROR: Writing FITS KOA header.\n");
    fclose(ffu);
    exit(1);
  }
  fclose(ffu);

/* Write ASCII header. */
  strcpy(line,"");
  ptr = 0;
  while((ptr < (headersize-160))&&(strcmp(line,"END") != 0)) {
    substrcpy_terminate(header,ptr,ptr+79,line,0);
    ii = clc(line); line[ii+1]='\0';
    fprintf(hdrfu,"%s\n",line);
    ptr = ptr + 80;
  }
/* Check. */
  if (ptr > (headersize-161)) { fprintf(stderr,"===WARNING:KTP: No END card found.\n"); }

  fclose(hdrfu);


}  /* ... do next echelle order ... */



return(ok);
}





/* ----------------------------------------------------------------------
  FORTRAN gateway to esiwave_apply.
*/
int esiwaveapply_ ( char aafile[], char bbfile[] )
{
int ii;
ii = esiwave_apply( aafile, bbfile );
return(ii);
}


/* ----------------------------------------------------------------------
  FORTRAN gateway to koa_tbl_products.
*/
int koatblproducts_ ( char objhead[], char archead[], float Flux[], float Var[], float Flat[], float Sky[], 
                      float Sum[], float Arc[], int *mpp, double tbl_eo[], double tbl_sp[],
                      float global[], float xpro[], float ypro[], float zpro[], int npro[] )
{
int ii,impp;
impp = *mpp;
ii = koa_tbl_products( objhead, archead, Flux, Var, Flat, Sky, Sum, Arc, impp, tbl_eo, tbl_sp, global, xpro, ypro, zpro, npro );
return(ii);
}


/* ----------------------------------------------------------------------
  Convert a date and a time into seconds since 1970.
  Input: userdate  : User Date in format: YYYY-MM-DD, YYYY/MM/DD, or YYYY MM DD
         usertime  : User Time in format: HH:MM:SS.SS, HH MM SS, or HH MM SS.S
*/
int esipipe_seconds_since_1970( char userdate[], char usertime[] )
{
/**/
char thedate[100];
char thetime[100];
int ii,tt,doy,yy,mm,dd,hh,nn,ss;
/**/
const int nd[12]  ={ 0,31,59,90,120,151,181,212,243,273,304,334 };
const int ndly[12]={ 0,31,60,91,121,152,182,213,244,274,305,335 }; /* leap */
/**/
/* Copy */
strcpy( thedate, userdate );
strcpy( thetime, usertime );
/* Eliminate "-" characters. */
for (ii=0; ii<strlen(thedate); ++ii) { 
  if (thedate[ii] == '-') { thedate[ii] = ' '; }
}
/* Get year,month,day,hour,minute,second. Truncates the seconds. */
yy = GLV(thedate,1); mm = GLV(thedate,2); dd = GLV(thedate,3);
hh = GLV(thetime,1); nn = GLV(thetime,2); ss = GLV(thetime,3);
/* Count up years from 1970 to yy-1 */
tt = 0;
for (ii=1970; ii<(yy-1); ++ii) {
  if ((ii%4) == 0) {
    tt = tt + (366 * 24 * 3600);  /* leap year */
  } else {
    tt = tt + (365 * 24 * 3600);  /* non-leap year */
  }
}
/* Day of year before beginning of month. */
if ((yy%4) == 0) {
  doy = ndly[ mm - 1 ];    /* leap year */
} else {
  doy = nd[ mm - 1 ];      /* non-leap year */
}
/* Seconds since 1970. */
tt = tt + ss + (nn * 60) + (hh * 3600) + ((doy + dd - 1) * 24 * 3600);
return(tt);
}


/* ----------------------------------------------------------------------
  esipipe: Creating an ESI makee run script.
  Looks at all the files in a list.  Groups and adds the Dome Flats.
  Groups the arc lamps.  Matches all the objects with stars, flats, and
  arclamps to create "makee.script".
*/
int esipipe( char file1[], char file2[], char rawdir[], char extopt[],
             int flatresum )
{
/**/
const int maxfsf = 40;
char fsf[40][100];
int ii_fsf[40];
/**/
const int maxalp = 40;
int alp1[40];
int alp2[40];
/**/
char ut[100];
char date_obs[100];
char line[100];
char wrd[100];
char obstype0[100];
char slmsknam0[100];
/**/
int nc0,nr0,utime0;
int ff,kk,ii,jj,pp,flatlist[99];
int sc,ec,sr,er,nc,nr,nfsf,nalp;
int fflo,jjlo,diff,lo;
int iiTrace,iiFlat,iiArc; 
/**/ 
const int headersize = 115200; 
char header[115200];
char header2[115200];
/**/
const int maxaa = 9184000;   /* 2240 x 4100 */
float *aa;
float *bb;
/**/
const int maxobs = 400;
RAWtype RAW[400];
/**/   
FILE *infu;
FILE *outfu;
/**/   

/* Allocate memory. */
aa = (float *)calloc(maxaa,sizeof(float));
bb = (float *)calloc(maxaa,sizeof(float));

/* Read in raw file list and load in structure by reading headers. */
printf("NOTE: esipipe: Reading file list from %s .\n",file1);
infu = fopen(file1,"r");
if (infu == NULL) {
  fprintf(stderr,"ERROR: esipipe: Cannot open %s .\n",file1);
  exit(0);
}
ii = 0;
/* Each file. */
while( fgetline(line,infu) == 1 ) {
/* Raw FITS filename. */
  strcpy(RAW[ii].file,line);
  cAddFitsExt( RAW[ii].file );
  strcpy(wrd,rawdir);
  strcat(wrd,"/");
  strcat(wrd,RAW[ii].file);
/* Read in FITS header. */
  if ( creadfitsheader( wrd, header, headersize ) != 1 ) {
    fprintf(stderr,"ERROR: esipipe: Cannot read ESI file %s .\n",wrd);
    exit(0);
  }
/* Dimensions. */
  cGetDimensions(header,headersize,&sc,&ec,&sr,&er,&nc,&nr);
  RAW[ii].nc = nc;
  RAW[ii].nr = nr;
/* Values. */
  cchead( "OBJECT" , header, headersize, RAW[ii].object );
  cchead( "OBSTYPE", header, headersize, RAW[ii].obstype );
  cchead( "SLMSKNAM",header, headersize, RAW[ii].slmsknam );
  RAW[ii].exptime= cinhead("EXPOSURE", header, headersize );
  RAW[ii].obsnum = cinhead("OBSNUM"  , header, headersize );
  RAW[ii].CuAr = 0;
  RAW[ii].Xenon= 0;
  cchead("LAMPCU1",header,headersize,wrd);
  if (cindex(wrd,"on") > -1) { RAW[ii].CuAr = 1; }
  cchead("LAMPNE1",header,headersize,wrd);
  if (cindex(wrd,"on") > -1) { RAW[ii].Xenon = 1; }
/* Find seconds since 1970. */
  cchead( "UT", header, headersize, ut );
  cchead( "DATE-OBS", header, headersize, date_obs );
  RAW[ii].utime = esipipe_seconds_since_1970( date_obs, ut );
/* Increment and check. */
  ++ii;
  if (ii >= maxobs) {
    fprintf(stderr,"ERROR: esipipe: Too many files.  Limit is %d .\n",maxobs);
    exit(0);
  }
/* Delete if not echellete. */
  cchead("SYNOPFMT",header,headersize,wrd);
  if (cindex(wrd,"Echellette") < 0) { ii = ii - 1; }
}
RAW[0].num = ii;
/* Close file. */
fclose(infu);

/* ................Read in observation numbers of stars (given by user). */
printf("NOTE: esipipe: Reading star number list from %s .\n",file2);
infu = fopen(file2,"r");
if (infu == NULL) {
  fprintf(stderr,"ERROR: esipipe: Cannot open star number list %s .\n",file2);
  exit(0);
}
while( fgetline(line,infu) == 1 ) {
  kk = GLV(line,1);
/* Go through raw file list. */
  for (ii=0; ii<RAW[0].num; ++ii) {
    if (RAW[ii].obsnum == kk) strcpy(RAW[ii].obstype,"Trace");
  }
}
fclose(infu);

/* ...........................Find dome or int flat groups and sum them. */
ii=0;
nfsf=0;
while (ii < RAW[0].num) {
  if ((strcmp(RAW[ii].obstype,"DmFlat") == 0)||
      (strcmp(RAW[ii].obstype,"IntFlat") == 0)) {
    strcpy(obstype0,RAW[ii].obstype);
    strcpy(slmsknam0,RAW[ii].slmsknam);
    nc0    = RAW[ii].nc;
    nr0    = RAW[ii].nr;
    utime0 = RAW[ii].utime;
    ff=0;
    flatlist[ff] = ii;
    ++ff;
    ++ii;
    while ((ii < RAW[0].num)&&
         (RAW[ii].nc == nc0)&&(RAW[ii].nr == nr0)&&  /* Matching dimensions.*/
         (ABS((RAW[ii].utime - utime0)) < 3600)&&    /* <1 hr sep. */
         (strcmp(RAW[ii].obstype,obstype0) == 0)&&   /* same observation type.*/
         (strcmp(RAW[ii].slmsknam,slmsknam0) == 0) ) {   /* same slit mask */
      flatlist[ff] = ii;
      ++ff;
      ++ii;
    }
/* Echo. */
    printf("\n   Sum flats:\n");
    for (jj=0; jj<ff; ++jj) {
      kk = flatlist[jj];
      printf("%4d : %s :%5ds : %s\n",
        RAW[kk].obsnum,RAW[kk].obstype,RAW[kk].exptime,RAW[kk].object);
    }
/* Summed flat filename. */
    sprintf(fsf[nfsf],"Flatsum-%4.4d.fits",RAW[flatlist[0]].obsnum);
/* Record element array number of first flat in sum. */
    ii_fsf[nfsf] = flatlist[0];
/* Process flats and add this group to the flat reference list. */
/* Write the flat sum unless it already exists (or rewrite requested). */
    strcpy(wrd,rawdir);
    strcat(wrd,"/");
    strcat(wrd,fsf[nfsf]);
    if ((flatresum == 1)||(FileExist(wrd) == 0)) {
/* Read the first file. */
      kk = flatlist[0];
      strcpy(wrd,rawdir);
      strcat(wrd,"/");
      strcat(wrd,RAW[kk].file);
      creadfits(wrd,aa,maxaa,header,headersize);
/* Read the remaining files and add into first file. */
      for (jj=1; jj<ff; ++jj) {
        kk = flatlist[jj];
        strcpy(wrd,rawdir);
        strcat(wrd,"/");
        strcat(wrd,RAW[kk].file);
        creadfits(wrd,bb,maxaa,header2,headersize);
        for (pp=0; pp<(RAW[kk].nc * RAW[kk].nr); ++pp) {
          aa[pp] = aa[pp] + bb[pp];
        }
      }
/* Add header card, write out flat sum and load fsf list. */
      sprintf(wrd,"Flat Sum: %d",RAW[flatlist[0]].obsnum);
      for (jj=1; jj<ff; ++jj) { 
        sprintf(wrd,"%s+%d",wrd,RAW[flatlist[jj]].obsnum);
      }
      ccheadset("OBJECT",wrd,header,headersize);
      strcpy(wrd,rawdir);
      strcat(wrd,"/");
      strcat(wrd,fsf[nfsf]);
      printf("Writing flat sum file: %s .\n",wrd);
      cwritefits(wrd,aa,header,headersize);      
    } else {
      printf("Using pre-existing flat sum file: %s .\n",fsf[nfsf]);
    }
/* Increment and check. */
    ++nfsf;
    if (nfsf >= maxfsf) {
      fprintf(stderr,"ERROR: esipipe: Too many flat sum groups. (maxfsf=%d)\n",maxfsf);
      exit(0);
    }
    --ii;
  }
  ++ii;
}

/* ........................... Find arc lamp pairings. */
ii=0;
nalp=0;
while(ii < RAW[0].num) {
  if (strcmp(RAW[ii].obstype,"Line") == 0) {
/* Look for valid pairings. */
    jj=ii+1;
/* Xenon and then CuAr. */
    if ((RAW[ii].Xenon== 1)&&(RAW[ii].CuAr == 0)&&(RAW[ii].exptime > 2)&&
        (RAW[jj].CuAr == 1)&&(RAW[jj].Xenon== 0)&&(RAW[jj].exptime > 299)&&
        (ABS((RAW[ii].utime - RAW[jj].utime)) < 3600)) {
      alp1[nalp] = ii;
      alp2[nalp] = jj;
      ++nalp;
      ++ii;
    } else {
/* CuAr and then Xenon. */
    if ((RAW[ii].CuAr == 1)&&(RAW[ii].Xenon== 0)&&(RAW[ii].exptime > 299)&&
        (RAW[jj].Xenon== 1)&&(RAW[jj].CuAr == 0)&&(RAW[jj].exptime > 2)&&
        (ABS((RAW[ii].utime - RAW[jj].utime)) < 3600)) {
      alp1[nalp] = ii;
      alp2[nalp] = jj;
      ++nalp;
      ++ii;
    } else {
/* Single CuAr. */
    if ((RAW[ii].CuAr == 1)&&(RAW[ii].Xenon== 0)&&(RAW[ii].exptime > 299)) {
      alp1[nalp] = ii;
      alp2[nalp] = -1;
      ++nalp;
    } else {
/* Single Xenon. */
    if ((RAW[ii].Xenon== 1)&&(RAW[ii].CuAr == 0)&&(RAW[ii].exptime > 2)) {
      alp1[nalp] = ii;
      alp2[nalp] = -1;
      ++nalp;
    }}}}
  }
  ++ii;
  if (nalp >= maxalp) {
    fprintf(stderr,"ERROR: esipipe: Too many arc line pairs. (maxalp=%d)\n",maxalp);
    exit(0);
  }
}
if (nalp == 0) {
  fprintf(stderr,"ERROR: esipipe: Found no arc line pairs.\n");
  exit(0);
}

/* Echo. */
printf("\n   Arclamp pairs or singles: \n");
for (ii=0; ii<nalp; ++ii) {
  if (alp2[ii] > -1) {
    printf("%5d: %4d %4d\n",ii,RAW[alp1[ii]].obsnum,RAW[alp2[ii]].obsnum);
  } else {
    printf("%5d: %4d\n",ii,RAW[alp1[ii]].obsnum);
  }
}

/* Match objects with star, flat, and arc lamps.  Write to "makee.script". */
outfu = fopen("makee.script","w");
printf("\nNOTE: Writing 'makee.script'.\n\n");
fprintf(outfu,"# \n");
for (ii=0; ii<RAW[0].num; ++ii) {
if ((strcmp(RAW[ii].obstype,"Object")==0)||
    (strcmp(RAW[ii].obstype,"Trace") ==0)) {

/* Find best trace exposure for this object. */
  lo = 1073741824;
  jjlo = -1;
  for (jj=0; jj<RAW[0].num; ++jj) {
  if (strcmp(RAW[jj].obstype,"Trace") == 0) {
    diff = ABS((RAW[ii].utime - RAW[jj].utime));
    if (diff < lo) {
      lo = diff;
      jjlo = jj;
    }
  }}
  if (jjlo < 0) {
    fprintf(stderr,"ERROR: esipipe: No Trace Star exposures found.\n");
    exit(0);
  }
  iiTrace = jjlo;


/* Find best flat sum for this object. */
  lo = 1073741824;
  fflo = -1;
  for (ff=0; ff<nfsf; ++ff) {
    jj = ii_fsf[ff];
    if (strcmp(RAW[ii].slmsknam,RAW[jj].slmsknam) == 0) {
      diff = ABS((RAW[ii].utime - RAW[jj].utime));
      if (diff < lo) {
        lo = diff;
        fflo = ff;
      }
    }
  }
/* If no best flat found, relax slit mask constraint. */
  if (fflo < 0) {
    for (ff=0; ff<nfsf; ++ff) {
      jj = ii_fsf[ff];
      diff = ABS((RAW[ii].utime - RAW[jj].utime));
      if (diff < lo) {
        lo = diff;
        fflo = ff;
      }
    }
    printf("NOTE: Using flat with different slit for obs#%d\n",RAW[ii].obsnum);
  }
/* Error if no flat sum found, abort. */
  if (fflo < 0) {
    fprintf(stderr,"ERROR: esipipe: No flat sums found.\n");
    exit(0);
  }
  iiFlat = fflo;
    

/* Find best arc lamp pair for this object. */
  lo = 1073741824;
  fflo = -1;
  for (ff=0; ff<nalp; ++ff) {
    jj = alp1[ff];
    diff = ABS((RAW[ii].utime - RAW[jj].utime));
    if (diff < lo) {
      lo = diff;
      fflo = ff;
    }
  }
  if (fflo < 0) {
    fprintf(stderr,"ERROR: esipipe: No arc lamp pairs found.\n");
    exit(0);
  }
  iiArc = fflo;
    

/* Print execution command. */
  strcpy(wrd,RAW[ii].file);
  jj = cindex(wrd,".fits");
  if (jj > 0) { wrd[jj]='\0'; }
  strcat(wrd,".log");
  if (alp2[iiArc] > -1) {
    fprintf(outfu,"makee %s %s %s %s %s raw=%s log=%s %s\n",
                  RAW[ii].file,RAW[iiTrace].file,fsf[iiFlat],
                  RAW[alp1[iiArc]].file, RAW[alp2[iiArc]].file,
                  rawdir, wrd, extopt );
  } else {
    fprintf(outfu,"makee %s %s %s %s raw=%s log=%s %s\n",
                  RAW[ii].file,RAW[iiTrace].file,fsf[iiFlat],
                  RAW[alp1[iiArc]].file,
                  rawdir, wrd, extopt );
  }

}}
fclose(outfu);

free(aa); aa = NULL;
free(bb); bb = NULL;

return(0);
}


/* ----------------------------------------------------------------------
  FORTRAN gateway to esipipe() .
*/
int ffesipipe_ ( char file1[], char file2[], char rawdir[], char extopt[], 
                 int *flatresum )
{
int ii,jj;
jj = *flatresum;
ii = esipipe( file1, file2, rawdir, extopt, jj );
return(ii);
}



/* ----------------------------------------------------------------------
  Rebin a calibrated arc lamp 2D spectrum.
  Input:  aafile   : Input 2D arclamp FITS spectrum file.
          bbfile   : Output 2D arclamp FITS spectrum file.
          binning  : Re-binning value, e.g. 2,3,4,5,...
  Returns "1" if all ok.
*/
int esi_rebinarc(char aafile[], char bbfile[], int binning)
{
/**/
char wrd[100];
char line[100];
int npt,pix,iok,order,ncoef;
int ii,sc,ec,sr,er,nc,nr,col,row,pixno,newnc;
/**/   
double coef[99],xv,x8[9000],y8[9000],w8[9000],high,wrms;
/**/   
const int headersize = 115200;
char header[headersize];
/**/   
const int maxpix = 240000;
float aa[maxpix],sum;
/**/   

/* Read first file. */
printf("NOTE: Reading %s .\n",bbfile);
iok = creadfits(aafile,aa,maxpix,header,headersize);
if (iok != 1) {
  fprintf(stderr,"***ERROR: esi_rebinarc:  Cannot read %s .\n",aafile);
  return(0);
}

/* Dimensions. */
cGetDimensions(header,headersize,&sc,&ec,&sr,&er,&nc,&nr);

/* Rebin data. */
newnc = nc / binning;
printf("NOTE: Rebinning by %d. Old #cols=%d. New #cols=%d.\n",binning,nc,newnc);
for (row=0; row<nr; ++row) {
  for (col=0; col<nc; col=col+binning) {
    pixno = col + (row * nc);
    sum=0; for (ii=0; ii<binning; ++ii) { sum = sum + aa[(pixno + ii)]; }
    aa[((col / binning) + (row * newnc))] = sum;
  }
}

/* Reset dimensions. */
cinheadset("NAXIS1",newnc,header,headersize);
timegetstring( line );
sprintf(wrd,"Arc spectrum rebinned by %d on %s .",binning,line);
ccheadset("REBINARC",wrd,header,headersize);

/* Re-compute wavelength calibration. */
for (row=1; row<=nr; ++row) {

/* Read reference scale polynomial. */
  esiwave_get_coef(row,header,headersize,coef,&ncoef);

/* Re-assign points. */
  npt=0;
  for (pix=sc; pix<ec; pix=pix+10) {
    xv     = ( (double)pix ) + ( ((double)binning - 1.0) / 2.0 );
    y8[npt]= cpolyval( ncoef, coef, xv );
    x8[npt]= ( (double)pix ) / ( (double)binning );
    w8[npt]= 1.;
    ++npt;
  }

/* Re-fit polynomial. */
  order = 6;
  f2cpolyfitglls_ ( &npt, x8, y8, w8, &order, coef, &iok );
  if (iok != 1) {
    fprintf(stderr,"***ERROR: esi_rebinarc:  FIT failed.\n");
    return(0);
  }

/* Report residuals. */
  f2cpolyfitgllsresiduals_ (&npt,x8,y8,w8,&order,coef,&high,&wrms);
  printf("NOTE: esi_rebinarc:  row=%2d  npt=%3d  high=%12.5e  wrms=%12.5e\n",
                           row,npt,high,wrms);

/* Write coeffecients into new header. */
  esiwave_put_coef(row,header,headersize,coef);

}

/* Write new arc file. */
printf("NOTE: Writing %s .\n",bbfile);
cwritefits( bbfile, aa, header, headersize );

return(1);
}


