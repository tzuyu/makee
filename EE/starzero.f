C starzero.f                              tab                  - July 1995
C
      implicit none
      integer*4 sc,ec,sr,er,row,ord,col
      integer*4 nc,nr,narg,inhead,lc,nbwr,getpos,i
      character*115200 header
      character*80 arg(9),outfile,starfile,bwrfile,chead,wv0,wv4,wrd
      character c7*7
      integer*4 maxpt
      parameter(maxpt=201000)
      real*4 a(maxpt)
      real*8 ech,xd,fhead,bwrA(900),bwrB(900),r81,r82
      real*8 coef(0:7),wave,polyval,hvsf,c,hv
      parameter(c=2.997925d+5)
      logical ok,badwave,findarg,atm,card_exist

C Get command line parameters.
      call arguments(arg,narg,9)
      atm = findarg(arg,narg,'-atm',':',wrd,i)
      if ((narg.ne.2).and.(narg.ne.3)) then
        print *,
     .  'Syntax: starzero (Standard Star FITS image) (Output image)'
        print *,
     .  '                 [file containing bad wavelength regions]'
        print *,'                 [-atm]'
        print *,
     .  '   Sets certain wavelength regions to zero (e.g. regions'
        print *,
     .  '   around absorption lines.)  If the "-atm" option is given,'
        print *,
     .  '   read the "atmabs.dat" file and set to zero the regions'
        print *,'   near atmospheric absorption features.'
        call exit(0)
      endif
      starfile= arg(1)
      call AddFitsExt(starfile)
      outfile = arg(2)
      call AddFitsExt(outfile)
      if (narg.eq.3) then
        bwrfile = arg(3)
        call AddFitsExt(bwrfile)
      else
        bwrfile = ' '
      endif

C Read fits file...
      call readfits(starfile,a,maxpt,header,ok)
      if (.not.ok) then
        print *,'Error reading fits file: ',starfile
        call exit(1)
      endif
      if (card_exist('HELIOVEL',header)) then
        hv = fhead('HELIOVEL',header)
        hvsf = dsqrt( (1.d0+(hv/c)) / (1.d0-(hv/c)) )
      else
        hv = 0.d0
        hvsf = 1.d0
      endif

C Try to find bwr file if not given.
      if (bwrfile.eq.' ') then
        wrd= chead('OBJECT',header)
        call WhatBWR(wrd,bwrfile)
        if (bwrfile.eq.' ') then
          print *,
     .  'Error- Could not find BWR file, enter on command line.'
          call exit(1)
        endif
      endif

      nc = inhead('NAXIS1',header)
      nr = inhead('NAXIS2',header)
      sc = inhead('CRVAL1',header)
      sr = inhead('CRVAL2',header)
      ec = sc + nc - 1
      er = sr + nr - 1
      print '(2a)','Standard star file= ',starfile(1:lc(starfile))
      ech= fhead('ECHANGL',header)
      xd = fhead('XDANGL',header)
      print '(a,2f9.4)','ECHA,XDA= ',ech,xd
      wrd= chead('OBJECT',header)
      print '(3a)',wrd(1:lc(wrd)),' ..with.. ',bwrfile(1:lc(bwrfile))
     
C Check header for flags.
      if (getpos('ATMEXTNC',header).eq.-1) then
        print *,'ERROR: No atmospheric extinction correction done.'
        call exit(1)
      endif

C Read bad wavelength regions.
      nbwr=0
      open(1,file=bwrfile,status='old')
1     read(1,*,end=5) r81,r82
      nbwr=nbwr+1
      bwrA(nbwr) = r81
      bwrB(nbwr) = r82
      goto 1
5     close(1)
      print '(3a,i5)',' From ',bwrfile(1:lc(bwrfile)),' :  nbwr=',nbwr

C Read in atmospheric absorption data, if requested.
      if (atm) then
        call GetMakeeHome(wrd)
        wrd = wrd(1:lc(wrd))//'atmabs.dat'
        open(1,file=wrd,status='old')
11      read(1,*,end=55) r81,r82
C Shift the lines to the heliocentric correction used for this spectrum.
        r81 = r81 * hvsf
        r82 = r82 * hvsf
        nbwr=nbwr+1
        bwrA(nbwr) = r81
        bwrB(nbwr) = r82
        goto 11
55      close(1)
        print '(a,i5)',' Including atmabs.dat ... nbwr=',nbwr
      endif
      
C Do each row.
      DO row=sr,er

C We use 7th order in HIRES reduced arc lamps, although they are really only
C 6th order polynomials for compatibility with Vista.
      ord = 7

C Extract coeffecients from header.
      write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',row
      wv0 = chead(c7,header)
      if (wv0.eq.' ') goto 800
      read(wv0,'(4(1pe17.9))',err=800) coef(0),coef(1),coef(2),coef(3)
      write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',row
      wv4 = chead(c7,header)
      if (wv4.eq.' ') goto 800
      read(wv4,'(4(1pe17.9))',err=800) coef(4),coef(5),coef(6),coef(7)

C Check each point.
      do col=sc,ec
        wave = polyval(ord+1,coef,dfloat(col))
        if (badwave(wave,bwrA,bwrB,nbwr)) then
          call SetToZero(a,sc,ec,sr,er,col,row)
        endif
      enddo

      ENDDO

C Write out FITS file.
      print '(2a)','Writing file: ',outfile(1:lc(outfile))
      call writefits(outfile,a,header,ok)
      if (.not.ok) then
        print *,'Error writing fits file: ',outfile
        call exit(1)
      endif
      
      print *,'All done.'
      stop
800   print *,'Error reading wavelength scale.'
      stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
      logical function badwave(wave,bwrA,bwrB,nbwr)
      implicit none
      integer*4 nbwr,i
      real*8 wave,bwrA(nbwr),bwrB(nbwr)
      logical ok
      ok=.true.
      do i=1,nbwr
        if ((wave.gt.bwrA(i)).and.(wave.lt.bwrB(i))) ok = .false.
      enddo
      badwave = (.not.ok)
      return
      end

C----------------------------------------------------------------------
      subroutine SetToZero(a,sc,ec,sr,er,col,row)
      implicit none
      integer*4 sc,ec,sr,er,col,row
      real*4 a(sc:ec,sr:er)
      a(col,row) = 0.
      return
      end

C----------------------------------------------------------------------
      subroutine WhatBWR(object,file)
      implicit none
      character*(*) object,file
      character*80 har,wrd
      integer*4 lc
      wrd = object
      call upper_case(wrd)
      call GetMakeeHome(har)

      if (index(wrd,'FEIGE 34').gt.0) then
        file = har(1:lc(har))//'/ssdb/f34.bwr'
      elseif (index(wrd,'FEIGE 67').gt.0) then
        file = har(1:lc(har))//'/ssdb/f67.bwr'
      elseif (index(wrd,'FEIGE 66').gt.0) then
        file = har(1:lc(har))//'/ssdb/f66.bwr'
      elseif (index(wrd,'FEIGE 110').gt.0) then
        file = har(1:lc(har))//'/ssdb/f110.bwr'

      elseif (index(wrd,'BD+28').gt.0) then
        file = har(1:lc(har))//'/ssdb/bd28.bwr'
      elseif (index(wrd,'BD+33').gt.0) then
        file = har(1:lc(har))//'/ssdb/f67.bwr'

      elseif (index(wrd,'BD +28').gt.0) then
        file = har(1:lc(har))//'/ssdb/bd28.bwr'
      elseif (index(wrd,'BD +33').gt.0) then
        file = har(1:lc(har))//'/ssdb/f67.bwr'

      elseif (index(wrd,'G191').gt.0) then
        file = har(1:lc(har))//'/ssdb/g191.bwr'
      elseif (index(wrd,'G 191').gt.0) then
        file = har(1:lc(har))//'/ssdb/g191.bwr'

      elseif (index(wrd,'HZ 44').gt.0) then
        file = har(1:lc(har))//'/ssdb/hz44.bwr'
      elseif (index(wrd,'HZ44').gt.0) then
        file = har(1:lc(har))//'/ssdb/hz44.bwr'
      else
        file = ' '
      endif
      return
      end
C----------------------------------------------------------------------
      subroutine WhatOSP(object,file)
      implicit none
      character*(*) object,file
      character*80 har,wrd
      integer*4 lc
      wrd = object
      call upper_case(wrd)
      call GetMakeeHome(har)

      if (index(wrd,'FEIGE 34').gt.0) then
        file = har(1:lc(har))//'/ssdb/feige34_osp.fits'
      elseif (index(wrd,'FEIGE 67').gt.0) then
        file = har(1:lc(har))//'/ssdb/feige67_osp.fits'
      elseif (index(wrd,'FEIGE 66').gt.0) then
        file = har(1:lc(har))//'/ssdb/feige66_osp.fits'
      elseif (index(wrd,'FEIGE 110').gt.0) then
        file = har(1:lc(har))//'/ssdb/feige110_osp.fits'

      elseif (index(wrd,'BD+28').gt.0) then
        file = har(1:lc(har))//'/ssdb/bd_28d4211_osp.fits'
      elseif (index(wrd,'BD+33').gt.0) then
        file = har(1:lc(har))//'/ssdb/bd_33d2642_osp.fits'

      elseif (index(wrd,'G191').gt.0) then
        file = har(1:lc(har))//'/ssdb/g191b2b_osp.fits'
      elseif (index(wrd,'G 191').gt.0) then
        file = har(1:lc(har))//'/ssdb/g191b2b_osp.fits'

      elseif (index(wrd,'HZ 44').gt.0) then
        file = har(1:lc(har))//'/ssdb/hz44_osp.fits'
      elseif (index(wrd,'HZ44').gt.0) then
        file = har(1:lc(har))//'/ssdb/hz44_osp.fits'
      else
        file = ' '
      endif
      return
      end
