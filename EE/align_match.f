C align_match.f                                       tab  apr96
C
C Align to match linear to log-linear and vice versa.
C
      implicit none
      character*115200 header1,header2,newhead
      character*80 arg(99),file1,file2,outfile,wrd
      real*8 fhead,cdelt1,crval1,get_the_wave,get_the_pixel
      real*8 bluelimit,redlimit,valread8,pix1,pix2,bigpix1,bigpix2
      real*8 blwv,rdwv,wave,waveblue,wavered,aa,bb,nwp
      integer*4 sc,ec,nc
      integer*4 narg,mx,i,inhead,naxis1,lc
      parameter(mx=2001000)
      real*4 ff(mx),a(mx),fv
      logical ok,loglin,findarg,nozero,lgi,patch

C Defaults.
      patch  = .true.
      nozero = .true.
      lgi    = .false.
      bluelimit = 0.d0
      redlimit  = 0.d0

C Get command line parameters and set defaults.
      call arguments(arg,narg,99)
      if (findarg(arg,narg,'xs=',':',wrd,i)) bluelimit = valread8(wrd)
      if (findarg(arg,narg,'xe=',':',wrd,i))  redlimit = valread8(wrd)
      if (findarg(arg,narg,'-nopatch','s',wrd,i))  patch = .false.

C If zero parameters or improper parameters, tell user syntax. 
      if (narg.eq.0) then
        print *,' '
        print *,' Syntax: align_match (FITS file 1 to be re-aligned)'
        print *,
     .  '                     (FITS file 2 to match the alignment to)'
        print *,'                     (FITS file output filename)'
        print *,'                     [ xs=  xe= ]  [ -nopatch ]'
        print *,' '
        print *,
     .  '  Re-aligns the first file to match the alignment of the'
        print *,'  second file.  Files must be linear or log-linear.'
        print *,' '
        call exit(0)
      endif

C Filenames.
      file1 = arg(1)
      call AddFitsExt(file1)
      file2 = arg(2)
      call AddFitsExt(file2)
      outfile = arg(3)
      call AddFitsExt(outfile)

C Read second file.
      print '(2a)','Reading:',file2(1:lc(file2))
      call readfits(file2,a,mx,header2,ok)
      loglin = (inhead('DC-FLAG',header2).eq.1)

C Zero final array.
      do i=1,mx
        ff(i)=0.
      enddo

C Read first image.
      print '(2a)','Reading:',file1(1:lc(file1))
      call readfits(file1,a,mx,header1,ok)
      sc = 1
      nc = inhead('NAXIS1',header1)
      ec = sc + nc - 1

C Limits of old data.
      blwv = get_the_wave(-1.d0,header1)
      blwv = get_the_wave(dfloat(sc),header1)
      rdwv = get_the_wave(dfloat(ec),header1)

C     print *,'limits of old data...'
C     print *,'blwv,rdwv=',blwv,rdwv

C Initial pixel for new array.
      wave= get_the_wave(-1.d0,header2)
      nwp = 1.d0
      wave= get_the_wave(nwp,header2)

C     print *,'Initial pixel for new array...'
C     print *,'wave=',wave

C Initialize get_the_pixel with header1.
      pix1 = get_the_pixel(-1.d0,header1)

      do while(wave.lt.rdwv)
        if (wave.lt.blwv) then
          ff(nint(nwp)) = 0.
        else
C Pixel boundaries.
          waveblue= get_the_wave(nwp-0.5d0,header2)
          wavered = get_the_wave(nwp+0.5d0,header2)
          pix1    = get_the_pixel(waveblue,header1)
          pix2    = get_the_pixel(wavered ,header1)
          if ((nint(pix1).ge.sc).and.(nint(pix2).le.ec)) then
            call linear_fluxave(a,sc,ec,1,1,1,pix1,pix2,fv,nozero,lgi)
            ff(nint(nwp)) = fv
          endif
        endif
        nwp = nwp + 1.d0
        wave= get_the_wave(nwp,header2)
      enddo

C Starting and ending pixel of new big spectrum.
      if (bluelimit.gt.0.) then
        nwp=1.d0
        wave = get_the_wave(nwp,header2)
        do while(wave.lt.bluelimit)
          nwp=nwp+1.d0
          wave = get_the_wave(nwp,header2)
        enddo
        bigpix1 = nwp
        nwp=1.d0
        wave = get_the_wave(nwp,header2)
        do while(wave.lt.redlimit)
          nwp=nwp+1.d0
          wave = get_the_wave(nwp,header2)
        enddo
        bigpix2 = nwp-1.d0
      else
        bigpix1 = 1.d0
        bigpix2 = inhead('NAXIS1',header2)
      endif
      if (bigpix2.lt.bigpix1) then
        print *,'bigpix1=',bigpix1
        print *,'bigpix2=',bigpix2
        print *,'Error combining spectra. bigpix2.lt.bigpix1.'
        call exit(1)
      endif

C Set header card values.
      if (loglin) then
        aa = fhead('CRVAL1',header2)
        bb = fhead('CDELT1',header2)
        crval1 = aa + (bb*(bigpix1-1.d0))
        cdelt1 = bb
        naxis1 = 1 + nint(bigpix2-bigpix1)
      else
        crval1 = get_the_wave(bigpix1,header2)
        cdelt1 = fhead('CDELT1',header2)
        naxis1 = 1 + nint(bigpix2-bigpix1)
      endif

C Copy header to new header.
      newhead = header1

C Add new cards to new header.
      call fheadset('CRVAL1',crval1,newhead)
      call fheadset('CDELT1',cdelt1,newhead)
      call cheadset('CTYPE1','LAMBDA',newhead)
      if (loglin) call inheadset('DC-FLAG',1,newhead)
      call inheadset('NAXIS1',naxis1,newhead)
      call inheadset('CRPIX1',1,newhead)
      call inheadset('NAXIS',1,newhead)
      call unfit('NAXIS2',newhead)
      call unfit('CRVAL2',newhead)
      call unfit('CDELT2',newhead)
      call unfit('CRPIX2',newhead)
      call cheadset('FILENAME',outfile,newhead)
      if (loglin) then
        call inheadset('WCSDIM',1,newhead)
        call cheadset('CTYPE1','LINEAR',newhead)
        call fheadset('CD1_1',cdelt1,newhead)
        call inheadset('LTM1_1',1,newhead)
        call cheadset('WAT0_001','system=equispec',newhead)
C Put in blank spaces for IRAF.
        i = index(newhead,'=equispec')
        newhead(i+9:i+9)=' '
        newhead(i+62:i+62)=char(39)
        call cheadset('WAT1_001',
     .       'wtype=linear label=Wavelength units=Angstroms',newhead)
C Put in blank spaces for IRAF.
        i = index(newhead,'units=Angstroms')
        newhead(i+15:i+15)=' '
        newhead(i+38:i+38)=char(39)
        call inheadset('DC-FLAG',1,newhead)
        call cheadset('APNUM1','1 1 1.0 1.0',newhead)
      else
C Remove IRAF type cards if we have a linear spectrum.
        call unfit('WCSDIM',newhead)
        call unfit('CD1_1',newhead)
        call unfit('LTM1_1',newhead)
        call unfit('WAT0_001',newhead)
        call unfit('WAT1_001',newhead)
        call unfit('DC-FLAG',newhead)
        call unfit('APNUM1',newhead)
      endif
C Patch up zero pixel spots if requested.
      if (patch) call interp_zero_pts(1,nint(nwp),ff)
C Write out flux spectrum.
      print '(2a)',' Writing flux : ',outfile(1:lc(outfile))
      call writefits(outfile,ff(nint(bigpix1)),newhead,ok)
      if (.not.ok) goto 860

      print *,'All done.'
      stop
800   continue
      print *,'Error reading WV header card.'
      stop
850   print *,'Error- filename must have .fits extension.'
      stop
860   print *,'Error reading or writing FITS file.'
      stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
C Find flux (per unit original bin) between the pixel values pix1 and pix2.
C
      subroutine linear_fluxave(a,sc,ec,sr,er,row,
     .                             pix1,pix2,bval,nozero,lgi)
C
      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er),bval
      real*8 pix1,pix2
      logical nozero,lgi,more
      real*8 Y0,Ym,Yp,x1,x3,sum,wsum,area,b0,b1
      integer*4 i,i0,i1,i2

C Check for zero pixels.
      if (nozero) then
        more = .true.
        do i=nint(pix1),nint(pix2)
          if (abs(a(i,row)).lt.1.e-30) more = .false.
        enddo
        if (.not.more) then
          bval = 0.
          return
        endif
      endif
C Sum and weight.
      sum = 0.d0
      wsum= 0.d0
C Whole pixels.
      i1 = nint(pix1) + 1
      i2 = nint(pix2) - 1
      if (i2.ge.i1) then
        do i=i1,i2
          sum = sum + dble(a(i,row))
          wsum= wsum+ 1.d0
        enddo
      endif

      IF (lgi) THEN

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))
      Ym = dble(a(max(sc,i0-1),row))
      Yp = dble(a(min(ec,i0+1),row))
      x1 = dfloat(i0) - 1.d0
      x3 = dfloat(i0) + 1.d0
      call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
      sum = sum + area
      wsum= wsum+ (b1-b0)
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))
        Ym = dble(a(max(sc,i0-1),row))
        Yp = dble(a(min(ec,i0+1),row))
        x1 = dfloat(i0) - 1.d0
        x3 = dfloat(i0) + 1.d0
        call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
        sum = sum + area
        wsum= wsum+ (b1-b0)
      endif

      ELSE

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))
      sum = sum + (b1-b0)*Y0
      wsum= wsum+ (b1-b0)
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))
        sum = sum + (b1-b0)*Y0
        wsum= wsum+ (b1-b0)
      endif

      ENDIF

C Average flux per unit original bin.
      bval = sngl( sum / wsum )

      return
      end


C----------------------------------------------------------------------
C Find flux error (per unit original bin) between the pixel values pix1 and
C pix2.  The variance is used in calculating the new error.
C
C The simple derivation of the error on  F = sum( Wi Ai ) / sum( Ai ) would be
C   var(F) = sum( Wi * Wi * var(Ai) ) / [ sum( Wi ) * sum( Wi ) ] , however,
C in cases of fractional pixels this underestimates the error since it neglects
C the correlation between adjacent bins.  We use the following equation:
C   var(F) = sum( Wi * var(Ai) ) / [ sum( Wi ) * sum( Wi ) ] , which is
C incorrect but probably a better estimate of the true error. Note that in
C the case of whole pixels the equations are the same.
C
      subroutine linear_errave(a,sc,ec,sr,er,row,pix1,pix2,bval,lgi)
C
      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er),bval
      real*8 pix1,pix2
      logical lgi,more
      real*8 Y0,Ym,Yp,x1,x3,sum,wsum,area,b0,b1,wt
      integer*4 i,i0,i1,i2

C Check for zero or negative pixels.
      more = .true.
      do i=nint(pix1),nint(pix2)
        if (a(i,row).lt.1.e-30) more = .false.
      enddo
      if (.not.more) then
        bval = -1.
        return
      endif
C Sum and weight.
      sum = 0.d0
      wsum= 0.d0
C Whole pixels.
      i1 = nint(pix1) + 1
      i2 = nint(pix2) - 1
      if (i2.ge.i1) then
        do i=i1,i2
          sum = sum + (dble(a(i,row))*dble(a(i,row)))
          wsum= wsum+ 1.d0
        enddo
      endif

      IF (lgi) THEN

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))*dble(a(i0,row))
      Ym = dble(a(max(sc,i0-1),row))*dble(a(max(sc,i0-1),row))
      Yp = dble(a(min(ec,i0+1),row))*dble(a(min(ec,i0+1),row))
      x1 = dfloat(i0) - 1.d0
      x3 = dfloat(i0) + 1.d0
      call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
      sum = sum + area
      wsum= wsum+ (b1-b0)
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))*dble(a(i0,row))
        Ym = dble(a(max(sc,i0-1),row))*dble(a(max(sc,i0-1),row))
        Yp = dble(a(min(ec,i0+1),row))*dble(a(min(ec,i0+1),row))
        x1 = dfloat(i0) - 1.d0
        x3 = dfloat(i0) + 1.d0
        call lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)
        sum = sum + area
        wsum= wsum+ (b1-b0)
      endif

      ELSE

C More to calculate.
      more = .true.
C Left fractional pixel.
      b0 = pix1
      b1 = dfloat(nint(pix1)) + 0.5d0
      if (b1.gt.pix2) then
        b1 = pix2
        more = .false.
      endif
      i0 = nint(pix1)
      Y0 = dble(a(i0,row))*dble(a(i0,row))
      wt = (b1-b0)
      sum = sum + wt*Y0
      wsum= wsum+ wt
C Right fractional pixel.
      if (more) then
        b1 = pix2
        b0 = dfloat(nint(pix2)) - 0.5d0
        i0 = nint(pix2)
        Y0 = dble(a(i0,row))*dble(a(i0,row))
        wt = (b1-b0)
        sum = sum + wt*Y0
        wsum= wsum+ wt
      endif

      ENDIF

C Average flux error per unit original bin.
      if ((wsum.gt.0.).and.(sum.gt.0.)) then
        bval = sngl( sqrt( sum ) / wsum )
      else
        bval = -1.0
      endif

      return
      end


C----------------------------------------------------------------------
C Given three bin fluxes, Ym,Y0,Yp, and the x values at the center of
C the outer bins, x1,x3, find the flux or area between two x boundary
C values, b0 and b1. These boundaries should be contained within the limits
C of the middle bin (inclusive.)
C
C This routine uses lgi interpolation (i.e. polynomial interpolation) by
C finding a parabola which matches the area of the three bins across each bin.
C
C Imagine the center of the bins being at x = -1.0, 0.0, and +1.0, neglecting
C any non-linearity from x1 through x3, and the three point fluxes are
C Ym, Y0, and, Yp.  The solution to a parabola which goes through the three
C points ( y = c0 + c1*x + c2*x*x ) would be c0 = Y0 , c1 = (Yp-Ym)/2, and
C c2 = (Ym+Yp)/2 - Y0 .
C
C However, we want a parabola such that the area under the curve within the
C region of each bin matches the flux in that bin.  The solution to such a
C parabola ( y = a0 + a1*x + a2*x*x ) would be:  a1 = (Yp-Ym)/2 = c1 , 
C a2 = (Ym+Yp)/2 - Y0 = c2, and  a0 = Y0 - (a2/12) = c0 - (c2/12) .
C
      subroutine lgi_align(Ym,Y0,Yp,x1,x3,b0,b1,area)

      implicit none
      real*8 Ym,Y0,Yp,x1,x3,b0,b1,area
      real*8 a0,a1,a2,n0,n1

C Coeffecients.
      a1 = (Yp-Ym)/2.d0
      a2 = ((Ym+Yp)/2.d0) - Y0
      a0 = Y0 - (a2/12.d0)

C Boundaries in terms of new x coordinates.
      n0 = ( 2.d0*(b0-x1)/(x3-x1) ) - 1.d0
      n1 = ( 2.d0*(b1-x1)/(x3-x1) ) - 1.d0

C Check (temporary?).
      if ((n1.lt.n0).or.(n0.lt.-0.501).or.(n1.gt.0.501)) then
        print *,'ERROR in lgi_align -- bad limits.'
        call exit(1)
      endif

C Integrate polynomial between n0 and n1.
      area = ( (a0*n1) + (a1*n1*n1/2.d0) + (a2*n1*n1*n1/3.d0) ) 
     .     - ( (a0*n0) + (a1*n0*n0/2.d0) + (a2*n0*n0*n0/3.d0) )

      return
      end

C----------------------------------------------------------------------
C Interpolate zero points.
C
      subroutine interp_zero_pts(sc,ec,a)
C
      implicit none
      integer*4 sc,ec
      real*4 a(sc:ec),sum,b(90000)
      integer*4 i,num1,num2,i1,i2
C Copy to temporary array.
        do i=sc,ec
          b(i)=a(i)
        enddo
C Search for a zero pixel.
        i1=sc
        do while(i1.le.ec)
          if (abs(b(i1)).lt.1.e-30) then
C Find the extent of the region of zero pixels.
            i2=i1+1
            do while((i2.le.ec).and.(abs(b(i2)).lt.1.e-30))
              i2=i2+1
            enddo
            i2=i2-1
C Find a mean value for local valid pixels.
            sum=0.
C Look for valid pixels to the left of region.
            i=i1-1
            num1=0
            do while((i.ge.sc).and.(num1.lt.10))
              if (abs(b(i)).gt.1.e-30) then
                num1=num1+1
                sum =sum +b(i)
              endif
              i=i-1
            enddo
C Look for valid pixels to the right of region.
            i=i2+1
            num2=0
            do while((i.le.ec).and.(num2.lt.10))
              if (abs(b(i)).gt.1.e-30) then
                num2=num2+1
                sum =sum +b(i)
              endif
              i=i+1
            enddo
            sum = sum / float(max(1,num1+num2))
C Fill in zero range.
            do i=i1,i2
              a(i)=sum
            enddo
          else
            i2=i1
          endif
          i1=i2+1
        enddo

      return
      end

C----------------------------------------------------------------------
C Gets the pixel value given a wavelength and a linear or log-linear header.
C Use wave < 0 to intialize.
C
C For a linear scale    :  wavelength = zero + disp*(pixel-1)
C For a log-linear scale:  wavelength = 10**( azero + bdisp*(pixel-1) )
C where azero=log10(zero) and bdisp=( ( disp in km/s )  / ( c * ln(10) ) ).
C
      real*8 function get_the_pixel(wave,header)
C
      implicit none
      real*8 wave
      character*(*) header
      logical loglin
      real*8 fhead,crval1,cdelt1
      integer*4 inhead
C
      common /GETTHEPIXELBLK/ crval1,cdelt1,loglin
C
      if (wave.lt.0.) then
        loglin = (inhead('DC-FLAG',header).eq.1)
        crval1 = fhead('CRVAL1',header)
        cdelt1 = fhead('CDELT1',header)
      endif
      if (loglin) then
        get_the_pixel = ( (log10(wave)-crval1)/cdelt1) + 1.d0
      else
        get_the_pixel = ( (wave-crval1)/cdelt1) + 1.d0
      endif
      return
      end

C----------------------------------------------------------------------
C Gets the wavelength value given a pixel and a linear or log-linear header.
C Use pixel < 0 to intialize.
C
C For a linear scale    :  wavelength = zero + disp*(pixel-1).
C For a log-linear scale:  wavelength = 10**( azero + bdisp*(pixel-1) ).
C where azero=log10(zero) and bdisp=( ( disp in km/s )  / ( c * ln(10) ) ).
C
      real*8 function get_the_wave(pixel,header)
      implicit none
      real*8 pixel
      character*(*) header
      logical loglin
      real*8 fhead,crval1,cdelt1
      integer*4 inhead
C
      common /GETTHEWAVEBLK/ crval1,cdelt1,loglin
C
      if (pixel.lt.0.) then
        loglin = (inhead('DC-FLAG',header).eq.1)
        crval1 = fhead('CRVAL1',header)
        cdelt1 = fhead('CDELT1',header)
      endif
      if (loglin) then
        get_the_wave = 10.d0 ** ( crval1 + cdelt1*(pixel-1.d0) )
      else
        get_the_wave = crval1 + ( cdelt1*(pixel-1.d0) )
      endif
      return
      end
