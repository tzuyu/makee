C arc2arc.f
C
      implicit none
      character*80 arg(99)
      integer*4 narg
      logical ok
C
      include 'soun.inc'
C
C Standard output.
      soun=6

C Get command line arguments.
      call arguments(arg,narg,99)

C Call the program.
      call arc2arc_main(arg,narg,ok)

      stop
      end

CENDOFMAIN
