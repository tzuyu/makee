C smthresp.f
C
      implicit none
      integer*4 sc,ec,sr,er
      integer*4 nc,nr,narg,inhead,lc
      character*115200 header
      character*80 arg(9),outfile,respfile
      character*80 chead,wrd
      integer*4 maxpt
      parameter(maxpt=201000)
      real*4 a(maxpt),b(maxpt)
      real*8 ech,xd,fhead
      logical ok

C Get command line parameters.
      call arguments(arg,narg,9)
      if (narg.ne.2) then
        print *,
     .  'Syntax: smthresp (Response function image) (Output image)'
        print *,
     .  '   Smooths a response function image and ignores zero pixels.'
        call exit(0)
      endif
      respfile= arg(1)
      call AddFitsExt(respfile)
      outfile = arg(2)
      call AddFitsExt(outfile)

C Read fits file...
      call readfits(respfile,a,maxpt,header,ok)
      if (.not.ok) then
        print *,'Error reading fits file: ',respfile
        call exit(1)
      endif
      nc = inhead('NAXIS1',header)
      nr = inhead('NAXIS2',header)
      sc = inhead('CRVAL1',header)
      sr = inhead('CRVAL2',header)
      ec = sc + nc - 1
      er = sr + nr - 1
      print '(2a)','Response function file= ',respfile(1:lc(respfile))
      ech= fhead('ECHANGL',header)
      xd = fhead('XDANGL',header)
      print '(a,2f9.4)','ECHA,XDA= ',ech,xd
     
C Smooth the image.
      call SmoothImage(a,b,sc,ec,sr,er)

C Write out FITS file.
      wrd = chead('OBJECT',header)
      wrd = wrd(1:lc(wrd))//' (Smoothed)'
      call cheadset('OBJECT',wrd,header)
      print '(2a)','Writing file: ',outfile(1:lc(outfile))
      call writefits(outfile,b,header,ok)
      if (.not.ok) then
        print *,'Error writing fits file: ',outfile
        call exit(1)
      endif
      
      print *,'All done.'
      stop
      end

CENDOFMAIN

C----------------------------------------------------------------------
C
      subroutine accum(a,sc,ec,sr,er,col,row,range,weight,
     .  sum,wsum,scale)
C
      implicit none
      integer*4 sc,ec,sr,er,col,row,range,i
      real*4 a(sc:ec,sr:er),sum,wsum,weight,scale
C Outside image.
      if ((row.lt.sr).or.(row.gt.er)) return
C Accumulate.
      do i=max(sc,col-range),min(ec,col+range)
        if (abs(a(i,row)).gt.1.e-30) then
          sum = sum + (weight*a(i,row)*scale)
          wsum= wsum+ weight
        endif
      enddo
      return
      end

C----------------------------------------------------------------------
      subroutine SmoothImage(a,b,sc,ec,sr,er)
      implicit none
      integer*4 sc,ec,sr,er
      real*4 a(sc:ec,sr:er)
      real*4 b(sc:ec,sr:er)
      real*4 sum,mean(-2:100),scale,wsum,rr
      integer*4 i,col,row

C Initialize.
      do i=-2,100
        mean(i) = 1.
      enddo

C Do each row.
      DO row=sr,er
C Find mean using points from overlapping non-zero region in three rows.
        call NonZeroMeans(a,sc,ec,sr,er,row,mean)
        rr = mean(row)
C Do each column.
        DO col=sc,ec
C Initialize.
          sum = 0.
          wsum= 0.
          scale = 1.
          call accum(a,sc,ec,sr,er,col,row,80,5.,sum,wsum,scale)
          call accum(a,sc,ec,sr,er,col,row,40,8.,sum,wsum,scale)
          scale = rr / mean(row-1)
          call accum(a,sc,ec,sr,er,col,row-1,60,2.,sum,wsum,scale)
          call accum(a,sc,ec,sr,er,col,row-1,30,2.,sum,wsum,scale)
          scale = rr / mean(row+1)
          call accum(a,sc,ec,sr,er,col,row+1,60,2.,sum,wsum,scale)
          call accum(a,sc,ec,sr,er,col,row+1,30,2.,sum,wsum,scale)
          scale = rr / mean(row-2)
          call accum(a,sc,ec,sr,er,col,row-2,20,1.,sum,wsum,scale)
          call accum(a,sc,ec,sr,er,col,row-2,20,1.,sum,wsum,scale)
          scale = rr / mean(row+2)
          call accum(a,sc,ec,sr,er,col,row+2,20,1.,sum,wsum,scale)
          call accum(a,sc,ec,sr,er,col,row+2,20,1.,sum,wsum,scale)
          b(col,row) = sum / wsum
        ENDDO
      ENDDO
      return
      end


C----------------------------------------------------------------------
C
      subroutine NonZeroMeans(a,sc,ec,sr,er,row,mean)
      implicit none
      integer*4 sc,ec,sr,er,row
      real*4 a(sc:ec,sr:er),mean(-2:100)
      real*4 sum1,sum2,sum3,sum4,sum5
      integer*4 num,col
      
C Initialize.
      sum1=0.
      sum2=0.
      sum3=0.
      sum4=0.
      sum5=0.
      num =0

C All five rows.
      if ((row.ge.sr+2).and.(row.le.er-2)) then
        do col=sc,ec
          if ( (a(col,row-2).gt.0.).and.
     .         (a(col,row-1).gt.0.).and.
     .         (a(col,row  ).gt.0.).and.
     .         (a(col,row+1).gt.0.).and.
     .         (a(col,row+2).gt.0.) ) then
            sum1 = sum1 + a(col,row-2)
            sum2 = sum2 + a(col,row-1)
            sum3 = sum3 + a(col,row  )
            sum4 = sum4 + a(col,row+1)
            sum5 = sum5 + a(col,row+2)
            num  = num  + 1
          endif
        enddo

      elseif (row.eq.sr+1) then
        do col=sc,ec
          if ( (a(col,row-1).gt.0.).and.
     .         (a(col,row  ).gt.0.).and.
     .         (a(col,row+1).gt.0.).and.
     .         (a(col,row+2).gt.0.) ) then
            sum2 = sum2 + a(col,row-1)
            sum3 = sum3 + a(col,row  )
            sum4 = sum4 + a(col,row+1)
            sum5 = sum5 + a(col,row+2)
            num  = num  + 1
          endif
        enddo

      elseif (row.eq.sr) then
        do col=sc,ec
          if ( (a(col,row  ).gt.0.).and.
     .         (a(col,row+1).gt.0.).and.
     .         (a(col,row+2).gt.0.) ) then
            sum3 = sum3 + a(col,row  )
            sum4 = sum4 + a(col,row+1)
            sum5 = sum5 + a(col,row+2)
            num  = num  + 1
          endif
        enddo

      elseif (row.eq.er-1) then
        do col=sc,ec
          if ( (a(col,row-2).gt.0.).and.
     .         (a(col,row-1).gt.0.).and.
     .         (a(col,row  ).gt.0.).and.
     .         (a(col,row+1).gt.0.) ) then
            sum1 = sum1 + a(col,row-2)
            sum2 = sum2 + a(col,row-1)
            sum3 = sum3 + a(col,row  )
            sum4 = sum4 + a(col,row+1)
            num  = num  + 1
          endif
        enddo

      elseif (row.eq.er) then
        do col=sc,ec
          if ( (a(col,row-2).gt.0.).and.
     .         (a(col,row-1).gt.0.).and.
     .         (a(col,row  ).gt.0.) ) then
            sum1 = sum1 + a(col,row-2)
            sum2 = sum2 + a(col,row-1)
            sum3 = sum3 + a(col,row  )
            num  = num  + 1
          endif
        enddo
 
      endif

      if (num.lt.10) then
        print *,'Error-- not enough points. row,num=',row,num
        call exit(1)
      endif

      if (sum1.gt.0.) mean(row-2) = sum1 / float(num)
      if (sum2.gt.0.) mean(row-1) = sum2 / float(num)
      if (sum3.gt.0.) mean(row)   = sum3 / float(num)
      if (sum4.gt.0.) mean(row+1) = sum4 / float(num)
      if (sum5.gt.0.) mean(row+2) = sum5 / float(num)

      return
      end

