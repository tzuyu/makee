C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C HDR-UTIL.for                                    Barlow  1991
C
C  Contains routines for munipulating FITS headers.
C
C ============== READ VALUES FROM A STRING ================================
C     SUBROUTINE DISSECT_VALUE(S,N,R,OK)     ! read Nth value from string
C     LOGICAL FUNCTION VALLIKE(C)
C     LOGICAL FUNCTION NUMLIKE(C)
C     SUBROUTINE GET_REAL(S,R,OK)
C =================================================================
C ============= CLEAN UP HEADER ===================================
C     SUBROUTINE NICE_HEADER(HEADER)         ! runs the following routines
C     SUBROUTINE ALL_NORMAL(C)
C     SUBROUTINE PUT_END_CARD(HEADER)
C     SUBROUTINE REMOVE_BLANK_LINES(HEADER)
C     SUBROUTINE REMOVE_BLANK_COMMENTS(HEADER)
C     SUBROUTINE HEADER_WARNINGS(HEADER)
C =================================================================
C =========== READ CARDS IN FROM HEADER ===============================
C     LOGICAL FUNCTION GETCARD(CNAME,HEADER,CARD) ! get card string from header
C     INTEGER*4 FUNCTION INHEAD(CNAME,HEADER)
C     REAL*8 FUNCTION FHEAD(CNAME,HEADER)
C     CHARACTER*80 FUNCTION CHEAD(CNAME,HEADER)
C     LOGICAL FUNCTION LHEAD(CNAME,HEADER,CARD)
C =================================================================
C =========== WRITE CARDS INTO HEADER =============================
C     INTEGER*4 FUNCTION GETSPACE(CNAME,HEADER)  ! find loc. card or make space
C     SUBROUTINE INHEADSET(CNAME,INPARAM,HEADER)
C     SUBROUTINE FHEADSET(CNAME,FPARAM,HEADER)
C     SUBROUTINE CHEADSET(CNAME,CPARAM,HEADER)
C ==============================================================
C ============== MISCELLANEOUS HEADER STUFF ====================
C     SUBROUTINE UNFIT(CNAME,HEADER)                  ! remove card
C     INTEGER*4 FUNCTION GETPOS(CNAME,HEADER)         ! find loc. of card
C     SUBROUTINE MAKE_BASIC_HEADER(HEADER,SC,NC,SR,NR)
C     SUBROUTINE SET_FITS_SCALE(A,HEADER,BITPIX,BZERO,BSCALE,AUTO)
C     SUBROUTINE GET_FITS_SCALE(HEADER,BITPIX,BZERO,BSCALE)
C     real*4 function getx(i,header)                  ! get pixels/wavelengths
C     logical function datecoord(hdr,udate,utime,epoch,coord,coord0,rashift)
C     subroutine epoch_to_doy(epoch,udate)
C     logical function card_exist(cname,header)
C ==============================================================

C----------------------------------------------------------------------
      logical function card_exist(cname,header)
      implicit none
      character*(*) cname
      character*(*) header
      integer*4 getpos
      card_exist = (getpos(cname,header).ne.-1)
      return
      end

C----------------------------------------------------------------------
C This function decides if an observation is a dark exposure.
C
      logical function DarkObs( header )
C
      implicit none
      character*(*) header    ! FITS header (input/output)
      logical card_exist,lhead
      integer*4 inhead
C
      DarkObs = .false.
      if (lhead('DARKCLOS',header)) then
         DarkObs = .true.
      endif
C Deleted this condition...  -tab 20apr05
C     if (lhead('AUTOSHUT',header)) then
C        DarkObs = .true.
C     endif
      if (card_exist('TTIME',header)) then
        if (inhead('TTIME',header) == 0) then
           DarkObs = .true.
        endif
      endif
      if (card_exist('ELAPTIME',header)) then
        if (inhead('ELAPTIME',header) == 0) then
           DarkObs = .true.
        endif
      endif
      if (card_exist('EXPOSURE',header)) then
        if (inhead('EXPOSURE',header) == 0) then
           DarkObs = .true.
        endif
      endif
      return
      end






C ============== READ VALUES FROM A STRING ================================

C--------------------------------------------------------------------------
C Calls DISSECT_VALUE (see below).

      logical function dissect(s,n,r)

      implicit none
      character*(*) s    ! String containing number         (input)
      integer*4 n        ! Number of entry in string        (input)
      real*8 r           ! Value of number                  (output)
      logical ok
      call dissect_value(s,n,r,ok)
      dissect = ok
      return
      end

C--------------------------------------------------------------------------

      SUBROUTINE DISSECT_VALUE(S,N,R,OK)

C Reads in a number from a string by selecting the nth continous sequence
C of value-like characters ( .,+,-,0-9,E,: ) with at least one number-like
C character (0-9).

      IMPLICIT NONE
      CHARACTER*(*) S    ! String containing number         (input)
      INTEGER*4 N        ! Number of entry in string        (input)
      REAL*8 R           ! Value of number                  (output)
      LOGICAL OK         ! .false. if error getting number  (output)

      INTEGER*4 LS,I,J,NUM,II,JJ,LL,K
      REAL*8 RR,DIV
      CHARACTER*80 T,TT
      LOGICAL NUMLIKE,VALLIKE,AGAIN

      R = 0.D0
      LS= LEN(S)
      T = ' '
      T(1:LS) = S
      CALL ALLCAPS(T)
      I=1
      NUM=0
C Find a string of value-like characters
1     DO WHILE( (I.LE.LS) .AND. (.NOT.VALLIKE(T(I:I))) )
        I=I+1
      ENDDO
      IF (I.GT.LS) GOTO 900   ! too far, failed to find requested value
      J=I+1
      DO WHILE( (J.LE.LS) .AND. (VALLIKE(T(J:J))) )
        J=J+1
      ENDDO
      J=J-1

C See if there is at least one number-like character
      K=I
      DO WHILE( (K.LE.J) .AND. (.NOT.NUMLIKE(T(K:K))) )
        K=K+1
      ENDDO
      IF (K.GT.J) THEN  ! this wasn't really a value, try again
        I=J+1
        GOTO 1
      ENDIF

C Have a value, see if this is the one we want
      NUM=NUM+1
      IF (NUM.LT.N) THEN      ! look for next value
        I = J + 1
        GOTO 1
      ENDIF

C Got the value
      IF (INDEX(T(I:J),':').GT.0) THEN   ! time-like value
        LL= J-I+1
        TT= ' '
        TT(1:LL)=T(I:J)
        K = INDEX(TT,'-')   ! If there is any -, its all negative
        IF (K.GT.0) THEN
          TT(K:K)='0'       ! This should make it all positive for now
          K=-1              ! This will be multiplied in later
        ELSE
          K=1
        ENDIF
        II = 1                       ! markers within substring
        JJ = INDEX(TT,':')
        TT(JJ:JJ) = 'x'              ! blank out location of :
        DIV = 1.D0
        RR = 0.D0
        AGAIN = .TRUE.
        DO WHILE (AGAIN)
          IF (JJ.EQ.II) GOTO 900     ! a single : found, abort!
          CALL GET_REAL(TT(II:JJ-1),R,OK)
          IF (.NOT.OK) RETURN        ! error reading value
          RR = RR + ( R/DIV )
          II = JJ + 1                ! step beyond the :
          IF (II.GT.LL) THEN         ! too far
            AGAIN=.FALSE.            ! flag a stop
          ELSE
            JJ=INDEX(TT,':')         ! find next :
            IF (JJ.EQ.0) THEN
              JJ=LL+1                ! go to beyond end of substring
            ELSE
              TT(JJ:JJ) = 'x'
            ENDIF
            DIV = DIV*60.0           ! next one will be divided another 60
          ENDIF
        ENDDO
        R = RR*DFLOAT(K)             ! multiply in sign
      ELSE
        CALL GET_REAL(T(I:J),R,OK)
      ENDIF

      RETURN

900   OK = .FALSE.
      RETURN
      END



C--------------------------------------------------------------------------
      LOGICAL FUNCTION VALLIKE(C)

      CHARACTER*1 C   ! Character to check if value-like   (input)
      INTEGER*4 I
      I=ICHAR(C)
      IF ((I.GT.47).AND.(I.LT.58)) THEN    ! 0,1,2,3,4,5,6,7,8,9
        VALLIKE=.TRUE.
      ELSEIF (I.EQ.43) THEN                ! +
        VALLIKE=.TRUE.
      ELSEIF (I.EQ.45) THEN                ! -
        VALLIKE=.TRUE.
      ELSEIF (I.EQ.46) THEN                ! .
        VALLIKE=.TRUE.
      ELSEIF (I.EQ.69) THEN                ! E
        VALLIKE=.TRUE.
      ELSEIF (I.EQ.58) THEN                ! :
        VALLIKE=.TRUE.
      ELSE
        VALLIKE=.FALSE.
      ENDIF
      RETURN
      END


C--------------------------------------------------------------------------
      LOGICAL FUNCTION NUMLIKE(C)

      CHARACTER*1 C   ! Character to check if number-like   (input)
      INTEGER*4 I
      I=ICHAR(C)
      IF ((I.GT.47).AND.(I.LT.58)) THEN    ! 0,1,2,3,4,5,6,7,8,9
        NUMLIKE=.TRUE.
      ELSE
        NUMLIKE=.FALSE.
      ENDIF
      RETURN
      END


C--------------------------------------------------------------------------

      SUBROUTINE GET_REAL(S,R,OK)

C Reads in a real (or integer) number from a string.
C "S" should contain no blanks.

      IMPLICIT NONE
      CHARACTER*(*) S    ! String containing number         (input)
      REAL*8 R           ! Value of number                  (output)
      LOGICAL OK         ! .false. if error getting number  (output)
      INTEGER*4 LS,DP,I
      CHARACTER*40 T
      OK=.TRUE.    ! assume the best
      T = ' '
      LS=LEN(S)
      IF (LS.GT.39) THEN
        PRINT *,'GET_REAL ERROR: String too long, >39 chars.'
        GOTO 990
      ENDIF
      DP = INDEX(S,'.')
      IF (DP.EQ.0) THEN    ! must insert decimal point
        I = INDEX(S,'E')
        IF (I.EQ.0) I = INDEX(S,'e')
        IF (I.GT.0) THEN
          T = T(1:39-LS)//S(1:I-1)//'.'//S(I:LS)
        ELSE
          T = T(1:39-LS)//S//'.'
        ENDIF
        READ(UNIT=T,FMT='(F40.20)',ERR=990) R
      ELSE
        T  = T(1:40-LS)//S
        IF (LS-DP.LT.20) THEN
          READ(UNIT=T,FMT='(F40.20)',ERR=990) R
        ELSE
          PRINT *,'GET_REAL ERROR: Too many decimal places.'
        ENDIF
      ENDIF
      RETURN

990   PRINT *,'GET_REAL ERROR: Error reading: ',S
      OK=.FALSE.
      R =0.0
      RETURN
      END

C----------------------------------------------------------------------
C Check header for problems.  Issue warnings.
C mode=1 : issue warnings only to standard output.
C mode=2 : issue warnings AND fix problems AND run "NICE_HEADER" routine.
C mode=3 : issue NO warnings, BUT fix problems AND run "NICE_HEADER" routine.
C
      subroutine check_fits_header(header,mode)
C
      implicit none
C
      character*(*) header
      integer*4 mode
C
      character*80 card
      integer*4 i,j,k,lc
      logical again
C
C Check.
      if ((mode.lt.1).or.(mode.gt.3)) then
        print *,'check_fits_header: ERROR: mode must be 1, 2, or 3.'
        return
      endif

Ctemp
      if (mode.ne.1) then
        print *,'check_fits_header: ERROR: Only mode=1 works now.'
        return
      endif
Ctemp

C All normal? (not between 32 and 126 inclusive?)
      i=1
      k=0
      do while(i.lt.len(header))
        j=ichar(header(i:i))
        if ((j.lt.32).or.(j.gt.126)) then
          if (mode.lt.3) then
            print *,'ERROR: Found abnormal character, ichar: ',
     .              j,'  at:',i
            k=k+1
            if (k.gt.100) then
              print *,'Exceeded warning limit (abnormal).'
              i=len(header)
            endif
          endif
        endif
        i=i+1
      enddo

C "=" sign in correct place except for "END" and "HISTORY" and "HIERARC"(ESO)?
      i=1
      k=0
      again=.true.
      do while(again)
        card = header(i:i+79)
C
        if (card(1:4).eq.'END ') then
          again=.false.
          if (mode.lt.3) then
            print *,'NOTE: Found END card at card#',(i/80)+1,
     .              ', Usual max=720.'
          endif
          if (lc(card).gt.3) then
            if (mode.lt.3) then
              print *,'ERROR: Found "END" card with extra chars, card#',
     .                 (i/80)+1
            endif
          endif
          if (lc(header).gt.(i+3)) then
            if (mode.lt.3) then
              print *,'ERROR: Found non-blank chars after "END", card#',
     .                 (i/80)+1
            endif
          endif
        elseif (card(1:7).eq.'HISTORY') then
          continue
        elseif (card(1:7).eq.'HIERARC') then
          continue
        else
          if (card(9:9).ne.'=') then
            if (mode.lt.3) then
              print *,'ERROR: Equal sign not in 9th column, card# ',
     .                 (i/80)+1
              k=k+1
              if (k.gt.100) then
                print *,'Exceeded warning limit (=).'
                i=len(header)
              endif
            endif
          endif
        endif
C
        if (card(1:1).eq.' ') then
          if (mode.lt.3) then
            print *,'ERROR: Keyword not left-justified, card# ',(i/80)+1
            k=k+1
            if (k.gt.100) then
              print *,'Exceeded warning limit (=).'
              i=len(header)
            endif
          endif
        endif
C
        i=i+80
        if (i.ge.len(header)) again=.false.
      enddo
C
      return
      end


C =================================================================

C ============= CLEAN UP HEADER ===================================

C------------------------------------------------------------------------
      SUBROUTINE NICE_HEADER(HEADER)
C
C This routine "cleans-up" FITS headers:
C   (1) Change any abnormal ASCII characters (decimal value not between
C          32 and 126 (inclusive)) to a blank space (ASCII 32).
C   (2) Insure that there is a proper END card and that it is the last
C          non-blank header card.
C   (3) Delete all blank header cards before the END card.
C   (4) Delete all "blank" COMMENT cards.
C   (5) Issue warnings for cards (other than END card) without an "=" in
C          the 9th column.
C   (6) Issue warnings for cards whose name is not left-justified and is
C          not all capitals.
C
      CHARACTER*(*) HEADER
      CALL ALL_NORMAL(HEADER)
      CALL PUT_END_CARD(HEADER)
      CALL REMOVE_BLANK_LINES(HEADER)
      CALL REMOVE_BLANK_COMMENTS(HEADER)
      CALL HEADER_WARNINGS(HEADER)
      RETURN
      END

C------------------------------------------------------------------------
C Changes all abnormal characters (not between 32 and 126 inclusive)
C to a blank space.
      SUBROUTINE ALL_NORMAL(C)
      IMPLICIT NONE
      CHARACTER*(*) C
      INTEGER*4 I,J
      DO I=1,LEN(C)
        J=ICHAR(C(I:I))
        IF ((J.LT.32).OR.(J.GT.126)) C(I:I)=' '
      ENDDO
      RETURN
      END

C------------------------------------------------------------------------
      SUBROUTINE PUT_END_CARD(HEADER)
      IMPLICIT NONE
      CHARACTER*(*) HEADER
      INTEGER*4 J,K,HLEN

      HLEN=LEN(HEADER)
      IF (MOD(HLEN,80).NE.0) THEN
        PRINT *,'Error in routine PUT_END_CARD.'
        PRINT *,'FITS header string length is not even multiple of 80.'
        RETURN
      ENDIF
      K=HLEN-79  ! first character of last card slot

C Work our way up towards the first nonblank header card
      J=0
      DO WHILE(K.GT.1)
        IF (HEADER(K:K+9).EQ.'          ') THEN
          K=K-80
        ELSE
          J=K
          K=0     ! stop loop
        ENDIF
      ENDDO
      IF (J.EQ.0) THEN
        PRINT *,'Error in routine PUT_END_CARD.'
        PRINT *,'Apparently header is completely empty.'
        call exit(0)
      ENDIF
C If last card is not a proper END card add one.
      IF (HEADER(J:J+7).NE.'END     ') THEN
        J=MIN(HLEN-79,J+80)     ! do not want to run off the end of string
        HEADER(J:J+79)=' '      ! put END card even if we have to erase the
        HEADER(J:J+2)='END'     ! last card
      ENDIF
      RETURN
      END

C------------------------------------------------------------------------
C Remove any blank cards before the END statement, and removes ANY cards
C after the END statement.
C
      SUBROUTINE REMOVE_BLANK_LINES(HEADER)
      IMPLICIT NONE
      CHARACTER*(*) HEADER
      CHARACTER*80 BLANK80
      INTEGER*4 I,K
      BLANK80=' '
      K=LEN(HEADER)
      I=1
      DO WHILE (HEADER(I:I+4).NE.'END  ')
        IF (HEADER(I:I+7).EQ.'        ') THEN
          HEADER = HEADER(1:I-1)//HEADER(I+80:K)//BLANK80
        ELSE
          I=I+80
        ENDIF
      ENDDO
      I=I+80
      DO WHILE ((I+79).LE.K)
        HEADER(I:I+79)=BLANK80
        I=I+80
      ENDDO
      RETURN
      END

C------------------------------------------------------------------------
C Remove any blank COMMENT cards, which are useless.
      SUBROUTINE REMOVE_BLANK_COMMENTS(HEADER)
      IMPLICIT NONE
      CHARACTER*(*) HEADER
      CHARACTER BLANK80*80, BADCARD*40
      INTEGER*4 I,K
      BLANK80=' '
      BADCARD='COMMENT = '//CHAR(39)//'                             '
      K=LEN(HEADER)
      I=1
      DO WHILE (HEADER(I:I+4).NE.'END  ')
        IF (HEADER(I:I+39).EQ.BADCARD) THEN
          HEADER = HEADER(1:I-1)//HEADER(I+80:K)//BLANK80
        ELSE
          I=I+80
        ENDIF
      ENDDO
      RETURN
      END

C------------------------------------------------------------------------
C Issue warnings for cards without an "=" in the 9th column 
C  (except HISTORY and HIERARC).
C Issue warnings for cards whose name is not left-justified. 
      SUBROUTINE HEADER_WARNINGS(HEADER)
      IMPLICIT NONE
      CHARACTER*(*) HEADER
      INTEGER*4 I,NOT_EQ,NOT_LJ
      NOT_EQ=0
      NOT_LJ=0
      I=1
      DO WHILE (HEADER(I:I+4).NE.'END  ')
        IF ((HEADER(I+8:I+8).NE.'=').AND.(HEADER(I:I+6).NE.'HISTORY')
     .                              .AND.(HEADER(I:I+6).NE.'HIERARC')
     .                              .AND.(HEADER(I:I+6).NE.'COMMENT'))
     .           NOT_EQ = NOT_EQ + 1
        IF (HEADER(I:I).EQ.' ') NOT_LJ = NOT_LJ + 1
        I=I+80
      ENDDO
      IF (NOT_EQ.GT.0)
     . PRINT *,'HEADER_WARNING: Cards found without proper "=" :',NOT_EQ
      IF (NOT_LJ.GT.0)
     . PRINT *,'HEADER_WARNING: Card names not left-justified :',NOT_LJ
      RETURN
      END

C =================================================================


C =========== READ CARDS IN FROM HEADER ===============================

C----------------------------------------------------------------------
C Get a card from a FITS header.  Returns .true. if card exists in header.
C
      LOGICAL FUNCTION GETCARD(CNAME,HEADER,CARD)

      IMPLICIT NONE
      CHARACTER*(*) CNAME          ! name of card   (input)
      CHARACTER*(*) HEADER         ! FITS header    (input)
      CHARACTER*80  CARD           ! header card    (output)
      INTEGER*4 I,K,GETPOS
      I = GETPOS(CNAME,HEADER)
      IF (I.LT.1) THEN
        CARD = ' '
        GETCARD = .FALSE.
      ELSE
        K = 1 + (I-1)*80
        CARD = HEADER(K:K+79)
        GETCARD = .TRUE.
      ENDIF
      RETURN
      END

C----------------------------------------------------------------------
C Finds logical value from a FITS header.
C
      LOGICAL FUNCTION LHEAD(CNAME,HEADER)

      IMPLICIT NONE
      CHARACTER*(*) CNAME         ! name of card
      CHARACTER*(*) HEADER        ! FITS header
      CHARACTER CARD*80
      LOGICAL GETCARD
      LHEAD=.false.
      IF (GETCARD(CNAME,HEADER,CARD)) THEN
        call upper_case(card)
        LHEAD = (index(card(9:80),'F').eq.0)
      ENDIF
      RETURN
      END

C----------------------------------------------------------------------
C Finds integer value from a FITS header.
C Returns -12345678 if card found but could not read value.
C Returns -22345678 if card could not be found.
C
      INTEGER*4 FUNCTION INHEAD(CNAME,HEADER)

      IMPLICIT NONE
      CHARACTER*(*) CNAME         ! name of card
      CHARACTER*(*) HEADER        ! FITS header
      CHARACTER CARD*80
      REAL*8 R
      LOGICAL OK,GETCARD

      IF (GETCARD(CNAME,HEADER,CARD)) THEN
        CALL DISSECT_VALUE(CARD(9:80),1,R,OK)
        IF (OK) THEN
          INHEAD = NINT(R)
        ELSE
          PRINT *,'Could read value from card:'
          PRINT *,CARD(1:80)
          INHEAD = -12345678
        ENDIF
      ELSE
CCC        PRINT *,'Could not find card in header:'
CCC        PRINT *,CNAME
        INHEAD = -22345678
      ENDIF
      RETURN
      END

C----------------------------------------------------------------------
C Finds real value from a FITS header.
C Returns -1.D+35 if card found but could not read value.
C Returns -2.D+35 if card could not be found.
C
      REAL*8 FUNCTION FHEAD(CNAME,HEADER)

      IMPLICIT NONE
      CHARACTER*(*) CNAME         ! name of card
      CHARACTER*(*) HEADER        ! FITS header
      CHARACTER CARD*80
      REAL*8 R
      LOGICAL OK,GETCARD

      IF (GETCARD(CNAME,HEADER,CARD)) THEN
        CALL DISSECT_VALUE(CARD(9:80),1,R,OK)
        IF (OK) THEN
          FHEAD = R
        ELSE
          PRINT *,'Could read value from card:'
          PRINT *,CARD(1:80)
          FHEAD = -1.D+35
        ENDIF
      ELSE
CCC        PRINT *,'Could not find card in header:'
CCC        PRINT *,CNAME
        FHEAD = -2.D+35
      ENDIF
      RETURN
      END

C----------------------------------------------------------------------
C Finds character string value from a FITS header.
C Returns a blank string if card not found.
C
      CHARACTER*80 FUNCTION CHEAD(CNAME,HEADER)

      IMPLICIT NONE
      CHARACTER*(*) CNAME       ! name of card  (input)
      CHARACTER*(*) HEADER      ! FITS header   (input)
      CHARACTER CARD*80
      INTEGER*4 I,J
      LOGICAL GETCARD
      CHEAD = ' '
      IF (GETCARD(CNAME,HEADER,CARD)) THEN
        I = INDEX(CARD(9:80),CHAR(39)) + 8
        IF ((I.GT.0) .AND. (I.LT.80)) THEN
          J = INDEX(CARD(I+1:80),CHAR(39)) + I
          CHEAD(1:J-I-1) = CARD(I+1:J-1)
        ENDIF
      ENDIF
      RETURN
      END

C =================================================================

C =========== WRITE CARDS INTO HEADER =============================

C----------------------------------------------------------------------
C Returns card position of a card or the position of a blank card which
C the routine will create before the END.
C In the case of "COMMENT" or "HISTORY", a new position is always created.
C
      integer*4 function getspace(cname,header)
C
      implicit none
      character*(*)   cname        ! FITS card name  (input)
      character*(*)   header       ! FITS header     (input/output)
C
      integer*4  posend,k,getpos
C
      if ((cname(1:7).ne.'COMMENT').and.(cname(1:7).ne.'HISTORY')) then
        getspace = getpos(cname,header)
        if (getspace.gt.0) return
      endif
1     posend = getpos('END',header)    ! Need to create a blank spot find END
      if (posend.lt.1) then
        call nice_header(header)
        goto 1
      endif
      if (posend.eq.(len(header)/80)) then
        print *,'No more room in header.'
        getspace = -1
        return
      endif
      k = (80*(posend)) + 1      ! Header position AFTER END card
      header(k:k+79) = ' '
      header(k:k+2)  = 'END'
      getspace = posend
      return
      end

C----------------------------------------------------------------------
C Change or create a logical header card in FITS header.
C
      SUBROUTINE LHEADSET(CNAME,LPARAM,HEADER)

      IMPLICIT NONE
      CHARACTER*(*) CNAME              ! card name          (input)
      LOGICAL       LPARAM             ! Logical Parameter  (input)
      CHARACTER*(*) HEADER             ! FITS header        (input/output)
      INTEGER*4 GETSPACE,LC,K,LASTCHAR
      K = 1 + ( 80* ( GETSPACE(CNAME,HEADER) - 1 ) )
      IF (K.LT.1) RETURN
      LC = LASTCHAR(CNAME)
      HEADER(K:K+79) = ' '
      HEADER(K:K+LC-1) = CNAME(1:LC)
      HEADER(K+8:K+8)  = '='
      if (LPARAM) then
        WRITE(UNIT=HEADER(K+29:K+29),FMT='(A1)') 'T'
      else
        WRITE(UNIT=HEADER(K+29:K+29),FMT='(A1)') 'F'
      endif
      RETURN
      END

C----------------------------------------------------------------------
C Change or create an integer header card in FITS header.
C
      SUBROUTINE INHEADSET(CNAME,INPARAM,HEADER)

      IMPLICIT NONE
      CHARACTER*(*) CNAME              ! card name          (input)
      INTEGER*4     INPARAM            ! Integer Parameter  (input)
      CHARACTER*(*) HEADER             ! FITS header        (input/output)
      INTEGER*4 GETSPACE,LC,K,LASTCHAR
      K = 1 + ( 80* ( GETSPACE(CNAME,HEADER) - 1 ) )
      IF (K.LT.1) RETURN
      LC = LASTCHAR(CNAME)
      HEADER(K:K+79) = ' '
      HEADER(K:K+LC-1) = CNAME(1:LC)
      HEADER(K+8:K+8)  = '='
      WRITE(UNIT=HEADER(K+10:K+29),FMT='(I20)') INPARAM
      RETURN
      END

C----------------------------------------------------------------------
C Change or create a real*8 header card in FITS header.
C
      SUBROUTINE FHEADSET(CNAME,FPARAM,HEADER)

      IMPLICIT NONE
      CHARACTER*(*) CNAME              ! card name          (input)
      REAL*8        FPARAM             ! Real*8 Parameter   (input)
      CHARACTER*(*) HEADER             ! FITS header        (input/output)
      INTEGER*4 GETSPACE,LC,K,LASTCHAR
      K = 1 + ( 80* ( GETSPACE(CNAME,HEADER) - 1 ) )
      IF (K.LT.1) RETURN
      LC = LASTCHAR(CNAME)
      HEADER(K:K+79) = ' '
      HEADER(K:K+LC-1) = CNAME(1:LC)
      HEADER(K+8:K+8)  = '='
      WRITE(UNIT=HEADER(K+10:K+29),FMT='(1PE20.12)') FPARAM
      RETURN
      END

C----------------------------------------------------------------------
C Change or create a character header card in FITS header.
C
      subroutine cheadset(cname,cparam,header)
C
      implicit none
C
      character*(*) cname             ! card name          (input)
      character*(*) cparam            ! Character string Parameter  (input)
      character*(*) header            ! FITS header        (input/output)
      integer*4 getspace,lc,lastchar,k
C
      k = 1 + ( 80* ( getspace(cname,header) - 1 ) )
      if (k.lt.1) return
      lc = lastchar(cname)
      header(k:k+79) = ' '
      header(k:k+lc-1) = cname(1:lc)
      header(k+8:k+8)  = '='
      lc = min(lastchar(cparam),68)    ! maximum length of string is 68
      header(k+10:k+10) = char(39)     ! apostrophe
      header(k+11:k+10+lc) = cparam(1:lc)
      header(k+11+lc:k+11+lc) = char(39)
C
      return
      end

C----------------------------------------------------------------------
C Change or create a character header card in FITS header.
C Fill spaces such that trailing apostrophe is after 68 characters.
C
      subroutine cheadset_fill(cname,cparam,header)

      implicit none
      character*(*) cname             ! card name          (input)
      character*(*) cparam            ! Integer Parameter  (input)
      character*(*) header            ! FITS header        (input/output)
      integer*4 getspace,lc,lastchar,k
      k = 1 + ( 80* ( getspace(cname,header) - 1 ) )
      if (k.lt.1) return
      lc = lastchar(cname)
      header(k:k+79) = ' '
      header(k:k+lc-1) = cname(1:lc)
      header(k+8:k+8)  = '='
      lc = min(lastchar(cparam),68)    ! maximum length of string is 68
      header(k+10:k+10) = char(39)     ! apostrophe
      header(k+11:k+10+lc) = cparam(1:lc)
      header(k+11+68:k+11+68) = char(39)
      return
      end

C ==============================================================

C ============== MISCELLANEOUS HEADER STUFF ====================

C----------------------------------------------------------------------
C Removes a header card from a FITS header.

      SUBROUTINE UNFIT(CNAME,HEADER)

      IMPLICIT NONE
      CHARACTER*(*)  CNAME        ! name of FITS header card  (input)
      CHARACTER*(*)  HEADER       ! FITS header               (input/output)
      CHARACTER*80 BLANK80
      INTEGER*4 I,GETPOS,K,LH,LASTCHAR

      IF (CNAME(1:LASTCHAR(CNAME)).EQ.'END') THEN
        PRINT *,'Cannot delete END card - cleaning up header.'
        CALL NICE_HEADER(HEADER)
        RETURN
      ENDIF
      I = GETPOS(CNAME,HEADER)
      IF (I.GT.0) THEN            ! card appears in header
        K = 1 + (80*(I-1))        ! position in header string
        BLANK80 = ' '
        LH= LEN(HEADER)
        IF (K.EQ.1) THEN
          HEADER = HEADER(K+80:LH)//BLANK80
        ELSE
          HEADER = HEADER(1:K-1)//HEADER(K+80:LH)//BLANK80
        ENDIF
      ENDIF
      RETURN
      END

C----------------------------------------------------------------------
C Returns card position of a card.  Returns -1 if card is not in header.
C
      integer*4 function getpos(cname,header)
C
      implicit none
      character*(*) cname       ! name of FITS card          (input)
      character*(*) header      ! FITS Header                (input)
      character*8 name
      integer*4   i,k,n,lastchar,lc
C
      lc = lastchar(cname)
      if (lc.gt.8) then
        print *,'Card name too long: ',cname(1:min(len(cname),40))
        getpos = -1
        return
      endif
      name = ' '
      name(1:lc) = cname(1:lc)
      n = len(header)/80
      i = 1
      do while(i.le.n)
        k = 1 + ((i-1)*80)
        if (header(k:k+7).eq.name) then
          getpos = i
          return
        endif
        i = i + 1
      enddo
      getpos = -1
      return
      end


C----------------------------------------------------------------------
C Makes a very basic 2 dimensional pixel image FITS header.
      SUBROUTINE MAKE_BASIC_HEADER(HEADER,SC,NC,SR,NR)
      IMPLICIT NONE
      CHARACTER*(*) HEADER  ! FITS header                         (output)
      INTEGER*4 SC,NC       ! starting column, number of columns  (input)
      INTEGER*4 SR,NR       ! starting row, number of rows        (input)

      HEADER = ' '
      IF (LEN(HEADER).LT.2880) THEN
        PRINT *,'Header string should be at least 2880 bytes.'
        RETURN
      ENDIF
C                       ----+----1----+----2----+----3
      HEADER(001:030)= 'SIMPLE  =                    T'
      HEADER(081:110)= 'BITPIX  =                  -32'
      HEADER(161:190)= 'NAXIS   =                    2'
      HEADER(241:243)= 'END'
      CALL INHEADSET('NAXIS1',NC,HEADER)
      CALL INHEADSET('NAXIS2',NR,HEADER)
      CALL INHEADSET('CRVAL1',SC,HEADER)
      CALL INHEADSET('CRVAL2',SR,HEADER)
      CALL INHEADSET('CDELT1',1,HEADER)
      CALL INHEADSET('CDELT2',1,HEADER)
      CALL INHEADSET('CRPIX1',1,HEADER)
      CALL INHEADSET('CRPIX2',1,HEADER)
      CALL  CHEADSET('CTYPE1','PIXEL',HEADER)
      CALL  CHEADSET('CTYPE2','PIXEL',HEADER)
      RETURN
      END

C------------------------------------------------------------------------------

      SUBROUTINE SET_FITS_SCALE(A,HEADER,BITPIX,BZERO,BSCALE,AUTO)

C Sets header cards BITPIX,BZERO,BSCALE,DATATYPE in HEADER.  Will calculate
C optimal BZERO and BSCALE if AUTO=.true. and BITPIX=16 or 32.
C For real*4 use: "call set_fits_scale(0.,header,-32,0.,0.,.false.)".
C
      IMPLICIT NONE
      REAL*4     A(*)          ! data array                  (input)
      CHARACTER*(*) HEADER     ! FITS header                 (input/output)
      INTEGER*4  BITPIX        ! bits per pixel (16,32,-32)  (input)
      REAL*8     BZERO,BSCALE  ! scaling parameters          (input/output)
      LOGICAL    AUTO          ! .true. if auto-scaling      (input)
      INTEGER*4 NPIX, INHEAD, I
      REAL*4 HIGH,LOW
      REAL*8 FITSMAX,FITSMIN,PIXELMIN,PIXELMAX

      IF (BITPIX.EQ.-32) THEN
        CALL INHEADSET('BITPIX',-32,HEADER)
        CALL CHEADSET('DATATYPE','REAL*4',HEADER)
        CALL UNFIT('BZERO',HEADER)
        CALL UNFIT('BSCALE',HEADER)
      ELSEIF ((BITPIX.EQ.8).OR.(BITPIX.EQ.16).OR.(BITPIX.EQ.32)) THEN
        IF (BITPIX.EQ.8) THEN
          CALL INHEADSET('BITPIX',8,HEADER)
          CALL CHEADSET('DATATYPE','INTEGER*1',HEADER)
        ELSEIF (BITPIX.EQ.16) THEN
          CALL INHEADSET('BITPIX',16,HEADER)
          CALL CHEADSET('DATATYPE','INTEGER*2',HEADER)
        ELSE
          CALL INHEADSET('BITPIX',32,HEADER)
          CALL CHEADSET('DATATYPE','INTEGER*4',HEADER)
        ENDIF
        IF (AUTO) THEN
          NPIX = MAX(1,INHEAD('NAXIS1',HEADER)) * 
     .           MAX(1,INHEAD('NAXIS2',HEADER))
          LOW = A(1)
          HIGH= A(1)
          DO I=1,NPIX
            IF (A(I).GT.HIGH) HIGH= A(I)
            IF (A(I).LT.LOW)  LOW = A(I)
          ENDDO
          PIXELMIN = DBLE(LOW)
          PIXELMAX = DBLE(HIGH)
          FITSMAX = ( 2.D0 ** (BITPIX-1) ) - 1
          FITSMIN = -1 * FITSMAX
          BZERO=(PIXELMIN*FITSMAX-PIXELMAX*FITSMIN)/(FITSMAX-FITSMIN)
          IF (HIGH.EQ.LOW) THEN
            BSCALE = 1.D0
          ELSE
            BSCALE = (PIXELMAX - PIXELMIN) / (FITSMAX - FITSMIN)
          ENDIF
          PRINT *,'Auto-scaling using:'
          PRINT *,'  ZERO = ',BZERO
          PRINT *,'  SCALE= ',BSCALE
          PRINT *,'  DISK = (TRUE - ZERO ) / SCALE'
        ENDIF
        CALL FHEADSET('BZERO',BZERO,HEADER)
        CALL FHEADSET('BSCALE',BSCALE,HEADER)
      ELSE
        PRINT *,'*** ERROR: Illegal BITPIX = ',BITPIX
        PRINT *,'*** Must be 8, 16, 32, or -32.'
      ENDIF
      RETURN
      END

C------------------------------------------------------------------------------
      SUBROUTINE GET_FITS_SCALE(HEADER,BITPIX,BZERO,BSCALE)
C Sets parameters BITPIX,BZERO, and BSCALE from HEADER.
      IMPLICIT NONE
      CHARACTER*(*) HEADER     ! FITS header                      (input)
      INTEGER*4 BITPIX         ! Bits per pixel (-32 for real*4)  (output)
      REAL*8 BZERO,BSCALE      ! Zero and scale for integer data  (output)
      REAL*8 FHEAD
      INTEGER*4 INHEAD
      BITPIX= INHEAD('BITPIX',HEADER)
      BZERO = 0.D0
      BSCALE= 0.D0
      IF ((BITPIX.EQ.8).OR.(BITPIX.EQ.16).OR.(BITPIX.EQ.32)) THEN
        BZERO = FHEAD('BZERO',HEADER)
        IF (BZERO.LT.-1.D+34) BZERO = 0.D0    ! card not found - use default
        BSCALE= FHEAD('BSCALE',HEADER)
        IF (BSCALE.LT.-1.D+34) BSCALE = 1.D0  ! card not found - use default
      ELSEIF ((BITPIX.NE.-32).AND.(BITPIX.NE.-64)) THEN
        PRINT *,'*** ERROR: Illegal BITPIX = ',BITPIX
        PRINT *,'*** Must be 8, 16, 32, -32, or -64.'
      ENDIF
      RETURN
      END

C------------------------------------------------------------------------------

      real*8 function getael(x,header)

C Get array element location for a spectrum given a pixel or wavelength value.
C The array element location "ael" is the entry number in the array of values as
C they appear in the FITS file.  For linear wavelength spectra, if CRPIX1=1
C (which it almost always is), then "ael" = pixel number (as seen by Vista).
C For polynomial scale and pixel scale data, "ael"= pixel# - CRVAL1 + CRPIX1 .
C
C You must initialize this function by first calling it with "x" < 0.  This 
C will set the scale type and get the polynomial coeffecients if necessary.  
C The function returns -1. on initialization if there is an error, otherwise
C it returns the x value.
C
      implicit none
      real*8 x
      character*(*) header
      integer*4 j,inhead,getpos,stype,order
      character*80 ctype1,chead,ipoly0,ipoly1
      real*8 crval1,cdelt1,crpix1,coef(0:6),wave0
      real*8 fhead,xx,cpoly
      logical ok
C
      common /getaelBLK/ stype,order,crval1,cdelt1,crpix1,coef,wave0
C

C Initialize.
      getael= -1.d0
      if (x.lt.0.) then

C Identify x-scale type and load parameters.
        if ( (inhead('NAXIS2',header).ne.1).and.
     .       (inhead('NAXIS',header).ne.1) ) then
          call printerr('Error- not a spectrum.')
          goto 990
        endif
        crpix1 = fhead('CRPIX1',header)
        if (crpix1.lt.-9999.) crpix1 = 1.d0
        if (getpos('CRVAL1',header).eq.-1) then
          call printerr('Error finding card CRVAL1.')
          goto 990
        endif
        crval1 = fhead('CRVAL1',header)
        cdelt1 = fhead('CDELT1',header)
        ctype1 = chead('CTYPE1',header)

        if ((ctype1.eq.' ').or.(index(ctype1,'PIXEL').gt.0)) then
          stype = 0

C Vista-style polynomial format (use inverse polynomial.)
        elseif (index(ctype1,'POLY_LAMBDA').gt.0) then
          stype = 1
          order = inhead('LAMORD',header) - 1
          if (order.gt.6) then
            call printerr('Error- Polynomial order > 6 ( LAMORD > 7 ).')
            goto 990
          endif
          ipoly0 = chead('IPOLY0',header)
          call dissect_value(ipoly0,1,wave0,ok)
          if (.not.ok) goto 910
          do j=0,min(2,order)
            call dissect_value(ipoly0,j+2,coef(j),ok)
            if (.not.ok) goto 910
          enddo
          if (order.gt.2) then
            ipoly1 = chead('IPOLY1',header)
            do j=3,order
              call dissect_value(ipoly1,j-2,coef(j),ok)
              if (.not.ok) goto 910
            enddo
          endif 

C Vista-style linear format.
        elseif ((index(ctype1,'LINEAR').gt.0).or.
     .          (index(ctype1,'LAMBDA').gt.0)) then
C Check for log-linear format.
          j = inhead('DC-FLAG',header)  
          if (j.eq.1) then
            stype = 3
          else
            stype = 2
            if (crval1.lt.1.) then
              cdelt1 = cdelt1*1.d+10
              crval1 = crval1*1.d+10
            endif
          endif

C Iraf linear format.
        elseif (index(ctype1,'lambda').gt.0) then
          stype = 2
          cdelt1 = fhead('CD1_1',header)

        else
          call printerr('Unidentifiable x-scale.')
          goto 990

        endif

        getael = 0.d0
        goto 900

      endif

C Calculate ael value.
      if (stype.eq.0) then
C       getx = crval1 + ( ael - crpix1 )
        getael = (x-crval1) + crpix1
      elseif (stype.eq.1) then
C       x =  crval1 + ( ael - crpix1 ) - pix0
C       getx = cpoly(x,order,coef)
        xx = x - wave0
        getael = cpoly(xx,order,coef)
      elseif (stype.eq.2) then
C       getx = crval1 + ( cdelt1 * ( ael - crpix1 ) )
        getael = ( (x-crval1) / cdelt1 ) + crpix1
      elseif (stype.eq.3) then
C       getx = 10.d0 ** ( crval1 + ( cdelt1 * ( ael - crpix1 ) ) )
        getael = ( ( log10(x) - crval1 ) / cdelt1 ) + crpix1
      else
        call printerr('Previous error disables getael calculation.')
        getael = -1.d0
      endif

900   continue
      return

910   continue
      print *,'Error reading polynomial scale.'
      return

990   continue
      getael = -1.d0
      return
      end


C------------------------------------------------------------------------------

      real*8 function getx(ael,header)

C Get pixel or wavelength value for a spectrum given an array location.  The
C array element location "ael" is the entry number in the array of values as
C they appear in the FITS file.  For linear wavelength spectra, if CRPIX1=1
C (which it almost always is), then "ael" = pixel number (as seen by Vista).
C For polynomial scale and pixel scale data, "ael"= pixel# - CRVAL1 + CRPIX1 .
C
C You must initialize this function by first calling it with "ael" = -1.  This 
C will set the scale type and get the polynomial coeffecients if necessary.  
C
C The function returns -1.e+30 if there is an error.
C
      implicit none
      real*8 ael
      character*(*) header
      integer*4 j,order,inhead,stype
      character*80 lc_card,card,lpoly0,lpoly1,chead
      real*8 crval1,cdelt1,coef(0:6),pix0,crpix1
      real*8 cpoly,x,fhead
      logical ok
C
      common /getxBLK/ order,stype,crval1,cdelt1,coef,pix0,crpix1
C
C Initialize.
      if (ael.lt.0.) then

C Identify x-scale type and load parameters.
        if ( (inhead('NAXIS2',header).ne.1).and.
     .       (inhead('NAXIS',header).ne.1) ) then
          call printerr('Error- not a spectrum.')
          goto 990
        endif
C Get basic parameters.  Set to 1 if not found.
        crpix1 = fhead('CRPIX1',header)
        if (crpix1.lt.-999000.) crpix1 = 1.d0
        crval1 = fhead('CRVAL1',header)
        if (crval1.lt.-999000.) crval1 = 1.d0
        cdelt1 = fhead('CDELT1',header)
        if (cdelt1.lt.-999000.) then
          cdelt1 = fhead('CD1_1',header)
          if (cdelt1.lt.-999000.) cdelt1 = 1.d0
        endif
C Get scale type.
        card = chead('CTYPE1',header)
        lc_card = card
        call lower_case(lc_card)
C Set values. 
        if (index(card,'POLY_LAMBDA').gt.0) then
          stype = 1
          order = inhead('LAMORD',header) - 1
          if (order.gt.6) then
            call printerr('Error- Polynomial order > 6 ( LAMORD > 7 ).')
            goto 990
          endif
          lpoly0 = chead('LPOLY0',header)
          call dissect_value(lpoly0,1,pix0,ok)
          if (.not.ok) goto 910
          do j=0,min(2,order)
            call dissect_value(lpoly0,j+2,coef(j),ok)
            if (.not.ok) goto 910
          enddo
          if (order.gt.2) then
            lpoly1 = chead('LPOLY1',header)
            do j=3,order
              call dissect_value(lpoly1,j-2,coef(j),ok)
              if (.not.ok) goto 910
            enddo
          endif 

        elseif ((index(card,'LINEAR').gt.0).or.
     .          (index(card,'LAMBDA').gt.0)) then
C Check for log-linear format.
          j = inhead('DC-FLAG',header)  
          if (j.eq.1) then
            stype = 3
          else
            stype = 2
            if (abs(crval1).lt.1.) then
              cdelt1 = cdelt1*1.d+10
              crval1 = crval1*1.d+10
            endif
          endif

        elseif (index(card,'lambda').gt.0) then
          stype = 2
          cdelt1 = fhead('CD1_1',header)

        elseif (index(lc_card,'pixel').gt.0) then
          stype = -1

        else
          stype = -2

        endif

C Return after initialization.
        getx=0
        return

      endif

C Calculate x value.
      if (stype.eq.-2) then
        getx = ael
      elseif (stype.eq.-1) then
        getx = crval1 + ( ael - crpix1 )
      elseif (stype.eq.1) then
        x =  crval1 + ( ael - crpix1 ) - pix0
        getx = cpoly(x,order,coef)
      elseif (stype.eq.2) then
        getx = crval1 + ( cdelt1 * ( ael - crpix1 ) )
      elseif (stype.eq.3) then
        getx = 10.d0 ** ( crval1 + ( cdelt1 * ( ael - crpix1 ) ) )
      else
        call printerr('getx: Unknown "stype"-- did you initialize?')
        getx = -1.d+30
      endif

      return

910   continue
      call printerr('Error getting polynomial coeffecients.')

990   continue
      getx = -1.d+30
      stype = -999
      return
      end

C------------------------------------------------------------------------------
      real*8 function cpoly(x,order,coef)
      implicit none
      real*8 x,coef(0:*),r
      integer*4 order,i
      r = 0.
      do i=order,1,-1
        r = x * ( coef(i) + r )
      enddo
      cpoly = r + coef(0)
      return
      end

C------------------------------------------------------------------------------
      real*4 function cpoly4(x,order,coef)
      implicit none
      real*4 x,coef(0:*),r
      integer*4 order,i
      r = 0.
      do i=order,1,-1
        r = x * ( coef(i) + r )
      enddo
      cpoly4 = r + coef(0)
      return
      end

C---------------------------------------------------------------------------
C Obtains Universal date, time, epoch, and 1950 coordinate designation.
C Returns .false. if problems.
C
      logical function datecoord(hdr,udate,utime,epoch,coord,
     .                           coord0,rashift)

      implicit none
      character*(*) hdr                 ! FITS header                (input)
      character udate*7,utime*5,coord*8 ! UT Date, time, 1950 desig. (output)
      character coord0*8                ! unprecessed coordinates    (output)
      real*4 epoch                      ! epoch (e.g. 1990.3428)     (output)
      real*4 rashift                    ! guider shift to east in "  (input)
      character*80 chead, temp
      character month(12)*3, cc1*2, cc2*2, cc3*2, c1*1
      real*4 ra,dec,ddays,adec
      real*8 fhead, ra8, dec8, year_start, year_end, cosdd
      integer*4 day,mm,yy,i,ndays(12),hr,minute,fc,lc,year4

      data MONTH / 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL',
     .             'AUG', 'SEP', 'OCT', 'NOV', 'DEC' /
      data NDAYS / 0,31,59,90,120,151,181,212,243,273,304,334 /

      udate = ' '
      utime = ' '
      coord = ' '
      coord0= ' '
      epoch = -1

C Get DAY,MM,YY
      call ReadFitsDate('DATE-OBS',hdr,year4,yy,mm,day)

C Create UDATE
      write(udate,'(i2.2,a3,i2.2)') day,month(mm),yy

C Create UTIME
      temp = chead('TIME',hdr)
      if (temp.eq.' ') temp = chead('UT',hdr)
      call strpos(temp,fc,lc)
      if (temp(fc+1:fc+1).eq.':') then
        utime='0'//temp(fc:fc+3)
      else
        utime=temp(fc:fc+4)
      endif

C Create epoch and 1950.0 coordinate designation
      ra8  = fhead('RA',hdr)
      dec8 = fhead('DEC',hdr)
      if ((yy.lt.40).OR.(yy.GT.99)) goto 990
      read(utime,'(i2,1x,i2)') hr,minute
      ddays=float(ndays(mm)+day-1) + (float(hr)/24.) + 
     .                               (float(minute)/1440.)
      if (mod(yy,4).eq.0) then        ! leap year
        if (mm.gt.2) ddays=ddays+1
        ddays=ddays/366.
      else
        ddays=ddays/365.
      endif
      epoch=float(1900+yy)+ddays     ! create epoch
      year_start= dble(epoch)
      year_end  = 1950.d0

C First do without precession or RA shift
      dec= sngl(dec8)
      ra = sngl(ra8)
C      
C Now set HHMM+DDD notation, in this notation TRUNCATION is used instead of
C  rounding-off. Examples:
C 12h 34m 58.3s  +12 13' 53''  becomes  1234+121 .not. 1235+122 or 1234+122
C 09h 10m 01.2s  -34 05' 10''  becomes  0910-340 .not. 0910-341
C 01h 59m 59.9s  +02 43' 19''  becomes  0159+027 .not. 0200+027
C
      adec   = abs(dec)
      write(cc1,'(i2.2)') int(ra)
      i = min(59,max(0,int((ra-float(int(ra)))*60.0)))
      write(cc3,'(i2.2)') i
      write(cc2,'(i2.2)') int(adec)
      i = min(9,max(0,int( ( adec-float(int(adec)) )*10.0 ) ))
      write(c1,'(i1)')  i
      if (dec.lt.0.) then
        coord0 = cc1//cc3//'-'//cc2//c1
      else
        coord0 = cc1//cc3//'+'//cc2//c1
      endif

C Now with precession and RA shift
      call precess(year_start,year_end,ra8,dec8)
      dec= sngl(dec8)
      ra = sngl(ra8) - ( rashift / (3600.*15.*cosdd(dec)) )
      adec = abs(dec)
      write(cc1,'(i2.2)') int(ra)
      i = min(59,max(0,int((ra-float(int(ra)))*60.0)))
      write(cc3,'(i2.2)') i
      write(cc2,'(i2.2)') int(adec)
      i = min(9,max(0,int( ( adec-float(int(adec)) )*10.0 ) ))
      write(c1,'(i1)')  i
      if (dec.lt.0.) then
        coord = cc1//cc3//'-'//cc2//c1
      else
        coord = cc1//cc3//'+'//cc2//c1
      endif

      datecoord=.true.
      return

990   call printerr('Error within datecoord subroutine.')
      datecoord=.false.
      return
      end

C---------------------------------------------------------------------------
C Converts an epoch into a day of year, e.g. 12FEB92.
C
      subroutine epoch_to_doy(epoch,udate)

      implicit none
      real*8 epoch                  ! epoch (e.g. 1990.3428)         (input)
      character udate*7             ! Universal date (e.g. 07MAR91)  (output)
      character month(12)*3
      integer*4 day,mm,yr,i,ndays(12),ndays_ly(12),ddays
      real*8 fy

      data MONTH    / 'JAN','FEB','MAR','APR','MAY','JUN','JUL',
     .                'AUG','SEP','OCT','NOV','DEC' /
      data NDAYS    / 0,31,59,90,120,151,181,212,243,273,304,334 /
      data NDAYS_LY / 0,31,60,91,121,152,182,213,244,274,305,335 /

C Create UDATE
      mm=0
      udate = ' '
      yr = int(epoch - 1900.d0)
      fy = epoch - dfloat(int(epoch))
      if (mod(yr,4).eq.0) then
        ddays = int(fy*366.d0)
        i=12
        do while(i.gt.0)
          if (ddays.ge.ndays_ly(i)) then 
            mm = i
            day= ( ddays - ndays_ly(i) ) + 1
            i = 0
          else
            i = i - 1
          endif
        enddo    
      else 
        ddays = int(fy*365.d0)
        i=12
        do while(i.gt.0)
          if (ddays.ge.ndays(i)) then 
            mm = i
            day= ( ddays - ndays(i) ) + 1
            i = 0
          else
            i = i - 1
          endif
        enddo    
      endif

      write(udate,'(i2.2,a3,i2.2)',err=990) day,month(mm),yr

      return

990   call printerr('Error within epoch_to_doy subroutine.')
      return
      end

C ==============================================================

C---------------------------------------------------------------------------
C Obtains Universal date, time, epoch, and 1950 coordinate designation.
C Returns .false. if problems.
C
C THIS VERSION ALLOWS A DECLINATION SHIFT AS WELL.
C
      logical function datecoord2(hdr,udate,utime,epoch,coord,coord0,
     .                           rashift,decshift)

      implicit none
      character*(*) hdr                 ! FITS header                (input)
      character udate*7,utime*5,coord*8 ! UT Date, time, 1950 desig. (output)
      character coord0*8                ! unprecessed coordinates    (output)
      real*4 epoch                      ! epoch (e.g. 1990.3428)     (output)
      real*4 rashift                    ! guider shift to east in "  (input)
      real*4 decshift                   ! dec shift north in "       (input)
      character*80 chead, temp
      character month(12)*3, cc1*2, cc2*2, cc3*2, c1*1
      real*4 ra,dec,ddays,adec
      real*8 fhead, ra8, dec8, year_start, year_end, cosdd
      integer*4 day,mm,yy,i,ndays(12),hr,minute,fc,lc,year4

      data MONTH / 'JAN','FEB','MAR','APR','MAY','JUN','JUL',
     .             'AUG','SEP','OCT','NOV','DEC' /
      data NDAYS / 0,31,59,90,120,151,181,212,243,273,304,334 /

      udate = ' '
      utime = ' '
      coord = ' '
      coord0= ' '
      epoch = -1

C Get DAY,MM,YY
      call ReadFitsDate('DATE-OBS',hdr,year4,yy,mm,day)

C Create UDATE
      write(udate,'(i2.2,a3,i2.2)') day,month(mm),yy

C Create UTIME
      temp = chead('TIME',hdr)
      if (temp.eq.' ') temp = chead('UT',hdr)
      call strpos(temp,fc,lc)
      if (temp(fc+1:fc+1).eq.':') then
        utime='0'//temp(fc:fc+3)
      else
        utime=temp(fc:fc+4)
      endif

C Create epoch and 1950.0 coordinate designation
      ra8  = fhead('RA',hdr)
      dec8 = fhead('DEC',hdr)
      if ((yy.lt.40).OR.(yy.GT.99)) goto 990
      read(utime,'(i2,1x,i2)') hr,minute
      ddays=float(ndays(mm)+day-1) + (float(hr)/24.) + 
     .                               (float(minute)/1440.)
      if (mod(yy,4).eq.0) then        ! leap year
        if (mm.gt.2) ddays=ddays+1
        ddays=ddays/366.
      else
        ddays=ddays/365.
      endif
      epoch=float(1900+yy)+ddays     ! create epoch
      year_start= dble(epoch)
      year_end  = 1950.d0

C First do without precession or RA shift
      dec= sngl(dec8)
      ra = sngl(ra8)
C      
C Now set HHMM+DDD notation, in this notation TRUNCATION is used instead of
C  rounding-off. Examples:
C 12h 34m 58.3s  +12 13' 53''  becomes  1234+121 .not. 1235+122 or 1234+122
C 09h 10m 01.2s  -34 05' 10''  becomes  0910-340 .not. 0910-341
C 01h 59m 59.9s  +02 43' 19''  becomes  0159+027 .not. 0200+027
C
      adec   = abs(dec)
      write(cc1,'(i2.2)') int(ra)
      i = min(59,max(0,int((ra-float(int(ra)))*60.0)))
      write(cc3,'(i2.2)') i
      write(cc2,'(i2.2)') int(adec)
      i = min(9,max(0,int( ( adec-float(int(adec)) )*10.0 ) ))
      write(c1,'(i1)')  i
      if (dec.lt.0.) then
        coord0 = cc1//cc3//'-'//cc2//c1
      else
        coord0 = cc1//cc3//'+'//cc2//c1
      endif

C Now with precession and RA shift and DEC shift
      call precess(year_start,year_end,ra8,dec8)
      dec= sngl(dec8) - ( decshift / 3600. )
      ra = sngl(ra8) - ( rashift / (3600.*15.*cosdd(dec)) )
      adec = abs(dec)
      write(cc1,'(i2.2)') int(ra)
      i = min(59,max(0,int((ra-float(int(ra)))*60.0)))
      write(cc3,'(i2.2)') i
      write(cc2,'(i2.2)') int(adec)
      i = min(9,max(0,int( ( adec-float(int(adec)) )*10.0 ) ))
      write(c1,'(i1)')  i
      if (dec.lt.0.) then
        coord = cc1//cc3//'-'//cc2//c1
      else
        coord = cc1//cc3//'+'//cc2//c1
      endif

      datecoord2=.true.
      return

990   call printerr('Error within datecoord2 subroutine.')
      datecoord2=.false.
      return
      end


C----------------------------------------------------------------------
C Window a 2-D FITS image.  The new size is given by sc,ec,sr,er .  The old
C size is given by the header.  The new size cards are added to the header.
C The original data is in a(). The sub-box data image is copied into a().
C
      subroutine window_fits(a,header,sc,ec,sr,er,ok)
C
      implicit none
      real*4 a(*)             ! Data image (input/output)
      character*(*) header    ! FITS header (input/output)
      integer*4 sc,ec,sr,er   ! New image dimensions (input)
      logical ok              ! Success? (output)
C
      integer*4 osc,oec,osr,oer,onc,onr,nc,nr
C
C Original dimensions.
      call GetDimensions(header,osc,oec,osr,oer,onc,onr)
C Transfer (copy) image data.
      call window_fits_transfer(osc,oec,osr,oer,sc,ec,sr,er,a,ok)
      if (.not.ok) return
C New size.
      nc = 1 + ec - sc
      nr = 1 + er - sr
C Set header cards.
      call inheadset('CRVAL1',sc,header)
      call inheadset('CRPIX1',1,header)
      call inheadset('NAXIS1',nc,header)
      call inheadset('CRVAL2',sr,header)
      call inheadset('CRPIX2',1,header)
      call inheadset('NAXIS2',nr,header)
C
      return
      end


C----------------------------------------------------------------------
C Copy subset of data matrix array into same array.
C Transfer a box given by sc,ec,sr,er (starting and ending column, starting
C and ending row) back into a(), loaded such that sc,sr 
C is the first data element.  sc,ec,sr,er must be within original
C dimensions osc,oec,osr,oer.   Set "soun" for error output.
C
      subroutine window_fits_transfer(osc,oec,osr,oer,sc,ec,sr,er,a,ok)
C
      implicit none
      integer*4 osc,oec,osr,oer  ! Original dimensions (input)
      integer*4 sc,ec,sr,er      ! New dimensions (input)
      real*4 a(*)                ! Data array (input/output)
      logical ok                 ! Success? (output)
C
      include 'soun.inc'
C
      integer*4 i,j,onc,nc,newpix,oldpix
C
C Check box size.
      if ( (sc.lt.osc).or.(ec.gt.oec).or.
     .     (sr.lt.osr).or.(er.gt.oer) ) then
        write(soun,'(a,4i6,a,4i6)')
     .         'ERROR: window_fits_transfer: box too big:',
     .          sc,ec,sr,er,' : ',osc,oec,osr,oer
        ok=.false.
        return
      endif
C Check box logical size.
      if ((sc.gt.ec).or.(sr.gt.er)) then
        write(soun,'(a,4i6)')
     .         'ERROR: window_fits_transfer: bad box dimensions:',
     .                             sc,ec,sr,er
        ok=.false.
        return
      endif
C
      onc= 1 + oec- osc
      nc = 1 + ec - sc
      do j=sr,er
        do i=sc,ec
          oldpix= 1 + (i-osc) + ((j-osr)*onc)
          newpix= 1 + (i- sc) + ((j- sr)* nc)
          a(newpix) = a(oldpix)
        enddo
      enddo
C
      ok=.true.
      return
      end


C----------------------------------------------------------------------
C Flip an image either left-right (flip='LR') or top-bottom (flip='TB').
C
      subroutine flip_fits(a,header,flip,ok)
C
      implicit none
C
      real*4 a(*)             ! data image (input/output).
      character*(*) header    ! FITS header (input/output).
      character*(*) flip      ! Either "LR" (left-right, column flip).
                              !     or "TB" (top-bottom, row flip).
      logical ok              ! Success?  (output)
C
      integer*4 sc,ec,sr,er,nc,nr
      integer*4 row,col,pixno1,pixno2
      real*4 temp
C
      include 'soun.inc'
C
C Dimensions of image.
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
C Left-right (column) flip.
      if (flip.eq.'LR') then
        do row=1,nr
          do col=1,nc/2
            pixno1=     col    + ((row-1)*nc)
            pixno2= (1+nc-col) + ((row-1)*nc)
            temp     = a(pixno1)                 ! Swap.
            a(pixno1)= a(pixno2)
            a(pixno2)= temp
          enddo
        enddo
C Top-bottom (row) flip.
      elseif (flip.eq.'TB') then
        do col=1,nc
          do row=1,nr/2
            pixno1= col + (( (  row   ) -1)*nc)
            pixno2= col + (( (1+nr-row) -1)*nc)
            temp     = a(pixno1)                 ! Swap.
            a(pixno1)= a(pixno2)
            a(pixno2)= temp
          enddo
        enddo
C Unknown.
      else
        write(soun,'(3a)')  'ERROR: flip_fits: ',
     .                'flip must be "LR" or "TB". flip=',flip
        ok=.false.
        return
      endif
      ok=.true.
      return
      end


C----------------------------------------------------------------------
C Rotate an image by "angle", where angle must be a multiple of 90, and
C range between -270 and +270.
C
      subroutine rotate_fits(a,b,header,angle,ok)
C
      implicit none
C
      real*4 a(*)             ! data image (input/output).
      real*4 b(*)             ! scratch image (scratch).
      character*(*) header    ! FITS header (input/output).
      real*4 angle            ! Angle to rotate in degrees, clockwise, must
                              !   be between -270 and +270 inclusive, in 
                              !   90 degree increments. (input).
      logical ok              ! Success?  (output)
C
      integer*4 ii,jj,sc,ec,sr,er,nc,nr,irot,nrot,temp
      integer*4 npix,newnc,newnr
      integer*4 row,col,newcol,newrow
      real*4 rr1,rr2 
C
      include 'soun.inc'
C
C Check.
      rr1= angle / 90.
      ii = rr1
      rr2= ii
      if (abs(rr1-rr2).gt.0.000001) then
        write(soun,'(2a,f12.5)')  'ERROR: RotateImage: ',
     .    'Angle must be in increments of 90.  angle=',angle
        ok=.false.
        return
      endif
      if ((angle.gt.270.).or.(angle.lt.-270.)) then
        write(soun,'(2a,f12.5)')  'ERROR: RotateImage: ',
     .    'Angle must be between -270 and +270.  angle=',angle
        ok=.false.
        return
      endif
C Number of rotations.
      nrot = nint(angle / 90.)
C Dimensions of image.
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
      npix = nc*nr
C Rotate image clockwise "nrot" times.
      if (nrot.gt.0) then
        do irot=1,abs(nrot)
          newnc = nr                  ! flipped.
          newnr = nc
          do ii=1,npix
            row= 1 + ((ii-1)/nc)
            col= ii - ((row-1)*nc)
            newcol = row
            newrow = 1 + nc - col
            jj = newcol + (newrow-1)*newnc
            b(jj) = a(ii)
          enddo
          do ii=1,npix
            a(ii) = b(ii)
          enddo
          temp = nc         ! Swap.
          nc   = newnc
          newnc= temp
          temp = nr
          nr   = newnr
          newnr= temp
        enddo
C Rotate image counter-clockwise "nrot" times.
      elseif (nrot.lt.0) then
        do irot=1,abs(nrot)
          newnc = nr                  ! flipped.
          newnr = nc
          do ii=1,npix
            row= 1 + ((ii-1)/nc)
            col= ii - ((row-1)*nc)
            newcol = 1 + nr - row
            newrow = col
            jj = newcol + (newrow-1)*newnc
            b(jj) = a(ii)
          enddo
          do ii=1,npix
            a(ii) = b(ii)
          enddo
          temp = nc         ! Swap.
          nc   = newnc
          newnc= temp
          temp = nr
          nr   = newnr
          newnr= temp
        enddo
      endif
C Reset axes.
      call inheadset('NAXIS1',nc,header)
      call inheadset('NAXIS2',nr,header)
      ok=.true.
      return
      end



C----------------------------------------------------------------------
C Interpolate zero points.  It averages along rows using "npx" good pixels in
C the columns on either side (10 is a typical number) which is 2*npx total.  
C
      subroutine fits_zero_interp(a,sc,ec,sr,er,npx)
      implicit none
      integer*4 sc,ec,sr,er,npx
      real*4 a(sc:ec,sr:er),sum,b(90000)
      integer*4 i,num1,num2,col,row
C Do all rows.
      do row=sr,er
C Copy to temporary array.
        do col=sc,ec
          b(col)=a(col,row)
        enddo
C Search for a zero pixel.
        do col=sc,ec
          if (abs(b(col)).lt.1.e-30) then
C Find a mean value for local valid pixels.
            sum=0.
C Look for valid pixels to the left of region.
            i=col-1
            num1=0
            do while((i.ge.sc).and.(num1.lt.npx))
              if (abs(b(i)).gt.1.e-30) then
                num1=num1+1
                sum =sum +b(i)
              endif
              i=i-1
            enddo
C Look for valid pixels to the right of region.
            i=col+1
            num2=0
            do while((i.le.ec).and.(num2.lt.npx))
              if (abs(b(i)).gt.1.e-30) then
                num2=num2+1
                sum =sum +b(i)
              endif
              i=i+1
            enddo
            sum = sum / float(max(1,num1+num2))
            a(col,row) = sum
          endif
        enddo
      enddo
      return
      end

C----------------------------------------------------------------------
C Grab date from DATE-OBS card in FITS header and put into the format:
C   mode=0 : DDMMMYY , 27MAR95
C   mode=1 : DDMmmYY , 27Mar95
C   mode=2 : YYMmmDD , 95Mar27
C   mode=3 : YYMMDD  , 950327
C   mode=4 : DD-Mmm-YY, 27-Mar-95
C The string "c" should be large enough to contain characters.
C
      subroutine GetFitsDate(header,c,mode)
      implicit none
      character*(*) header,c
      integer*4 mode
      integer*4 day,mon,year,year4
      character*3 MONTH(12),MTHCP(12) 
C
      data MONTH / 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
     .             'Aug', 'Sep', 'Oct', 'Nov', 'Dec' /
      data MTHCP / 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL',
     .             'AUG', 'SEP', 'OCT', 'NOV', 'DEC' /
C
C Initialize.
      c = ' '
C FITS header date.
      call ReadFitsDate('DATE-OBS',header,year4,year,mon,day)
C mode=0 : DDMMMYY , 27MAR95
      if (mode.eq.0) then
        write(c,'(i2.2,a3,i2.2)',err=9) max(0,min(99,day)),
     .        MTHCP(max(1,min(12,mon))),max(0,min(99,year))
C mode=1 : DDMmmYY , 27Mar95
      elseif (mode.eq.1) then
        write(c,'(i2.2,a3,i2.2)',err=9) max(0,min(99,day)),
     .        MONTH(max(1,min(12,mon))),max(0,min(99,year))
C mode=2 : YYMmmDD , 95Mar27
      elseif (mode.eq.2) then
        write(c,'(i2.2,a3,i2.2)',err=9) max(0,min(99,year)),
     .        MONTH(max(1,min(12,mon))),max(0,min(99,day))
C mode=3 : YYMMDD  , 950327
      elseif (mode.eq.3) then
        write(c,'(i2.2,i2.2,i2.2)',err=9) max(0,min(99,year)),
     .                      max(1,min(12,mon)),max(0,min(99,day))
C mode=4 : DD-Mmm-YY, 27-Mar-95
      elseif (mode.eq.4) then
        write(c,'(i2.2,a1,a3,a1,i2.2)',err=9) max(0,min(99,day)),
     .        '-',MONTH(max(1,min(12,mon))),'-',max(0,min(99,year))
      endif
9     return
      end


C----------------------------------------------------------------------
C
      subroutine OLD_GetBinning(header,xbin,ybin)
C
      implicit none
      character*(*) header
      integer*4 xbin,ybin,ibin,jbin,inhead,i
      real*4 valread
      character*80 chead,card
C
      card = chead('BINNING',header)
      i = index(card,',')
      ibin = nint(valread(card(1:i-1)))
      jbin = nint(valread(card(i+1:)))
      xbin = nint(2048./float(inhead('NAXIS1',header)-30))
      ybin = nint(2048./float(inhead('NAXIS2',header)))
      if (ibin.ne.xbin) then
        print *,'WARNING: BINNING card and image size are inconsistent.'
        print *,'WARNING: ibin=',ibin,' xbin=',xbin
      endif
      if (jbin.ne.ybin) then
        print *,'WARNING: BINNING card and image size are inconsistent.'
        print *,'WARNING: jbin=',jbin,' ybin=',ybin
      endif
      return
      end


C----------------------------------------------------------------------
C New version, more general, and ESI and HIRES2 compatible.
C
      subroutine GetBinning(header,xbin,ybin)
C
      implicit none
      character*(*) header
      integer*4 xbin,ybin,lc
      real*8 fgetlinevalue
      character*80 chead,wrd
C
      wrd = chead('BINNING',header)
      if (lc(wrd).gt.1) then
        xbin = max(-1, nint(fgetlinevalue(wrd,1)) )
        ybin = max(-1, nint(fgetlinevalue(wrd,2)) )
      else
        wrd = chead('CCDSUM',header)
        xbin = max(-1, nint(fgetlinevalue(wrd,1)) )
        ybin = max(-1, nint(fgetlinevalue(wrd,2)) )
      endif
C
      return
      end


C----------------------------------------------------------------------
C
      subroutine GetWindow(header,wx1,wy1,wx2,wy2)
C
      implicit none
      character*(*) header
      integer*4 wx1,wx2,wy1,wy2
      real*8 fgetlinevalue
      character*80 chead,wrd
C
      wrd = chead('WINDOW',header)
      wx1= max(0, nint( fgetlinevalue(wrd,2) ) )
      wy1= max(0, nint( fgetlinevalue(wrd,3) ) )
      wx2= max(0, nint( fgetlinevalue(wrd,4) ) )
      wy2= max(0, nint( fgetlinevalue(wrd,5) ) )
C
      return
      end


C----------------------------------------------------------------------
C Get frame time in terms of seconds since 1970.
C
      subroutine HIRES_FrameTime(header,frtime)
C
      implicit none
      character*(*) header
      real*8 frtime
C
      integer*4 year,year2,month,day,t
      real*8 fhead,hour
      character c17*17
C
      call ReadFitsDate('DATE-OBS',header,year,year2,month,day)
      hour = fhead('UT',header)
      write(c17,'(i2.2,1x,i2.2,1x,i2.2,9x)') year2,month,day
      call convert_time_string(c17,t,0)
      frtime = dfloat(t) + (hour*3600.d0)
C
      return
      end


C----------------------------------------------------------------------
C Read a DATE card from a FITS header in either the old format: DD/MM/YY.   
C Or the new format: yyyy-mm-dd  or yyyy-mm-ddThh:mm:ss .
C Return 0,0,0,0 if problems.
C
      subroutine ReadFitsDate(card,header,year,year2,month,day)
C
      implicit none
      character*(*) card       ! Header card name (e.g. "DATE-OBS") (input)
      character*(*) header     ! FITS header (input)
      integer*4 year           ! Year in 4 digits (e.g. 1995,2004) (output)
      integer*4 year2          ! Year in 2 digits (e.g. 95,04) (output)
      integer*4 month          ! Month (output)
      integer*4 day            ! Day of month (output)
C
      character*80 s,chead
      integer*4 fc,lc,i,j
      real*4 valread
C
      year =0
      year2=0
      month=0
      day  =0
C
      s = chead(card(1:lc(card)),header)
      s = s(fc(s):lc(s))
      if (index(s(1:4),'/').gt.0) then
        i = index(s,'/')
        if (i.eq.0) return
        s(i:i)=' ' 
        j = index(s,'/')
        if (j.eq.0) return
        s(j:j)=' ' 
        day  = nint(valread(s(i-2:i-1)))
        month= nint(valread(s(i+1:j-1)))
        year = nint(valread(s(j+1:j+2)))
      else
        i = index(s,'-')
        if (i.eq.0) return
        s(i:i)=' ' 
        j = index(s,'-')
        if (j.eq.0) return
        s(j:j)=' ' 
        year = nint(valread(s(1:i-1)))
        month= nint(valread(s(i+1:j-1)))
        day  = nint(valread(s(j+1:j+2)))
      endif
C
      if (year.lt.100) then
        year2 = year
        if (year2.gt.50) then
          year = year2 + 1900
        else
          year = year2 + 2000
        endif
      else
        if (year.ge.2000) then
          year2 = year - 2000
        else
          year2 = year - 1900
        endif
      endif
C
      return
      end


C----------------------------------------------------------------------
C Get heliocentric velocity scale factor (hvsf).
C Use altitude of 4000 meters (probably doesn't make much difference.)
C
      subroutine get_hvsf(header,hvsf)

      implicit none
      character*(*) header
      real*8 hvsf
C
      include 'soun.inc'
C
      real*8 ra,dec,ut,fhead,latitude,longitude,epoch
      real*8 vdiurnal,vlunar,vannual,vhelio
      integer*4 year4,year,month,day
      logical card_exist
C
C Speed of light in kilometers per second (exact).  Approx. altitude Mauna Kea.
      real*8 c,altitude
      parameter(c=2.997925d+5,altitude=4000.d0)
C
C First get correct date and epoch.
      call ReadFitsDate('DATE-OBS',header,year4,year,month,day)
      year = year4
      call ast_date_to_epoch(year,month,day,ut,epoch)
C Time.
      if (card_exist('UTC',header)) then
        ut = fhead('UTC',header)
      else
        ut = fhead('TIME',header)
      endif
C Position (precess it).
      ra = fhead('RA1950',header)
      dec= fhead('DEC1950',header)
      call precess(1950.d0,epoch,ra,dec)
C Global position.
      latitude = fhead('LATITUDE',header)
      longitude = fhead('LONGITUD',header)
      if ((latitude.lt.-999.).or.(longitude.lt.-999.)) then
        write(soun,'(a)')
     .  'ERROR- Could not find latitude and/or longitude in header.'
        call exit(1)
      endif
C Find heliocentric velocity.
      call heliocentric_velocity(year,month,day,ut,latitude,longitude,
     .               ra,dec,altitude,vdiurnal,vlunar,vannual,vhelio)
      if (abs(vhelio).gt.35.) then
        write(soun,'(a,f12.4)') 'ERROR: vhelio is too big:',vhelio
        call exit(1)
      endif
C Echo to user.
      write(soun,'(a,f9.4)')
     .  ' Heliocentric velocity correction (km/s) =',vhelio
C Scale factor.
      hvsf = dsqrt( (1.d0+(vhelio/c)) / (1.d0-(vhelio/c)) )
C
      return
      end

