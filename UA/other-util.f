C
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C
C other-util.f     Other Routines     tab  Apr92-Jun92, Mar00
C
C
C  These routines are not needed for the makee package.
C  They require certain non-existent routines such as "writeint",
C  "readint", etc.    -tab 21mar00
C
C




C--------------------------------------------------------------------
C Given object RA and DEC (ra,dec), and search radius in degrees (search),
C returns number of SAO stars found (n), SAO numbers (sao(*)), RA and DEC of
C stars (sra(*),sdec(*)), V magnitudes (mag(*)), and position relative to
C object in arcminutes (x(*),y(*)).
C

      subroutine find_sao(ra,dec,search,n,sao,sra,sdec,mag,x,y)

      implicit none
      real*8 ra,dec,search
      integer*4 sao(*)
      real*4 x(*),y(*),mag(*)
      real*8 sra(*),sdec(*)

      integer*4 ifile,ia(3),i,j,n
      integer*4 openc,closec,readint,readbyte
      real*8 v,tra,tdec,ra_start,ra_stop,ra_dist,dec_dist
      real*8 rasep,rasep1,rasep2,r,cosdd
      character cv*1,datfile*80,coord*24
      byte ba(12)
      equivalence(ba(1),ia(1))

      if (search.gt.1.51) then
        datfile='/u2/public/catalogs/saocat.bin'//char(0)
        call printerr(
     .      'Note that search distances greater than 1.5 degrees')
        call printerr(
     .      'require a significantly longer time to process.')
      else
        i = int(ra)
        j = int( (ra-dfloat(i)) * 6. ) * 10
        write(datfile,'(a,i2.2,i2.2,a1)')
     .           '/u2/public/catalogs/saocat_sub/subcat_',i,j,char(0)
      endif

      if (openc(ifile,datfile,0).ne.0) goto 990

      ra_dist = search / (15.d0*cosdd(dec))
      dec_dist= search
      ra_start = ra - ra_dist
      ra_stop  = ra + ra_dist

      if (ra_start.lt.0.) ra_start= ra_start + 24.d0
      if (ra_stop.gt.24.) ra_stop = ra_stop  - 24.d0
      rasep = dabs(ra_stop - ra_start)
      if (rasep.gt.12.) rasep = 24. - rasep

      n=0
1     continue
      if (readint(ifile,ba,12).ne.0) goto 5
      if (readbyte(ifile,cv).ne.0) goto 5
      tra = dfloat(ia(1))/3.6d6
      tdec= dfloat(ia(2))/3.6d6
      rasep1= dabs(ra_start-tra)
      if (rasep1.gt.12.) rasep1 = 24. - rasep1
      rasep2= dabs(ra_stop -tra)
      if (rasep2.gt.12.) rasep2 = 24. - rasep2
      if ((rasep1.le.rasep).and.(rasep2.le.rasep)) then
        if (abs(tdec-dec).lt.dec_dist) then
          j = ia(3)
          v  = dfloat(ichar(cv))/10.d0
          call write_radec(tra,tdec,coord,5)
          n=n+1
          r = dabs(tra-ra)
          if (r.gt.12.)  r = 24.d0 - r
          if (tra.lt.ra) r = -1.d0 * r
          x(n)   = sngl( -60.d0*(   r    )*15.d0*cosdd(tdec) )
          y(n)   = sngl(  60.d0*(tdec-dec) )
          mag(n) = sngl( v )
          sao(n) = j
          sra(n) = tra
          sdec(n)= tdec
        endif
      endif
      goto 1

5     continue

      if (closec(ifile).ne.0) goto 990
      return

990   call printerr('Error opening or closing SAO data file.')
      return
      end


C-----------------------------------------------------------------
C Given an RA and DEC (ura,udec), dots-per-inch (dpi), and plate centers
C (pc(4,2) = x,y positions for each of the 4 quadrants),  this routine
C yields the POSS E plate# (plate), the RA and DEC of the plate center
C (ra,dec), the quadrant 1..4  where the object is (quad) where 1=NE,2=NW,
C 3=SE,4=SW, and the pixel position (xpix,ypix) on the scanner with the
C appropriate plate positioning for that quadrant.  It also gives the
C arcseconds north (ddec) and east (dra) of plate center.
C
C Dispersions:  "/col = aspcol, "/row = asprow (see below for current values).
C

      subroutine best_plate(ura,udec,dpi,pc,uplate,FINALra,FINALdec,
     .                      quad,xpix,ypix,ddec,dra,force)

      implicit none
      integer*4 uplate,ifile,bplate
      integer*4 openc,closec,readint,quad,FINALplate
      real*8 ura,udec,as,ra1,ra2,xpix,ypix,ddec,dra,r8,aspcol,asprow
      real*8 a1,a2,t1,t2,sep,minsep,bra,bdec,pc(4,2),dpi
      real*8 eqvra,eqvdec,eqvplate,FINALra,FINALdec,cosdd
      character*80 tfile
      logical force
C
      byte byra(8)
      equivalence(byra(1),eqvra)
      byte bydec(8)
      equivalence(bydec(1),eqvdec)
      byte byplate(4)
      equivalence(byplate(1),eqvplate)
C

      aspcol = 2.837d0 * (600.d0/dpi)
      asprow = 2.840d0 * (600.d0/dpi)

      a1 = (ura/24.d0)*360.d0
      t1 = 90.d0 - udec

      tfile='/u2/public/ADC/misc/poss/poss.bin'//char(0)
      if (openc(ifile,tfile,0).ne.0) goto 990

      bra    = 0.d0
      bdec   = 0.d0
      bplate = 0

      if (force) then

C Select plate with given plate number.
        minsep=1.d+29
1       continue
        if (readint(ifile,byra,8).ne.0) goto 5
        if (readint(ifile,bydec,8).ne.0) goto 990
        if (readint(ifile,byplate,4).ne.0) goto 990
        if (eqvplate.eq.uplate) then
          minsep = sep
          bplate = eqvplate
          bra    = eqvra
          bdec   = eqvdec
        endif
        goto 1
5       continue
        if (closec(ifile).ne.0)  goto 990

      else

C Select plate with object closest to plate center.
        minsep=1.d+29
11      continue
        if (readint(ifile,byra,8).ne.0) goto 55
        if (readint(ifile,bydec,8).ne.0) goto 990
        if (readint(ifile,byplate,4).ne.0) goto 990
        a2 = (eqvra/24.d0)*360.d0
        t2 = 90.d0 - eqvdec
        sep= as(a1,t1,a2,t2)
        if (sep.lt.minsep) then
          minsep = sep
          bplate = eqvplate
          bra    = eqvra
          bdec   = eqvdec
        endif
        goto 11
55      close(1)
        if (closec(ifile).ne.0)  goto 990

      endif

      uplate = bplate
      eqvra  = bra
      eqvdec = bdec

C Compute xpix and ypix given pc(,), aspcol and asprow.
      ra1 = ura
      ra2 = eqvra
      if ((ra1.lt.3.).or.(ra1.gt.21.)) then
        ra1 = ra1 - 12.
        ra2 = ra2 - 12.
        if (ra1.lt.0.) ra1 = ra1 + 24.
        if (ra2.lt.0.) ra2 = ra2 + 24.
      endif
      ddec = 3600.d0 * (udec - eqvdec)
      r8   = udec
      dra  = 3600.d0 * (ra1 - ra2)*15.d0*cosdd(r8)
      ypix=0.
      xpix=0.
      if ( (ra1.ge.ra2).and.(udec.ge.eqvdec) ) then
        quad = 1
        xpix = pc(1,1) + ( ddec / aspcol )
        ypix = pc(1,2) - ( dra / asprow )

      elseif ( (ra1.le.ra2).and.(udec.ge.eqvdec) ) then
        quad = 2
        xpix = pc(2,1) - ( ddec / aspcol )
        ypix = pc(2,2) + ( dra / asprow )

      elseif ( (ra1.ge.ra2).and.(udec.le.eqvdec) ) then
        quad = 3
        xpix = pc(3,1) + ( ddec / aspcol )
        ypix = pc(3,2) - ( dra / asprow )

      elseif ( (ra1.le.ra2).and.(udec.le.eqvdec) ) then
        quad = 4
        xpix = pc(4,1) - ( ddec / aspcol )
        ypix = pc(4,2) + ( dra / asprow )

      endif

      FINALra   = eqvra
      FINALdec  = eqvdec
      FINALplate= eqvplate
      return
990   print *,'Error with c routine.'
      return
      end


C--------------------------------------------------------------------

      subroutine show_plate_info(plate,ra,dec,quad,ddec,dra)

      implicit none
      integer*4 plate,quad
      real*8 ra,dec,ddec,dra,r8
      character c5*5,cquad(4)*2,plate_center*30

      cquad(1)='NE'
      cquad(2)='NW'
      cquad(3)='SE'
      cquad(4)='SW'

      print *,'  '
      print *,'    Plate    =  E-',plate
      call write_radec(ra,dec,plate_center,4)
      print *,'Plate Center = ',plate_center
      print *,'  Quadrant   = ',quad,'   (',cquad(quad),' corner)'

      if (ddec.lt.0.) then
        c5 = 'south'
      else
        c5 = 'north'
      endif
      r8 = abs(ddec)/3600.d0
      print '(a,f5.1,3a)','  Dec. offset =',r8,
     .                    ' degrees ',c5,' of plate center'
      r8 = r8 / 0.18635d0
      print '(a,f5.1,3a)','               ',r8,
     .                    ' cm ',c5,' of plate center.'
      r8 = 17.35d0 - r8
      print '(a,f5.1,3a)','               ',r8,
     .                    ' cm from ',c5,' edge of plate.'
      r8 = r8 * 11.181d0
      if (r8.lt.30.) then
        print *,':::::::::::::::::::::::::::::::::::::::::::::::::'
        print '(3a)','   Note proximity to ',c5,' edge of plate:'
        print '(a,f5.1,3a)','               ',r8,
     .                      ' arcminutes from ',c5,' edge.'
        print *,':::::::::::::::::::::::::::::::::::::::::::::::::'
      endif

      if (dra.lt.0.) then
        c5 = 'west '
      else
        c5 = 'east '
      endif
      r8 = abs(dra)/3600.d0
      print '(a,f5.1,3a)','  R.A. offset =',r8,' degrees ',
     .                    c5,'of plate center'
      r8 = r8 / 0.18635d0
      print '(a,f5.1,3a)','               ',r8,' cm ',
     .                    c5,'of plate center.'
      r8 = 17.35d0 - r8
      print '(a,f5.1,3a)','               ',r8,' cm from ',
     .                    c5,'edge of plate.'
      r8 = r8 * 11.181d0
      if (r8.lt.30.) then
        print *,':::::::::::::::::::::::::::::::::::::::::::::::::'
        print '(3a)','   Note proximity to ',c5,'edge of plate:'
        print '(a,f5.1,3a)','               ',r8,
     .                      ' arcminutes from ',c5,'edge.'
        print *,':::::::::::::::::::::::::::::::::::::::::::::::::'
      endif

      return
      end


C
C------------------------------------------------------------------------
C Reads in a raw PBM image file.
C
      subroutine readpbm(ifile,b,maxpix,width,height,ok)
C
      implicit none
      integer*4 ifile,maxpix        ! input
      integer*4 width,height        ! output  ( npix = (width*height)/8 )
      byte b(maxpix)                ! output
      logical ok                    ! output
      logical readcs
      integer*4 npix,readint
      character*80 cs
C
      print *,'NOTE: readpbm: linux incompatible'
C
      if (.not.readcs(ifile,cs)) goto 920
      if (cs(1:2).ne.'P4') goto 930
      if (.not.readcs(ifile,cs)) goto 920
      read(cs,'(i)') width
      if (.not.readcs(ifile,cs)) goto 920
      read(cs,'(i)') height
      npix=width*height
      if (mod(npix,8).eq.0) then
        npix = npix / 8
      else
        npix = 1 + ( npix / 8 )
      endif
      if (npix.gt.maxpix) goto 940
      if (readint(ifile,b,npix).ne.0) goto 950
      ok=.true.
      return
920   print *,'Error reading line of characters.'
      goto 990
930   print *,'Error- not a raw PBM file.'
      goto 990
940   print *,'Image too big. npix,maxpix: ',npix,maxpix
      goto 990
950   print *,'Error reading bytes from PBM file.'
990   ok=.false.
      return
      end


C------------------------------------------------------------------------
C Reads in the next non-comment character string separated by whitespace
C from the PNM (portable image) file given by ifile .
C
      logical function readcs(ifile,cs)
C
      implicit none
      integer*4 ifile,type
      character cs*70
      logical readchars
      if (.not.readchars(ifile,cs,type)) goto 910
      if (cs(1:1).eq.'#') then
        do while(type.ne.13)
          if (.not.readchars(ifile,cs,type)) goto 910
        enddo
      endif
      readcs = .true.
      return
910   readcs = .false.
      return
      end



C------------------------------------------------------------------------
C Reads a set of characters up to the next whitespace, in file given by
C ifile .  "type" is the ASCII code for the whitespace character which
C ended the string.
C
      logical function readchars(ifile,cs,type)
C
      implicit none
C
      integer*4 ifile,type,i,ierr,readint
      character*(*) cs*(*)
      logical whitespace
C
      character*1 c
      byte byc(1)
      equivalence(byc(1),c)
C
      cs = ' '
      c = ' '
      do while(whitespace(c))
        ierr = readint(ifile,byc,1)
        if (ierr.ne.0) goto 910
      enddo
      i=1
      cs(i:i)=c
      ierr = readint(ifile,byc,1)
      if (ierr.ne.0) goto 910
      do while(.not.whitespace(c))
        i=i+1
        cs(i:i)=c
        ierr = readint(ifile,byc,1)
        if (ierr.ne.0) goto 910
      enddo
      type=ichar(c)
      readchars = .true.
      return
910   readchars = .false.
      return
      end

C------------------------------------------------------------------------
C Tells whether a character is a whitespace character.
      logical function whitespace(c)
      implicit none
      character c*1
      integer*4 i
      i=ichar(c)
      if ( ((i.ge.8).and.(i.le.13)).or.(i.eq.32) ) then
        whitespace=.true.
      else
        whitespace=.false.
      endif
      return
      end

C------------------------------------------------------------------------
C Writes out a raw PBM image file.
C
      subroutine writepbm(ifile,b,maxpix,width,height,ok)
C
      implicit none
C
      integer*4 ifile,maxpix      ! input
      integer*4 width,height      ! input
      byte b(maxpix)              ! input (npix=((width*height))/8)
      logical ok                  ! output
      logical writecs
      integer*4 npix,writeint,k,j,fc,lc
      character cs*70, dum*8
C
      npix=width*height
      if (mod(npix,8).eq.0) then
        npix = npix / 8
      else
        npix = 1 + ( npix / 8 )
      endif
C     print *,'Write PBM indicator.'
      cs='P4'
      if (.not.writecs(ifile,cs,2)) goto 920
C     print *,'Write width and height on the same line.'
      cs=' '
      dum=' '
      write(dum,'(i8)') width
      call strpos(dum,fc,lc)
      cs(1:(lc-fc+1))=dum(fc:lc)
      j=lc-fc+3
      dum=' '
      write(dum,'(i8)') height
      call strpos(dum,fc,lc)
      k=lc-fc+j
      cs(j:k)=dum(fc:lc)
      if (.not.writecs(ifile,cs,k)) goto 920
      if (writeint(ifile,b,npix).ne.0) goto 950
      ok=.true.
      return
920   print *,'Error writing string of characters.'
      goto 990
950   print *,'Error writing.'
990   ok=.false.
      return
      end


C------------------------------------------------------------------------
C Writes out the first n characters of cs to ifile and then a <cr>.
C
      logical function writecs(ifile,USERcs,n)
C
      implicit none
C
      integer*4 ifile,n,writeint
      character USERcs*70
      character cs*70,cr*1
      byte bycs(70)
      equivalence(bycs(1),cs)
      byte bycr(1)
      equivalence(bycr(1),cr)
C
      cs = USERcs
C
      if (writeint(ifile,bycs,n).ne.0) goto 990
      cr = char(13)
      if (writeint(ifile,bycr,1).ne.0) goto 990
      writecs = .true.
      return
990   writecs = .false.
      return
      end


C----------------------------------------------------------------------
C Write a ".pgm" portable gray map image, given a real*4 array.
C
C Read/Write portable image maps     TAB April 1992
C
C Link with cstuff.o
C
      subroutine writepgm_real4(n,nc,nr,a,b,amin,amax,pgmfile)
C
      implicit none
      integer*4 n            ! size of the array (input).
      integer*4 nc,nr        ! number of columns, number of rows (input).
      real*4 a(n)            ! image data (input).
      byte b(n)              ! scratch array (scratch).
      real*4 amin,amax       ! minimum and maximum value (input).
      character*(*) pgmfile  ! output file (input).
C
      character*80 fn
      integer*4 ios,ifile,openc,closec,lc
      integer*4 lastchar,k,i
      logical ok
C
C You must now convert the reals to bytes.
      do i=1,n
        b(i)  = max(0,min(255,  nint( 255.*(a(i)-amin)/(amax-amin) ) ))
      enddo
C Filename.
      fn = pgmfile
      k = lastchar(fn)
      fn(k+1:k+1) = char(0)
C Open.
      ios = openc(ifile,fn,1)
      fn(k+1:k+1) = ' '
      if (ios.ne.0) goto 910
C Write PGM.
      call writepgm(ifile,b,n,nc,nr,255,ok)
      if (.not.ok) goto 920
C Close.
      ios = closec(ifile)
      if (ios.ne.0) goto 930
      return
C Errors.
910   print *,'Error opening : ',pgmfile(1:lc(pgmfile))
      goto 999
920   print *,'Error reading : ',pgmfile(1:lc(pgmfile))
      goto 999
930   print *,'Error closing : ',pgmfile(1:lc(pgmfile))
999   return
      end


C------------------------------------------------------------------------
C Writes out a raw PGM image file.
C
      subroutine writepgm(ifile,b,maxpix,width,height,maxval,ok)
C
      implicit none
      integer*4 ifile,maxpix           ! input
      integer*4 width,height,maxval    ! input
      byte b(maxpix)                   ! input
      logical ok                       ! output
      logical writecs
      integer*4 npix,ierr,writeint,k,j,fc,lc
      character cs*70, dum*8
C
C Image size.
      npix=width*height
C Image type.
      cs='P5'
      if (.not.writecs(ifile,cs,2)) goto 920
C Width and height.
      cs=' '
      dum=' '
      write(dum,'(i8)') width
      call strpos(dum,fc,lc)
      cs(1:(lc-fc+1))=dum(fc:lc)
      j=lc-fc+3
      dum=' '
      write(dum,'(i8)') height
      call strpos(dum,fc,lc)
      k=lc-fc+j
      cs(j:k)=dum(fc:lc)
      if (.not.writecs(ifile,cs,k)) goto 920
C Maximum value.
      cs=' '
      dum=' '
      write(dum,'(i8)') maxval
      call strpos(dum,fc,lc)
      k=lc-fc+1
      cs(1:k)=dum(fc:lc)
      if (.not.writecs(ifile,cs,k)) goto 920
C Write image.
      if (npix.gt.maxpix) goto 940
      ierr = writeint(ifile,b,npix)
      ok=.true.
      return
C Error messages.
920   print *,'Error writing string of characters.'
      goto 990
930   print *,'Error- not a raw PGM file.'
      goto 990
940   print *,'Image too big. npix,maxpix: ',npix,maxpix
990   ok=.false.
      return
      end


C------------------------------------------------------------------------
C Reads in a raw PGM image file.

      subroutine readpgm(ifile,b,maxpix,width,height,maxval,ok)

      implicit none
      integer*4 ifile,maxpix           ! input
      integer*4 width,height,maxval    ! output
      byte b(maxpix)                   ! output
      logical ok                       ! output
      logical readcs
      integer*4 npix,ierr,readint
      character*70 cs

      print *,'NOTE: readpgm: linux incompatible'

      if (.not.readcs(ifile,cs)) goto 920
      if (cs(1:2).ne.'P5') goto 930
      if (.not.readcs(ifile,cs)) goto 920
      read(cs,'(i)') width
      if (.not.readcs(ifile,cs)) goto 920
      read(cs,'(i)') height
      if (.not.readcs(ifile,cs)) goto 920
      read(cs,'(i)') maxval
      npix=width*height
      if (npix.gt.maxpix) goto 940
      ierr = readint(ifile,b,npix)
      ok=.true.
      return

920   print *,'Error reading line of characters.'
      goto 990
930   print *,'Error- not a raw PGM file.'
      goto 990
940   print *,'Image too big. npix,maxpix: ',npix,maxpix

990   ok=.false.
      return
      end


