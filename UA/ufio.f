C ufio.f         Unix/Sun FITS Input/Ouput Routines      tab  April 1992
C
C           Converted to use fully C routines -tab  March 2000
C
C     (for IEEE Real*4, Integer*2, and Integer*4 FITS disk files)
C
C ------------------------------------------------------------------------------
C
C ------------------------------------------------------------------------------
C  ROUTINES:
C
C     subroutine readfitsxy(file,x,y,maxpt,npix)
C     subroutine readfitsxy8(file,x,y,maxpt,npix)
C     subroutine read_fits_header(file,header,ok)
C     subroutine readfits_header(file,header,ok)  (ALIAS)
C     subroutine readfits(file,a,maxpt,header,ok)
C     subroutine writefits(file,a,header,ok)
C
C ------------------------------------------------------------------------------
C
C **************** SAMPLE PROGRAM ONE *****************************
C This program will read bitpix=8,16,32,and -32 FITS files, but
C will only write real*4 (bitpix=-32) FITS files.
C
C     implicit none
C     integer*4 maxpt
C     parameter(maxpt=1000000)
C     real*4 a(maxpt)
C     character file*80, header*8640
C     logical ok
C
C     file=' '
C     print *,'Input file: '
C     read(5,'(a)') file
C
C     call readfits(file,a,maxpt,header,ok)
C
C     file=' '
C     print *,'Output file: '
C     read(5,'(a)') file
C
C     call writefits(file,a,header,ok)
C
C     stop
C     end 
C
C *******************************************************************


C-------------------------------------------------------------------
C Read in Unix/IEEE fits spectrum, return npix=-1 if errors.
C
      subroutine readfitsxy(file,x,y,maxpt,npix)
C
      implicit none
      character*(*) file       ! fits filename    (input)
      real*4 x(*),y(*)         ! x,y pairs        (output)
      integer*4 maxpt          ! maximum number of points (input)
      integer*4 npix           ! number of pairs  (output)
      character*115200 header
      integer*4 inhead,i
      real*8 getx
      logical ok

      call readfits(file,y,maxpt,header,ok)
      if (.not.ok) goto 920
      if ((inhead('NAXIS2',header).ne.1).and.
     .    (inhead('NAXIS',header).ne.1)) goto 930

      npix=max(0,inhead('NAXIS1',header))

      if (getx(-1.d0,header).lt.0.) goto 940
      do i=1,npix
        x(i)=sngl(getx(dfloat(i),header))
      enddo 

      return 

920   write(0,'(a)') 'Error reading fits file.'
      goto 990

930   write(0,'(a)') 'Error- fits file not a spectrum.'
      goto 990

940   write(0,'(a)') 'Error calling getx function.'

990   npix=-1

      return
      end

C-------------------------------------------------------------------
C Read in Unix/IEEE fits spectrum, return npix=-1 if errors.
C This version returns real*8 arrays.  Maximum number of points is 300,000.
C
      subroutine readfitsxy8(file,x8,y8,maxpt,npix)
C
      implicit none
      character*(*) file       ! fits filename    (input)
      real*8 x8(*),y8(*)       ! x,y pairs        (output)
      integer*4 maxpt          ! maximum number of points (input)
      integer*4 npix           ! number of pairs  (output)
      character*115200 header
      integer*4 inhead,i,m
      parameter(m=300000)
      real*4 y(m)
      real*8 getx
      logical ok

      call readfits(file,y,maxpt,header,ok)
      if (.not.ok) goto 920
      if ((inhead('NAXIS2',header).ne.1).and.
     .    (inhead('NAXIS',header).ne.1)) goto 930

      npix=max(0,inhead('NAXIS1',header))
      if (npix.gt.m) then
        print *, 'Error-- readfitsxy8: '
        print *, 'Too many points in file, maximum=',m
        goto 920
      endif

      if (getx(-1.d0,header).lt.0.) goto 940
      do i=1,npix
        x8(i) = getx(dfloat(i),header)
        y8(i) = dble(y(i))
      enddo 

      return 

920   write(0,'(a)') 'Error reading fits file.'
      goto 990

930   write(0,'(a)') 'Error- fits file not a spectrum.'
      goto 990

940   write(0,'(a)') 'Error calling getx function.'

990   npix=-1

      return
      end


C------------------------------------------------------------------
C Non-standard alias.
C
      subroutine readfits_header(file,header,ok)
      implicit none
      character*(*) file       ! FITS file        (input)
      character*(*) header     ! FITS header      (output)
      logical ok
      call read_fits_header(file,header,ok)
      return
      end

C------------------------------------------------------------------
C Non-standard alias.
C
      subroutine readfitsheader(file,header,ok)
      implicit none
      character*(*) file       ! FITS file        (input)
      character*(*) header     ! FITS header      (output)
      logical ok
      call read_fits_header(file,header,ok)
      return
      end


C------------------------------------------------------------------
C Reads IEEE Unix FITS disk files.
C
      subroutine read_fits_header(file,header,ok)
C
      implicit none
C
      character*(*) file       ! FITS file        (input)
      character*(*) header     ! FITS header      (output)
      logical ok
      integer*4 lc,ii, headersize, ffcreadfitsheader
      character*1000 cfile
C
      headersize = 115200
      cfile = file
      call cappendnull(cfile,lc(cfile))
      ii = ffcreadfitsheader( cfile, header, headersize )
      if (ii.eq.1) then
        ok = .true.
      else
        ok = .false.
        write(0,'(2a)') 'Error reading FITS header : ',
     .                    file(1:lc(file))
      endif
      return
      end


C------------------------------------------------------------------
C Reads IEEE Unix FITS disk files (HIRES2 raw format file).
C
      subroutine readfits_header_rawhires2(file,header,ok)
C
      implicit none
C
      character*(*) file       ! FITS file        (input)
      character*(*) header     ! FITS header      (output)
      logical ok
      integer*4 lc,ii, headersize, ffcreadfitsheaderrawhires2
      character*1000 cfile
C
      headersize = 115200
      cfile = file
      call cappendnull(cfile,lc(cfile))
      ii = ffcreadfitsheaderrawhires2( cfile, header, headersize )
      if (ii.eq.1) then
        ok = .true.
      else
        ok = .false.
        write(0,'(2a)') 'Error reading FITS header : ',
     .                    file(1:lc(file))
      endif
      return
      end


C------------------------------------------------------------------
C Reads IEEE Unix FITS disk files.
C
      subroutine readfits(file,a,maxpt,header,ok)
C
      implicit none
C
      character*(*) file       ! FITS file        (input)
      real*4 a(*)              ! data             (output)
      integer*4 maxpt          ! maximum number of points for a()  (input)
      character*(*) header     ! FITS header      (output)
      logical ok
      integer*4 lc,ii,headersize, ffcreadfits
      character*1000 cfile
C
      headersize = 115200
      cfile = file
      call cappendnull(cfile,lc(cfile))
      ii = ffcreadfits( cfile, a, maxpt, header, headersize )
      if (ii.eq.1) then
        ok = .true.
      else
        ok = .false.
        write(0,'(2a)') 'Error reading FITS file : ',
     .                    file(1:lc(file))
      endif
      return
      end


C------------------------------------------------------------------
C Writes IEEE Unix FITS disk files.  Always BITPIX = -32 (real*4).
C
      subroutine writefits(file,a,header,ok)
C
      implicit none
      character*(*) file       ! FITS file        (input)
      real*4 a(*)              ! data             (input)
      character*(*) header     ! FITS header      (input)
      logical ok               ! .false. if error (output)
C
      integer*4 lc,ii,headersize, ffcwritefits
      character*1000 cfile
C
      headersize = 115200
      cfile = file
      call cappendnull(cfile,lc(cfile))
      call inheadset('BITPIX',-32,header)
      ii = ffcwritefits( cfile, a, header, headersize )
      if (ii.eq.1) then
        ok = .true.
      else
        ok = .false.
        write(0,'(2a)') 'Error writing FITS file : ',
     .                    file(1:lc(file))
      endif
      return
      end

