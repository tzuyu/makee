
C----------------------------------------------------------------------
C Read real (REAL or DOUBLE) ipac header cards, store in real*8 array.
C Parameters that are not found are set to -1.d+40.
C
      subroutine readipach8(file,p,np,x,options)
C
      implicit none
      character*(*) file    ! IPAC table filename (input).
      character*(*) p(*)    ! Requested header parameter names (input).
      integer*4 np          ! Number of parameters requested (input).
      real*8 x(*)           ! Real header values (output).
      character*(*) options ! Special options (input).
C
      character*2000 w,line,type,ww,wl
      integer*4 i,i1,i2,ie,lc,firstchar
      real*8 valread8
C
C Blank.
      do i=1,np
        x(i)=-1.d+40
      enddo
C
C Open file.
      open(1,file=file,status='old')
C Read header cards.
      read(1,'(a)',end=5) line
      do while(line(1:1).ne.'|')
C Valid card?
      if (line(1:1).eq.char(92)) then   !  char(92)='\'
        i=index(line,' ')
        type = line(2:i-1)
        call upper_case(type)
        if ( (type(1:4).ne.'CHAR').and.
     .       ((type(1:4).eq.'REAL').or.(type(1:4).eq.'DOUB')
     .                             .or.(type(1:3).eq.'INT')) )  then
C Look for equals sign.
          ie=index(line,'=')
C First non-blank looking backwards.
          i2=ie-1
          do while((i2.gt.1).and.(line(i2:i2).eq.' '))
            i2=i2-1
          enddo
          if (i2.gt.1) then
C Next blank look backwards.
            i1=i2-1
            do while((i1.gt.1).and.(line(i1:i1).ne.' '))
              i1=i1-1
            enddo
            i1=i1+1
            if (i1.gt.1) then
C Find a user requested card that matches.
              do i=1,np
                w = p(i)
                call upper_case(w)
                wl= line(i1:i2)
                call upper_case(wl)
                if (w(1:lc(w)).eq.wl(1:lc(wl))) then
                  ww= line(ie+1:)
                  ww= ww(firstchar(ww):lc(ww))
                  x(i) = valread8( ww )
                endif
              enddo
            endif              ! if i1>1
          endif                ! if i2>1
        endif                  ! if not "CHAR"
      endif                    ! if "\"
      read(1,'(a)',end=5) line
      enddo                    ! while not "|"
5     close(1)
      return
      end


C----------------------------------------------------------------------
C Read character ipac header cards, store in character array.
C Parameters that are not found are set to blank strings.
C
      subroutine readipachC(file,p,np,c,options)
C
      implicit none
      character*(*) file    ! IPAC table filename (input).
      character*(*) p(*)    ! Requested header parameter names (input).
      integer*4 np          ! Number of parameters requested (input).
      character*(*) c(*)    ! Character header values (output).
      character*(*) options ! Special options (input).
C
      character*2000 w,line,type,ww,wl
      integer*4 i,i1,i2,ie,lc,firstchar

C Open file.
      open(1,file=file,status='old')
C Read header cards.
      read(1,'(a)',end=5) line
      do while(line(1:1).ne.'|')
C Valid card?
      if (line(1:1).eq.char(92)) then   !  char(92)='\'
        i=index(line,' ')
        type = line(2:i-1)
        call upper_case(type)
        if (type(1:4).eq.'CHAR') then
C Look for equals sign.
          ie=index(line,'=')
C First non-blank looking backwards.
          i2=ie-1
          do while((i2.gt.1).and.(line(i2:i2).eq.' '))
            i2=i2-1
          enddo
          if (i2.gt.1) then
C Next blank look backwards.
            i1=i2-1
            do while((i1.gt.1).and.(line(i1:i1).ne.' '))
              i1=i1-1
            enddo
            i1=i1+1
            if (i1.gt.1) then
C Find a user requested card that matches.
              do i=1,np
                w = p(i)
                call upper_case(w)
                wl= line(i1:i2)
                call upper_case(wl)
                if (w(1:lc(w)).eq.wl(1:lc(wl))) then
                  ww = line(ie+1:lc(line))
                  c(i) = ' '
                  c(i) = ww(firstchar(ww):lc(ww))
                endif
              enddo
            endif              ! if i1>1
          endif                ! if i2>1
        endif                  ! if "CHAR"
      endif                    ! if "\"
      read(1,'(a)',end=5) line
      enddo                    ! while not "|"
5     close(1)
      return
      end


C----------------------------------------------------------------------
C Get boundaries for a parameter in an IPAC table column header line.
C
      subroutine ipac_column_bounds(line1,line2,parm,i1,i2)
C
      implicit none
      character*(*) line1   ! IPAC table parameter column header line (input).
      character*(*) line2   ! dummy string (same length as line1) (input).
      character*(*) parm    ! parameter name (input/output).
      integer*4 i1,i2       ! column limits (output).
C
      character*200 parm2
      integer*4 i,lc,k,kk,j
C
C Copy line.
      line2=line1
      kk= lc(line2)
C Blank out vertical bars "|".
      do i=1,kk
        if (line2(i:i).eq.'|') line2(i:i)=' '
      enddo
      k=lc(parm)
C Pad parameter name.
      parm2 = ' '//parm(1:k)//' '
      j = index(line2,parm2(1:k+2))
      if (j.eq.0) then
        print *,'Parameter "',parm(1:lc(parm)),'" is not in table.'
        parm = 'NOT_IN_TABLE'
        return
      endif
C Look backwards.
      i1 = j
      do while((i1.gt.0).and.(line1(i1:i1).ne.'|'))
        i1=i1-1
      enddo
      if (i1.lt.1) i1=1
C Look forwards.
      i2 = j+1
      do while((i2.lt.kk).and.(line1(i2:i2).ne.'|'))
        i2=i2+1
      enddo
C Pull back one.
      i2=i2-1
      return
      end

C----------------------------------------------------------------------
C Read number data from an IPAC table.
C Options: "-nowarn" : exclude lines containing the string "WARNING:".
C          "band=n"  : include only BAND=n (n=1 or 2) data.
C
C If a parameter is not found, p() is set to "NOT_IN_TABLE" or if
C type is wrong "WRONG_TYPE".
C
      subroutine readipac8(file,p,np,x,xd1,xd2,nx,options)
C
      implicit none
      character*(*) file    ! IPAC table filename (input).
      character*(*) p(*)    ! Requested table parameter names (input/output).
      integer*4 np          ! Number of parameters requested (input).
      integer*4 xd1,xd2     ! Dimensions for x() matrix (input).
      real*8 x(xd1,xd2)     ! Data (1st axis is the value for a given parameter,
                            !   2nd axis is the parameter number) (output).
      integer*4 nx          ! Number of entries in the table (output).
                            !   (Note: x(nx,np).)
      character*(*) options ! Special options.
C
      character*2000 header,wrd,subhead,s
      integer*4 ip(1000,2),i,n,band,k,b1,b2,valid(1000),nvalid,j,ii,lc
      real*8 valread8
      logical nowarn
C
C Check.
      if (np.gt.xd2) then
        print *,'ERROR: readipac8: xd2 array dim. not large enough: ',
     .            np,xd2,' : file=',file(1:lc(file))
        return
      endif
C Zero.
      do i=1,xd1
      do j=1,xd2
      x(i,j)=0.d0
      enddo
      enddo
C
C Options.
      nowarn = (index(options,'-nowarn').gt.0)
      band=0
      if (index(options,'band=1').gt.0) band=1
      if (index(options,'band=2').gt.0) band=2
C
C Open file.
      open(1,file=file,status='old')
C Read header.
111   read(1,'(a)',end=5) header
      if (header(1:1).ne.'|') goto 111
      read(1,'(a)',end=5) subhead
      nvalid=0
      do i=1,np
        if (p(i).ne.' ') then
          call ipac_column_bounds(header,wrd,p(i),ip(i,1),ip(i,2))
          s = subhead(ip(i,1):ip(i,2))
          if ((index(s,'C').ne.0).or.(index(s,'c').ne.0)) then
            p(i)='WRONG_TYPE'
          elseif (p(i)(1:12).ne.'NOT_IN_TABLE') then
            nvalid=nvalid+1
            valid(nvalid)=i
          endif
        endif
      enddo
      if (band.ne.0) call ipac_column_bounds(header,wrd,'B',b1,b2)
C Read data.
      n=0
1     continue
      read(1,'(a)',end=5) wrd
      if (wrd(1:1).eq.'\\') goto 1
      if (nowarn) then
        if (index(wrd,'WARNING:').gt.0) goto 1
      endif
      if (band.ne.0) then
        k = nint( valread8( wrd(b1:b2) ) )
        if (k.ne.band) goto 1
      endif
      n=n+1
      do ii=1,nvalid
        i = valid(ii)
        x(n,i) = valread8( wrd(ip(i,1):ip(i,2)) )
      enddo
      if (n.ge.xd1) then
        print *,'ERROR: readipac8: xd1 array dim. ',
     .          'not large enough: ',n,xd1,
     .             ' : file=',file(1:lc(file))
        goto 5
      endif
      goto 1
5     continue
      close(1)
      nx = n
      return
      end

C----------------------------------------------------------------------
C MODIFIED: to accept continuation lines: "\ ++". 
C
C Read character data from an IPAC table.
C Options: "-nowarn" : exclude lines containing the string "WARNING:".
C          "band=n"  : include only BAND=n (n=1 or 2) data.
C
      subroutine readipacC(file,p,np,c,cd1,cd2,nc,options)
C
      implicit none
      character*(*) file       ! IPAC table filename (input).
      character*(*) p(*)       ! Requested table parameter names (input).
      integer*4 np             ! Number of parameters requested (input).
      integer*4 cd1,cd2        ! Dimensions for c() matrix (input).
      character*(*) c(cd1,cd2) ! Character data (1st axis is the value for a
                               ! given parameter, 2nd axis is the parameter
                               !  number) (output).
      integer*4 nc             ! Number of entries in the table (output).
                               !   (Note: c(nc,np).)
      character*(*) options    ! Special options.
C
      character*2000 header,wrd,s,subhead
      integer*4 ip(1000,2),i,n,fc,lc,k,band,b1,b2,ii,j
      integer*4 valid(1000),nvalid
      real*8 valread8
      logical nowarn
C
C Check.
      if (np.gt.cd2) then
        print *,'ERROR: readipacC: cd2 array dim. not large enough: ',
     .         np,cd2,' : file=',file(1:lc(file))
        return
      endif
C
C Options.
      nowarn = (index(options,'-nowarn').gt.0)
      band=0
      if (index(options,'band=1').gt.0) band=1
      if (index(options,'band=2').gt.0) band=2
C Zero.
      do i=1,cd1
      do j=1,cd2
      c(i,j)=' '
      enddo
      enddo
C
C Open file.
      open(1,file=file,status='old')
C Read header.
111   read(1,'(a)',end=5) header
      if (header(1:1).ne.'|') goto 111
      read(1,'(a)',end=5) subhead
      nvalid=0
      do i=1,np
        if (p(i).ne.' ') then
          call ipac_column_bounds(header,wrd,p(i),ip(i,1),ip(i,2))
          s = subhead(ip(i,1):ip(i,2))
          if ((index(s,'C').eq.0).and.(index(s,'c').eq.0)) then
            p(i)='WRONG_TYPE'
          elseif (p(i)(1:12).ne.'NOT_IN_TABLE') then
            nvalid=nvalid+1
            valid(nvalid)=i
          endif
        endif
      enddo
      if (band.ne.0) call ipac_column_bounds(header,wrd,'B',b1,b2)
C Read data.
      n=0
1     continue
      read(1,'(a)',end=5) wrd
C Skip header cards, except continuation cards.
      if ((wrd(1:1).eq.'\\').and.(wrd(1:4).ne.'\\ ++')) goto 1
      if (nowarn) then
        if (index(wrd,'WARNING:').gt.0) goto 1
      endif
C Band inclusion.
      if (band.ne.0) then
        k = nint( valread8( wrd(b1:b2) ) )
        if (k.ne.band) goto 1
      endif
C If not contination...
      if (wrd(1:4).ne.'\\ ++') then
C New entry.
        n=n+1
        do ii=1,nvalid
          i = valid(ii)
          s = wrd(ip(i,1):ip(i,2))
          c(n,i) = ' '
          c(n,i) = s(fc(s):lc(s))
        enddo
      elseif (n.gt.0) then
C ...continuation (append to old entry).
        do ii=1,nvalid
          i = valid(ii)
          s = wrd(ip(i,1):ip(i,2))
          if (lc(s).gt.0) then
            c(n,i) = c(n,i)(1:lc(c(n,i)))//s(fc(s):lc(s))
          endif
        enddo
      endif
C Check.
      if (n.ge.cd1) then
        print *,'ERROR: readipacC: cd1 array dim. ',
     .          'not large enough: ',n,cd1,
     .             ' : file=',file(1:lc(file))
        goto 5
      endif
      goto 1
5     continue
      close(1)
      nc = n
      return
      end

C----------------------------------------------------------------------
C Print and run command using "csys".
      subroutine csysp(wrd)
      implicit none
      character*(*) wrd
      character*1000 wrd2
      integer*4 lc
      print '(a)',wrd(1:lc(wrd))
      wrd2 = wrd(1:lc(wrd))//char(0)
      call csys(wrd2)
      return
      end

C----------------------------------------------------------------------
C Print and run command using "system".
      subroutine fsysp(wrd)
      implicit none
      character*(*) wrd
      integer*4 lc
      print '(a)',wrd(1:lc(wrd))
      call system(wrd(1:lc(wrd)))
      return
      end

