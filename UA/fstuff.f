C fstuff.f                             tab March 2000
C
C These are gateway routines between FORTRAN and C and vice versa routines.
C
C These are routines which call C routines but are called BY FORTRAN programs.
C (For example, strings need terminal nulls before they can go to C.)
C
C These are ALSO routines which call FORTRAN routines but are called BY C
C   main programs. (For example, routine names cannot contain "_" characters.
C   When called by the C program the routine name will end with "_" inside
C   the C code.)  (Also: strings must be exactly defined in length, i.e.
C   cannot use "character*(*)" in FORTRAN routine called by C program.)
C

C------------------------------------------------------------------------
      real*8 function fgetlinevalue( line, nn )
      implicit none
      character*(*) line
      integer*4 nn,lc
      character*1000 temp
      real*8 ffgetlinevalue
      temp = line
      call cappendnull(temp,lc(temp))
      fgetlinevalue = ffgetlinevalue(temp,nn)
      return
      end  
C------------------------------------------------------------------------
      real*8 function fgetlinevaluequiet( line, nn )
      implicit none
      character*(*) line
      integer*4 nn,lc
      character*1000 temp
      real*8 ffgetlinevaluequiet
      temp = line
      call cappendnull(temp,lc(temp))
      fgetlinevaluequiet = ffgetlinevaluequiet(temp,nn)
      return
      end  



C . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
C
C                            ... f2c ...
C . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


C----------------------------------------------------------------------
C See write_radec().
C Note "s" has 40 characters.
      subroutine f2cwriteradec( ra, dec, s, mode )
      implicit none
      real*8 ra,dec
      integer*4 mode,ii,lastchar
      character*40 s
      print *,'ra=',ra,'  dec=',dec,'  mode=',mode
      call write_radec(ra,dec,s,mode)
      ii = lastchar(s)
      s(ii+1:ii+1) = char(0)
      return
      end
 
C----------------------------------------------------------------------
C See poly_fit_gj().
C
      subroutine f2cpolyfitgj(npt,x,y,wt,order,ucoef,xoff,forcexoff,iok)
C
      implicit none
C
      integer*4 npt         ! Number of points ( npt > order+1 ) (output).
      real*8 x(*)           ! x values (input).
      real*8 y(*)           ! y values (input).
      real*8 wt(*)          ! weight values ( usually 1 / sig^2 ) (input).
C                           !    Entries with weight=0 excluded from data set.
      integer*4 order       ! Polynomial order (0 to 99) (input).
      real*8 ucoef(*)       ! User coeffecients ( size >= order+1 ) (output).
      real*8 xoff           ! X value offset. Subtract this value from x values
C                           !    before using coeffecients. (input/output).
      integer*4 forcexoff   ! Force routine to use the given "xoff" (input)
C                           !    0 = no force, xoff determined by routine.
C                           !    1 = force, use user value of xoffset.
      integer*4 iok         ! Success? (1/0) (output)
C
      logical ok
C
      call poly_fit_gj(npt,x,y,wt,order,ucoef,xoff,forcexoff,ok)
      iok=0
      if (ok) iok=1
C
      return
      end

C-------------------------------------------------------------------------
C See poly_fit_gj_residuals().
C
      subroutine f2cpolyfitgjresiduals(np,x8,y8,w8,order,coef,
     .                                 xoff,high,wrms,wppd)
      implicit none
      integer*4 np,order
      real*8 x8(*),y8(*),w8(*),coef(*),xoff,high,wrms,wppd
      call poly_fit_gj_residuals(np,x8,y8,w8,order,coef,
     .                           xoff,high,wrms,wppd)
      return
      end


C----------------------------------------------------------------------
C See poly_fit_glls().
C
      subroutine f2cpolyfitglls(npt,x,y,wt,order,ucoef,iok)
C
      implicit none
C
      integer*4 npt         ! Number of points ( npt > order+1 ) (output).
      real*8 x(*)           ! x values (input).
      real*8 y(*)           ! y values (input).
      real*8 wt(*)          ! weight values ( usually 1 / sig^2 ) (input).
C                           !    Entries with weight=0 excluded from data set.
      integer*4 order       ! Polynomial order (0 to 99) (input).
      real*8 ucoef(*)       ! User coeffecients ( size >= order+1 ) (output).
      integer*4 iok         ! Success? (1/0) (output)
C
      logical ok
C
      call poly_fit_glls(npt,x,y,wt,order,ucoef,ok)
      iok=0
      if (ok) iok=1
C
      return
      end

C-------------------------------------------------------------------------
C See poly_fit_glls_residuals().
C
      subroutine f2cpolyfitgllsresiduals(np,x8,y8,w8,order,
     .                                   coef,high,wrms)
      implicit none
      integer*4 np,order
      real*8 x8(*),y8(*),w8(*),coef(*),high,wrms
      call poly_fit_glls_residuals(np,x8,y8,w8,order,coef,high,wrms)
      return
      end




