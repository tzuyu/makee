C
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C
C   THIS VERSION MODIFIED FOR LINUX...   March 2000.
C
C

C ast-util.f        Astro - Utilities                tab  August 1992

C Taken mostly from iraf: noao.astutil

C-----------------------------------------------------------------------
C AST_HELJD -- General astrometric calculations from parameter line.
C This was written to be compatible with calls to the old subroutine from
C old Vista called HELJD.
C Input:  rarad,decrad,m,d,y,utime,lat,long
C Output: julday,heljd,lst,harad,zrad,am,v
C
 
      subroutine ast_heljd(rarad,decrad,m,d,y,utime,
     .     julday,heljd,lstrad,harad,zrad,am,v,lat,long)

      implicit none
      include 'helio.inc'
      integer*4 m
      real*8 d,y
      real*8 rarad,decrad,utime,julday,heljd,lstrad,harad
      real*8 zrad,am,v,lat,long, RADTODEG, DEGTORAD
   
      ra = RADTODEG(rarad) / 15.d0
      dec= RADTODEG(decrad)
      latitude = lat
      longitude= long
      altitude = 1300.d0     ! default is about 4000 feet (Lick)
      ut   = utime
      year = nint(y)
      month= m
      day  = nint(d)

      call helio

      julday = jd
      heljd  = hjd
      lstrad = DEGTORAD(lmst*15.d0)
      harad  = DEGTORAD(ha*15.d0)
      zrad   = DEGTORAD(tzd)
      am     = airmass
      v      = vhelio
      
      return
      end
       

C-----------------------------------------------------------------------
C HELIO -- General astrometric calculations.

      subroutine helio

      implicit none
      include 'helio.inc'
      real*8 ast_mst, ast_julday, lt
 
      call ast_date_to_epoch(year,month,day,ut,epoch)
      lmst = ast_mst(epoch,longitude)
      ha   = lmst - ra
      jd   = ast_julday(epoch) 
      call ast_hjd(ra,dec,epoch,lt,hjd)
      call ast_vrotate(ra,dec,epoch,latitude,longitude,altitude,
     .                 vdiurnal)
      call ast_vbary(ra,dec,epoch,vlunar)
      call ast_vorbit(ra,dec,epoch,vannual)
      vhelio = vannual + vlunar + vdiurnal
      call SkyStuff(ha,dec,latitude,airmass,azimuth,tzd,azd,pa)

      return
      end


C-----------------------------------------------------------------------
C DEGTORAD -- Convert degrees into radians
      real*8 function DEGTORAD(deg)
      implicit none
      real*8 deg
      DEGTORAD = 3.1415926535898d0 * deg / 180.d0
      return
      end

C-----------------------------------------------------------------------
C RADTODEG -- Convert degrees into radians
      real*8 function RADTODEG(rad)
      implicit none
      real*8 rad
      RADTODEG = 180.d0 * rad / 3.1415926535898d0
      return
      end

C-----------------------------------------------------------------------
C AST_DATE_TO_EPOCH -- Convert Gregorian date and solar mean time to
C a Julian epoch.  A Julian epoch has 365.25 days per year and 24
C hours per day.

      subroutine ast_date_to_epoch (year, month, day, ut, epoch)

      implicit none
      integer*4 year,month,day  ! Year, Month(1-12), Day of Month
      real*8    ut              ! Date universal time (mean solar day) (hours)
      real*8    epoch           ! Julian epoch
      integer*4 yr,ast_day_of_year
      real*8 r

      if (year.lt.100) then
        yr = 1900 + year
      else
        yr = year
      endif
      ut = dfloat( int(ut * 360000.d0 + 0.5d0) ) / 360000.d0
      r = dfloat(ast_day_of_year(yr,month,day))
      epoch = dfloat(yr) + ( r - 1 + ut / 24.d0 ) / 365.25d0

      return
      end


C-----------------------------------------------------------------------
C AST_EPOCH_TO_DATE -- Convert a Julian epoch to year, month, day, and time.

      subroutine ast_epoch_to_date (epoch, year, month, day, ut)

      implicit none
      real*8    epoch           ! Julian epoch
      integer*4 year,month,day  ! Year, Month(1-12), Day of Month
      real*8    ut              ! Universal time for date (in hours)
      integer*4 d,ast_day_of_year
      real*8 r

      year = int(epoch)
      r = dfloat(year)
      d = int( (epoch - r) * 365.25d0 )
      ut = ( (epoch - r) * 365.25d0 - dfloat(d) ) * 24.d0
      ut = dfloat( int(ut * 360000.d0 + 0.5d0) ) / 360000.d0
      if (ut.ge.24.d0) then
        d = d + 1
        ut = ut - 24.d0
      endif
      d = d + 1
      month = 1
      do while( d .ge. ast_day_of_year(year,month+1,1) )
        month = month + 1
      enddo
      day = d - ast_day_of_year(year,month,1) + 1

      return
      end


C-----------------------------------------------------------------------
C AST_DAY_OF_YEAR -- The day number for the given year is returned.

      integer*4 function ast_day_of_year (year, month, day)

      implicit none
      integer*4 year,month,day  ! Year, Month(1-12), Day of Month
      integer*4 d
      integer*4 bom(13)         ! Beginning of month
      data bom / 1,32,60,91,121,152,182,213,244,274,305,335,366 /

      d = bom(month) + day - 1
      if ( (month.gt.2) .and. (mod(year,4).eq.0) .and.
     .     ( (mod(year,100).ne.0).or.(mod(year,400).eq.0) ) ) d = d + 1
      ast_day_of_year = d
      return
      end

C-----------------------------------------------------------------------
C AST_DAY_OF_WEEK -- Return the day of the week for the given Julian day.
C The integer day of the week is 0=Sunday - 6=Saturday.  The character string
C is the three character abbreviation for the day of the week.  Note that
C the day of the week is for Greenwich if the standard UT is used.

      subroutine ast_day_of_week (jd, d, name, sz_name)

      implicit none
      real*8    jd              ! Julian date
      integer*4 d               ! Day of the week (0=SUN)
      character*(*) name        ! Name for day of the week
      integer*4 sz_name         ! Size of name string
      character*3 nam

      d = mod(int(jd - 0.5d0) + 2, 7)
      if (d.eq.0) then
        nam = 'SUN'
      elseif (d.eq.1) then
        nam = 'MON'
      elseif (d.eq.2) then
        nam = 'TUE'
      elseif (d.eq.3) then
        nam = 'WED'
      elseif (d.eq.4) then
        nam = 'THU'
      elseif (d.eq.5) then
        nam = 'FRI'
      elseif (d.eq.6) then
        nam = 'SAT'
      else
        nam = 'UNK'
      endif
      if (sz_name.ge.3) then
        name(1:3) = nam
      else
        name(1:max(1,sz_name)) = nam(1:max(1,sz_name))
      endif
      return
      end
        

C-----------------------------------------------------------------------
C AST_JULDAY -- Convert epoch to Julian day.

      real*8 function ast_julday (epoch)

      implicit none
      real*8    epoch               ! Epoch
      integer*4 year, century, i
      real*8    jd

      year = int(epoch) - 1
      century = year / 100
      i = 365 * year - century + int(year/4) + int(century/4)
      jd = 1721425.5d0 + dfloat(i)
      jd = jd + ( epoch - dfloat(int(epoch)) ) * 365.25d0
      ast_julday = jd
      return
      end


C-----------------------------------------------------------------------
C AST_MST -- Mean sidereal time of the epoch at the given longitude.
C This procedure may be used to optain Greenwich Mean Sidereal Time (GMST)
C by setting the longitude to 0.

      real*8 function ast_mst (epoch, longitude)

      implicit none
      real*8  epoch, longitude   ! Epoch,  Longitude in degrees
      real*8  jd, ut, t, st, ast_julday

C Determine JD and UT, and T (JD in centuries from J2000.0).
      jd = ast_julday(epoch)
      ut = (jd - dfloat(int(jd)) - 0.5d0) * 24.d0
      t  = (jd - 2451545.d0) / 36525.d0

C The GMST at 0 UT in seconds is a power series in T.
      st = 24110.54841d0 + 
     .         t * (8640184.812866d0 + t * (0.093104d0 - t * 6.2d-6))

C Correct for longitude and convert to standard hours.
      st = mod(st / 3600.d0 + ut - longitude / 15.d0, 24.0d0)
      if (st.lt.0) st = st + 24
    
      ast_mst = st

      return
      end


C-----------------------------------------------------------------------
C AST_COORD -- Convert spherical coordinates to new system.
C
C This procedure converts the longitude-latitude coordinates (a1, b1)
C of a point on a sphere into corresponding coordinates (a2, b2) in a
C different coordinate system that is specified by the coordinates of its
C origin (ao, bo).  The range of a2 will be from -pi to pi.

      subroutine ast_coord (ao, bo, ap, bp, a1, b1, a2, b2)

      implicit none
      real*8  ao, bo          ! Origin of new coordinates (radians)
      real*8  ap, bp          ! Pole of new coordinates (radians)
      real*8  a1, b1          ! Coordinates to be converted (radians)
      real*8  a2, b2          ! Converted coordinates (radians)
      
      real*8  sao, cao, sbo, cbo, sbp, cbp
      real*8  x, y, z, xp, yp, zp, temp

      x  = dcos(a1) * dcos(b1)
      y  = dsin(a1) * dcos(b1)
      z  = dsin(b1)
      xp = dcos(ap) * dcos(bp)
      yp = dsin(ap) * dcos(bp)
      zp = dsin(bp)

C Rotate the origin about z.
      sao = dsin(ao)
      cao = dcos(ao)
      sbo = dsin(bo)
      cbo = dcos(bo)
      temp = -xp * sao + yp * cao
      xp = xp * cao + yp * sao
      yp = temp
      temp = -x * sao + y * cao
      x = x * cao + y * sao
      y = temp

C Rotate the origin about y.
      temp = -xp * sbo + zp * cbo
      xp = xp * cbo + zp * sbo
      zp = temp
      temp = -x * sbo + z * cbo
      x = x * cbo + z * sbo
      z = temp

C Rotate pole around x.
      sbp = zp
      cbp = yp
      temp = y * cbp + z * sbp
      y = y * sbp - z * cbp
      z = temp

C Final angular coordinates.
      a2 = atan2(y,x)
      b2 = dasin(z)
 
      return
      end



C-----------------------------------------------------------------------
C AST_VROTATE -- Radial velocity component of the observer relative to
C the center of the Earth due to the Earth's rotation.

      subroutine ast_vrotate (ra,dec,epoch,latitude,longitude,
     .                        altitude,v)

      implicit none
      real*8  ra              ! Right Ascension of observation (hours)
      real*8  dec             ! Declination of observation (degrees)
      real*8  epoch           ! Epoch of observation (Julian epoch)
      real*8  latitude        ! Latitude (degrees)
      real*8  longitude       ! Latitude (degrees)
      real*8  altitude        ! Altitude (meters)
      real*8  v               ! Velocity (km / s)

      real*8  lat, dlat, r, vc, lmst, ast_mst, DEGTORAD, TWOPI
      parameter(TWOPI=6.2831853071796d0)

C LAT is the latitude in radians.
      lat = DEGTORAD(latitude)

C Reduction of geodetic latitude to geocentric latitude (radians).
C Dlat is in arcseconds.

      dlat = -(11.d0 * 60.d0 + 32.743000d0) * dsin(2.d0 * lat) +
     .   1.163300d0 * dsin(4.d0 * lat) -0.002600d0 * dsin(6.d0 * lat)

      lat  = lat + DEGTORAD(dlat / 3600.d0)

C R is the radius vector from the Earth's center to the observer (meters).
C Vc is the corresponding circular velocity
C (meters/sidereal day converted to km / sec).
C (sidereal day = 23.934469591229 hours (1986))
      r=6378160.0d0*(0.998327073d0+0.00167643800d0*dcos(2.d0 * lat) -
     .  0.00000351d0*dcos(4.d0*lat)+0.000000008d0*dcos(6.d0 * lat))
     .    + altitude
      vc = TWOPI * (r / 1000.d0)  / (23.934469591229d0 * 3600.d0)

C Project the velocity onto the line of sight to the star.
      lmst = ast_mst(epoch,longitude)
      v = vc * dcos(lat) * dcos(DEGTORAD(dec)) * 
     .         dsin(DEGTORAD((ra-lmst)*15.d0))

      return
      end


C-----------------------------------------------------------------------
C AST_VBARY -- Radial velocity component of center of the Earth relative to
C to the barycenter of the Earth-Moon system.

      subroutine ast_vbary (ra, dec, epoch, v)

      implicit none
      real*8  ra              ! Right ascension of observation (hours)
      real*8  dec             ! Declination of observation (degrees)
      real*8  epoch           ! Julian epoch of observation
      real*8  v               ! Component of orbital velocity (km/s)

      real*8  t, oblq, omega, llong, lperi, inclin, em, anom, vmoon
      real*8  r, d, l, b, lm, bm, ast_julday, DEGTORAD, HALFPI, TWOPI
      parameter(HALFPI=1.5707963267949d0, TWOPI=6.2831853071796d0)

C T is the number of Julian centuries since J1900.
      t = (ast_julday(epoch) - 2415020.d0) / 36525.d0

C OBLQ is the mean obliquity of the ecliptic
C OMEGA is the longitude of the mean ascending node
C LLONG is the mean lunar longitude (should be 13.1763965268)
C LPERI is the mean lunar longitude of perigee
C INCLIN is the inclination of the lunar orbit to the ecliptic
C EM is the eccentricity of the lunar orbit (dimensionless)
C All quantities except the eccentricity are in degrees.

      oblq  = 23.452294d0 - 
     .  t * (0.0130125d0 + t * (0.00000164d0 - t * 0.000000503d0))
      omega = 259.183275d0 - 
     .  t * (1934.142008d0 + t * (0.002078d0 + t * 0.000002d0))
      llong = 270.434164d0 + t * 
     .  (481267.88315d0 + t * (-0.001133d0 + t * 0.0000019d0)) - omega
      lperi = 334.329556d0 + t * 
     .  (4069.034029d0 - t * (0.010325d0 + t * 0.000012d0)) - omega
      em    = 0.054900489d0
      inclin= 5.1453964d0

C Determine true longitude.  Compute mean anomaly, convert to true
C anomaly (approximate formula), and convert back to longitude.
C The mean anomaly is only approximate because LPERI should
C be the true rather than the mean longitude of lunar perigee.

      lperi = DEGTORAD(lperi)
      llong = DEGTORAD(llong)
      anom = llong - lperi
      anom=anom+(2.d0 * em - 0.25d0 * em**3.d0) * dsin(anom) + 1.25d0 * 
     .       em**2.d0 * dsin(2.d0 * anom) + 
     .       13.d0/12.d0 * em**3.d0 * dsin(3.d0 * anom)
      llong = anom + lperi

C L and B are the ecliptic longitude and latitude of the observation.
C LM and BM are the lunar longitude and latitude of the observation
C in the lunar orbital plane relative to the ascending node.

      r = DEGTORAD(ra * 15.d0)
      d = DEGTORAD(dec)
      omega = DEGTORAD(omega)
      oblq = DEGTORAD(oblq)
      inclin = DEGTORAD(inclin)

      call ast_coord(0.d0, 0.d0, -HALFPI, HALFPI-oblq, r, d, l, b)
      call ast_coord(omega,0.d0,omega-HALFPI,HALFPI-inclin,l,b,lm,bm)

C VMOON is the component of the lunar velocity perpendicular to the
C radius vector.  V is the projection onto the line of sight to the
C observation of the velocity of the Earth's center with respect to
C the Earth-Moon barycenter.  The 81.53 is the ratio of the Earth's
C mass to the Moon's mass.

      vmoon = (TWOPI / 27.321661d0) * 
     .           384403.12040d0 / sqrt (1.d0 - em**2.d0) / 86400.d0
      v = vmoon * dcos(bm) * (dsin(llong - lm) - em * dsin(lperi - lm))
      v = v / 81.53d0

      return
      end


C-----------------------------------------------------------------------
C AST_VORBIT -- Radial velocity component of the Earth-Moon barycenter
C relative to the Sun.

      subroutine ast_vorbit (ra, dec, epoch, v)

      implicit none
      real*8  ra              ! Right ascension of observation (hours)
      real*8  dec             ! Declination of observation (degrees)
      real*8  epoch           ! Julian epoch of observation
      real*8  v               ! Component of orbital velocity (km/s)

      real*8  t,manom,lperi,oblq,eccen,tanom,slong,r,d,l,b,vorb
      real*8  ast_julday, DEGTORAD, PI, HALFPI, TWOPI
      parameter(PI    = 3.1415926535898d0,
     .          HALFPI= 1.5707963267949d0,
     .          TWOPI = 6.2831853071796d0)

C T is the number of Julian centuries since J1900.
      t = (ast_julday(epoch) - 2415020.d0) / 36525.d0

C MANOM is the mean anomaly of the Earth's orbit (degrees)
C LPERI is the mean longitude of perihelion (degrees)
C OBLQ is the mean obliquity of the ecliptic (degrees)
C ECCEN is the eccentricity of the Earth's orbit (dimensionless)

      manom = 358.47583d0 + 
     .    t * (35999.04975d0 - t * (0.000150d0 + t * 0.000003d0))
      lperi = 101.22083d0 +
     .    t * (1.7191733d0 + t * (0.000453d0 + t * 0.000003d0))
      oblq  = 23.452294d0 - 
     .    t * (0.0130125d0 + t * (0.00000164d0 - t * 0.000000503d0))
      eccen = 0.01675104d0 - t * (0.00004180d0 + t * 0.000000126d0)

C Convert to principle angles
      manom = mod(manom,360.d0)
      lperi = mod(lperi,360.d0)

C Convert to radians
      r = DEGTORAD(ra * 15.d0)
      d = DEGTORAD(dec)
      manom = DEGTORAD(manom)
      lperi = DEGTORAD(lperi)
      oblq  = DEGTORAD(oblq)

C TANOM is the true anomaly (approximate formula) (radians)
      tanom=manom+(2.d0*eccen - 0.25d0*eccen**3.d0) * dsin(manom) +
     .          1.25d0 * eccen**2.d0 * dsin(2.d0 * manom) +
     .     13.d0/12.d0 * eccen**3.d0 * dsin(3.d0 * manom)

C SLONG is the true longitude of the Sun seen from the Earth (radians)
      slong = lperi + tanom + PI

C L and B are the longitude and latitude of the star in the orbital
C plane of the Earth (radians)

      call ast_coord(0.d0, 0.d0, -HALFPI, HALFPI-oblq, r, d, l, b)

C VORB is the component of the Earth's orbital velocity perpendicular
C to the radius vector (km/s) where the Earth's semi-major axis is
C 149598500 km and the year is 365.2564 days.

      vorb = ( (TWOPI / 365.2564d0) * 
     .          149598500.d0 / sqrt (1.d0 - eccen**2.d0) ) / 86400.d0

C V is the projection onto the line of sight to the observation of
C the velocity of the Earth-Moon barycenter with respect to the
C Sun (km/s).

      v= vorb * dcos(b) * ( dsin(slong - l) - eccen * dsin(lperi - l) )

      return
      end


C-----------------------------------------------------------------------
C AST_HJD -- Helocentric Julian Day

      subroutine ast_hjd (ra, dec, epoch, lt, hjd)

      implicit none
      real*8  ra              ! Right ascension of observation (hours)
      real*8  dec             ! Declination of observation (degrees)
      real*8  epoch           ! Julian epoch of observation
      real*8  lt              ! Light travel time in seconds
      real*8  hjd             ! Helocentric Julian Day

      real*8  jd,t,manom,lperi,oblq,eccen,tanom,slong,r,d,l,b,rsun
      real*8  ast_julday, PI, HALFPI, TWOPI, DEGTORAD
      parameter(PI    = 3.1415926535898d0,
     .          HALFPI= 1.5707963267949d0,
     .          TWOPI = 6.2831853071796d0)

C JD is the geocentric Julian date.
C T is the number of Julian centuries since J1900.

      jd = ast_julday (epoch)
      t = (jd - 2415020.d0) / 36525.d0

C MANOM is the mean anomaly of the Earth's orbit (degrees)
C LPERI is the mean longitude of perihelion (degrees)
C OBLQ is the mean obliquity of the ecliptic (degrees)
C ECCEN is the eccentricity of the Earth's orbit (dimensionless)

      manom = 358.47583d0 + 
     .   t * (35999.04975d0 - t * (0.000150d0 + t * 0.000003d0))
      lperi = 101.22083d0 + 
     .   t * (1.7191733d0 + t * (0.000453d0 + t * 0.000003d0))
      oblq  = 23.452294d0 - 
     .   t * (0.0130125d0 + t * (0.00000164d0 - t * 0.000000503d0))
      eccen = 0.01675104d0 - t * (0.00004180d0 + t * 0.000000126d0)

C Convert to principle angles
      manom = mod(manom,360.d0)
      lperi = mod(lperi,360.d0)

C Convert to radians
      r = DEGTORAD(ra * 15.d0)
      d = DEGTORAD(dec)
      manom = DEGTORAD(manom)
      lperi = DEGTORAD(lperi)
      oblq  = DEGTORAD(oblq)

C TANOM is the true anomaly (approximate formula) (radians)
      tanom=manom+(2.d0*eccen - 0.25d0*eccen**3.d0) * dsin(manom) +
     .          1.25d0 * eccen**2.d0 * dsin(2.d0 * manom) +
     .     13.d0/12.d0 * eccen**3.d0 * dsin(3.d0 * manom)

C SLONG is the true longitude of the Sun seen from the Earth (radians)
      slong = lperi + tanom + PI

C L and B are the longitude and latitude of the star in the orbital
C plane of the Earth (radians)

      call ast_coord (0.d0, 0.d0, -HALFPI, HALFPI-oblq, r, d, l, b)

C R is the distance to the Sun.
      rsun = (1.d0 - eccen**2.d0) / (1.d0 + eccen * dcos(tanom))

C LT is the light travel difference to the Sun
      lt = -0.005770d0 * rsun * dcos(b) * dcos(l - slong)

      hjd = jd + lt

      return
      end


C-----------------------------------------------------------------------
C SkyStuff -- Calculate zenith angle, airmass, position angle, and azimuth.
C
      subroutine SkyStuff(ha,dec,latitude,airmass,azimuth,tzd,azd,pa)

C  This subroutine calculates several quantities given the hour angle,
C    declination and latitude.
C
C INPUT:
C   ha       : Hour Angle  (0h at meridian, negative in east)
C   dec      : Declination 
C   latitude : Latitude    
C
C OUTPUT:
C   airmass  : Air Mass  (1 at zenith)   
C   azimuth  : Azimuth  (0 degrees north, 90 degrees east,...)
C   tzd      : Zenith angle (True)  (0 degrees at zenith)
C   azd      : Zenith angle (Apparent)  (0 degrees at zenith)
C   pa       : Position Angle  (0 deg red end toward south, 90 deg west,...)
       
      implicit none
      real*8 ha, dec, latitude, airmass, azimuth, tzd, azd, pa
      real*8 r, xp, yp, zp, harad, decrad, latrad
      real*8 oldazd, differr, a, b, c, x, y, z
      integer*4 i

      real*8 DEGTORAD, RADTODEG, PI, HALFPI, TWOPI
      parameter(PI    = 3.1415926535898d0,
     .          HALFPI= 1.5707963267949d0,
     .          TWOPI = 6.2831853071796d0)

      decrad = DEGTORAD(dec)
      harad  = DEGTORAD(ha * 15.d0)
      latrad = DEGTORAD(latitude)

C Now calculate azimuth and True Zenith Distance (tzd)
      zp = dsin(decrad)
      xp = dcos(decrad) * dsin(harad)
      yp = dcos(decrad) * dcos(harad)
      x  = xp
      z  = zp * dsin(latrad) + yp * dcos(latrad)
      y  = yp * dsin(latrad) - zp * dcos(latrad)
      if (abs(z).gt.1.d-20) then
        tzd = datan(dsqrt(x*x+y*y)/z)
      else
        tzd = HALFPI
      endif
      if (abs(y).gt.1.d-20) then
        azimuth = datan(x/y)
      elseif (x.gt.0.) then
        azimuth = HALFPI + PI
      elseif (x.lt.0.) then
        azimuth = PI
      endif
      if (y.gt.0.) azimuth = PI + azimuth
      if (azimuth.lt.0.) azimuth = TWOPI + azimuth
    
C Now calculate position angle... (0 deg.=red end toward south, 90deg.=west)
C Need to use x,y,z alt-azimuth euclidean coordinates and the spherical
C triangle rule: cos(a)=cos(b)cos(c)+sin(b)sin(c)cos(ANGLE) where ANGLE is
C opposite "side" a.
C
      a = HALFPI - latrad
      b = dacos( y * (-1.d0) * dcos(latrad) + z * dsin(latrad) )
      c = dacos( z )
      if ( (abs(dsin(b)).lt.1.d-20) .or. (abs(dsin(c)).lt.1.d-20) ) then
        pa = 9.d0 * PI                       ! error
      else
        r = ( dcos(a) - dcos(b) * dcos(c) ) / ( dsin(b) * dsin(c) )
        if (abs(r).gt.1.) then               ! Avoid round off problems
          pa = 9.d0 * PI
          if ( (r.ge.1.) .and.(r.lt.1.001) ) pa = 0.d0
          if ( (r.le.-1.).and.(r.gt.-1.001) ) pa = PI
        else
          pa = dacos( r )
        endif
        if (ha.lt.0.) pa = -1.d0 * pa
      endif

C Now calculate the Zenith angle (Apparent) at about 4000.0 angstroms...
C ...unless the angle is too large to do an accurate calculation.
C (pressure=760 mm Hg, T=15 deg Celius, dry air) from CRC and other sources
C
      if (RADTODEG(tzd).gt.80.) then
        azd = 9.d0 * PI                      ! error
      else

C Iterate general equation.
C
        azd = tzd
        i = 0
        differr = 1.d0
        do while ((i.lt.40).and.(differr.gt.1.d-10))
          i = i + 1
          oldazd = azd
          r = dtan(azd)
          r = 58.16d0 * r - 0.067d0 * r * r * r
          azd = tzd - DEGTORAD( r / 3600.d0 )
          differr = dabs( oldazd - azd )
        enddo

      endif
  
C Now calculate airmass...
C The Earth's average radius is about 6370 km.  The Atmosphere's thickness is
C about 11.6 km (level of tropopause at about 41 degrees latitude).
C Divide the Earth's average radius by the atmosphere's thickness.
C
      r = 6370.d0 / 11.6d0

C This formula is very inaccurate for zenith angles greater than 80.
C Note that the apparent zenith angle is used here.
C
      if ( RADTODEG(azd) .gt. 80. ) then
        airmass = 99.d0
      else
        airmass = r * dcos(PI-azd) + 
     .        dsqrt( (r*dcos(PI-azd))**2.d0 + 1.d0 + 2.d0 * r )
      endif

      azd = RADTODEG(azd)
      tzd = RADTODEG(tzd)
      pa  = RADTODEG(pa)
      azimuth = RADTODEG(azimuth)

      return
      end


C----------------------------------------------------------------------
C Calculates azimuth, true zenith distance, and local sidereal time given
C RA, Dec, UT epoch, latitude, and longitude.
C Input:
C   ra       : Right Ascension
C   dec      : Declination 
C   epoch    : Universal Time epoch (as in ast_date_to_epoch, etc.)
C   latitude : Latitude    
C   longitude: Longitude
C Output:
C   azimuth  : Azimuth  (0 degrees north, 90 degrees east,...)
C   zd       : Zenith angle (True)  (0 degrees at zenith)
C   lst      : Local Sidereal Time
C
      subroutine SkySpot(ra,dec,epoch,latitude,longitude,azimuth,zd,
     .                   lst,ha)
      implicit none
      real*8 ra,dec,epoch,latitude,longitude
      real*8 azimuth,zd,lst
      real*8 ast_mst,ha,airmass,tzd,azd,pa
      lst = ast_mst(epoch,longitude)
      ha  = lst - ra
      if (ha.gt.+12.) ha = ha - 24. 
      if (ha.lt.-12.) ha = ha + 24. 
      call SkyStuff(ha,dec,latitude,airmass,azimuth,tzd,azd,pa)
      zd=tzd
      return
      end


C------------------------------------------------------------------------
C AST_PRECESS -- Precess coordinates from epoch1 to epoch2.
C
C The method used here is based on the new IAU system described in the
C supplement to the 1984 Astronomical Almanac.  The precession is
C done in two steps; precess epoch1 to the standard epoch J2000.0 and then
C precess from the standard epoch to epoch2.  The precession between
C any two dates is done this way because the rotation matrix coefficients
C are given relative to the standard epoch.
C

      subroutine ast_precess (ra1, dec1, epoch1, ra2, dec2, epoch2)

      implicit none
      real*8  ra1, dec1, epoch1           ! First coordinates
      real*8  ra2, dec2, epoch2           ! Second coordinates
      real*8  r0(3), r1(3), p(3,3)
      real*8  DEGTORAD, RADTODEG

C If the input epoch is 0, assume the input epoch is the same as the output
C epoch. If the two epochs are the same, return the coordinates from epoch1.

      if ((epoch1.lt.1.e-20).or.(abs(epoch1-epoch2).lt.1.e-20)) then
        ra2 = ra1
        dec2= dec1
        return
      endif

C Rectangular equatorial coordinates (direction cosines).
      ra2 = DEGTORAD(ra1 * 15.d0)
      dec2= DEGTORAD(dec1)

      r0(1) = dcos(ra2) * dcos(dec2)
      r0(2) = dsin(ra2) * dcos(dec2)
      r0(3) = dsin(dec2)

C If epoch1 is not the standard epoch then precess to the standard epoch.

      if (epoch1.ne.2000.) then
        call ast_rotmatrix(epoch1,p)

C Note that we multiply by the inverse of p which is the
C transpose of p.

        r1(1) = p(1, 1) * r0(1) + p(1, 2) * r0(2) + p(1, 3) * r0(3)
        r1(2) = p(2, 1) * r0(1) + p(2, 2) * r0(2) + p(2, 3) * r0(3)
        r1(3) = p(3, 1) * r0(1) + p(3, 2) * r0(2) + p(3, 3) * r0(3)
        r0(1) = r1(1)
        r0(2) = r1(2)
        r0(3) = r1(3)
      endif

C If epoch2 is not the standard epoch then precess from the standard
C epoch to the desired epoch.

      if (epoch2.ne.2000.) then
        call ast_rotmatrix (epoch2, p)
        r1(1) = p(1, 1) * r0(1) + p(2, 1) * r0(2) + p(3, 1) * r0(3)
        r1(2) = p(1, 2) * r0(1) + p(2, 2) * r0(2) + p(3, 2) * r0(3)
        r1(3) = p(1, 3) * r0(1) + p(2, 3) * r0(2) + p(3, 3) * r0(3)
        r0(1) = r1(1)
        r0(2) = r1(2)
        r0(3) = r1(3)
      endif

C Convert from radians to hours and degrees.
      ra2 = RADTODEG(atan2(r0(2),r0(1))/15.d0)
      dec2= RADTODEG(dasin(r0(3)))
      if (ra2.lt.0.) ra2 = ra2 + 24.d0

      return
      end


C------------------------------------------------------------------------
C ROTMATRIX -- Compute the precession rotation matrix from the standard epoch
C J2000.0 to the specified epoch.

      subroutine ast_rotmatrix (epoch, p)

      implicit none
      real*8  epoch                     ! Epoch of date
      real*8  p(3, 3)                   ! Rotation matrix
      real*8  t, a, b, c, ca, cb, cc, sa, sb, sc
      real*8  ast_julday, DEGTORAD

C The rotation matrix coefficients are polynomials in time measured in Julian
C centuries from the standard epoch.  The coefficients are in degrees.

      t = (ast_julday(epoch) - 2451545.d0) / 36525.d0

      a = t * (0.6406161d0 + t * (0.0000839d0 + t * 0.0000050d0))
      b = t * (0.6406161d0 + t * (0.0003041d0 + t * 0.0000051d0))
      c = t * (0.5567530d0 - t * (0.0001185d0 + t * 0.0000116d0))

C Compute the cosines and sines once for efficiency.
      ca = dcos(DEGTORAD(a))
      sa = dsin(DEGTORAD(a))
      cb = dcos(DEGTORAD(b))
      sb = dsin(DEGTORAD(b))
      cc = dcos(DEGTORAD(c))
      sc = dsin(DEGTORAD(c))

C Compute the rotation matrix from the sines and cosines.
      p(1, 1) = ca * cb * cc - sa * sb
      p(2, 1) = -1. * sa * cb * cc - ca * sb
      p(3, 1) = -1. * cb * sc
      p(1, 2) = ca * sb * cc + sa * cb
      p(2, 2) = -1. * sa * sb * cc + ca * cb
      p(3, 2) = -1. * sb * sc
      p(1, 3) = ca * sc
      p(2, 3) = -1. * sa * sc
      p(3, 3) = cc
    
      return 
      end



C.............................................................................
C.............................................................................
C..Below are various low precision routines for position of the Sun and Moon..
C.............................................................................
C.............................................................................

C----------------------------------------------------------------------
C (New routine written/modified July 2008.)
C
C Get Sun data during the day on a certain local date.
C
      subroutine Sun_Daytime(year,month,uday,zone,latitude,longitude,
     .                   sunloc,sunsid,sundat)
      implicit none
C inputs:
      integer*4 year,month,uday    ! local date
      real*8 zone                  ! hours offset from GMT (+8.0 for PST)
      real*8 latitude,longitude    ! geographical coordinates of observer
C outputs:
      real*8 sunloc(8)   ! decimal hours local time for above horizon of:
                         ! +40deg,+30deg,+20deg,-1deg,-1deg,+20deg,+30deg,+40deg.
      real*8 sunsid(8)   ! decimal hours sidereal time.
      real*8 sundat(2)   ! Sun RA and Dec at noon local time.
C
      integer*4 day
      real*8 epoch,zd,pzd
      real*8 ut,ep_noon
C There are 8766 hours in a year of 365.25 days.
      real*8 ehour,eminute
      parameter(ehour  =  1.d0 / 8766.d0)
      parameter(eminute= ehour / 60.d0  )
C User day.
      day = uday
C Find Universal Time at local noon.
      ut = 12.d0 + zone
      if (ut.gt.24.) then
        day= day + 1
        ut = ut - 24.
      elseif (ut.lt.0.) then
        day= day - 1
        ut = ut + 24.
      endif
      call ast_date_to_epoch(year,month,day,ut,ep_noon)
C
C Get Sun RA and Dec at local noon.
      call Sun_RA_Dec(year,month,day,ut,sundat(1),sundat(2))
C
C Get epoch at 40 degrees above horizon.
      epoch = ep_noon
      call To_Sun_ZD_incr(epoch,latitude,longitude,50.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(1),epoch,longitude,sunsid(1))
C Get epoch at 30 degrees above horizon.
      call To_Sun_ZD_incr(epoch,latitude,longitude,60.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(2),epoch,longitude,sunsid(2))
C Get epoch at 20 degrees above horizon. 
      call To_Sun_ZD_incr(epoch,latitude,longitude,70.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(3),epoch,longitude,sunsid(3))
C Get epoch at 1 degrees below horizon. 
      call To_Sun_ZD_incr(epoch,latitude,longitude,91.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(4),epoch,longitude,sunsid(4))
C
C Skip forward until zd begins to decrease.
      call SunZD(epoch,latitude,longitude,zd)
      pzd=zd-1.d0
      do while(zd.gt.pzd)
        epoch = epoch + (ehour/10.d0)
        pzd=zd
        call SunZD(epoch,latitude,longitude,zd)
      enddo
C
C Get epoch at 1 below horizon.
      call To_Sun_ZD_decr(epoch,latitude,longitude,91.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(5),epoch,longitude,sunsid(5))
C Get epoch at 20 degrees above horizon.
      call To_Sun_ZD_decr(epoch,latitude,longitude,70.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(6),epoch,longitude,sunsid(6))
C Get epoch at 30 degrees above horizon.
      call To_Sun_ZD_decr(epoch,latitude,longitude,60.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(7),epoch,longitude,sunsid(7))
C Get epoch at 40 degrees above horizon.
      call To_Sun_ZD_decr(epoch,latitude,longitude,50.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(8),epoch,longitude,sunsid(8))
C
      return
      end

C----------------------------------------------------------------------
C Get Sun and Moon data on a certain local date.
      subroutine Sun_and_Moon(year,month,uday,zone,latitude,longitude,
     .                   sunloc,sunsid,sundat,moonloc,moonsid,moondat)
      implicit none
C inputs:
      integer*4 year,month,uday    ! local date
      real*8 zone                  ! hours offset from GMT (+8.0 for PST)
      real*8 latitude,longitude    ! geographical coordinates of observer
C outputs:
      real*8 sunloc(7)   ! decimal hours local time for:
                         ! set:1deg,12deg,18deg,middle,rise:18deg,12deg,1deg.
      real*8 sunsid(7)   ! decimal hours sidereal time.
      real*8 sundat(2)   ! Sun RA and Dec at noon local time.
      real*8 moonloc(2)  ! decimal hours local time moonset(1deg),rise(1deg).
      real*8 moonsid(2)  ! decimal hours sideral time moonset,moonrise.
      real*8 moondat(5)  ! Moon RA and Dec at midnight, moon distance in units
                         ! of Earth radii (about 6378 km), moon size in
                         ! arc-minutes, moon illumination percentage.
      integer*4 day
      real*8 epoch,zd,pzd
      real*8 ut,ep_noon,ep_sunset,ep_sunrise,ep_midpoint
      real*8 ra,dec,theta
      real*8 atandd,cosdd
C There are 8766 hours in a year of 365.25 days.
      real*8 ehour,eminute
      parameter(ehour  =  1.d0 / 8766.d0)
      parameter(eminute= ehour / 60.d0  )
C User day.
      day = uday
C Find Universal Time at local noon.
      ut = 12.d0 + zone
      if (ut.gt.24.) then
        day= day + 1
        ut = ut - 24.
      elseif (ut.lt.0.) then
        day= day - 1
        ut = ut + 24.
      endif
      call ast_date_to_epoch(year,month,day,ut,ep_noon)
C :::::::::::::::: SUN STUFF :::::::::::::::::::::::::::::::::::
C Get Sun RA and Dec at local noon.
      call Sun_RA_Dec(year,month,day,ut,sundat(1),sundat(2))
C Get epoch at 1 degree twilight zd=91.d0
      epoch = ep_noon
      call To_Sun_ZD_incr(epoch,latitude,longitude,91.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(1),epoch,longitude,sunsid(1))
      ep_sunset = epoch
C Get epoch at 12 degree twilight, zd=102.0d0
      call To_Sun_ZD_incr(epoch,latitude,longitude,102.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(2),epoch,longitude,sunsid(2))
C Get epoch at 18 degree twilight, zd=108.0d0
      call To_Sun_ZD_incr(epoch,latitude,longitude,108.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(3),epoch,longitude,sunsid(3))
C Skip forward until zd begins to decrease.
      call SunZD(epoch,latitude,longitude,zd)
      pzd=zd-1.d0
      do while(zd.gt.pzd)
        epoch = epoch + (ehour/10.d0)
        pzd=zd
        call SunZD(epoch,latitude,longitude,zd)
      enddo
C Get epoch at 18 degree twilight, zd=108.0d0
      call To_Sun_ZD_decr(epoch,latitude,longitude,108.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(5),epoch,longitude,sunsid(5))
C Get epoch at 12 degree twilight, zd=102.0d0
      call To_Sun_ZD_decr(epoch,latitude,longitude,102.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(6),epoch,longitude,sunsid(6))
C Get epoch at 1 degree twilight, zd=91.0d0
      call To_Sun_ZD_decr(epoch,latitude,longitude,91.0d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call local_times(ut,zone,sunloc(7),epoch,longitude,sunsid(7))
      ep_sunrise = epoch
C Get times for the middle of the night.
      ep_midpoint = (ep_sunrise+ep_sunset)/2.d0
      call ast_epoch_to_date(ep_midpoint,year,month,day,ut)
      call local_times(ut,zone,sunloc(4),ep_midpoint,longitude,
     .                 sunsid(4))
C :::::::::::::::: MOON STUFF :::::::::::::::::::::::::::::::::::
C Get Moon RA, Dec, and distance at the middle of the night.
      call ast_epoch_to_date(ep_midpoint,year,month,day,ut)
      call Moon_RA_Dec(year,month,day,ut,moondat(1),moondat(2),
     .                 moondat(3))
C Get Moon size in arcminutes.
C Earth radius at equator: 6378 km.
C Moon diameter: 3476 km, radius: 1738 km.
C Moon size in arcminutes.
      moondat(4) = 120.d0 * atandd( 1738.d0 / (moondat(3)*6378.d0) )
C Moon illumination percentage.
C Find angle between Sun and Moon and RA and Dec of Sun at middle of night.
      call Sun_RA_Dec(year,month,day,ut,ra,dec)
      call AS_RA_DEC(ra,dec,moondat(1),moondat(2),theta)
      theta = min(180.d0,max(0.d0,abs(theta)/60.d0))
      if (theta.le.90.) then
        moondat(5) = 50.d0*(1.d0-cosdd(theta))
      else
        moondat(5) = 50.d0 + (50.d0*(cosdd(180.d0-theta)))
      endif
C See if Moon ZD at local noon is above or below horizon.
      epoch = ep_noon
      call MoonZD(epoch,latitude,longitude,zd)
C Above horizon! We will find moonset then moonrise.
      IF (zd.lt.90.) THEN
C Skip forward until ZD is increasing.
        call MoonZD(epoch,latitude,longitude,zd)
        pzd=zd+1.d0
        do while(zd.lt.pzd)
          epoch = epoch + (ehour/10.d0)
          pzd = zd
          call MoonZD(epoch,latitude,longitude,zd)
        enddo
C Find moonset.
        call To_Moon_ZD_incr(epoch,latitude,longitude,91.d0)
        call ast_epoch_to_date(epoch,year,month,day,ut)
        call local_times(ut,zone,moonloc(1),epoch,longitude,moonsid(1))
C Skip forward until ZD is decreasing.
        call MoonZD(epoch,latitude,longitude,zd)
        pzd=zd-1.d0
        do while(zd.gt.pzd)
          epoch = epoch + (ehour/10.d0)
          pzd=zd
          call MoonZD(epoch,latitude,longitude,zd)
        enddo
C Find moonrise.
        call To_Moon_ZD_decr(epoch,latitude,longitude,91.d0)
        call ast_epoch_to_date(epoch,year,month,day,ut)
        call local_times(ut,zone,moonloc(2),epoch,longitude,moonsid(2))
C Below horizon! We will find moonrise then moonset.
      ELSE
C Skip forward until ZD is decreasing.
        call MoonZD(epoch,latitude,longitude,zd)
        pzd=zd-1.d0
        do while(zd.gt.pzd)
          epoch = epoch + (ehour/10.d0)
          pzd = zd
          call MoonZD(epoch,latitude,longitude,zd)
        enddo
C Find moonrise.
        call To_Moon_ZD_decr(epoch,latitude,longitude,91.d0)
        call ast_epoch_to_date(epoch,year,month,day,ut)
        call local_times(ut,zone,moonloc(2),epoch,longitude,moonsid(2))
C Skip forward until ZD is increasing.
        call MoonZD(epoch,latitude,longitude,zd)
        pzd=zd+1.d0
        do while(zd.lt.pzd)
          epoch = epoch + (ehour/10.d0)
          pzd=zd
          call MoonZD(epoch,latitude,longitude,zd)
        enddo
C Find moonset.
        epoch = epoch - (ehour/10.d0)
        call To_Moon_ZD_incr(epoch,latitude,longitude,91.d0)
        call ast_epoch_to_date(epoch,year,month,day,ut)
        call local_times(ut,zone,moonloc(1),epoch,longitude,moonsid(1))
      ENDIF
      return
      end
C----------------------------------------------------------------------
C Get local time decimal hour and local sidereal time given ut, zone offset,
C epoch, and longitude.
      subroutine local_times(ut,zone,lhr,epoch,longitude,lst)
      implicit none
      real*8 ut,zone,lhr,epoch,longitude,lst,ast_mst
      lhr = ut - zone
      if (lhr.gt.24.) then
        lhr = lhr - 24.
      elseif (lhr.lt.0.) then
        lhr = lhr + 24.
      endif
      lst = ast_mst(epoch,longitude)
      return
      end
C-------------------------------------------------------------------
C Get Sun's zenith distance.  Add 180 when it dips below horizon.
      subroutine SunZD(epoch,latitude,longitude,zd)
      implicit none
      real*8 epoch,latitude,longitude,zd,lst,ha,azimuth,ut,ra,dec
      integer*4 year,month,day
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call Sun_RA_Dec(year,month,day,ut,ra,dec)
      call SkySpot(ra,dec,epoch,latitude,longitude,azimuth,zd,lst,ha)
      if (zd.lt.0.) zd = zd + 180.d0
      return
      end
C----------------------------------------------------------------------
C Set epoch to noon on input, returns epoch of requested zenith distance (rzd).
C This assumes ZD is increasing.
      subroutine To_Sun_ZD_incr(epoch,latitude,longitude,rzd)
      implicit none
      real*8 epoch,latitude,longitude,rzd
      real*8 zd
C There are 8766 hours in a year.
      real*8 ehour,eminute
      parameter(ehour  =  1.d0 / 8766.d0)
      parameter(eminute= ehour / 60.d0  )
C Increase epoch by 0.1 hours until zd greater than rzd.
      zd=0.
      do while(zd.lt.rzd)
        epoch = epoch + (ehour/10.d0)
        call SunZD(epoch,latitude,longitude,zd)
      enddo
C Back up epoch and increase by 0.1 minutes until zd greater than rzd.
      zd=0.
      epoch = (epoch-(ehour/10.d0))-eminute
      do while(zd.lt.rzd)
        epoch = epoch + (eminute/10.d0)
        call SunZD(epoch,latitude,longitude,zd)
      enddo
C Back up half an increment.
      epoch = epoch - (eminute/20.d0)
      return
      end
C----------------------------------------------------------------------
C Set epoch to noon on input, returns epoch of requested zenith distance (rzd).
C This assumes ZD is decreasing.
      subroutine To_Sun_ZD_decr(epoch,latitude,longitude,rzd)
      implicit none
      real*8 epoch,latitude,longitude,rzd
      real*8 zd
C There are 8766 hours in a year.
      real*8 ehour,eminute
      parameter(ehour  =  1.d0 / 8766.d0)
      parameter(eminute= ehour / 60.d0  )
C Increase epoch by 0.1 hours until zd less than rzd.
      zd=99999.d0
      do while(zd.gt.rzd)
        epoch = epoch + (ehour/10.d0)
        call SunZD(epoch,latitude,longitude,zd)
      enddo
C Back up epoch and increase by 0.1 minutes until zd less than rzd.
      zd=99999.d0
      epoch = (epoch-(ehour/10.d0))-eminute
      do while(zd.gt.rzd)
        epoch = epoch + (eminute/10.d0)
        call SunZD(epoch,latitude,longitude,zd)
      enddo
C Back up half an increment.
      epoch = epoch - (eminute/20.d0)
      return
      end
C-------------------------------------------------------------------
C Get Moon's zenith distance.  Add 180 when it dips below horizon.
      subroutine MoonZD(epoch,latitude,longitude,zd)
      implicit none
      real*8 epoch,latitude,longitude,zd,lst,ha,azimuth,ut,ra,dec,R
      integer*4 year,month,day
      call ast_epoch_to_date(epoch,year,month,day,ut)
      call Moon_RA_Dec(year,month,day,ut,ra,dec,R)
      call SkySpot(ra,dec,epoch,latitude,longitude,azimuth,zd,lst,ha)
      if (zd.lt.0.) zd = zd + 180.d0
      return
      end
C----------------------------------------------------------------------
C Returns epoch of requested zenith distance (rzd). Assumes ZD is increasing.
      subroutine To_Moon_ZD_incr(epoch,latitude,longitude,rzd)
      implicit none
      real*8 epoch,latitude,longitude,rzd
      real*8 zd
C There are 8766 hours in a year.
      real*8 ehour,eminute
      parameter(ehour  =  1.d0 / 8766.d0)
      parameter(eminute= ehour / 60.d0  )
C Increase epoch by 0.1 hours until zd greater than rzd.
      zd=0.
      do while(zd.lt.rzd)
        epoch = epoch + (ehour/10.d0)
        call MoonZD(epoch,latitude,longitude,zd)
      enddo
C Back up epoch and increase by 0.1 minutes until zd greater than rzd.
      zd=0.
      epoch = (epoch-(ehour/10.d0))-eminute
      do while(zd.lt.rzd)
        epoch = epoch + (eminute/10.d0)
        call MoonZD(epoch,latitude,longitude,zd)
      enddo
C Back up half an increment.
      epoch = epoch - (eminute/20.d0)
      return
      end
C----------------------------------------------------------------------
C Returns epoch of requested zenith distance (rzd). Assumes ZD is decreasing.
      subroutine To_Moon_ZD_decr(epoch,latitude,longitude,rzd)
      implicit none
      real*8 epoch,latitude,longitude,rzd
      real*8 zd
C There are 8766 hours in a year.
      real*8 ehour,eminute
      parameter(ehour  =  1.d0 / 8766.d0)
      parameter(eminute= ehour / 60.d0  )
C Increase epoch by 0.1 hours until zd less than rzd.
      zd=99999.d0
      do while(zd.gt.rzd)
        epoch = epoch + (ehour/10.d0)
        call MoonZD(epoch,latitude,longitude,zd)
      enddo
C Back up epoch and increase by 0.1 minutes until zd less than rzd.
      zd=99999.d0
      epoch = (epoch-(ehour/10.d0))-eminute
      do while(zd.gt.rzd)
        epoch = epoch + (eminute/10.d0)
        call MoonZD(epoch,latitude,longitude,zd)
      enddo
C Back up half an increment.
      epoch = epoch - (eminute/20.d0)
      return
      end


C--------------------------------------------------------------------------
C Sun position calculations.                         tab  July 1994
C Taken from Sky and Telescope, August 1994 (page 84), by Roger W. Sinnott.
C Calculations the RA and DEC of the Sun at the given UT.  UT is given in year,
C month, day, and hour.  Accurate to within about 1 minute of arc.
C Whether the coordinates are 1950 or 2000 or something else is not known.
C It does not say in the Sky and Telescope article.
C
      subroutine Sun_RA_Dec(year,month,day,hour,ra,dec)
      implicit none
      integer*4 year,month,day
      real*8 hour,ra,dec
      real*8 T,TT,Z0,H,Y,M,DY,J,F
C Set time variables.
      H = hour
      Z0=H/24.d0
      Y = dfloat(year)
      M = dfloat(month)
      DY= dfloat(day)
      call calendar_to_julian(Y,M,DY,J,F)
      T=(J-2451545.d0)+F
C TT is in centuries from 1900.0
      TT=(T/36525.d0) + 1.d0
      T=T+Z0
C Get Sun's position.
      call GetSunPosition(T,TT,ra,dec)
      return
      end
C----------------------------------------------------------------------
C Fundamental arguments (Van Flandern & Pulkkinen 1979).
      subroutine GetSunPosition(T,TT,RA,DEC)
      implicit none
      real*8 T,RA,DEC
      real*8 DINT,L,G,P2,V,U,W,TT,S
      parameter(P2=2.d0*3.141592654d0)
      L = 0.779072d0 + (0.00273790931d0 * T)
      G = 0.993126d0 + (0.0027377785d0  * T) 
      L = L - DINT(L)
      G = G - DINT(G)
      L = L*P2
      G = G*P2
      V = 0.39785d0 * SIN(L)
      V = V - (0.01000d0 * SIN(L-G))
      V = V + (0.00333d0 * SIN(L+G))
      V = V - (0.00021d0 * TT * SIN(L))
      U = 1.d0 - (0.03349d0 * COS(G))
      U = U - (0.00014d0 * COS(2.d0*L))
      U = U + (0.00008d0 * COS(L))
      W = (-0.00010d0) - (0.04129d0 * SIN(2.d0*L))
      W = W + (0.03211d0 * SIN(G))
      W = W + (0.00104d0 * SIN((2.d0*L)-G))
      W = W - (0.00035d0 * SIN((2.d0*L)+G))
      W = W - (0.00008d0 * TT * SIN(G))
C Compute Sun's RA and DEC.
      S  = W / SQRT(U-(V*V))
      RA = L + ATAN(S/SQRT(1.d0-(S*S)))
      S  = V / SQRT(U)
      DEC= ATAN(S/SQRT(1.d0-(S*S)))
      RA = RA*24.d0/P2
      if (RA.lt.0) RA = RA + 24.d0
      DEC= DEC*360.d0/P2
      return
      end
C----------------------------------------------------------------------
      real*8 function DINT(r)
      real*8 r
      DINT = dfloat(INT(r))
      return
      end
C----------------------------------------------------------------------
C Convert a calendar date to a Julian date.
      subroutine calendar_to_julian(Y,M,DY,J,F)
      implicit none
      real*8 Y,M,DY,J,F
      real*8 DINT,D1,G,S,ATEMP,J3
      J3= 0
      G = 1.d0
      if (Y.lt.1583.0) G=0.d0
      D1= DINT(DY)
      F = DY - D1 - 0.5d0
      J = -1.d0 * DINT(7.d0*(DINT((M+9.d0)/12.d0)+Y)/4.d0)
      if (G.eq.0.) goto 1260
      if ((M-9.d0).lt.0.) then
        S = -1.d0
      else
        S = +1.d0
      endif
      ATEMP = ABS(M-9.d0)
      J3= DINT(Y+(S*DINT(ATEMP/7.d0)))
      J3= -1.d0 * DINT((DINT(J3/100.d0)+1.d0)*0.75d0)
1260  continue
      J = J + DINT(275.d0*M/9.d0) + D1 + (G*J3) 
      J = J + 1721027.d0 + (2.d0*G) + (367.d0*Y)
      if (F.ge.0.) return
      F = F + 1.d0
      J = J - 1.d0
      return
      end
C--------------------------------------------------------------------------
C Moon position calculations.                         tab  July 1994
C Taken from Sky and Telescope July 1989, page 78 by Roger W. Sinnott
C Calculations the RA and DEC of the Moon at the given UT.
C UT given in year, month, day, and hour.
C Accurate to within about 40 minutes of arc.
C The distance to the moon "R" is in earth radii (about 6378 km).
      subroutine Moon_RA_Dec(year,month,day,hour,ra,dec,R)
      implicit none
      integer*4 year,month,day
      real*8 hour,ra,dec,R
      real*8 T,Z0,H,Y,M,DY,J,F

C Set time variables.
      H = hour
      Z0=H/24.d0
      Y = dfloat(year)
      M = dfloat(month)
      DY= dfloat(day)
      call calendar_to_julian(Y,M,DY,J,F)
      T=(J-2451545.d0)+F
      T=T+Z0
C Get Sun's position.
      call GetMoonPosition(T,ra,dec,R)
      return
      end
C--------------------------------------------------
C Get Moon RA and DEC given T.    Fundamental Arguments.
C The distance "R5" is in earth radii (about 6378 km).
      subroutine GetMoonPosition(T,ra,dec,R5)
      implicit none
      real*8 T,ra,dec
      real*8 A5,D5,R5,S,F,L,M,D,N,G,U,V,W
      real*8 DINT,PI,PI2
      parameter(PI =3.141592654d0)
      parameter(PI2=6.283185308d0)
      L = 0.606434d0 + (0.03660110129d0*T)
      M = 0.374897d0 + (0.03629164709d0*T)
      F = 0.259091d0 + (0.03674819520d0*T)
      D = 0.827362d0 + (0.03386319198d0*T)
      N = 0.347343d0 - (0.00014709391d0*T)
      G = 0.993126d0 + (0.00273777850d0*T)
      L = L - DINT(L)
      M = M - DINT(M)
      F = F - DINT(F)
      D = D - DINT(D)
      N = N - DINT(N)
      G = G - DINT(G)
      L = L * PI2
      M = M * PI2
      F = F * PI2
      D = D * PI2
      N = N * PI2
      G = G * PI2
      V =     ( 0.39558d0 * sin(F+N) )
      V = V + ( 0.08200d0 * sin(F) )
      V = V + ( 0.03257d0 * sin((M-F)-N) )
      V = V + ( 0.01092d0 * sin(M+F+N) )
      V = V + ( 0.00666d0 * sin(M-F) )
      V = V - ( 0.00644d0 * sin((M+F-(2.d0*D))+N) )
      V = V - ( 0.00331d0 * sin((F-(2.d0*D))+N) )
      V = V - ( 0.00304d0 * sin(F-(2.d0*D)) )
      V = V - ( 0.00240d0 * sin(((M-F)-(2.d0*D))-N) )
      V = V + ( 0.00226d0 * sin(M+F) )
      V = V - ( 0.00108d0 * sin(M+F-(2.d0*D)) )
      V = V - ( 0.00079d0 * sin(F-N) )
      V = V + ( 0.00078d0 * sin(F+(2.d0*D)+N) )
      U =1.d0-( 0.10828d0 * cos(M) )
      U = U - ( 0.01880d0 * cos(M-(2.d0*D)) )
      U = U - ( 0.01479d0 * cos(2.d0*D) )
      U = U + ( 0.00181d0 * cos((2.d0*M)-(2.d0*D)) )
      U = U - ( 0.00147d0 * cos(2.d0*M) )
      U = U - ( 0.00105d0 * cos((2.d0*D)-G) )
      U = U - ( 0.00075d0 * cos((M-(2.d0*D))+G) )
      W =     ( 0.10478d0 * sin(M) )
      W = W - ( 0.04105d0 * sin((2.d0*F)+(2.d0*N)) )
      W = W - ( 0.02130d0 * sin(M-(2.d0*D)) )
      W = W - ( 0.01779d0 * sin((2.d0*F)+N) )
      W = W + ( 0.01774d0 * sin(N) )
      W = W + ( 0.00987d0 * sin(2.d0*D) )
      W = W - ( 0.00338d0 * sin((M-(2.d0*F))-(2.d0*N)) )
      W = W - ( 0.00309d0 * sin(G) )
      W = W - ( 0.00190d0 * sin(2.d0*F) )
      W = W - ( 0.00144d0 * sin(M+N) )
      W = W - ( 0.00144d0 * sin((M-(2.d0*F))-N) )
      W = W - ( 0.00113d0 * sin(M+(2.d0*F)+(2.d0*N)) )
      W = W - ( 0.00094d0 * sin((M-(2.d0*D))+G) )
      W = W - ( 0.00092d0 * sin((2.d0*M)-(2.d0*D)) )
C Compute RA, Dec, Distance.
      S = W / SQRT(U-(V*V))
      A5= L + ATAN(S/SQRT(1.d0-(S*S)))
      S = V / SQRT(U)
      D5= ATAN(S/SQRT(1.d0-(S*S)))
      R5= 60.40974d0 * SQRT(U)
C Convert to standard units.
      ra = A5*12.d0/PI
      if (ra.lt.0.) ra = 24.d0 + ra
      dec= D5*180.d0/PI
      return
      end


C----------------------------------------------------------------------
C From Cong Xu, Sep98.
C
      subroutine Zodical_Background(el,eb,day,al,zodi)
      implicit none
C
C*****************************************************************************
C* routine for estimating the Zodiacal background. 
C* Adoptted from the model of J. Good (ISSA Explanatory Supplement, App.G.)
C* with the following simplifications:
C*          (i) ignore the iclination between the Zodi plan and the
C*              eclipse plan (i=1.57 deg), i.e. assuming i=0.
C*         (ii) ignore the ellipticity of the earth orbit (e=1.6e-5),
C*              i.e. assuming e=0
C*        (iii) ignore the offsets
C* Accuracy: compared to irsky, the 12mu is over estimated about 20 percent,
C*           while the 25mu is under estimated about 20 percent. Can be fixed
C*           by decreasing t0 (now t0=266K) a bit.
C*
C* Input:
Cc      el-- ecl lon (2000) in deg, eb -- ecl lat (2000) in deg, 
Cc      day -- day counts after the Spring Equinoxi
Cc      al -- lambda in micron
C* output:
C*        zodi in mJy/pix (WIRE pixel = 15.5"*15.5"
C*                                                   CX 3.4.1998
C*
C*
C*
C* new version:
C*      In order to get better fit, the following parameters are changed:
C*                  t0=235K            (orginal t0=266K) 
C*                  rho0=1.6*1.439e-20 (orginal rho0=1.439e-20)
C*      Predictions for the zodi at positions of 3 galaxies are compared with
C*      irsky values. They are M84 (eb=14), M31 (eb=33) and NGC6946 (eb=72).  
C*      The agreements are within 5%.
C*                                                   CX 3.5.1998
C****************************************************************************
C
      real*8 el,eb,day,al,zodi
      real*8 cosdd,sindd,cta,el1,eb1,r0,x0,y0,z0,al1,a,b
      real*8 pix,rho0,f
C
      common/r0/r0
      common/earth/x0,y0,z0,al1
      common/ecl/el1,eb1
C
      al1=al
      el1=el
      eb1=eb
      r0=1.496e13      !au in cm
      cta=day/365.24*360.       
C: postion of the sun:
      x0=r0*cosdd(cta)
      y0=r0*sindd(cta)
      z0=0.   
      a=1.e-5*r0
      b=1000.*r0
      call qromo(a,b,f)
C f-- surface brightness in erg/sec/cm2/Hz/sr
      rho0=1.6*1.439e-20
      zodi=f*rho0*1.e17  ! bg in MJy/sr
C WIRE pix in arcsec**2
      pix=15.5*15.5   
      zodi=zodi*0.0235*pix
      return
      end

C----------------------------------------------------------------------
C From Cong Xu, Sep98.
C
      real*8 function emiss(rl)
      implicit none
C
      real*8 cosdd,sindd,el,eb,rl,r0,x0,y0,z0,al,t0
      real*8 absorb,planck,x,y,z,delta,r,rr,t,aa,pp
C
      common/r0/r0
      common/earth/x0,y0,z0,al
      common/ecl/el,eb
C
      x=rl*cosdd(el)
      y=rl*sindd(el)
      z=rl*sindd(eb)
      r=sqrt((x-x0)**2+(y-y0)**2)
      t0=235.
      delta=0.359
      rr=sqrt(r**2+z**2)
      t=t0*(r0/rr)**delta
      aa=absorb(r,z)
      pp=planck(al,t)
      emiss=aa*pp
      return
      end

C----------------------------------------------------------------------
C From Cong Xu, Sep98.
C
      real*8 function absorb(r,z)
      implicit none
C
      real*8 alfa,beta,gama,qq,rho,r0,r,z,ql,aa
C
      common/r0/r0
C
C rho0=1.439e-20
C
      alfa=1.803
      beta=4.973
      gama=1.265
      aa=(r0/r)**alfa
      qq=beta*(abs(z)/r)**gama
      if(qq.gt.20.)then
        ql=alog10(sngl(aa)) - 10.87
        if(ql.lt.-13.)ql=-13.
        rho=10.**ql
      elseif(qq.lt.1.e-6)then
        rho=aa
      else
        rho=aa*exp(-qq)
      endif
      absorb=rho
      return
      end

C----------------------------------------------------------------------
C From Cong Xu, Sep98.
C
        real*8 function planck(al,t)
        implicit none
C
        real*8 h,ak,c,aa,qq,ql,an,al,t
C
C the Planck function, in erg/sec/cm**2/Hz/sr
C input: al-- wavelength in micron
C        t -- temperature in K
C
        h=6.626e-27
c Planck constant
        ak=1.38e-16
c Boltzmann constant
        c=3.0e10
c the speed of light
        an=c*1.e4/al
c the frequency
        aa=2.*h*an*(an/c)**2
        qq=h*an/(ak*t)
        if(qq.gt.30.)then
          ql=alog10(sngl(aa))-13.0
          if(ql.lt.-18.)ql=-18.
          planck=10.**ql
        elseif(qq.lt.1.e-6)then
          planck=aa/qq
        else
          planck=aa/(exp(qq)-1.)
        endif
        return
        end

C----------------------------------------------------------------------
C From Cong Xu, Sep98.
C
      subroutine midpnt(a,b,s,n,it)
C
      implicit none
      integer*4 n,it,j
      real*8 a,b,sum,del,ddel,x,s,tnm,emiss
C
      if (n.eq.1) then
        s=(b-a)*emiss(0.5*(a+b))
        it=1
      else
        tnm=it
        del=(b-a)/(3.*tnm)
        ddel=del+del
        x=a+0.5*del
        sum=0.
        do 11 j=1,it
          sum=sum+emiss(x)
          x=x+ddel
          sum=sum+emiss(x)
          x=x+del
11      continue
        s=(s+(b-a)*sum/tnm)/3.
        it=3*it
      endif
      return
      end

C----------------------------------------------------------------------
C From Cong Xu, Sep98.
C
      subroutine qromo(a,b,ss)
C
      implicit none
      real*8 eps,a,b,ss,dss,x
      integer*4 jmaxp,jmax,k,km,it,j
      parameter (eps=1.e-6,jmax=14,jmaxp=jmax+1,km=4,k=km+1)
      real*8 s(jmaxp),h(jmaxp)
C
      it=0
      x = 0.0
      h(1)=1.
      do 11 j=1,jmax
        call midpnt(a,b,s(j),j,it)
        if (j.ge.k) then
          call polint(h(j-km),s(j-km),k,x,ss,dss)
          if (abs(dss).lt.eps*abs(ss)) return
        endif
        s(j+1)=s(j)
        h(j+1)=h(j)/9.
11    continue
      print *, 'TOO MANY STEPS.'
      read(*,*)
      end

C----------------------------------------------------------------------
C From Cong Xu, Sep98.
C
      subroutine polint(xa,ya,n,x,y,dy)
C
      implicit none
      integer*4 nmax,n,ns,i,mm
      real*8 dif,dift,hp,ho,den,x,y,dy,w
C
      parameter(nmax=10) 
      real*8 xa(n),ya(n),c(nmax),d(nmax)
C
      ns=1
      dif=abs(x-xa(1))
      do 11 i=1,n 
        dift=abs(x-xa(i))
        if (dift.lt.dif) then
          ns=i
          dif=dift
        endif
        c(i)=ya(i)
        d(i)=ya(i)
11    continue
      y=ya(ns)
      ns=ns-1
      do 13 mm=1,n-1
        do 12 i=1,n-mm
          ho=xa(i)-x
          hp=xa(i+mm)-x
          w=c(i+1)-d(i)
          den=ho-hp
          if(den.eq.0.) then
            print *, 'Someone placed a pause statement here, why???'
            read(*,*)
          endif
          den=w/den
          d(i)=hp*den
          c(i)=ho*den
12      continue
        if (2*ns.lt.n-mm)then
          dy=c(ns+1)
        else
          dy=d(ns)
          ns=ns-1
        endif
        y=y+dy
13    continue
      return
      end




C----------------------------------------------------------------------
C Rotate ra,dec to a new coordinate system defined by an origin (ora,odec)
C and a pole (pra,pdec).  New ra will run 0 to 360 degrees.
C
      subroutine ast_rotate_radec(ra,dec,ora,odec,pra,pdec)
C
      implicit none
      real*8  ra,dec      ! coordinates to rotate in degrees (input/output).
      real*8  ora,odec    ! new origin coordinates in degrees (input).
      real*8  pra,pdec    ! new pole coordinates in degrees (input).
C
      real*8  ao, bo      ! Origin of new coordinates (radians)
      real*8  ap, bp      ! Pole of new coordinates (radians)
      real*8  a1, b1      ! Coordinates to be converted (radians)
      real*8  a2, b2      ! Converted coordinates (radians)
C
      real*8 pi,q
      parameter(pi=3.141592654d0)
      parameter(q=180.d0/pi)
C
      a1 = ra / q
      b1 = dec/ q
      ao = ora / q
      bo = odec/ q
      ap = pra / q
      bp = pdec/ q

      call ast_coord (ao, bo, ap, bp, a1, b1, a2, b2)
      ra = a2 * q
      dec= b2 * q
      if (ra.lt.0.) ra = ra + 360.d0

      return
      end


C-----------------------------------------------------------------------
C Calculate heliocentric velocity.
C
      subroutine heliocentric_velocity(year,month,day,ut,
     .               latitude,longitude,
     .               ra,dec,altitude,vdiurnal,vlunar,vannual,vhelio)
      implicit none
C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C Input quantities:
      real*8    ra,         ! Right Ascension in hours
     .          dec,        ! Declination in degrees
     .          latitude,   ! Latitude in degrees
     .          longitude,  ! Longitude west of Greenwich meridan in degrees
     .          altitude,   ! Altitude above sea level in meters
     .          ut          ! Universal Time of date in hours (mean solar day)
      integer*4 year,       ! Year (UT)
     .          month,      ! Month (UT)
     .          day         ! Day (UT)
C Output quantities:
      real*8    vhelio,     ! Velocity correction (in km/sec) to heliocentric
                            !   rest frame (negative when observer is receding
                            !   from object relative to the Sun).
     .          vannual,    ! Velocity correction for orbital motion of the
                            !   Earth-Moon Barycenter about the Sun.
     .          vlunar,     ! Velocity correction for motion of the center of
                            !   the Earth about the Earth-Moon Barycenter.
     .          vdiurnal    ! Velocity correction for rotation of the observer
                            !   about center of the Earth.
C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      real*8 epoch
      call ast_date_to_epoch(year,month,day,ut,epoch)
      call ast_vrotate(ra,dec,epoch,latitude,longitude,
     .                     altitude,vdiurnal)
      call ast_vbary(ra,dec,epoch,vlunar)
      call ast_vorbit(ra,dec,epoch,vannual)
      vhelio = vannual + vlunar + vdiurnal
      return
      end

