/* cufio.c       Unix/Sun FITS Input/Ouput Routines      tab  apr92/aug99   */


#include "ctab.h"
#include "cmisc.h"

#define TESTMODE 0

/* Globals for cgetx function. */
int gx_stype, gx_order;
double gx_crval1, gx_cdelt1, gx_crpix1, gx_pix0, gx_coef[100];


/* ---------------------------------------------------------------------
  Return the string position of a given FITS header card position.
  Return "-1" if card not found.
  Input: card (FITS card name, max: 8 characters).
       : header (long string containing FITS header, in 2880 blocks,e.g.115200).
       : headersize (Length of header string).
*/
/*@@*/
int cgetpos(char card[], char header[], int headersize )
{
/**/
char hc[9];    /* Given header card.      */
char hc0[9];   /* Comparison header card. */ 
char hcend[9]; /* END card.               */
char cc;
int ii,jj,kk,nn;
/**/
/* Initialize (blank) header card. */
for (jj=0; jj<8; ++jj) hc[jj]=' ';
hc[8]='\0';
/* END card. */
strcpy(hcend,"END     ");
/* Copy header card.  Cannot be longer than 8 characters. */
nn=0;
while((card[nn] != '\0')&&(nn < 8)) { hc[nn] = card[nn]; ++nn; }
hc[8]='\0';
ii=-1;
kk= 0;
while (kk < headersize) {
  cc = header[kk+8];
  header[kk+8] = '\0';
  strcpy(hc0,&header[kk]);
  hc0[8]='\0';
  header[kk+8] = cc;
  if (strcmp( hc0, hc ) == 0) ii = kk;
  if (ii > -1) break;
  if (strcmp( hc0, hcend ) == 0) break;
  kk = kk + 80;
}
return(ii);
}


/* ----------------------------------------------------------------------
  Delete a new FITS header card (if it exists).
  Input: card[], header[], headersize.
  Output: header[].
*/
/*@@*/
void cunheadset(char card[], char header[], int headersize)
{
/**/
int poc,poe,ii;
/**/
/* Does the card exist? */
poc = cgetpos(card, header, headersize);
/* If so, delete this card. */
if (poc >= 0) { 
/* Find END card. */
  poe = cgetpos("END", header, headersize);
/* Abort if no END card. */
  if (poe < 0) {
    fprintf(stderr,"ERROR: cunheadset: no END card found.\n");
    exit(0);
  }
/* Move all following cards up. */
  for (ii=poc; ii<poe; ++ii) { header[ii] = header[ii+80]; }
/* Delete old END card. */
  for (ii=poe; ii<(poe+80); ++ii) { header[ii] = ' '; }
}
return;
}


/* ----------------------------------------------------------------------
  Delete a FITS header card and any multiple copies.
  Input: card[], header[], headersize.
  Output: header[].
*/
/*@@*/
void remove_FITS_card(char card[], char header[], int headersize)
{
/* Does the card exist? */
while( cgetpos(card, header, headersize) > -1 ) {
  cunheadset(card,header,headersize);
}
return;
}


/* ----------------------------------------------------------------------
  Make room for a new FITS header card.  Returns new header, and the pointer 
    position for the new card.  Also writes the card name in the new position.
  Input: card[], header[], headersize.
  Output: header[], ptr.
  Returns "1" if ok.
*/
/*@@*/
int headroom(char card[], char header[], int headersize, int *ptr)
{
/**/
int ii,jj;
/**/
/* Does the card exist? */
*ptr = cgetpos(card, header, headersize);
/* If not, make room for a new card. */
if (*ptr < 0) { 
/* Look for END card. */
  *ptr = cgetpos("END", header, headersize);
  if (*ptr < 0) {
    fprintf(stderr,"ERROR: headroom: No END card found.\n");
    return(0);
  }
/* Check to see if there is room for another card. */
  if (*ptr+161 > headersize) {
    fprintf(stderr,"ERROR: headroom: No more room in header for another card.\n");
    return(0);
  }
/* Add a new END card. */
  ii= *ptr + 80;
  header[ii+0]='E';
  header[ii+1]='N';
  header[ii+2]='D';
  for (jj=ii+3; jj<(ii+80); ++jj) { header[jj]=' '; }
}
/* Clear space at position of new card. */
for (jj=*ptr; jj<(*ptr+80); ++jj) { header[jj]=' '; }
/* Write the card name. */
substrcpy(card, 0, MIN((clc(card)),(7)) ,header, *ptr);
header[*ptr+8] = '=';
return(1);
}


/* ---------------------------------------------------------------------
  Set up for adding a card a given location (card number).
  This removes any cards with the given card name, and then adds a new
  card at the given card location number.  Card number "cardloc" would
  be added at the [(cardloc-1)*80]'th header string array location element
  number. For example, card number 3 would be added at the 160th array location.
  You may then use ccheadset, cfheadset, etc. to change the card value.
  NOTE: This can be used to force the "SIMPLE" card to be at location 1, or
        the "BITPIX" card to be at card location 2 (for example).
    Input:  card       : name of the card up the 8 characters.
    Input:  cardloc    : location for a new card.
    In/Out: header     : FITS header string.
    Input:  headersize : FITS header string size.
  WARNING: User should avoid overwriting END card or adding card after END card.
*/
/*@@*/
void SetupCardLocation(char card[], int cardloc, char header[], 
                       int headersize )
{
/**/
int ii,ptr,eoh;
char wrd[100];
/**/

/* Get rid of all occurrences of this card. */
remove_FITS_card( card, header, headersize );

/* Array location. */
ptr = (cardloc-1)*80;

/* Where is END card? */
eoh = cgetpos("END     ",header,headersize);

/* Checks. */
if (ptr < 0) {
  fprintf(stderr,"***ERROR: SetupCardLocation: cardloc must be >0.\n");
  return;
}
if (eoh < 0) {
  fprintf(stderr,"***ERROR: SetupCardLocation: No END card.\n");
  return;
}
if (eoh >= headersize-80) {
  fprintf(stderr,"***ERROR: SetupCardLocation: END card at end of string.\n");
  return;
}
if (ptr >= eoh) {
  fprintf(stderr,"***ERROR: SetupCardLocation: requested location at or past END card.\n");
  return;
}

/* Add a space at the given location. */
/* Move cards down one card space. */
for (ii=eoh; ii>=ptr; ii=ii-80 ) {
  substrcpy(header,ii,ii+79,wrd,0);
  substrcpy(wrd,0,79,header,ii+80);
}

/* Clear space. */
for (ii=ptr; ii<(ptr+80); ++ii) { header[ii]=' '; }

/* Add a dummy card with the given name. */
substrcpy(card, 0, MIN((clc(card)),(7)) ,header, ptr);

/* And an equals sign. */
header[ptr+8] = '=';

return;
}


/* ---------------------------------------------------------------------
  Set a double precision header card.
  Change an existing card, or add a new card.
  Input: card, value, header, headersize.
  Output: header.
*/
/*@@*/
void cfheadset(char card[], double value, char header[], int headersize )
{
/**/
int ptr;
char wrd[21];
/**/
/* Clear space for card. */
if ( headroom(card, header, headersize, &ptr) == 0 ) {
  fprintf(stderr,"ERROR: cfheadset: in headroom function.\n"); 
  return;
}
/* Write the value. */
sprintf(wrd,"%20.12e",value);
substrcpy(wrd,0,19,header,ptr+10);
return;
}


/* ---------------------------------------------------------------------
  Set a double precision header card.
  Change an existing card, or add a new card.
  Input: card, value, header, headersize.
         comment : string to follow value in header card.
         mode    : 0=20.12e  1=20.5f
  Output: header.
*/
/*@@*/
void cfheadset2(char card[], double value, char header[], int headersize, 
                char comment[], int mode )
{
/**/
int ptr;
char wrd[300];
char temp[200];
/**/
/* Clear space for card. */
if ( headroom(card, header, headersize, &ptr) == 0 ) {
  fprintf(stderr,"ERROR: cfheadset2: in headroom function.\n"); 
  return;
}
/* Write the value. */
if (clc(comment) > 0) {
  strcpy(temp,comment);
  strcat(temp,"                                                                 ");
  if (mode == 1) { sprintf(wrd,"%20.5f / %s" ,value,temp); } else {
                   sprintf(wrd,"%20.12e / %s",value,temp);        }
  substrcpy(wrd,0,68,header,ptr+10);
} else {
  if (mode == 1) { sprintf(wrd,"%20.5f" ,value); } else {
                   sprintf(wrd,"%20.12e",value);        }

  substrcpy(wrd,0,19,header,ptr+10);
}
return;
}

/* ---------------------------------------------------------------------
  Set an integer header card.  Change an existing card, or add a new card.
  Input: card, value, header, headersize.
  Output: header.
*/
/*@@*/
void cinheadset(char card[], int value, char header[], int headersize )
{
/**/
int ptr;
char wrd[21];
/**/
/* Clear space for card. */
if ( headroom(card, header, headersize, &ptr) == 0 ) {
  fprintf(stderr,"ERROR: cinheadset: in headroom function.\n"); 
  return;
}
/* Write the value. */
sprintf(wrd,"%20d",value);
substrcpy(wrd,0,19,header,ptr+10);
return;
}

/* ---------------------------------------------------------------------
  Set an integer header card.  Change an existing card, or add a new card.
  Input: card, value, header, headersize.
         comment : string to follow value in header card.
  Output: header.
*/
/*@@*/
void cinheadset2(char card[], int value, char header[], int headersize, char comment[] )
{
/**/
int ptr;
char wrd[300];
char temp[200];
/**/
/* Clear space for card. */
if ( headroom(card, header, headersize, &ptr) == 0 ) {
  fprintf(stderr,"ERROR: cinheadset2: in headroom function.\n"); 
  return;
}
/* Write the value. */
if (clc(comment) > 0) {
  strcpy(temp,comment);
  strcat(temp,"                                                                 ");
  sprintf(wrd,"%20d / %s",value,temp);
  substrcpy(wrd,0,68,header,ptr+10);
} else {
  sprintf(wrd,"%20d",value);
  substrcpy(wrd,0,19,header,ptr+10);
}
return;
}

/* ---------------------------------------------------------------------
  Set an character header card.  Change an existing card, or add a new card.
  Input: card, value, header, headersize.
  Output: header.
*/
/*@@*/
void ccheadset(char card[], char value[], char header[], int headersize )
{
/**/
int ii,ptr;
char wrd[70];
/**/
/* Clear space for card. */
if ( headroom(card, header, headersize, &ptr) == 0 ) {
  fprintf(stderr,"ERROR: ccheadset: in headroom function.\n"); 
  return;
}
/* Copy value.  Cannot be longer than 67 characters. */
for (ii=0; ii<67; ++ii) { wrd[ii] = ' '; }
ii=0;
while((value[ii] != '\0')&&(ii < 67)) {
  wrd[ii] = value[ii];
  ++ii;
}
/* Load card value. */
header[ptr+10] = '\47';    /* single quote (octal 47, decimal 39) */
for (ii=(ptr+11); ii<(ptr+78); ++ii) { header[ii] = wrd[ii-(ptr+11)]; }
/* Trailing quote at last non-blank character. */
ii=ptr+78;
while (header[ii] == ' ') --ii;
header[ii+1] = '\47';
return;
}

/* ---------------------------------------------------------------------
  Set a logical FITS header card.
  Input: card, value(either 'F' or 'T'), header, headersize.
  Output: header.
*/
/*@@*/
void clheadset(char card[], char value, char header[], int headersize )
{
/**/
int ptr;
/**/
/* Clear space for card. */
if ( headroom(card, header, headersize, &ptr) == 0 ) {
  fprintf(stderr,"ERROR: clheadset: in headroom function.\n"); 
  return;
}
/* Load card value, either 'T' or 'F' */ 
header[ptr+29] = value;
return;
}

/* ---------------------------------------------------------------------
  Set a logical FITS header card.
  Input: card, value(either 'F' or 'T'), header, headersize, comment.
  Output: header.
*/
/*@@*/
void clheadset2(char card[], char value, char header[], int headersize, char comment[] )
{
/**/
int ptr;
char temp[200];
/**/
/* Clear space for card. */
if ( headroom(card, header, headersize, &ptr) == 0 ) {
  fprintf(stderr,"ERROR: clheadset: in headroom function.\n"); 
  return;
}
/* Load card value, either 'T' or 'F' */ 
header[ptr+29] = value;
header[ptr+30] = ' ';
/* Add comment. */
if (clc(comment) > 0) {
  strcpy(temp,"/ ");
  strcat(temp,comment);
  strcat(temp,"                                                                 ");
  substrcpy(temp,0,48,header,ptr+31);
}
return;
}


/* ---------------------------------------------------------------------
  Find the character string value of a header card.
  Input: card (FITS card name, max: 8 characters).
       : header (long string containing FITS header, in 2880 blocks,e.g.115200).
       : headersize (Length of header string).
  Output: value (header card value, string should have at least 71 characters.)
*/
/*@@*/
void cchead(char card[], char header[], int headersize, char value[] )
{
/**/
int pp,p1,p2,ii,jj;
/**/
/* Default. */
strcpy(value," ");
/* Get card position. */
pp = cgetpos(card, header, headersize);
if (pp < 0) { return; }
/* Find first single quote. */
p1 = pp+8;
while( (header[p1] != '\47')&&(p1 < pp+79) ) { ++p1; };
/* Find first single quote, looking backwards. */
p2 = pp+79;
while( (header[p2] != '\47')&&(p2 > p1) ) { --p2; };
/* Load string. */
jj=0;
for (ii=p1+1; ii<p2; ++ii ) {
  value[jj] = header[ii];
  ++jj;
}
value[jj] = '\0'; 
ii=clc(value); 
value[ii+1]='\0';
return;
}


/* ---------------------------------------------------------------------
  Find the value of a logical card (return 0 if "F" and 1 if "T").
  Input: card (FITS card name, max: 8 characters).
       : header (long string containing FITS header, in 2880 blocks,e.g.115200).
       : headersize (Length of header string).
*/
/*@@*/
int clhead(char card[], char header[], int headersize)
{
/**/
int pp,p1;
/**/
/* Get card position. */
pp = cgetpos(card, header, headersize);
if (pp < 0) { return(0); }
/* Find first non-blank character past the equals sign. */
p1 = pp+9;
while( (header[p1] == ' ')&&(p1 < pp+79) ) { ++p1; };
if (header[p1] == 'T') return(1);
return(0);
}


/* ---------------------------------------------------------------------
  Return the double precision value of a header card.
  Return "-65001." if card not found.
  Return "-65009." if there is an error.
  Input: card (FITS card name, max: 8 characters).
       : header (long string containing FITS header, in 2880 blocks,e.g.115200).
       : headersize (Length of header string).
*/
/*@@*/
double cfhead(char card[], char header[], int headersize )
{
/**/
int ii;
double rr;
char wrd[100];
/**/
ii = cgetpos(card, header, headersize);
if (ii < 0) { return(-65001.); }
if (header[ii+8] != '=') {
  fprintf(stderr,"ERROR: cfhead: Found card but not equals sign. Card=%s\n",card);
  return(-65009.);
}
/* Create word. */
substrcpy(header,ii+9,ii+79,wrd,0);
wrd[80]='\0'; ii=clc(wrd); wrd[ii+1]='\0';
/* Find first value. */
rr = GetLineValue(wrd,1);
if (rr < -1.e+39) return(-65009.);
return( rr );
}


/* ---------------------------------------------------------------------
  Return the integer value of a header card.
  Return "-65001" if card not found.
  Return "-65009" if there is an error.
  Input: card (FITS card name, max: 8 characters).
       : header (long string containing FITS header, in 2880 blocks,e.g.115200).
       : headersize (Length of header string).
*/
/*@@*/
int cinhead(char card[], char header[], int headersize )
{
int ii;
ii = cfhead(card, header, headersize);
return(ii);
}


/* ---------------------------------------------------------------------
  Add note at the end of a FITS header card.
  First erases last note.
  WARNING: Be sure the string " / " does not appear in your character
           header card.
  WARNING: Does not allow "'" characters since this can mess up character
           string cards.
  Finds blank space after position 30-- will as much of note as will fit.
  Does not start any earlier than position 30. Maximum length of note is 47.
  Nothing done if card does not exist.
  Nothing done if not at least 20 blank spaces at end of card.
  Returns -1 if nothing done.
  Returns 1 if ok.
     Input:  card (FITS card name, max: 8 characters).
     Input:  usernote (note to add).
    In/Out:  header (string containing FITS header, in 2880 blocks, e.g.115200).
     Input:  headersize (Length of header string).
*/
/*@@*/
int cnoteheadset( char card[], char usernote[], char header[], int headersize )
{
/**/
int ptr,ii,kk,nlc,mlc,ii0,more;
char note[100];
/**/
/* Copy string to note without the apostrophes. */
nlc = MIN((clc(usernote)),(99));
for (ii=0; ii<=nlc; ++ii) {
  note[ii] = usernote[ii];
  if (note[ii] == '\047') note[ii]='_';
}
note[nlc+1]='\0';
/* Position in header. */
ptr = cgetpos( card, header, headersize );
/* Return if card does not exist. */
if (ptr < 0) return(-1);
/* Look for old note (blank-slash-blank). */
more=1;
ii0=-1;
ii=(ptr+30);
while (more == 1) {
  if ( (header[ ii+0 ]==' ')&&
       (header[ ii+1 ]=='/')&&
       (header[ ii+2 ]==' ') ) {
    ii0 = ii;
    more=0;
  } else {
    ++ii;
    if (ii > (ptr+76)) more=0;
  }
}
/* Erase note. */
if (ii0 != -1) {
  for (ii=ii0; ii<(ptr+80); ++ii) { header[ii]=' '; }
}
/* How much blank space? */
ii0=ptr+79;
while ( (ii0 >= (ptr+30))&&(header[ii0] == ' ') ) --ii0;
++ii0;
/* Enough space? */
if (ii0 > (ptr+59)) return(-1);
/* Length of note.  Cannot be longer than (ptr+79) - ii0 - 3. */
mlc = (ptr+79) - ii0 - 3;
nlc = MIN((clc(note)),(mlc));
/* Add note... Always start with " / ". */
header[ii0+0] = ' ';
header[ii0+1] = '/';
header[ii0+2] = ' ';
for (kk=0; kk<=nlc; ++kk) {
  header[(ii0+3+kk)] = note[kk];
}
return(1);
}


/* ---------------------------------------------------------------------
  Read note from the end of a FITS header card.
  Nothing done if the pattern " / " not found at or after position 30.
     Input:  card (FITS card name, max: 8 characters).
     Input:  header (string containing FITS header, in 2880 blocks, e.g.115200).
     Input:  headersize (Length of header string).
    Output:  note (note to add at and past the 38th character on card line).
  WARNING: note string must have at least 50 characters.
*/
/*@@*/
void cnotehead( char card[], char header[], int headersize, char note[] )
{
/**/
int ptr,kk,ii,ii0,more;
/**/
/* Default. */
strcpy(note,"");
/* Position in header. */
ptr = cgetpos( card, header, headersize );
/* Return if card does not exist. */
if (ptr < 0) return;
/* Look for blank-slash-blank. */
more=1;
ii0=-1;
ii=(ptr+30);
while (more == 1) {
  if ( (header[ ii+0 ]==' ')&&
       (header[ ii+1 ]=='/')&&
       (header[ ii+2 ]==' ') ) {
    ii0 = ii;
    more=0;
  } else {
    ++ii;
    if (ii > (ptr+76)) more=0;
  }
}

if (ii0 == -1) return;
/* Load note string. */
kk=0;
for (ii=(ii0+3); ii<(ptr+80); ++ii) {
  note[kk] = header[ii]; 
  ++kk;
}
note[kk]='\0';
return;
}


/* ---------------------------------------------------------------------
  Reads/Writes Standard FITS header (raw HIRES2).
    Input: headersize (maximum size of header, in 2880 byte blocks:e.g.115200).
    Input/Output: header (FITS header string).
  Returns "1" if ok.
*/
/*@@*/
int crw_fitshead_rawhires2( char header[], FILE *ffu, int headersize )
{
/**/
char fb[2880];   /* FITS Block. */
int ii,k;
/**/
/* Clear. */
for (ii=0; ii<2880; ++ii ) { fb[ii]=' '; }
/* Read FITS header (raw HIRES2). */
for (ii=0; ii<headersize; ++ii ) { header[ii]=' '; }   /* Clear header. */
header[headersize-1]='\0';
k = 0;
while(1) {
 ii = fread(&fb,1,2880,ffu);
 substrcpy(fb, 0, 2879, header, k );
 k=k+2880;
/* Found END card? */
 ii = cgetpos("END     ",header,headersize);
 if ( ii > -1) { k = ii; break; }
 if (k >= headersize) break;
}
/* #@#
if (k >= headersize){
  fprintf(stderr,"ERROR: crw_fitshead: r: No END card found.\n");
}
   #@# */
return(1);
}




/* ---------------------------------------------------------------------
  Reads/Writes Standard FITS header.
    Input: mode=0 (read)  mode=1 (write),  *ffu (input file unit).
           mode=-1 (read only one header in the binary FITS table case)
         : headersize (maximum size of header, in 2880 byte blocks:e.g.115200).
    Input/Output: header (FITS header string).
  Returns "1" if ok.
*/
/*@@*/
int crw_fitshead( char header[], int mode, FILE *ffu, int headersize )
{
/**/
char fb[2880];   /* FITS Block. */
int ii,k,fitsExtend;
/**/
/* Clear. */
for (ii=0; ii<2880; ++ii ) { fb[ii]=' '; }

/* Read FITS header. */
if ( (mode == 0)||(mode == -1) ) {
  for (ii=0; ii<headersize; ++ii ) { header[ii]=' '; }   /* Clear header. */
  header[headersize-1]='\0';
  k = 0;
  while(1) {
   ii = fread(&fb,1,2880,ffu);
   substrcpy(fb, 0, 2879, header, k );
   k=k+2880;
/* Found END card? */
   ii = cgetpos("END     ",header,headersize);
   if ( ii > -1) { k = ii; break; }
   if (k >= headersize) break;
  }
  if (k >= headersize){ 
    fprintf(stderr,"ERROR: crw_fitshead: r: No END card found.\n");
  }
/* FITS extension?  (GALEX simulation only). */
  fitsExtend = 0;
  if (cinhead("NAXIS",header,headersize) == 0) {
    if (cgetpos("EXTEND",header,headersize) > -1) {
      if ( clhead("EXTEND", header, headersize) == 1) { fitsExtend = 1; } else {
      if (cinhead("EXTEND", header, headersize) == 1) { fitsExtend = 1; } }
    }
  }
  if ( ( fitsExtend == 1 ) &&
       ( mode == 0 ) &&
       ( cinhead("NAXIS", header, headersize) == 0 ) ) {  /* GALEX only */
/* Read another header. */
    while(1) {
     ii = fread(&fb,1,2880,ffu);
     substrcpy(fb, 0, 2879, header, k );
     k=k+2880;
/* Found second END card? */
     ii = cgetpos("END     ",header,headersize);
     if ( ii > -1) { k = ii; break; }
     if (k >= headersize) break;
    }
    if (k >= headersize){ 
      fprintf(stderr,"ERROR: crw_fitshead: r: No 2nd END card found.\n");
    }
  }

/* Write FITS header. */
} else { 
if ( mode == 1 ) {
  k = 0;
  while(1) {
   substrcpy(header, k, k+2879, fb, 0 );
   ii = fwrite(&fb,1,2880,ffu);
/* Found END card? */
   ii = cgetpos("END     ",fb,2880);
   if ( ii > -1) { break; }
   k=k+2880;
   if (k >= headersize) break;
  }
  if (k >= headersize) { 
    fprintf(stderr,"ERROR: crw_fitshead: w: No END card found.\n");
  }

/* Unknown. */
} else {
  fprintf(stderr,"ERROR: crw_fitshead: mode=%d is not valid.\n",mode);

}}
return(1);
}



/* ---------------------------------------------------------------------
  Reads/Writes Standard FITS integer*2 data.
    Input: mode=0 (read)  mode=1 (write),  *ffu (input file unit).
         : npt (number of data points).
         : bzero, bscale such that:
            (true float value) = bzero + ( (scaled short int value) * bscale )
    Input/Output: fd (FITS data array).
    Returns "1" if succesful.
*/
/*@@*/
int crw_fitsshort(float fd[], int mode, FILE *ffu, int npt, 
                    double bzero, double bscale )
{
/**/
short int fb[1440];   /* FITS Block, 2880 bytes.  */
short int ww;
int ii,k;
/**/
/* Check. */
if ( ABS((bscale)) < 1.e-38 ) {
  fprintf(stderr,"ERROR: crw_fitsshort: bscale value too small : %20.10e\n",bscale);
  return(0);
}
/* Clear. */
for (ii=0; ii<1440; ++ii ) { fb[ii]=0; }
/* Choose mode. */
switch (mode) {
/* Read FITS data. */
case 0:
  for (ii=0; ii<npt; ++ii ) { fd[ii]=0.; }   /* Clear data. */
  k = 0;
  while(1) {
   ii = fread((char *)&fb,1,2880,ffu);
   for (ii=0; ii<1440; ++ii) { 
     ww = fb[ii];
     ww = ByteSwap2( ww );
     fd[ii+k] = bzero + ( bscale * ww );
   }
   k = k + 1440;
   if (k >= npt) break;
  }
  break;
/* Write FITS data. */
case 1:
  k = 0;
  while(1) {
   for (ii=0; ii<1440; ++ii) { 
     ww = ( (fd[ii+k] - bzero) / bscale );
     ww = ByteSwap2( ww );
     fb[ii]= ww;
   }
   ii = fwrite((char *)&fb,1,2880,ffu);
   k = k + 1440;
   if (k >= npt) break;
  }
  break;
default:
  fprintf(stderr,"ERROR: crw_fitsshort: mode=%d is not valid.\n",mode);
  return(0);
}
return(1);
}



/* ---------------------------------------------------------------------
  Reads/Writes Standard FITS real*4 data.
    Input: mode=0 (read)  mode=1 (write),  *ffu (input file unit).
         : npt (number of data points).
    Input/Output: fd (FITS data array).
  Return "1" if ok.
*/
/*@@*/
int crw_fitsfloat(float fd[], int mode, FILE *ffu, int npt )
{
/**/
float fb[720];   /* FITS Block, 2880 bytes.  */
float rr;
int ii,k;
/**/
/* Clear. */
for (ii=0; ii<720; ++ii ) { fb[ii]=0.; }
/* Choose mode. */
switch (mode) {
/* Read FITS data. */
case 0:
  for (ii=0; ii<npt; ++ii ) { fd[ii]=0.; }   /* Clear data. */
  k = 0;
  while(1) {
   ii = fread((char *)&fb,1,2880,ffu);
   for (ii=0; ii<720; ++ii) { 
     rr = fb[ii]; 
     rr = SwapFloat( rr );
     fd[ii+k] = rr;
   }
   k = k + 720;
   if (k >= npt) break;
  }
  break;
/* Write FITS data. */
case 1:
  k = 0;
  while(1) {
   for (ii=0; ii<720; ++ii) { 
     rr = fd[ii+k];
     rr = SwapFloat( rr );
     fb[ii] = rr;
   }
   ii = fwrite((char *)&fb,1,2880,ffu);
   k = k + 720;
   if (k >= npt) break;
  }
  break;
default:
  fprintf(stderr,"ERROR: crw_fitsfloat: mode=%d is not valid.\n",mode);
  break;
}
return(1);
}


/* ---------------------------------------------------------------------
  Add ".fits" to a filename if it doesn't have a .fits extension.
     Input: filename[] (FITS file name with or without .fits extension).
    Output: filename[] (FITS file name with .fits extension).
*/
/*@@*/
void cAddFitsExt(char filename[])
{
if (cindex(filename,".fits") == -1) strcat(filename,".fits");
return;
}

/* ---------------------------------------------------------------------
  Add ".asc" to a filename if it doesn't have a .asc extension.
     Input: filename[] (ASCII file name with or without .asc extension).
    Output: filename[] (ASCII file name with .asc extension).
*/
/*@@*/
void cAddAscExt(char filename[])
{
if (cindex(filename,".asc") == -1) strcat(filename,".asc");
return;
}

/* ---------------------------------------------------------------------
  Add ".xy" to a filename if it doesn't have a .xy extension.
     Input: filename[] (".xy" pair file name with or without .xy extension).
    Output: filename[] (".xy" file name with .xy extension).
*/
/*@@*/
void cAddXyExt(char filename[])
{
if (cindex(filename,".xy") == -1) strcat(filename,".xy");
return;
}


/* ---------------------------------------------------------------------
  Get dimensions of an image.  
  Returns number of pixels or "-1" if problem.
     Input: header[] (FITS header string, should be 115200 characters.).
          : headersize (number of bytes in header strings, should be 115200).
    Output: sc,ec,sr,er,nc,nr  
            (starting and ending column and row, number of columns and rows).
*/
/*@@*/
int cGetDimensions(char header[], int headersize, 
                   int *sc, int *ec, int *sr, int *er, int *nc, int *nr)
{
/**/
int numpix,naxis;
/**/
/* Check number of axis, but do not exit on error. */
naxis = cinhead("NAXIS",header,headersize);
if ((naxis != 1)&&(naxis != 2)) {
  fprintf(stderr,"ERROR: cGetDimensions: NAXIS is not 1 or 2.\n");
  return(-1);
}
*sc= MAX( 0, cinhead("CRVAL1",header,headersize) );
*sr= MAX( 0, cinhead("CRVAL2",header,headersize) );
*nc= MAX( 1, cinhead("NAXIS1",header,headersize) );
*nr= MAX( 1, cinhead("NAXIS2",header,headersize) );
*ec= *sc + *nc - 1;
*er= *sr + *nr - 1;
numpix = (*nc) * (*nr);
return(numpix);
}


/* ---------------------------------------------------------------------
  Create a basic FITS image file header.
     Input: header[] (FITS header string, should be 115200 characters.).
          : headersize (number of bytes in header strings, should be 115200).
          : numcol (number of columns in image).
          : numrow (number of rows in image).
    Output: header[].
*/
/*@@*/
void cMakeBasicHeader(char header[], int headersize, int numcol, int numrow )
{
/**/
int ii;
/**/
/* Clear header. */
for (ii=0; ii<headersize; ++ii) header[ii] = ' ';
/* Start header. */
/* Remember that:
     SIMPLE must be first card, and
     BITPIX must be third card, and
     NAXIS  must be fourth card, and
     NAXISi must be following cards for STANDARD FITS.
*/
/*         ----+----1----+----2----+----3 */
substrcpy("SIMPLE  =                    T", 0, 29, header,   0);
substrcpy("BITPIX  =                  -32", 0, 29, header,  80);
substrcpy("END                           ", 0, 29, header, 160);
/* Add cards. */
cinheadset("NAXIS", 2,header,headersize);
cinheadset("NAXIS1",numcol,header,headersize);
cinheadset("NAXIS2",numrow,header,headersize);
cinheadset("CRVAL1",0,header,headersize);
cinheadset("CRVAL2",0,header,headersize);
cinheadset("CDELT1",1,header,headersize);
cinheadset("CDELT2",1,header,headersize);
ccheadset("OBJECT","No Name",header,headersize);
ccheadset("DATATYPE","REAL*4",header,headersize);
return;
}


/* ---------------------------------------------------------------------
  Read a FITS header for a FITS file (raw HIRES2).
    Input: fitsfile (full filename of FITS image file.)
         : headersize (number of bytes in header strings, should be 115200).
   Output: header[] (FITS header string, should be 115200 characters).
  Returns "1" if everything runs ok. 
*/
/*@@*/
int creadfitsheaderrawhires2(char fitsfile[], char header[], int headersize )
{
/**/
FILE *ffu;
/**/
/* Open file for reading. */
ffu = fopen(fitsfile,"r");
if (ffu == NULL) {
  fprintf(stderr,
   "ERROR: creadfitsheaderrawhires2: Cannot open FITS file %s\n",fitsfile);
  return(0);
}
/* Read in FITS header string (raw HIRES2). */
if (crw_fitshead_rawhires2(header, ffu, headersize ) == 0) { 
  fclose(ffu); 
  return(0);
}
fclose(ffu); 
return(1);
}


/* ---------------------------------------------------------------------
  Read a FITS header for a FITS file.
    Input: fitsfile (full filename of FITS image file.)
         : headersize (number of bytes in header strings, should be 115200).
   Output: header[] (FITS header string, should be 115200 characters).
  Returns "1" if everything runs ok. 
*/
/*@@*/
int creadfitsheader(char fitsfile[], char header[], int headersize )
{
/**/
FILE *ffu;
/**/
/* Open file for reading. */
ffu = fopen(fitsfile,"r");
if (ffu == NULL) {
  fprintf(stderr,"ERROR: creadfitsheader: Cannot open FITS file %s\n",fitsfile);
  return(0);
}
/* Read in FITS header string. */
if (crw_fitshead(header, 0, ffu, headersize ) == 0) { 
  fclose(ffu); 
  return(0);
}
fclose(ffu); 
return(1);
}


/* ---------------------------------------------------------------------
  Read a FITS image file. Data can be either BITPIX=16 (short int) 
  or BITPIX=-32 (float), but output will be in float.
    Input: fitsfile (full filename of FITS image file.)
         : maxpix (maximum number of pixels which can be read).
         : headersize (number of bytes in header strings, should be 115200).
    Output: fd[] (FITS image data).
          : header[] (FITS header string, should be 115200 characters.).
  Returns "1" if everything runs ok. 
*/
/*@@*/
int creadfits(char fitsfile[], float fd[], int maxpix,
                                  char header[], int headersize )
{
/**/
int naxis1,naxis2,npt,bitpix,ok;
FILE *ffu;
double bzero,bscale;
/**/
/* Open file for reading. */
ffu = fopen(fitsfile,"r");
if (ffu == NULL) {
  fprintf(stderr,"ERROR: creadfits: Cannot open FITS file %s\n",fitsfile);
  return(0);
}
/* Read in FITS header string. */
if (crw_fitshead(header, 0, ffu, headersize ) == 0) { 
  fclose(ffu); 
  return(0);
}
/* Compute number of pixels and data type. */
naxis1 = cinhead("NAXIS1", header, headersize);
naxis1 = MAX(1,naxis1);
naxis2 = cinhead("NAXIS2", header, headersize);
naxis2 = MAX(1,naxis2);
npt    = naxis1*naxis2;
if (npt < 1) {
  fprintf(stderr,"ERROR: creadfits: no points in data array, npt=%d.\n",npt);
  return(0);
}
if (npt > maxpix) {
  fprintf(stderr,"ERROR: creadfits: too many points, %d; maximum is %d .\n",
                 npt,maxpix);
  return(0);
}
/* Data type. */
bitpix = cinhead("BITPIX", header, headersize);
/* Zero and scale value for short integers. */
bzero=0.; bscale=1.;
if (bitpix == 16) {
  bzero = cfhead("BZERO", header, headersize);
  bscale= cfhead("BSCALE", header, headersize);
  if (ABS((bzero +65001.)) < 1.e-38) bzero =0.; /* no card. */
  if (ABS((bzero +65009.)) < 1.e-38) bzero =0.; /* bad card. */
  if (ABS((bscale+65001.)) < 1.e-38) bscale=1.; /* no card. */
  if (ABS((bscale+65009.)) < 1.e-38) bscale=1.; /* bad card. */
}
/* Select a reading mode. */
switch (bitpix) {
/* Read short integers. */
case 16:
  ok = crw_fitsshort(fd, 0, ffu, npt, bzero, bscale );
  break;
/* Read float. */
case -32:
  ok = crw_fitsfloat(fd, 0, ffu, npt );
  break;
/* Unknown. */
default:
  fprintf(stderr,"ERROR: creadfits: Unable to read BITPIX=%d\n",bitpix);
  ok = 0;
  break;
}
fclose(ffu);
/* Now data is float. */
cunheadset("BZERO",header,headersize);
cunheadset("BSCALE",header,headersize);
cinheadset("BITPIX",-32,header,headersize);
ccheadset("DATATYPE","REAL*4",header,headersize);
return(ok);
}




/* ---------------------------------------------------------------------
  Write a FITS image file. Data can be either BITPIX=16 (signed short int) 
  or BITPIX=-32 (float), but input data array must be in float.
  For the short integer write, add a "BITPIX  = 16" header card, as
  well as a "BZERO   =" and a "BSCALE  =" header cards where:
    (scaled short integer value) = ( (true value) - BZERO ) / ( BSCALE ).
  If BZERO and BSCALE are not found, values will be computed and assigned.

    Input: fitsfile (full filename of FITS image file.)
         : headersize (number of bytes in header strings, should be 115200).
         : fd[] (FITS image data).
         : header[] (FITS header string, should be 115200 characters.).

  Returns "1" if everything runs ok. 
*/
/*@@*/
int cwritefits(char fitsfile[], float fd[], char header[], int headersize )
{
/**/
int naxis1,naxis2,npt,bitpix,ok,ii,jj;
float hi,lo;
double bzero,bscale;
FILE *ffu;
/**/
/* Compute number of pixels and data type. */
naxis1 = cinhead("NAXIS1", header, headersize);
naxis1 = MAX(1,naxis1);
naxis2 = cinhead("NAXIS2", header, headersize);
naxis2 = MAX(1,naxis2);
npt    = naxis1*naxis2;
if (npt < 1) {
  fprintf(stderr,"ERROR: cwritefits: no points in data array, npt=%d.\n",npt);
  return(0);
}
/* Data type. */
bitpix = cinhead("BITPIX", header, headersize);
/* Zero and scale value for short integers. */
bzero=0.; bscale=1.;
if (bitpix == 16) {
  bzero = cfhead("BZERO", header, headersize);
  bscale= cfhead("BSCALE", header, headersize);
  ii=1;
  if (ABS((bzero +65001.)) < 1.e-38) ii=0; /* no card. */
  if (ABS((bzero +65009.)) < 1.e-38) ii=0; /* bad card. */
  if (ABS((bscale+65001.)) < 1.e-38) ii=0; /* no card. */
  if (ABS((bscale+65009.)) < 1.e-38) ii=0; /* bad card. */
  if (ii == 0) {
    hi = fd[0];    
    lo = fd[0];    
    for (jj=0; jj<npt; ++jj) {
      if (fd[jj] > hi) hi=fd[jj];
      if (fd[jj] < lo) lo=fd[jj];
    }
    bzero = (hi + lo)/2.;
    bscale= (hi + lo)/65530.;
    printf("  cwritefits: Using zero=%18.10e and scale=%18.10e for short integer scaling.\n",bzero,bscale);
/* Add new cards to header. */
    cfheadset("BZERO",bzero,header,headersize);
    cfheadset("BSCALE",bscale,header,headersize);
  }
}
/* Set data type. */
switch (bitpix) {
case 16:
  ccheadset("DATATYPE","INTEGER*2",header,headersize);
  ccheadset("DISK","FITS",header,headersize);
  break;
case -32:
  ccheadset("DATATYPE","REAL*4",header,headersize);
  break;
default:
  break;
}
/* Open file for writing. */
ffu = fopen(fitsfile,"w");
if (ffu == NULL) {
  fprintf(stderr,"ERROR: cwritefits: Cannot open FITS file %s\n",fitsfile);
  return(0);
}
/* Write out FITS header string. */
if (crw_fitshead(header, 1, ffu, headersize ) == 0) { 
  fclose(ffu); 
  return(0);
}
/* Select a data type writing mode. */
switch (bitpix) {
/* Write short integers. */
case 16:
  ok = crw_fitsshort(fd, 1, ffu, npt, bzero, bscale );
  break;
/* Write float. */
case -32:
  ok = crw_fitsfloat(fd, 1, ffu, npt );
  break;
/* Unknown. */
default:
  fprintf(stderr,"ERROR: cwritefits: Unable to write BITPIX=%d\n",bitpix);
  ok = 0;
  break;
}
fclose(ffu);
return(ok);
}


/* ----------------------------------------------------------------------
  Get pixel or wavelength value for a spectrum given an array location.  The
  array element location "ael" is the entry number in the array of values as
  they appear in the FITS file.  (NOTE: the first value is "1" not "0"). 
  For linear wavelength spectra, if CRPIX1=1 (which it almost always is), 
  then "ael" = pixel number (as seen by Vista).
  For polynomial scale and pixel scale data, "ael"= pixel# - CRVAL1 + CRPIX1 .
 
  You must initialize this function by first calling it with "ael" = -1.  This
  will set the scale type and get the polynomial coeffecients if necessary.
 
  The function returns -1.e+30 if there is an error.
*/
/*@@*/
double cgetx(double ael, char header[], int headersize)
{
/**/
int ii,jj,order,stype;
char lc_card[100], card[100], lpoly0[100], lpoly1[100];
double crval1,cdelt1,coef[100],pix0,crpix1,xval;
/**/

/* Defaults. */
stype = 0;
order = 0;
crval1= 0.;
cdelt1= 0.;
crpix1= 0.;
pix0  = 0.;


/* Initialize. */
if (ael < 0.) {

/* Identify x-scale type and load parameters. */

/* Check. */
  if ( (cinhead("NAXIS2",header,headersize) != 1)&&
       (cinhead("NAXIS",header,headersize)  != 1)&&
       (cinhead("NAXIS",header,headersize)  != 2) ) {
    fprintf(stderr,"ERROR: cgetx: not a spectrum.\n");
    return(-1.e+30);
  }

/*  Get basic parameters.  Set to 1 if not found. */
  crpix1 = cfhead("CRPIX1",header,headersize);
  if (crpix1 < -50999.) crpix1 = 1.;
  crval1 = cfhead("CRVAL1",header,headersize);
  if (crval1 < -50999.) crval1 = 1.;
  cdelt1 = cfhead("CDELT1",header,headersize);
  if (cdelt1 < -50999.) cdelt1 = 1.;

/* Get scale type. */
  cchead("CTYPE1",header,headersize,card);
  strcpy(lc_card,card);
  clower_case(lc_card);

/* Set values. */

/* Polynomial */
  if (cindex(card,"POLY_LAMBDA") > -1) {
    stype = 1;
    order = cinhead("LAMORD",header,headersize) - 1;
    if (order > 6) {
      fprintf(stderr,"ERROR: cgetx: LAMORD > 7 : %d\n",order);
      return(-1.e+30);
    }
    cchead("LPOLY0",header,headersize,lpoly0);
    pix0 = GetLineValue(lpoly0,1);
    for (jj=0; jj <= MIN(2,order); ++jj) {
      coef[jj] = GetLineValue(lpoly0,jj+2);
    }
    if (order > 2) {
      cchead("LPOLY1",header,headersize,lpoly1);
      for (jj=3; jj <= order; ++jj) {
        coef[jj] = GetLineValue(lpoly1,jj-2);
      }
    }
  }
/* Linear */
  else { 
    if ((cindex(card,"LINEAR") > -1)||(cindex(card,"LAMBDA") > -1)) {
      stype = 2;
      if (ABS((crval1)) < 1.) {
        cdelt1 = cdelt1 * 1.e+10;  /* convert meters to Angstroms. */
        crval1 = crval1 * 1.e+10;  /* convert meters to Angstroms. */
      }
    }
/* Other linear. */
  else {
    if (cindex(card,"lambda") > -1) {
      stype = 2;
      cdelt1 = cfhead("CD1_1",header,headersize);
    }
/* Pixel. */
  else {
    if (cindex(lc_card,"pixel") > -1) {
      stype = -1;
    }
/* Unknown. */
  else { 
    stype = -2;

/* Close elses */
  }}}}

/* Set global variables. */
gx_stype  = stype;
gx_crval1 = crval1;
gx_cdelt1 = cdelt1;
gx_crpix1 = crpix1;
gx_pix0   = pix0;
gx_order  = order;
for (ii=0; ii <= gx_order; ++ii) { gx_coef[ii] = coef[ii]; }

}   /* End of initialize. */

/* Compute x value. */

switch (gx_stype) {

case -2:           /* unknown */
  xval = ael;
  break;

case -1:           /* pixel */
  xval = gx_crval1 + (ael - gx_crpix1);
  break;

case 1:            /* polynomial */
  xval = gx_crval1 + (ael - gx_crpix1) - gx_pix0;
  xval = cpolyval(gx_order+1,gx_coef,xval);
  break;

case 2:            /* linear */
  xval = gx_crval1 + ( gx_cdelt1 * (ael - gx_crpix1) );
  break;

default:           /* error unknown */
  xval=0;
  fprintf(stderr,"ERROR: cgetx: unknown stype: %d\n",gx_stype);
  break;

}

return(xval);
}


/* ---------------------------------------------------------------------
  Read a string parameter in a free format string header, such as:
  "name=value name=value name=value ..."
  Input: card, shead, sheadsize.
 Output: value (string).
  Returns the value.  Returns -9999. if not present.
  Note: "card" must include equals sign (if in shead), i.e. card is "name=".
*/
/*@@*/
void ccshead(char card[], char shead[], int sheadsize, char value[] )
{
/**/
int ptr,v1,v2;
/**/
/* Default if card not found. */
strcpy(value,"");
/* Look for card. */
ptr = cindex(shead,card);
if (ptr > -1) {                /* Found card. */
/* Find point beyond card and copy and read value. */
  v1 = ptr + clc(card) + 1;
/* Find next blank.   Note: v1 and v2 define bounds of value. */
  v2 = cindex2(shead," ",v1);
  if (v2 < 0) { v2 = clc(shead); } else { v2 = v2 - 1; }
  substrcpy(shead,v1,v2,value,0);
}
return;
}


/* ---------------------------------------------------------------------
  Read a parameter in a free format string header, such as:
  "name=value name=value name=value ..."
  Input: card, shead, sheadsize.
  Returns the value.  Returns -9999. if not present.
  Note: "card" must include equals sign (if in shead), i.e. card is "name=".
  Returns -1.e+40 if there is an error reading the value.
*/
/*@@*/
double cfshead(char card[], char shead[], int sheadsize )
{
/**/
int ptr,v1,v2;
double value;
/**/
/* Default if card not found. */
value = -9999.;
/* Look for card. */
ptr = cindex(shead,card);
if (ptr > -1) {                /* Found card. */
/* Find point beyond card and copy and read value. */
  v1 = ptr + clc(card) + 1;
/* Find next blank.   Note: v1 and v2 define bounds of value. */
  v2 = cindex2(shead," ",v1);
  if (v2 < 0) { v2 = clc(shead); } else { v2 = v2 - 1; }
  value = cvalread(shead,v1+1,v2+1);
}
return(value);
}



/* ---------------------------------------------------------------------
  Changes or adds a parameter in free format string header, such as:
  "name=value name=value name=value  -name### ..."
  Input: card, value, shead, sheadsize, mode.
  Output: shead.
  Note: "card" must include equals sign (if in shead), i.e. card is "name=".
  "mode" controls the format:
      mode = 0 : %15.8e 
      mode = 1 : %f
      mode = 2 : %7.3f
*/
/*@@*/
void cfsheadset(char card[], double value, char shead[],int sheadsize, int mode)
{
/**/
int kk,pp,ptr,v1,v2,w1,w2,mm;
char wrd[100];
char temphead[5000];
/**/

/* Write value.  Note: w1 and w2 define bounds of new value. */
switch (mode) {
case 0:
  sprintf(wrd,"%15.8e",value);
  break;
case 1:
  sprintf(wrd,"%f",value);
  break;
case 2:
  sprintf(wrd,"%7.3f",value);
  break;
default:
  sprintf(wrd,"%15.8e",value);
  break;
}
w1 = cfc(wrd);
w2 = clc(wrd);

/* Look for card. */
ptr = cindex(shead,card);

if (ptr > -1) {               /* Found card. */

/* Find point beyond end of card name. */
  v1 = ptr + clc(card) + 1;
/* Find next blank.   Note: v1 and v2 define bounds of old value. */
  v2 = cindex2(shead," ",v1);
  if (v2 < 0) { v2 = clc(shead); } else { v2 = v2 - 1; }
/* Copy first part of string. */
  strcpy(temphead,"");
  substrcpy(shead,0,v1-1,temphead,0);
  temphead[v1] = '\0';
/* Copy new value into string. */
  kk = clc(temphead);
  substrcpy(wrd,w1,w2,temphead,kk+1);
  temphead[kk+2+(w2-w1)] = '\0';
/* Copy last part of string. */
  pp = clc(shead);
  if (v2 < pp) {
    kk = clc(temphead);
    substrcpy(shead,v2+1,pp,temphead,kk+1);
    temphead[kk+2+(pp-v2+1)] = '\0';
  }
/* Copy back to shead. */
  strcpy(shead,temphead);

} else {                        /* Card not found. */

/* Copy first part of string. */
  strcpy(temphead,"");
  pp = clc(shead);
  substrcpy(shead,0,pp,temphead,0);
  temphead[pp+1] = ' ';
  temphead[pp+2] = '\0';
/* Copy new card name. */
  mm = clc(card);
  substrcpy(card,0,mm,temphead,pp+2);
  temphead[pp+3+mm] = '\0';
/* Copy new value into string. */
  kk = clc(temphead);
  substrcpy(wrd,w1,w2,temphead,kk+1);
  temphead[kk+2+(w2-w1)] = '\0';

}

/* Copy back to shead. */
  strcpy(shead,temphead);

return;
}


/* ----------------------------------------------------------------------
  Write an .xy format file with errors.  Header (if given, must start 
    with "SIMPLE").
  Input:  xyfile     (filename of .xy file).
          nn         (number of values).
          xx         (x values).
          yy         (y values).
          ee         (y error values).
          errmode    (0=do not write error values, 1=write error values).
          header     (header string in "FITS" style, blank if no header).
          headersize (size of header string in bytes).
  Returns "1" if ok.
*/
/*@@*/
int cwritexye(char xyfile[], int nn, float xx[], float yy[], float ee[], int errmode, char header[], int headersize )
{
/**/
FILE *outfu;
char line[100];
int ptr,ii;
/**/

/* Open .xy file for writing. */
outfu = fopen(xyfile,"w");
if (outfu == NULL) {
  fprintf(stderr,"ERROR: cwritexye: Cannot open XY file %s\n",xyfile);
  return(0);
}

/* Write header (if there is one). */
if (clc(header) > 0) {
  ptr=0;
  substrcpy(header,ptr,ptr+79,line,0);
  line[80]='\0'; ii = clc(line); line[ii+1]='\0';
  while ( strcmp(line,"END") != 0 ) { 
    fprintf(outfu,"%s\n",line);
    ptr = ptr + 80;
    if (ptr > headersize) {
      fprintf(stderr,"ERROR: cwritexye: No END card found in header.\n");
      fclose(outfu);
      return(0);
    }
    substrcpy(header,ptr,ptr+79,line,0);
    line[80]='\0'; ii = clc(line); line[ii+1]='\0';
  }
  fprintf(outfu,"%s\n",line);
}
    
/* Write data. */
if (errmode == 1) {
  for (ii=0; ii<nn; ++ii) {
    fprintf(outfu,"%14.7e %14.7e %14.7e\n",xx[ii],yy[ii],ee[ii]);
  }
} else {
  for (ii=0; ii<nn; ++ii) {
    fprintf(outfu,"%14.7e %14.7e\n",xx[ii],yy[ii]);
  }
}

/* Close file. */
fclose(outfu);

return(1);
}


/* ---------------------------------------------------------------------
  Read a FITS spectrum file.
    Input: file  : full filename of FITS image file.
         : headersize : maximum size of header string in bytes.
         : maxpix     : maximum number of pixels.
   Output: nn    : number of spectral points (number of columns).
         : xx    : wavelength values from header.
         : aa    : data values.
         : header: FITS header.
  Returns "1" if everything runs ok. 
*/
/*@@*/
int creadspfits(char file[], int *nn, float xx[], float aa[],
                int maxpix, char header[], int headersize )
{
/**/
int sc,ec,sr,er,nc,nr,ii;
double ael;
/**/
/* Read data. */
if (creadfits(file,aa,maxpix,header,headersize) == 0) {
  fprintf(stderr,"ERROR: creadspfits: Reading FITS file %s .\n",file);
  return(0);
}
ii = cGetDimensions(header,headersize,&sc,&ec,&sr,&er,&nc,&nr);
/* Number of spectral points. */
*nn = nc;
/* Load xx points. */
xx[0] = cgetx(-1.,header,headersize);    /* Initialize. */
if (xx[0] < -1.e+29) {
  fprintf(stderr,"ERROR: creadfitsxye: getting wavelengths from header.\n");
  return(0);
}
/* */
/* Note that the Array-Element-Location is one plus to xx[] element number. */
/* But in "cgetx" for linear scales, CRPIX1 is subtracted from ael.          */
/* CRPIX1 should be set to 1, the actual wavelength calculation would be:   */
/*   wavelength = crval1 + ( ( ael - crpix1 ) * cdelt1 )                    */
/*   wavelength = crval1 + ( ( 1+ii -  1    ) * cdelt1 )                    */
/*   wavelength = zeropoint + ( (    ii     ) * dispersion )                */
/* */
for (ii=0; ii<nc; ++ii) {
    ael = 1. + ii;
    xx[ii] = cgetx(ael,header,headersize);
}
return(1);
}


/* ---------------------------------------------------------------------
  Read an XY... file like a FITS spectrum file.  The XY file may have 
  multiple columns which are treated likes rows in a FITS image.
    Input: xyfile     : full filename of XY file.
         : headersize : maximum size of header string in bytes.
         : maxpix     : maximum number of pixels.
   Output: nn    : number of spectral points (number of columns).
         : xx    : wavelength values from data file.
         : aa    : data values.
         : header: FITS header.
  Returns "1" if everything runs ok. 
*/
/*@@*/
int creadspxy(char xyfile[], int *nn, float xx[], float aa[],
                int maxpix, char header[], int headersize )
{
/**/
FILE *infu;
char line[100];
int pixno,ptr,ii,icol,irow,ncol,nrow;
float rr;
/**/
/* Initialize header. */
for (ii=0; ii<headersize; ++ii) { header[ii] = ' '; }
header[0]='\0';
/* Open .xy file. */
infu = fopen(xyfile,"r");
if (infu == NULL) {
  fprintf(stderr,"ERROR: creadspxy: Cannot open .xy file %s\n",xyfile);
  return(0);
}
/* Try to read header. */
if ( fgetline(line,infu) != 1) {
  fprintf(stderr,"ERROR: creadspxy: EOF too soon (no lines).\n");
  return(0);
}
/* Look for SIMPLE, if not there, skip trying to read header. */
ptr=0;
if ( cindex(line,"SIMPLE") > -1 ) {
  substrcpy(line,0,clc(line),header,ptr);
  while ( cindex(line,"END") != 0 ) {
    if ( fgetline(line,infu) != 1) {
      fprintf(stderr,"ERROR: creadspxy: EOF too soon (no END card).\n");
      return(0);
    }
    ptr = ptr + 80;
    if (ptr > headersize) {
      fprintf(stderr,"ERROR: creadspxy: header too big, size>%d\n",headersize);
      return(0);
    }
    substrcpy(line,0,clc(line),header,ptr);
  }
  if ( fgetline(line,infu) != 1) {
    fprintf(stderr,"ERROR: creadspxy: EOF too soon (no data).\n");
    return(0);
  }
}

/* How many "rows" in the data? */
rr=0.;
nrow=0;
while (rr > -1.e+39) {
  ++nrow;
  rr = GetLineValueQuiet(line,nrow);
}
/* Number of "rows" (actually columns in the ASCII file) */
nrow = nrow - 1;
/* Remember that first ASCII column (row 1) is really wavelengths. */
nrow = nrow - 1;

/* We need the number of columns... must read the whole file. */
ncol=1;
while (fgetline(line,infu) == 1) { ++ncol; }
/* Re-open and read first line. */
fclose(infu);
infu = fopen(xyfile,"r");
ii = fgetline(line,infu);

/* Start new header if one not read in. */
if (ptr == 0) {

/* Start header. */
  cMakeBasicHeader(header,headersize,ncol,nrow);
  ccheadset("OBJECT",xyfile,header,headersize);
  ccheadset("FILENAME",xyfile,header,headersize);

} else {

/* Re-read the header, plus one line. */
  while (cindex(line,"END") != 0) { ii = fgetline(line,infu); }
  ii = fgetline(line,infu);

/* Load new (data determined) number of columns and number of rows. */
  cinheadset("NAXIS" ,2   ,header,headersize);
  cinheadset("NAXIS1",ncol,header,headersize);
  cinheadset("NAXIS2",nrow,header,headersize);

}

/* Just in case. */
cinheadset("BITPIX",-32,header,headersize);
ccheadset("DATATYPE","REAL*4",header,headersize);

/* Read data until end, first line already read in. */
icol = 0;
while (1) {
/* Wavelengths in first column. */
  xx[icol] = GetLineValue(line,1);
/* Data. */
  for (irow=0; irow<nrow; ++irow) {
     rr = GetLineValue(line,(irow+2));
     pixno = icol + (irow * ncol);
     aa[pixno] = rr;
  }
  if (fgetline(line,infu) != 1) break;
  ++icol;
  if (icol > maxpix) {
    fprintf(stderr,"ERROR: creadspxy: Too many points.\n");
    return(0);
  }
}
fclose(infu);

/* Number of points. */
*nn = ncol;

return(1);
}



/* ----------------------------------------------------------------------
  Reads in an .xy or a .fits file with wavelengths (from header) and
  flux (yy) and error (ee) and a third row (pp).

Scratch: scr[] : array with a size at least 3 x maxpt.

  Input: file   : .fits or .xy filename.
         maxpt  : maximum number of points in arrays.
         xxtc   : Table column number for xx[] (0 for index number).
         yytc   : Table column number for yy[] (0 for index number).
         zztc   : Table column number for zz[] (0 for index number).
         uutc   : Table column number for uu[] (0 for index number).
         vvtc   : Table column number for vv[] (0 for index number).

 Output:   npt  : number of points in spectra.
           xx   : x array.
           yy   : y array.
           zz   : z array.
           uu   : u array.
           vv   : v array.
         header : FITS style header string. 

Input: headersize : maximum size of header string.

  Returns "1" if ok.
*/
/*@@*/
int creadspecfile( float scr[], char file[], int maxpt, 
              int xxtc, int yytc, int zztc, int uutc, int vvtc, int *npt, 
              float xx[], float yy[], float zz[], float uu[], float vv[], 
              char header[], int headersize )
{
/**/
int numcol,ii,nn,ptr,ipacflag,headflag;
double rr;
char tempfile[100];
char line[1000];
FILE *infu;
/**/

/* Initialize. */
cMakeBasicHeader( header, headersize, 1, 1 );
*npt = 0;
for (ii=0; ii<maxpt; ++ii) {
  xx[ii]=0.;  yy[ii]=0.;  zz[ii]=0.;  uu[ii]=0.;  vv[ii]=0.;
}

/* Create filename and look for file. */
strcpy(tempfile,file);
if (FileExist(tempfile) == 0) {
  strcpy(tempfile,file);
  strcat(tempfile,".xy");
  if (FileExist(tempfile) == 0) {
    strcpy(tempfile,file);
    strcat(tempfile,".fits");
  }
}
if (FileExist(tempfile) == 0) {
  fprintf(stderr,"ERROR: creadspecfile: Cannot open file %s .\n",tempfile);
  exit(0);
}

/* Copy back. */
strcpy(file,tempfile);

/* FITS FILE */
if (cindex(file,".fits") > -1) {

/*  Read FITS file. */
  if (creadspfits(file,npt,xx,yy,maxpt,header,headersize) == 0) {
    fprintf(stderr,"ERROR: creadspecfile: Reading FITS file %s .\n",file);
    exit(0);
  }

} else {

/* Read FITS style or IPAC style header stuff if it exists. */
  ipacflag = 0;
  headflag = 0;
/* Open. */
  infu = fopen(file,"r");
/* First line. */
  ii = fgetline(line,infu);
  if (line[0] == '|') {
    ii = fgetline(line,infu);
    if (line[0] == '|') { ipacflag = 1; }
  }
/* Check for END card. */
  while ( fgetline(line,infu) == 1 ) {
    if (cindex(line,"END") == 0) { headflag = 1;  break; }
  }
/* Close. */
  fclose(infu);
/* Re-open */
  infu = fopen(file,"r");
/* IPAC table? */
  if (ipacflag == 1) {
    ii = fgetline(line,infu);
    ii = fgetline(line,infu);
  }
/* FITS style header? */
  if (headflag == 1) {
    ptr=0;
    while ( fgetline(line,infu) == 1 ) {
      for (ii=ptr; ii<ptr+80; ++ii) { header[ii]=' '; } /* clear */
      ii = clc(line);                                   /* last character */
      substrcpy(line,0,ii,header,ptr);                  /* copy to header */
      ptr=ptr+80;
      ii = cindex(line,"END");
      if (ii == 0) break;
    }
  }
    
/* First data line.  Find number of columns. */
  ii = fgetline(line,infu);
  numcol = 0 ;
  rr = 0.;
  while ( rr != -1.e+40 ) { 
    ++numcol; 
    rr = GLVQ(line,numcol);
    if (numcol > 999) break;         /* just in case */
  }
  numcol = numcol - 1;
  if (numcol > 990) {
    fprintf(stderr,"ERROR: Too many columns, numcol >= %d .\n",numcol);
    exit(0);
  }

/* Echo. */
  printf("  Number of data columns found = %d .\n",numcol);

/* Check. */
  if ( (xxtc > numcol)||(yytc > numcol)||(zztc > numcol)||
       (uutc > numcol)||(zztc > numcol) ) {
    fprintf(stderr,"ERROR: Column number exceeds number of columns of data.\n");
    exit(0);
  }

/* Read data. */
  nn=0;
  while (1) {
    if (xxtc < 1) { xx[nn] = nn; } else { xx[nn] = GLV(line,xxtc); }
    if (yytc < 1) { yy[nn] = nn; } else { yy[nn] = GLV(line,yytc); }
    if (zztc < 1) { zz[nn] = nn; } else { zz[nn] = GLV(line,zztc); }
    if (uutc < 1) { uu[nn] = nn; } else { uu[nn] = GLV(line,uutc); }
    if (vvtc < 1) { vv[nn] = nn; } else { vv[nn] = GLV(line,vvtc); }
    ++nn;
    if (nn >= maxpt) {
      fprintf(stderr,"ERROR: Too many data points.  Maximum=%d\n",maxpt);
      exit(0);
    }
    *npt = nn;
    if (fgetline(line,infu) != 1) break;
  }

/* Close */
  fclose(infu);

}

return(1);
}



/* ----------------------------------------------------------------------
  Reads in a Todd truth spectrum file (binary FITS table).
  Input: fitsfile : (id).fits
    Output: np    : number of points in spectra.
            xx[]  : wavelengths.
            yy[]  : spectrum.
  Returns "1" if ok.
*/
/*@@*/
int creadtruthspec(char fitsfile[], int *np, float xx[], float yy[] )
{
/**/
float scr[9000];
/**/
const int headersize = 115200;
char header[headersize];
/**/
FILE *ffu;
/**/
int ii,nbytes,kk;
/**/

/* Open binary FITS file. */
ffu = fopen(fitsfile,"r");
if (ffu == NULL) {
  fprintf(stderr,"ERROR: creadtruthspec: Cannot open FITS file %s\n",fitsfile);
  return(0);
}

/* Read FITS table first header. */
ii = crw_fitshead( header,-1, ffu, headersize ); if (ii != 1) return(0);

/* Read FITS table second header. */
ii = crw_fitshead( header, 0, ffu, headersize ); if (ii != 1) return(0);

/* Number of points. */
*np = cinhead("NAXIS2",header,headersize);

/* Read points. */
nbytes = 4 * 2 * ( *np );
ii = fread(scr,1,nbytes,ffu);
if (ii != nbytes) return(0);

/* Load points. */
for (ii=0; ii<(*np); ++ii) {
  kk = ii * 2;
  xx[ii] = SwapFloat(( scr[ (kk    ) ] ));
  yy[ii] = SwapFloat(( scr[ (kk + 1) ] ));
}

/* Close file. */
fclose(ffu);

return(1);
}



/* - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . */
/* - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . - . */



/* ---------------------------------------------------------------------
  Reads Standard FITS integer*2 data (new HIRES data 2004).
   Output: fd[]       : FITS data array.
    Input: maxpix     : maximum size of fd[].
           ffu        : FITS file unit.
           header[]   : FITS header for this image.
           headersize : maximum size of header string.
    Returns "1" if succesful.
*/
/*@@*/
int chires2_readfits_raw( float fd[], int maxpix, FILE *ffu, char header[], int headersize )
{
/**/
short int fb[1440];   /* FITS Block, 2880 bytes.  */
short int ww;
/**/
int ii,k,naxis1,naxis2,npt,bitpix;
/**/
double bzero,bscale;
/**/

/* Compute number of pixels and data type. */
naxis1 = cinhead("NAXIS1", header, headersize);
naxis1 = MAX(1,naxis1);
naxis2 = cinhead("NAXIS2", header, headersize);
naxis2 = MAX(1,naxis2);
npt    = naxis1*naxis2;
if (npt < 1) {
  fprintf(stderr,"ERROR: chires2_readfits_raw: no points in data array, npt=%d.\n",npt);
  return(0);
}
if (npt > maxpix) {
  fprintf(stderr,"ERROR: chires2_readfits_raw: too many points, %d; maximum is %d .\n",npt,maxpix);
  return(0);
}

/* Data type. Note:  bzero, bscale defined such that: 
    (true float value) = bzero + ( (scaled short int value) * bscale )
*/
bitpix = cinhead("BITPIX", header, headersize);
bzero=0.; bscale=1.;
if (bitpix == 16) {
  bzero = cfhead("BZERO", header, headersize);
  bscale= cfhead("BSCALE", header, headersize);
  if (ABS((bzero +65001.)) < 1.e-38) bzero =0.; /* no card. */
  if (ABS((bzero +65009.)) < 1.e-38) bzero =0.; /* bad card. */
  if (ABS((bscale+65001.)) < 1.e-38) bscale=1.; /* no card. */
  if (ABS((bscale+65009.)) < 1.e-38) bscale=1.; /* bad card. */
} else {
  fprintf(stderr,"ERROR: chires2_readfits_raw: BITPIX must be 16.\n");
  return(0);
}

/* Check. */
if ( ABS((bscale)) < 1.e-38 ) {
  fprintf(stderr,"ERROR: chires2_readfits_raw: bscale value too small : %20.10e\n",bscale);
  return(0);
}

/* Clear. */
for (ii=0; ii<1440; ++ii ) { fb[ii]=0;  }
for (ii=0; ii<npt;  ++ii ) { fd[ii]=0.; }

/* Read FITS data. */
k = 0;
while(1) {
 ii = fread((char *)&fb,1,2880,ffu);
 for (ii=0; ii<1440; ++ii) {
   ww = fb[ii];
   ww = ByteSwap2( ww );
   fd[ii+k] = bzero + ( bscale * ww );
 }
 k = k + 1440;
 if (k >= npt) break;
}

return(1);
}



/* ---------------------------------------------------------------------
  Add in header cards from header0 that do not exist in header.
      Input: header0[]   : original FITS header.
     In/Out: header[]    : new FITS header.
      Input: headersize  : size of header strings.
*/
/*@@*/
void chires2_copy_header_cards( char header0[], char header[], int headersize )
{
/**/
int ptr0,ptr,found,ptr_END;
char cardname[20];
char cardname0[20];
char card[100];
/**/

/* Special comment. */
ccheadset("ADDCARDS","....... Add in FITS cards from first header unit, -tab ......",header,headersize);

/* Position of END card. */
ptr = 0;
while(strcmp(cardname,"END     ") != 0) {
  ptr = ptr + 80;
  substrcpy_terminate(header,ptr,ptr+7,cardname,0);
}
ptr_END = ptr;

/* Go through each header0 card and copy into header if it is not there. */
ptr0 = 0;
strcpy(cardname0,"");
while(strcmp(cardname0,"END     ") != 0) {
  substrcpy_terminate(header0,ptr0,ptr0+7,cardname0,0);
  found = 0;
  ptr = 0;
/* Exceptions. */
  if (strcmp(cardname0,"EXTEND  ") == 0) found = 1;
  if (strcmp(cardname0,"COMMENT ") == 0) found = 1;
  if (found == 0) {
    strcpy(cardname,"");
    while(strcmp(cardname,"END     ") != 0) {
      substrcpy_terminate(header,ptr,ptr+7,cardname,0);
      if (strcmp(cardname,cardname0) == 0) found = 1; 
      ptr = ptr + 80;
    }
  }
  if (strcmp(cardname0,"COMMENT ") == 0) found = 0;
/* Add in card if not found. */
  if (found == 0) {
    substrcpy(header0,ptr0,ptr0+79,card,0);
    substrcpy(card,0,79,header,ptr_END);
    ptr_END = ptr_END + 80;
    substrcpy("END     ",0,7,header,ptr_END);
    card[80]='\0';
  }
  ptr0 = ptr0 + 80;
}
return;
}



/* ---------------------------------------------------------------------
  Reads/Writes Standard FITS header.
     In/Out: header[]   : FITS header string to read or write in.
    Scratch: temphead[] : scratch.
      Input: mode       : mode=0 (read)  mode=1 (write).
           : ffu        : input or output file unit
           : headersize : the maximum size of header, in 2880 byte blocks:e.g.115200.
  Returns "1" if ok.
*/
/*@@*/
int chires2_rw_fitshead( char header[], char temphead[], int mode, FILE *ffu, int headersize )
{
/**/
char fb[2880];   /* FITS Block. */
int ii,tt,kk;
char wrd[100];
char card[81];
/**/

/* Clear. */
for (ii=0; ii<2880; ++ii ) { fb[ii]=' '; }

/* Read FITS header. */
if (mode == 0) {

/* Clear headers. */
  for (ii=0; ii<headersize; ++ii ) { header[ii]=' '; temphead[ii]=' '; }
  header[headersize-1]  ='\0';
  temphead[headersize-1]='\0';
  kk = 0;
  tt = 0;
  while(1) {
    ii = fread(&fb,1,2880,ffu);
    substrcpy(fb, 0, 2879, header, kk );
/* Transfer and clean header. */
    for (ii=kk; ii<kk+2880; ii=ii+80) {
      substrcpy_terminate(header,ii,ii+79,wrd,0);
      if (clc(wrd) > 1) {
        substrcpy(wrd,0,79,temphead,tt);
        tt=tt+80;
      }
    }
    kk = kk + 2880;
    ii = cgetpos("END     ",header,headersize);    /* Found END card? */
    if (ii > -1) { kk = ii; break; }
    if (kk >= headersize) break;
  }

/* Copy header back. */
  strcpy(header,temphead);

/* Remove XTENSION card with SIMPLE card if needed. */
  substrcpy_terminate(header,0,7,wrd,0);  
  if (strcmp(wrd,"XTENSION") == 0) {
    strcpy(card,"SIMPLE  =                    T");
/*               ----+----1----+----2----+----3    */
    for (ii=30; ii<80; ++ii) { card[ii]=' '; } 
    card[80]='\0';
    substrcpy(card,0,79,header,0);
  }

/* Check. */
  if (kk >= headersize){
    fprintf(stderr,"ERROR: chires2_rw_fitshead: r: No END card found.\n");
  }



/* Write FITS header. */
} else {
if ( mode == 1 ) {

/* Write header. */
  kk = 0;
  while(1) {
   substrcpy( header, kk, kk+2879, fb, 0 );
   ii = fwrite(&fb,1,2880,ffu);
   ii = cgetpos("END     ",fb,2880);            /* Found END card? */
   if (ii > -1) { break; }
   kk=kk+2880;
   if (kk >= headersize) break;
  }
  if (kk >= headersize) {
    fprintf(stderr,"ERROR: chires2_rw_fitshead: w: No END card found.\n");
  }

/* Unknown. */
} else {
  fprintf(stderr,"ERROR: chires2_rw_fitshead: mode=%d is not valid.\n",mode);

}}
return(1);
}



/* ---------------------------------------------------------------
  Process a HIRES 2004 raw data file by splitting it up into
  multiple files: _0.fits is the header card only HDU, _1.fits is
  the first image (CCDLOC=1), etc. up to _3.fits.  You have the choice 
  of producing all 4 files or any one of the four.
    Input: rawfile[] : raw filename.
           mode      :  0 : write out _0.fits only.
                     :  1 : write out _1.fits only.
                     :  2 : write out _2.fits only.
                     :  3 : write out _3.fits only.
                     :  4 : write out all files.
                     :  5 : write out all files, except _0.fits.
                     : -1 : information only.
           verbose   : If "1" then print out messages.
             quiet   : If "1" then do not report file reading/writing.
*/
void chires2_readwrite( char rawfile[], int mode, int verbose, int quiet )
{
/**/
const int headersize = 144000;
char header0[headersize];
char header[headersize];
char temphead[headersize];
char wholefile[200];
char wrd[200];
char wrd2[200];
/**/
int ii,naxis1,naxis2,npt,force_quiet;
int count,npt_last,maxpix;
/**/
FILE *ffu;
FILE *outfu;
/**/
float *fd;
/**/

/* Set filename. */
strcpy(wholefile,rawfile);
cAddFitsExt(wholefile);

/* Read FITS files. */
if (quiet == 0) printf("  Reading raw HIRES2 FITS file: %s .\n",wholefile);

/* Open file for reading. */
ffu = fopen(wholefile,"r");
if (ffu == NULL) {
  fprintf(stderr,"***ERROR:chires2_readwrite: Cannot open FITS file %s\n",wholefile);
  return;
}

/* Read in zeroth FITS header string. */
if (verbose == 1) printf("  Read zeroth HDU.\n");
if (chires2_rw_fitshead( header0, temphead, 0, ffu, headersize ) == 0) {
  fprintf(stderr,"***ERROR:chires2_readwrite: Error reading FITS file (0th HDU): %s\n",wholefile);
  exit(1);
}

/* Write zeroth FITS header. */
if ((mode == 0)||(mode == 4)) {
  strcpy(wrd,wholefile);
  ii = cindex(wrd,".fits");
  if (ii > 0) wrd[ii]='\0';
  strcat(wrd,"_0.fits");
  outfu = fopen(wrd,"w");
  if (quiet == 0) printf("  Writing: %s .\n",wrd);
  if (chires2_rw_fitshead( header0, temphead, 1, outfu, headersize ) == 0) {
    fprintf(stderr,"***ERROR: Error writing FITS file %s .\n",wrd);
    exit(1);
  }
  fclose(outfu);
}


/* Three HDUs with data image. */
if (mode != 0) {
maxpix   = 0;
npt_last = 0;
fd       = NULL;
for (count=1; count<=3; ++count) {

/* Exit early? */
  if (mode < count) break;

/* Force silence? */
  if ((mode != count)&&(mode != 4)&&(mode != 5)) { force_quiet=1; } else { force_quiet=quiet; }

/* Read in next image FITS header string. */
  if ((verbose == 1)&&(force_quiet == 0)) printf("  Read HDU#%d  header cards.\n",count);
  if (chires2_rw_fitshead( header, temphead, 0, ffu, headersize ) == 0) {
    fprintf(stderr,"***ERROR:chires2_readwrite: Error reading FITS file %s\n",wholefile);
    exit(1);
  }

/* Compute number of pixels and data type. */
  naxis1 = cinhead("NAXIS1", header, headersize);
  naxis1 = MAX(1,naxis1);
  naxis2 = cinhead("NAXIS2", header, headersize);
  naxis2 = MAX(1,naxis2);
  npt    = naxis1 * naxis2;
  if ((verbose == 1)&&(force_quiet == 0)) printf("  Number of columns = %d, rows = %d .\n",naxis1,naxis2);
  
/* Allocate. Must be large enough to hold an extra block of data. */
  if (npt > npt_last) {
    if (npt_last == 0) { free(fd); fd = NULL; }
    maxpix = npt + 9000;
    fd  = (float *)calloc((maxpix),sizeof(float));
  }
  
/* Read in next FITS data image. */
  if ((verbose == 1)&&(force_quiet == 0)) printf("  Read image HDU#%d data image.\n",count);
  if (chires2_readfits_raw( fd, maxpix, ffu, header, headersize ) == 0) {
    fprintf(stderr,"***ERROR:chires2_readwrite: Error reading FITS file data image: %s\n",wholefile);
    exit(1);
  }
  
/* Copy in header cards from 0th HDU. */
  chires2_copy_header_cards( header0, header, headersize );
  
/* Write image. */
  if ((mode == count)||(mode == 4)||(mode == 5)) {
    strcpy(wrd,wholefile);
    ii = cindex(wrd,".fits");
    if (ii > 0) wrd[ii]='\0';
    sprintf(wrd2,"%s_%d.fits",wrd,count);
    if ((TESTMODE == 1)&&(FileExist(wrd2) == 1)) {
      printf("  (warning): Skip writing: %s .\n",wrd2);
    } else {
      if (force_quiet == 0) printf("  Writing: %s .\n",wrd2);
      cwritefits( wrd2, fd, header, headersize );
    }
  }

/* Save. */
  npt_last = npt;

}

/* Free memory. */
if (fd != NULL) {
  free(fd); fd = NULL;
}

}

/* Close FITS file. */
fclose(ffu);

return;
}


