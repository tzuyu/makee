C
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C
C misc.f      Miscellaneous Routines            tab  April-June 1992
C

C-----------------------------------------------------------------
C Arc-Cosine to degrees.
C
      real*8 function acosdd( value )
C
      implicit none
      real*8 pi,pio180,value
      parameter( pi = 3.141592654 )
      parameter( pio180 = pi / 180. )
      acosdd = acos( value ) / pio180
      return
      end

C-----------------------------------------------------------------
C Arc-Tangent to degrees.
C
      real*8 function atandd( value )
C
      implicit none
      real*8 pi,pio180,value
      parameter( pi = 3.141592654 )
      parameter( pio180 = pi / 180. )
      atandd = atan( value ) / pio180
      return
      end


C-----------------------------------------------------------------
C Cosine from degrees.
C
      real*8 function cosdd( ThetaDeg )
C
      implicit none
      real*8 pi,pio180,ThetaDeg
      parameter( pi = 3.141592654 )
      parameter( pio180 = pi / 180. )
      cosdd = cos( ThetaDeg * pio180 )
      return
      end

C-----------------------------------------------------------------
C Sine from degrees.
C
      real*8 function sindd( ThetaDeg )
C
      implicit none
      real*8 pi,pio180,ThetaDeg
      parameter( pi = 3.141592654 )
      parameter( pio180 = pi / 180. )
      sindd = sin( ThetaDeg * pio180 )
      return
      end

C-----------------------------------------------------------------
C Tangent from degrees.
C
      real*8 function tandd( ThetaDeg )
C
      implicit none
      real*8 pi,pio180,ThetaDeg
      parameter( pi = 3.141592654 )
      parameter( pio180 = pi / 180. )
      tandd = tan( ThetaDeg * pio180 )
      return
      end


C-----------------------------------------------------------------
      real*4 function valread(ss)
      implicit none
      character*(*) ss
      real*8 r8,fgetlinevalue
      r8 = fgetlinevalue(ss,1)
      valread = r8
      return
      end

C-----------------------------------------------------------------
      real*8 function valread8(ss)
      implicit none
      character*(*) ss
      real*8 fgetlinevalue
      valread8 = fgetlinevalue(ss,1)
      return
      end

C------------------------------------------------------------
      logical function life(h)
      implicit none
      character*(*) h
      character*100 wrd2
      integer*4 time,i,ii
      i=time()
      wrd2 = '/usr/etc/ping '//h//' 5 > /dev/null'
      call sys(wrd2)
      ii=time()
      if ((ii-i).lt.4) then
        life=.true.
      else
        life=.false.
      endif 
      return
      end

C--------------------------------------------------------------
C Returns a random number between a and b.  If a >= b, then
C randomizer is initialized.
      real*4 function real_rand(a,b)
      implicit none
      real*4 a,b 
      integer*4 time,seed
      real*4 rand,c
      real*8 r
      if (a.ge.b) then
        r=dfloat(time())
        seed=3+nint(1000.d0*((r/1000.d0)-dfloat(int(r/1000.d0))))
        c=rand(seed)
        real_rand=0.
      else
        c=(a+(rand(0)*(b-a)))
        real_rand=min(max(c,a),b)
      endif
      return
      end

C--------------------------------------------------------------
C Returns a random number between i and j.  If i >= j, then
C randomizer is initialized.
      integer*4 function int_rand(i,j)
      implicit none
      integer*4 i,j,k,time,seed
      real*4 rand
      real*8 r
      if (i.ge.j) then
        r=dfloat(time())
        seed=3+nint(1000.d0*((r/1000.d0)-dfloat(int(r/1000.d0))))
        k=int(rand(seed))
        int_rand=0
      else
        k=int(float(i)+(rand(0)*(float(j-i)+0.99999)))
        int_rand=min(max(k,i),j)
      endif
      return
      end

C---------------------------------------------------------------------- 
C Shuffle a list of integers, "nshuf" times.
C
      subroutine int_shuffle(key,nkey,nshuf)
C
      implicit none
      integer*4 nkey,int_rand,k,i,nshuf,swap,ii
      integer*4 key(nkey)
      k = int_rand(1,0)
      do ii=1,nshuf
      do i=1,nkey
        k = int_rand(1,nkey)
        if (i.ne.k) then
          swap = key(i)
          key(i) = key(k)
          key(k) = swap
        endif
      enddo
      enddo
      return
      end
      

C------------------------------------------------------------
      subroutine printerr(s)
      implicit none
      character*(*) s
      character*1000 wrd
      integer*4 lastchar
      wrd = s(1:lastchar(s))//char(13)//char(10)//char(0)
      call ftostderr(wrd)
      return
      end


C--------------------------------------------------------------
C Get command line arguments.
C
      subroutine arguments(arg,narg,max)
C
      implicit none
      integer*4 narg,i,IARGC,max
      character*(*) arg(*)
      narg = min(IARGC(),max)
      do i=1,narg
        arg(i) = ' '
        call GETARG(i,arg(i))
      enddo
      return
      end

C--------------------------------------------------------------
C Just like arguments except parameters read from file "parms.dat".
C
      subroutine garguments(arg,narg,max)
C
      implicit none
      integer*4 narg,max,lastchar
      character*(*) arg(*)
      character*200 line 
      open(33,file='parms.dat',status='old')
      narg = 0 
      do while(narg.lt.max) 
        read(33,'(a)',end=5) line 
        narg = narg + 1
        arg(narg) = line(1:lastchar(line))
      enddo 
5     close(33)
      return
      end

C------------------------------------------------------------
C Finds an argument "parm" in "arg(1..narg)" and extracts the words
C "wrd(1..n)" deliminated by "d".  Returns true if argument found.  The
C argument is deleted from the argument list if found (i.e. arg is reset
C such that arg(1..narg-1) for the remaining arguments.
C
C If d is "A" or "a", then do not deliminate, load entire string into wrd(1).
C
C If d is "S" or "s", then the argument must match the parm exactly in length
C   (can be used for switches, e.g. "-quit", which have no values.)
C
      logical function findarg(arg,narg,parm,d,wrd,n)
C
      implicit none
      character*(*) arg(*),parm,wrd(*)
      character d*1, s*500
      integer*4 narg,n,i,ii,j,k,l,lc
C
C Initialize.
      k=lc(parm)
      n=-1
      i=1
      wrd(1)=' '
C
C Look for parameter, if we find one occurrence, look for more, we will
C use the last parameter seen.
C
      do while(i.le.narg)
        if (arg(i)(1:k).eq.parm(1:k)) then
        l=lc(arg(i))
C Watch for "switch" deliminator option.
        if ( ((d.ne.'s').and.(d.ne.'S')) .or. (l.eq.k) ) then
          n=0
          if (l.gt.k) then
            s=' '
            s=arg(i)(k+1:l)
C Watch for "All" the string deliminator option.
            if ((d.eq.'A').or.(d.eq.'a')) then
              n=1
              wrd(n)=s(1:lc(s))
            else
              do while(index(s,d).ne.0)
                n=n+1
                ii=index(s,d)
                wrd(n)=' '
                wrd(n)=s(1:ii-1)
                s=s(ii+1:lc(s))
              enddo
              n=n+1
              wrd(n)=' '
              wrd(n)=s(1:lc(s))
            endif
          endif
          do j=i,narg-1
            arg(j)=arg(j+1)
          enddo
          narg=narg-1
          i=i-1
        endif
        endif
        i=i+1
      enddo
      if (n.eq.-1) then
        findarg = .false.
      else
        findarg = .true.
      endif
      return
      end


C------------------------------------------------------------
C NEW(01Dec98): This version takes the first occurrence of a parameter.
C This allows you to read in many parameters with the same name.
C
C Finds an argument "parm" in "arg(1..narg)" and extracts the words
C "wrd(1..n)" deliminated by "d".  Returns true if argument found.  The
C argument is deleted from the argument list if found (i.e. arg is reset
C such that arg(1..narg-1) for the remaining arguments.
C
C If d is "A" or "a", then do not deliminate, load entire string into wrd(1).
C
C If d is "S" or "s", then the argument must match the parm exactly in length
C   (can be used for switches, e.g. "-quit", which have no values.)
C
      logical function findarg2(arg,narg,parm,d,wrd,n)
C
      implicit none
      character*(*) arg(*),parm,wrd(*)
      character d*1, s*500
      integer*4 narg,n,i,ii,j,k,l,lc
      logical again
C
C Initialize.
      k=lc(parm)
      n=-1
      i=1
      wrd(1)=' '
C
C Look for parameter, stop when we see the first occurrence.
C
      again=.true.
      do while((i.le.narg).and.(again))
        if (arg(i)(1:k).eq.parm(1:k)) then
        l=lc(arg(i))
C Watch for "switch" deliminator option.
        if ( ((d.ne.'s').and.(d.ne.'S')) .or. (l.eq.k) ) then
          n=0
          if (l.gt.k) then
            s=' '
            s=arg(i)(k+1:l)
C Watch for "All" the string deliminator option.
            if ((d.eq.'A').or.(d.eq.'a')) then
              n=1
              wrd(n)=s(1:lc(s))
            else
              do while(index(s,d).ne.0)
                n=n+1
                ii=index(s,d)
                wrd(n)=' '
                wrd(n)=s(1:ii-1)
                s=s(ii+1:lc(s))
              enddo
              n=n+1
              wrd(n)=' '
              wrd(n)=s(1:lc(s))
            endif
          endif
          do j=i,narg-1
            arg(j)=arg(j+1)
          enddo
          narg=narg-1
          i=i-1
          again=.false.
        endif
        endif
        i=i+1
      enddo
      if (n.eq.-1) then
        findarg2 = .false.
      else
        findarg2 = .true.
      endif
      return
      end


C--------------------------------------------------------------
C Does the argument exist (avoid using findarg).
C
      logical function arg_exist(parm)
C
      implicit none
      integer*4 narg,n
      character*(*) parm
      character*80 arg(1),wrd
      logical findarg
      call arguments(arg,narg,1)
      arg_exist = findarg(arg,narg,parm,':',wrd,n)
      return
      end


C------------------------------------------------------------------------
C Opens input and output files if they have been specified. If standard io,
C   then iun=5 and oun=6, otherwise iun=1, oun=2.
C Assumes a parameter line like:  command  parm1 parm2 [infile [outfile]]
C   where numin=3 in this case. Returns .true. if everything opened ok.
C
C Example:  command p1 p2 [p3] [p4] [infile [outfile]]
C   where p3 and p4 are optional parameters.  In this case, numim=
C   (3) or (4) or (5) depending on whether: (neither p3 or p4 were specified)
C   or (just one of either p3 or p4) or (both p3 and p4 specified).
C
      logical function inout(arg,narg,numin,iun,oun)

      implicit none
      character*(*) arg(*)      ! input
      integer*4 narg, numin     ! input
      integer*4 iun, oun        ! output
      integer*4 ierr
      character*100 wrd
      if (narg.lt.numin) then
        iun = 5
      else
        iun = 1
        open(unit=iun,file=arg(numin),status='old',iostat=ierr)
        if (ierr.ne.0) goto 91
      endif
      if (narg.lt.numin+1) then
        oun = 6
      else
        oun = 2
        open(unit=oun,file=arg(numin+1),status='unknown',iostat=ierr)
        if (ierr.ne.0) goto 92
      endif
      inout = .true.
      return
91    wrd = 'Error opening : '//arg(numin)
      call printerr(wrd)
      inout = .false.
      return
92    if (iun.ne.5) close(iun)
      wrd = 'Error opening : '//arg(numin+1)
      call printerr(wrd)
      inout = .false.
      return
      end


C------------------------------------------------------------------------
C system call using C routine
      subroutine sys(s)
      character*(*) s
      character*1000 wrd
      integer*4 lastchar
      wrd = s(1:lastchar(s))//char(0)
      call csys(wrd)
      return
      end


C------------------------------------------------------------------------
C Get photometry ratings for each day of all imaging observing runs.
C
      subroutine photom(n,date,place,pc)

      implicit none
      integer*4 maxn 
      parameter(maxn=100) 
      character date(maxn)*7,line*40,place(maxn)*1,cpc*6
      integer*4 pc(maxn),n

      open(33,file='/home/tab/phot/phot_cond.txt',status='old')
      
      n = 0 
1     continue
      read(33,'(a)',end=5) line
      if (line(1:2).eq.'  ') goto 1
      n = n + 1 
      date(n) = line(1:2)//line(4:6)//line(8:9) 
      place(n)= line(12:12)
      cpc = line(15:20)
      if (cpc.eq.'pb    ') then
        pc(n) = 1 
      else if (cpc.eq.'ps/pb ') then
        pc(n) = 2 
      else if (cpc.eq.'ps    ') then
        pc(n) = 3 
      else if (cpc.eq.'pb not') then
        pc(n) = 4 
      else if (cpc.eq.'not   ') then
        pc(n) = 5 
      else
        pc(n) = -1
      endif   
      goto 1
5     continue
      close(33) 
      return
      end  

C--------------------------------------------------------------
      subroutine find_median(a,n,median)
      implicit none
      real*4 a(*),median        ! array,median     (input,output)
      integer*4 n               ! number of values (input)
      if (n.lt.1) then
        median=0.
        return
      endif
      call qcksrt(n,a)
      if (mod(n,2).eq.0) then
        median = (a(n/2)+a((n/2)+1))/2.
      else
        median = a((n/2)+1)
      endif
      return
      end

C--------------------------------------------------------------
C This version uses real*8 numbers.
      subroutine find_median8(a,n,median)
      implicit none
      real*8 a(*),median        ! array,median     (input,output)
      integer*4 n               ! number of values (input)
      if (n.lt.1) then
        median=0.
        return
      endif
      call qcksrt8(n,a)
      if (mod(n,2).eq.0) then
        median = (a(n/2)+a((n/2)+1))/2.
      else
        median = a((n/2)+1)
      endif
      return
      end

C--------------------------------------------------------------
C originated from NUMERICAL RECIPES...           tab jul 1988
C 
      subroutine qcksrt(n,arr)

      implicit none
      integer*4 n,m,nstack 
      real*4 fm,fa,fc,fmi
      parameter (m=7,nstack=100,fm=7875.,fa=211.,
     .           fc=1663.,fmi=1.2698413e-4)
      real*4 arr(n),istack(nstack)
      integer*4 jstack,l,ir,j,i,iq
      real*4 fx,a 

      jstack=0
      l=1
      ir=n
      fx=0.
10    if (ir-l.lt.m) then
        do 13 j=l+1,ir
          a=arr(j)
          do 11 i=j-1,1,-1
            if (arr(i).le.a)goto 12
            arr(i+1)=arr(i)
11        continue
          i=0
12        arr(i+1)=a
13      continue
        if (jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        i=l
        j=ir
        fx=mod(fx*fa+fc,fm)
        iq=l+(ir-l+1)*(fx*fmi)
        a=arr(iq)
        arr(iq)=arr(l)
20      continue
21        if (j.gt.0) then
            if (a.lt.arr(j)) then
              j=j-1
              goto 21
            endif
          endif
          if (j.le.i) then
            arr(i)=a
            goto 30
          endif
          arr(i)=arr(j)
          i=i+1
22        if (i.le.n) then
            if (a.gt.arr(i)) then
              i=i+1
              goto 22
            endif
          endif
          if (j.le.i) then
            arr(j)=a
            i=j
            goto 30
          endif
          arr(j)=arr(i)
          j=j-1
        goto 20
30      jstack=jstack+2
        if (jstack.gt.nstack) then
          print *, 'qcksrt: NSTACK must be made larger.'
          read(*,*)
        endif
        if (ir-i.ge.i-l) then
          istack(jstack)=ir
          istack(jstack-1)=i+1
          ir=i-1
        else
          istack(jstack)=i-1
          istack(jstack-1)=l
          l=i+1
        endif
      endif
      goto 10
      end

C--------------------------------------------------------------
C originated from NUMERICAL RECIPES...           tab jul 1988
C This version uses real*8 numbers.
C
      subroutine qcksrt8(n,arr)

      implicit none
      integer*4 n,m,nstack 
      real*8 fm,fa,fc,fmi
      parameter (m=7,nstack=100,fm=7875.,fa=211.,
     .           fc=1663.,fmi=1.2698413d-4)
      real*8 arr(n),istack(nstack)
      integer*4 jstack,l,ir,j,i,iq
      real*8 fx,a 

      jstack=0
      l=1
      ir=n
      fx=0.
10    if (ir-l.lt.m) then
        do 13 j=l+1,ir
          a=arr(j)
          do 11 i=j-1,1,-1
            if (arr(i).le.a)goto 12
            arr(i+1)=arr(i)
11        continue
          i=0
12        arr(i+1)=a
13      continue
        if (jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        i=l
        j=ir
        fx=mod(fx*fa+fc,fm)
        iq=l+(ir-l+1)*(fx*fmi)
        a=arr(iq)
        arr(iq)=arr(l)
20      continue
21        if (j.gt.0) then
            if (a.lt.arr(j)) then
              j=j-1
              goto 21
            endif
          endif
          if (j.le.i) then
            arr(i)=a
            goto 30
          endif
          arr(i)=arr(j)
          i=i+1
22        if (i.le.n) then
            if (a.gt.arr(i)) then
              i=i+1
              goto 22
            endif
          endif
          if (j.le.i) then
            arr(j)=a
            i=j
            goto 30
          endif
          arr(j)=arr(i)
          j=j-1
        goto 20
30      jstack=jstack+2
        if (jstack.gt.nstack) then
          print *, 'qcksrt: NSTACK must be made larger.'
          read(*,*)
        endif
        if (ir-i.ge.i-l) then
          istack(jstack)=ir
          istack(jstack-1)=i+1
          ir=i-1
        else
          istack(jstack)=i-1
          istack(jstack-1)=l
          l=i+1
        endif
      endif
      goto 10
      end


C--------------------------------------------------------------
C originated from NUMERICAL RECIPES...           tab jul 1988
C Modified for key sorting...                    tab nov 1997
C
C The key() array is a list of element positions in the arr() array.
C The number of elements in the key() array may be smaller than or 
C equal to the number of elements in the arr() array.  This routine
C sorts the array key() using conditionals based on the values in arr().
C 
      subroutine qcksrt_subkey(narr,arr,nkey,key)
C
      implicit none
      integer*4 narr,nkey
      real*4 arr(narr)
      integer*4 key(nkey)
      integer*4 m,nstack 
      real*4 fm,fa,fc,fmi
      parameter (m=7,nstack=100,fm=7875.,fa=211.,
     .           fc=1663.,fmi=1.2698413e-4)
      real*4 istack(nstack),fx
      integer*4 jstack,l,ir,j,i,iq,k
C
      jstack=0
      l=1
      ir=nkey
      fx=0.
10    if (ir-l.lt.m) then
        do 13 j=l+1,ir
          k=key(j)
          do 11 i=j-1,1,-1
            if (arr(key(i)).le.arr(k)) goto 12
            key(i+1)=key(i)
11        continue
          i=0
12        key(i+1)=k
13      continue
        if (jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        i=l
        j=ir
        fx=mod(fx*fa+fc,fm)
        iq=l+(ir-l+1)*(fx*fmi)
        k=key(iq)
        key(iq)=key(l)
20      continue
21        if (j.gt.0) then
            if (arr(k).lt.arr(key(j))) then
              j=j-1
              goto 21
            endif
          endif
          if (j.le.i) then
            key(i)=k
            goto 30
          endif
          key(i)=key(j)
          i=i+1
22        if (i.le.nkey) then
            if (arr(k).gt.arr(key(i))) then
              i=i+1
              goto 22
            endif
          endif
          if (j.le.i) then
            key(j)=k
            i=j
            goto 30
          endif
          key(j)=key(i)
          j=j-1
        goto 20
30      jstack=jstack+2
        if (jstack.gt.nstack) then
          print *, 'qcksrt: NSTACK must be made larger.'
          read(*,*)
        endif
        if (ir-i.ge.i-l) then
          istack(jstack)=ir
          istack(jstack-1)=i+1
          ir=i-1
        else
          istack(jstack)=i-1
          istack(jstack-1)=l
          l=i+1
        endif
      endif
      goto 10
      end


C--------------------------------------------------------------
C originated from NUMERICAL RECIPES...           tab jul 1988
C Modified for key sorting...                    tab nov 1997
C Modified for real*8...                         tab oct 1998
C
C The key() array is a list of element positions in the real*8 arr() array.
C The number of elements in the key() array may be smaller than or 
C equal to the number of elements in the arr() array.  This routine
C sorts the array key() using conditionals based on the values in arr().
C 
      subroutine qcksrt8_subkey(narr,arr,nkey,key)

      implicit none
      integer*4 narr,nkey
      real*8 arr(narr)
      integer*4 key(nkey)
      integer*4 m,nstack 
      real*8 fm,fa,fc,fmi
      parameter (m=7,nstack=100,fm=7875.d0,fa=211.d0,fc=1663.d0,
     .           fmi=1.2698413d-4)
      real*8 istack(nstack),fx
      integer*4 jstack,l,ir,j,i,iq,k

      jstack=0
      l=1
      ir=nkey
      fx=0.d0
10    if (ir-l.lt.m) then
        do 13 j=l+1,ir
          k=key(j)
          do 11 i=j-1,1,-1
            if (arr(key(i)).le.arr(k)) goto 12
            key(i+1)=key(i)
11        continue
          i=0
12        key(i+1)=k
13      continue
        if (jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        i=l
        j=ir
        fx=mod(fx*fa+fc,fm)
        iq=l+(ir-l+1)*(fx*fmi)
        k=key(iq)
        key(iq)=key(l)
20      continue
21        if (j.gt.0) then
            if (arr(k).lt.arr(key(j))) then
              j=j-1
              goto 21
            endif
          endif
          if (j.le.i) then
            key(i)=k
            goto 30
          endif
          key(i)=key(j)
          i=i+1
22        if (i.le.nkey) then
            if (arr(k).gt.arr(key(i))) then
              i=i+1
              goto 22
            endif
          endif
          if (j.le.i) then
            key(j)=k
            i=j
            goto 30
          endif
          key(j)=key(i)
          j=j-1
        goto 20
30      jstack=jstack+2
        if (jstack.gt.nstack) then
          print *, 'qcksrt: NSTACK must be made larger.'
          read(*,*)
        endif
        if (ir-i.ge.i-l) then
          istack(jstack)=ir
          istack(jstack-1)=i+1
          ir=i-1
        else
          istack(jstack)=i-1
          istack(jstack-1)=l
          l=i+1
        endif
      endif
      goto 10
      end


C--------------------------------------------------------------
C originated from NUMERICAL RECIPES... 
C Version sorts via a key array which is initially just 1,2,3,...  tab sep93
C It destroys (sorts) the original array.
C 
      subroutine qcksrtkey(n,arr,key)
C
      implicit none
C
      integer*4 n,m,nstack 
      real*4 fm,fa,fc,fmi
      parameter (m=7,nstack=100,fm=7875.,fa=211.,
     .           fc=1663.,fmi=1.2698413e-4)
      real*4 arr(n),istack(nstack)
      integer*4 jstack,l,ir,j,i,iq,key(n),k
      real*4 fx,a 
C
      jstack=0
      l=1
      ir=n
      fx=0.
10    if (ir-l.lt.m) then
        do 13 j=l+1,ir
          a=arr(j)
          k=key(j)
          do 11 i=j-1,1,-1
            if (arr(i).le.a)goto 12
            arr(i+1)=arr(i)
            key(i+1)=key(i)
11        continue
          i=0
12        arr(i+1)=a
          key(i+1)=k 
13      continue
        if (jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        i=l
        j=ir
        fx=mod(fx*fa+fc,fm)
        iq=l+(ir-l+1)*(fx*fmi)
        a=arr(iq)
        k=key(iq)
        arr(iq)=arr(l)
        key(iq)=key(l)
20      continue
21        if (j.gt.0) then
            if (a.lt.arr(j)) then
              j=j-1
              goto 21
            endif
          endif
          if (j.le.i) then
            arr(i)=a
            key(i)=k
            goto 30
          endif
          arr(i)=arr(j)
          key(i)=key(j)
          i=i+1
22        if (i.le.n) then
            if (a.gt.arr(i)) then
              i=i+1
              goto 22
            endif
          endif
          if (j.le.i) then
            arr(j)=a
            key(j)=k
            i=j
            goto 30
          endif
          arr(j)=arr(i)
          key(j)=key(i)
          j=j-1
        goto 20
30      jstack=jstack+2
        if (jstack.gt.nstack) then
          print *, 'qcksrt: NSTACK must be made larger.'
          read(*,*)
        endif
        if (ir-i.ge.i-l) then
          istack(jstack)=ir
          istack(jstack-1)=i+1
          ir=i-1
        else
          istack(jstack)=i-1
          istack(jstack-1)=l
          l=i+1
        endif
      endif
      goto 10
      end


C-----------------------------------------------------------------
C Writes RA DEC in readable form.  "s" should be at least 26 characters.
C "ra" and "dec" are in decimal hours and decimal degrees.
C
C mode:  1 = HHMM+DDD
C mode:  2 = HHMM+DDMM
C mode:  3 = HHMM.S+DDMM
C mode:  4 = HH MM SS.S  +DD MM SS
C mode:  5 = HH MM SS.SS  +DD MM SS.S
C mode:  6 = HH MM SS.SSS  +DD MM SS.SS
C            ----+----1----+----2----+----3----+----4
C
      subroutine write_radec(ra,dec,s,mode)

      implicit none
      real*8 ra,dec,rs,ds,thesign
      integer*4 rh,rm,dd,dm,mode
      character*(*) s
      character c*1

      if ((ra.lt.0.).or.(ra.gt.99.)) goto 900
      call deci2hms(ra,rh,rm,rs,thesign)
      rh = min(99,max(-9,rh))
      rm = min(99,max(-9,rm))
      rs = min(99.,max(-9.,rs))
      if ((dec.lt.-99.).or.(dec.gt.99.)) goto 900
      call deci2hms(dec,dd,dm,ds,thesign)
      dd = min(99,max(-99,dd))
      dm = min(99,max(-99,dm))
      ds = min(99.,max(-99.,ds))
      if (thesign.gt.0.) then
        c = '+'
      else
        c = '-'
      endif
      if (mode.eq.1) then
        write(s,'(i2.2,i2.2,a1,i2.2,i1)') rh,rm,c,dd,
     .              min(9,max(0,int((dfloat(dm)+ds/60.d0)/6.d0)))
      elseif (mode.eq.2) then
        write(s,'(i2.2,i2.2,a1,i2.2,i2.2)') rh,rm,c,dd,dm
      elseif (mode.eq.3) then
        write(s,'(i2.2,i2.2,a1,i1,a1,i2.2,i2.2)')
     .     rh,rm,'.',min(9,max(0,int(rs/6.d0))),c,dd,dm
      elseif (mode.eq.4) then
        write(s,'(i2.2,1x,i2.2,1x,f4.1,2x,a1,i2.2,1x,i2.2,1x,i2.2)')
     .         rh,rm,min(59.9,rs),c,dd,dm,nint(min(59.0,ds))
        if (s(7:7).eq.' ') s(7:7)='0'
      elseif (mode.eq.5) then
        write(s,'(i2.2,1x,i2.2,1x,f5.2,2x,a1,i2.2,1x,i2.2,1x,f4.1)')
     .         rh,rm,min(59.99,rs),c,dd,dm,min(59.9,ds)
        if (s(7:7).eq.' ') s(7:7)='0'
        if (s(21:21).eq.' ') s(21:21)='0'
      elseif (mode.eq.6) then
        write(s,'(i2.2,1x,i2.2,1x,f6.3,2x,a1,i2.2,1x,i2.2,1x,f5.2)')
     .         rh,rm,min(59.999,rs),c,dd,dm,min(59.99,ds)
        if (s(7:7).eq.' ') s(7:7)='0'
        if (s(22:22).eq.' ') s(22:22)='0'
      endif
      return
900   s=' '
      return
      end



C-----------------------------------------------------------------
C Writes hours:minutes:seconds.  "s" should be at least 11 characters.
C "plus" is either '+' or ' ' depending on whether positive values should
C have an explicit positive.
C
      subroutine write_hms(deci,s,plus)

      implicit none
      real*8 deci,hs,thesign
      integer*4 hh,hm
      character*(*) s
      character*1 plus
      if ((deci.gt.99.).or.(deci.lt.-99.)) goto 900
      call deci2hms(deci,hh,hm,hs,thesign)
      if (thesign.gt.0.) then
        write(s,'(a,i2.2,a,i2.2,a,i2.2,f2.1)') plus,hh,':',hm,':',
     .                  int(hs),(hs-dfloat(int(hs)))
      else
        write(s,'(a,i2.2,a,i2.2,a,i2.2,f2.1)') '-',hh,':',hm,':',
     .                  int(hs),(hs-dfloat(int(hs)))
      endif
      if (s(10:11).eq.'**') s(10:11)='.9'
      return
900   s = ' '
      return
      end


C-----------------------------------------------------------------
C Reads R.A. (ra) and Dec. (dec) from strings (uc) with the following formats:
C
C HHMM+DDD
C HHMM+DDMM
C HHMM.S+DDMM
C HH MM SS.S  +DD MM SS
C HH MM SS.SS  +DD MM SS.S
C ----+----1----+----2----+----3----+----4
C The first three formats must have string lengths of 8, 9, and 11 characters,
C respectively.  The last two formats can have various deliminations between
C the 6 numbers, and the "-" for southern declinations can be anywhere within
C the string.
C
      subroutine read_radec(ra,dec,uc)

      implicit none
      real*8 ra,dec,rh,rm,rs,dd,dm,ds,thesign,r8
      character*(*) uc
      character*80 s,leftjust
      integer*4 i,n1,n2,n3,n4,lastchar,lc,j,im,ip
      logical dissect,ok,vallike

      s = leftjust(uc)
      lc= lastchar(s)

C All "value-like" characters (0-9,+,-,.,E,:).
      ok=.true.
      do i=1,lc
        if (.not.vallike(s(i:i))) ok=.false.
      enddo
      if (.not.ok) then
C All deliminated: 16 43 21.3 +39 32 34
        thesign=1.d0
        if (index(s,'-').gt.0) thesign=-1.d0
        if (.not.dissect(s,1,r8)) goto 995
        rh=abs(r8)
        if (.not.dissect(s,2,r8)) goto 995
        rm=abs(r8)
        if (.not.dissect(s,3,r8)) goto 995
        rs=abs(r8)
        if (.not.dissect(s,4,r8)) goto 995
        dd=abs(r8)
        if (.not.dissect(s,5,r8)) goto 995
        dm=abs(r8)
        if (.not.dissect(s,6,r8)) goto 995
        ds=abs(r8)
        ra = rh + (rm + rs/60.d0)/60.d0
        dec= dd + (dm + ds/60.d0)/60.d0
        goto 8
      endif

C Find plus.
      ip=index(s,'+')
      if (ip.gt.0) then
        s(ip:ip)='#'
        j=index(s,'+')
        if (j.gt.0) goto 995
      endif
C Find minus.
      im=index(s,'-')
      if (im.gt.0) then
        s(im:im)='#'
        j=index(s,'-')
        if (j.gt.0) goto 995
      endif
C Must have one sign and one sign only.
      if ((ip.gt.0).and.(im.gt.0)) goto 995
      if ((ip.eq.0).and.(im.eq.0)) goto 995
      thesign = 1.d0
      if (im.gt.0) thesign = -1.d0

C ----+----1----+
C 1643.3+395
      if ((lc.eq.10).and.(s(5:5).eq.'.').and.(s(7:7).eq.'#')) then
        read(s,'(i2,i2,f2.1,1x,i2,i1)',err=995) n1,n2,r8,n3,n4
        ra = dfloat(n1) + (dfloat(n2)+r8+0.05d0)/60.d0
        dec= dfloat(n3) + ((dfloat(n4)/10.d0)+0.05d0)

C ----+----1----+
C 1643+39
      elseif ((lc.eq.7).and.(s(5:5).eq.'#')) then
        read(s,'(i2,i2,1x,i2)',err=995) n1,n2,n3
        ra = dfloat(n1) + (dfloat(n2)+0.5d0)/60.d0
        dec= dfloat(n3) + 0.5d0
    
C ----+----1----+
C 1643+395
      elseif ((lc.eq.8).and.(s(5:5).eq.'#')) then
        read(s,'(i2,i2,1x,i2,i1)',err=995) n1,n2,n3,n4
        ra = dfloat(n1) + (dfloat(n2)+0.5d0)/60.d0
        dec= dfloat(n3) + ((dfloat(n4)/10.d0)+0.05d0)

C ----+----1----+
C 1643+3932
      elseif ((lc.eq.9).and.(s(5:5).eq.'#')) then
        read(s,'(i2,i2,1x,i2,i2)',err=995) n1,n2,n3,n4
        ra = dfloat(n1) + (dfloat(n2)+0.5d0)/60.d0
        dec= dfloat(n3) + (dfloat(n4)+0.5d0)/60.d0

C ----+----1----+
C 1643.3+3932
      elseif ((lc.eq.11).and.(s(5:5).eq.'.').and.(s(7:7).eq.'#')) then
        read(s,'(i2,i2,f2.1,1x,i2,i2)',err=995) n1,n2,r8,n3,n4
        ra = dfloat(n1) + (dfloat(n2)+r8+0.05d0)/60.d0
        dec= dfloat(n3) + (dfloat(n4)+0.5d0)/60.d0

C Does not fit a standard format.
      else
        goto 995

      endif

8     continue
      ra = ra + 1.d-9
      dec= thesign * ( dec + 1.d-9 )
      return
995   print *,'Error reading RA DEC value:'
      print '(a)',uc(1:lastchar(uc))
      ra = 99.
      dec= 99.
      return
      end
      


C---------------------------------------------------------------------------
C Friendly gateway to AMOEBA multidimensional minimization.
C Returns .false. if maximum iterations exceeded or too many parameters.
C The hard limit on number of parameters is given by AmoebaMax in cphomax.h .
C
C FUNK should be declared "real*8" and "EXTERNAL" in routine which calls
C this routine.  The syntax for the function is "real*8 function FUNK(PAR)",
C all other information should be passed via common block.
C
      logical function amoeba_gate(FUNK,np,par,parsl,resid)
C
      implicit none
      real*8 FUNK        ! External function to minimize   (input)
      integer*4 np       ! Number of parameters            (input)
      real*8 par(np)     ! Parameters                      (input/output)
      real*8 parsl(np)   ! Scale length for each parameter (input)
      real*8 resid       ! Residual (final value of FUNK)  (output)
C
      EXTERNAL FUNK
C
      real*8 ftol
      integer*4 niter
C
      ftol = 1.d-9
      call ffamoebagate( FUNK, np, par, parsl, ftol, resid, niter )
      if (niter.gt.0) then
        amoeba_gate = .true.
      else
        amoeba_gate = .false.
      endif
      return
      end


C----------------------------------------------------------------
C Find QSO in the QSO catalog given ra and dec.
      
      subroutine find_qso(ura,udec,qra,qdec,bestsep,lines,n,friends,nf)

      implicit none

      real*8 ura,udec,qra,qdec,ra,dec
      character*200 lines(90),friends(90)
      integer*4 n,nf

      character*200 l,ls(90)
      character*80 s,datfile,c80
      integer*4 i,k
      real*8 a1,t1,a2,t2,sep,bestsep,as,fsep

C Defaults.
      n=0
      nf=0
      lines(1) = ' '
      qra = 0.d0
      qdec= 0.d0

C Set alpha, theta for user coordinates and maximum search radius in arcmins.
      a1 = 360.d0*(ura/24.d0)
      t1 = 90.d0 - udec
C Let user set this (if reasonable).
      if ((bestsep.lt.6.d0).or.(bestsep.gt.1200.d0)) bestsep = 60.d0
C Separation between friends.
      fsep = bestsep / 2.d0

C Subset of catalog to search.
      write(datfile,'(a,i2.2,a)') '/home/tab/qso/sub/sub_',int(ura)

1     open(33,file=datfile,status='old')
      read(33,'(a)') l

3     k=1
      ls(k) = l
      read(33,'(a)') l
      do while((l(1:1).ne.'0').and.(l(1:1).ne.'1')
     .    .and.(l(1:1).ne.'2').and.(l(1:6).ne.'THEEND'))
        k=k+1
        ls(k) = l
        read(33,'(a)') l
      enddo

C Get ra and dec.
      s = ls(1)(22:33)//' '//ls(2)(22:33)
      call read_radec(ra,dec,s)

      a2 = 360.d0*(ra/24.d0)
      t2 = 90.d0 - dec
      sep= as(a1,t1,a2,t2)

      if (sep.lt.bestsep) then
        do i=1,k
          lines(i)=ls(i)
        enddo
        n=k
        bestsep=sep
        qra = ra
        qdec= dec
      endif

C Record friends.
      if (sep.lt.fsep) then
        nf=nf+1
        c80=' '
        call write_radec(ra,dec,c80,4)
        friends(nf)=' '
        friends(nf)=
     .    c80(1:25)//ls(1)(11:20)//ls(1)(49:53)//' '//ls(1)(67:73)
      endif

      if (l(1:6).ne.'THEEND') goto 3

      close(33)

      if (n.eq.0) bestsep = 9999.

      return
      end


C--------------------------------------------------------------------
C Extracts QSO information from the lines in the QSO catalog for
C a particular object.
C
      subroutine qso_info(lines,n,ra,dec,sel,other,v,z,ref,note)

      implicit none
      character*200 lines(90)
      character*(*) note
      real*8 ra,dec,v,z
      character sel(9)*1,other(9)*10,clean*5,dum*5,coord*80,c80*80
      integer*4 ref(5,9),n,i,j,lc
      real*4 valread

      do j=1,9
        sel(j) = ' '
        other(j) = ' '
        do i=1,5
          ref(i,j)=0
        enddo
      enddo
      note = ' '
      coord = lines(1)(22:33)//' '//lines(2)(22:33)
      call read_radec(ra,dec,coord)
      dum = clean(lines(1)(49:53))
      v = dble(valread(dum))
      dum = clean(lines(1)(68:72))
      z = dble(valread(dum))
      do i=1,min(n,9)
        if (i.gt.1) sel(i-1) = lines(i)(8:8)
        other(i) = lines(i)(11:20) 
        ref(1,i) = nint(valread(lines(i)(93:96)))
        ref(2,i) = nint(valread(lines(i)(98:101)))
        ref(3,i) = nint(valread(lines(i)(103:106)))
        ref(4,i) = nint(valread(lines(i)(108:111)))
        ref(5,i) = nint(valread(lines(i)(113:116)))
      enddo
      do i=1,n
        c80  = lines(i)(119:)
        note = note(1:max(1,lc(note)))//' '//c80(1:lc(c80))
      enddo
      return
      end


C-------------------------------------------------------------------
C Blanks-out special characters near V and z in QSO catalog.
C
      character*5 function clean(udum)

      implicit none
      character*5 udum,dum
      integer*4 i

      dum = udum
      i = index(dum,'*')
      do while(i.gt.0)
        dum(i:i) = ' '
        i = index(dum,'*')
      enddo
      i = index(dum,'+')
      do while(i.gt.0)
        dum(i:i) = ' '
        i = index(dum,'+')
      enddo
      i = index(dum,'(')
      do while(i.gt.0)
        dum(i:i) = ' '
        i = index(dum,'(')
      enddo
      i = index(dum,')')
      do while(i.gt.0)
        dum(i:i) = ' '
        i = index(dum,')')
      enddo
      clean = dum
      return
      end


C-------------------------------------------------------------------
C Gets author(s), journal, etc. from the QSO Catalog reference database
C given the reference number.
C
      subroutine qsocat_reference(n,s)

      implicit none
      integer*4 n               ! Reference number   (input)
      character*(*) s           ! Reference string   (output)
      character c4*4,line*300,datfile*80
      integer*4 k

      if (n.lt.1000) then
        write(c4,'(1x,i3.3)') n
      else
        write(c4,'(i4)') n
      endif
      if (n.lt.1000) then
        datfile = '/home/tab/qso/sub/refs.A'
      elseif (n.lt.2000) then
        datfile = '/home/tab/qso/sub/refs.B'
      else
        datfile = '/home/tab/qso/sub/refs.C'
      endif
      open(31,file=datfile,status='old')
      k = min(300,len(s))
      s = ' '
      do while(s.eq.' ')
        read(31,'(a)',end=5) line
        if (c4.eq.line(1:4)) s = line(7:k)
      enddo
5     close(31)
      return
      end


C-----------------------------------------------------------------------
C Finds nearest element array number to value xpt in array x(1..n).
C List need not be sorted.  See neari_bs for speed.
C Usually speed is irrelevant unless the array is large (>100,000) or you
C are calling it many times (>1000).
      integer*4 function neari(xpt,n,x)
      implicit none
      integer*4 n,i,imin
      real*4 x(n),r,xpt,rmin
      imin=1
      rmin=abs(xpt-x(1))
      do i=2,n
        r=abs(xpt-x(i))
        if (r.lt.rmin) then
          rmin=r
          imin=i
        endif
      enddo
      neari=imin
      return
      end

C-----------------------------------------------------------------------
C Finds nearest element array number to value xpt in array x(1..n).
C List need not be sorted.  See neari_bs for speed.
C This version works on real*8 arrays.
      integer*4 function neari8(xpt,n,x)
      implicit none
      integer*4 n,i,imin
      real*8 x(n),r,xpt,rmin
      imin=1
      rmin=abs(xpt-x(1))
      do i=2,n
        r=abs(xpt-x(i))
        if (r.lt.rmin) then
          rmin=r
          imin=i
        endif
      enddo
      neari8=imin
      return
      end

C-----------------------------------------------------------------------
C Finds nearest element array number to value xpt in array x(1..n).
C This version uses a binary search for speed (this routine takes about
C 0.2 milliseconds for 2000 element array, whereas neari takes about
C 20 milliseconds).  The array must be sorted for this to work.
      integer*4 function neari_bs(xpt,n,x)
      implicit none
      integer*4 n,i,i1,i2
      real*4 x(n),xpt
C Binary search.
      i1=1
      i2=n
      do while(i2-i1.gt.1)
        i=(i1+i2)/2
        if (x(i).gt.xpt) then
          i2=i
        else
          i1=i
        endif
      enddo
      if (i2.le.i1) print *,'ERROR in neari_bs: this should not happen.'
      if (abs(x(i1)-xpt).lt.abs(x(i2)-xpt)) then
        neari_bs=i1
      else
        neari_bs=i2
      endif
      return
      end

C-----------------------------------------------------------------------
C Finds nearest element array number to value xpt in array x(1..n).
C This version uses a binary search for speed (this routine takes about
C 0.2 milliseconds for 2000 element array, whereas neari takes about
C 20 milliseconds).  The array must be sorted for this to work.
C This version works on real*8 arrays.
      integer*4 function neari_bs8(xpt,n,x)
      implicit none
      integer*4 n,i,i1,i2
      real*8 x(n),xpt
C Binary search.
      i1=1
      i2=n
      do while(i2-i1.gt.1)
        i=(i1+i2)/2
        if (x(i).gt.xpt) then
          i2=i
        else
          i1=i
        endif
      enddo
      if (i2.le.i1) then
        print *,'ERROR in neari_bs8: this should not happen.'
      endif
      if (abs(x(i1)-xpt).lt.abs(x(i2)-xpt)) then
        neari_bs8=i1
      else
        neari_bs8=i2
      endif
      return
      end

C----------------------------
      subroutine symsel(i,s)
      implicit none
      integer*4 i,s
      if (i.eq.1) s=6
      if (i.eq.2) s=7
      if (i.eq.3) s=5
      return
      end

C----------------------------------------------------
C Convert an 8-bit byte into two 4-bit hexadecimal characters.
      subroutine byte_to_hex(b,hihex,lohex)
      implicit none
      byte b
      character*1 hihex,lohex,hexchar
      integer*4 k
      k=b
      if (k.lt.0) k=k+256
      k=255-k
      lohex = hexchar(mod(k,16))
      hihex = hexchar(k/16)
      return
      end

C----------------------------------------------------
C Convert a 0 to 15 integer to a character: 0,1,...8,9,A,B,C,D,E,F .
      character*1 function hexchar(i)
      implicit none
      integer*4 i
      if ((i.lt.0).or.(i.gt.15)) then
        call printerr('ERROR- hex out of range.')
        return
      endif
      if (i.lt.10) then
        hexchar = char(ichar('0') + i)
      else
        hexchar = char(ichar('A') + i - 10)
      endif
      return
      end

C----------------------------------------------------------------------
C Set up parmhead string from parameter file.
      subroutine ReadParmFile(file,parmhead)
      implicit none
      character*(*) file
      character*(*) parmhead
      character*80 line
      integer*4 ptr
      parmhead=' '
      ptr=1
      open(35,file=file,status='old')
1     continue
      read(35,'(a)',end=5) line
      if (line(1:1).eq.'C') goto 1
      if (index(line,'=').eq.0) goto 1
      parmhead(ptr:ptr+79)=line
      ptr=ptr+80
      goto 1
5     continue
      parmhead(ptr:ptr+2)='END'
      close(35)
      return
      end 
C----------------------------------------------------------------------
C Get character parameter value out of parmhead.  parm is the parameter name
C and parmhead contains all the 80 character parameter cards.
      character*80 function GetCharParm(parm,parmhead)
      implicit none
      character*(*) parm
      character*(*) parmhead
      character*80 str
      integer*4 ptr,k,lc,ptr1,ptr2
      k=lc(parm)
      str=parm(1:k)//'='
      ptr=index(parmhead,str(1:k+1))
      if (ptr.eq.0) then
        print *,'Parameter not found: ',parm(1:k)
        call exit(1)
      endif
      ptr1=ptr+k+1
      ptr=ptr1
      do while(parmhead(ptr:ptr).ne.' ')
        ptr=ptr+1
      enddo
      ptr2=ptr-1
      GetCharParm=' '
      if (ptr2.ge.ptr1) GetCharParm=parmhead(ptr1:ptr2)
      return
      end
C----------------------------------------------------------------------
C Get real parameter value out of parmhead.  parm is the parameter name
C and parmhead contains all the 80 character parameter cards.
C
      real*4 function GetRealParm(parm,parmhead)
C
      implicit none
      character*(*) parm
      character*(*) parmhead
      character*80 str,wrd
      integer*4 ptr,ii,lc,k
      real*8 fgetlinevalue,r8
C
C Find the value.
      k=lc(parm)
      str=parm(1:k)//'='
      ptr=index(parmhead,str(1:k+1))
      if (ptr.eq.0) then
        print *,'ERROR: GetRealParm: Parameter not found: ',parm(1:k)
        call exit(1)
      endif
C
C Read the value.
      ii = ptr+k+1
      wrd = parmhead((ii):(ii+40))
      r8 = fgetlinevalue(wrd,1)
      if (r8.lt.-1.d+39) goto 9
      GetRealParm = r8
      return
C
C Error.
9     GetRealParm = -32760.
      call printerr('ERROR: GetRealParm: Error reading value.')
      return
      end

C----------------------------------------------------------------------
C Get the EE home directory using MAKEE_DIR or HAR_DIR or MKHARDIR or HARDIR 
C environment variable.
      subroutine Get_EE_Home(wrd)
      implicit none 
      character*(*) wrd
      character*80 s
      integer*4 lc
      call GetMakeeHome(s)
      if (s.ne.' ') s = s(1:lc(s))//'/EE/'
      wrd=s
      return 
      end 

C----------------------------------------------------------------------
C Get the HAR home directory using MAKEE_DIR or HAR_DIR or MKHARDIR or HARDIR 
C environment variable.
C
      subroutine GetMakeeHome(wrd)
C
      implicit none 
      character*(*) wrd
      character*80 s
      integer*4 lc
      s=' '
      call getenv('MAKEE_DIR',s)
      if (s.eq.' ') call getenv('HAR_DIR',s)
      if (s.eq.' ') call getenv('HARDIR',s)
      if (s.eq.' ') call getenv('MKHARDIR',s)
      if (s.eq.' ') then
        print *,
     . 'Error-- Could not find the MAKEE_DIR or HAR_DIR or MKHARDIR'
        print *,
     . '  environment variable.  Set one of these to the directory'
        print *,
     . '  containing the UA and EE subdirectories.'
      else
        s = s(1:lc(s))//'/'
      endif
      wrd=s
      return 
      end 

C------------------------------------------------------------
      subroutine pauseit()
      implicit none
      character*1 dummy
      print '(a)','Hit return to continue.'
      read(5,'(a)') dummy
      return
      end

C----------------------------------------------------------------------
      subroutine AddFitsExt(s)
      implicit none
      character*(*) s
      integer*4 lc
      if (index(s,'.fits').eq.0) s = s(1:lc(s))//'.fits'
      return
      end

C----------------------------------------------------------------------
      subroutine SubFitsExt(s)
      implicit none
      character*(*) s
      integer*4 lc,i
      i = index(s,'.fits')
      if ((i.gt.1).and.(i.eq.lc(s)-4)) s = s(1:i-1)
      return
      end


C----------------------------------------------------------------------
C Given the apparent residual intensity for a blue component (fb) and a
C red component (fr) of a doublet whose optical depth ratio must be 2:1
C (blue:red) return the actual zero point level (zp) for partial covering.
C It also returns the implied optical depth at that point.
C
      subroutine ZeroPoint(fb,fr,zp,taub,taur)
      implicit none
      real*4 fb,fr,zp,taub,taur,r
      taub = -1.e+30
      taur = -1.e+30
      zp   = -1.e+30
      if (fb.lt.fr) then
        zp =  ((fr*fr)-fb) / ((2.*fr)-(fb+1.))
        r  = (fb-zp)/(1.-zp)
        if (r.gt.0.) taub = -1.*log(r)
        r  = (fr-zp)/(1.-zp)
        if (r.gt.0.) taur = -1.*log(r)
      endif
      return
      end

C----------------------------------------------------------------------
C Obtain a wavelength for a given pixel value in a reduced HIRES spectrum.
C Use row=0 to initialize the polynomial coeffecients for all the rows.
C Then use row=desired row# on subsequent calls.  Returns -1.d+60 if problems.
C
      real*8 function GetSpimWave(header,pix,row)

      implicit none
      character*(*) header
      real*8 pix
      integer*4 row
      integer*4 j,oa(1:99),sr,er,sc,ec,nr,nc,inhead,getpos,dcflag
      real*8 ca(0:21,1:99),polyval,fhead
      character*80 card1,card2,chead
      character c7*7, c8*8
C
      common /GetSpimWaveBLK/ ca,oa,nc,dcflag
C
C Initialize coeffecients.
      if (row.eq.0) then
        card1 = chead('CTYPE1',header)
        if ((card1(1:6).eq.'LAMBDA').or.(card1(1:6).eq.'MULTIS')) then
C Is this a log-linear scale?
          dcflag = max(0,inhead('DCFLAG',header))
C Linear scale.
          sc = 1
          nc = inhead('NAXIS1',header)
          ec = sc + nc - 1
          sr = max(1,inhead('CRVAL2',header))
          nr = inhead('NAXIS2',header)
          er = sr + nr - 1
          if (nr.eq.1) then
C Look for CRVL1_## cards.
            write(c8,'(a,i2.2)') 'CRVL1_',sr
            if (getpos(c8,header).ne.-1) then
              ca(0,sr) = fhead(c8,header)
              write(c8,'(a,i2.2)') 'CDLT1_',sr
              ca(1,sr) = fhead(c8,header)
            else
              ca(0,sr) = fhead('CRVAL1',header)
              ca(1,sr) = fhead('CDELT1',header)
            endif
C Make linear values work in polynomial equation: lambda = c0 + c1*(pixel).
            if (dcflag.ne.1) ca(0,sr) = ca(0,sr) - ca(1,sr)
            oa(sr)=1
            if ((dcflag.ne.1).and.(ca(0,j).lt.200.)) goto 810
          else
C Assume there exist CRVL1_## cards.
            do j=sr,er
              write(c8,'(a,i2.2)') 'CRVL1_',j
              ca(0,j) = fhead(c8,header)
              write(c8,'(a,i2.2)') 'CDLT1_',j
              ca(1,j) = fhead(c8,header)
C Make linear values work in polynomial equation: lambda = c0 + c1*(pixel).
              if (dcflag.ne.1) ca(0,j) = ca(0,j) - ca(1,j)
              oa(j) = 1
              if ((dcflag.ne.1).and.(ca(0,j).lt.200.)) goto 810
            enddo
          endif
        else
C Polynomial scale.
          sc = inhead('CRVAL1',header)
          nc = inhead('NAXIS1',header)
          ec = sc + nc - 1
          sr = inhead('CRVAL2',header)
          nr = inhead('NAXIS2',header)
          er = sr + nr - 1
          do j=sr,er
            write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',j
            card1 = chead(c7,header)
            write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',j
            card2 = chead(c7,header)
            if ((card1.eq.' ').or.(card2.eq.' ')) goto 810
            read(card1,'(4(1pe17.9))') ca(0,j),ca(1,j),ca(2,j),ca(3,j)
            read(card2,'(4(1pe17.9))') ca(4,j),ca(5,j),ca(6,j),ca(7,j)
            oa(j)=7
            do while((oa(j).gt.0).and.(abs(ca(oa(j),j)).lt.1.d-60))
              oa(j)=oa(j)-1
            enddo
            if (oa(j).lt.1) goto 810
          enddo
        endif
        GetSpimWave = 0.d0
      else
C Calculate wavelength.
        if (nc.lt.1) goto 820
        if (dcflag.eq.1) then
C Log-linear.
          GetSpimWave = 10.d0 ** ( ca(0,row) + (ca(1,row)*(pix-1.d0)) )
        else
C Polynomial or linear (order=1).
          GetSpimWave = polyval(oa(row)+1,ca(0,row),pix)
        endif
      endif
      return
810   continue
CCC   print *,'GetSpimWave: Error reading WV header cards.'
      GetSpimWave=-1.d+60
      return
820   continue
      print *,
     . 'GetSpimWave: Error subroutine apparently not initialized.'
      GetSpimWave=-1.d+60
      return
      end

C----------------------------------------------------------------------
C Obtain a pixel value for a given wavelength in a reduced HIRES spectrum.
C Use row=0 to initialize the polynomial coeffecients for all the rows.
C Then use row=desired row# on subsequent calls. Returns -1.d+60 if problems.
C
      real*8 function GetSpimPix(header,wave,row)
C
      implicit none
C
      character*(*) header
      real*8 wave
      integer*4 row,  i
      integer*4 j,oa(1:99),oai(1:99),sr,er
      integer*4 sc,ec,nr,nc,inhead,np,dcflag
      real*8 ca(0:21,1:99),cai(0:21,1:99)
      real*8 polyval,x8(900),y8(900),w8(900)
      real*8 high,wrms,tw,pix,disp,fhead
      character*80 card1,card2,chead
      character c7*7, c8*8
      logical ok
C
      common /GetSpimPixBLK/ ca,cai,oa,oai,nc,dcflag
C
      IF (row.eq.0) THEN

C If row=0 then initialize forward and inverse polynomials.
        sc = inhead('CRVAL1',header)
        nc = inhead('NAXIS1',header)
        ec = sc + nc - 1
        sr = inhead('CRVAL2',header)
        nr = inhead('NAXIS2',header)
        er = sr + nr - 1
        do j=sr,er
CCC this section added courtesy of Jason Prochaska
          card1 = chead('CTYPE1',header)
          if ((card1(1:6).eq.'LAMBDA').or.(card1(1:6).eq.'MULTIS')) then
            dcflag = max(0,inhead('DCFLAG',header))
            write(c8,'(a,i2.2)') 'CRVL1_',j
            ca(0,j) = fhead(c8,header)
            write(c8,'(a,i2.2)') 'CDLT1_',j
            ca(1,j) = fhead(c8,header)
            do i=2,7
              ca(i,j) = 0.0
            enddo
            if (dcflag.ne.1) ca(0,sr) = ca(0,sr) - ca(1,sr)
CCC this ^^ section added courtesy of Jason Prochaska */
          else
            write(c7,'(a,i1,a,i2.2)') 'WV_',0,'_',j
            card1 = chead(c7,header)
            write(c7,'(a,i1,a,i2.2)') 'WV_',4,'_',j
            card2 = chead(c7,header)
            if ((card1.eq.' ').or.(card2.eq.' ')) goto 810
            read(card1,'(4(1pe17.9))') ca(0,j),ca(1,j),ca(2,j),ca(3,j)
            read(card2,'(4(1pe17.9))') ca(4,j),ca(5,j),ca(6,j),ca(7,j)
          endif
          oa(j)=7
          do while((oa(j).gt.0).and.(abs(ca(oa(j),j)).lt.1.d-60))
            oa(j)=oa(j)-1
          enddo
          if (oa(j).lt.1) goto 810
C Load values for inverse polynomial fit.  pix = func(wave)
          np=0
          do i=sc,ec,10
            np    = np + 1
            x8(np)= polyval(oa(j)+1,ca(0,j),dfloat(i))
            y8(np)= dfloat(i)
            w8(np)= 1.d0
          enddo
C Fit inverse polynomial.
          oai(j)=16
2         continue
          if (oai(j).lt.4) goto 830
          call poly_fit_glls(np,x8,y8,w8,oai(j),cai(0,j),ok)
          if (.not.ok) then
            oai(j)=oai(j)-1
            goto 2
          endif
C Check residuals.
          call poly_fit_glls_residuals(np,x8,y8,w8,
     .                                 oai(j),cai(0,j),high,wrms)
          if ((wrms.gt.1.0).or.(high.gt.3.0)) then
            oai(j)=oai(j)-1
            goto 2
          endif
        enddo
        GetSpimPix = 0.d0

      ELSE

C Calculate pixel.  Here we use the forward polynomials to get a better value.
        if (nc.lt.1) goto 820
C Initial guess.
        pix = polyval(oai(row)+1,cai(0,row),wave)
C Local dispersion.
        disp = ( polyval(oa(row)+1,ca(0,row),pix+5.d0)
     .         - polyval(oa(row)+1,ca(0,row),pix-5.d0) ) / 10.d0
        tw  = polyval(oa(row)+1,ca(0,row),pix)
        i=0
        do while((i.lt.100).and.(abs(tw-wave)/disp.gt.1.d-7))
          pix = pix + (wave-tw)/disp
          tw  = polyval(oa(row)+1,ca(0,row),pix)
          i=i+1
        enddo
        if (i.gt.99) goto 840
        GetSpimPix = pix

      ENDIF
      return
810   continue
C     print *,'GetSpimPix: Error reading WV header cards.'
      goto 900
820   continue
      print *,'GetSpimPix: Error subroutine apparently not initialized.'
      goto 900
830   continue
      print *,'GetSpimPix: Error fitting inverse polynomials.'
      goto 900
840   continue
      print *,'GetSpimPix: Error converging on pixel solution.'
900   continue
      GetSpimPix=-1.d+60
      return
      end

C-------------------------------------------------------------------------
C Find x value given y value and inverse polynomial ( x = f(y) ) and forward 
C polynomial ( y = f(x) ).  This assumes that the inverse polynomial fit 
C provides only an approximate value and the forward polynomial is exactly 
C correct, so the routine uses the inverse as a guess and then iteratively 
C converges on the value using the forward polynomial within a fractional
C error of 1.d-9.
C
      subroutine GetXFromY(x,y,cof,ncof,icof,nicof,ok)
C
      implicit none
      real*8 x,y,cof(*),icof(*),ty,dy,polyval
      integer*4 ncof,nicof,i
      logical ok
C Initial guess.
      x  = polyval(nicof,icof,y)
C Local dispersion.
      dy = polyval(ncof,cof,x+0.5d0) - polyval(ncof,cof,x-0.5d0)
C Trial y.
      ty = polyval(ncof,cof,x)
C Converge in on value.
      i=0
      do while((i.lt.100).and.(abs(ty-y)/dy.gt.1.d-9))
        x  = x + (y-ty)/dy
        ty = polyval(ncof,cof,x)
        i  = i + 1
      enddo
      ok = (i.lt.100)
      return
      end


C----------------------------------------------------------------------
C Get dimensions in a 2-D or 1-D image FITS header.
C
      subroutine GetDimensions(header,sc,ec,sr,er,nc,nr)
C
      implicit none
      character*(*) header
      character*80 chead,wrd
      integer*4 sc,ec,sr,er,nc,nr,inhead,naxis
      logical card_exist
C
C Check number of axis, but do not exit on error.
      naxis= inhead('NAXIS',header)
      if ((naxis.ne.1).and.(naxis.ne.2)) then
        print *,'ERROR in GetDimensions: NAXIS is not 1 or 2.'
      endif
C Check for unknown CTYPE1 types.
C If no CTYPE1 given, assume pixel.
      if (.not.card_exist('CTYPE1',header)) then
        wrd='pixel'
      else
        wrd = chead('CTYPE1',header)
        call lower_case(wrd)
      endif
C If no "pixel" or Vista "poly_lambda" is given in the CTYPE1, assume that
C the starting col and starting row is 1.
      if ((index(wrd,'pixel').eq.0).and.
     .              (index(wrd,'poly_lambda').eq.0)) then
        sc= 1
        sr= 1
      else
        sc= max(0,inhead('CRVAL1',header))
        sr= max(0,inhead('CRVAL2',header))
      endif
      nc= max(1,inhead('NAXIS1',header))
      nr= max(1,inhead('NAXIS2',header))
      ec= sc + nc - 1
      er= sr + nr - 1
      return
      end

C----------------------------------------------------------------------
C Bin data using whole pixels. "mode=0" means normal averaging rebinning,
C "mode=1" means rebin (averaging) error array (ignores negative pixels).
      subroutine DoBin4(bin,np,x,mode)
      implicit none
      integer*4 bin,n,i,mode,pix,np
      real*4 x(*),xt
      logical ok
      if (mode.eq.0) then
        n=0
        pix=1
        do while((pix+bin-1).le.np)
          xt=0.
          do i=pix,(pix+bin-1)
            xt= xt + x(i)
          enddo
          n=n+1
          x(n)= xt / float(bin)
          pix = pix + bin
        enddo
        np = n
      elseif (mode.eq.1) then
        n=0
        pix=1
        do while((pix+bin-1).le.np)
          ok=.true.
          xt=0.
          do i=pix,(pix+bin-1)
            xt= xt + (x(i)*x(i))
            if (x(i).lt.0.) ok=.false.
          enddo
          n=n+1
          if (ok) then
            x(n)= sqrt( xt ) / float(bin)
          else
            x(n)= -1.
          endif
          pix = pix + bin
        enddo
        np = n
      endif
      return
      end
C----------------------------------------------------------------------
C Bin data using whole pixels. "mode=0" means normal averaging rebinning,
C "mode=1" means rebin (averaging) error array.
      subroutine DoBin8(bin,np,x,mode)
      implicit none
      integer*4 bin,n,i,mode,pix,np
      real*8 x(*),xt
      logical ok
      if (mode.eq.0) then
        n=0
        pix=1
        do while((pix+bin-1).le.np)
          xt=0.
          do i=pix,(pix+bin-1)
            xt= xt + x(i)
          enddo
          n=n+1
          x(n)= xt / dfloat(bin)
          pix = pix + bin
        enddo
        np = n
      elseif (mode.eq.1) then
        n=0
        pix=1
        do while((pix+bin-1).le.np)
          ok=.true.
          xt=0.
          do i=pix,(pix+bin-1)
            xt= xt + (x(i)*x(i))
            if (x(i).lt.0.) ok=.false.
          enddo
          n=n+1
          if (ok) then
            x(n)= sqrt( xt ) / dfloat(bin)
          else
            x(n)= -1.d0
          endif
          pix = pix + bin
        enddo
        np = n
      endif
      return
      end


C------------------------------------------------------------------------
C Return function value of polynomial given a "coeffecient array".
C
C    ca(1) is the number of coeffecients (polynomial order+1).
C    ca(2..) are the coeffecients.
C
      real*8 function polyarrNOoff(ca,xv)
C
      implicit none
      integer*4 nc,i
      real*8 ca(*),xv,r
      nc = nint(ca(1))
      r  = 0.d0
      do i=1,nc-1
        r = ( r + ca(nc+2-i) ) * xv
      enddo
      polyarrNOoff = r + ca(2)
      return
      end

C------------------------------------------------------------------------
C Return function value of polynomial given a "coeffecient array".
C THIS VERSION ASSUMES an x-offset value in the first position.
C
C    ca(1) is "x offset".
C    ca(2) is the number of coeffecients (polynomial order+1).
C    ca(3..) are the coeffecients.
C
      real*8 function polyarroff(ca,xv)
C
      implicit none
      integer*4 nc,i
      real*8 ca(*),xv,r,xoff
      xoff= ca(1)
      nc  = nint(ca(2))
      r   = 0.d0
      do i=1,nc-1
        r = ( r + ca(nc+3-i) ) * (xv - xoff)
      enddo
      polyarroff = r + ca(3)
      return
      end


C----------------------------------------------------------------------
C Replace NANs with a constant.
      subroutine fixnans(a,npix,constant)
      implicit none
      real*4 a(*),constant
      integer*4 npix,i
      do i=1,npix
        if (a(i).ne.a(i)) a(i) = constant
      enddo
      return
      end

C----------------------------------------------------------------------
C Convert an ASCII file to a PostScript file using Courier fonts.
C "land" : Use landscape mode.  "pt=n" : Specify pointsize n (default 10).
C "bold" : Use Courier-Bold instead of Courier (warning: different aspect).
C "head" : Put header at top of file, i.e. filename and date.
C "left=n" : Shift left n points (n = 70 = one inch).
C
C ptsz char/line line/page   LANDSCAPE: char/line line/page'
C  12 ..  79  .....  55  ................  105  ...  40'
C  10 ..  92  .....  66  ................  126  ...  48'
C   9 .. 107  .....  73  ................  142  ...  53'
C   8 .. 117  .....  82  ................  160  ...  60'
C   7 .. 131  .....  94  ................  183  ...  68'
C   6 .. 170  ..... 110  ................  210  ...  80'
C
C If psfile is blank, send to standard output.
C
      subroutine ASCII_to_PS(afile,psfile,ptsz,left,bold,land,head)
C
      implicit none
      character*(*) afile,psfile
      integer*4 ptsz,left
      logical bold,land,head
      integer*4 iun,oun,nlpp,hpos,lno,lc,i
      character*300 s,ss,header
      character*24 c24
C Line skip scaling factor.
      real*4 lss
      parameter(lss=1.1)
C Form header.
      call fdate(c24)
      write(header,'(4a)') ':: ',afile(1:lc(afile)),' ::  ',c24
C Open ASCII file.
      open(1,file=afile,status='old')
      iun=1
C Open output file (if it was specified).
      if (psfile.ne.' ') then
        open(2,file=psfile,status='unknown')
        oun=2
      else
        oun=6
      endif
C Number of lines per page.
      if (land) then
        nlpp = int( 530. / ( float(ptsz)*lss ) )
      else
        nlpp = int( 730. / ( float(ptsz)*lss ) )
      endif
C Write beginning stuff.
      write(oun,'(a)') '%!PS-Adobe-2.0' 
      write(oun,'(a,i3,a)') '/ptsz ',ptsz,' def'
      write(oun,'(a,f5.2,a)') '/lskip ptsz ',lss,' mul def'
      write(oun,'(a)') '/landscape{90 rotate 000 -800 translate}def'
      write(oun,'(a)') '/vinit 754 def'
      if (ptsz.lt.10) then
        hpos = 20 + left
      else
        hpos = 30 + left
      endif
      write(oun,'(a,i3,a)') '/hpos ',min(999,max(-99,hpos)),' def'
      write(oun,'(a)') 
     .    '/newline{/vpos vpos lskip sub def hpos vpos moveto}def'
      if (land) then
        write(oun,'(2a)') '/newpage{/vpos vinit def hpos vpos move',
     .                    'to landscape newline}def'
      else
        write(oun,'(a)') 
     .    '/newpage{/vpos vinit def hpos vpos moveto}def'
      endif
      write(oun,'(a)') '/l{show newline}def'
      if (bold) then
        write(oun,'(a)') '/Courier-Bold findfont'
      else
        write(oun,'(a)') '/Courier findfont'
      endif
      write(oun,'(a)') 'ptsz scalefont'
      write(oun,'(a)') 'setfont'
      write(oun,'(a)') 'newpage'
      lno=1
C Add header to top.
      if (head) then
        write(oun,'(3a)',err=802) '(',header(1:lc(header)),')l'
        lno=lno+1
      endif
C Print lines.
1     continue 
      read(iun,'(a)',end=5,err=801) s
      i=1
      do while(i.le.lc(s))
        if ((s(i:i).eq.')').or.(s(i:i).eq.'(').or.(s(i:i).eq.'\\')) then
          write(ss,'(3a)') s(1:i-1),'\\',s(i:len(s)-1) 
          s=ss
          i=i+1
        endif
        i=i+1
      enddo
      write(oun,'(3a)',err=802) '(',s(1:lc(s)),')l'
      if (mod(lno,nlpp).eq.0) then 
        write(oun,'(a)') 'showpage'
        write(oun,'(a)') 'newpage'
        lno=1
      else
        lno=lno+1 
      endif
      goto 1
5     continue
      write(oun,'(a)') 'showpage'
      close(iun)
      if (oun.ne.6) close(oun)
      return
801   continue
      print *,'Error reading input file.'
      return
802   continue
      print *,'Error writing output.'
      return
      end

C----------------------------------------------------------------------
C mode=0: mm/dd/yy .  mode=1: yy/mm/dd .
C
      real*8 function getdate(c8,mode)
      character*8 c8
      integer*4 mm,dd,yy,mode
      real*8 DecimalYear,valread8
      if (mode.eq.0) then
        mm = nint(valread8(c8(1:2)))
        dd = nint(valread8(c8(4:5)))
        yy = nint(valread8(c8(7:8)))
      else
        yy = nint(valread8(c8(1:2)))
        mm = nint(valread8(c8(4:5)))
        dd = nint(valread8(c8(7:8)))
      endif
      getdate = DecimalYear(mm,dd,yy)
      return
      end

C----------------------------------------------------------------------
C Input: month, day, year (given in two digits, e.g. 1997 is just 97).
      real*8 function DecimalYear(month,day,year)
      implicit none
      integer*4 month,day,year
      integer*4 NDAYS(12),NDAYSLY(12)
      data NDAYS   / 0,31,59,90,120,151,181,212,243,273,304,334 /
      data NDAYSLY / 0,31,60,91,121,152,182,213,244,274,305,335 /
C Year 2000 fix.
      if (year.lt.30) year=year+100
C Leap year (the year 2000 IS a leap year).
      if (mod(year,4).eq.0) then
        DecimalYear = dfloat(year) + dfloat(NDAYSLY(month)+(day-1))/366.
      else
        DecimalYear = dfloat(year) + dfloat(NDAYS(month)  +(day-1))/365.
      endif
      return
      end


C----------------------------------------------------------------------
C Convert a decimal year into a month, day and year.
C   Input: dy (decimal year value).
C   Output:  month, day, year.
C
      subroutine ConvertDecimalYear(dy,month,day,year)
C
      implicit none
      real*8 dy
      real*4 valread
      integer*4 month,day,year,t
      character*17 s
C 2 or 4 digits in year?
      if (dy.lt.1000.) then
        if (dy.gt.50.) then     ! Y2K check
          dy = dy + 1900.
        else
          dy = dy + 2000.
        endif
      endif
C Seconds since 1970.
      t = nint( ( dy - 1970. ) * 3.155760d+7 )
C Convert a time in seconds since 1/1/1970 00:00:00 to a string in the form:
C "YY MM DD HH:NN:SS".
C  ----+----1----+--
      call make_time_string(t,s,0)
      year = nint(valread(s(1:2)))
      month= nint(valread(s(4:5)))
      day  = nint(valread(s(7:8)))
C
      return
      end



C----------------------------------------------------------------------
C Place carraige returns at the same column for every line of a text file
C by finding the longest line and padding blanks to any and all shorter lines.
C
      subroutine padtext(infile,user_outfile,verbose)
C
      implicit none
      character*(*) infile,user_outfile
      logical verbose
      character*500 outfile,wrd
      character*5000 line
      integer*4 i,k,lc,high,time,ios

C Open output file.  Be sure not to conflict with any other running programs.
      if (user_outfile(1:8).eq.'-clobber') then
        i = time()      ! Seconds since 1/1/1970.
        write(outfile,'(a,i10.10)') '/tmp/pad-',i
        open(2,file=outfile,status='new',iostat=ios)
        do while((ios.ne.0).and.(i.gt.0))
          close(2)
          i=i-1000
          call sleep(1)
          print *,'padtext: WARNING: Trying alternative pad file. i=',i
          write(outfile,'(a,i10.10)') '/tmp/pad-',i
          open(2,file=outfile,status='new',iostat=ios)
        enddo
        if (i.lt.2) then
          print *,'padtext: ERROR: big problem #1.'
          return
        endif
      else
        outfile = user_outfile
        open(2,file=outfile,status='new',iostat=ios)
        if (ios.ne.0) then
          print *,'padtext: ERROR: big problem #2.'
          return
        endif
      endif

C Find longest line in input file.
      high=0
      open(1,file=infile,status='old')
1     continue
      line=' '
      read(1,'(a)',end=5) line
      k = lc(line)
      if (k.gt.high) high=k
      goto 1
5     close(1)

C Echo.
      if (verbose) then
        print *,'Longest line is',high,' characters long.'
        print *,'All lines will be this long, padded with blanks.'
        print '(2a)','Reading: ',infile(1:lc(infile))
        print '(2a)','Writing: ',outfile(1:lc(outfile))
      endif

C Write new file.
      open(1,file=infile ,status='old')
11    continue
      line=' '
      read(1,'(a)',end=55) line
Ctemp
C     do i=1,high
C       k = ichar(line(i:i))
C       if ((k.lt.32).or.(k.gt.126)) line(i:i)=' '
C     enddo
Ctemp
      write(2,'(a)') line(1:high)
      goto 11
55    continue
      close(1)
      close(2)

C Clobber.
      if (user_outfile(1:8).eq.'-clobber') then
        wrd = '/usr/bin/mv '//outfile(1:lc(outfile))//
     .        ' '//infile(1:lc(infile))
        print '(a)',wrd(1:lc(wrd))
        call sys(wrd)
      endif

      return
      end


C----------------------------------------------------------------------
C Read FITS header (for (raw) HIRES data) from a file.
C ... Will also read ESI headers (tab 21mar00).
C
      subroutine rhh_main(arg,narg,ok)

      implicit none
      integer*4 m
      parameter(m=300000)
      real*4 ee(m),yy(m),arr(m),s2n,s2n80,valread,minexpos
      real*8 fhead,xdangl,echangl,cenwave,bluewave,redwave
      real*8 GetSpimWave,ra,dec,equinox
      real*8 w1,w2,mean_wr,mean_dv,c,slitwid,fgetlinevalue
      character*80 arg(900),wrd,fil1name,fil2name,UTtime
      character*80 dfile,efile,vfile,slmsknam,synop
      character*80 chead,object,obstype,lampname,lfilname
      character*80 filename,deckname,xdispers
      character*80 filelist
      character header*115200, obsdate*7,radec*9,c2s*2,c16*16
      integer*4 nfile,access,n,npix,row
      integer*4 narg,i,frameno,inhead
      integer*4 ttime,lc,sc,ec,sr,er,nc,nr
      integer*4 revindex,colbin,rowbin,maxpt
      logical findarg,allwave,wave,eachrow,big0,big1,big2,big3
      logical date,size,bin,sig,ok,columnheader,hires2
      logical card_exist,atmextnc,initials,nofluxstr
      logical firstpass,brief
C
      include 'soun.inc'
      include 'global.inc'
C
 
C Set parameters.
      firstpass = .true.
      maxpt= m
      allwave = findarg(arg,narg,'-allwave','s',wrd,i)
      wave = findarg(arg,narg,'-wave','s',wrd,i)
      date = findarg(arg,narg,'-date','s',wrd,i)
      size = findarg(arg,narg,'-size','s',wrd,i)
      bin  = findarg(arg,narg,'-bin' ,'s',wrd,i)
      big0 = findarg(arg,narg,'-big0','s',wrd,i)
      big1 = findarg(arg,narg,'-big1','s',wrd,i)
      big2 = findarg(arg,narg,'-big2','s',wrd,i)
      big3 = findarg(arg,narg,'-big3','s',wrd,i)
      sig  = findarg(arg,narg,'-sig', 's',wrd,i)
      brief= findarg(arg,narg,'-brief','s',wrd,i)
      hires2 = findarg(arg,narg,'-hires2','s',wrd,i)
      if (.not.hires2) then
        hires2 = findarg(arg,narg,'-h2','s',wrd,i)
      endif
      columnheader = findarg(arg,narg,'-columnheader', 's',wrd,i)
      if (.not.columnheader) then
        columnheader = findarg(arg,narg,'-ch', 's',wrd,i)
      endif
      eachrow = findarg(arg,narg,'-eachrow','s',wrd,i)
      filelist= ' '
      if (findarg(arg,narg,'filelist=',':',wrd,i)) filelist = wrd
      if (big1.or.big2.or.big0.or.big3.or.date.or.eachrow) then
        wave=.true.
      endif
      minexpos = 0.
      if (findarg(arg,narg,'minexpos=',':',wrd,i)) then
        minexpos = valread(wrd)
      endif
C Unadvertised.
      initials  = findarg(arg,narg,'-initials',':',wrd,i)
      nofluxstr = findarg(arg,narg,'-nofluxstr',':',wrd,i)
C
      if ( (narg.lt.1) .and. 
     .    (.not.((narg.eq.0).and.(filelist.ne.' '))) ) then
        print *,'Syntax:  rhh  (list of FITS file(s)) or  [filelist=]'
        print *,'   [-wave] [-eachrow] [-size] [-date] [-sig] [-bin]'
        print *,'   [-big0 | -big1 | -big2 | -big3]'
        print *,'   [-columnheader | -ch]   [ -brief ]  [ -allwave ]'
        print *,' '
        print *,'  Read HIRES/ESI FITS Headers.'
        print *,'     (Print a one line listing for each file.)'
        print *,' '
        print *,
     . ' filelist=  : Filename of file with list of FITS files.'
        print *,' -wave : Show wavelength range for calibrated data.'
        print *,
     . ' -date : Include date on line (when used with -wave).'
        print *,' -size : Show number of columns and rows.'
        print *,
     . ' -bin  : Show binning, decker, slit width, filter #2 (CuSO4).'
        print *,
     . ' -sig  : Try to calculate S/N from error file (form: *e.fits,'
        print *,'         E-*.fits, Err-*.fits.)'
        print *,
     . ' -big0 : Original big format: date, RA, Dec, wavelengths, etc.'
        print *,
     . ' -big1 : New big format: date, RA, Dec, wavelengths, S/N, etc.'
        print *,
     . '         (for ESI data, use -big1 for 80 column list with S/N)'
        print *,
     . ' -big2 : New big format (<80 columns).'
        print *,
     . ' -big3 : Similar to big2 but allows for longer object names.'
        print *,
     . ' -eachrow : Show wavelength range for each row, average'
        print *,
     . '            dispersion, and bluest and redest dispersion.'
        print *,
     . ' -nofluxstr : Delete (Flux) from object name.'
        print *,
     . ' minexpos=  : Minimum exposure time to include in list.'
        print *,
     . '   -brief   : Show HIRES2 data in a brief format.'
        print *,
     . '   -allwave : Show wavelengths of each order for each file.'
        ok=.false.
        return
      endif

C File list?
      if (filelist.ne.' ') then
        narg=0
        open(1,file=filelist,status='old')
1       continue
        narg=narg+1
        read(1,'(a)',end=5) wrd
        goto 1
5       continue
        narg=narg-1
        close(1)
      endif

C File list?
      if (filelist.ne.' ') open(16,file=filelist,status='old')

C List each file.
      DO nfile=1,narg

C File list?
      if (filelist.ne.' ') then
        read(16,'(a)') dfile
        if ((initials).and.(index(dfile,'-').gt.0)) then
          i = index(dfile,'-')
          if ((dfile(i-2:i-2).eq.'9').or.(dfile(i-2:i-2).eq.'0')) then
            c2s = dfile(i+1:i+2)
          else
            c2s = '  '
          endif
        endif
      else
        dfile = arg(nfile)
      endif
      call AddFitsExt(dfile)
C Check for HIRES2 on first pass.
      if (firstpass.and.(.not.hires2)) then
        call readfits_header_rawhires2(dfile,header,ok)
        wrd = chead('DETECTOR',header)
        if (index(wrd,'Mosaic').gt.0) hires2 = .true.
        firstpass = .false.
      endif
C Special read for HIRES2 files.
      if (hires2) then
        call readfits_header_rawhires2(dfile,header,ok)
      else
        call readfits_header(dfile,header,ok)
      endif
      if (.not.ok) goto 900

C HIRES or ESI?
      Global_HIRES = .false.
      Global_ESI   = .false.
      wrd = chead('INSTRUME',header)
      if (index(wrd,'HIRES').gt.0) then
        Global_HIRES = .true.
      elseif (index(wrd,'ESI').gt.0) then
        Global_ESI = .true.
      endif
C Restrictions on ESI.
      if (Global_ESI) then
        sig =.false.
        big2=.false.
        big3=.false.
      endif

C Try to put column header if requested.
      if ((nfile.eq.1).and.(columnheader)) then
        if ((Global_ESI).and.(big1)) then 
          write(soun,'(2a)') 
     .    'Coordinate  Date  Frame Obs.Type    Slit  Bin ',
     .    'Expos. Mdn.S/N(80%)  Name      '
        elseif ((Global_ESI).and.(.not.big1)) then 
          write(soun,'(2a)') 
     .    'Frame Filename Date(UT) Time  Obs.Type     ',
     .    ' Slit  Bin  Expos. Name'
        elseif ((Global_HIRES).and.(big1)) then 
          write(soun,'(3a)') 
     .    'Coordinate Date(UT) Fra# Filaname     EchAngl  XDAngl',
C    .    '1942-1502  30Jun00   17  Flux-017     -0.0670 -0.2320',
     .    ' Deck Filt1  Lamp  L.Filt.  Wavelengths  Expos',
C    .    '  C5  kv380  none   ng3    4162.9 6525.4 3000s ',
     .    '  Mdn.S/N  Object Name'
C    .    '   14.50  NGC 6822 ee (Flux)'
        elseif ((Global_HIRES).and.(.not.big1).and.(.not.big0).and.
     .          (.not.big2).and.(.not.big3).and.(.not.date).and.
     .          (.not.sig)) then
          write(soun,'(2a)')
     .    'Fra#  Filename   EchAngl  XDAngl   XD  Lamp L.Filt ',
C    .    ' 138  Flux-138   -0.0670 -0.2320  RED  none   ng3  ',
     .    ' Expos  Object Name'
C    .    '  300s  Feige 67 (Flux)'
        endif
      endif

C Get filename.
      filename= dfile(1:index(dfile,'.fits')-1)
      i = revindex(filename,'/')
      if (i.gt.0) filename = filename(i+1:)

C Dimensions.
      nc = inhead('NAXIS1',header)
      nr = inhead('NAXIS2',header)
      sc = inhead('CRVAL1',header)
      sr = inhead('CRVAL2',header)
      ec = sc + nc - 1
      er = sr + nr - 1

C Has the data (flux) file been extinction corrected?
      atmextnc = card_exist('ATMEXTNC',header)  

C Signal to noise...
      s2n  =0.
      s2n80=0.
      if (sig.or.big1.or.big2.or.big3) then
        efile=' '
        vfile=' '
        i = index(dfile,'Flux-')
        if (i.gt.0) efile = dfile(1:i-1)//'Err'//dfile(i+4:)
        if (i.gt.0) vfile = dfile(1:i-1)//'Var'//dfile(i+4:)
        i = index(dfile,'F-')
        if (i.gt.0) efile = dfile(1:i-1)//'E'//dfile(i+1:)
        if (efile.eq.' ') then
          i = index(dfile,'.fits')
          efile = dfile(1:i-1)//'e'//dfile(i:)
        endif
C Check existence of error or variance files.
        s2n=0.
        if ((access(efile,'r').ne.0).and.(access(vfile,'r').ne.0)) then
          write(soun,'(5a)')
     .      ' Error- Could not find error (or variance) file: ',
     .      efile(1:lc(efile)),' (',vfile(1:lc(vfile)),')'
          s2n = -9.
        endif
        if ((access(efile,'r').ne.0).and.(access(vfile,'r').eq.0)) then
          if (atmextnc) then
            write(soun,'(2a)') ' Error- Have variance file, ',
     .                    'but data has been extinction corrected.'
            s2n=-9.
          endif
        endif
C Find signal-to-noise. 
        if (s2n.gt.-1.) then
          npix = nc*nr
          if (access(efile,'r').eq.0) then
            call readfits(efile,ee,maxpt,header,ok)
            if (.not.ok) goto 900
          else
            call readfits(vfile,ee,maxpt,header,ok)
            if (.not.ok) goto 900
            do i=1,npix
              if (ee(i).gt.0.) then
                ee(i) = sqrt(ee(i))
              else
                ee(i) = 0.
              endif
            enddo
          endif
          call readfits(dfile,yy,maxpt,header,ok)
          if (.not.ok) goto 900
          n=0
          do i=1,npix
            if (ee(i).gt.0.) then
              n=n+1
              arr(n) = yy(i)/ee(i)
            endif
          enddo
          call find_median(arr,n,s2n)
          s2n80 = arr( nint(float(n) * 0.8) )
          s2n80 = max(-9.,min(999.,s2n80))
        endif
        s2n = max(-9.,min(999.,s2n))
      endif

C Find binning.
      if (card_exist('BINNING',header)) then
        wrd = chead('BINNING',header)
        colbin = nint( fgetlinevalue(wrd,1) )
        rowbin = nint( fgetlinevalue(wrd,2) )
      else
        colbin = 0
        rowbin = 0
      endif

      if (wave) then
C Initialize wavelength = func(pixel) function.
        if (GetSpimWave(header,0.d0,0).lt.0.) then
          bluewave=0.d0
          redwave =0.d0
        else
C Wavelength range.
          bluewave= GetSpimWave(header,dfloat(sc),1)
          redwave = GetSpimWave(header,dfloat(ec),er)
        endif
      endif 

C Show every order.
      if (allwave) then
        do row=1,er
          if (GetSpimWave(header,0.d0,0).lt.0.) then
            bluewave=0.d0
            cenwave =0.d0
            redwave =0.d0
          else
            bluewave= GetSpimWave(header,dfloat(sc),row)
            cenwave = GetSpimWave(header,dfloat(((sc+ec)/2)),row)
            redwave = GetSpimWave(header,dfloat(ec),row)
          endif
          write(soun,'(9x,i5,3f10.2)') 
     .      row,bluewave,cenwave,redwave 
        enddo
      endif

C Header cards.
      frameno = inhead('FRAMENO',header)
      slitwid = fhead('SLITWID',header)
      xdangl  = fhead('XDANGL',header)
      echangl = fhead('ECHANGL',header)
      lampname= chead('LAMPNAME',header)
      lfilname= chead('LFILNAME',header)
      deckname= chead('DECKNAME',header)
      xdispers= chead('XDISPERS',header)
      fil1name= chead('FIL1NAME',header)
      fil2name= chead('FIL2NAME',header)
      object  = chead('OBJECT',header)
C Delete Flux string?
      if (nofluxstr) then
        i=index(object,'(Flux)')
        if (i.gt.1) object = object(1:i-1)//object(i+6:lc(object))
      endif
      ttime   = inhead('TTIME',header)
      if (ttime.lt.minexpos) goto 808      ! skip this listing...
      call GetFitsDate(header,obsdate,1)
      ra      = fhead('RA',header)
      dec     = fhead('DEC',header)
      equinox = fhead('EQUINOX',header)
      if (abs(equinox-1950.).gt.0.01) then
        call precess(equinox,1950.d0,ra,dec)
      endif
      call write_radec(ra,dec,radec,2)

      if (Global_ESI) then
        UTtime = chead('UT',header)          ! Universal time.
        obstype = chead('OBSTYPE',header)    ! observation type
        slmsknam = chead('SLMSKNAM',header)  ! Slit mask name.
        wrd      = chead('SYNOPFMT',header)  ! observing mode.
        synop = '??'
        if (index(wrd,'Echellette').gt.0) synop = 'Ec'
        if (index(wrd,'Imaging')   .gt.0) synop = 'Im'
        if (index(wrd,'LowRes')    .gt.0) synop = 'Lo'
        
C Lamp String for ESI data.
        c16=' '
        wrd = chead('LAMPCU1',header)
        if (index(wrd,'on').gt.0) c16 = c16(1:lc(c16))//'+cu1'
        wrd = chead('LAMPNE1',header)
        if (index(wrd,'on').gt.0) c16 = c16(1:lc(c16))//'+ne1'
        wrd = chead('LAMPNE2',header)
        if (index(wrd,'on').gt.0) c16 = c16(1:lc(c16))//'+ne2'
        wrd = chead('LAMPQTZ1',header)
        if (index(wrd,'on').gt.0) c16 = c16(1:lc(c16))//'+qtz1'
C
C Write line for ESI data.
        if (big1) then
          write(soun,'(a9,2x,a7,i4,2x,a2,a1,a4,a4,
     .                 1x,a4,1x,i2,a,i1,1x,i5,a,f6.1,f6.1,2x,a14)')
     .     radec,
     .     obsdate,
     .     frameno,
     .     synop(1:2),':',
     .     obstype(1:4),
     .     c16(1:4),
     .     slmsknam(1:4),
     .     colbin,'x',rowbin,
     .     ttime,'s',
     .     s2n,s2n80,
     .     object(1:14)
        else
          write(soun,'(i4,2x,a9,a7,1x,a5,2x,a2,a1,a4,a6,
     .                 1x,a4,1x,i2,a,i1,1x,i5,a,a18)')
     .     frameno,filename(1:9),
     .     obsdate(1:7),UTtime(1:5),
     .     synop(1:2),':',
     .     obstype(1:4),
     .     c16(1:6),
     .     slmsknam(1:4),
     .     colbin,'x',rowbin,
     .     ttime,'s  ',
     .     object(1:18)
        endif

C The rest is for HIRES data only.
      elseif (big0) then
       write(soun,'(a9,2x,a7,i5,2x,a12,2f8.4,2x,
     .              a,1x,a,2(2x,a),1x,2f7.1,i5,2a)')
     .    radec,obsdate,frameno,filename,echangl,xdangl,
     .    deckname(1:3),fil1name(1:5),lampname(1:5),lfilname(1:5),
     .    bluewave,redwave,ttime,'s  ',object(1:min(29,lc(object)))

      elseif (big1.and.initials) then
        write(soun,'(a9,2x,a7,i5,2x,a12,2f8.4,2x,a,1x,a,2(2x,a),1x,
     .    2f7.1,i5,a,f7.2,2x,4a)') 
     .    radec,obsdate,frameno,filename,echangl,xdangl,
     .    deckname(1:3),fil1name(1:5),lampname(1:5),lfilname(1:5),
     .    bluewave,redwave,ttime,'s  ',s2n,object(1:min(29,lc(object))),
     .    '  {',c2s,'}'

      elseif (big1) then
        write(soun,'(a9,2x,a7,i5,2x,a12,2f8.4,2x,a,1x,a,2(2x,a),1x,
     .    2f7.1,i5,a,f7.2,2x,a)') 
     .    radec,obsdate,frameno,filename,echangl,xdangl,
     .    deckname(1:3),fil1name(1:5),lampname(1:5),lfilname(1:5),
     .    bluewave,redwave,ttime,'s  ',s2n,object(1:min(29,lc(object)))

      elseif (big2) then
        write(soun,'(a9,2x,a7,i5,1x,2f8.4,1x,a,2f7.1,i5,a,f6.1,2x,a)')
     .    radec,obsdate,frameno,echangl,xdangl,deckname(1:3),
     .    bluewave,redwave,ttime,'s',s2n,object(1:min(7,lc(object)))

      elseif (big3.and.initials) then
        write(soun,'(a9,2x,a7,i4,2f7.3,2i5,i5,a,f6.1,1x,4a)')
     .    radec,obsdate,frameno,echangl,xdangl,
     .    nint(bluewave),nint(redwave),ttime,'s',s2n,
     .    object(1:16),
     .            ' {',c2s,'}'

      elseif (big3) then
        write(soun,'(a9,2x,a7,i4,2f7.3,2f7.1,i5,a,f6.1,2x,a)')
     .    radec,obsdate,frameno,echangl,xdangl,
     .    bluewave,redwave,ttime,'s',s2n,object(1:min(16,lc(object)))

      elseif (size) then
          write(soun,'(a5,i4,2x,a9,2f8.4,i7,a,i4,1x,i5,2a)')
     .      obsdate(3:7),frameno,filename,echangl,xdangl,
     .      nc,' x',nr,ttime,'s  ',object(1:min(22,lc(object)))


      elseif (sig) then
        if (wave) then
          write(soun,'(i4,2x,a9,2f8.4,2f7.1,i5,a,f7.2,2x,a)')
     .     frameno,filename,echangl,xdangl,
     .     bluewave,redwave,ttime,'s  ',s2n,object(1:min(18,lc(object)))
        else
          write(soun,'(i4,2x,a9,2f8.4,2x,a,2x,a,i5,a,f7.2,2x,a)')
     .      frameno,filename,echangl,xdangl,lampname(1:5),lfilname(1:5),
     .      ttime,'s  ',s2n,object(1:min(18,lc(object)))
        endif


C---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8
      elseif (hires2) then

        if (brief) then
          write(soun,'(i5,2x,a12,2f8.4,2x,a,2x,a,2x,a,i5,2a)')
     .     frameno,filename,echangl,xdangl,xdispers(1:3),
     .     lampname(1:5),lfilname(1:5),ttime,'s  ',
     .     object(1:min(21,lc(object)))
        else

          if (bin) then
            write(soun,
     .       '(a7,i6,2x,a20,2f8.4,2x,a,2x,a,2x,a,i5,a,
     .         i2,a,i1,1x,a3,f5.2,2x,a,2x,a)')
     .       obsdate(1:7),frameno,filename,echangl,xdangl,
     .       xdispers(1:3),lampname(1:5),lfilname(1:5),ttime,'s ',
     .       colbin,'x',rowbin,deckname(1:3),min(99.99,slitwid),
     .       radec,object(1:min(21,lc(object)))
          else

            if (wave) then
              write(soun,
     .   '(a7,i6,2x,a12,2f8.4,2x,
     .     a3,2x,a5,2x,a5,
     .     i5,a,i2,a,i1,1x,a3,
     .     a5,2x,a5,1x
     .     2i6,2x,a,2x,a)')
     .         obsdate(1:7),frameno,filename,echangl,xdangl,
     .         xdispers(1:3),lampname(1:5),lfilname(1:5),
     .         ttime,'s ',colbin,'x',rowbin,deckname(1:3),
     .         fil1name(1:5),fil2name(1:5),
     .         nint(bluewave),nint(redwave),
     .         radec,object(1:min(21,lc(object)))
            else

              if (lc(filename).gt.12) then
                write(soun,
     .          '(a7,i6,2x,a20,2f8.4,2x,a,2x,a,2x,a,i5,a,a,2x,a)')
     .          obsdate(1:7),frameno,filename,echangl,xdangl,
     .          xdispers(1:3),lampname(1:5),lfilname(1:5),ttime,'s  ',
     .          radec,object(1:min(21,lc(object)))
              else
                write(soun,
     .          '(a7,i6,2x,a12,2f8.4,2x,a,2x,a,2x,a,i5,a,a,2x,a)')
     .          obsdate(1:7),frameno,filename,echangl,xdangl,
     .          xdispers(1:3),lampname(1:5),lfilname(1:5),ttime,'s  ',
     .          radec,object(1:min(21,lc(object)))
              endif
            endif
          endif
        endif


      elseif (bin) then
          write(soun,'(i4,2x,a9,2f8.4,2x,a5,1x,a3,f5.2,i2,a,i1,i6,2a)')
     .      frameno,filename,echangl,xdangl,
     .      fil2name(1:5),deckname(1:3),min(99.99,slitwid),
     .      colbin,'x',rowbin,ttime,'s  ',object(1:min(19,lc(object)))


      elseif (wave) then
        if (date) then
          write(soun,'(a7,i4,2x,a10,2f8.4,2f7.1,i5,2a)')
     .      obsdate(1:7),frameno,filename,echangl,xdangl,
     .      bluewave,redwave,ttime,'s  ',object(1:min(19,lc(object)))
        else
          write(soun,'(i4,2x,a10,2f8.4,2x,a,2f7.1,i5,2a)')
     .      frameno,filename,echangl,xdangl,xdispers(1:3),
     .      bluewave,redwave,ttime,'s  ',object(1:min(21,lc(object)))
        endif
        if (eachrow) then
          c = 2.99792458d+5
          mean_wr = 0.d0
          mean_dv = 0.d0
          do i=sr,er
            bluewave= GetSpimWave(header,dfloat(sc),i)
            redwave = GetSpimWave(header,dfloat(ec),i)
            w1      = GetSpimWave(header,dfloat(sc+1),i)
            w2      = GetSpimWave(header,dfloat(ec-1),i)
            mean_wr = mean_wr + (redwave-bluewave)
            mean_dv = mean_dv + ( ( (redwave-bluewave)/dfloat(ec-sc) )
     .                          / ( (redwave+bluewave)/2.d0 ) )
            write(soun,'(i4,a,2f11.4,f8.4,a,2f8.4,2a,2f7.3,a)') i,' :',
     .        bluewave,redwave,(redwave-bluewave)/dfloat(ec-sc),
     .        '  [',w1-bluewave,redwave-w2,' ]',
     .        '  [',c*(w1-bluewave)/bluewave,c*(redwave-w2)/redwave,' ]'
          enddo
          mean_wr = mean_wr / dfloat(1+er-sr)
          write(soun,'(a,f8.3,a,f7.4,a,f6.3)')
     .               ' Mean:  Wavelength range=',mean_wr,
     .               '    Ang/pix=',mean_wr/dfloat(ec-sc),
     .               '    Vel/pix=',c*mean_dv/dfloat(1+er-sr)
        endif

      else
        write(soun,'(i4,2x,a10,2f8.4,2x,a,2x,a,2x,a,i5,2a)')
     .     frameno,filename,echangl,xdangl,xdispers(1:3),
     .     lampname(1:5),lfilname(1:5),ttime,'s  ',
     .     object(1:min(21,lc(object)))

      endif

808   continue

      ENDDO

C Close file.
      if (filelist.ne.' ') close(16)

      ok=.true.
      return

900   continue
      ok=.false.
      write(soun,'(a)') 'Error reading FITS file.'
      return
      end 


C----------------------------------------------------------------------
C Open a file to append to it.
C
      subroutine open_for_append(file,oun)
C
      implicit none
      character*(*) file
      integer*4 oun
      character*80 wrd
C
      open(oun,file=file,status='unknown')
1     read(oun,'(a)',end=5) wrd
      goto 1
5     return
      end

C----------------------------------------------------------------------
C wire_int2hex
C
      subroutine wire_int2hex(i,s)
C
      implicit none
      integer*4 i,lc
      character*(*) s
      call int2hex(i,s)
      if (lc(s).eq.7) then
        s =    '0'//s(1:7)
      elseif (lc(s).eq.6) then
        s =   '00'//s(1:6)
      elseif (lc(s).eq.5) then 
        s =  '000'//s(1:5)
      elseif (lc(s).eq.4) then
        s = '0000'//s(1:4)
      endif
      return
      end


C----------------------------------------------------------------------
C Get the last modified time for a file.
C Time is in seconds since 00:00:00, January 1, 1970 (GMT).
C Return -1 if error.
C
      integer*4 function filetime(file)
C
      implicit none
      character*(*) file
      integer*4 lstat,statb(13),ierr
      ierr =  lstat(file,statb)
      if (ierr.eq.0) then
        filetime = statb(10)
      else
        filetime = -1
      endif
      return
      end

C----------------------------------------------------------------------
C Convert a string in the form: "YY MM DD HH NN SS.SSS".
C Where the blanks can be any character.
C
C "YY MM DD HH:NN:SS.SSS" to seconds since 1970.  (Y2K ok: 1970 to 2070.)
C
      subroutine convert_time_string(s,t,mode)
C
      implicit none
      integer*4 mode              ! 0=YY MM DD  1=YY/MM/DD  2=YY-MM-DD (input).
      character*(*) s             ! Time string (input).
      integer*4 t                 ! Seconds since 1/1/1970 (output).
      character*80 w
      character c1*1
      integer*4 firstchar,lastchar
      integer*4 yy,mm,dd,hh,nn,ss,i
      integer*4 nd(12),ndly(12),doy
      real*4 valread
C
      data nd   / 0,31,59,90,120,151,181,212,243,273,304,334 /
      data ndly / 0,31,60,91,121,152,182,213,244,274,305,335 /
C
      if (mode.eq.0) then
        c1=' '
      elseif (mode.eq.1) then
        c1='/'
      elseif (mode.eq.2) then
        c1='-'
      endif
C
      w=s(firstchar(s):lastchar(s))   !YY/MM/DD HH:NN:SS.SSS
      i=index(w,c1)
      yy = nint(valread(w(1:i-1)))
      if (yy.ge.70) then
        yy=yy+1900
      else
        yy=yy+2000
      endif
      w=w(i+1:)                       !MM/DD HH:NN:SS.SSS
      i=index(w,c1)
      mm = nint(valread(w(1:i-1)))
      w=w(i+1:)                       !DD HH:NN:SS.SSS
      i=index(w,' ')
      dd = nint(valread(w(1:i-1)))
      w=w(i+1:)                       !HH:NN:SS.SSS
      i=index(w,':')
      hh = nint(valread(w(1:i-1)))
      w=w(i+1:)                       !NN:SS.SSS
      i=index(w,':')
      nn = nint(valread(w(1:i-1)))
      w=w(i+1:)                       !SS.SSS
      ss = nint(valread(w))
C
C Count up all years from 1970 to yy-1.
C Account for leap years, remember that 2000 IS a leap year (but 1900 was not).
      t = 0
      if (yy.gt.1970) then
        do i=1970,yy-1
          if (mod(i,4).eq.0) then
            t = t + (366*24*3600)     ! leap year
          else
            t = t + (365*24*3600)     ! non-leap year 
          endif
        enddo
      endif
C
      if (mod(yy,4).eq.0) then
        doy = ndly(mm)   ! leap year.
      else
        doy = nd(mm)     ! non-leap year.
      endif
      t = t + ss + (nn*60) + (hh*3600) + ((doy+dd-1)*24*3600)
C
      return
      end

C----------------------------------------------------------------------
C Convert a time in seconds since 1/1/1970 00:00:00 to a string in the form:
C "YY MM DD HH:NN:SS" or "YY/MM/DD HH:NN:SS" .
C
      subroutine make_time_string(t,s,mode)
C
      implicit none
      integer*4 mode              ! 0= YY MM DD   1= YY/MM/DD  (input).
      integer*4 t                 ! Seconds since 1/1/1970 (input).
      character*(*) s             ! Time string (output).
      integer*4 tarray(9)
C Convert GMT. 
      call gmtime(t,tarray)
C Y2K.
      if (tarray(6).gt.99) tarray(6)=tarray(6)-100
C
      if (mode.eq.0) then
        write(s,'(3(i2.2,a),2(i2.2,a),i2.2)') tarray(6),' ',
     .     tarray(5)+1,' ',
     .     tarray(4),' ',tarray(3),':',tarray(2),':',tarray(1)
      else
        write(s,'(3(i2.2,a),2(i2.2,a),i2.2)') tarray(6),'/',
     .     tarray(5)+1,'/',
     .     tarray(4),' ',tarray(3),':',tarray(2),':',tarray(1)
      endif
      return
      end

C----------------------------------------------------------------------
C Write message to stdout in HTML format.
C mode=0 : do not exit,  mode=1 : exit with status 1.
C new=0  : do not preface with html header.
C new=1  : preface with html header.
C
      subroutine cgi_error(s,mode,new)
C
      implicit none
      character*(*) s
      integer*4 mode,lc,new
      if (new.eq.1) then
        call startplainhtml()
      endif
      print '(a,a)','ERROR: ',s(1:lc(s))
      if (mode.eq.1) call exit(1)
      return
      end

C----------------------------------------------------------------------
C Replace all occurrences of "s" in "w" with "c".
C
      subroutine cgi_string_replace(w,s,c)
C
      implicit none
      character*(*) w,s,c
      integer*4 i,ks,lc
      ks = lc(s)
      i = index(w,s)
      do while(i.gt.0)
        w = w(1:i-1)//c//w(i+ks:lc(w))
        i = index(w,s)
      enddo
      return
      end
 
C----------------------------------------------------------------------
C Replace special characters with their real meanings.
C (This is not complete!)
C
      subroutine cgi_line_fix(w)
C
      implicit none
      character*(*) w
      call cgi_string_replace(w,'+',' ')
      call cgi_string_replace(w,'%2F','/')
      call cgi_string_replace(w,'%2B','+')
      call cgi_string_replace(w,'%21','!')
      call cgi_string_replace(w,'%40','@')
      call cgi_string_replace(w,'%23','#')
      call cgi_string_replace(w,'%24','$')
      call cgi_string_replace(w,'%25','%')
      call cgi_string_replace(w,'%5E','^')
      call cgi_string_replace(w,'%25','%')
      call cgi_string_replace(w,'%28','(')
      call cgi_string_replace(w,'%29',')')
      call cgi_string_replace(w,'%3A',':')
      call cgi_string_replace(w,'%3B',';')
      call cgi_string_replace(w,'%22','"')
      call cgi_string_replace(w,'%27',char(39))   ! '
      call cgi_string_replace(w,'%2B','+')
      call cgi_string_replace(w,'%3D','=')
      call cgi_string_replace(w,'%7C','|')
      call cgi_string_replace(w,'%5C',char(92))   ! \
      call cgi_string_replace(w,'%7E','~')
      call cgi_string_replace(w,'%60','`')
      call cgi_string_replace(w,'%3C','<')
      call cgi_string_replace(w,'%2C',',')
      call cgi_string_replace(w,'%3E','>')
      call cgi_string_replace(w,'%3F','?')
      call cgi_string_replace(w,'%2F','/')
      call cgi_string_replace(w,'%7B','{')
      call cgi_string_replace(w,'%7D','}')
      call cgi_string_replace(w,'%5B','[')
      call cgi_string_replace(w,'%5D',']')
C
C NOT DOING THE AMPERSANDs! (see cgi_arguments)
C     call cgi_string_replace(w,'%26','&')
C
      return
      end


C--------------------------------------------------------------
C Read standard input for a Web/CGI interface and parse the parameters.
C
      subroutine cgi_arguments(arg,narg,max,line)
C
      implicit none
      integer*4 narg,i,max,ptr,k,lc
      character*(*) arg(*)
      character*(*) line
C
C Read standard input line from Web/CGI Form.
      read(5,'(a)',end=9) line
C Fix up line.
      call cgi_line_fix(line)
C Parse lines at "&".
      narg=0
      ptr=1
      k = lc(line)
3     continue
      i = index(line,'&')
      if (i.eq.0) i=k+1
      narg=narg+1
      arg(narg) = line(ptr:i-1)
C In case there was a real "&" in this string.
      call cgi_string_replace(arg(narg),'%26','&')
C Maximum number of arguments.
      if (narg.ge.max) return
C Rerun.
      if (i.lt.k) then
        line(i:i)=' '
        ptr=i+1
        goto 3
      endif
      return
C Error.
9     continue
      narg=0
      return
      end

C----------------------------------------------------------------------
C This finds a temporary filename which does not already exist as a file
C with the root name "root" and suffix of seconds since 1970.
C
      subroutine select_tempfile(root,tempfile)
C
      implicit none
      character*(*) root      ! (input)  (e.g. "/tmp/tempfile-").
      character*(*) tempfile  ! (output)  (e.g. "/tmp/tempfile-923912003").
      integer*4 i,time,ios,lc
C
      i = time()
1     continue
      write(tempfile,'(a,i10.10)') root(1:lc(root)),i
      open(1,file=tempfile,status='new',iostat=ios)
      if (ios.ne.0) then
        call sleep(1)
        i = i - 90000
        if (i.lt.0) then
          write(0,'(a)') 
     .       'select_tempfile: ERROR: Creating temporary file.'
          tempfile='bad.error.temp.file'
          return
        endif
        goto 1
      endif
      close(1)
      return
      end

C----------------------------------------------------------------------
C Good hexadecimal OBSID?
C
      logical function good_obsid(oi)
C
      implicit none
      character*(*) oi
      character*8 c8
      integer*4 i,j,k,lc
C Check length.
      if (lc(oi).ne.8) then
        good_obsid = .false.
        return
      endif
C Copy.
      c8 = oi
C Make upper case.
      call upper_case(c8)
C Check for eight hexadecimal characters.
      j=0
      do i=1,8
        k = ichar(c8(i:i))
        if ( ((k.ge.48).and.(k.le.57)).or.
     .       ((k.ge.65).and.(k.le.70)) ) then
          j=j+1
        endif
      enddo
C Do not allow zero OBSID.
      if ( (j.eq.8).and.(c8.ne.'00000000') ) then
        good_obsid = .true.
      else
        good_obsid = .false.
      endif
      return
      end

C----------------------------------------------------------------------
C Make a ".lock" file, first wait for ".lock" to disappear.
C Sleeps for 10 seconds between iterations.
C
      subroutine make_lock(file,iter,st,MaxAge,ok)
C
      implicit none
      character*(*) file   ! root filename to append ".lock" to. (input)
      integer*4 iter       ! Number of iterations (input)
      integer*4 st         ! Sleep in seconds between iterations (input)
      integer*4 MaxAge     ! Maximum age in seconds, if .lock file is older
C                          !   than this, try to delete the .lock, if MaxAge=0
C                          !   than no age limit. (input)
      logical ok           ! Success? (output)
C
      character*500 lockfile
      integer*4 i,j,lc,access,filetime,time
C
C Lock file.
      lockfile = file(1:lc(file))//'.lock'
C Is .lock too old?
      if ((MaxAge.gt.0).and.(access(lockfile,'r').eq.0)) then
        i = filetime(lockfile) 
        j = time()
        if (j-i.gt.MaxAge) then
          print *,'Lock file is too old, try to delete...'
          open(1,file=lockfile,status='unknown',iostat=i)
          close(1,status='delete',iostat=i)
        endif
      endif
C Wait for lock file to disappear.
      ok=.true.
      i=1
      do while((access(lockfile,'r').eq.0).and.(i.le.iter))
        print '(3a,i7,a)','Found ',lockfile(1:lc(lockfile)),', waiting',
     .                     st,' seconds.'
        call sleep(st)
        i=i+1
      enddo
      if (i.gt.iter) then
        print '(2a)','ABORT: Timeout on: ',lockfile(1:lc(lockfile))
        ok=.false.
        return
      endif
C Create lock file.
      open(1,file=lockfile,status='new',iostat=i)
      if (i.ne.0) then
        print '(2a)',
     .    'ABORT: Error opening lockfile: ',lockfile(1:lc(lockfile))
        ok=.false.
        return
      endif       
      write(1,'(2x)')
      close(1)
      return
      end

C----------------------------------------------------------------------
C Delete a ".lock" file.
C
      subroutine delete_lock(file,ok)
C
      implicit none
      character*(*) file
      logical ok
      character*500 lockfile
      integer*4 i,lc
C Lock file.
      lockfile = file(1:lc(file))//'.lock'
C Delete lock file.
      open(1,file=lockfile,status='unknown',iostat=i)
      if (i.ne.0) then
        print '(2a)','ABORT: Error deleting: ',lockfile(1:lc(lockfile))
        ok=.false.
        return
      endif
      close(1,status='delete')
      ok=.true.
      return
      end

C----------------------------------------------------------------------
C Delete a file.
C
      subroutine delete_file(file,ok)
C
      implicit none
      character*(*) file
      logical ok
      integer*4 i,lc
C
      open(1,file=file,status='unknown',iostat=i)
      if (i.ne.0) then
        print '(2a)','ERROR: delete_file: ',file(1:lc(file))
        ok=.false.
        return
      endif
      close(1,status='delete')
      ok=.true.
      return
      end
 
 

C--------------------------------------------------------------
C originated from NUMERICAL RECIPES...           tab jul 1988
C Modified for key sorting...                    tab nov 1997
C Modified for string sorting...                 tab jun 1999
C
C The key() array is a list of element positions in the arr() string array.
C The number of elements in the key() array may be smaller than or 
C equal to the number of elements in the arr() array.  This routine
C sorts the array key() using conditionals based on the values in arr().
C 
      subroutine qcksrt_string_subkey(narr,arr,nkey,key)
C
      implicit none
      integer*4 narr,nkey
      character*(*) arr(narr)
      integer*4 key(nkey)
      integer*4 m,nstack 
      real*4 fm,fa,fc,fmi
      parameter (m=7,nstack=100,fm=7875.,fa=211.,
     .           fc=1663.,fmi=1.2698413e-4)
      real*4 istack(nstack),fx
      integer*4 jstack,l,ir,j,i,iq,k
C
      jstack=0
      l=1
      ir=nkey
      fx=0.
10    if (ir-l.lt.m) then
        do 13 j=l+1,ir
          k=key(j)
          do 11 i=j-1,1,-1
            if (arr(key(i)).le.arr(k)) goto 12
            key(i+1)=key(i)
11        continue
          i=0
12        key(i+1)=k
13      continue
        if (jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        i=l
        j=ir
        fx=mod(fx*fa+fc,fm)
        iq=l+(ir-l+1)*(fx*fmi)
        k=key(iq)
        key(iq)=key(l)
20      continue
21        if (j.gt.0) then
            if (arr(k).lt.arr(key(j))) then
              j=j-1
              goto 21
            endif
          endif
          if (j.le.i) then
            key(i)=k
            goto 30
          endif
          key(i)=key(j)
          i=i+1
22        if (i.le.nkey) then
            if (arr(k).gt.arr(key(i))) then
              i=i+1
              goto 22
            endif
          endif
          if (j.le.i) then
            key(j)=k
            i=j
            goto 30
          endif
          key(j)=key(i)
          j=j-1
        goto 20
30      jstack=jstack+2
        if (jstack.gt.nstack) then
          print *, 'qcksrt: NSTACK must be made larger.'
          read(*,*)
        endif
        if (ir-i.ge.i-l) then
          istack(jstack)=ir
          istack(jstack-1)=i+1
          ir=i-1
        else
          istack(jstack)=i-1
          istack(jstack-1)=l
          l=i+1
        endif
      endif
      goto 10
      end


C----------------------------------------------------------------------
C Flip an image, top to bottom.  a() is real, b() is scratch.
C
      subroutine flipfits(a,b,sc,ec,sr,er)
C
      implicit none
      integer*4 sc,ec,sr,er,newrow,row,col
      real*4 a(sc:ec,sr:er)
      real*4 b(sc:ec,sr:er)
C Copy.
      do row=sr,er
        do col=sc,ec
          b(col,row) = a(col,row)
        enddo
      enddo
C Flip.
      do row=sr,er
        newrow = 1 + er - row
        do col=sc,ec
          a(col,newrow) = b(col,row)
        enddo
      enddo
      return
      end
      

