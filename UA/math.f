C math.f      Math and Statistics Routines          tab  10 March 1993 ++

C------------------------------------------------------------------
C Given values y with errors (ye), calculate the probability that the
C values arise from the same parent value.  In other words, test the
C fit of a flat line to the data points.
C
C   Inputs:  n         : number of points
C            y(1..n)   : data value array
C            ye(1..n)  : data value errors (normal distribution)
C            flag(1..n): flag which is 1 if the point should be used, 0 if
C                          it should be ignored. 
C
C  Outputs:  ym        : mean value derived from minimizing chi-square
C                          (i.e. weighted least-squares fit to a flat line)
C            yme       : error in the mean value
C            chisq     : chi-square statistic
C            nu        : degrees of freedom: (number of flag=1 points) - 1
C            prob      : probability that the data arose from the model
C                          (i.e. "goodness-of-fit" of data to a flat line)
C
      subroutine flat_chisq(n,y,ye,flag,ym,yme,chisq,nu,prob)

      implicit none

      integer*4 n,i,flag(*),num
      real*8 y(*),ye(*),ym,yme,chisq,prob,r,wt,nu

C Calculate mean value.
      ym = 0.d0
      wt = 0.d0
      do i=1,n
        if (flag(i).eq.1) then 
          r = 1.d0 / ( ye(i)*ye(i) ) 
          ym = ym + ( r * y(i) )
          wt = wt + r  
        endif 
      enddo
      if (wt.gt.0.) then
        ym =  ym / wt
        yme= dsqrt( 1.d0 / wt )
      else              ! If no flagged points, try using unflagged points.
        ym = 0.d0
        wt = 0.d0
        do i=1,n
          r = 1.d0 / ( ye(i)*ye(i) ) 
          ym = ym + ( r * y(i) )
          wt = wt + r  
        enddo
        if (wt.eq.0.) then
          ym = 0. 
          yme= 0. 
        else
          ym = ym / wt
          yme= dsqrt( 1.d0 / wt )
        endif 
      endif 

C Calculate chi-square.
      chisq = 0.d0
      num   = 0 
      do i=1,n
        if (flag(i).eq.1) then 
          r = ( y(i) - ym ) / ye(i) 
          chisq = chisq + ( r * r )
          num = num + 1 
        endif
      enddo 

C Degrees of freedom.
      if (num.lt.2) then
        print *,'ERROR- flat_chisq: too few usable points for test.'
        nu   = 0.d0 
        return
      endif
      nu = dfloat(num-1)

C Chi-Squared test.
      r = chisq/nu
      if (r.gt.100.) then
        prob = 0.d0
        print *,' NOTE- chi-square / deg. of freedom = ',nint(sngl(r))
      else 
        call chisq_test(nu,chisq,prob)
      endif 

      return
      end  


C------------------------------------------------------------
C Given the degrees of freedom (nu) and the chi-squared statistic (chisq),
C the probability of agreement is calculated.

      subroutine chisq_test(nu,chisq,prob)

      implicit none
      real*8 nu,chisq,prob,gammq
      prob= gammq(nu/2.d0,chisq/2.d0)
      return
      end 

C------------------------------------------------------------
C Returns the incomplete gamma function Q(a,x) = 1 - P(a,x) .

      real*8 function gammq(a,x)

      implicit none
      real*8 a,x,gamser,gammcf,gln
      if (x.lt.0..or.a.le.0.) then
        print *,'ERROR- gammq: x<0 or a<=0'
        gammq = 0.
        return
      endif  
      if(x.lt.a+1.)then           ! Use the series representation
        call gser(gamser,a,x,gln)
        gammq=1.-gamser           ! and take its complement
      else                        ! Used the continued fraction representation
        call gcf(gammcf,a,x,gln)
        gammq=gammcf
      endif
      return
      end

C------------------------------------------------------
C Returns the incomplete gamma function P(a,x) evaluated by its series
C representation as GAMSER.  Also returns ln(Gamma(a)) as GLN.

      subroutine gser(gamser,a,x,gln)

      implicit none
      real*8 gamser,a,x,gln
      real*8 eps,del,gammln,ap,sum
      integer*4 itmax,n
      parameter (itmax=100,eps=3.d-7)
      gln=gammln(a)
      if(x.le.0.)then
        if (x.lt.0.) print *,'ERROR- gser: x < 0.'
        gamser=0.d0
        return
      endif
      ap=a
      sum=1.d0/a
      del=sum
      do 11 n=1,itmax
        ap=ap+1.d0
        del=del*x/ap
        sum=sum+del
        if(abs(del).lt.abs(sum)*eps)go to 1
11    continue
      print *,'ERROR- gser: a too large, itmax too small'
1     gamser=sum*dexp(-x+a*dlog(x)-gln)
      return
      end

C------------------------------------------------------
C Returns the incomplete gamma function Q(a,x) evaluated by its continued
C fraction representation as GAMMCF.  Also returns Gamma(a) as GLN.
C
      subroutine gcf(gammcf,a,x,gln)
C
      implicit none 
      real*8 gammcf,a,x,gln
      real*8 a0,a1,b0,b1,fac,an,eps,gammln,gold,ana,anf,g
      integer*4 itmax,n   
      parameter (itmax=100,eps=3.d-7)
      g=0.
      gln=gammln(a)
      gold=0.d0
      a0=1.d0
      a1=x
      b0=0.d0
      b1=1.d0
      fac=1.d0
      do 11 n=1,itmax
        an=dfloat(n)
        ana=an-a
        a0=(a1+a0*ana)*fac
        b0=(b1+b0*ana)*fac
        anf=an*fac
        a1=x*a0+anf*a1
        b1=x*b0+anf*b1
        if(a1.ne.0.)then
          fac=1.d0/a1
          g=b1*fac
          if(abs((g-gold)/g).lt.eps)go to 1
          gold=g
        endif
11    continue
      print *,'ERROR- gcf: a too large, itmax too small'
1     gammcf=dexp(-x+a*dlog(x)-gln)*g
      return
      end

C--------------------------------------------------------------------
      real*8 function gammln(xx)
      implicit none 
      real*8 xx
      real*8 cof(6),stp,half,one,fpf,x,tmp,ser
      integer*4 j 
      data cof,stp/76.18009173d0,-86.50532033d0,24.01409822d0,
     *    -1.231739516d0,.120858003d-2,-.536382d-5,2.50662827465d0/
      data half,one,fpf/0.5d0,1.0d0,5.5d0/
      x=dble(xx)-one
      tmp=x+fpf
      tmp=(x+half)*dlog(tmp)-tmp
      ser=one
      do 11 j=1,6
        x=x+one
        ser=ser+cof(j)/x
11    continue
      gammln=tmp+dlog(stp*ser)
      return
      end


C----------------------------------------------------------------
C Simulates a normal distribution by returning values with a frequency
C consistent with a gaussian distribution with sigma = 1.0.
C If xnd = -1. then initialize, an x value is returned in xnd.
C
      subroutine NormDist(xnd)
C
      implicit none
      integer*4 i
      real*8 xnd,a(50000),r,gaussian_sig,x,sum,xinc
      real*4 real_rand 
C
      xinc = 0.
C
      if (xnd.lt.-0.9) then

        sum = 0.d0 
        xinc= 0.0001d0
        do i=1,50000
          x   = dfloat(i) * xinc
          sum = sum + 
     .         ( xinc * (gaussian_sig(x)+gaussian_sig(x-xinc))/2. )
          a(i)= sum
        enddo 
        r = dble(real_rand(1.,0.))
        xnd = 0.

      else

        r = dble(real_rand(0.,0.5))
        i=1 
        do while( (i.le.50000).and.(r.gt.a(i)) )
          i=i+1 
        enddo 
        xnd = ( dfloat(i-1) + (r-a(i-1))/(a(i)-a(i-1)) ) * xinc

      endif 

      return
      end  


    
C--------------------------------------------------------------------
C For this gaussian, sigma is x=1.0 and the integral -inf to inf is 1.0 .
      real*8 function gaussian_sig(x)
      implicit none
      real*8 x,c
      parameter(c=0.398942280d0)           !  c = 1 / sqrt(2*pi)
      gaussian_sig = c * dexp((x*x)/(-2.))
      return 
      end
        
C--------------------------------------------------------------------
C For this gaussian, the HWHM is x=1.0 and the integral -inf to inf is 1.0 .
      real*8 function gaussian_hwhm(x)
      implicit none
      real*8 x,c,ln2
      parameter(ln2=0.693147181d0,c=0.469718639d0)    !  c = sqrt(ln2/pi)
      gaussian_hwhm = c * dexp(-1.*x*x*ln2)
      return 
      end


C-------------------------------------------------------------------------
C Polynomial fitting by direct inversion of matrices.
C Input:
C   np     = number of points in data arrays.               
C   x,y,w  = x and y values and weight for each data point.
C   order  = polynomial order, can range from 0 to 19, however spurious
C            numerical roundoff errors and excessive computation times
C            can be expected for orders larger than 4th or 5th.
C Output: 
C   coeff  = order+1 polynomial coeffecients ( c(0), c(1), c(2), ... )
C
      subroutine poly_fit(np,x,y,w,order,coeff)
      implicit none
      integer*4 np,order 
      real*4 x(*),y(*),w(*)
      real*8 coeff(*)
      real*8 cf(20),vy(20),a(20,20),b(20,20)
      real*8 meanx,meany,meanw,xx,yy,ww 
      real*8 sum,detofa,detofb
      integer*4 i,j,k,n,ncoeff
C Number of coeffecients is the polynomial order plus one.
      ncoeff = order + 1 
C Zero arrays.
      do i=1,20
        vy(i) = 0.d0
        do j=1,20
          a(i,j) = 0.d0
          b(i,j) = 0.d0
        enddo
      enddo
C Find mean of x values.
      sum = 0.d0
      do i=1,np
        sum = sum + abs(dble(x(i)))
      enddo   
      meanx = sum / dfloat(np)
C Find mean of y values.
      sum = 0.d0 
      do i=1,np
        sum = sum + abs(dble(y(i)))
      enddo   
      meany = sum / dfloat(np)
C Find mean of w values.
      sum = 0.d0 
      do i=1,np
        sum = sum + abs(dble(w(i)))
      enddo   
      meanw = sum / dfloat(np)
C Load up arrays. 
      do i=1,np
        xx = dble(x(i)) / meanx 
        yy = dble(y(i)) / meany
        ww = dble(w(i)) / meanw
        do j=1,ncoeff
          vy(j) = vy(j) + ww*yy*(xx**dfloat(j-1))
          do k=1,ncoeff
            a(j,k) = a(j,k) + ww*(xx**dfloat(j-1))*(xx**dfloat(k-1))
          enddo 
        enddo 
      enddo 
C Determinant of denominator matrix.
      n = ncoeff
      call determgate(a,n,detofa)
      if (abs(detofa).lt.1.d-200) then
        print *,'ERROR- determinant of A too small.'
        return
      endif 
C Calculate Coeffecients.
      do i=1,ncoeff
C Substitute vy() into ith column of b.
        do j=1,ncoeff
          do k=1,ncoeff
            if (k.eq.i) then
              b(j,k) = vy(j)
            else
              b(j,k) = a(j,k)
            endif   
          enddo
        enddo
C Determinant of numerator matrix.
        n = ncoeff
        call determgate(b,n,detofb)

        cf(i) = meany * ( detofb / detofa ) / (meanx**dfloat(i-1))
      enddo
C Substitute coeffecients back into input array.
      do i=1,ncoeff
        coeff(i) = cf(i)
      enddo 
      return
      end       
C-------------------------------------------------------------------------
C Check residuals from polynomial fitting.
C Input:
C   np     = number of points in data arrays.               
C   x,y,w  = x and y values and weight for each data point.
C   order  = polynomial order, can range from 0 to 19, however spurious
C            numerical roundoff errors and excessive computation times
C            can be expected for orders larger than 4th or 5th.
C   coeff  = order+1 polynomial coeffecients ( c(0), c(1), c(2), ... )
C Output: 
C   high   = high residual
C   wrms   = weighted root-mean-square
C
      subroutine poly_fit_residuals(np,x,y,w,order,coeff,high,wrms)

      implicit none
      integer*4 np,order,i
      real*4 x(*),y(*),w(*)
      real*8 coeff(*),xv,polyval
      real*4 high,wrms,yy,diff,wsum

      high=0.
      wrms=0.
      wsum=0.
      do i=1,np
        xv = dble(x(i))
        yy = sngl(polyval(order+1,coeff,xv))
        diff = yy - y(i)
        if (abs(diff).gt.abs(high)) high=diff
        wrms = wrms + (w(i)*diff*diff)
        wsum = wsum + w(i)
      enddo
      wrms = sqrt(wrms/wsum)
      return
      end



C----------------------------------------------------------------------
C poly_fit_gj.f                                 tab  Aug96  Apr99
C
C Fit polynomial y = coef(1) + coef(2)*x + coef(3)*x*x + ... to data points
C using a method of normal equations and Gauss-Jordan elimination.
C Maximum order is 99 (100 coeffecients.)
C WARNING: coef() should be at least "np" big.
C
      subroutine poly_fit_gj(npt,x,y,wt,order,ucoef,xoff,forcexoff,ok)
C
      implicit none
C
      integer*4 npt         ! Number of points ( npt > order+1 ) (output).
      real*8 x(*)           ! x values (input).
      real*8 y(*)           ! y values (input).
      real*8 wt(*)          ! weight values ( usually 1 / sig^2 ) (input).
C                           !    Entries with weight=0 excluded from data set.
      integer*4 order       ! Polynomial order (0 to 99) (input).
      real*8 ucoef(*)       ! User coeffecients ( size >= order+1 ) (output).
      real*8 xoff           ! X value offset. Subtract this value from x values
C                           !    before using coeffecients. (input/output).
      integer*4 forcexoff   ! Force routine to use the given "xoff" (input)
C                           !    0 = no force, xoff determined by routine.
C                           !    1 = force, use user value of xoffset.
      logical ok            ! Success? (output)
C
C "mp" must be >= ndata and "np" must be >= number of coeffecients (order+1).
      integer*4 mp,np 
      parameter(mp=20000,np=100)
C
      real*8 chisq,a(np),covar(np,np),yoff,yscale,xscale,rr,coef(np)
      real*8 xx(mp),yy(mp),sig(mp),wsum
      integer*4 ncoef
      integer*4 ndata,i,ma,lista(np),mfit
C
      EXTERNAL polyfuncs
C

C Check order.
      if ((order.gt.99).or.(order.lt.0)) goto 903
C Zero coeffecients set lista().
      ncoef = order + 1
      do i=1,np
        coef(i)=0.d0
        lista(i)=i
      enddo
C Check size of data set.
      if (npt.gt.mp) goto 901
C Check order of polynomial requested.
      if (ncoef.gt.np) goto 902
C
C Determine xoff.
      if (forcexoff.eq.0) then
        xoff=0.
        wsum=0.
        do i=1,npt
          if (wt(i).gt.0.) then
            xoff = xoff + wt(i)*x(i)
            wsum = wsum + wt(i)
          endif
        enddo
        xoff = xoff / wsum
      endif
C
C Determine xscale.
      xscale=0.
      do i=1,npt
        if (wt(i).gt.0.) xscale = xscale + wt(i)*abs(x(i)-xoff)
      enddo
      xscale = xscale / wsum
      xscale = 1.d0
C Determine yoff.
      yoff=0.
      do i=1,npt
        if (wt(i).gt.0.) yoff = yoff + wt(i)*y(i)
      enddo
      yoff = yoff / wsum
      yoff = 0.d0
C Determine yscale.
      yscale=0.
      do i=1,npt
        if (wt(i).gt.0.) yscale = yscale + wt(i)*abs(y(i)-yoff)
      enddo
      yscale = yscale / wsum
      yscale = 1.d0
C Check values.
      if ((abs(xscale).lt.1.d-50).or.(abs(yscale).lt.1.d-50)) then
        print *,'Error-- x or y values are too small.'
        ok=.false.
        return
      endif
C Load data arrays.
      ndata=0
      do i=1,npt
        if (wt(i).gt.0.) then
          ndata=ndata+1
          xx(ndata) = (x(i)-xoff)/xscale
          yy(ndata) = (y(i)-yoff)/yscale
          sig(ndata)= 1.0 / sqrt(wt(i))
        endif
      enddo
C Number of parameters.
      ma  = ncoef
      mfit= ncoef
C Call least squares fitting routine.
C Check.
      if (mfit.lt.1) then
        print *,'ERROR: poly_fit_gj: Polynomial order is < 0: ',
     .          mfit-1
        ok=.false.
        return
      endif
      if (ndata.le.mfit) then
        print *,'ERROR: poly_fit_gj: Too few data points : ',ndata
        ok=.false.
        return
      endif
C Numerical Recipes Less-Square Fit.
      call NR_lfit(xx,yy,sig,ndata,a,ma,lista,mfit,covar,np,
     .             chisq,polyfuncs)
C Load coeffecients and adjust for y scaling and offset.
      do i=1,ncoef
        coef(i) = ( a(i)*yscale )
      enddo
      coef(1) = coef(1) + yoff
C Adjust for x scaling.
      rr = 1.d0
      do i=2,ncoef
        rr   = rr/xscale
        coef(i) = rr*coef(i) 
      enddo
C Transfer coeffecients to user array.
      do i=1,ncoef
        ucoef(i) = coef(i) 
      enddo
C Successful fit.
      ok=.true.
      return
901   continue
      print *,'ERROR: poly_fit_gj: number of points in data set :',ndata
      print *,'ERROR: poly_fit_gj: maximum number of data points:',mp
      ok=.false.
      return
902   continue
      print *,'ERROR: poly_fit_gj: polynomial order requested:',ncoef-1
      print *,'ERROR: poly_fit_gj: maximum order possible    :',np-1
      ok=.false.
      return
903   continue
      print *,'ERROR: poly_fit_gj: Order out of range: ',order
      print *,'ERROR: poly_fit_gj: Must be between 0 and 99.'
      ok=.false.
      return
      end
C----------------------------------------------------------------------
C Fitting routine for a polynomial of degree np-1, with np coeffecients.
      subroutine polyfuncs(x,p,np)
      implicit none
      integer*4 np
      real*8 x,p(np)
      integer*4 j
      p(1)=1.0
      do j=2,np
        p(j)=p(j-1)*x
      enddo
      return
      end
C-------------------------------------------------------------------------
C Check residuals from polynomial fitting.
C Input:   np       = number of points in data arrays.
C          x8,y8,w8 = x and y values and weight for each data point.
C          order    = polynomial order.
C          coef     = order+1 polynomial coeffecients ( c(0), c(1), c(2), ... )
C          xoff     = x offset.
C Output:  high     = high residual
C          wrms     = weighted root-mean-square
C          wppd     = weighted average point-to-point deviation
C
      subroutine poly_fit_gj_residuals(np,x8,y8,w8,order,coef,
     .                                 xoff,high,wrms,wppd)
C
      implicit none
      integer*4 np,order,i
      real*8 x8(*),y8(*),w8(*),coef(*),xoff
      real*8 high,wrms,wppd,xv,yv,diff,polyval,sum1,sum2,wsum1,wsum2
      high=0.d0
      wrms=0.d0
      wppd=0.d0
      sum1 =0.d0
      wsum1=0.d0
      sum2 =0.d0
      wsum2=0.d0
      do i=1,np
        xv = x8(i) - xoff
        yv = polyval(order+1,coef,xv)
        diff = yv - y8(i)
        if (abs(diff).gt.abs(high)) high=diff
        sum1 = sum1 + (w8(i)*diff*diff)
        wsum1= wsum1+ w8(i)
        if (i.gt.1) then
          xv = x8(i-1) - xoff
          yv = polyval(order+1,coef,xv)
          sum2 = sum2 + (w8(i)*w8(i-1)*abs(diff - (yv - y8(i-1))))
          wsum2= wsum2+ (w8(i)*w8(i-1))
        endif
      enddo
      if (wsum1.gt.0.) wrms = sqrt(sum1/wsum1)
      if (wsum2.gt.0.) wppd = sqrt(sum2/wsum2)
      return
      end
C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C Least-squares fitting routine.
C Taken from Numerical Recipes (c) 1986.
C
      subroutine NR_lfit(x,y,sig,ndata,a,ma,lista,mfit,
     .                   covar,ncvm,chisq,funcs)
C
      implicit none
      integer*4 ndata,ncvm,ma
      real*8 chisq
      integer*4 MMAX,kk,mfit,j,ihit,k,i
      parameter(MMAX=100)
      real*8 x(ndata),y(ndata),sig(ndata),a(ma)
      real*8 covar(ncvm,ncvm),beta(MMAX),afunc(MMAX)
      integer*4 lista(ma)
      real*8 ym,sig2i,wt,sum
C
      EXTERNAL funcs
C
      kk=mfit+1
      do j=1,ma
        ihit=0
        do k=1,mfit
          if (lista(k).eq.j) ihit=ihit+1
        enddo
        if (ihit.eq.0) then
          lista(kk)=j
          kk=kk+1
        else if (ihit.gt.1) then
          pause 'Improper set in LISTA'
        endif
      enddo
      if (kk.ne.(ma+1)) pause 'Improper set in LISTA'
      do j=1,mfit
        do k=1,mfit
          covar(j,k)=0.
        enddo
        beta(j)=0.
      enddo
      do i=1,ndata
        call funcs(x(i),afunc,ma)
        ym=y(i)
        if (mfit.lt.ma) then
          do j=mfit+1,ma
            ym=ym-a(lista(j))*afunc(lista(j))
          enddo
        endif
        sig2i=1./sig(i)**2
        do j=1,mfit
          wt=afunc(lista(j))*sig2i
          do k=1,j
            covar(j,k)=covar(j,k)+wt*afunc(lista(k))
          enddo
          beta(j)=beta(j)+ym*wt
        enddo
      enddo
      if (mfit.gt.1) then
        do j=2,mfit
          do k=1,j-1
            covar(k,j)=covar(j,k)
          enddo
        enddo
      endif
      call gaussj(covar,mfit,ncvm,beta,1,1)
      do j=1,mfit
        a(lista(j))=beta(j)
      enddo
      chisq=0.
      do i=1,ndata
        call funcs(x(i),afunc,ma)
        sum=0.
        do j=1,ma
          sum=sum+a(j)*afunc(j)
        enddo
        chisq=chisq+((y(i)-sum)/sig(i))**2
      enddo
      call covsrt(covar,ncvm,ma,lista,mfit)
      return
      end
C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      subroutine covsrt(covar,ncvm,ma,lista,mfit)
      implicit none
      integer*4 ncvm,ma,mfit
      integer*4 lista(mfit)
      real*8 covar(ncvm,ncvm),swap
      integer*4 i,j
      do j=1,ma-1
        do i=j+1,ma
          covar(i,j)=0.
        enddo
      enddo
      do i=1,mfit-1
        do j=i+1,mfit
          if (lista(j).gt.lista(i)) then
            covar(lista(j),lista(i))=covar(i,j)
          else
            covar(lista(i),lista(j))=covar(i,j)
          endif
        enddo
      enddo
      swap=covar(1,1)
      do j=1,ma
        covar(1,j)=covar(j,j)
        covar(j,j)=0.
      enddo
      covar(lista(1),lista(1))=swap
      do j=2,mfit
        covar(lista(j),lista(j))=covar(1,j)
      enddo
      do j=2,ma
        do i=1,j-1
          covar(i,j)=covar(j,i)
        enddo
      enddo
      return
      end

C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C
      subroutine gaussj(a,n,np,b,m,mp)
C
      implicit none
      integer*4 NMAX,n,np,m,mp
      parameter(NMAX=100)
      real*8 a(np,np),b(np,mp)
      integer*4 ipiv(NMAX),indxr(NMAX),indxc(NMAX)
      real*8 big,dum,pivinv
      integer*4 icol,irow,i,j,k,l,ll
C
      icol=0
      irow=0
      do j=1,n
        ipiv(j)=0
      enddo
      do i=1,n
        big=0.
        do j=1,n
          if (ipiv(j).ne.1) then
            do k=1,n
              if (ipiv(k).eq.0) then
                if (abs(a(j,k)).ge.big) then
                  big=abs(a(j,k))
                  irow=j
                  icol=k
                endif
              elseif (ipiv(k).gt.1) then
                print *,'ERROR: gaussj: Singular matrix- 001.'
                call exit(0)
              endif
            enddo
          endif
        enddo
        ipiv(icol)=ipiv(icol)+1
        if (irow.ne.icol) then
          do l=1,n
            dum=a(irow,l)
            a(irow,l)=a(icol,l)
            a(icol,l)=dum
          enddo
          do l=1,m
            dum=b(irow,l)
            b(irow,l)=b(icol,l)
            b(icol,l)=dum
          enddo
        endif
        indxr(i)=irow
        indxc(i)=icol
        if (a(icol,icol).eq.0.) then
          print *,'ERROR: gaussj: Singular matrix- 002.'
          call exit(0)
        endif
        pivinv=1./a(icol,icol)
        a(icol,icol)=1.
        do l=1,n
          a(icol,l)=a(icol,l)*pivinv
        enddo
        do l=1,m
          b(icol,l)=b(icol,l)*pivinv
        enddo
        do ll=1,n
          if (ll.ne.icol) then
            dum=a(ll,icol)
            a(ll,icol)=0.
            do l=1,n
              a(ll,l)=a(ll,l)-a(icol,l)*dum
            enddo
            do l=1,m
              b(ll,l)=b(ll,l)-b(icol,l)*dum
            enddo
          endif
        enddo
      enddo
      do l=n,1,-1
        if (indxr(l).ne.indxc(l)) then
          do k=1,n
            dum=a(k,indxr(l))
            a(k,indxr(l))=a(k,indxc(l))
            a(k,indxc(l))=dum
          enddo
        endif
      enddo
      return
      end




C-------------------------------------------------------------------------
C-------------------------------------------------------------------------
C  **** THE ROUTINES BELOW MAY BE IN VISTA SOURCE FILES ****
C-------------------------------------------------------------------------
C-------------------------------------------------------------------------
C Polynomial fitting using Vista's Generalized Least Squares fitting (GLLS).
C Input:
C   np     = number of points in data arrays.               
C   x,y,w  = x and y values and weight for each data point.
C   order  = polynomial order, can range from 0 to 19.
C Output: 
C   coef   = order+1 polynomial coeffecients ( c(0), c(1), c(2), ... )
C
      subroutine poly_fit_glls(np,x8,y8,w8,order,coef,ok)

      implicit none
      real*8 x8(*),y8(*),w8(*),coef(*)
      integer*4 np,order,i,ncoef,mo
      parameter(mo=19)
      real*8 aa(0:mo,0:mo),dcoef(0:mo),chisq,sum,wsum
      logical GLLS,ok
      EXTERNAL FPOLY
C Number of coeffecients is the polynomial order plus one.
      ncoef = order + 1
      do i=1,ncoef
        coef(i) = 0.d0
      enddo
C Fit flat line.
      if (order.lt.1) then
        sum =0.d0
        wsum=0.d0
        do i=1,np
          sum =sum + (y8(i)*w8(i))
          wsum=wsum+ (w8(i))
        enddo
        if (wsum.gt.1.d-30) then
          coef(1) = sum / wsum
          ok = .true.
        else
          coef(1) = 0.d0
          ok = .false.
        endif
      else
C Fit polynomial.
        ok = GLLS(x8,y8,w8,np,coef,ncoef,dcoef,aa,chisq,FPOLY,.true.)
      endif
      return
      end
C------------------------------------------------------------------------
C Return function value of polynomial given the number of coeffecients
C and the coeffecients.
      real*8 function polyval(nc,co,xv)
      implicit none
      integer*4 nc,i
      real*8 co(nc),xv,r
      r  = 0.d0
      do i=1,nc-1
        r = ( r + co(nc+1-i) ) * xv
      enddo
      polyval = r + co(1)
      return
      end
C------------------------------------------------------------------------
C Return function value of polynomial given the number of coeffecients
C and the coeffecients.
      real*8 function polyvaloff(nc,co,xv,xoff)
      implicit none
      integer*4 nc,i
      real*8 co(nc),xv,r,xoff
      r  = 0.d0
      do i=1,nc-1
        r = ( r + co(nc+1-i) ) * ( xv - xoff )
      enddo
      polyvaloff = r + co(1)
      return
      end
C-------------------------------------------------------------------------
C Check residuals from polynomial fitting.
C Input:
C   np       = number of points in data arrays.               
C   x8,y8,w8 = x and y values and weight for each data point.
C   order    = polynomial order, can range from 0 to 19, however spurious
C              numerical roundoff errors and excessive computation times
C              can be expected for orders larger than 4th or 5th.
C   coef     = order+1 polynomial coeffecients ( c(0), c(1), c(2), ... )
C Output: 
C   high     = (weighted) high residual
C   wrms     = weighted root-mean-square
C
      subroutine poly_fit_glls_residuals(np,x8,y8,w8,order,
     .                                   coef,high,wrms)
      implicit none
      integer*4 np,order,i
      real*8 x8(*),y8(*),w8(*),coef(*)
      real*8 high,wrms,xv,yv,diff,polyval,wsum
      high=0.d0
      wrms=0.d0
      wsum=0.d0
      do i=1,np
        if (w8(i).gt.0.) then
          xv = x8(i)
          yv = polyval(order+1,coef,xv)
          diff = yv - y8(i)
          if (abs(diff).gt.abs(high)) high=diff
          wrms = wrms + (w8(i)*diff*diff)
          wsum = wsum + w8(i)
        endif
      enddo
      wrms = sqrt(wrms/max(1.d-10,wsum))
      return
      end


C----------------------------------------------------------------------
C----------------------------------------------------------------------
C         ROUTINES BELOW ARE TAKEN DIRECTLY FROM VISTA CODE
C----------------------------------------------------------------------
C----------------------------------------------------------------------

      LOGICAL FUNCTION GLLS(X,Y,WT,NPTS,PAR,NPAR,DPAR,A,VAR,FUNCS,
     &                      INVERT)

C     Subroutine to perform a general linear least-squares fit.

C     Given a set of NPTS pairs X(I),Y(I) and their relative weights WT(I),
C     use direct chisq miminization to find the NPAR coeficients PARj of the
C     fitting function Y(x) = SUMoverj {PARj*PARFj(x)}. That is, the data is
C     modeled by a linear combination of any specified functions of x. These
C     "basic functions" PARFj(x) can be wildly nonlinear functions of x. Here,
C     "general linear least squares" only refers to the model's dependence
C     on its parameters PARj.
C     Here, we miminize chi-squared by solving the normal equations with a
C     LU-decomposition algorithm. A single iterated-improvement of the
C     solution is performed to restore from round-off errors and from most
C     close-to-singular cases.
C
C     INPUT:
C         X(NPTS)         X-values of the data pairs.
C         Y(NPTS)         Y-values of the data pairs.
C         WT(NPTS)        Weights (not sigmas) of the data pairs.
C         NPTS            Number of data pairs.
C         NPAR            Number of parameters to fit.
C         A(N)            REAL*8 working array with physical dimension at
C                         least NPAR**2. On output, A has the error matrix.
C         PAR(NPAR)       REAL*8 working array. On output the fitted parameters.
C         DPAR(NPAR)      Real*8 working array. On output returns the
C                         estimated error of the parameters.
C         FUNCS           A REAL*8 function FUNCS(x,parf,npar) returning in
C                         array PARF the NPAR functions PARFj, evaluated on x.
C                         EXAMPLE: To fit a NP-1 order polynomial,
C                                  create an external function FPOLY:
C                               REAL*8 FUNCTION FPOLY(X,P,NP)
C                               REAL*8 P(NP), X
C                               P(1) = 1.0D0
C                               DO 111 J=2,NP
C                                       P(J) = P(J-1)*X
C                           111 CONTINUE
C                               RETURN
C                               END
C         INVERT          When FALSE, the routine does the fit without going
C                         all the way to invert the matrix so the errors on
C                         the paramaters are not estimated (nor the variance).
C                         When TRUE, the full operation is performed.
C     OUTPUT:
C         PAR(NPAR)       Coefficients of the fit.
C         DPAR(NPAR)      Estimated error on the parameters.
C         A(NPAR,NPAR)    Error matrix. WARNING: the array is not scaled to
C                         its physical dimension in the calling program. Then,
C                         if A was dimensioned NxM in the main program, the
C                         (i,j) covariance wont be A(i,j) but A(i+(j-1)*N).
C         VAR             Unbiased Mean Variance of the fit:
C                         NPTS/(NPTS-NPAR)*SUM{WTi)*(Yi)-Y(Xi))**2}/SUM{WTi}.
C
C       J. Jesus Gonzalez       1/8/88

C     20 parameters: limit of the MATINV subroutines.
      PARAMETER (MAXDIM=20)
      IMPLICIT REAL*8 (A-H,O-Z)

      REAL*8 X(NPTS), Y(NPTS), WT(NPTS), VAR
      DIMENSION PAR(NPAR), DPAR(NPAR), PARF(MAXDIM)
      DIMENSION A(NPAR,NPAR), B(MAXDIM), C(MAXDIM,MAXDIM)
      INTEGER INDX(MAXDIM)
      LOGICAL ERROR, INVERT

C     Check to see if the data is in a valid form.
      GLLS = .TRUE.
      IF (NPAR .GT. MAXDIM) THEN
          PRINT *,'Sorry, can fit', MAXDIM,' parameters at the most.'
          GLLS = .FALSE.
          RETURN
      ELSE IF (NPTS .LT. NPAR) THEN
          PRINT *,'Not enough points to do fit (NPTS =',NPTS,').'
          GLLS = .FALSE.
          RETURN
      END IF

C     Initialize the symmetric matrix A, and vector B.
      DO 8714 J=1,NPAR
          B(J) = 0.0D0
          DO 8715 K=1,J
              A(K,J) = 0.0D0
8715      CONTINUE
8714  CONTINUE

C     Accumulate coefficients of normal equations in matrix A and vector B.
      DO 8716 I=1,NPTS
          Z = FUNCS(X(I),PARF,NPAR)
          DO 8717 J=1,NPAR
              TMP = PARF(J)*WT(I)
              DO 8718 K=1,J
                  A(K,J) = A(K,J) + PARF(K)*TMP
8718          CONTINUE
              B(J) = B(J) + Y(I)*TMP
8717      CONTINUE
8716  CONTINUE


C     Fill in the symmetric matrix A.
      DO 8719 J=1,NPAR,1
          DO 8720 K=J+1,NPAR,1
              A(K,J) = A(J,K)
8720      CONTINUE
8719  CONTINUE

C     Make copies of A and B in arrays C and PAR, we'll need the originals
C     later to improve the solution.
      DO 8721 J=1,NPAR
          DO 8722 K=1,NPAR
              C(K,J) = A(K,J)
8722      CONTINUE
          PAR(J) = B(J)
8721  CONTINUE

C     Solve the normal equations on C and PAR, by LU-decomposition.
      CALL LUDCMP(C,NPAR,MAXDIM,INDX,E,ERROR)
      IF (ERROR) THEN
          PRINT* ,' Fit failed.'
          GLLS = .FALSE.
          RETURN
      END IF
      CALL LUBKSB(C,NPAR,MAXDIM,INDX,PAR)

C     Improve the solution PAR to restore to full machine precision. Use this
C     very neat trick: Suppose vector x is the exact solution of A*x=b; We do
C     not know x but some wrong solution x+dx, where dx is the unknown error
C     vector; but A*(x+dx)=b+db, and subtracting the exact equation we get an
C     equation for the error dx in terms of the observed error db, A*dx=db.
C     Here, C corresponds to the LU-decomposition of A and  PAR to the solution
C     x+dx to be improved. We use DPAR as working space for db.
      DO 8723 J=1,NPAR
          DPAR(J) = -B(J)
          DO 8724 K=1,NPAR
              DPAR(J) = DPAR(J) + A(K,J)*PAR(K)
8724      CONTINUE
8723  CONTINUE

C     Solve for error term and subtract the error from the solution.
      CALL LUBKSB(C,NPAR,MAXDIM,INDX,DPAR)

      DO 8725 J=1,NPAR
          PAR(J) = PAR(J) - DPAR(J)
8725  CONTINUE

C     Evaluate a mean variance for the fit.
      IF (.NOT. INVERT) RETURN
      VAR = 0.0D0
      SUMWT = 0.0D0
      DO 8726 I=1,NPTS
          Z = FUNCS(X(I),PARF,NPAR)
          TMP = 0.0D0
          DO 8727 J=1,NPAR
              TMP = TMP + PAR(J)*PARF(J)
8727      CONTINUE
          VAR =  VAR + WT(I)*(Y(I)-TMP)**2
          SUMWT = SUMWT + WT(I)
8726  CONTINUE
      IF (SUMWT.NE.0) VAR = VAR/SUMWT
      IF (NPTS.GT.NPAR) VAR = (DBLE(NPTS)/DBLE(NPTS-NPAR))*VAR

C     Invert A to estimate the error of the parameters from the error matrix.
C     This is easy to do column by column using C, the LU-decomposition form
C     of A. Will use A as identity matrix for the column-by-column inversion.
      Z = SUMWT/DBLE(NPTS)
      DO 8728 J=1,NPAR
          DO 8729 K=1,NPAR
              A(K,J)=0.0D0
8729      CONTINUE
          A(J,J) = Z
          CALL LUBKSB(C,NPAR,MAXDIM,INDX,A(1,J))
          DPAR(J) = DSQRT(VAR*DABS(A(J,J)))
8728  CONTINUE
      RETURN
      END



C*********************************************************
C
      SUBROUTINE LUDCMP(A,N,NP,INDX,D,XERR)
C
      implicit none
C
C     Replaces the N x N matrix A, of physical dimension NP, by the LU
C     decomposition of a rowwise permutation of itself. INDX is an output
C     vector which records the row permutation affected by the partial
C     pivoting. D is output as +/- 1 depending on whether the number of row
C     interchanges was even or odd, respectively. This routine is used in
C     combination with LUBKSB to solve linear equations or to invert a matrix.
C
C     Largest dimension, and a small number.
C
      integer*4 N,NP
      integer*4 NMAX
      real*8 TINY,D
      logical XERR
C
      PARAMETER (NMAX=20,TINY=1.0D-28)
C
      real*8 A(NP,NP)
      real*8 VV(NMAX)
      real*8 AAMAX,SUM,DUM
      integer*4 INDX(N),I,J,K,IMAX
C

      XERR = .FALSE.
      IMAX = 0
      D=1.0D0
C     Loop over rows to get the implicit scaling.
      DO 8730 I=1,N
          AAMAX=0.0D0
          DO 8731 J=1,N
              IF (DABS(A(I,J)).GT.AAMAX) AAMAX=DABS(A(I,J))
8731      CONTINUE
          IF (AAMAX.EQ.0.0D0) THEN
C         No non-zero largest element.
          PRINT *,'poly_fit_glls: GLLS: LUDCMP: Singular matrix. 003'
              XERR = .TRUE.
              call exit(0)
          END IF
C         Save the scaling.
          VV(I)=1.0D0/AAMAX
8730  CONTINUE

      DO 8732 J=1,N
          IF (J.GT.1) THEN
              DO 8733 I=1,J-1
                  SUM=A(I,J)
                  IF (I.GT.1) THEN
                      DO 8734 K=1,I-1
                          SUM=SUM-A(I,K)*A(K,J)
8734                  CONTINUE
                      A(I,J)=SUM
                  END IF
8733          CONTINUE
          END IF
C         Search for the largest pivot.
          AAMAX=0.0D0
          DO 8735 I=J,N
              SUM=A(I,J)
              IF (J.GT.1)THEN
                  DO 8736 K=1,J-1
                      SUM=SUM-A(I,K)*A(K,J)
8736              CONTINUE
                  A(I,J)=SUM
              END IF
C             Dum if the pivot's figure of merit.
              DUM=VV(I)*DABS(SUM)
              IF (DUM.GE.AAMAX) THEN
                  IMAX=I
                  AAMAX=DUM
              END IF
8735      CONTINUE
          IF (J.NE.IMAX) THEN
C         Need to interchange rows.
              DO 8737 K=1,N
                  DUM=A(IMAX,K)
                  A(IMAX,K)=A(J,K)
                  A(J,K)=DUM
8737          CONTINUE
C             Reset the parity of D.
              D=-D
              VV(IMAX)=VV(J)
          END IF
          INDX(J)=IMAX
C         Divide by the pivot element, If zero, the matrix is singular,
C         substitute TINY for zero.
          IF (J.NE.N) THEN
              IF (A(J,J).EQ.0.0D0) A(J,J)=TINY
              DUM=1.0D0/A(J,J)
              DO 8738 I=J+1,N
                  A(I,J)=A(I,J)*DUM
8738          CONTINUE
          END IF
8732  CONTINUE
      IF (A(N,N).EQ.0.0D0) A(N,N)=TINY
      RETURN
      END

C     *********************************************************
      SUBROUTINE LUBKSB(A,N,NP,INDX,B)

C     Solves the set of N linear equations A*X=B. On input, A is not the
C     original matrix but its LU decomposition given by subroutine LUDCMP.
C     B is the right-hand side vector; on output it returns the solution
C     vector X.  A, N, NP, and INDX are not modified, so can be left in place
C     for successive calls with different right-hand sides B. This routine
C     takes into account that B will begin with many zeroe elements, so it
C     is efficient for use in matrix inversion.

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION A(NP,NP),INDX(N),B(N)

C     Initialize the index of the first non-zero element in B.
C     Unscramble the permutation while performing the forward substitution.
      II=0
      DO 8739 I=1,N
          LL=INDX(I)
          SUM=B(LL)
          B(LL)=B(I)
          IF (II.NE.0) THEN
              DO 8740 J=II,I-1
                  SUM=SUM-A(I,J)*B(J)
8740          CONTINUE
          ELSE IF (SUM.NE.0.) THEN
              II=I
          END IF
          B(I)=SUM
8739  CONTINUE
C     Now, perform the back-substitution.
      DO 8741 I=N,1,-1
          SUM=B(I)
          IF (I.LT.N) THEN
              DO 8742 J=I+1,N
                  SUM=SUM-A(I,J)*B(J)
8742          CONTINUE
          END IF
          B(I)=SUM/A(I,I)
8741  CONTINUE
      RETURN
      END


C*********************************************************
C
      REAL*8 FUNCTION FPOLY(X,P,NP)
C
      implicit none
C
C Function used in conjunction with GLLS to fit a polynomial.   C
C
      integer*4 NP,J
      REAL*8 P(NP),X
C
      P(1) = 1.0D0
      DO 8713 J=2,NP
        P(J) = P(J-1)*X
8713  CONTINUE
      FPOLY = 0
      RETURN
      END

C----------------------------------------------------------------------
C----------------------------------------------------------------------
C         ROUTINES ABOVE ARE TAKEN DIRECTLY FROM VISTA CODE
C----------------------------------------------------------------------
C----------------------------------------------------------------------

C----------------------------------------------------------------------
C Subroutine obtained from Ken Lanzetta/Art Wolfe via Vesa Junkkarinen (7Dec95)
C Optical depth for a voigt profile.  
C Input (real*8):
C        lambda = observed wavelength of interest in Angstroms.
C        z      = redshift of absorption line.
C        lambda0= rest wavelength of transition in Angstroms.
C        f      = oscillator strength.
C        gamma  = gamma value.
C        n      = column density in atoms per square centimeter.
C        b      = b value in kilometers per second.
C Output (real*8):
C        tau    = optical depth of voigt profile.
C
      subroutine tau_voigt(lambda,z,lambda0,f,gamma,n,b,tau)
      implicit none
      real*8 lambda,z,lambda0,f,gamma,n,b,tau
      real*8 lambdar,ve,ckms,p1,p2,h

      ckms = 299792.5d0
      lambdar = lambda / (1.d0 + z)
      ve = ((lambda0 - lambdar)/lambdar) * ckms/b
      ve = abs(ve)
      p1 = 1.497d-15 * f * lambda0 * n / b
      p2 = gamma * lambda0 / (1.2566d+14 * b)
      tau = p1 * h(p2,ve)
      return
      end
C----------------------------------------------------------------------
C Goes with tau_voigt...
      real*8 function h(a,x)
      implicit none
      real*8 a,x
      real*8 c(27)
      real*8 a1,a2,a3,b1,b2,s,t
      integer*4 nset,k,n
      complex z
      data nset/0/
      if (nset.ne.1) then
        k=-16
        do n=1,27
          k=k+1
          c(n)=(0.0897935610625833d0)*exp(-1.d0 * dfloat(k*k) / 9.d0)
        enddo
        nset=1
      endif
      if (a.eq.0.) then
        if (x.le.8.) then
          h=exp(-x*x)
        else
          h=0.d0
        endif
        return
      else
        a1=3.d0*a
        a2=a*a
        if (a.lt.0.1) then
          z=ccos(cmplx(9.42477796076938d0*x,9.42477796076938d0*a))
          a3=a2-x*x
          if (a3.gt.-70.) then
            h=exp(a3)*cos(2.d0*a*x)
          else
            h=0.d0
          endif
        else
          z=cexp(cmplx(-9.42477796076938d0*A,9.42477796076938d0*X))
          h=0.d0
        endif
        b1=(1.d0-real(z))*a*1.5d0
        b2=-aimag(z)
        s=-8.d0 - 1.5d0*x
        t=s*s+2.25d0*a2
        do n=1,27
          t=t+s+0.25
          s=s+0.5d0
          b1=a1-b1
          b2=-b2
          if (t.gt.2.5e-12) then
            h=h + 1.772453851d0*c(n)*(b1+b2*S)/t
          else
            h=h - 1.772453851d0*c(n)*a/3.d0
          endif
        enddo
        return
      endif
      end

C----------------------------------------------------------------------
C Estimate log10 of column density (in cm^-2) given an observed equivalent 
C width, observed wavelength, rest wavelength (all in Angstroms), oscillator 
C strength, and b value (in km/s).
C NOTE: See CogColumn2() to get equivalent width from column density.
C
      subroutine CogColumn(basefile,ew,obswave,restwave,oscill,b_value,
     .                     column_density)
      implicit none
      real*8 ew,obswave,restwave,oscill,b_value,column_density
      integer*4 m,flag
      parameter(m=2000)
      real*8 b(6),lognfl(m),logwll(m,6),r0,r1,r2,r3,r4,r5,r6
      real*8 ulogwll,ulognfl,frac
      integer*4 n,nb,nw1,nw2,i,neari8,lc
      character*80 line
      character*(*) basefile
C
C     parameter(basefile='/x1/tom/cog/lyman.out')
C
      common /CogColumnBLK/ flag
C
C Read in data from file. Only need to read once.
      IF (flag.ne.123456) THEN
      open(1,file=basefile,status='old',iostat=i)
      if (i.ne.0) goto 901
      read(1,'(a)') line
      read(1,'(7x,6(f11.4))') b(1),b(2),b(3),b(4),b(5),b(6)
      n=0
1     continue
      read(1,*,end=5) r0,r1,r2,r3,r4,r5,r6
      n=n+1
      lognfl(n)  = r0
      logwll(n,1)= r1
      logwll(n,2)= r2
      logwll(n,3)= r3
      logwll(n,4)= r4
      logwll(n,5)= r5
      logwll(n,6)= r6
      goto 1
5     continue
      close(1)
      ENDIF
C Select best b value.
      nb = neari8(b_value,6,b)
      if (abs(b_value-b(nb)).gt.0.001) print '(a,f8.3)',' Using: b = ',b(nb)
C Set user logwll value.
      ulogwll = log10( ew / obswave )
C Select two logwll values which straddle the input value.
      i = neari8(ulogwll,n,logwll(1,nb))
      if ((i.eq.1).or.(i.eq.n)) then
        print *,'Error-- equivalent width out of range.'
        column_density = 0.d0
        return
      endif
      if (logwll(i,nb).gt.ulogwll) then
        nw1 = i-1
        nw2 = i
      else
        nw1 = i
        nw2 = i+1
      endif
C Estimate best lognfl value.
      frac = ( ulogwll - logwll(nw1,nb) ) /
     .            ( logwll(nw2,nb) - logwll(nw1,nb) )
      ulognfl =  lognfl(nw1) + ( frac*( lognfl(nw2) - lognfl(nw1) ) )
      column_density = ulognfl - log10( oscill * restwave * 1.d-8 )
C Set flag, so no read on second call.
      flag = 123456
      return
901   continue
      line = basefile
      print '(2a)',' Error opening database file: ',line(1:lc(line))
      return
      end
C...................................................................
C1215.670   6.2650E+08   4.162E-01  H I     
C b=        5.0000    10.0000    15.0000    20.0000    30.0000    40.0000
C 6.000    -6.0578    -6.0556    -6.0548    -6.0545    -6.0541    -6.0539
C 6.100    -5.9590    -5.9562    -5.9552    -5.9548    -5.9543    -5.9540
C 6.200    -5.8605    -5.8569    -5.8557    -5.8551    -5.8545    -5.8542
C...................................................................

C----------------------------------------------------------------------
C Estimate an observed equivalent width (ew,in Angstroms) given a log10 of the
C column density (in cm^-2), observed wavelength, rest wavelength 
C (all in Angstroms), oscillator strength, and b value (in km/s).
C NOTE: See CogColumn() to get column density from equivalent width.
C
      subroutine CogColumn2(basefile,ew,obswave,restwave,oscill,b_value,
     .                     column_density)
      implicit none
      real*8 ew,obswave,restwave,oscill,b_value,column_density
      integer*4 m,flag
      parameter(m=2000)
      real*8 b(6),lognfl(m),logwll(m,6),r0,r1,r2,r3,r4,r5,r6
      real*8 ulogwll,ulognfl,frac
      integer*4 n,nb,nw1,nw2,i,neari8,lc
      character*80 line
      character*(*) basefile
C
C     parameter(basefile='/x1/tom/cog/lyman.out')
C
      common /CogColumn2BLK/ flag
C
C Read in data from file. Only need to read once.
      IF (flag.ne.123456) THEN
      open(1,file=basefile,status='old',iostat=i)
      if (i.ne.0) goto 901
      read(1,'(a)') line
      read(1,'(7x,6(f11.4))') b(1),b(2),b(3),b(4),b(5),b(6)
      n=0
1     continue
      read(1,*,end=5) r0,r1,r2,r3,r4,r5,r6
      n=n+1
      lognfl(n)  = r0
      logwll(n,1)= r1
      logwll(n,2)= r2
      logwll(n,3)= r3
      logwll(n,4)= r4
      logwll(n,5)= r5
      logwll(n,6)= r6
      goto 1
5     continue
      close(1)
      ENDIF
C Select best b value.
      nb = neari8(b_value,6,b)
      if (abs(b_value-b(nb)).gt.0.001) print '(a,f8.3)',' Using: b = ',b(nb)
C Set user lognfl value.
      ulognfl = column_density + log10( oscill * restwave * 1.d-8 )
C Select two lognfl values which straddle the input value.
      i = neari8(ulognfl,n,lognfl)
      if ((i.eq.1).or.(i.eq.n)) then
        print *,'Error-- log10 column density out of range.'
        ew = 0.d0
        return
      endif
      if (lognfl(i).gt.ulognfl) then
        nw1 = i-1
        nw2 = i
      else
        nw1 = i
        nw2 = i+1
      endif
C Estimate best logwll value.
      frac = ( ulognfl - lognfl(nw1) ) /
     .               ( lognfl(nw2) - lognfl(nw1) )
      ulogwll =  logwll(nw1,nb) + 
     .            ( frac*( logwll(nw2,nb) - logwll(nw1,nb) ) )
      ew = ( 10.d0 ** ulogwll ) * obswave
C Set flag, so no read on second call.
      flag = 123456
      return
901   continue
      line = basefile
      print '(2a)',' Error opening database file: ',line(1:lc(line))
      return
      end
C...................................................................
C1215.670   6.2650E+08   4.162E-01  H I     
C b=        5.0000    10.0000    15.0000    20.0000    30.0000    40.0000
C 6.000    -6.0578    -6.0556    -6.0548    -6.0545    -6.0541    -6.0539
C 6.100    -5.9590    -5.9562    -5.9552    -5.9548    -5.9543    -5.9540
C 6.200    -5.8605    -5.8569    -5.8557    -5.8551    -5.8545    -5.8542
C...................................................................


C----------------------------------------------------------------------
C Calculate column density for a bin assuming the line is completely
C resolved and covers the background light source.
C
C Inputs: wave1 = wavelength at blue edge of bin.
C         wave2 = wavelength at red edge of bin.
C         rest  = rest wavelength of line in Angstroms.
C         uri   = (user) residual intensity of bin.
C         urie  = (user) error in residual intensity of bin.
C         osc   = oscillator strength.
C         zp    = zero point (normally 0 unless partial coverage).
C
C Outputs: cd   = column density in cm^-2.
C          cde  = error in column density in cm^-2.
C
C Physical constants:
C  m_e = electron mass = 9.10939 x 10^-28 grams
C  c   = speed of light in vacuum = 2.99792458 x 10^10 cm/s (exactly)
C  pi  = 3.141592654
C  e   = electron charge = 1.602177 x 10^-19 Coulombs
C  e   = 4.80325 x 10^-10 statCoulombs or ESUs
C  e^2 = 2.30708 x 10^-19 (ESUs^2) or ergs * cm
C  mc^2= 8.18711 x 10^-7 ergs
C
C Column Density = [ mc^2 tau / ( f lambda0 pi e^2 ) ]  (times)
C     ln( wavelength at red edge of bin / wavelength at blue edge of bin )
C
C See Junkkarinen et al., ApJ 265,51 (1983).
C
      subroutine ColumnDensity(wave1,wave2,rest_Ang,uri,urie,osc,
     .                         zp,cd,cde)
C
      implicit none
C
      real*8 wave1,wave2,rest_Ang,osc,zp
      real*4 uri,urie,cd,cde,ri,rie
C mc^2 and e^2 constants.
      real*8 mc2,e2,pi
      parameter( mc2=8.18711d-7, e2=2.30708d-19, pi=3.141592654d0 )
      real*8 tau,const,natlog,rest
      real*4 cddev,cdtop,cdbot
      integer*4 ncddev
C Modify residual intensity if given zero point.
      if (abs(zp).gt.1.e-20) then
        if ((zp.lt.0.0001).or.(zp.gt.0.9999)) goto 804
        ri = ( uri - zp ) / ( 1.0 - zp )
        rie= ( urie     ) / ( 1.0 - zp )
      else
        ri = uri
        rie= urie
      endif
C Residual intensity must be less than 1.0 and greater than 0.0.
      if (ri.gt.0.9999) goto 801
      if (ri.lt.0.0001) goto 802
C Convert rest wavelength from Angstroms to centimeters.
      rest = rest_Ang * 1.d-08
C Constant.
      const= ((mc2)/(dble(osc)*rest*pi*e2)) * natlog(wave2/wave1)
C Find column.
      tau  = -1.d0 * natlog(dble(ri))
      cd   = sngl( const * tau )
C Find 1 sigma deviation.
      cddev =0.
      ncddev=0
C CD 1 sigma above.
      if ((ri+rie).lt.0.9999) then
        tau  = -1.d0 * natlog(dble(ri+rie))
        cdtop= sngl( const * tau )
        cddev= cddev + abs(cdtop-cd)
        ncddev= ncddev + 1
      endif
C CD 1 sigma below.
      if ((ri-rie).gt.0.0001) then
        tau  = -1.d0 * natlog(dble(ri-rie))
        cdbot= sngl( const * tau ) 
        cddev= cddev + abs(cdbot-cd)
        ncddev= ncddev + 1
      endif
      if (ncddev.lt.1) goto 803
      cde = cddev / float(ncddev)
      return
801   continue
      print *,'Residual intensity too weak, assuming zero column.'
      cd = 0.
      cde=-1.
      return
802   continue
      print *,'ERROR ColumnDensity- Residual intensity too ',
     .        'strong-- undefined.'
      cd =-1.
      cde=-1.
      return
803   continue
      print *,'ERROR ColumnDensity- Cannot calculate error for ',
     .        'column density.'
      cd =-1.
      cde=-1.
804   continue
      print *,'ERROR ColumnDensity- Zero point too small or too large.'
      cd =-1.
      cde=-1.
      return
      end
C----------------------------------------------------------------------
C This goes with ColumnDensity.
      real*8 function natlog(xx)
      implicit none
      real*8 xx
      natlog = dlog(xx)
      return
      end

