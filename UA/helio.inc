
C::::::::::::::::::::::  helio.inc  :::::::::::::::::::::::::::::::::

C Input quantities:
      real*8    ra,         ! Right Ascension in hours
     .          dec,        ! Declination in degrees
     .          latitude,   ! Latitude in degrees
     .          longitude,  ! Longitude west of Greenwich meridan in degrees
     .          altitude,   ! Altitude above sea level in meters
     .          ut          ! Universal Time of date in hours (mean solar day)
      integer*4 year,       ! Year (UT)         
     .          month,      ! Month (UT)
     .          day         ! Day (UT)

C Output quantities:
      real*8    epoch,      ! Julian epoch in years
     .          lmst,       ! Local mean sideral time in hours
     .          ha,         ! hour angle in hours
     .          airmass,    ! Air Mass (approximate, neglects altitude)
     .          pa,         ! Position Angle in degrees (blue:0=north,90=east)
     .          azimuth,    ! Azimuth in degress (0=north,90=east)
     .          tzd,        ! True Zenith Distance in degrees
     .          azd,        ! Apparent Zenith Distance (corrected for
                            !   atmospheric refraction) in degrees
     .          jd,         ! Julian day
     .          hjd,        ! Heliocentric Julian day ( hjd - jd = light travel
                            !   difference to Sun (for light from object) )
     .          vhelio,     ! Velocity correction (in km/sec) to heliocentric
                            !   rest frame (negative when observer is receding
                            !   from object relative to the Sun).
     .          vannual,    ! Velocity correction for orbital motion of the
                            !   Earth-Moon Barycenter about the Sun.
     .          vlunar,     ! Velocity correction for motion of the center of
                            !   the Earth about the Earth-Moon Barycenter.
     .          vdiurnal    ! Velocity correction for rotation of the observer
                            !   about center of the Earth.
      
      common /helioblk/ ra, dec, latitude, longitude, altitude, ut,
     .                  epoch, lmst, ha, airmass, pa, azimuth, tzd, azd,
     .                  jd, hjd, vhelio, vannual, vlunar, vdiurnal,
     .                  year, month, day  

C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

