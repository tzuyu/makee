/* cmath-util.c                               tab  Nov99,Mar00,Jun00  */

#include "ctab.h"

#include "cphomax.h"

#include "cmisc.h"



#define AmoebaMax 90


/* --------------------------------------------------------------------
 For this gaussian, sigma is x=1.0 and the integral -inf to inf is 1.0 .
 Return gaussian value given "x".
*/
double gaussian_sig( double x )
{
const double c = 0.398942280;   /*  c = 1 / sqrt(2*pi) */
return( c * exp((x*x)/(-2.)) );
}

/* --------------------------------------------------------------------
  For this gaussian, the HWHM is x=1.0 and the integral -inf to inf is 1.0 .
  Return gaussian value given "x".
*/
double gaussian_hwhm( double x )
{
/**/
const double ln2 = 0.693147181;      /* natural logarithm of 2. */
const double c = 0.469718639;        /*  c = sqrt(ln2/pi)       */
/**/
return( c * exp(-1.*x*x*ln2) );
}


/* -----------------------------------------------------------------------
  Given a FWHM and an "x", return the Gaussian value.
*/
double gaussval( double x, double FWHM )
{
double xx,rr;
xx = x / ( FWHM / 2. );
rr = gaussian_hwhm( xx );
return(rr);
}


/* -----------------------------------------------------------------------
  Convert x,y,z unit sphere to RA,DEC.
  Input: x,y,z (coordinates on unit sphere).
  Output: ra,dec (RA and DEC in decimal degrees).
*/
void xyz2radec(double x, double y, double z, double *ra, double *dec)
{
*dec = radtodeg( asin(z) );
*ra  = radtodeg( atan2(y,x) );
if ( *ra < 0. ) *ra += 360.;
return;
}


/* -----------------------------------------------------------------------
  Convert RA,DEC to x,y,z unit sphere.
  Input: ra,dec (RA and DEC in decimal degrees).
  Output: x,y,z (coordinates on unit sphere).
*/
void radec2xyz(double ra, double dec, double *x, double *y, double *z)
{
*x = cos(degtorad(dec)) * cos(degtorad(ra));
*y = cos(degtorad(dec)) * sin(degtorad(ra));
*z = sin(degtorad(dec));
return;
}


/* ---------------------------------------------------------------------
  Convert ra,dec in degrees to primed coordinate frame rap and decp in
  degrees, using rotation matrix.  Set matrix using SetRotationMatrix().
  Input:  ra,dec    (actual coordinates in decimal degrees).
          RM        (3x3 rotation matrix)
  Output: yp,zp     (relative RA and DEC in primed frame in radians)
*/
void radec2ypzp(double ra, double dec, double RM[3][3],
                 double *yp, double *zp )
{
/**/
double x,y,z;
/**/
/* Convert to unit sphere. */
radec2xyz(ra,dec,&x,&y,&z);
/* Rotate.  Gnomonic projection. */
*yp= RM[0][1]*x + RM[1][1]*y + RM[2][1]*z;
*zp= RM[0][2]*x + RM[1][2]*y + RM[2][2]*z;
return;
}


/* ---------------------------------------------------------------------
  Convert ra,dec in degrees to primed coordinate frame rap and decp in
  degrees, using rotation matrix.  Set matrix using SetRotationMatrix().
  Input:  ra,dec    (actual coordinates in decimal degrees).
          RM        (3x3 rotation matrix)
  Output: rap,decp  (relative RA and DEC in primed frame in degrees)
*/
void radec2prime(double ra, double dec, double RM[3][3],
                 double *rap, double *decp )
{
/**/
double x,y,z,yp,zp;
/**/
/* Convert to unit sphere. */
radec2xyz(ra,dec,&x,&y,&z);
/* Rotate. */
yp= RM[0][1]*x + RM[1][1]*y + RM[2][1]*z;
zp= RM[0][2]*x + RM[1][2]*y + RM[2][2]*z;
/* Gnomonic projection. */
*rap = radtodeg(yp);
*decp= radtodeg(zp);
return;
}


/* ---------------------------------------------------------------------
  Convert x,y,z on unit sphere to primed coordinate frame using
  rotation matrix.  Set matrix using SetRotationMatrix().
  The prime system is set up such that the x-prime axis points in the direction
  of the RA,DEC origin, and the z-prime axis lies in the plane of the x-prime
  axis and the original z axis.
  On the new prime image plane, up-down (row#) will be zp, and left-right
  (col#) will be yp, where yp and zp are in radians.
  Input:  x,y,z     (on unit sphere)
          RM        (3x3 rotation matrix)
  Output: xp,yp,zp  (in primed frame)
*/
void xyz2prime(double x, double y, double z, double RM[3][3],
               double *xp, double *yp, double *zp)
{
*xp= RM[0][0]*x + RM[1][0]*y + RM[2][0]*z;
*yp= RM[0][1]*x + RM[1][1]*y + RM[2][1]*z;
*zp= RM[0][2]*x + RM[1][2]*y + RM[2][2]*z;
return;
}


/* ---------------------------------------------------------------------
  Convert x,y,z on unit sphere to primed coordinate frame using
  rotation matrix.  Set matrix using SetRotationMatrix().

  The prime system is set up such that the x-prime axis points in the direction
  of the RA,DEC origin, and the z-prime axis lies in the plane of the x-prime
  axis and the original z axis.

  Input:  xp,yp,zp  (in prime system)
          RMinv     (3x3 inverse rotation matrix)
  Output: x,y,z     (on unit sphere)
*/
void prime2xyz(double xp, double yp, double zp, double RMinv[3][3],
               double *x, double *y, double *z)
{
*x= RMinv[0][0]*xp + RMinv[1][0]*yp + RMinv[2][0]*zp;
*y= RMinv[0][1]*xp + RMinv[1][1]*yp + RMinv[2][1]*zp;
*z= RMinv[0][2]*xp + RMinv[1][2]*yp + RMinv[2][2]*zp;
return;
}


/* ---------------------------------------------------------------------
  Returns multiplication of two 3x3 matices.
  Input:  aa,bb     (3x3 matrices).
  Output: cc        (3x3 matrix multiplication result).
*/
void MatMul3D(double aa[3][3], double bb[3][3], double cc[3][3] )
{
/**/
int jj,col,row;
double rr;
/**/
for (col=0; col<3; ++col) {
  for (row=0; row<3; ++row) {
    cc[col][row] = 0.;
  }
}
for (col=0; col<3; ++col) {
  for (row=0; row<3; ++row) {
    for (jj=0; jj<3; ++jj) { 
      rr = aa[jj][col] * bb[row][jj];
      cc[col][row] = cc[col][row] + rr;
    }
  }
}
return;
}


/* ---------------------------------------------------------------------
  Returns the determinant of a 2x2 matrix
  Input:  aa     (2x2 matrix).
*/
double MatDet2D(double aa[2][2])
{
return( ( aa[0][0] * aa[1][1] ) - ( aa[1][0] * aa[0][1] ) );
}


/* ---------------------------------------------------------------------
  Returns the sub-determinant within a 3x3 matrix.
  The determinant of the 2x2 matrix obtained by deleting the
  "col" column and the "row" row.
  Input:  col,row  : column and row to be deleted.
          aa       : a 3x3 matrix.
*/
double MatSubDet3D(int col, int row, double aa[3][3])
{
/**/
double bb[2][2];
int xx,yy,ii,jj;
/**/
/* Initialize. */
/* Form sub matrix */
xx=-1;
for (ii=0; ii<3; ++ii) {
if (ii != col) {
  ++xx;
  yy=-1;
  for (jj=0; jj<3; ++jj) {
  if (jj != row) {
    ++yy;
    bb[xx][yy] = aa[ii][jj];
  }
  }
}
}
return( MatDet2D(bb) );
}


/* ---------------------------------------------------------------------
  Returns the determinant of a 3x3 matrix
  Input:  aa     (3x3 matrix).
*/
double MatDet3D(double aa[3][3])
{
return(
     aa[0][0] * MatSubDet3D(0,0,aa) 
   - aa[1][0] * MatSubDet3D(1,0,aa)
   + aa[2][0] * MatSubDet3D(2,0,aa)
      );
}


/* ---------------------------------------------------------------------
  Returns the co-factor within a 3x3 matrix for column "col" and row "row".
  Input: col,row    (column and row).
         aa         (3x3 matrix).
*/
double MatCof3D(int col, int row, double aa[3][3])
{
double prefix;
/* Prefix. */
if ( (col+row)%2 == 0 ) { prefix = 1.; } else { prefix = -1.; }
/* Sub-determinant. */
return( prefix * MatSubDet3D(col,row,aa) );
}



/* ---------------------------------------------------------------------
  Invert a 3x3 matrix.
  Input:  mm     (3x3 matrix).
  Output: mminv  (inverse 3x3 matrix).
*/
void MatInv3D(double mm[3][3], double mminv[3][3] )
{
/**/
double aa[3][3],temp;
int col,row;
/**/
/* First create cofactor matrix. */
for (col=0; col<3; ++col) {
  for (row=0; row<3; ++row) {
     aa[col][row] = MatCof3D(col,row,mm);
  }
}
/* Transpose cofactor matrix (classical adjoint). */
for (col=0; col<3; ++col) {
  for (row=0; row<3; ++row) {
    if (col > row) {
      temp         = aa[col][row]; 
      aa[col][row] = aa[row][col];
      aa[row][col] = temp;
    }
  }
}
/* Determinant of 3x3 matrix. */
temp = MatDet3D(mm);
if (ABS(temp) < 1.e-50) {
  fprintf(stderr,"ERROR: MatInv3D: matrix non-invertible.\n");
  exit(0);
}
/* Inverse matrix: classical adjoint divided by determinant. */
for (col=0; col<3; ++col) {
  for (row=0; row<3; ++row) {
    mminv[col][row] = aa[col][row]  / temp;
  }
}
return;
}


/* ---------------------------------------------------------------------
  Invert a 2x2 matrix.
  Input:  mm     (2x2 matrix).
  Output: mminv  (inverse 2x2 matrix).
*/
void MatInv2D(double mm[2][2], double mminv[2][2] )
{
/**/
double aa[2][2],temp;
/**/
/* First create classical adjoint (cofactor and transpose) matrix. */
aa[0][0] =    mm[1][1];
aa[1][1] =    mm[0][0];
aa[1][0] =-1.*mm[1][0];
aa[0][1] =-1.*mm[0][1];
/* Determinant of 2x2 matrix. */
temp = (mm[0][0]*mm[1][1]) - (mm[1][0]*mm[0][1]);
if (ABS(temp) < 1.e-50) {
  fprintf(stderr,"ERROR: MatInv2D: matrix non-invertible.\n");
  exit(0);
}
/* Inverse matrix: classical adjoint divided by determinant. */
mminv[0][0] = aa[0][0] / temp;
mminv[1][0] = aa[1][0] / temp;
mminv[0][1] = aa[0][1] / temp;
mminv[1][1] = aa[1][1] / temp;
return;
}


/* ---------------------------------------------------------------------
  Set rotation matrices (RM[0].*, see phomax.h) to convert x,y,z on unit sphere
  to primed coordinate frame and vice versa.
  The prime system is set up such that the x-prime axis points in the direction
  of the RA,DEC origin, and the z-prime axis lies in the plane of the x-prime
  axis and the original z axis.

  Input: alpha,theta  : Origin on unit sphere, alpha is RA and theta is DEC
                          in RADIANS.  Use xyz2radec to compute from x,y,z.
 Output: RM           : 3x3 and 2x2 rotation matrices (structure).
*/
void SetRotationMatrix( double alpha, double theta, RMtype RM[1] )
{
/**/
const double pi         =           3.141592654;
const double halfpiclip = (pi/2.) - 0.000000001;
/**/
/* Clip DEC, just in case. */
theta = MIN( theta , halfpiclip        );
theta = MAX( theta , halfpiclip * -1.0 );
/* Set up forward matrix: x,y,z to xp,yp,zp. */
RM[0].mat[0][0] =     cos(theta) * cos(alpha);
RM[0].mat[0][1] =-1.*              sin(alpha);
RM[0].mat[0][2] =-1.* sin(theta) * cos(alpha);
RM[0].mat[1][0] =     cos(theta) * sin(alpha);
RM[0].mat[1][1] =                  cos(alpha);
RM[0].mat[1][2] =-1.* sin(theta) * sin(alpha);
RM[0].mat[2][0] =     sin(theta);
RM[0].mat[2][1] = 0.;
RM[0].mat[2][2] =     cos(theta);
/* Invert matrix. */
MatInv3D(RM[0].mat,RM[0].inv);
return;
}

/* ---------------------------------------------------------------------
  This routine used with Galex simulation images, see "RotMat2D" for a
  more general 2-D rotation matrix.
  Set image plane rotation matrices (RM[0].img and RM[0].imginv in RM[0].*) to
    convert x,y to a "fixed-grism-PA" x,y and vice versa.
  Input: PA      : image grism position angle in RADIANS.
 Output: RM      : 3x3 and 2x2 rotation matrices (structure).
*/
void SetImageRotationMatrix( double PA, RMtype RM[1] )
{
/**/
/* Rotation matrix on the image plane. PA direction will be decr. x in image. */
RM[0].img[0][0] =-1.*sin(PA);
RM[0].img[1][0] =-1.*cos(PA);
RM[0].img[0][1] =    cos(PA);
RM[0].img[1][1] =-1.*sin(PA);
/* Invert matrix. */
MatInv2D(RM[0].img,RM[0].imginv);
return;
}

/* ---------------------------------------------------------------------
  Create a 2-D rotation matrix (input angle is in degrees not radians).
Use:
  Xp =    RotMat[0][0] * X   +  RotMat[1][0] * Y
  Yp =    RotMat[0][1] * X   +  RotMat[1][1] * Y
Or:
  X  = RotMatInv[0][0] * Xp  +  RotMatInv[1][0] * Yp
  Y  = RotMatInv[0][1] * Xp  +  RotMatInv[1][1] * Yp

If you use:
  colp =    RotMat[0][0] * col   +  RotMat[1][0] * row
  rowp =    RotMat[0][1] * col   +  RotMat[1][1] * row
Then the prime image will appear to be rotated clockwise "Angle" degrees.

  Input: Angle           : image grism position angle in DEGREES.

 Output: RotMat[2][2]    : 2x2 rotation matrix.
         RotMatInv[2][2] : 2x2 rotation matrix.

*/
void SetRotMat2D( double Angle, double RotMat[2][2], double RotMatInv[2][2] )
{
/**/
double theta;
/**/
/* To Radians. */
theta = degtorad( Angle );
/* Rotation matrix on 2D image plane. */
RotMat[0][0] =    cos( theta );
RotMat[1][0] =    sin( theta );
RotMat[0][1] =-1.*sin( theta );
RotMat[1][1] =    cos( theta );

/* Invert matrix. */
MatInv2D(RotMat,RotMatInv);

return;
}


/* --------------------------------------------------------------------
  Extrapolates by a factor fac through the face of the simplex across from
  the high point, tries it, and replaces the high point if the new point
  is better.
*/
double camotry( double pp[AmoebaMax][AmoebaMax], double y[],
                double psum[], int ndim,
                double (*funk)(double []), int ihi, double fac )
{
/**/
int jj;
double fac1,fac2,ytry;
double ptry[AmoebaMax];
/**/

/* Initialize. */
fac1 = (1. - fac) / ndim;
fac2 = fac1 - fac;
for (jj=0; jj<ndim; ++jj) {
  ptry[jj] = (psum[jj] * fac1) - (pp[ihi][jj] * fac2);
}

/* Evaluate the function at the trial point. */
ytry = (*funk)(ptry);

/* If it's better than the highest, then replace the highest. */
if (ytry < y[ihi]) {
  y[ihi] = ytry;
  for (jj=0; jj<ndim; ++jj) {
    psum[jj] = psum[jj] + (ptry[jj] - pp[ihi][jj]);
    pp[ihi][jj] = ptry[jj];
  }
}

return(ytry);
}

/* --------------------------------------------------------------------
               NOTE:  Use camoeba_gate(), see below.
  Multidimensional minimization of the function funk(x) where x[0..ndim-1] is
  a vector in ndim dimensions, by the downhill simplex method of Nelder and
  Mead.  The matrix pp[0..ndim][0..ndim-1] is input.  Its ndim+1 rows are
  ndim-dimensional vectors which are the vertices of the starting simplex.
  Also input is the vector y[..ndim+1], whose components must be preinitialized
  to the values of funk evaluated at the ndim+1 vertices (rows) of p; and
  ftol the fractional convergence tolerance to be achieved in the function
  value (n.b.!).  On output, p and y will have been reset to ndim+1 new points
  all within ftol of a minimum function value, and nfunk gives the number of
  function evaluations taken.
  Returns *nfunk = -1, if error.
*/
void camoeba( double pp[AmoebaMax][AmoebaMax], double y[], int ndim,
              double ftol, double (*funk)(double []), int *nfunk )
{
/**/
int ii,jj,ihi,ilo,inhi,mpts;
/**/
double rtol,sum,swap,ysave,ytry;
double psum[AmoebaMax];
/**/
const double TINY = 1.e-10;
const int NMAX = 9000;
/**/

/* Initialize. */
mpts = ndim + 1;

/* Iterations. */
*nfunk = 0;

/* GET "psum". */
for (jj=0; jj<ndim; ++jj) {
  sum = 0.;
  for (ii=0; ii<mpts; ++ii) { sum = sum + pp[ii][jj]; }
  psum[jj] = sum;
}

/* Big loop. */
while (1) {

/* First we must determine which point is the highest (worst), next-highest,
   and lowest (best), by looping over the points in the simplex.  */
  ilo = 0;
  if (y[0] > y[1]) { ihi=0; inhi=1; } else {ihi=1; inhi=0; }
  for (ii=0; ii<mpts; ++ii) {
    if (y[ii] <= y[ilo]) ilo=ii;
    if (y[ii] > y[ihi]) {
      inhi=ihi;
      ihi=ii;
    } else {
     if ((y[ii] > y[inhi])&&(ii != ihi)) inhi=ii;
    }
  }

/* Compute the fractonal range from highest to lowest and return if ok. */
  rtol= 2. * ABS((y[ihi] - y[ilo])) / ( ABS((y[ihi])) + ABS((y[ilo])) + TINY );

/* If returning, put the best point and value in slot 1. */
  if (rtol < ftol) {
    swap=y[0]; y[0]=y[ilo]; y[ilo]=swap;
    for (ii=0; ii<ndim; ++ii) {
      swap=pp[0][ii]; pp[0][ii]=pp[ilo][ii]; pp[ilo][ii]=swap;
    }
    break;
  }

/* Check iteration limit. */
  if (*nfunk >= NMAX) {
    fprintf(stderr,"***ERROR: camoeba: NMAX(%d) iterations exceeded.\n",NMAX);
    *nfunk = -1;
    break;
  }

/* Increment the iterations. */
  *nfunk = *nfunk + 2;

/* Begin a new iteration.  First extrapolate by a factor -1 through the face
   of the simplex across from the high point, i.e. reflect the simplex from
   the high point. */
  ytry = camotry(pp,y,psum,ndim,funk,ihi,-1.0);

/* Gives a result better than the best point, so try an additional
   extrapolation by a factor of 2. */

  if (ytry <= y[ilo]) {
    ytry = camotry(pp,y,psum,ndim,funk,ihi,2.0);

/* The reflected point is worse than the second-highest, so look for an
   intermediate lower point, i.e. do a one-dimensional contraction. */
  } else {
  if (ytry >= y[inhi]) {
    ysave = y[ihi];
    ytry = camotry(pp,y,psum,ndim,funk,ihi,0.5);

/* Can't seem to get rid of that high point.  Better contract around the
   lowest (best) point. */
    if (ytry >= ysave) {
      for (ii=0; ii<mpts; ++ii) {
        if (ii != ilo) {
          for (jj=0; jj<ndim; ++jj) {
            psum[jj] = 0.5 * (pp[ii][jj]+ pp[ilo][jj]);
            pp[ii][jj]= psum[jj];
          }
          y[ii] = (*funk)(psum);
        }
      }
      *nfunk = *nfunk + ndim;

/* GET "psum". */
      for (jj=0; jj<ndim; ++jj) {
        sum = 0.;
        for (ii=0; ii<mpts; ++ii) { sum = sum + pp[ii][jj]; }
        psum[jj] = sum;
      }
    }

  } else {
   *nfunk = *nfunk - 1;

  }}                           /* Close both "else"'s */
}                              /*  while (1) { ... */
return;
}

/* ----------------------------------------------------------------------
  A gateway to the amoeba multi-dimensional minimization routine.

  Input: "funk" : An external function of function to be minimized.
                    Declare as: "double (yourname)(double [])" in your code and
                    then call this routine using "..., &yourname, ...".
  Input:   npar : Number of parameters in your function.
                    See "AmoebaMax" definition for maximum number (-1).
 In/Out:  par[] : Initial guess at the parameter values.  Returns the final
                    (function minimized) values.
  Input: parsl[]: Initial scale length of parameter values, i.e. roughly this
                    is a guess in the uncertainity of each parameter.
                    Use positive numbers.
  Input:   ftol : Fractional tolerance, use "1.e-9", or your choice.

 Output:  resid : Final residual value of "funk".
 Output:  niter : Number of iterations required for convergence.
                    Returns "niter == -1" if NOT successful.

*/
void camoeba_gate( double (*funk)(double []),
                   int npar, double par[], double parsl[],
                   double ftol, double *resid, int *niter )
{
/**/
double partemp[AmoebaMax], y[AmoebaMax];
double pp[AmoebaMax][AmoebaMax];
/**/
int ii,jj,ndim,mpts;
/**/

/* Dimensions for amoeba arrays. */
ndim = npar;
mpts = npar + 1;

/* Set up pp[][] matrix. */
for (ii=0; ii<mpts; ++ii) {
  for (jj=0; jj<ndim; ++jj) {
    pp[ii][jj] = par[jj];
  }
}
/* Add scale lengths */
for (jj=0; jj<ndim; ++jj) {
  ii = jj + 1;
  pp[ii][jj] = pp[ii][jj] + parsl[jj];
}
/* Initial evaluations. */
for (ii=0; ii<mpts; ++ii) {
  for (jj=0; jj<ndim; ++jj) {
    partemp[jj] = pp[ii][jj];
  }
  y[ii] = (*funk)( partemp );
}

/* Call Amoeba minimization. */
camoeba( pp, y, ndim, ftol, funk, niter );

/* Load results. */
for (jj=0; jj<ndim; ++jj) { par[jj] = pp[1][jj]; }
*resid = y[0];

return;
}

