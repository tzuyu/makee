C
C
C  THIS VERSION MODIFIED FOR LINUX
C
C

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C astro_lib.f
C
C   Subroutines and functions for ASTRO.FOR
C
C
C THESE SUBROUTINES/FUNCTIONS USE DOUBLE-PRECISION (REAL*8) FOR ALL
C   THE REAL VARIABLES.  YOU SHOULD ALWAYS USE REAL*8 VARIABLES IN CALLING THEM
C   AND THE FORMAT 1.0D+12 WHEN INCLUDING CONSTANTS...
C
C 345----1----+----2----+----3----+----4----+----5----+----6----+----7----+
C
C A LIST OF SUBROUTINES AND FUNCTIONS (IN ORDER OF APPEARANCE) :
C RESETZ(Z)    : resets value(s) of redshift used in other subs
C RESETHQ(H,Q) : resets values of the hubble constant and qo
C real*8 function PD(Z0,H0,Q0)      : proper distance
C real*8 function ARCCOSH(Y,PRECSN) : inverse hyperbolic cosine
C RELVEL(Z)    : relative velocity between redshifts
C RELVELW      : relative velocity between wavelengths
C VACAIR       : vacuum-to-air/air-to-vacuum conversions
C PRECESS_IT   : runs/displays RA-DEC precession
C PRECESS      : actually does the RA-DEC precession
C real*8 function AS(A1,T1,A2,T2)   : angle between two RA-DEC positions
C ANGSEP       : displays angle between two RA-DEC positions
C CLUSTER      : calculates values for a cluster of objects
C LUM(LE,F0,Z0,H0,Q0,R,FLAG) : calculates luminosity/flux
C LUMFLUX(Z,H,Q) : displays luminosity, flux, and proper distance(cm)
C ANGLE(TH,D,Z0,H0,Q0,R) : calculates observed angular separation/distance
C SPATIAL(Z,H,Q) : displays angle, distance, and proper distance(Mpc)
C LBTIME(FTAU,AGE,Z0,H0,Q0) : calculates look-back time/age of universe
C LOOKBACK(Z,H,Q) : displays look-back time/age of universe/etc...
C GALACTIC     : converts equatorial to galactic or ecliptic coordinates
C EQU2GAL(ALPHA,DELTA,XL,XB) : used by GALACTIC
C GAL2EQU(XL,XB,ALPHA,DELTA) : used by GALACTIC
C DECI2HMS(D,H,M,S,THESIGN)  : converts decimal values to hours,minutes,seconds
C HMS2DECI(D,H,M,S,THESIGN)  : converts hours,minutes,seconds to decimal values
C DISPSKY      : displays sky quantities (air mass, position angle, etc...)
C SKY(HA,DEC,LAT,AM,AZI,ZT,ZA,PA,DR,AT,WV) : given hour angle, declination and
C      latitude it calculates air mass, azimuthal angle, zenith angle (true and
C      apparent), position ang., differential refraction, atmospheric transmiss.
C AST_HELJD(RA,DEC,MONTH,DAY,YEAR,UT,JD,HJD,LST,HA,Z,AIRMAS,V,LATITUDE,
C           LONGITUDE)  (This is in ast-util.f) :
C      given ra,dec,date,longitude, and latitude it calculates the julian date,
C      local sidereal time, hour angle, zenith angle, etc... used by DISPSKY.
C TABLES       : creates tables in output files of zenith angle(Z), azimuth,
C      tan(Z), sec(Z), air mass, and position angle at a given latitude (using
C      the subroutine SKY) as a function of hour angle and declination.
C RELTRANS : creates a table of ST, ZA, TAN(ZA), AIRMASS, TUB, relative
C      transmission at a number of wavelengths to HA=0, and HA.
C LATLONG(LAT,LONG,RA,DEC,K) : request user for inputs of latitude, longitude,
C      RA, and DEC; programmer can select which ones using K.
C LOCATE : Calculates the best RA-DEC position of an object given distances to
C      stars with known positions in millimeters.
C
C----------------------------------------------------------------------
C SkySpot(ra,dec,epoch,lat,long,azimuth,zd,lst)  calculates azimuth, true
C      zenith distance, and local sidereal time given RA, Dec, UT epoch,
C      latitude, and longitude.  (This is in ast-util.f .)
C----------------------------------------------------------------------


C----------------------------------------------------------------------
      SUBROUTINE RESETZ(Z)
C allows user to input an array of redshifts into Z(...) with 0 at end
C if a "0" is entered then the current array is used...
      IMPLICIT NONE
      REAL*8 Z(0:20),TEMP
      INTEGER I

      print *,'Enter "0" to maintain current value(s).'
      WRITE(6,6020)
6020  FORMAT(2X,'Enter REDSHIFT : ',$)
      READ(5,*) TEMP
      IF (TEMP.EQ.0.0) RETURN
      Z(1) = TEMP
      Z(2) =0.0
      IF (Z(1).EQ.-1.0) THEN
       print *,'Enter "0" to stop...'
       Z(0)=1.0
       I=0
       DO WHILE(Z(I).NE.0.0)
        I=I+1
        WRITE(6,6025) I
6025    FORMAT(2X,'Enter REDSHIFT #',I2,' : ',$)
        READ(5,*) Z(I)
       END DO
      END IF

      RETURN
      END



C----------------------------------------------------------------------
      SUBROUTINE RESETHQ(H,Q)
C allows user to input an array of the Hubble constant and the
C deceleration parameter with 0's at the ends...
      IMPLICIT NONE
      REAL*8 H(0:20),Q(0:20)
      INTEGER I

      H(2) =0.0
      WRITE(6,6001)
6001  FORMAT(2X,'Enter Ho : ',$)
      READ(5,*) H(1)
      IF (H(1).EQ.-1.0) THEN
       print *,'Enter "0" to stop...'
       H(0)=1.0
       I=0
       DO WHILE(H(I).NE.0.0)
        I=I+1
        WRITE(6,6002) I
6002    FORMAT(2X,'Enter Ho #',I2,' : ',$)
        READ(5,*) H(I)
       END DO
      END IF

      Q(2) =-9.0
      WRITE(6,6020)
6020  FORMAT(2X,'Enter qo : ',$)
      READ(5,*) Q(1)
      IF (Q(1).EQ.-1.0) THEN
       print *,'Enter "-9.0" to stop...'
       Q(0)=1.0
       I=0
       DO WHILE(Q(I).NE.-9.0)
        I=I+1
        WRITE(6,6030) I
6030    FORMAT(2X,'Enter qo #',I2,' : ',$)
        READ(5,*) Q(I)
       END DO
      END IF

      RETURN
      END


C----------------------------------------------------------------------
      REAL*8 FUNCTION PD(Z0,H0,Q0)
C calculates proper distance in Mega-parsecs
      IMPLICIT NONE
      REAL*8 Z0,H0,Q0,R
      IF (Q0.LT.0.0) THEN
       print *,'ERROR-- qo LESS THAN ZERO'
       PD = 1.0
       RETURN
      END IF
      IF (Q0.LT.1.0D-10) THEN
       R=Z0*(1.0+ Z0/2.0)/(1.0+Z0)
                         ELSE
       R=(Q0*Z0 +(Q0-1.0)*(DSQRT(1.0+2.0*Q0*Z0)-1.0))/(Q0*Q0*(1.0+Z0))
      END IF
      R = R*(2.997925D+5/H0)
      PD = R
      RETURN
      END


C----------------------------------------------------------------------
      REAL*8 FUNCTION ARCCOSH(Y,PRECSN)
C calculates the absolute value of the inverse hyperbolic cosine
C   in radians to within +/- PRECSN
      IMPLICIT NONE
      REAL*8 Y,X1,X2,X3,PRECSN
      X2 =1.0
100   IF (DCOSH(X2).GT.Y) THEN 
        X1 = 0.0
        GOTO 200
      ELSE
        X2 = X2*2.0
        GOTO 100
      END IF
200   X3 = (X1+X2)/2.0
      IF (DABS(X1-X2).LT.PRECSN) GOTO 900
      IF (DCOSH(X3).GT.Y) THEN
       X2=X3
      ELSE
       X1=X3
      END IF
      GOTO 200             
900   ARCCOSH=X3

      RETURN
      END



C----------------------------------------------------------------------
      SUBROUTINE RELVEL(Z)
   
      IMPLICIT NONE
      REAL*8 ZE,Z(0:20),BETA,V(0:20)
      INTEGER I

      print *,'            '
      print *,'BETA = [(1+Ze)^2 - (1+Za)^2] / [(1+Ze)^2 + (1+Za)^2]'
      print *,'            '
      print *,'1+Za = (1+Ze) * [(1-beta)/(1+beta)]**(0.5)'
      print *,'            '

      WRITE(6,6010)
6010  FORMAT(2X,'Enter Z emission : ',$)
      READ(5,*) ZE

      CALL RESETZ(Z)

      V(2) =0.0
      WRITE(6,6020)
6020  FORMAT(2X,'Enter velocity : ',$)
      READ(5,*) V(1)
      IF (V(1).EQ.-1.0) THEN
       print *,'Enter "0" to stop...'
       V(0)=1.0
       I=0
       DO WHILE(V(I).NE.0.0)
        I=I+1
        WRITE(6,6025) I
6025    FORMAT(2X,'Enter velocity #',I2,' : ',$)
        READ(5,*) V(I)
       END DO
      END IF

      print *,'    Ze       Z      VEL.(km/sec)     beta  '
      I=1
      DO WHILE (Z(I).NE.0.0)
       BETA=((1.0+ZE)**2.0-(1.0+Z(I))**2.0)
       BETA=BETA/((1.0+ZE)**2.0+(1.0+Z(I))**2.0)
       WRITE(6,6030) ZE,Z(I),(BETA*2.997925D+5),BETA
6030   FORMAT(1X,2F9.5,F14.2,F12.8)
       I=I+1
      END DO
      I=1
      DO WHILE (V(I).NE.0.0)
       BETA = V(I)/(2.997925D+5)
       Z(I) = (1.0+ZE)*DSQRT((1.0-BETA)/(1.0+BETA)) -1.0
       WRITE(6,6040) ZE,Z(I),(BETA*2.997925D+5),BETA
6040   FORMAT(1X,2F9.5,F14.2,F12.8)
       I=I+1
      END DO
     
      RETURN
      END

C----------------------------------------------------------------------
C Find vel (set vel=0) given lam1 and lam2 or find lam2 (set lam2=0)
C given lam1 and vel.  The velocity will be lam2 relative to lam1, i.e. lam1
C is the reference point. Finds relative velocity.  If lam2<lam1 then vel<0.
C
      subroutine RelativeVelocity(lam1,lam2,vel)
      implicit none
      real*8 lam1,lam2,vel,beta,sol
      parameter(sol=2.99792458d+5)
      if (abs(vel).lt.1.d-30) then
        beta= (lam2*lam2 - lam1*lam1) / (lam2*lam2 + lam1*lam1)
        vel = sol * beta
      elseif (abs(lam1).lt.1.d-30) then
        beta= vel / sol
        lam1= lam2 * dsqrt( (1.d0 - beta) / (1.d0 + beta) )
      elseif (abs(lam2).lt.1.d-30) then
        beta= vel / sol
        lam2= lam1 / dsqrt( (1.d0 - beta) / (1.d0 + beta) )
      endif
      return
      end
C----------------------------------------------------------------------
C Find velocity (km/s) given Ze and Za. If Za<Ze, a positive velocity is given.
C
      real*8 function OutflowZ(ze,za)
      implicit none
      real*8 ze,za,beta
      beta = ( (1.d0+ze)*(1.d0+ze) - (1.d0+za)*(1.d0+za) )
     .     / ( (1.d0+ze)*(1.d0+ze) + (1.d0+za)*(1.d0+za) )
      OutflowZ = beta * 299792.5d0
      return
      end

C----------------------------------------------------------------------
      SUBROUTINE RELVELW
   
      IMPLICIT NONE
      REAL*8 LE,L(0:20),BETA,V(0:20)
      INTEGER I

      print *,'            '
      print *,'BETA = [ Le^2 - La^2 ] / [ Le^2 + La^2 ]'
      print *,'             '
      print *,'La = Le * [(1-beta)/(1+beta)]**(0.5)'
      print *,'            '

      WRITE(6,6010)
6010  FORMAT(2X,'Enter LAMBDA emission : ',$)
      READ(5,*) LE
      
      L(2) =0.0
      WRITE(6,6020)
6020  FORMAT(2X,'Enter lambda : ',$)
      READ(5,*) L(1)
      IF (L(1).EQ.-1.0) THEN
       print *,'Enter "0" to stop...'
       L(0)=1.0
       I=0
       DO WHILE(L(I).NE.0.0)
        I=I+1
        WRITE(6,6025) I
6025    FORMAT(2X,'Enter lambda #',I2,' : ',$)
        READ(5,*) L(I)
       END DO
      END IF

      V(2) =0.0
      WRITE(6,6030)
6030  FORMAT(2X,'Enter velocity : ',$)
      READ(5,*) V(1)
      IF (V(1).EQ.-1.0) THEN
       print *,'Enter "0" to stop...'
       V(0)=1.0
       I=0
       DO WHILE(V(I).NE.0.0)
        I=I+1
        WRITE(6,6035) I
6035    FORMAT(2X,'Enter velocity #',I2,' : ',$)
        READ(5,*) V(I)
       END DO
      END IF


      print *,'    Le     Lambda   VEL.(km/sec)     beta  '
      I=1
      DO WHILE (L(I).NE.0.0)
       BETA=(LE**2.0-L(I)**2.0)/(LE**2.0+L(I)**2.0)
       WRITE(6,6040) LE,L(I),(BETA*2.997925D+5),BETA
6040   FORMAT(1X,2F9.3,F14.2,F12.8)
       I=I+1
      END DO
      I=1
      DO WHILE (V(I).NE.0.0)
       BETA = V(I)/(2.997925D+5)

C Fixed this incorrect equation 26 August 1992 -tab
C      L(I) = LE*DSQRT((1.0D0-BETA)/(1.0D0+BETA)) -1.0
       L(I) = LE*DSQRT((1.0D0-BETA)/(1.0D0+BETA))

       WRITE(6,6050) LE,L(I),(BETA*2.997925D+5),BETA
6050   FORMAT(1X,2F9.3,F14.2,F12.8)
       I=I+1
      END DO
     
      RETURN
      END


C-------------------------------------------------------------------
C Adjusts wavelengths w(1..nw) by vel (km/sec).  If vel > 0 then w(..) will
C increase (redshift).  If vel < 0 then w(..) will decrease (blueshift).
C
      subroutine velcorr_array(w,nw,vel)

      implicit none
      integer*4 nw,i
      real*4 w(nw),vel,r
      real*8 r8

      r8 = dble(vel) / 2.997925D+5
      r8 = dsqrt((1.d0 + r8)/(1.d0 - r8))
      r = sngl(r8)
      do i=1,nw
        w(i) = w(i) * r
      enddo
      return
      end

C-------------------------------------------------------------------
C Clips x y arrays to only include pairs with x values between x1 and x2.
C
      subroutine cut_array(x,y,num,x1,x2)

      implicit none
      integer*4 num,i,n
      real*4 x(*),y(*),x1,x2

      n = 0 
      do i=1,num
        if ( (x(i).gt.x1).and.(x(i).lt.x2) ) then
          n=n+1
          x(n) = x(i)
          y(n) = y(i)
        endif
      enddo
      num = n
      return
      end


C-------------------------------------------------------------------
C Adjusts wavelengths w(1..nw) from air to vacuum (mode = 1)
C or from vacuum to air (mode = -1).  Assumes 15 deg Celsius and 76 cm Hg .
C
      subroutine vacair_array(w,nw,mode)

      implicit none
      integer*4 nw,i,mode
      real*4 w(nw),r
      real*8 x,n,sign

      sign = dfloat(mode)
      do i=1,nw
        x = dble(w(i))*dble(w(i))
        n = 2726.43 +(12.288/(x*1.0d-8)) +(0.3555/(x*x*1.0d-16))
        n = n*(1.0d-7) + 1.0
        r = sngl( 1.d0 + ( sign*(n-1.d0) ) )
        w(i) = w(i) * r
      enddo
      return
      end

C-------------------------------------------------------------------
C Calculates vacuum-to-air/ air-to-vacuum conversions...

      subroutine vacair

      implicit none
      real*8 w(50),n,x,y
      integer i,j

      i=0
100   i=i+1
      write(6,6020) i
6020  format(2x,'enter wavelength #',i2,' (-1 to stop) : ',$)
      read(5,*) w(i)
      if (w(i).ge.0.0) goto 100

      print *, '                 '
      print *, ' index of refraction of air (15 deg c, 76 cm hg)'
      print *, '      (n-1) x 10^7 = '
      print *, ' 2726.43 + 12.288/(w^2 x 10^-8) + 0.3555/(w^4 x 10^-16)'
      print *, '                 '
      print *, ' wavelength     n     air->vac (add)   ->vac     ->air'
      print *, '                 '

      do j=1,(i-1)
       x = w(j)
       n = 2726.43 +(12.288/(x*x*1.0d-8)) +(0.3555/(x*x*x*x*1.0d-16))
       n = n*(1.0d-7) + 1.0
       y = n*x-x
       write(6,6030) x,n,y,x+y,x-y
6030   format(1x,f9.3,f12.8,f10.4,f15.3,f10.3)
      end do
      print *,'            '

      return
      end

C-------------------------------------------------------------------
C Vacuum to air...
C Assumes index of refraction of air with 15 deg c, 76 cm hg.
C Uses:  (n-1) x 10^7 = 2726.43 + 12.288/(w^2 x 10^-8) + 0.3555/(w^4 x 10^-16)
C
      real*8 function vac2air(wave)
      implicit none
      real*8 n,x,y,wave
      x = wave
      n = 2726.43 +(12.288/(x*x*1.0d-8)) +(0.3555/(x*x*x*x*1.0d-16))
      n = n*(1.0d-7) + 1.0
      y = n*x-x
      vac2air = x-y
      return
      end

C-------------------------------------------------------------------
C Air to vacuum...
C Assumes index of refraction of air with 15 deg c, 76 cm hg.
C Uses:  (n-1) x 10^7 = 2726.43 + 12.288/(w^2 x 10^-8) + 0.3555/(w^4 x 10^-16)
C
      real*8 function air2vac(wave)
      implicit none
      real*8 n,x,y,wave
      x = wave
      n = 2726.43 +(12.288/(x*x*1.0d-8)) +(0.3555/(x*x*x*x*1.0d-16))
      n = n*(1.0d-7) + 1.0
      y = n*x-x
      air2vac = x+y
      return
      end


C--------------------------------------------------------------------------
      SUBROUTINE PRECESS_IT
C
C This program uses an approximate precession formula to calculate approximate
C  values of R.A. and DEC at an entered date#2 given the coordinates of an
C  entered date#1.  Errors will arise due to the exact time being taken as
C  12:00:00 am (at the beginning) of the entered day; and that the year is
C  assumed to be a non-leap-year with the fractional year being taken as
C  ( day-of-year - 1.0 ) / 365.250 ;  and also that nutation of the earth's
C  orbit, annual aberration and latitude variation are neglected.
C  (annual parallax and proper motion are not considered since this program is
C   mainly intended for reduction of coordinates for distant objects)
C
C The calculated values should be accurate to within a few arc-seconds (not
C  taking into account aberration).
C  
C NOTE: epoch 1950.0 is entered as  1,1,1950
C
      IMPLICIT NONE
      REAL*8 DYEAR(2),ra1,dec1,ra2,dec2
      REAL*8 RAS,SEC,THESIGN,RA,DEC,YEAR_START, YEAR_END
      INTEGER*4 RAH,RAM,DEG,MIN,RASYM,DECSYM
      INTEGER*4 NDAYS(12),DAY(2),YEAR(2),MONTH(2)
      CHARACTER*1 CSIGN
      real*8 cosdd

C ...number of days before the first of each month (non-leap year)...
C
C  "thirty days hath september; april,june, and november, all the rest have
C        thirty-one, except february (which has 28)..."
C
      NDAYS(1)= 0
      NDAYS(2)= 31
      NDAYS(3)= 59
      NDAYS(4)= 90
      NDAYS(5)= 120
      NDAYS(6)= 151
      NDAYS(7)= 181
      NDAYS(8)= 212
      NDAYS(9)= 243
      NDAYS(10)=273
      NDAYS(11)=304
      NDAYS(12)=334

10    WRITE(6,1000) 
1000  FORMAT(1X,'Enter  DAY, MONTH, YEAR  of first date : ',$)
      READ(5,*) DAY(1),MONTH(1),YEAR(1)
      IF (MONTH(1).GT.12) GOTO 10
      IF (YEAR(1).LT.100.) YEAR(1) = YEAR(1) + 1900

20    WRITE(6,1001) 
1001  FORMAT(1X,'Enter  DAY, MONTH, YEAR of second date : ',$)
      READ(5,*) DAY(2),MONTH(2),YEAR(2)
      IF (MONTH(2).GT.12) GOTO 20
      IF (YEAR(2).LT.100.) YEAR(2) = YEAR(2) + 1900

      DYEAR(1)=DFLOAT(NDAYS(MONTH(1))+DAY(1)-1)/365.250+DFLOAT(YEAR(1))
      DYEAR(2)=DFLOAT(NDAYS(MONTH(2))+DAY(2)-1)/365.250+DFLOAT(YEAR(2))
      print *,DYEAR(1),DYEAR(2)
      print *,'   '
      WRITE(6,1010)
1010  FORMAT(1X,'Enter RIGHT ASCENSION (Hours, Min, Sec) : ',$)
      READ(5,*) RAH,RAM,RAS
      WRITE(6,1020)
1020  FORMAT(1X,'Enter  DECLINATION  (Degrees, Min, Sec) : ',$)
      READ(5,*) DEG,MIN,SEC
      CALL HMS2DECI(RA,RAH,RAM,RAS,THESIGN)
      CALL HMS2DECI(DEC,DEG,MIN,SEC,THESIGN)
      ra1 = RA
      dec1= DEC
      print *,RA,DEC
      print *,'    '
      YEAR_START = DYEAR(1)
      YEAR_END   = DYEAR(2)
      CALL PRECESS(YEAR_START,YEAR_END,RA,DEC)
C  now output the results...
      CALL DECI2HMS(RA,RAH,RAM,RAS,THESIGN)
      CALL DECI2HMS(DEC,DEG,MIN,SEC,THESIGN)
      CSIGN='+'
      IF (THESIGN.LT.0.0) CSIGN='-'
C RA designation convention is HHMM with no rounding up for the minutes
      RASYM = RAH*100 + RAM
      DECSYM = DEG*10 + INT(FLOAT(MIN)/6.0)
      WRITE(6,1100) RAH,RAM,RAS
1100  FORMAT(1X,'RIGHT ASCENSION : ',I3,I3,F5.1)
      WRITE(6,1110) CSIGN,DEG,MIN,SEC,RASYM,CSIGN,DECSYM
1110  FORMAT(1X,'   DECLINATION  : ',A1,I2,I3,F5.1,
     +          '          COORDINATE NAME : ',I4.4,A1,I3.3)

C Precession from iraf.
      call ast_precess(ra1,dec1,YEAR_START,ra2,dec2,YEAR_END)
      print *,'Differences with Iraf precession in arcseconds.'
      print *,(ra-ra2)*15.d0*3600.d0*cosdd(dec),(dec-dec2)*3600.d0

      RETURN
      END


C--------------------------------------------------------------------------
      SUBROUTINE PRECESS(YEAR_START,YEAR_END,RA,DEC)
C
C Calculates approximate value of R.A. and DEC between two given dates...
C
C This program uses an approximate precession formula to calculate approximate
C  values of R.A. and DEC at an entered date#2 given the coordinates of an
C  entered date#1.  Errors will arise due to the exact time being taken as
C  12:00:00 am (at the beginning) of the entered day; and that the year is
C  assumed to be a non-leap-year with the fractional year being taken as
C  ( day-of-year - 1.0 ) / 365.250 ;  and also that nutation of the earth's
C  orbit, annual aberration and latitude variation are neglected.
C  (annual parallax and proper motion are not considered since this program is
C   mainly intended for reduction of coordinates for distant objects)
C
C The calculated values should be accurate to within a few arc-seconds (not
C  taking into account aberration).
C  
C
C  Magnitudes of effects --
C
C     PRECESSION : about 50.2" per year  (motion of the nodes of the ecliptic)
C
C       NUTATION : lunar      =  +/-  9 " in 18.6 years;
C                  solar      =  +/- 1.2" in  0.5 years;
C                  fortnightly=  +/- 0.1" in  15  days    
C
C     ABERRATION : 20.47 " at maximum    (aberration of light; yearly cyclic)
C 
C     LATITUDE VARIATION : 0.04" maximum (wandering of pole of rotation)
C
C
C  NOTE: epoch 1950.0 is entered as  1,1,1950
C
      IMPLICIT NONE
      REAL*8 YEAR_START, YEAR_END   ! start and end dates in decimal years
      REAL*8 RA, DEC                ! position in decimal hours and degrees
      REAL*8 A0,D0,A,D,M,N,AM,DM,T,DYEAR(2)
      real*8 sindd,cosdd,tandd

      DYEAR(1) = YEAR_START   ! first date
      DYEAR(2) = YEAR_END     ! second date
      A = RA*15.              ! alpha
      D = DEC                 ! delta
C
C first calculate YEAR 2000.0  alpha (A0) and delta (D0)
C
      T=(DYEAR(1)-2000.00)/100.00
    
      M = 1.2812323*T + 0.0003879*T*T + 0.0000101*T*T*T
      N = 0.5567530*T - 0.0001185*T*T - 0.0000116*T*T*T

      AM = A - (.50)*( M + N*sindd(A)*tandd(D) )
      DM = D - (.50)*N*cosdd(AM)

      A0 = A - M - N*sindd(AM)*tandd(DM)
      D0 = D - N*cosdd(AM)

C
C  now calculate back to time #2...
C
      T=(DYEAR(2)-2000.00)/100.00
    
      M = 1.2812323*T + 0.0003879*T*T + 0.0000101*T*T*T
      N = 0.5567530*T - 0.0001185*T*T - 0.0000116*T*T*T

      AM = A0 + (.50)*( M + N*sindd(A0)*tandd(D0) )
      DM = D0 + (.50)*N*cosdd(AM)

      A = A0 + M + N*sindd(AM)*tandd(DM)
      D = D0 + N*cosdd(AM)

C now fix results
      IF (A.GT.360.0) A=A-360.0
      IF  (A.LT.0.0)  A=A+360.0
      RA = 24.00*(A/360.00)
      DEC = D

      RETURN
      END


C----------------------------------------------------------------------
C Calculates angular separation in arc-minutes given right ascension and
C declination in decimal hours and degrees.
      subroutine AS_RA_DEC(ra1,dec1,ra2,dec2,angsep)
      implicit none
      real*8 ra1,dec1,ra2,dec2,angsep,as,a1,t1,a2,t2
      a1 = 360.d0 * ( ra1 / 24.d0 )
      a2 = 360.d0 * ( ra2 / 24.d0 )
      t1 = 90.d0 - dec1
      t2 = 90.d0 - dec2
      angsep = as(a1,t1,a2,t2)
      return
      end

C----------------------------------------------------------------------
      REAL*8 FUNCTION AS(A1,T1,A2,T2)
C calculates angular separation in arc-minutes given alpha and theta.
C alpha == 360 * (R.A./24)...   theta == 90 - declination
      IMPLICIT NONE
      REAL*8 A1,T1,A2,T2,X1,Y1,Z1,X2,Y2,Z2
      real*8 sindd,cosdd,acosdd

      X1=sindd(T1)*cosdd(A1)
      Y1=sindd(T1)*sindd(A1)
      Z1=cosdd(T1)

      X2=sindd(T2)*cosdd(A2)
      Y2=sindd(T2)*sindd(A2)
      Z2=cosdd(T2)

      AS = acosdd(X1*X2 + Y1*Y2 + Z1*Z2)*60.0

      RETURN
      END



C----------------------------------------------------------------------
      SUBROUTINE ANGSEP
C calculates the angular separation between two RA-DEC positions...  

      IMPLICIT NONE
      REAL*8 RS,DS,A1,T1,A2,T2,THESIGN,R,AS
      INTEGER RH,RM,DD,DM
 
      WRITE(6,6010)
6010  FORMAT(2X,' #1   R.A.(hr,min,sec), DEC(deg,min,sec) : ',$)
      READ(5,*)  RH,RM,RS,DD,DM,DS
      CALL HMS2DECI(A1,RH,RM,RS,THESIGN)
      A1 = A1*90.0/6.0
      CALL HMS2DECI(T1,DD,DM,DS,THESIGN)
      T1 = 90.0 - T1
      
      WRITE(6,6015)
6015  FORMAT(2X,' #2   R.A.(hr,min,sec), DEC(deg,min,sec) : ',$)
      READ(5,*)  RH,RM,RS,DD,DM,DS
      CALL HMS2DECI(A2,RH,RM,RS,THESIGN)
      A2 = A2*90.0/6.0
      CALL HMS2DECI(T2,DD,DM,DS,THESIGN)
      T2 = 90.0 - T2

      WRITE(6,6050) A1,T1
6050  FORMAT(/,1X,'Alpha, Theta   1:(',F13.7,F13.7,') ')
      WRITE(6,6055) A2,T2
6055  FORMAT(1X,'Alpha, Theta   2:(',F13.7,F13.7,') ')
      print *,'     '
      print *,'SEPARATION -     '
      R = AS(A1,T1,A2,T2)
      WRITE(6,6060) R/60.0,R,R*60.0
6060  FORMAT(/,1X,'IN DEG:',F10.6,'    IN MIN:',F11.5,
     +'    IN SEC:',F13.5)

      RETURN
      END


C----------------------------------------------------------------------
      SUBROUTINE CLUSTER
c This subroutine calculates the "center" of the cluster, distance-from-
c  center for each, and the mean distance-from-center.
  
      IMPLICIT NONE
      INTEGER I,N
      INTEGER RH(20),RM(20),DD(20),DM(20),RHR,RMIN,DDEG,DMIN
      REAL*8 RS(20),DS(20),A(20),T(20),THESIGN,RSEC,DSEC
      REAL*8 CA(20),CT(20),ADIST(20),AS,R,SUMA,SUMT,COCA,COCT
      CHARACTER*30 FN
      CHARACTER*1 CSIGN
      real*8 cosdd
 
      print *,'     '
      WRITE(6,6000) 
6000  FORMAT(1X,'Write positions to a file ("n" for none) : ',$)
      READ(5,5000) FN
5000  FORMAT(A30)
      IF (FN.NE.'n') OPEN(UNIT=2,FILE=FN,STATUS='unknown')

      WRITE(6,6001)
6001  FORMAT(1X,'Enter number of positions : ',$)
      READ(5,*)  N

      SUMA = 0.0
      SUMT = 0.0
      DO I=1,N
       WRITE(6,6010) I
6010   FORMAT(1X,' #',I1,'   R.A.(hr,min,sec), DEC(deg,min,sec) : ',$)
       READ(5,*)  RH(I),RM(I),RS(I),DD(I),DM(I),DS(I)
       CALL HMS2DECI(A(I),RH(I),RM(I),RS(I),THESIGN)
       A(I) = A(I)*90.0/6.0
       CALL HMS2DECI(T(I),DD(I),DM(I),DS(I),THESIGN)
       T(I) = 90.0 - T(I)
       SUMA = SUMA + A(I)
       SUMT = SUMT + T(I)
      END DO
      COCA = SUMA/DFLOAT(N)
      COCT = SUMT/DFLOAT(N)

      print *,'  '
      print *,' N    R.A. ALPHA      THETA         DELTA A.('')'
     +  ,'   DELTA T.('')  ANG. DIST('')'

      SUMA = 0.0 
      DO I=1,N
       ADIST(I) = AS(A(I),T(I),COCA,COCT)
       SUMA = SUMA + ADIST(I)
       CA(I) = (A(I)-COCA)*60.0*cosdd(90.0-((T(I)+COCT)/2.0))
       CT(I) = (T(I)-COCT)*60.0
       WRITE(6,6100) I,A(I),T(I),CA(I),CT(I),ADIST(I)
6100   FORMAT(1X,I2,2X,F13.7,1X,F13.7,1X,F13.7,1X,F13.7,1X,F13.7)
       IF (FN.NE.'n') WRITE(2,2000) CA(I),CT(I)
2000   FORMAT(1X,2F14.7)
      END DO
      IF (FN.NE.'n') CLOSE(UNIT=2)

      R = SUMA/DFLOAT(N)
      WRITE(6,6200) R,R*60.0
6200  FORMAT(/,1X,'AVE. ANG. DIST. IN MIN:',F11.5,'   IN SEC:',F11.5)
      WRITE(6,6300) COCA,COCT
6300  FORMAT(1X,'CENTER: ALPHA=',F13.7,'   THETA=',F13.7)
      
c calculate RA and DEC of center of cluster...
      R = COCA*6.0/90.0
      CALL DECI2HMS(R,RHR,RMIN,RSEC,THESIGN)
      R = 90.0 - COCT
      CALL DECI2HMS(R,DDEG,DMIN,DSEC,THESIGN)
      CSIGN='+'
      IF (THESIGN.LT.0.0) CSIGN='-'
      WRITE(6,6400) RHR,RMIN,RSEC,CSIGN,DDEG,DMIN,DSEC
6400  FORMAT(1X,8X,'RA=',I2,I3,F6.2,'       DEC=',A1,I2,I3,F6.2)

      RETURN
      END



C---------------------------------------------------------------------
      SUBROUTINE LUM(LE,F0,Z0,H0,Q0,R,FLAG)
C calculates luminosity/observed flux either in ergs/sec flag=0,
C ergs/sec/angstrom flag=1, or ergs/sec/hz flag=2...
      IMPLICIT NONE
      REAL*8 LE,F0,Z0,H0,Q0,R,X,PD
      INTEGER FLAG

      X=1+Z0
      R=PD(Z0,H0,Q0)*3.0857D+24
      IF (LE.EQ.0.0) THEN
       IF (FLAG.EQ.0) LE=(F0*R)*4*(3.141592654)*R*X*X
       IF (FLAG.EQ.1) LE=(F0*R)*4*(3.141592654)*R*X*X*X
       IF (FLAG.EQ.2) LE=(F0*R)*4*(3.141592654)*R*X
      ELSE
       IF (FLAG.EQ.0) F0=(LE/R)/(4*(3.141592654)*R*X*X)
       IF (FLAG.EQ.1) F0=(LE/R)/(4*(3.141592654)*R*X*X*X)
       IF (FLAG.EQ.2) F0=(LE/R)/(4*(3.141592654)*R*X)
      END IF

      RETURN
      END


C---------------------------------------------------------------------
      SUBROUTINE LUMFLUX(Z,H,Q)
C displays values of luminosity/flux...
      IMPLICIT NONE
      INTEGER I,IQ,IH,IZ,ILE,IF0,FLAG
      REAL*8 Z(0:20),H(0:20),Q(0:20),L(0:20),F(0:20)
      REAL*8 Z0,H0,Q0,LE,F0,R

      print *, '                 '
      print *,'Obs. flux/luminosity, proper dist. d in cm.'
      print *,'   Le = fo*4*PI*d*d*(1+z)^2    ergs/s/cm2'
      print *,'   Le = fo*4*PI*d*d*(1+z)^3    ergs/s/cm2/ang'
      print *,'   Le = fo*4*PI*d*d*(1+z)      ergs/s/cm2/hz'
      print *,'            '

      IF (H(1).EQ.0.0) CALL RESETHQ(H,Q)
      CALL RESETZ(Z)

      WRITE(6,6000)
6000  FORMAT(1X,'ergs/s (0), ergs/s/ang (1), ergs/s/hz (2) : ',$)
      READ(5,*) FLAG
      print *,'       ' 

      L(2) =0.0
      WRITE(6,6001)
6001  FORMAT(2X,'Enter LUMINOSITY : ',$)
      READ(5,*) L(1)
      IF (L(1).EQ.-1.0) THEN
       print *,'Enter "0" to stop...'
       L(0)=1.0
       I=0
       DO WHILE(L(I).NE.0.0)
        I=I+1
        WRITE(6,6002) I
6002    FORMAT(2X,'Enter LUMINOSITY #',I2,' : ',$)
        READ(5,*) L(I)
       END DO
      END IF

      F(2) =0.0
      WRITE(6,6003)
6003  FORMAT(2X,'Enter OBSERVED FLUX : ',$)
      READ(5,*) F(1)
      IF (F(1).EQ.-1.0) THEN
       print *,'Enter "0" to stop...'
       F(0)=1.0  
       I=0
       DO WHILE(F(I).NE.0.0)
        I=I+1
        WRITE(6,6004) I
6004    FORMAT(2X,'Enter OBSERVED FLUX #',I2,' : ',$)
        READ(5,*) F(I)
       END DO
      END IF

      WRITE(6,6099)
6099  FORMAT(1X,'  qo     Ho      z         Le  ',
     + '         flux        P.D.(cm)')
6100  FORMAT(1X,F5.2,F7.1,F9.5,3E14.5)
      IQ=1
      DO WHILE (Q(IQ).NE.-9.0)
       Q0=Q(IQ)
       IH=1
       DO WHILE (H(IH).NE.0.0)
        H0=H(IH)  
        IZ=1
        DO WHILE (Z(IZ).NE.0.0)
         Z0=Z(IZ)
         ILE=1
         DO WHILE (L(ILE).NE.0.0)
          LE=L(ILE)
          F0=0.0
          CALL LUM(LE,F0,Z0,H0,Q0,R,FLAG)
          WRITE(6,6100) Q0,H0,Z0,LE,F0,R
          ILE=ILE+1
         END DO
         IF0=1
         DO WHILE (F(IF0).NE.0.0)
          F0=F(IF0)
          LE=0.0
          CALL LUM(LE,F0,Z0,H0,Q0,R,FLAG)
          WRITE(6,6100) Q0,H0,Z0,LE,F0,R
          IF0=IF0+1
         END DO
         IZ=IZ+1
        END DO
        IH=IH+1
       END DO
       IQ=IQ+1
      END DO

      RETURN
      END



C---------------------------------------------------------------------
      SUBROUTINE ANGLE(TH,D,Z0,H0,Q0,R)
C calculates the observed angle (in arcsecs) between two points
C  a distance D (in kiloparsecs) apart...
      IMPLICIT NONE
      REAL*8 TH,D,Z0,H0,Q0,R,PD
      R = PD(Z0,H0,Q0)
      IF (TH.EQ.0.0) THEN
       TH=(2.06265D+2)*D*(1.0+Z0)/R
      ELSE
       D = TH*R/((1.0+Z0)*2.06265D+2)
      END IF
      RETURN
      END


C---------------------------------------------------------------------
      SUBROUTINE SPATIAL(Z,H,Q)
C displays values of observed angular separation and proper diameters
      IMPLICIT NONE
      INTEGER I,IQ,IH,IZ,ID,ITH
      REAL*8 Z(0:20),H(0:20),Q(0:20),D(0:20),TH(0:20)
      REAL*8 Z0,H0,Q0,D0,TH0,R

      print *, '                 '
      print *,'Obs. arcsec separation, D in kpc, proper dist. d in Mpc'
      print *,'   theta = 2.06265D+2 * D (1+z) / d'
      print *,'            '

      IF (H(1).EQ.0.0) CALL RESETHQ(H,Q)
      CALL RESETZ(Z)

      TH(2) =0.0
      WRITE(6,6001)
6001  FORMAT(2X,'Enter THETA(arcsec) : ',$)
      READ(5,*) TH(1)
      IF (TH(1).EQ.-1.0) THEN
       print *,'Enter "0" to stop...'
       TH(0)=1.0
       I=0
       DO WHILE(TH(I).NE.0.0)
        I=I+1
        WRITE(6,6002) I
6002    FORMAT(2X,'Enter THETA(arcsec) #',I2,' : ',$)
        READ(5,*) TH(I)
       END DO
      END IF

      D(2) =0.0
      WRITE(6,6003)
6003  FORMAT(2X,'Enter DISTANCE(kpc) : ',$)
      READ(5,*) D(1)
      IF (D(1).EQ.-1.0) THEN
       print *,'Enter "0" to stop...'
       D(0)=1.0
       I=0
       DO WHILE(D(I).NE.0.0)
        I=I+1
        WRITE(6,6004) I
6004    FORMAT(2X,'Enter DISTANCE(kpc) #',I2,' : ',$)
        READ(5,*) D(I)
       END DO
      END IF

      WRITE(6,6099)
6099  FORMAT(1X,'  qo     Ho      z     Dist.(kp',
     + 'c)     theta(")      theta('')       P.D.(Mpc)')
6100  FORMAT(1X,F5.2,F7.1,F9.5,4E14.5)
      IQ=1
      DO WHILE (Q(IQ).NE.-9.0)
       Q0=Q(IQ)
       IH=1
       DO WHILE (H(IH).NE.0.0)
        H0=H(IH)  
        IZ=1
        DO WHILE (Z(IZ).NE.0.0)
         Z0=Z(IZ)
         ID=1
         DO WHILE (D(ID).NE.0.0)
          D0=D(ID)
          TH0=0.0
          CALL ANGLE(TH0,D0,Z0,H0,Q0,R)
          WRITE(6,6100) Q0,H0,Z0,D0,TH0,TH0/60.0,R
          ID=ID+1
         END DO
         ITH=1
         DO WHILE (TH(ITH).NE.0.0)
          D0=0.0
          TH0=TH(ITH)
          CALL ANGLE(TH0,D0,Z0,H0,Q0,R)
          WRITE(6,6100) Q0,H0,Z0,D0,TH0,TH0/60.0,R
          ITH=ITH+1
         END DO
         IZ=IZ+1
        END DO
        IH=IH+1
       END DO
       IQ=IQ+1
      END DO

      RETURN
      END


C---------------------------------------------------------------------
C calculates fractional look-back time and age of the universe.
C
      SUBROUTINE LBTIME(FTAU,AGE,Z0,H0,Q0)
C
      IMPLICIT NONE
      REAL*8 FTAU,AGE,Z0,H0,Q0,T0,THE,TH0,Y,ARCCOSH

C FTAU  is defined as the "look-back-time / age-of-universe"
C AGE   is defined as the age of the universe in years
C
C ...T0 "HUBBLE TIME" is in sidereal years...
      T0 = (3.0857D+19/3.15581D+7)/H0
      IF (Q0.EQ.0.5) THEN
        FTAU = 1.0 - (1.0+Z0)**(-1.5)
        AGE = (2.0/3.0)*T0
      END IF
      IF (Q0.EQ.0.0) THEN
        FTAU = Z0/(1.0+Z0)
        AGE = T0
      END IF
      IF (Q0.GT.0.5) THEN
        TH0 = DACOS((1.0-Q0)/Q0)
        THE = DACOS((Z0+DCOS(TH0))/(1.0+Z0))
        FTAU = 1.0 - (THE-DSIN(THE))/(TH0-DSIN(TH0))
        AGE = (TH0-DSIN(TH0))*T0*Q0*((2.0*Q0-1.0)**(-1.5))
      END IF
      IF ((Q0.LT.0.5).AND.(Q0.NE.0.0)) THEN
        Y = (1.0-Q0)/Q0
        TH0 = ARCCOSH(Y,1.0D-15)
        Y = (Z0+Y)/(1.0+Z0)
        THE = ARCCOSH(Y,1.0D-15)
        FTAU = 1.0 - (DSINH(THE)-THE)/(DSINH(TH0)-TH0)
        AGE = (DSINH(TH0)-TH0)*T0*Q0*((1.0-2.0*Q0)**(-1.5))
      END IF

      RETURN
      END


C---------------------------------------------------------------------
      SUBROUTINE LOOKBACK(Z,H,Q)
C displays values of fractional look-back time, age of universe,...
      IMPLICIT NONE
      INTEGER IQ,IH,IZ
      REAL*8 Z(0:20),H(0:20),Q(0:20),Z0,H0,Q0,FTAU,AGE

      print *, '                 '
      print *,'qo=0.5: look-back/age = 1- (1+z)^-1.5, age=Ho^-1 (2/3)'
      print *,'qo=0  : look-back/age = z/(1+z)       , age=Ho^-1'
      print *,'            '

      IF (H(1).EQ.0.0) CALL RESETHQ(H,Q)
      CALL RESETZ(Z)

      WRITE(6,6099)
6099  FORMAT(1X,'  qo     Ho      z       L.B./AGE   ',
     + '   L.B.(yrs)     AGE(yrs)      AGE-L.B.')
6100  FORMAT(1X,F5.2,F7.1,F9.5,4E14.5)
      IQ=1
      DO WHILE (Q(IQ).NE.-9.0)
       Q0=Q(IQ)
       IH=1
       DO WHILE (H(IH).NE.0.0)
        H0=H(IH)  
        IZ=1
        DO WHILE (Z(IZ).NE.0.0)
         Z0=Z(IZ)
         CALL LBTIME(FTAU,AGE,Z0,H0,Q0)
         WRITE(6,6100) Q0,H0,Z0,FTAU,FTAU*AGE,AGE,AGE-(FTAU*AGE)
         IZ=IZ+1
        END DO
        IH=IH+1
       END DO
       IQ=IQ+1
      END DO

      RETURN
      END


C---------------------------------------------------------------------
      SUBROUTINE GALACTIC
C This subroutine converts equatorial coordinates to galactic and vice versa
C  T.Barlow_Aug1988
C
C Modified Sept. 1998 to include ecliptic coordinate conversion.
C

      IMPLICIT NONE
      INTEGER I,MODE
      INTEGER RH(0:20),RM(0:20),DD(0:20),DM(0:20)
      REAL*8 RS(0:20),DS(0:20),GLO(0:20),GLA(0:20)
      REAL*8 RA,DEC,SD,DUM
      CHARACTER*1 CSD

      RA=0.0
      DEC=0.0
 
      print *,' '
      print *,' (1) Equatorial-to-Galactic.'
      print *,' (2) Galactic-to-Equatorial.'
      print *,' (3) Equatorial-to-Ecliptic.'
      print *,' (4) Ecliptic-to-Equatorial.'
      WRITE(6,6000) 
6000  FORMAT(1X,'      Choose : ',$)
      READ(5,*) MODE

C ----------------start of mode=1---------------------
      IF (MODE.EQ.1) THEN
       print *,' Enter "-1,0,0" for array.'
       RH(2)=-1
       WRITE(6,6010)
6010   FORMAT(2X,'R.A.(hr,min,sec) : ',$)
       READ(5,*) RH(1),RM(1),RS(1)
       IF (RH(1).NE.-1) THEN
        WRITE(6,6012)
6012    FORMAT(2X,'DEC(deg,min,sec) : ',$)
        READ(5,*) DD(1),DM(1),DS(1)
       END IF
       IF (RH(1).EQ.-1) THEN
        print *,'Enter "-1,0,0" to stop...'
        RH(0)=0
        I=0
        DO WHILE(RH(I).NE.-1)
         I=I+1
         WRITE(6,6015)
6015     FORMAT(2X,'R.A.(hr,min,sec) : ',$)
         READ(5,*) RH(I),RM(I),RS(I)
         IF (RH(I).NE.-1) THEN
          WRITE(6,6020)
6020      FORMAT(2X,'DEC(deg,min,sec) : ',$)
          READ(5,*) DD(I),DM(I),DS(I)
         END IF
        END DO
       END IF
C
C Now call the subroutines for each array element and display the info...
       print *,'   '
       print *,'           .......B1950......'
       print *,'     RIGHT                          GALACTIC'
       print *,'   ASCENSION     DECLINATION   LONGITUDE  LATITUDE'
       print *,'   '
       I=1
       DO WHILE(RH(I).NE.-1)
        CALL HMS2DECI(DEC,DD(I),DM(I),DS(I),SD)
        CALL HMS2DECI(RA,RH(I),RM(I),RS(I),DUM)
C give it RA in decimal degrees
        RA=360.0*RA/24.0
C
        CALL equtogal(RA,DEC,GLO(I),GLA(I))
C
C       CALL EQU2GAL(RA,DEC,GLO(I),GLA(I))
C
        CSD=' '
        IF (SD.LT.0.0) CSD='-'
        WRITE(6,6030) RH(I),RM(I),RS(I),CSD,IABS(DD(I)),
     +    IABS(DM(I)),DABS(DS(I)),GLO(I),GLA(I)
6030    FORMAT(2X,2I3,F6.2,2X,A1,2I3,F6.2,F12.4,F10.4)
        I=I+1
       END DO
      END IF
C -----------end of mode=1----------------------

C -----------start of mode=2----------------------
      IF (MODE.EQ.2) THEN
       print *,' Enter "-1,0" for array.'
       GLO(2)=-1.0
       WRITE(6,6040)
6040   FORMAT(2X,'Galactic longitude, latitude : ',$)
       READ(5,*) GLO(1),GLA(1)
       IF (GLO(1).EQ.-1) THEN
        print *,'Enter "-1,0" to stop...'
        GLO(0)=0.0
        I=0
        DO WHILE(GLO(I).NE.-1)
         I=I+1
         WRITE(6,6045)
6045     FORMAT(2X,'Galactic longitude, latitude : ',$)
         READ(5,*) GLO(I),GLA(I)
        END DO
       END IF
C
C Now call the subroutines for each array element and display the info...
       print *,'   '
       print *,'           .......B1950......'
       print *,'     RIGHT                         GALACTIC'
       print *,'   ASCENSION    DECLINATION   LONGITUDE  LATITUDE'
       print *,'   '
       I=1
       DO WHILE(GLO(I).NE.-1)
C
C       CALL GAL2EQU(GLO(I),GLA(I),RA,DEC)
C
        call galtoequ(GLO(I),GLA(I),RA,DEC)
C
        RA=12.0*RA/180.0
        CALL DECI2HMS(RA,RH(I),RM(I),RS(I),SD)
        CALL DECI2HMS(DEC,DD(I),DM(I),DS(I),SD)
        CSD=' '
        IF (SD.LT.0.0) CSD='-'
        WRITE(6,6050) RH(I),RM(I),RS(I),CSD,DD(I),DM(I),DS(I),
     +   GLO(I),GLA(I)
6050    FORMAT(2X,2I3,F6.2,2X,A1,2I3,F6.2,F12.4,F10.4)
        I=I+1
       END DO
      END IF
C -----------end of mode=2----------------------

C ----------------start of mode=3---------------------
      IF (MODE.EQ.3) THEN
       print *,' Enter "-1,0,0" for array.'
       RH(2)=-1
       print '(a,$)','R.A.(hr,min,sec) : '
       READ(5,*) RH(1),RM(1),RS(1)
       IF (RH(1).NE.-1) THEN
        print '(a,$)','DEC(deg,min,sec) : '
        READ(5,*) DD(1),DM(1),DS(1)
       END IF
       IF (RH(1).EQ.-1) THEN
        print *,'Enter "-1,0,0" to stop...'
        RH(0)=0
        I=0
        DO WHILE(RH(I).NE.-1)
         I=I+1
         print '(a,$)','R.A.(hr,min,sec) : '
         READ(5,*) RH(I),RM(I),RS(I)
         IF (RH(I).NE.-1) THEN
          print '(a,$)','DEC(deg,min,sec) : '
          READ(5,*) DD(I),DM(I),DS(I)
         END IF
        END DO
       END IF
C
C Now call the subroutines for each array element and display the info...
       print *,'   '
       print *,'           .......J2000......'
       print *,'     RIGHT                          ECLIPTIC'
       print *,'   ASCENSION     DECLINATION   LONGITUDE  LATITUDE'
       print *,'   '
       I=1
       DO WHILE(RH(I).NE.-1)
        CALL HMS2DECI(DEC,DD(I),DM(I),DS(I),SD)
        CALL HMS2DECI(RA,RH(I),RM(I),RS(I),DUM)
C give it RA in decimal degrees
        RA=360.0*RA/24.0
C
        call equtoecl(RA,DEC,GLO(I),GLA(I))
C
        CSD=' '
        IF (SD.LT.0.0) CSD='-'
        WRITE(6,'(2X,2I3,F6.2,2X,A1,2I3,F6.2,F12.4,F10.4)') 
     .    RH(I),RM(I),RS(I),CSD,IABS(DD(I)),
     .    IABS(DM(I)),DABS(DS(I)),GLO(I),GLA(I)
        I=I+1
       END DO
      END IF
C -----------end of mode=3----------------------


C -----------start of mode=4----------------------
      IF (MODE.EQ.4) THEN
       print *,' Enter "-1,0" for array.'
       GLO(2)=-1.0
       print '(a,$)','Ecliptic longitude, latitude : '
       READ(5,*) GLO(1),GLA(1)
       IF (GLO(1).EQ.-1) THEN
        print *,'Enter "-1,0" to stop...'
        GLO(0)=0.0
        I=0
        DO WHILE(GLO(I).NE.-1)
         I=I+1
         print '(a,$)','Ecliptic longitude, latitude : '
         READ(5,*) GLO(I),GLA(I)
        END DO
       END IF
C
C Now call the subroutines for each array element and display the info...
       print *,'   '
       print *,'           .......J2000......'
       print *,'     RIGHT                         ECLIPTIC'
       print *,'   ASCENSION    DECLINATION   LONGITUDE  LATITUDE'
       print *,'   '
       I=1
       DO WHILE(GLO(I).NE.-1)
C
        call ecltoequ(GLO(I),GLA(I),RA,DEC)
C
        RA=12.0*RA/180.0
        CALL DECI2HMS(RA,RH(I),RM(I),RS(I),SD)
        CALL DECI2HMS(DEC,DD(I),DM(I),DS(I),SD)
        CSD=' '
        IF (SD.LT.0.0) CSD='-'
        WRITE(6,'(2X,2I3,F6.2,2X,A1,2I3,F6.2,F12.4,F10.4)') 
     .     RH(I),RM(I),RS(I),CSD,DD(I),DM(I),DS(I),GLO(I),GLA(I)
        I=I+1
       END DO
      END IF
C -----------end of mode=4----------------------

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE EQU2GAL(ALPHA,DELTA,XL,XB)
C-----------------------------------------------------------------------
C Steven E. Bradley   6 Jun 1984    ...........modified T.Barlow Aug1988
C
C Converts equatorial coordinates ALPHA and DELTA to galactic
C coordinates XL,XB according to the following formulas:
C (P.A.S.P. 91:405 June 1979, Adair P. Lane)
C
C SIN(XB)           = SIN(DELTA)*COS(62.6) - 
C	              COS(DELTA)*SIN(ALPHA-282.25)*SIN(62.6)
C
C TAN(XL-33)        = TAN(ALPHA-282.25)*COS(62.6) +
C                     TAN(DELTA)*SIN(62.6)/COS(ALPHA-282.25)
C
C where the literal numbers are in degrees.
C
C Where:
C
C 	ALPHA -> real*8 right ascention in degrees
C       DELTA -> real*8 declination in degrees
C          XL <- real*8 galactic longitude (IAU definition) in degrees
C          XB <- real*8 galactic latitude (IAU definition) in degrees
C
C-----------------------------------------------------------------------
C
        IMPLICIT NONE
	REAL*8  ALPHA,DELTA,XL,XB
        REAL*8  PI,CV,C62,S62,A,A1,A2

	PI = 3.14159265358979D0
	CV = 360.0 / 2.0 / PI

	C62 = DCOS(62.6/CV)
	S62 = DSIN(62.6/CV)
C
C                                    latitude
C
	A  = DSIN(DELTA/CV)*C62-DCOS(DELTA/CV)*
     +        DSIN((ALPHA-282.25)/CV)*S62
	XB = CV * DASIN(A)
C
C                                    longitude
C
	A  = DTAN(DELTA/CV)*S62/DCOS((ALPHA-282.25)/CV)
	A  = DTAN((ALPHA-282.25)/CV)*C62 + A
	A1 = CV * DATAN(A) 
	A2 = A1 + 180.		! ambiguity to be resolved

	IF (DABS(DCOS(XB/CV)*DCOS(A1/CV) - 
     1      DCOS(DELTA/CV)*DCOS((ALPHA-282.25)/CV)) .LE. 1.D-4) THEN
	   A = A1 + 33.0
	ELSE IF (DABS(DCOS(XB/CV)*DSIN(A2/CV) - DCOS(DELTA/CV)*
     1           DSIN((ALPHA-282.25)/CV)*C62 - DSIN(DELTA/CV)*S62) .LE.
     2           1.D-4) THEN
	   A = A2 + 33.0
	ELSE
	   A = 0.0
	   WRITE (5,*) '***ERROR #1 IN EQU2GAL***'
	END IF

	IF (A .LT. 0.0) THEN
	   XL = A + 360.0
	ELSE IF (A .GT. 360.0) THEN
	   XL = A - 360.0
	ELSE
	   XL = A
	END IF

	RETURN
	END

C-----------------------------------------------------------------------
	SUBROUTINE GAL2EQU(XL,XB,ALPHA,DELTA)
C-----------------------------------------------------------------------
C Steven E. Bradley   6 June 1984         ......modified T.Barlow Aug1988
C
C Converts galactic coordinates XL and XB to equatorial coordinates 
C ALPHA and DELTA according to the following formulas:
C (P.A.S.P. 91:405 June 1979, Adair P. Lane)
C
C
C SIN(DELTA)        = COS(XB)*SIN(XL-33)*SIN(62.6) +
C                     SIN(XB)*COS(62.6)
C
C SIN(ALPHA-282.25) = TAN(DELTA)*
C                     {COS(XB)*SIN(L-33)*COS(62.6) - SIN(XB)*SIN(62.6)}/
C                     {COS(XB)*SIN(L-33)*SIN(62.6) + SIN(XB)*COS(62.6)}
C
C
C where the literal numbers are in degrees.
C
C Where:
C
C          XL -> real*8 galactic longitude (IAU definition) in degrees
C          XB -> real*8 galactic latitude (IAU definition) in degrees
C 	ALPHA <- real*8 right ascention in degrees
C       DELTA <- real*8 declination in degrees
C
C-----------------------------------------------------------------------
C
        IMPLICIT NONE
	REAL*8  ALPHA,DELTA,XL,XB,PI,CV,C62,S62,A,A1,A2

	PI = 3.14159265358979D0
	CV = 360.0 / 2.0 / PI

	C62 = DCOS(62.6/CV)
	S62 = DSIN(62.6/CV)
C
C                                    declination
C
	A     = DCOS(XB/CV)*DSIN((XL-33.0)/CV)*S62 + DSIN(XB/CV)*C62
	DELTA = CV * DASIN(A)
C
C                                    right ascention
C
	A     = DCOS(XB/CV)*DSIN((XL-33.0)/CV)*C62 - DSIN(XB/CV)*S62
	A     = A / DCOS(DELTA/CV)
	A1    = CV * DASIN(A) 
	A2    = 180.0 - A1		! ambiguity

	IF (DABS(DCOS(XB/CV)*DCOS((XL-33.0)/CV) - 
     1      DCOS(DELTA/CV)*DCOS(A1/CV)) .LE. 1.D-4) THEN
	   A = A1 + 282.25
	ELSE IF (DABS(DCOS(XB/CV)*DSIN((XL-33.0)/CV) - DCOS(DELTA/CV)*
     1           DSIN(A2/CV)*C62 - DSIN(DELTA/CV)*S62) .LE.
     2           1.D-4) THEN
	   A = A2 + 282.25
	ELSE
	   A = 0.0
	   WRITE (5,*) '***ERROR #1 IN GAL2EQU***'
	END IF
	
	IF (A .LT. 0.0) THEN
	   ALPHA = A + 360.0
	ELSE IF (A .GT. 360.0) THEN
	   ALPHA = A - 360.0
	ELSE
	   ALPHA = A
	END IF

	RETURN
	END



C---------------------------------------------------------------------
      SUBROUTINE DECI2HMS(D,H,M,S,THESIGN)
C This subroutine converts decimal hours (or degrees) to hours, minutes,
C  and seconds. Precision = REAL*8. H and M are INTEGERS.
C
C WARNING: the outputted quantities H,M, and S are always positive,
C           the sign is given in THESIGN.
C 
      IMPLICIT NONE
      REAL*8 D,S,THESIGN,SAVED
      INTEGER H,M

      SAVED=D
      IF (D.EQ.0.0) THESIGN=1.0
      IF (D.NE.0.0) THESIGN=D/DABS(D)
      D=DABS(D)
      H=INT(D)
      D=D-DFLOAT(H)
      M=INT(D*60.0)
      D=D-(DFLOAT(M)/60.0)
      S=D*3600.0
      D=SAVED
  
      RETURN
      END


C---------------------------------------------------------------------
      SUBROUTINE HMS2DECI(D,H,M,S,THESIGN)
C This subroutine converts hours (or degrees), minutes, and seconds to
C  a decimal hours (or degrees).  Percision = REAL*8. H and M are INTEGERS.
C 
C IMPORTANT NOTE: If any of the input quantities H,M, or S are negative, then
C                  D and THESIGN will be negative.
C
      IMPLICIT NONE
      REAL*8 D,S,THESIGN
      INTEGER H,M

      THESIGN=1.0
      IF ((H.LT.0).OR.(M.LT.0).OR.(S.LT.0.0)) THESIGN=-1.0
      D=DFLOAT(IABS(H))+DFLOAT(IABS(M))/60.0+DABS(S)/3600.0
      D=D*THESIGN
     
      RETURN
      END



C---------------------------------------------------------------------
      SUBROUTINE DISPSKY
C This subroutine controls and displays information relating to the
C  subroutine sky which calculates zenith angles, azimuth, airmass,
C  differential refraction, position angle, etc... given the hour
C  angle and declination.
C
      IMPLICIT NONE
      INTEGER I,J,K,H,M,MODE
      REAL*8 HA,DEC,LAT,AM,AZI,ZT,ZA,PA,DR(0:20),AT(0:20),WV(0:20)
      REAL*8 W1,W2,N1,N2,S,PI,THESIGN,REF,R,RA,LONG
      REAL*8 RARAD,DECRAD,UT,JD,HJD,LST,HARAD,ZRAD,AIRMAS,V
      INTEGER MONTH
      REAL*8 DAY,YEAR
      real*8 va,vl,vd
      real*8 tandd

      PI=(3.14159265358979D0)
      print *,'    '
      WRITE(6,6000) 
6000  FORMAT(1X,'By Hour Angle(0) or Universal Time(1,yields JD) : ',$)
      READ(5,*) MODE

C select latitude, longitude, RA, and DEC.
      K=0
      IF (MODE.EQ.0) K=5                     ! NO RA OR LONG
      CALL LATLONG(LAT,LONG,RA,DEC,K)
      DECRAD=PI*DEC/180.0

      IF (MODE.EQ.0) THEN
        WRITE(6,6015) 
6015    FORMAT(1X,'Hour Angle   (hours,minutes,seconds)  : ',$)
        READ(5,*) H,M,S
        CALL HMS2DECI(HA,H,M,S,THESIGN)
        print *,'     '
      ELSE
        RARAD=PI*(RA*15.D0)/180.D0
        WRITE(6,6030)
6030    FORMAT(1X,'Universal Time (year,month,day,hours) : ',$)
        READ(5,*)  YEAR,MONTH,DAY,UT
   
        CALL AST_HELJD(RARAD,DECRAD,MONTH,DAY,YEAR,UT,JD,HJD,LST,
     +            HARAD,ZRAD,AIRMAS,V,LAT,LONG)
        LST=12.0*LST/PI
        HA = LST - RA
        IF (HA.GT.12.) HA = HA - 24.0
        IF (HA.LT.-12.) HA = HA + 24.0
        print *,'      '
        print *,'                Julian date : ',JD
C HJD in this calculation does not appear accurate.  tab - 29 August 1992
C       print *,' Returned heliocentric J.D. : ',HJD
        print *,'Local sidereal time (hours) : ',LST
        print *,'         Hour angle (hours) : ',HA
        CALL DECI2HMS(LST,H,M,S,THESIGN)
        WRITE(6,6031) H,M,S
6031    FORMAT(1X,'Local Sidereal Time = ',I2,':',I2,':',F4.1)
        CALL DECI2HMS(HA,H,M,S,THESIGN)
        IF (THESIGN.GT.0.0) WRITE(6,6032) H,M,S
6032    FORMAT(1X,'HOUR ANGLE =  +',I2,':',I2,':',F4.1)
        IF (THESIGN.LT.0.0) WRITE(6,6033) H,M,S
6033    FORMAT(1X,'HOUR ANGLE =  -',I2,':',I2,':',F4.1)
        print *,'      '
        print '(a,f12.6)','Heliocentric velocity correction (km/s) : ',v
        call getheliostuff(va,vl,vd)
        print '(a,3f12.6)','vannual,vlunar,vdiurnal=',va,vl,vd
        print *,'      '

      END IF
    
      CALL SKY(HA,DEC,LAT,AM,AZI,ZT,ZA,PA,DR,AT,WV)
   
      REF=(ZT-ZA)*3600.0
      IF (DABS(REF).GT.999.0) REF=999.0
      print *,'             (N=0,E=90)        ZENITH ANGL',
     +  'E         POSITION'
      print *,'  AIR MASS    AZIMUTH       TRUE       APP',
     +  'ARENT       ANGLE    TUB  REFRACT(")'
      WRITE(6,6050) AM,AZI,ZT,ZA,PA,(PA+180.0),REF
6050  FORMAT(1X,F10.6,F13.7,F13.7,F12.7,F12.4,F7.1,F10.4)
      print *,'    '
      WRITE(6,6060) (WV(J),J=1,9)
6060  FORMAT(1X,'WAVE :',9F8.1)
      WRITE(6,6070) (AT(J),J=1,9)
6070  FORMAT(1X,'TRANS:',9F8.4)
      WRITE(6,6080) (DR(J),J=1,9)
6080  FORMAT(1X,'D.REF:',9F8.3)
      print *,'   '

C Now give the user an opportunity to calculate more exact differential
C  refraction between to wavelengths. Here I have included both the CRC
C  and Allen's formula and included a 2km elevation corrected value.
      WRITE(6,6100) 
6100  FORMAT(1X,'Differential refraction between 2 wavelengths?',
     +  ' (1/0) : ',$)
      READ(5,*) I
      IF (I.NE.0) THEN
50      WRITE(6,6110)
6110    FORMAT(1X,'Wavelength #1 : ',$)
        READ(5,*) W1
        WRITE(6,6115)
6115    FORMAT(1X,'Wavelength #2 : ',$)
        READ(5,*) W2
        IF ((W1.LE.0.0).OR.(W2.LE.0.0)) GOTO 50
C formula originating in CRC Handbook...
        N1=2726.43+(12.288/(W1*W1*1.0D-8))+
     .                 (0.3555/(W1*W1*W1*W1*1.0D-16))
        N1=N1*(1.0D-7)
        N2=2726.43+(12.288/(W2*W2*1.0D-8))+
     .                 (0.3555/(W2*W2*W2*W2*1.0D-16))
        N2=N2*(1.0D-7)
        R = (2.06265D+5)*(N1-N2)*tandd(ZA)
        print *,'(za<50 deg,760mm Hg,T=15C,dry,CRC) separation("): ',R
C formula from Allen... wavelengths in microns...
        R=(1.0D4/W1)
        N1=64.328+(29498.1/(146.0-R*R))+(255.4/(41.0-R*R))
        N1=N1/(1.0D+6)
        R=(1.0D4/W2)
        N2=64.328+(29498.1/(146.0-R*R))+(255.4/(41.0-R*R))
        N2=N2/(1.0D+6)
        R = (2.06265D+5)*(N1-N2)*tandd(ZA)
        print *,'(za<50 deg,760mm Hg,T=15C,dry,Allen) separation("): ',R
C adjustments for 2km,P=600,T=7,f=8... (LAT=+/-30)
        R=(1.0D4/W1)
        N1=64.328+(29498.1/(146.0-R*R))+(255.4/(41.0-R*R))
        N1=N1*(600.0*(1.0+(1.049-0.0157*7.0)*600.0*1.0D-6))
        N1=N1/(720.883*(1.0+0.003661*7.0))
        N1=N1-8.0*(0.0624-0.000680*R*R)/(1.0+0.003661*7.0)
        N1=N1/(1.0D+6)
        R=(1.0D4/W2)
        N2=64.328+(29498.1/(146.0-R*R))+(255.4/(41.0-R*R))
        N2=N2*(600.0*(1.0+(1.049-0.0157*7.0)*600.0*1.0D-6))
        N2=N2/(720.883*(1.0+0.003661*7.0))
        N2=N2-8.0*(0.0624-0.000680*R*R)/(1.0+0.003661*7.0)
        N2=N2/(1.0D+6)
        R = (2.06265D+5)*(N1-N2)*tandd(ZA)
        print *,'(2km,lat=+/-30,600mm Hg,T=7C,H20=8mm Hg, Allen) : ',R
      END IF
     
      RETURN
      END


C----------------------------------------------------------------------
      subroutine getheliostuff(va,vl,vd)
      implicit none
      include 'helio.inc'
      real*8 va,vl,vd
      va=vannual
      vl=vlunar
      vd=vdiurnal
      return
      end


C-----------------------------------------------------------------------
      SUBROUTINE SKY(HA,DEC,LAT,AM,AZI,ZT,ZA,PA,DR,AT,WV)
C This subroutine calculates several quantities given the hour angle,
C  declination and latitude.
C PARAMETERS:
C HA : (decimal) Hour Angle  (0h at meridian, negative in east)
C DEC: (decimal) Declination 
C LAT: (decimal) Latitude    
C AM : AirMass  (1 at zenith)   
C AZI: Azimuth  (0 degrees due north, 90 deg. due east, ...)
C ZT : Zenith angle (True)  (0 degrees at zenith)
C ZA : Zenith angle (Apparent)  (0 degrees at zenith)
C PA : Position Angle  (0 deg. red end toward due south, 90 deg due west,...)
C DR(0:20) : Differential Refraction (func. of wavelength)
C AT(0:20) : Atmospheric Transmission (func. of wavelength)
C WV(0:20) : Corresponding wavelengths in angstroms
       
      IMPLICIT NONE
      REAL*8 HA,DEC,LAT,AM,AZI,ZT,ZA,PA,DR(0:20),AT(0:20),WV(0:20)
      REAL*8 EARTHRAD,ATMTHICK,R,XP,YP,ZP,X,Y,Z,HADEG
      REAL*8 OLDZA,ZARAD,DIFFERR,N,N5,PI,A,B,C
      INTEGER*4 I
      real*8 sindd,cosdd,atandd,acosdd,tandd
    
      PI=(3.14159265358979D0)

C wavelength array; UV atmospheric cutoff to 10 microns.
      WV(0)=3200.0D0
      WV(1)=3500.0D0
      WV(2)=4000.0D0
      WV(3)=4500.0D0
      WV(4)=5000.0D0
      WV(5)=5500.0D0
      WV(6)=6000.0D0
      WV(7)=6500.0D0
      WV(8)=7000.0D0
      WV(9)=8000.0D0
      WV(10)=9000.0D0
      WV(11)=10000.0D0
      WV(12)=12000.0D0
      WV(13)=15000.0D0
      WV(14)=20000.0D0
      WV(15)=50000.0D0
      WV(16)=100000.0D0

C rough atmospheric transmission (Lick, 1 airmass, in sync with wavelengths).
C most values derived indirectly from HAYES DS et al ApJ 197:593 (1975).
      AT(0)=0.3685D0    ! 3200 angstroms
      AT(1)=0.5420D0    ! 3500
      AT(2)=0.6862D0    ! 4000
      AT(3)=0.7785D0    ! 4500
      AT(4)=0.8295D0    ! 5000
      AT(5)=0.8480D0    ! 5500
      AT(6)=0.8618D0    ! 6000
      AT(7)=0.8967D0    ! 6500
      AT(8)=0.9160D0    ! 7000
      AT(9)=0.9300D0    ! 8000
      AT(10)=0.9441D0   ! 9000
      AT(11)=0.9420D0   ! 10000
C no accurate values beyond 10,000 angstroms.
      AT(12)=0.95D0     ! 12000
      AT(13)=0.95D0     ! 15000
      AT(14)=0.95D0     ! 20000
      AT(15)=0.95D0     ! 50000
      AT(16)=0.95D0     ! 100000

C Now calculate AZImuth and Zenith angle (True)...
      HADEG=90.0*HA/6.0
      ZP=sindd(DEC)
      XP=cosdd(DEC)*sindd(HADEG)
      YP=cosdd(DEC)*cosdd(HADEG)
      X=XP
      Z=ZP*sindd(LAT)+YP*cosdd(LAT)
      Y=YP*sindd(LAT)-ZP*cosdd(LAT)
      IF (Z.NE.0.0) THEN
        ZT=atandd(DSQRT(X*X+Y*Y)/Z)
      ELSE
        ZT=90.0
      END IF
      IF (Y.NE.0.0) THEN
       AZI=atandd(X/Y)
      ELSE IF (X.GT.0.0) THEN
       AZI=270.0
      ELSE IF (X.LT.0.0) THEN
       AZI=90.0
      END IF
      IF (Y.GT.0.0) AZI=180.0+AZI
      IF (AZI.LT.0.0) AZI=360.0+AZI
    
C Now calculate position angle... (0 deg.=red end toward south, 90deg.=west)
C Need to use x,y,z alt-azimuth euclidean coordinates. and the spherical
C triangle rule: cos(a)=cos(b)cos(c)+sin(b)sin(c)cos(ANGLE) where ANGLE is
C opposite "side" a.
      A=90.0-LAT
      B=acosdd(Y*(-1)*cosdd(LAT)+Z*sindd(LAT))
      C=acosdd(Z)
      IF ((sindd(B).EQ.0.0).OR.(sindd(C).EQ.0.0)) THEN
       PA=999.0
      ELSE
       R=(cosdd(A)-cosdd(B)*cosdd(C))/(sindd(B)*sindd(C))
C adjust for possible numerical(machine) problems...
       IF (DABS(R).GT.1.0) THEN
        PA=999.0
        IF ((R.GE.1.0).AND.(R.LT.1.001)) PA=0.0
        IF ((R.LE.-1.0).AND.(R.GT.-1.001)) PA=180.0
       ELSE
        PA=acosdd(R)
       END IF
       IF (HA.LT.0.0) PA=(-1.0)*PA
      END IF

C Now calculate the Zenith angle (Apparent) at about 4000.0 angstroms...
C ...unless the angle is too large to do an accurate calculation.
C (pressure=760 mm Hg, T=15 deg Celsius, dry air) from CRC and other sources
      IF (ZT.GT.80.0) THEN
       ZA=999.0
      ELSE
C Iterate general equation to an accuracy greater than 0.0001 arc-seconds;
C R=ZT-ZA in arc-seconds; guess first that ZA=ZT...
       ZA=ZT
       I=0
       DIFFERR=(1.0D+0)
       DO WHILE ((I.LT.40).AND.(DIFFERR.GT.1.0D-10))
        I=I+1
        OLDZA=ZA
        R=tandd(ZA)
        R=(5.816D+1)*R-(6.7D-2)*R*R*R
        ZA=ZT-(R/3600.0D0)
        DIFFERR=DABS(OLDZA-ZA)
       END DO
      END IF
  
C Now calculate airmass...
C The Earth's average radius is about 6370 km.  The Atmosphere's thickness is
C  about 11.6 km. (level of tropopause at about 41 degrees latitude)
      EARTHRAD=(6370.0D0)
      ATMTHICK=(11.6D0)
      R=EARTHRAD/ATMTHICK
C This formula is grossly inaccurate for zenith angles greater than 80.
C Use the apparent zenith angle.
      IF (ZA.GT.80.0) THEN
       AM=99.0
      ELSE
       ZARAD=PI*(ZA/180.0)
       AM=R*DCOS(PI-ZARAD)+DSQRT(((R*DCOS(PI-ZARAD))**2.0)+1.0+2.0*R)
      END IF

C Now calculate atmospheric transmission...
      DO I=0,16
       AT(I)=(AT(I)**(AM))
      END DO

C Now calculate differential refraction relative to 5000 angstroms in arcsecs.
C (pressure=760 mm Hg, T=15 deg Celsius, dry air) from CRC and other sources
      X = 5000.0
      N5= 2726.43+(12.288/(X*X*1.0D-8))+(0.3555/(X*X*X*X*1.0D-16))
      N5= N5*(1.0D-7)
      DO I=0,16
       IF (ZA.GT.67.0) THEN
        DR(I)=99.0
       ELSE
        X = WV(I)
        N = 2726.43+(12.288/(X*X*1.0D-8))+(0.3555/(X*X*X*X*1.0D-16))
        N = N*(1.0D-7)
        DR(I) = (2.06265D+5)*(N-N5)*tandd(ZA)
       END IF
       DR(I) = MIN(99.0,DR(I))
      END DO
      
      RETURN
      END
C - - - - - - - - END OF SKY - - - - - - - -


C-------------------------------------------------------------------------
      SUBROUTINE TABLES
C This subroutine is intended to output tables of values (to a disk file)
C   related to a particular lattitude. The table is declination vs. hour angle.
C The quantities are: zenith angle, azimuthal direction, tangent of zenith
C   angle, secant of zenith angle, air mass, and position angle.
C It uses the subroutine SKY.
C
      IMPLICIT NONE
      REAL*8 HA,DEC,LAT,AM,AZI,ZT,ZA,PA,DR(0:20),AT(0:20),WV(0:20)
      REAL*8 QUANT,DSTART,DSTOP,DINC,HASTART,HASTOP,HAINC
      REAL*8 HAS(1:30),DECS(1:200),Q(1:30),LONG,RA
      INTEGER*4 I,J,K,NHAS,NDECS,lc
      CHARACTER*40 OUTFIL 
      character c200*200,c3*3
      real*8 tandd,cosdd

C select latitude.
      K=4
      CALL LATLONG(LAT,LONG,RA,DEC,K)

C next choose quantity...
      print *,'1=ZENITH ANGLE, 2=AZIMUTH, 3=TAN(Z), 4=SEC(Z), 5=',
     +       'AIR MASS, 6=POSITION ANGLE'
      WRITE(6,6010)
6010  FORMAT(1X,'Choose a quantity : ',$)
      READ(5,*) QUANT
      print *,'   '

C next choose ranges and increments of DEC and HA...
      WRITE(6,6020)
6020  FORMAT(1X,'DECLINATION (start, stop, increment ) : ',$)
      READ(5,*) DSTART,DSTOP,DINC
      print *,'Note: there is a maximum of 18(x7) hour angle values.'
      WRITE(6,6030)
6030  FORMAT(1X,' HOUR ANGLE (start, stop, increment ) : ',$)
      READ(5,*) HASTART,HASTOP,HAINC

C now produce table...
      WRITE(6,6040)
6040  FORMAT(1X,'Output filename : ',$)
      READ(5,5000) OUTFIL
5000  FORMAT(A40)
      OPEN(UNIT=1,FILE=OUTFIL,STATUS='unknown')
      GOTO (11,12,13,14,15,16),QUANT
11     WRITE(1,1001) LAT
1001   FORMAT(1X,'     Zenith angle for latitude=',F11.6)
       GOTO 20
12     WRITE(1,1002) LAT
1002   FORMAT(1X,'     Azimuthal angle for latitude=',F11.6)
       GOTO 20
13     WRITE(1,1003) LAT
1003   FORMAT(1X,'     Tangent of the zenith angle for latitude=',F11.6)
       GOTO 20
14     WRITE(1,1004) LAT
1004   FORMAT(1X,'     Secant of the zenith angle for latitude=',F11.6)
       GOTO 20
15     WRITE(1,1005) LAT
1005   FORMAT(1X,'     Air mass for for latitude=',F11.6)
       GOTO 20
16     WRITE(1,1006) LAT
1006   FORMAT(1X,'     Position angle for latitude=',F11.6)
20     WRITE(1,1010)
1010   FORMAT(1X,' ') 
       WRITE(1,1020)
1020   FORMAT(1X,' DEC                  HOUR ANGLE -->')
       WRITE(1,1010)
C load values into arrays for convenience and speed...
      K=0
      DO HA=HASTART,HASTOP,HAINC
       K=K+1
       HAS(K)=HA
      END DO
      NHAS=K
      K=0
      DO DEC=DSTART,DSTOP,DINC
       K=K+1
       DECS(K)=DEC
      END DO
      NDECS=K
C write hour angle header....
      WRITE(1,1060) (HAS(J),J=1,NHAS)
1060  FORMAT(4X,F7.2,:,F7.2,:,F7.2,:,F7.2,:,F7.2,:,F7.2,:,F7.2,:,
     +  F7.2,:,F7.2,:,F7.2,:,F7.2,:,F7.2,:,F7.2,:,F7.2,:,F7.2,:,F7.2,:,
     +  F7.2,:,F7.2)
      WRITE(1,1010)

      DO J=1,NDECS
       DEC=DECS(J)
       DO I=1,NHAS
        HA=HAS(I)
        CALL SKY(HA,DEC,LAT,AM,AZI,ZT,ZA,PA,DR,AT,WV)
        GOTO (31,32,33,34,35,36),QUANT
31      Q(I)=ZT
         GOTO 40
32      Q(I)=AZI
         GOTO 40
33      Q(I)=tandd(ZT)
         GOTO 40
34      Q(I)=(1.0/cosdd(ZT))
         GOTO 40
35      Q(I)=AM
         GOTO 40
36      Q(I)=PA
40      CONTINUE
       END DO
       GOTO (51,51,53,53,53,51),QUANT
51      WRITE(C200,fmt=1101) (Q(I),I=1,NHAS)
        write(c3,'(i3)') nint(dec)
        write(1,'(a,a,2x,a)') c3,c200(1:lc(c200)),c3
        GOTO 60
53      WRITE(C200,fmt=1103) (Q(I),I=1,NHAS)
        write(c3,'(i3)') nint(dec)
        write(1,'(a,a,2x,a)') c3,c200(1:lc(c200)),c3
60     CONTINUE
      END DO
C This format for zenith angle, azimuth, and position angle...
1101  FORMAT(1X,F7.1,:,F7.1,:,F7.1,:,F7.1,:,F7.1,:,F7.1,:,
     +  F7.1,:,F7.1,:,F7.1,:,F7.1,:,F7.1,:,F7.1,:,F7.1,:,F7.1,:,F7.1,:,
     +  F7.1,:,F7.1,:,F7.1)
C This format for tangent,secant, and air mass...
1103  FORMAT(1X,F7.3,:,F7.3,:,F7.3,:,F7.3,:,F7.3,:,F7.3,:,
     +  F7.3,:,F7.3,:,F7.3,:,F7.3,:,F7.3,:,F7.3,:,F7.3,:,F7.3,:,F7.3,:,
     +  F7.3,:,F7.3,:,F7.3)

C write hour angle header...............  AGAIN.
      WRITE(1,1010)
      WRITE(1,1060) (HAS(J),J=1,NHAS)

      CLOSE(UNIT=1)

      RETURN
      END


C---------------------------------------------------------------------
      SUBROUTINE RELTRANS
C This subroutine creates table of various quantities vs. hour angle,
C  such as: airmass, zenith angle, tan(za), sidereal time, best
C  tub angle, transmissions relative to HA=0.0 for several wavelengths.
C
      IMPLICIT NONE
      INTEGER*4 I,J,K
      REAL*8 HA,DEC,LAT,AM,AZI,ZT,ZA,PA,DR(0:20),AT(0:20),WV(0:20)
      REAL*8 PI,R,RA,LONG,HA1,HA2,HAINC,TAM(9)
      REAL*8 RS,DS,THESIGN
      INTEGER*4 STH,STM,TUB,STYLE,RH,RM,DD,DM
      CHARACTER OUTFILE*40, PORM*1
      LOGICAL FILEOUT
      real*8 tandd
C
      PI=(3.14159265358979D0)
      STYLE=0
C
C select latitude, RA, and DEC.
      K=6                                 ! no longitude
      CALL LATLONG(LAT,LONG,RA,DEC,K)

C select hour angles...
      WRITE(6,6025)
6025  FORMAT(1X,'(Decimal) hour angles ( FIRST, LAST, INCREMENT ) : ',$)
      READ(5,*) HA1,HA2,HAINC

C select filename (if any)...
      WRITE(6,6027)
6027  FORMAT(1X,'Enter output filename (or "n" for none) : ',$)
      READ(5,5027) OUTFILE
5027  FORMAT(A40)
      FILEOUT =((OUTFILE(1:1).NE.'n').AND.(OUTFILE(1:1).NE.'N'))
      IF (FILEOUT) THEN
        OPEN(UNIT=2,FILE=OUTFILE,STATUS='unknown')
C no formatter style...
        STYLE=0
      END IF

C write header...
      CALL DECI2HMS(RA,RH,RM,RS,THESIGN)
      CALL DECI2HMS(DEC,DD,DM,DS,THESIGN)
      IF (THESIGN.GE.0.0) PORM='+'
      IF (THESIGN.LT.0.0) PORM='-'
      WRITE(6,6029)
6029  FORMAT(2X)
      WRITE(6,6030)  RH,RM,RS,PORM,DD,DM,DS
6030  FORMAT(1X,'RA= ',I2,':',I2,':',F4.1,
     +     '      DEC= ',A1,I2,' ',I2,''' ',F4.1,'"')
      WRITE(6,6029)
      WRITE(6,6032)
6032  FORMAT(1X,'                          ',
     +          '           Transmission at and relative to HA=0')
      WRITE(6,6035)
6035  FORMAT(1X,'  ST   ZA  TANZ  AIRMS  TU',
     +          'B   HA   350  400  450  500  550  600  650  700  800')
      WRITE(6,6029)

      IF (FILEOUT) THEN
        IF (STYLE.EQ.1) THEN
          WRITE(2,2005)  
          WRITE(2,2010)  
          WRITE(2,2015)  
          WRITE(2,2020)  
          WRITE(2,2025)  
          WRITE(2,2030)
          WRITE(2,2035)  
          WRITE(2,2040)
2005      FORMAT('.lm 5')
2010      FORMAT('.rm 95')  
2015      FORMAT('.m1 0')
2020      FORMAT('.m2 0')  
2025      FORMAT('.m3 0')
2030      FORMAT('.m4 0')
2035      FORMAT('|E')
2040      FORMAT('.vb')
        END IF

        WRITE(2,6029)
        WRITE(2,6030) RH,RM,RS,PORM,DD,DM,DS
        WRITE(2,6029)
        WRITE(2,6032)
        WRITE(2,6035)
        WRITE(2,6029)
      END IF

C first run calculations for HA=0.0...
      HA=0.0
      CALL SKY(HA,DEC,LAT,AM,AZI,ZT,ZA,PA,DR,AT,WV)
      DO I=1,9       ! 350,400,450,500,550,600,650,700,800  (in nm)
        TAM(I)=AT(I)
      END DO

C now do calculations for each hour angle...
      DO HA=HA1,HA2,HAINC

        CALL SKY(HA,DEC,LAT,AM,AZI,ZT,ZA,PA,DR,AT,WV)
        R = RA + HA
        IF (R.LT.0.0)  R = R+24.0
        IF (R.GE.24.0) R = R-24.0
        STH = INT(R)
        STM = NINT((R-DFLOAT(INT(R)))*60.0)
C position angles east (HA<0) are negative.
        TUB = NINT(180.0+PA)

C print out data on screen...
        IF (ABS(HA).LT.0.001) THEN
          WRITE(6,6040) STH,STM,NINT(ZA),tandd(ZA),AM,TUB,HA,
     +      (NINT(TAM(J)*100.0),J=1,9)
6040      FORMAT(1X,I2,':',I2,I4,F6.2,F7.3,I5,F6.1,(9(I5)))
        ELSE
          WRITE(6,6040) STH,STM,NINT(ZA),tandd(ZA),AM,TUB,HA,
     +      (NINT(100.0*AT(J)/TAM(J)),J=1,9)
        END IF

        IF (FILEOUT) THEN
          IF (ABS(HA).LT.0.001) THEN
            WRITE(2,6040) STH,STM,NINT(ZA),tandd(ZA),AM,TUB,HA,
     +        (NINT(TAM(J)*100.0),J=1,9)
          ELSE
            WRITE(2,6040) STH,STM,NINT(ZA),tandd(ZA),AM,TUB,HA,
     +        (NINT(100.0*AT(J)/TAM(J)),J=1,9)
          END IF
          IF (STYLE.EQ.1) WRITE(2,6029)
        END IF

      END DO
      
      IF ((STYLE.EQ.1).AND.FILEOUT) THEN
        WRITE(2,6029)
        WRITE(2,6075)
6075    FORMAT(1X,'Differential atmospheric refraction in arc-seconds ',
     +            '(multiple by TANZ):')
        WRITE(2,6080)
6080    FORMAT(1X,
     +   ' 2--+--+--+--+--|T&|R--+--+--+--+--1--+--+--+--+--|T&|R',
     +   '--+--+--+--+--0--+--+--+--+--|T&|R--+--+--+--+')
        WRITE(2,6082)
6082    FORMAT(1X,
     +      '|T |p                |p           |p       |p        ',
     +      '     |p        |p     |p    |p      |p     |p')
        WRITE(2,6084)
6084    FORMAT(1X,'|R325              350         375     400       ',
     +            '    450      500   550  600    700   900nm')
        WRITE(2,6029)
      END IF

      IF (FILEOUT) CLOSE(UNIT=2)

      RETURN
      END



C-------------------------------------------------------------------------
      SUBROUTINE LATLONG(LAT,LONG,RA,DEC,K)
C This subroutine selects the user desired latitude and longitude.
C If K=0 all quantities are desired, if K=1 no RA, if K=2 no DEC,
C  if K=3 no RA or DEC, if K=4 no RA, DEC, or LONG, if K=5 no RA or LONG,
C  if K=6 no LONG.
C
      IMPLICIT NONE
      REAL*8 LAT,LONG,RA,DEC,S,THESIGN
      INTEGER K,H,M

C 1 degree of latitude=69.41mi(equator),=68.70mi(pole),1min=1.15mi,1sec=101ft.
C [1] UC San Diego     : LAT=+32.901     LONG=117.244 (west)      ELEV~100 m
C [2] Lick Observatory : LAT=+37.340361  LONG=121.6455417(west)   ELEV=1283 m
C [3] KPNO Observatory : LAT=+31.958     LONG=111.595(west)       ELEV=2064 m
C [4] CTIO Obs.(4meter): LAT=-30.16606   LONG=70.81489(west)      ELEV=2210 m
C [5] PALOMAR Obs.     : LAT=+33.3561    LONG=116.8639(west)      ELEV=1706 m
C [6] MAUNA KEA        : LAT=+19.8261    LONG=155.4722(west)      ELEV=4200 m
C     LA CAMPANAS Obs. : LAT=-29.00      LONG=71. (??????)        ELEV=???

C select latitude and longitude.
      LONG=0.0
      WRITE(6,6005)
6005  FORMAT(1X,'Latitude(91=UCSD, 92=Lick, 93=KPNO, 94=CTIO, 95=',
     +  'PALO, 96=MKEA): ',$)
      READ(5,*) LAT
      IF (LAT.EQ.91.0) THEN
        LONG=117.244D0
        LAT = 32.901D0
      ELSE IF (LAT.EQ.92.0) THEN
        LONG=121.6455417D0
        LAT = 37.340361D0
      ELSE IF (LAT.EQ.93.0) THEN
        LONG=111.595D0
        LAT = 31.958D0
      ELSE IF (LAT.EQ.94.0) THEN
         LONG= 70.81489D0
         LAT =-30.16606D0
      ELSE IF (LAT.EQ.95.0) THEN
         LONG=116.8639D0
         LAT = 33.3561D0
      ELSE IF (LAT.EQ.96.0) THEN
         LONG=155.4722D0
         LAT = 19.8261D0
      END IF

      IF ((LONG.EQ.0.0).AND.(K.LT.4)) THEN
       WRITE(6,6007)
6007   FORMAT(1X,'Longitude (degrees, west is positive) : ',$)
       READ(5,*) LONG
      ELSE
       WRITE(6,6008) LAT,LONG
6008   FORMAT(1X,'LATITUDE=',F12.7,'     LONGITUDE=',F13.7)
      END IF

      IF ((K.EQ.0).OR.(K.EQ.2).OR.(K.EQ.6)) THEN   
        WRITE(6,6015)
6015    FORMAT(1X,'Right Ascension (hours,minutes,secs)  : ',$)
        READ(5,*) H,M,S
        CALL HMS2DECI(RA,H,M,S,THESIGN)
      END IF

      IF ((K.LE.1).OR.(K.GE.5)) THEN
        WRITE(6,6020)
6020    FORMAT(1X,'Declination (degrees,minutes,seconds) : ',$)
        READ(5,*) H,M,S
        CALL HMS2DECI(DEC,H,M,S,THESIGN)
      END IF

      RETURN
      END


C-------------------------------------------------------------------------
C                                             MODIFIED: 6-APRIL-1990  BARLOW
      SUBROUTINE LOCATE
C
C Calculates best RA,DEC position for an object, given the millimeter
C  distances to SAO stars, millimeter distances between SAO stars, the
C  accurate coordinates of the SAO stars, and a guess (within 1 degree) of the
C  object position.
C
      IMPLICIT NONE
      REAL*8 A(0:99), D(0:99)     !alpha (from RA) and theta (from DEC)
      REAL*8 DIST(1:99), WT(1:99) !distances to stars from the object & weights
      REAL*8 RDIST(1:99)          !real distance (DIST * AS_PER_MM)
      REAL*8 AS                   !arc-minutes separation function
      REAL*8 DAPM,INCAPM,SCALE    !variation in arcsecs/mm
      INTEGER*4 S(1:99)           !star number

      REAL*8 A1,A2,AINC,D1,D2,DINC,BESTERR,BESTA,BESTD,BESTSCALE
      REAL*8 R,RS,DS,THESIGN,AS_PER_MM,WTSUM,DTOA,DD,MM,AA,E,APM
      INTEGER*4 RH,RM,DDEG,DM,IERR
      INTEGER*4 NDIST,I,J,K
      CHARACTER INFILE*30, CSIGN*1
      real*8 cosdd

      WRITE(6,600)
600   FORMAT(1X,'Enter input file (or "help" for information) : ',$)
      READ(5,FMT='(A30)') INFILE

      IF((INDEX(INFILE,'help').GT.0).OR.(INDEX(INFILE,'HELP').GT.0))THEN
      print *,'    '
      print *,'   Calculates best RA,DEC position for an object, given'
      print *,'the millimeter distances to known stars, between known'
      print *,'stars, accurate coordinates of the known stars, and a'
      print *,'guess within 1 degree for the object position.'
      print *,'Searches first in a 3600" box with 100" increments,'
      print *,'then 500"/10", and finally 50"/1".'
      print *,'    '
      print *,'FORM of INPUT FILE (free format) : '
      print *,' 0 RAhr RAmin RAsec DECdeg DECmin DECsec (user guess)'
      print *,' 1   (coordinates of SAO star #1)'
      print *,' 2   (coordinates of SAO star #2)'
      print *,' 3   ... etc.'
      print *,'-1  0 0 0   0 0 0  (signifies the end of list of stars)'
      print *,' 0  1  mm weight (dist. in mm between object and star 1)'
      print *,' 2  4  mm weight (dist. in mm between star 2 and star 4)'
      print *,' 0  3  mm weight (  "    " "    "     object and star 3)'
      print *,' 0  2  mm weight     ... etc.'
      print *,'-1  0  0   (signifies end of list of millim. distances)'
      print *,'    '
      print *,'   For southern declinations enter the most significant'
      print *,'non-zero unit as a negative number.  Enter higher '
      print *,'weighting factors for measurements made with eyepiece'
      print *,'and smaller distances.'
      print *,'    '
      RETURN
      ENDIF

      OPEN(UNIT=1,FILE=INFILE,STATUS='OLD',IOSTAT=IERR)

      IF (IERR.NE.0) THEN
        print *,'Error opening: ',INFILE
        RETURN
      ENDIF

C Read in coordinates of stars and guess for object.
      K=0
      DO WHILE (K.NE.-1)
        READ(1,*) K,RH,RM,RS,DDEG,DM,DS
        IF (K.LT.0) THEN
          K = -1
        ELSE
          CALL HMS2DECI(R,RH,RM,RS,THESIGN)
          A(K) = 15. * R                        ! convert to degrees
          CALL HMS2DECI(R,DDEG,DM,DS,THESIGN)
          D(K) = 90. - R                        ! convert to theta
        ENDIF
      ENDDO

C Read in distances between things.
      AS_PER_MM = 0.0
      WTSUM = 0.0
      NDIST = 0
      I = 0
      DO WHILE (I.GE.0) 
        READ(1,*) I,J,MM,R
        IF (I.LT.0) THEN
          I=-1
        ELSE
          IF (I.EQ.0) THEN       ! object to star
            NDIST = NDIST + 1
            DIST(NDIST) = MM
            WT(NDIST) = R
            S(NDIST) = J
          ELSE                   ! two stars
            APM = 60.*AS(A(I),D(I),A(J),D(J))/MM
            WRITE(6,602) I,J,APM,R
602         FORMAT(1X,'Stars:',I3,I3,'  yield:',F10.4,'   weight=',F4.1)
            AS_PER_MM = AS_PER_MM + R*(APM)
            WTSUM = WTSUM + R
          ENDIF
        ENDIF
      ENDDO
      IF (WTSUM.EQ.0.0) THEN
        AS_PER_MM = 67.0            ! default for POSS
        print *,'No star-star pairs, using POSS default.'
      ELSE
        AS_PER_MM = AS_PER_MM / WTSUM
      ENDIF
      WRITE(6,603) AS_PER_MM
603   FORMAT(1X,'Using arc-seconds per millimeter of :',F10.4)
      CLOSE(UNIT=1)

      WRITE(6,605)
605   FORMAT(1X,'Do you want to vary the arcsecs/mm (1/0) ? ',$)
      READ(5,*) I
      IF (I.EQ.1) THEN           ! set up scale variations
        WRITE(6,606)
606     FORMAT(1X,'Enter +/- range and increments : ',$)
        READ(5,*) DAPM,INCAPM
        print *,'This will be applied during 50" and 5" searches.'
      ELSE
        DAPM=0.0
        INCAPM=0.1
      ENDIF

C Convert distances in mm to real distances in arc-seconds.
      DO I=1,NDIST
        RDIST(I) = DIST(I) * AS_PER_MM
      ENDDO

C First iteration, 3600" box with 100" increments.
      DTOA = 1.0/cosdd(D(0))    ! Theta units to alpha units
      WRITE(6,610)
610   FORMAT(1X,'Starting 3600" by 3600" w/100" increments search...',$)
      D1 = D(0) - 0.5
      D2 = D(0) + 0.5
      DINC = 100./3600.
      A1 = A(0) - 0.5*DTOA
      A2 = A(0) + 0.5*DTOA
      AINC = DTOA*100./3600.
      BESTERR = 1.0D+30
      DO AA = A1,A2,AINC
        DO DD = D1,D2,DINC
          E = 0
          DO I=1,NDIST
            R = RDIST(I) - 60.*AS(AA,DD,A(S(I)),D(S(I)))
            E = E + WT(I)*R*R
          ENDDO
          IF (E.LT.BESTERR) THEN
            BESTERR = E
            BESTA = AA
            BESTD = DD
          ENDIF
        ENDDO
      ENDDO

C Next iteration, 500" box with 10" increments.
      WRITE(6,620)
620   FORMAT(1X,'Starting 500" by 500" with 10" increments search...',$)
      D1 = BESTD - 250./3600.
      D2 = BESTD + 250./3600.
      DINC = 10./3600.
      A1 = BESTA - DTOA*250./3600.
      A2 = BESTA + DTOA*250./3600.
      AINC = DTOA*10./3600.
      DO AA = A1,A2,AINC
        DO DD = D1,D2,DINC
          E = 0
          DO I=1,NDIST
            R = RDIST(I) - 60.*AS(AA,DD,A(S(I)),D(S(I)))
            E = E + WT(I)*R*R
          ENDDO
          IF (E.LT.BESTERR) THEN
            BESTERR = E
            BESTA = AA
            BESTD = DD
          ENDIF
        ENDDO
      ENDDO

C Next iteration, 50" box with 1" increments.
      WRITE(6,630)
630   FORMAT(1X,'Starting 50" by 50" with 1" increments search...',$)
      D1 = BESTD - 25./3600.
      D2 = BESTD + 25./3600.
      DINC = 1./3600.
      A1 = BESTA - DTOA*25./3600.
      A2 = BESTA + DTOA*25./3600.
      AINC = DTOA*1./3600.
C Start using variable arcsecs/mm
      DO SCALE=(AS_PER_MM-DAPM),(AS_PER_MM+DAPM),INCAPM
      DO I=1,NDIST  ! Convert distances in mm to real distances in arc-seconds.
        RDIST(I)=DIST(I) * SCALE
      ENDDO
      WRITE(6,629) SCALE
629   FORMAT(1X,'   Using scale of :',F10.4,'  ...',$)
      DO AA = A1,A2,AINC
        DO DD = D1,D2,DINC
          E = 0
          DO I=1,NDIST
            R = RDIST(I) - 60.*AS(AA,DD,A(S(I)),D(S(I)))
            E = E + WT(I)*R*R
          ENDDO
          IF (E.LT.BESTERR) THEN
            BESTERR = E
            BESTA = AA
            BESTD = DD
          ENDIF
        ENDDO
      ENDDO
      ENDDO

C Next iteration, 5" box with 0.1" increments.
      WRITE(6,631)
631   FORMAT(1X,'Starting 5" by 5" with 0.1" increments search...',$)
      D1 = BESTD - 2.5/3600.
      D2 = BESTD + 2.5/3600.
      DINC = 0.1/3600.
      A1 = BESTA - DTOA*2.5/3600.
      A2 = BESTA + DTOA*2.5/3600.
      AINC = DTOA*0.1/3600.
C Using variable arcsecs/mm
      DO SCALE=(AS_PER_MM-DAPM),(AS_PER_MM+DAPM),INCAPM
      DO I=1,NDIST
        RDIST(I)=DIST(I) * SCALE
      ENDDO
      WRITE(6,629) SCALE
      DO AA = A1,A2,AINC
        DO DD = D1,D2,DINC
          E = 0
          DO I=1,NDIST
            R = RDIST(I) - 60.*AS(AA,DD,A(S(I)),D(S(I)))
            E = E + WT(I)*R*R
          ENDDO
          IF (E.LT.BESTERR) THEN
            BESTERR = E
            BESTSCALE = SCALE
            BESTA = AA
            BESTD = DD
          ENDIF
        ENDDO
      ENDDO
      ENDDO

C Report final residuals to the user.
      DO I=1,NDIST
        RDIST(I)=DIST(I)*BESTSCALE
      ENDDO
      DO I=1,NDIST                ! find individual residuals
        R = RDIST(I) - 60.*AS(BESTA,BESTD,A(S(I)),D(S(I)))
        WRITE(6,645) S(I),R,WT(I)
645     FORMAT(1X,'Star:',I4,'   Residual =',F9.2,'    Weight =',F5.1)
      ENDDO
      WRITE(6,647) BESTSCALE
647   FORMAT(1X,'Best scaling was :',F10.4)

C Calculate RMS error.
      WTSUM = 0.0       
      DO I=1,NDIST
        WTSUM = WTSUM + WT(I)
      ENDDO
      E = DSQRT(BESTERR/WTSUM)

C Print out RA and DEC in H:M:S D:M:S format.
      DD = 90. - BESTD            ! convert to decimal degrees declination
      AA = BESTA/15.              ! convert back to decimal hours
      CALL DECI2HMS(DD,DDEG,DM,DS,THESIGN)
      CALL DECI2HMS(AA,RH,RM,RS,R)
      IF (THESIGN.LT.0) THEN
        CSIGN = '-'
      ELSE
        CSIGN = '+'
      ENDIF
      print *,'  '
      WRITE(6,650) RH,RM,RS,CSIGN,DDEG,DM,DS,E
650   FORMAT(1X,'RA: ',I2,'h ',I2,'m ',F5.2,'s    DEC: '
     .      ,A1,I2,I3,''' ',F4.1,'"      ERROR:',F9.2,'"')
      print *,CHAR(7)
       
      RETURN
      END

C
C::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

C Additional routines....

C---------------------------------------------------------------------
C Calculates luminosity "lum" (ergs/sec/Hz) of magnitude "v"
C (5550 Ang. observed) at rest wavelength "wave" with spectral index
C "alpha" ( Lnu proport. nu**alpha ) at redshift "ze" with
C "h0" and "q0" cosmology.

      subroutine luminosity(v,wave,alpha,ze,h0,q0,lum)

      implicit none 
      real*8 v,wave,alpha,ze,h0,q0,lum 
      real*8 x,r,fnu,pd,pi
      parameter(pi=3.141592654d0) 

      x  = 1.d0 + ze
      fnu= (10.d0)**((48.64d0 + v)/-2.5) 
      r  = 5550.d0 / x
      fnu= fnu*((r/wave)**alpha)
      r = pd(ze,h0,q0)*3.0857d+24    ! Mpc to centimeters
      lum = 4.d0*pi*r*r*fnu*x
      return
      end 


C----------------------------------------------------------------------
      subroutine sun_moon_tables
      implicit none
      character outfile*80,c9a*9,c9b*9
      integer*4 year,month,day,add_dates,day_inc,i,oun
      real*8 zone,latitude,longitude
      real*8 sunloc(7),sunsid(7),sundat(2)
      real*8 moonloc(2),moonsid(2),moondat(5)
      real*8 dint,epoch,ut
      logical first
      print '(a,$)','Output file (0 for screen) : '
      read(5,'(a)') outfile
      if (outfile(1:1).eq.'0') then
        oun=6
      else
        open(1,file=outfile,status='unknown')
        oun=1
      endif
C Get location.
      call ask_place(latitude,longitude,zone)
      write(oun,'(2x)')
      write(oun,'(a,f9.4,a,f9.4,a,f5.1)')
     .  ' Using longitude=',longitude,'  latitude=',latitude,
     .  '  zone=',zone
      write(oun,'(2x)')
      print '(a,$)',' Enter local month, day, year : '
      read(5,*) month,day,year
      if (year.lt.30) year=year+2000
      if (year.lt.100) year=year+1900
      print '(a,$)','Additional dates : '
      read(5,*) add_dates
      add_dates = max(0,add_dates)
      if (add_dates.gt.0) then
        print '(a,$)','Days increment between dates : '
        read(5,*) day_inc
      endif
      call ast_date_to_epoch(year,month,day,ut,epoch)
      first=.true.
      DO i=1,add_dates+1
      call Sun_and_Moon(year,month,day,zone,latitude,longitude,
     .                  sunloc,sunsid,sundat,moonloc,moonsid,moondat)
      if (first) then
        write(oun,'(2a)') ' All events occur at sealevel after lo',
     .                    'cal noon on the date indicated.'
      endif
      if (year.gt.1999) then
        write(oun,'(60x,i2,a,i2,a,i2)') month,'/',day,'/',(year-2000)
      else
        write(oun,'(60x,i2,a,i2,a,i2)') month,'/',day,'/',(year-1900)
      endif

      if (first) then
        write(oun,'(8(a))')             ' Twilight: ',
     .      ' 1deg  ','12deg  ','18deg  ','midpt  ',
     .      '18deg  ','12deg  ',' 1deg  '
      endif

      write(oun,'(a,7(1x,i2.2,a,i2.2,1x))') '   Local :',
     .  int(sunloc(1)),':',nint((sunloc(1)-dint(sunloc(1)))*60.d0),
     .  int(sunloc(2)),':',nint((sunloc(2)-dint(sunloc(2)))*60.d0),
     .  int(sunloc(3)),':',nint((sunloc(3)-dint(sunloc(3)))*60.d0),
     .  int(sunloc(4)),':',nint((sunloc(4)-dint(sunloc(4)))*60.d0),
     .  int(sunloc(5)),':',nint((sunloc(5)-dint(sunloc(5)))*60.d0),
     .  int(sunloc(6)),':',nint((sunloc(6)-dint(sunloc(6)))*60.d0),
     .  int(sunloc(7)),':',nint((sunloc(7)-dint(sunloc(7)))*60.d0)
      write(oun,'(a,7(1x,i2.2,a,i2.2,1x))') ' Sidereal:',
     .  int(sunsid(1)),':',nint((sunsid(1)-dint(sunsid(1)))*60.d0),
     .  int(sunsid(2)),':',nint((sunsid(2)-dint(sunsid(2)))*60.d0),
     .  int(sunsid(3)),':',nint((sunsid(3)-dint(sunsid(3)))*60.d0),
     .  int(sunsid(4)),':',nint((sunsid(4)-dint(sunsid(4)))*60.d0),
     .  int(sunsid(5)),':',nint((sunsid(5)-dint(sunsid(5)))*60.d0),
     .  int(sunsid(6)),':',nint((sunsid(6)-dint(sunsid(6)))*60.d0),
     .  int(sunsid(7)),':',nint((sunsid(7)-dint(sunsid(7)))*60.d0)

      call write_radec(sundat(1),sundat(2),c9a,2)
      call write_radec(moondat(1),moondat(2),c9b,2)
      write(oun,'(5a,i4,a,f6.2,2a)') 
     .   ' Sun at noon: ',c9a,'.   Moon at midpt: ',c9b,',',
     .   min(100,max(0,nint(moondat(5)))),'%,', moondat(4),char(39),'.'

      write(oun,'(a,8(i2.2,a))')
     .  ' Moonset/rise:  ',
     .   int(moonloc(1)),':',nint((moonloc(1)-dint(moonloc(1)))*60.d0),
     .'/',int(moonloc(2)),':',nint((moonloc(2)-dint(moonloc(2)))*60.d0),
     .' (local)  ',
     .   int(moonsid(1)),':',nint((moonsid(1)-dint(moonsid(1)))*60.d0),
     .'/',int(moonsid(2)),':',nint((moonsid(2)-dint(moonsid(2)))*60.d0),
     .' (sidereal)'

      epoch = epoch + (dfloat(day_inc)/365.25d0)
      call ast_epoch_to_date(epoch,year,month,day,ut)
      first = .false.

      ENDDO
      if (oun.ne.6) close(oun)
      return
      end

C----------------------------------------------------------------------
      subroutine ask_place(latitude,longitude,zone)
      implicit none
      real*8 latitude,longitude,zone
      integer*4 ii
      print '(2a)', '(1=Lick, 2=Mauna Kea, 3=KPNO, 4=CTIO,',
     .              ' 5=Palomar, 6=Pasadena, 9=La Jolla, 0=?)'
      print '(a,$)','   Select location : '
      read(5,*) ii
      if (ii.eq.9) then
        longitude= +117.244d0       ! La Jolla ( UCSD )
        latitude =  +32.901d0
        zone     =   +8.d0
      elseif (ii.eq.1) then
        longitude= +121.6455417d0   ! Lick Observatory ( 3 meter )
        latitude =  +37.340361d0
        zone     =   +8.d0
      elseif (ii.eq.2) then
        longitude= +155.4722d0      ! Mauna Kea
        latitude =  +19.8261d0
        zone     =  +10.d0
      elseif (ii.eq.3) then          
        longitude= +111.595d0       ! KPNO
        latitude =  +31.958d0
        zone     =   +7.d0
      elseif (ii.eq.4) then
        longitude=  +70.81489d0    ! CTIO ( 4 meter )
        latitude =  -30.16606d0
        print *,'Do not know GMT offset for CTIO, guessing +5 hours.'
        zone     =   +5.d0
      else if (ii.eq.5) then
        longitude= +116.8639d0     ! Palomar
        latitude =  +33.3561d0
        zone     =   +8.d0
      else if (ii.eq.6) then
        longitude= +118.150d0     ! Pasadena
        latitude =  +34.150d0
        zone     =   +8.d0
      else
        print '(a,$)','Enter GMT offset in hours (+ for west) : '
        read(5,*) zone
        print '(a,$)','Enter longitude (+ for west) : '
        read(5,*) longitude
        print '(a,$)','Enter latitude : '
        read(5,*) latitude
      endif
      return
      end


C----------------------------------------------------------------------
C Convert Decimal degrees (RA and DEC) and hours, minutes, seconds and
C  degrees, minutes, seconds.
C
      subroutine Decimal_To_Hours()
C
      implicit none
      integer*4 mode
      character*40 ss
      real*8 ra,dec,rh,rm,rs,dd,dm,ds
C Echo.
      print *,'Convert Decimal degrees (RA and DEC) and hours,',
     .        ' minutes, seconds and'
      print *,'degrees, minutes, seconds.'
C Ask mode.
      print '(a)',' 1 : Decimal degrees to H M S +/-D M S.'
      print '(a)',' 2 : H M S +/-D M S o decimal degrees.'
      print '(a,$)',' Choose : '
      read(5,*) mode
C From decimal degrees.
      if (mode.eq.1) then
        print '(a,$)','Enter decimal degrees RA, DEC: '
        read(5,*) ra,dec
        call write_radec(ra/15.,dec,ss,6)
        print '(a)',ss
      endif
C From H M S.
       if (mode.eq.2) then
        print '(a,$)','Enter H M S +/-D M S : '
        read(5,*) rh,rm,rs,dd,dm,ds
        ra = 15. * ( rh + (rm + (rs/60.))/60. )
        dec=         dd + (dm + (ds/60.))/60.
        print '(2f12.7)',ra,dec
      endif
      return
      end
