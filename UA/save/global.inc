C This file is global.inc

C:::::::::::::::::::::::::
C Global variables for makee routines.
C
      real*4 Global_eperdn       ! electrons per digital number
      real*4 Global_rov          ! readout variance in counts
      real*4 Global_exposure     ! exposure time in seconds
      real*4 Global_sw           ! current estimate of slit width
      integer*4 Global_obsnum    ! observation number of object
      integer*4 Global_eon       ! current echelle order number
      integer*4 Global_neo       ! current number of echelle orders
      integer*4 Global_xbin      ! current x binning factor (columns)
      integer*4 Global_ybin      ! current y binning factor (rows)
      logical Global_HIRES       ! Is this HIRES?
      logical Global_ESI         ! Is this ESI?
C
      common / GLOBALBLK /
     .            Global_eperdn, Global_rov, 
     .            Global_exposure, Global_sw, 
     .            Global_obsnum, Global_eon, Global_neo,
     .            Global_xbin, Global_ybin,
     .            Global_HIRES, Global_ESI

