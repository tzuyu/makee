/* cpg-util.c                        tab  August 1999 */


#include "ctab.h"
#include "cphomax.h"
#include "cmisc.h"
#include "cufio.h"
#include "cmath-util.h"
#include "cpgplot.h"


/* --------------------------------------------------------------------
  Draw a gaussian curve.
  Input: xoffset : subtract this from x values before scaling.
         area : total area -inf to +inf.
                 (For histrogram this is (number of bins)*(bin size).)
         sigma: 1 sigma value.
         npp  : number of points to plot
*/
void cpg_PlotGaussian(float xoffset, float area, float sigma, int npp)
{
/**/
const int maxpp=1001;
float xx[1001],yy[1001];
float xmin,xmax,ymin,ymax,xinc,xv;
double xvn;
int nn;
/**/
/* Check. */
if (npp > maxpp) {
  fprintf(stderr,"ERROR: cpg_PlotGaussian: too many points requested:%d\n",npp);
  return;
}
/* Plot over full window. */
cpgqwin(&xmin,&xmax,&ymin,&ymax);
/* Create point array. */
xinc = (xmax-xmin)/npp;
nn=0;
for (xv=xmin; xv<=xmax; xv=xv+xinc) {
/* Cutoff at 20 sigmas. */
  if (ABS((xv)) < 20.) {
    xx[nn] = xv;
    xvn = (xv - xoffset) / sigma;
    yy[nn] = area * gaussian_sig( xvn );
    nn++;
  }
}
/* Plot. */
cpgline(nn,xx,yy);
return;
}



/* ---------------------------------------------------------------------- */
/*
  Plot histogram of data points (using PGPLOT).
  Input: y[] (data points),  n (number of data points),  
         xmin (minimum bin value), xmax (maximum bin value),
         xinc (size of bins), xtick (see cpgbox()), nxtick (see cpgbox()),
         mode: 0=do not make new box or window.
               1=normal.
               2=normal, but plot dotted lines.
  Output: fwhm : Full Width Half Maximum estimate (0.939437 * area / peak).

*/
void cpg_BinHist(float y[], int n, float xmin, float xmax, float xinc, 
                 float xtick, int nxtick, int mode, float *fwhm)
{
/**/
int const max_nb = 9000;
float xb[9000],yb[9000];
int nb,ii,binnum;
float ymin,ymax,sum,peak;
char const center = '\1';  /* Centered.  ( Use '\0' for not centered. ) */
/**/
/* Initialize. */
nb = ((xmax-xmin)/xinc) + 1.5;
if (nb > max_nb) nb = max_nb;
/* Initialize bins. */
for (ii=0; ii<nb; ++ii) {
  xb[ii] = xmin + ( (xinc*ii) + (xinc/2.) );
  yb[ii] = 0.;
}
/* Count number in bins. */
for (ii=0; ii<n; ++ii) {
  binnum     = ( ( (y[ii]-xmin) - (xinc/2.) ) / xinc ) + 0.5;
  yb[binnum] = yb[binnum] + 1.;
}
/* Make new window and box. */
if (mode > 0) {
  ymin = 0.0;
  ymax = 1.0;
  for (ii=0; ii<nb; ++ii) {
    if (yb[ii] > ymax) ymax = yb[ii];
  }
  ymax = ymax * 1.05;
  cpgswin(xmin,xmax,ymin,ymax);
  cpgbox("BICNST",xtick,nxtick,"BICNST",0.,0);
}
/* FWHM estimate. */
sum=0.;
peak=0.;
for (ii=0; ii<nb; ++ii) {
  sum = sum + yb[ii];
  if (yb[ii] > peak) peak = yb[ii];
}
if ((peak > 0.)&&(sum > 0.)) {
  *fwhm = (0.939437 * (sum*xinc)) / peak;
} else {
  *fwhm = -1.;
}
/* Draw bins. */
cpgbbuf();
if (mode == 2) cpgsls(4);
cpgbin(nb,xb,yb,center);
if (mode == 2) cpgsls(1);
cpgebuf();
return;
}


/* --------------------------------------------------------------------
  Draws a horizontal line.
*/
void cpg_HLine(float x1, float x2, float y)
{
float xx[2],yy[2];
xx[0] = x1; xx[1] = x2;
yy[0] = y;  yy[1] = y;
cpgline(2,xx,yy);
return;
}


/* --------------------------------------------------------------------
  Draw a box.
*/
void cpg_DrawBox(float x1, float x2, float y1, float y2)
{
float xx[5],yy[5];
xx[0] = x1; yy[0] = y1;
xx[1] = x2; yy[1] = y1;
xx[2] = x2; yy[2] = y2;
xx[3] = x1; yy[3] = y2;
xx[4] = x1; yy[4] = y1;
cpgline(5,xx,yy);
return;
}

/* --------------------------------------------------------------------
  Draws a line.
*/
void cpg_drawline(float x1, float x2, float y1, float y2)
{
float xx[2],yy[2];
xx[0] = x1; xx[1] = x2;
yy[0] = y1; yy[1] = y2;
cpgline(2,xx,yy);
return;
}

/* --------------------------------------------------------------------
  Draw a single point.
*/
void cpg_pt1(float x1, float y1, int sym)
{
float xx[2],yy[2];
xx[0] = x1; 
yy[0] = y1;
cpgpt(1,xx,yy,sym);
return;
}

