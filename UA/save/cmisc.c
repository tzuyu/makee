/* cmisc.c    Miscellaneous  Utility Archive in C      tab  July,August 1999 */  
/* Tom's inclusions. */
#include "ctab.h"

#include <math.h>

#include "../SystemChoice.h"


/* ----------------------------------------------------------------------
  Get the HAR home directory using MAKEE_DIR or HAR_DIR or MKHARDIR or HARDIR
  environment variable.
*/
/*@@*/
void cGetMakeeHome( char wrd[] )
{
/**/
char *s;
/**/
s = NULL;
s = getenv("MAKEE_DIR");
if (s == NULL) s = getenv("HAR_DIR");
if (s == NULL) s = getenv("HARDIR");
if (s == NULL) s = getenv("MKHARDIR");
if (s == NULL) {
  fprintf(stderr,"ERROR: GetMakeeHome: Could not find the MAKEE_DIR or HAR_DIR \n");
  fprintf(stderr,"ERROR: GetMakeeHome: or MKHARDIR enviroment variables.\n");
  fprintf(stderr,"ERROR: GetMakeeHome: Set one of these to the directory\n");
  fprintf(stderr,"ERROR: GetMakeeHome: containing the UA and EE subdirectories.\n");
} else {
  strcat(s,"/");
}
strcpy(wrd,s);
return;
}



/* ----------------------------------------------------------------------
  Given a character 0,1,2,...a,b,c,d,e,f , return a 4 byte string with
  the binary equivalent.
*/
/*@@*/
void hexbinarystring( char cc, char ss[] )
{
strcpy(ss,"");
         if (cc == '0') { strcpy(ss,"0000");
} else { if (cc == '1') { strcpy(ss,"0001");
} else { if (cc == '2') { strcpy(ss,"0010");
} else { if (cc == '3') { strcpy(ss,"0011");
} else { if (cc == '4') { strcpy(ss,"0100");
} else { if (cc == '5') { strcpy(ss,"0101");
} else { if (cc == '6') { strcpy(ss,"0110");
} else { if (cc == '7') { strcpy(ss,"0111");
} else { if (cc == '8') { strcpy(ss,"1000");
} else { if (cc == '9') { strcpy(ss,"1001");
} else { if (cc == 'a') { strcpy(ss,"1010");
} else { if (cc == 'b') { strcpy(ss,"1011");
} else { if (cc == 'c') { strcpy(ss,"1100");
} else { if (cc == 'd') { strcpy(ss,"1101");
} else { if (cc == 'e') { strcpy(ss,"1110");
} else { if (cc == 'f') { strcpy(ss,"1111");
}}}}}}}}}}}}}}}}
return;
}

/* ----------------------------------------------------------------------
  Given a 16-bit integer "ii", return a 16 character binary string "bs".
*/
/*@@*/
void show_binary16( unsigned short int ii, char bs[] )
{
/**/
char wrd[5];
char c4[5];
int kk;
/**/
sprintf(wrd,"%x",ii);
kk = strlen(wrd);
if (kk == 1) { strcpy(bs,"000000000000"); } else {
if (kk == 2) { strcpy(bs,"00000000");     } else {
if (kk == 3) { strcpy(bs,"0000");         } else {
if (kk == 4) { strcpy(bs,"");                    }}}}
for (ii=0; ii<kk; ++ii) {
  hexbinarystring(wrd[ii],c4);
  strcat(bs,c4);
}
return;
}


/* ----------------------------------------------------------------------
  Given a 8-bit integer "ii", return a 8 character binary string "bs".
*/
/*@@*/
void show_binary8( unsigned char ii, char bs[] )
{
/**/
char wrd[3];
char c2[3];
int kk;
/**/
sprintf(wrd,"%x",ii);
kk = strlen(wrd);
if (kk == 1) { strcpy(bs,"0000");         } else {
if (kk == 2) { strcpy(bs,"");                    }}
for (ii=0; ii<kk; ++ii) {
  hexbinarystring(wrd[ii],c2);
  strcat(bs,c2);
}
return;
}


/* ----------------------------------------------------------------------
  Write a double precision value into a 9 characters in >=10 character string.
  Input:  value : value to write to string.
  Output:    ss : string, must be at least 10 characters long.
  (Like rf9 in misc.f.)
*/
/*@@*/
void WriteString9(double value, char ss[])
{
if (ABS((value)) < 0.0001) { sprintf(ss,"%9.2e",value);
} else {
if ((value < 99.999999)&&(value > -9.999999)) { sprintf(ss,"%9.6f",value);
} else {
if ((value < 999.99999)&&(value > -99.99999)) { sprintf(ss,"%9.5f",value);
} else {
if ((value < 9999.9999)&&(value > -999.9999)) { sprintf(ss,"%9.4f",value);
} else {
if ((value < 99999.999)&&(value > -9999.999)) { sprintf(ss,"%9.3f",value);
} else {
if ((value < 999999.99)&&(value > -99999.99)) { sprintf(ss,"%9.2f",value);
} else {
if ((value < 9999999.9)&&(value > -999999.9)) { sprintf(ss,"%9.1f",value);
} else { sprintf(ss,"%9.2e",value);
}}}}}}}
ss[9]='\0';
return;
}


/* ----------------------------------------------------------------------
  Write a double precision value into a 7 characters in >=8 character string.
  Input:  value : value to write to string.
  Output:    ss : string, must be at least 8 characters long.
  (Like rf7 in misc.f.)
*/
/*@@*/
void WriteString7(double value, char ss[])
{
if (ABS((value)) < 0.001) { sprintf(ss,"%7.0e",value);
} else {
if ((value < 99.9999)&&(value > -9.9999)) { sprintf(ss,"%7.4f",value);
} else {
if ((value < 999.999)&&(value > -99.999)) { sprintf(ss,"%7.3f",value);
} else {
if ((value < 9999.99)&&(value > -999.99)) { sprintf(ss,"%7.2f",value);
} else {
if ((value < 99999.9)&&(value > -9999.9)) { sprintf(ss,"%7.1f",value);
} else {
if ((value < 999999.)&&(value > -99999.)) { sprintf(ss,"%7.0f",value);
} else { sprintf(ss,"%7.0e",value);
}}}}}}
ss[7]='\0';
return;
}


/* ----------------------------------------------------------------------
 Bin data using whole pixels.  Ignores negative pixels in mode 1.

 Input:  bin : binning factor (>1).
        mode : binning mode (0=normal averaging, 1= averaging error array).

In/Out:   np : number of points in array. 
        xx[] : array of values to be binned.
*/
/*@@*/
void cDoBin4(int bin, int *np, float xx[], int mode)
{
/**/
int nn,ii,pix,ok;
float xt;
/**/
/* Normal, averaging rebinning. */
if (mode == 0) {
  nn =-1;
  pix= 0;
  while((pix+bin-1) < *np) {
    xt=0.;
    for (ii=pix; ii<=(pix+bin-1); ++ii) {
      xt = xt + xx[ii];
    }
    ++nn;
    xx[nn]= xt / bin;
    pix = pix + bin;
  }
  *np = nn;
/* Error array rebinning. */
} else {
  nn =-1;
  pix= 0;
  while((pix+bin-1) < *np) {
    ok=1;
    xt=0.;
    for (ii=pix; ii<=(pix+bin-1); ++ii) {
      xt = xt + (xx[ii] * xx[ii]);
      if (xx[ii] < 0.) ok=0;
    }
    ++nn;
    if (ok == 1) {
      xx[nn] = sqrt( xt ) / bin;
    } else {
      xx[nn] = -1.;
    }
    pix = pix + bin;
  }
  *np = nn;
}
return;
}


/* ----------------------------------------------------------------------
  Does a file exist?  If so return "1".
*/
/*@@*/
int FileExist( char filename[] )
{
FILE *fu;
int ii;
fu = fopen(filename,"r");
if (fu == NULL) { ii=0; } else { ii=1; fclose(fu); }
return(ii);
}



/* ----------------------------------------------------------------------
  Get seconds since 1970.
*/
/*@@*/
int timeget()
{
/**/
int ii;
time_t tp;
/**/
time(&tp);
ii = tp;
return(ii);
}


/* ----------------------------------------------------------------------
  Seconds since new year.
*/
/*@@*/
int ssny()
{
int doy,min,hour,sec,ii;
time_t tp;
struct tm *ptr;
time(&tp);
ptr = localtime(&tp);
doy = ptr->tm_yday;
hour= ptr->tm_hour;
min = ptr->tm_min;
sec = ptr->tm_sec;
ii = (doy * 86400) + (hour * 3600) + (min * 60) + (sec);
return(ii);
}


/* ----------------------------------------------------------------------
  Get a time string.
*/
/*@@*/
void timegetstring(char uline[])
{
time_t tp;
struct tm *ptr;
time(&tp);
ptr = localtime(&tp);
strftime(uline,25,"%Y-%m-%dT%H:%M:%S", ptr);
return;
}


/* ----------------------------------------------------------------------
  Get system time.
*/
/*@@*/
void getsystime( char uline[] )
{
/**/
FILE *infu;
char *ptr;
char line[100];
int ii;
/**/
ii   = system("date > /tmp/datestring");
infu = fopen("/tmp/datestring","r");
ptr  = fgets(line,sizeof(line),infu);
line[strlen(line)-1] = '\0';
fclose(infu);
strcpy(uline,line);
return;
}

/* ------------------------------- */
void si2char( char c2[2], char cc[2])
{
cc[0] = c2[0];
cc[1] = c2[1];
return;
}
/* ------------------------------- */
void li2char( char c4[4], char cc[4])
{
cc[0] = c4[0];
cc[1] = c4[1];
cc[2] = c4[2];
cc[3] = c4[3];
return;
}
/* ------------------------------- */
short int str2si( short int *c2 )
{
short int ii;
ii = *c2;
return(ii);
}
/* ------------------------------- */
long int str2li( long int *c4 )
{
long int ii;
ii = *c4;
return(ii);
}


/* ----------------------------------------------------------------------
  Reverse order of bytes in a two byte number (short integer) so that
  it can read directly by Sun Sparcstation UNIX system.
  Call using "bswp2( &val )" -- will give warning message on compilation.
*/
/*@@*/
void bswp2(char bb[])
{
char tc;
tc=bb[0]; bb[0]=bb[1]; bb[1]=tc;
return;
}


/* ----------------------------------------------------------------------
  Reverse order of bytes in a four byte number (long integer or float) so that
  it can read directly by Sun Sparcstation UNIX system.
  Call using "bswp4( &val )" -- will give warning message on compilation.
*/
/*@@*/
void bswp4(char bb[])
{
char tc;
tc=bb[3]; bb[3]=bb[0]; bb[0]=tc;
tc=bb[2]; bb[2]=bb[1]; bb[1]=tc;
return;
}


/* ----------------------------------------------------------------------
  Reverse order of bytes in a eight byte number (double only) so that
  it can read directly by Sun Sparcstation UNIX system.
  Call using "bswp8( &val )" -- will give warning message on compilation.
*/
/*@@*/
void bswp8(char bb[])
{
char tc;
tc=bb[7]; bb[7]=bb[0]; bb[0]=tc;
tc=bb[6]; bb[6]=bb[1]; bb[1]=tc;
tc=bb[5]; bb[5]=bb[2]; bb[2]=tc;
tc=bb[4]; bb[4]=bb[3]; bb[3]=tc;
return;
}


/* ----------------------------------------------------------------------
  Swap bytes in double precision real using a character string.
*/  
/*@@*/
double SwapDouble( double value )
{
double rr;
rr = value;
if ( SUNSYS == 1 ) return(rr);
bswp8( (char *) &rr );
return(rr);
}

/* ----------------------------------------------------------------------
  Swap bytes in float precision real using a character string.
*/  
/*@@*/
float SwapFloat( float value )
{
float rr;
rr = value;
if ( SUNSYS == 1 ) return(rr);
bswp4( (char *) &rr );
return(rr);
}



/* ----------------------------------------------------------------------
  Reverse order of bytes in a two byte number (unsigned integer) so that
  it can read directly by Sun Sparcstation UNIX system.
  Will not give warning message on compilation.
*/
/*@@*/
unsigned short int ByteSwap2us( unsigned short int ii )
{
unsigned short int jj;
unsigned short int i0,i1;
if ( SUNSYS == 1 ) { jj = ii; return(jj); }
i0 = ii;
i1 = (i0 << 8) | (i0 >> 8);
jj = i1;
return(jj);
}

/* ----------------------------------------------------------------------
  Reverse order of bytes in a two byte number (integer) so that
  it can read directly by Sun Sparcstation UNIX system.
  Will not give warning message on compilation.
*/
/*@@*/
short int ByteSwap2( short int ii )
{
short int jj;
unsigned short int i0,i1;
if ( SUNSYS == 1 ) { jj = ii; return(jj); }
i0 = ii;
i1 = (i0 << 8) | (i0 >> 8);
jj = i1;
return(jj);
}

/* ----------------------------------------------------------------------
  Reverse order of bytes in a four byte number (integer) so that
  it can read directly by Sun Sparcstation UNIX system.
  Will not give warning message on compilation.
*/
/*@@*/
long int ByteSwap4( long int ii )
{
long int jj;
unsigned long int i0,i1,i2,i3,i4,i5;
if ( SUNSYS == 1 ) { jj = ii; return(jj); }
i0 = ii;
i1 = (i0 << 24);
i2 = (i0 >> 8);  i2 = (i2 << 24); i2 = (i2 >> 8);
i3 = (i0 << 8);  i3 = (i3 >> 24); i3 = (i3 << 8);
i4 = (i0 >> 24);
i5 = (i1 | i2 | i3 | i4 );
jj = i5;
return(jj);
}



/* ----------------------------------------------------------------------
  Find the difference between two RA positions (given in decimal degrees).
    Input: ra1, ra2 (must be between 0 and 360).
  Return difference in the sense ra1 - ra2, but account for RA=0 meridian.
  Assume positions are within 22 degrees of each other.
*/
/*@@*/
double FindRADiff(double ra1, double ra2)
{
/**/
double a,b,diff;
/**/
a=ra1; b=ra2;
if ((a > 45.)&&(a < 315.)&&(b > 45.)&&(b < 315.)) {
  diff = a - b;
} else {
  a = a + 90;
  b = b + 90;
  if (a > 360.) a = a - 360.;
  if (b > 360.) b = b - 360.;
  diff = a - b;
}
return(diff);
}


/* ----------------------------------------------------------------------
  Find the average between two RA positions (given in decimal degrees).
    Input: ra1, ra2 (must be between 0 and 360).
  Return average position in the sense (ra1+ra2)/2, but account for RA=0 
  meridian.  Assume positions are within 22 degrees of each other.
  Answer will be between 0 and 360.
*/
/*@@*/
double FindRAMean(double ra1, double ra2)
{
double a,b,mean;
a=ra1; b=ra2;
if ((a > 45.)&&(a < 315.)&&(b > 45.)&&(b < 315.)) {
  mean = (a+b)/2.;
} else {
  a = a + 90;
  b = b + 90;
  if (a > 360.) a = a - 360.;
  if (b > 360.) b = b - 360.;
  mean = ( (a+b)/2. ) - 90.;
  if (mean < 0.) mean = mean + 360.;
}
return(mean);
}



/* ----------------------------------------------------------------------
  Evaluate polynomial: y = f(x) = co[0] + c[1]*x + c[2]*x*x + ...
    Input: nc (number of coeffecients)
    Input: co (coeffecient array)
    Input: xv (x value)
  Returns function value.
*/
/*@@*/
double cpolyval(int nc, double co[], double xv)
{
/**/
int ii;
double rr;
/**/
rr = 0.;
for (ii=0; ii < (nc-1); ++ii) { rr = ( rr + co[(nc-1)-ii] ) * xv; } 
rr = rr + co[0];
return(rr);
}


/* ----------------------------------------------------------------------
  Find the nearest element in an array to the given value.
    Input: xx (given value).
    Input: narr (number of elements in array).
    Input: arr (the array to be searched).
  Array need not be sorted.
*/
/*@@*/
int cneari(double xx, int narr, double arr[])
{
/**/
int ii,bestii;
double lo,diff;
/**/
bestii=0;
lo=ABS((xx - arr[0]));
for (ii=0; ii<narr; ++ii) {
  diff = ABS((xx - arr[ii]));
  if (diff < lo) { lo=diff; bestii=ii; }
}
return(bestii);
}


/* ----------------------------------------------------------------------
  THIS VERSION WORKS WITH 4 BYTE REALS (float).
  Find the nearest element in an array to the given value.
    Input: xx (given value).
    Input: narr (number of elements in array).
    Input: arr (the array to be searched).
  Array need not be sorted.
*/
/*@@*/
int cneari4(float xx, int narr, float arr[])
{
/**/
int ii,bestii;
float lo,diff;
/**/
bestii=0;
lo=ABS((xx - arr[0]));
for (ii=0; ii<narr; ++ii) {
  diff = ABS((xx - arr[ii]));
  if (diff < lo) { lo=diff; bestii=ii; }
}
return(bestii);
}


/* ----------------------------------------------------------------------
  Find the nearest element in an array to the given value.
  This version uses a binary search for speed.
  The array must be sorted for this to work.
    Input: xx (given value).
    Input: narr (number of elements in array).
    Input: arr (the array to be searched).
  Array MUST be sorted, low values to high values.
*/
/*@@*/
int cneari_bs(double xx, int narr, double arr[])
{
/**/
int ii,kk,i1,i2;
/**/
/* Binary search. */
if (narr < 1) return(0);
ii=0;
i1=0;
i2=narr-1;
while ((i2-i1) > 1) {
  ii = (i1+i2)/2;
  if (arr[ii] > xx) { i2=ii; } else { i1=ii; }
}
if (i2 <= i1) { printf("ERROR: cneari_bs: this should not happen.\n"); }
if ( ABS((arr[i1] - xx)) < ABS((arr[i2] - xx)) ) { kk=i1; } else { kk=i2; }
return(kk);
}


/* ----------------------------------------------------------------------
  THIS VERSION WORKS WITH 4 BYTE REALS (float).
  Find the nearest element in an array to the given value.
  This version uses a binary search for speed.
  The array must be sorted for this to work.
    Input: xx (given value).
    Input: narr (number of elements in array).
    Input: arr (the array to be searched).
  Array MUST be sorted, low values to high values.
*/
/*@@*/
int cneari_bs4(float xx, int narr, float arr[])
{
/**/
int ii,kk,i1,i2;
/**/
/* Binary search. */
if (narr < 1) return(0);
ii=0;
i1=0;
i2=narr-1;
while ((i2-i1) > 1) {
  ii = (i1+i2)/2;
  if (arr[ii] > xx) { i2=ii; } else { i1=ii; }
}
if (i2 <= i1) { printf("ERROR: cneari_bs: this should not happen.\n"); }
if ( ABS((arr[i1] - xx)) < ABS((arr[i2] - xx)) ) { kk=i1; } else { kk=i2; }
return(kk);
}


/* ----------------------------------------------------------------------
  Take an input from the terminal to create a pause.
*/
/*@@*/
void cpauseit()
{
  char dummy[3];
  printf("Hit return.");
  fgets(dummy,sizeof(dummy),stdin);
  return;
}


/* ---------------------------------------------------------------------- */
/* Prints characters a1 to a2 in string "a" on standard output, with a <CR>
   on the end. First character is number 0.
*/
/*@@*/
void substrprt(char a[], int a1, int a2 )
{
int ii;
for (ii=a1; ii<=a2; ++ii) { printf("%c",a[ii]); }
printf("\n");
return;
}


/* ---------------------------------------------------------------------- */
/* Copies characters a1 to a2 in string "a" into the string "b" starting
   at the position "b1".  First character is number 0.
*/
/*@@*/
void substrcpy( char a[], int a1, int a2, char b[], int b1 )
{
int ii;
for (ii=a1; ii<=a2; ++ii) { b[ b1 + (ii-a1) ] = a[ ii ]; }
return;
}


/* ---------------------------------------------------------------------- */
/* Compares the characters a1 to a2 in string "a" to the characters in
     string "b" starting at b1.  If equal return "0", if not, return "1".
     First character is number 0.
*/
/*@@*/
int substrcmp(char a[], int a1, int a2, char b[], int b1 )
{
int ii,n,ok;
if (strlen(a) < a2) return(1);
if (strlen(b) < b1+(a2-a1)) return(1);
ok=0;
n = 1+(a2-a1);
for (ii=0; ii<n; ++ii) { 
if (a[a1+ii] != b[b1+ii]) ok = 1;
}
return(ok);
}
  

/* ---------------------------------------------------------------------- */
/* Compares the first "n" characters of two strings.  If equal return "0",
   if not, return "1".  (same convention as "strcmp").  */
/*@@*/
int substrcmp0(char a[], char b[], int n )
{
int ii,ok;
if (strlen(a) < n) return(1);
if (strlen(b) < n) return(1);
ok=0;
for (ii=0; ii<n; ++ii) { if (a[ii] != b[ii]) ok = 1; }
return(ok);
}


/* ----------------------------------------------------------------------
  Returns truncated integer.
*/
/*@@*/
int cint( double rr )
{
int ii;
ii = rr;
return(ii);
}

/* ----------------------------------------------------------------------
  Returns nearest integer.
*/
/*@@*/
int cnint( double rr )
{
int ii;
if (rr > 0.) {
  ii = rr + 0.5;
} else {
  ii = rr - 0.5;
}
return(ii);
}

/* ----------------------------------------------------------------------
  Converts a string to upper case.
*/
/*@@*/
void cupper_case(char a[])
{
/**/
int ii,ia;
char aa;
/**/
ia = strlen(a);
for (ii=0; ii<ia; ++ii) { aa = a[ii];
  if (aa == 'a') aa='A';
  if (aa == 'b') aa='B';
  if (aa == 'c') aa='C';
  if (aa == 'd') aa='D';
  if (aa == 'e') aa='E';
  if (aa == 'f') aa='F';
  if (aa == 'g') aa='G';
  if (aa == 'h') aa='H';
  if (aa == 'i') aa='I';
  if (aa == 'j') aa='J';
  if (aa == 'k') aa='K';
  if (aa == 'l') aa='L';
  if (aa == 'm') aa='M';
  if (aa == 'n') aa='N';
  if (aa == 'o') aa='O';
  if (aa == 'p') aa='P';
  if (aa == 'q') aa='Q';
  if (aa == 'r') aa='R';
  if (aa == 's') aa='S';
  if (aa == 't') aa='T';
  if (aa == 'u') aa='U';
  if (aa == 'v') aa='V';
  if (aa == 'w') aa='W';
  if (aa == 'x') aa='X';
  if (aa == 'y') aa='Y';
  if (aa == 'z') aa='Z';  a[ii] = aa;
}
return;
}

/* ----------------------------------------------------------------------
  Converts a string to lower case.
*/
/*@@*/
void clower_case(char a[])
{
/**/
int ii,ia;
char aa;
/**/
ia = strlen(a);
for (ii=0; ii<ia; ++ii) { aa = a[ii];
  if (aa == 'A') aa='a';
  if (aa == 'B') aa='b';
  if (aa == 'C') aa='c';
  if (aa == 'D') aa='d';
  if (aa == 'E') aa='e';
  if (aa == 'F') aa='f';
  if (aa == 'G') aa='g';
  if (aa == 'H') aa='h';
  if (aa == 'I') aa='i';
  if (aa == 'J') aa='j';
  if (aa == 'K') aa='k';
  if (aa == 'L') aa='l';
  if (aa == 'M') aa='m';
  if (aa == 'N') aa='n';
  if (aa == 'O') aa='o';
  if (aa == 'P') aa='p';
  if (aa == 'Q') aa='q';
  if (aa == 'R') aa='r';
  if (aa == 'S') aa='s';
  if (aa == 'T') aa='t';
  if (aa == 'U') aa='u';
  if (aa == 'V') aa='v';
  if (aa == 'W') aa='w';
  if (aa == 'X') aa='x';
  if (aa == 'Y') aa='y';
  if (aa == 'Z') aa='z';  a[ii] = aa;
}
return;
}


/* ---------------------------------------------------------------------- */
/* Finds the first occurence of substring "b" in string "a".
   Returns the position of the substring within "a", or will return "-1"
   if the substring is not found.  First character is number 0.
*/
/*@@*/
int cindex(char a[], char b[] )
{
/**/
int ii,jj,ia,ib,pos;
/**/
/* Get and check lengths of strings. */
ia = strlen(a);
ib = strlen(b);
/* Return immediately if string a is too small or either string empty. */
if (ia < ib) { return(-1); }
if (ia < 1)  { return(-1); }
if (ib < 1)  { return(-1); }
/* Find substring. */
pos=-1;
ii=0;
while (ii <= (ia-ib)) {
  pos=ii;
  for (jj=0; jj<ib; ++jj) { if (a[ii+jj] != b[jj]) pos = -1; }
  if (pos > -1)  break;
  ++ii;
}
return(pos);
}


/* ---------------------------------------------------------------------- */
/* Finds the first occurence of substring "b" in string "a" at or after
   position "ptr".  Returns this position of the substring within "a", or 
   will return "-1" if the substring is not found. First character is number 0.
*/
/*@@*/
int cindex2(char a[], char b[], int ptr )
{
/**/
int ii,jj,ia,ib,pos;
/**/
/* Get and check lengths of strings. */
ia = strlen(a);
ib = strlen(b);
/* Return immediately if string a is too small or either string empty. */
if (ia < ib) { return(-1); }
if (ia < ptr+1) { return(-1); }
if (ia < 1)  { return(-1); }
if (ib < 1)  { return(-1); }
/* Find substring. */
pos=-1;
ii=ptr;
while (ii <= (ia-ib)) {
  pos=ii;
  for (jj=0; jj<ib; ++jj) { if (a[ii+jj] != b[jj]) pos = -1; }
  if (pos > -1)  break;
  ++ii;
}
return(pos);
}


/* ---------------------------------------------------------------------- */
/* Finds the last occurence of a non-blank character in string "a".
   The first character position is 0.
   Returns -1 if string is completely blank.
*/
/*@@*/
int clc(char a[])
{
int ii,ia;
ia = strlen(a);
ii=ia-1;
while ((ii >= 0)&&(a[ii] == ' ')) { --ii; }
return(ii);
}

/* ---------------------------------------------------------------------- */
/* Finds the first occurence of a non-blank character in string "a".
   The first character position is 0.
   Returns -1 if string is completely blank.
*/
/*@@*/
int cfc(char a[])
{
int ii,ia;
ia = strlen(a);
ii=0;
while ((ii <= ia)&&(a[ii] == ' ')) { ++ii; }
if (ii == ia) ii=-1;
return(ii);
}


/* ---------------------------------------------------------------------- */
/* Copy the argv[] strings into arg[100][100]. */
/*@@*/
void cargcopy(char *argv[], int argc, int *narg, char arg[100][100])
{
  int ii;
  *narg = argc - 1;
  for (ii=0; ii<=(*narg); ++ii) {
    strcpy(&arg[ii][0],&argv[ii][0]);
  }
return;
}


/* ---------------------------------------------------------------------- */
/* Finds an argument "parm" in argument series and extracts the word
   which follows the "parm" string and loads it into "wrd".  It also decrements
   narg by 1, and reloads the arg[][] array to delete the found entry.
   This function will "delete and load" only the first matching argument.
   Returns "1" if the argument was found, otherwise "0".  If "d" is "s", 
   then this is a switch and the parm must match the argument exactly.
     Input:  arg[][], narg, parm, d.
     Output: narg,wrd,n.  (n is always 1).
*/
/*@@*/
int cfindarg(char arg[100][100], int *narg, char parm[], char d, char wrd[] )
{
/**/
int ii,jj,kk,ok,parmlen,arglen;
/**/

/* Initialize. */
  parmlen = strlen(parm);
  ok=0;
  ii=1;

/* Switch only. */
  if (d == 's') {

/* Find the parameter. Exact match. */
    while ( (ii <= (*narg)) && (ok == 0) ) {
      if ( (substrcmp0(arg[ii],parm,parmlen) == 0) &&
           (strlen(parm) == strlen(arg[ii])) ) {
        ok=1;
      }
      else {
        ++ii;
      }
    }

/* Value parameter, not a switch. */
  } else {

/* Find the parameter. */
    while ( (ii <= (*narg)) && (ok == 0) ) {
      if (substrcmp0(arg[ii],parm,parmlen) == 0) {
/* Load the word. */
        arglen = strlen(arg[ii]);
        for (jj=parmlen; jj<arglen; ++jj) {
          wrd[jj-parmlen] = arg[ii][jj];
        }
        wrd[arglen-parmlen] = '\0';
        ok=1;
      }
      else {
        ++ii;
      }
    }

  }

/* Reload the arg[] array. */
  if (ok == 1) {
    for (kk=ii; kk<(*narg); ++kk) { strcpy(arg[kk],arg[kk+1]); }
    *narg = *narg - 1;
  }

  return(ok);
}



/* ---------------------------------------------------------------------- */
/* Compute the angular separation in degrees between two ra and dec
   positions.  ra and dec are in decimal degrees.
*/
/*@@*/
double cangsep(double ra1, double dec1, double ra2, double dec2)
{
/**/
  double a1,t1,a2,t2,x1,y1,z1,x2,y2,z2,rr;
  const double pi = 3.141592654;
  const double pio180 = pi/180.;   /* PI over 180 degrees. */
/**/
/* Set alpha and theta (in radians). */
  a1 = ra1 * pio180;
  t1 = ( 90. - dec1 ) * pio180;
  a2 = ra2 * pio180;
  t2 = ( 90. - dec2 ) * pio180;
/* Computations. */
  x1 = sin(t1) * cos(a1);
  y1 = sin(t1) * sin(a1);
  z1 = cos(t1);
  x2 = sin(t2) * cos(a2);
  y2 = sin(t2) * sin(a2);
  z2 = cos(t2);
  rr = acos( (x1*x2) + (y1*y2) + (z1*z2) );
  rr = ( rr / pio180 );
  return(rr);
}


/* ----------------------------------------------------------------------
  Given x,y arrays and a new x point (xpt) return an interpolated value for
  the y axis.  x,y array must be monotonically increasing in x.  "0" is
  returned for xpt values outside of the range.
   Inputs:  n (number of points in x,y arrays).   x[],y[] (arrays).
            xpt (value to interpolate towards).
*/
/*@@*/
double yinterp(int n, double x[], double y[], double xpt)
{
/**/
  int ii;
  double yy;
/**/
  if (xpt < x[0]) return(0.);
  if (xpt > x[n-1]) return(0.);
  ii=1;
  while ( xpt > x[ii] ) ++ii;
  yy = y[ii-1] + ( (y[ii]-y[ii-1]) * (xpt-x[ii-1]) / (x[ii]-x[ii-1]) );
  return(yy);
}


/* ---------------------------------------------------------------------- */
/* 
  Given a two char. stellar type (O5 to M9) return an effective temperature.
*/
/*@@*/
double TypeToTemp(char st[2])
{
/**/
  double stv,rr;
/**/
  double Type[14] = { 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 
                      4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 6.8 };
  double Teff[14] = { 40000., 28000., 15500., 9900., 8500., 7400., 6580.,
                       6030.,  5520.,  4900., 4130., 3480., 2800., 2400. };
/**/
/* Type. */
  stv=-1.;
  if (st[0] == 'O') stv = 0.;
  if (st[0] == 'B') stv = 1.;
  if (st[0] == 'A') stv = 2.;
  if (st[0] == 'F') stv = 3.;
  if (st[0] == 'G') stv = 4.;
  if (st[0] == 'K') stv = 5.;
  if (st[0] == 'M') stv = 6.;
/* sub-type. */
  if (st[1] == '0') stv = stv + 0.0;
  if (st[1] == '1') stv = stv + 0.1;
  if (st[1] == '2') stv = stv + 0.2;
  if (st[1] == '3') stv = stv + 0.3;
  if (st[1] == '4') stv = stv + 0.4;
  if (st[1] == '5') stv = stv + 0.5;
  if (st[1] == '6') stv = stv + 0.6;
  if (st[1] == '7') stv = stv + 0.7;
  if (st[1] == '8') stv = stv + 0.8;
  if (st[1] == '9') stv = stv + 0.9;
/* Default. Type = G0 */
  if (stv < 0.) stv=4.0;
/* Clip. */
  if (stv < 0.5) stv=0.5001;
  if (stv > 6.8) stv=6.7999;
/* Interpolate to get temperature. */
  rr = yinterp(14, Type, Teff, stv);
  return(rr);
}


/* ----------------------------------------------------------------------
  Return "1" if the character is a numeral 0-9 (else return "0").
*/
/*@@*/
int numeral(char cc)
{
if (cc == '0') return(1);
if (cc == '1') return(1);
if (cc == '2') return(1);
if (cc == '3') return(1);
if (cc == '4') return(1);
if (cc == '5') return(1);
if (cc == '6') return(1);
if (cc == '7') return(1);
if (cc == '8') return(1);
if (cc == '9') return(1);
return(0);
}


/* ----------------------------------------------------------------------
  Return "1" if the character is a numeric: 0-9, ".", "E", "e", "+", or "-".
*/
/*@@*/
int numeric(char cc)
{
if (numeral(cc) == 1) return(1);
if (cc == '.') return(2);
if (cc == '-') return(2);
if (cc == '+') return(2);
if (cc == 'E') return(2);
if (cc == 'e') return(2);
return(0);
}



/* ----------------------------------------------------------------------
  Find the bounds of IPAC table header column.
  NOTE: This is case insensitive.
  Assumes the first character is position number 1 (not 0).
  Returns 0,0 if named column not found.
    Input:  ipachead[]    : A string of an IPAC table header line.
            name[]        : Name of an IPAC table header column.
   Output:  p1            : Location in header string of beginning of column.
            p2            : Location in header string of end of column.
*/
/*@@*/
void cIPAChead(char ipachead[], char name[], int *p1, int *p2)
{
/**/
char temphead[1000];
char tempname[100];
int pp,eos,i1,i2;
/**/
/* Copy header and name. */
strcpy(temphead,ipachead);
strcpy(tempname,name);
/* Convert to upper case. */
cupper_case(temphead);
cupper_case(tempname);
/* End of string and position of card name. */
eos= strlen(temphead) - 1;
pp = cindex(temphead,tempname);
if (pp < 0) { *p1=0;  *p2=0; return; }
/* left bound. */
i1=pp-1;
while( i1 > 0) {
  if (temphead[i1] == '|') break;
  --i1;
}
/* right bound. */
i2=pp+1;
while(i2 <= eos) {
  if (temphead[i2] == '|') break;
  ++i2;
}
--i2;
/* Return results. Add one since first character is called 1, not 0. */
*p1 = i1 + 1;
*p2 = i2 + 1;
return;
}


/* ----------------------------------------------------------------------
  Convert a string segment into a double precision number.
  The string segment is defined by up1 and up2.
  (Here up1 must be >= 1, it cannot be 0.  In other words, the routine 
   thinks that the string starts with number 1 not number 0 (as usual).)
  Returns "-1.e+40" if there is an error.
*/
/*@@*/
double cvalread(char userline[], int up1, int up2)
{
/**/
  int eos,dp,i1,i2,ii,jj,kk,ee,jjj,p1,p2,ok;
  double neg,sum,r1,r2,exponent;
  char cc[2];
  char line[1000];
/**/

/* Maximum string length. */
  kk = strlen(userline)-1;

/* Limit pointers. Convert to zero origin. */
  p1 = up1-1;  if (p1<0) p1=0;  if (p1>kk) p1=kk;
  p2 = up2-1;  if (p2<0) p2=0;  if (p2>kk) p2=kk;

/* Load line string. */
  jj=0;
  for (ii=p1; ii<=p2; ++ii) {
    line[jj] = userline[ii];
    ++jj;
  }
  line[jj] = '\0';

/* Last real character of string. */
  eos = strlen(line)-1;

/* Default. */
   exponent = 1.;

/* Look for exponent. */
  ee=0;
  while ( (line[ee] != 'E') && (line[ee] != 'e') ) {
    ++ee;
    if (ee>eos) break; 
  }

/* Exponent exists? */
  if (ee <= eos) {

/* First numeral. */
    ii=ee+1;

/* End of string? */
    if (ii <= eos) {

/* The sign. */
      neg = +1.;
      if (line[ii] == '+') { ++ii; } 
        else { if (line[ii] == '-') { ++ii; neg = -1.; } }

/* End of string? */
      if (ii <= eos) {

/* Find next non-numeral. */
        jj=ii;
        while ( (jj <= eos) && (numeral(line[jj]) == 1) ) ++jj;
        jj=jj-1;
/* Numbers? */
        if (jj >= ii ) {
          sum = 0.;
          for (kk=jj; kk>=ii; kk=kk-1) {
            cc[0] = line[kk];
            cc[1] = '\0';
            sscanf(cc,"%d",&jjj);
            r1 = jjj; 
            r2 = jj-kk;
            sum= sum + ( r1 * pow(10.,r2) );
          }
          if (neg > 0.) { exponent = pow(10.,sum); } 
                   else { exponent = 1./pow(10.,sum); }
        }
      }
    }
    line[ee] = '\0';  /* Cut off string at "E" or "e". */
  }

/* Last real character of string. */
  eos = strlen(line)-1;

/* See if any numerals exist in string. */
  ok=0;
  for (ii=0; ii<strlen(line); ++ii) { if (numeral(line[ii]) == 1) ok=1; }
  if (ok == 0) {
    return(-1.e+40);     /* ERROR: cvalread: reading string. */
  }

/* Initialize. */
  neg = +1.;

/* Find decimal point (or end of string). */
  dp=0;
  while ((line[dp] != '.')&&(line[dp] != '\0')) ++dp;
/* If no decimal point, find first numeral, then find next non-numeral. */
  if (line[dp] == '\0') {
    ii=0;
    while ( numeral(line[ii]) == 0 ) ++ii;
    while ( numeral(line[ii]) == 1 ) ++ii;
    dp = ii;
  }

/* Go left from decimal point to find first non-numeral. */
  i1=dp-1;
  while ( (i1>=0) && (numeral(line[i1]) == 1) ) i1=i1-1; 
/* Set negative if non-numeral character is "-" */
  if (i1>=0) { if (line[i1] == '-') neg = -1.; };
/* Offset from non-numeral character. */
  i1=i1+1;
/* Go right from decimal point to find first non-numeral. */
  i2=dp+1;
  while ( (i2<=eos) && (numeral(line[i2]) == 1) ) i2=i2+1; 
  i2=i2-1;

/* Left side of decimal point. */
  sum=0.;
  for (ii=dp-1; ii>=i1; ii=ii-1) {
    cc[0] = line[ii];
    cc[1] = '\0';
    sscanf(cc,"%d",&jj);
    r1 = jj;
    r2 = (dp-ii)-1.;
    sum = sum + ( r1 * pow(10.,r2) );
  }

/* Right side of decimal point. */
  for (ii=dp+1; ii<=i2; ii=ii+1) {
    cc[0] = line[ii];
    cc[1] = '\0';
    sscanf(cc,"%d",&jj);
    r1 = jj;
    r2 = -1.*(ii-dp);
    sum = sum + ( r1 * pow(10.,r2) );
  }
  return(neg*sum*exponent);
}


/* ----------------------------------------------------------------------
  Find the starting and ending positions of the next numeric word from a line,
  starting from character position "pp".  In this case a numeric word 
  (0-9,.,E,e,+,-) must contain at least one numeral (0-9).

   Input:  line[]      (whole string.)
           pp          (consider only string from this point forward.)
                       (use "0" for whole string.)
   Output: sow         (start of word position.)
           eow         (end of word position.)
           ok          (1 if successful, 0 if error.)
*/
/*@@*/
void FindNumericWord(char line[], int pp, int *sow, int *eow, int *ok)
{
/**/
int ii,kk,eos,repeat,numeralflag;
/**/
/* Defaults. */
*sow=-1;
*eow=-1;
/* End of string. */
eos = strlen(line)-1;
if (pp>eos) {
  *ok=0;
  return;
}
/* Starting point. */
ii=pp;
/* Find next numeric character. */
repeat=1;
while (repeat == 1) {
  numeralflag = 0;
  while (ii <= eos) {
    kk = numeric(line[ii]);
    if (kk == 1) numeralflag=1;
    if (kk > 0) break;
    ++ii;
  }
  if (ii > eos ) {
    *ok=0;
    return;
  }
  *sow = ii;
/* Find next non-numeric character. */
  ++ii;
  while (ii <= eos) {
    kk = numeric(line[ii]);
    if (kk == 1) numeralflag = 1;
    if (kk == 0) break;
    ++ii;
  }
/* Do not repeat if a numeral (0-9) was found. */
  if (numeralflag == 1) repeat=0;
}
/* Back up one position, unless beyond end-of-string. */
if (ii > eos ) ii = eos;  else ii=ii-1;
*eow = ii;
*ok=1;
return;
}

 
/* ----------------------------------------------------------------------
  Read the nn'th number from a line string.
  Returns "-1.e+40" if there is a problem.
*/
/*@@*/
double GetLineValue(char line[], int nn)
{
/**/
int sow,eow,ii,jj,ok;
double rr;
char wrd[10000];
/**/
/* Fix NANs. */
strcpy(wrd,line);
cupper_case(wrd);
while( cindex(wrd,"NAN") > -1) {
  ii = cindex(wrd,"NAN");
  jj = clc(wrd);
  wrd[ii+0] = '0';
  if (ii+1 <= jj) wrd[ii+1] = ' ';
  if (ii+2 <= jj) wrd[ii+2] = ' ';
}
/* Get bounds of nn'th number. */
eow=-1;
for (ii=0; ii<nn; ++ii) { 
  FindNumericWord(wrd,eow+1,&sow,&eow,&ok);
  if (ok == 0) {
    fprintf(stderr,"ERROR: GetLineValue: getting value number %d.\n",nn);
    return(-1.e+40);
  }
}
/* Read value from "wrd". */
rr = cvalread(wrd,sow+1,eow+1);
return(rr);
}

/* ----------------------------------------------------------------------
  Read the nn'th number from a line string.
  No error messages will be printed.
  Returns "-1.e+40" if there is a problem.
*/
/*@@*/
double GetLineValueQuiet(char line[], int nn)
{
/**/
int sow,eow,ii,jj,ok;
double rr;
char wrd[10000];
/**/
/* Fix NANs. */
strcpy(wrd,line);
cupper_case(wrd);
while( cindex(wrd,"NAN") > -1) {
  ii = cindex(wrd,"NAN");
  jj = clc(wrd);
  wrd[ii+0] = '0';
  if (ii+1 <= jj) wrd[ii+1] = ' ';
  if (ii+2 <= jj) wrd[ii+2] = ' ';
}
/* Get bounds of nn'th number. */
eow=-1;
for (ii=0; ii<nn; ++ii) { 
  FindNumericWord(wrd,eow+1,&sow,&eow,&ok);
  if (ok == 0) { return(-1.e+40); }
}
/* Read value from "wrd". */
rr = cvalread(wrd,sow+1,eow+1);
return(rr);
}

/* ----------------------------------------------------------------------
  Calls GetLineValueQuiet()...
  Returns "-1.e+40" if there is a problem.
*/
/*@@*/
double GLVQ(char line[], int nn)
{
return( GetLineValueQuiet(line,nn) );
}
  
/* ----------------------------------------------------------------------
  Calls GetLineValue()...
  Returns "-1.e+40" if there is a problem.
*/
/*@@*/
double GLV(char line[], int nn)
{
return( GetLineValue(line,nn) );
}
  


/* ----------------------------------------------------------------------
  Read a line from a file.  Put a NULL at end of string.  
  Return 0 if EOF, 1 if ok.
*/
/*@@*/
int fgetline(char userline[], FILE *fu)
{
  char *ptr;
  char line[1000];
  int ii;
  ptr = fgets(line,sizeof(line),fu);
  line[strlen(line)-1] = '\0';
  ii = clc(line);
  line[ii+1] = '\0';
  strcpy(userline,line);
  if (ptr == '\0') { return(0); } else { return(1); }
}

/* ----------------------------------------------------------------------
  Read a real value from standard input.
*/
/*@@*/
float readrealstdin()
{
  float r;
  char line[100];
  fgets(line,sizeof(line),stdin);
  sscanf(line, "%f", &r) ;
  return(r);
}


/* ----------------------------------------------------------------------
  Given B-V, return effective stellar temperature in degrees Kelvin
  (main sequence, solar metallicity).  Data from "Bowers and Deeming" text 
  book and "Lejeune, Cuisinier, and Buser: A&ASupp 125, 229 (1997)."
*/
/*@@*/
double StarTempBmV(double userbv)
{
/**/
  double bv[27]=
{
-0.35 ,
-0.31 ,
-0.16 ,
-0.100,
-0.050,
+0.000,
+0.050,
+0.100,
+0.13 ,
+0.175,
+0.260,
+0.27 ,
+0.335,
+0.42 ,
+0.425,
+0.58 ,
+0.595,
+0.70 ,
+0.760,
+0.875,
+0.89 ,
+1.095,
+1.18 ,
+1.350,
+1.45 ,
+1.63 ,
+1.80
}; 
  double stp[27]=
{
 40000.,
 28000.,
 15500.,
 11750.,
 10200.,
  9500.,
  8870.,
  8600.,
  8500.,
  8000.,
  7500.,
  7400.,
  7000.,
  6580.,
  6500.,
  6030.,
  6000.,
  5520.,
  5500.,
  5000.,
  4900.,
  4500.,
  4130.,
  4000.,
  3480.,
  2800.,
  2400.
};
  double rr;
/**/
if (userbv <= bv[0] ) return(40000.);
if (userbv >= bv[26]) return(2400.);
rr = yinterp(27, bv, stp, userbv);
return(rr);
}


/* ----------------------------------------------------------------------
  Return the value of the black body function:
     flux(a,T,x) = a * ( (h*c*c)/x^5 ) / ( exp((h*c)/(k*T*x)) - 1 )
  given a, T, and x.
*/
/*@@*/
double BlackBodyAT(double a, double T, double x)
{
/**/
  const double c = 2.997925e+18 ;  /*  Ang/sec      */
  const double k = 1.380662e-16 ;  /*  ergs/Kelvin  */
  const double h = 6.626176e-27 ;  /*  ergs*sec     */
  double p1 = h*c*c;
  double p2 = h*c/k;
/**/
  double r;
/**/
  r = ( a * p1  / (x*x*x*x*x) ) / ( exp(p2/(T*x)) - 1. ) ;
  return(r);
}


/* ----------------------------------------------------------------------
  Return the integral value of the black body function in terms of:
    " photons / second / cm^2 "  between "lam1" and "lam2" given a
    temperature "Teff" and a flux value "flam0" (ergs/sec/cm^2/Angstrom)
    at a wavelength "lam0".
*/
/*@@*/
double IntegrateBlackBody(double lam1, double lam2, double Teff, 
                          double flam0, double lam0)
{
/**/
  const double c = 2.997925e+18 ;  /*  Ang/sec      */
  const double h = 6.626176e-27 ;  /*  ergs*sec     */
  double hc = h*c;
/**/
  double a, lam, pscm2;
/**/
/* Find value of scale factor "a". */
  a = flam0 / BlackBodyAT(1.,Teff,lam0);
/* Integrate photon flux between lam1 and lam2 to yield: photons/sec/cm^2  */
/* Note: photons/sec/cm^2/Angstrom == flam / (hc/lam)   */
  pscm2=0.;
  for (lam=lam1; lam<=lam2; lam=lam+0.1 ) {
    pscm2 = pscm2 + ( 0.1 * BlackBodyAT(a,Teff,lam) / (hc/lam) ) ;
  }
  return(pscm2);
}


/* ----------------------------------------------------------------------
  Return photons/second for the FUV and NUV bands of GALEX for a star given
   the magnitudes B and V.
  (Uses BlackBody integration.)
*/
/*@@*/
void BB_GalexRateBV(double B, double V, double *FUVrate, double *NUVrate)
{
/**/
  const double Aeff = 50.;   /* 50 cm effective area for GALEX. */
/**/
  double Teff,flam0,lam0,lam1,lam2;
/**/
/* Find star temperature. */
  Teff = StarTempBmV(B-V);
/* Find flam0 for 5550 Angstroms. */
  lam0 = 5550.;
  flam0 = pow( 10., (V+21.17)/(-2.5) );
/* Range for Galex Far-UV throughput. */
  lam1 = 1400.;  lam2 = 1800.;
  *FUVrate = Aeff * IntegrateBlackBody(lam1, lam2, Teff, flam0, lam0);
/* Range for Galex Near-UV throughput. */
  lam1 = 1800.;  lam2 = 2900.;
  *NUVrate = Aeff * IntegrateBlackBody(lam1, lam2, Teff, flam0, lam0);
  return;
}




/* ----------------------------------------------------------------------
  Given a temperature and two x,flux pairs, return the best possible fit
  for the value "a" using:
     flux(a,T,x) = a * ( (h*c*c)/x^5 ) / ( exp((h*c)/(k*T*x)) - 1 )
  Use:
     a = SUM { Yi * flux(1,T,Xi) } / SUM { flux(1,T,Xi)^2 }
*/
/*@@*/
double SolveBlackBodyA(double T, double xp[2], double yp[2])
{
/**/
  double a,sum1,sum2,x,flux;
  int ii,nn;
/**/

  printf("T=%e\n",T);

  a   =1.;
  nn  =2 ;
  sum1=0.;
  sum2=0.;
  for(ii=0; ii<nn; ++ii) {
    x = xp[ii];
    printf("nn=%d  a=%e  T=%e  x=%e\n",nn,a,T,x);
    flux = BlackBodyAT(a,T,x);
    printf("flux=%e\n",flux);
    sum1 = sum1 + ( yp[ii] * flux );
    sum2 = sum2 + ( flux * flux );
  }
  printf("sum1=%e   sum2=%e  1/2=%e\n",sum1,sum2,sum1/sum2);
  return(sum1/sum2);
}



/* ----------------------------------------------------------------------
  Used by gser() and gfc().
*/
/*@@*/
double gammln(double xx)
{
  double cof[6] = { 76.18009173, -86.50532033,     24.01409822,
                   -1.231739516,   0.120858003e-2, -0.536382e-5 };
  double stp = 2.50662827465 ;
  double half= 0.5;
  double one = 1.0;
  double fpf = 5.5;
  double x,tmp,ser;
  int j;
/**/
  x  = xx - one ;
  tmp= x + fpf ;
  tmp= ( (x+half) * log(tmp) ) - tmp ;
  ser= one ;
  for (j=0; j<=5; ++j) {
    x  = x + one ;
    ser= ser + (cof[j]/x) ;
  }
  return( tmp + log(stp*ser) );
}


/* ----------------------------------------------------------------------
  Returns the incomplete gamma function P(a,x) evaluated by its series
  representation as GAMSER.  Also returns ln(Gamma(a)) as GLN.
    Input: a,x.   Output: gamser,gln.
*/
/*@@*/
void gser(double a, double x, double *gamser, double *gln)
{
  double del,ap,sum;
  int ok,n;
/**/
  const double EPS = 3.e-7;
  const int ITMAX = 10000;
/**/
  *gln = gammln(a);
  if (x<=0.) {
    if (x<0.) printf("ERROR- gser: x < 0.\n");
    *gamser = 0.;
    return;
  }
  ap = a;
  sum= 1./a;
  del= sum;
  ok = 0;
  n  = 1;
  while (n<=ITMAX) {
    ap = ap + 1.;
    del= (del*x)/ap;
    sum= sum + del;
    if ( ABS((del)) < (ABS((sum))*EPS) ) { ok=1; n=ITMAX; }
    ++n;
  }
  if (ok==0) {
    printf("ERROR- gser: a too large(%f), ITMAX too small(%d)\n",a,ITMAX);
  }
  *gamser = sum * exp( (-1.*x) + (a*log(x)) - *gln );
  return;
}


/* ----------------------------------------------------------------------
  Returns the incomplete gamma function Q(a,x) evaluated by its continued
  fraction representation as GAMMCF.  Also returns Gamma(a) as GLN.
    Input: a,x.  Output: gammcf,gln.
*/
/*@@*/
void gcf(double a, double x, double *gammcf, double *gln)
{
  double a0,a1,b0,b1,fac,an,gold,ana,anf,g;
  int n,ok ;
/**/
  const int ITMAX = 10000 ;
  const double EPS = 3.e-7;
/**/
  *gln = gammln(a);
  g   = 0.;
  gold= 0.;
  a0  = 1.;
  a1  = x ;
  b0  = 0.;
  b1  = 1.;
  fac = 1.;
  ok  = 0 ;
  n   = 1 ;
  while ( n<=ITMAX ) {
    an  = n;
    ana = an - a ;
    a0  = (a1+(a0*ana))*fac ;
    b0  = (b1+(b0*ana))*fac ;
    anf = an*fac;
    a1  = (x*a0) + (anf*a1);
    b1  = (x*b0) + (anf*b1);
    if (a1 != 0.) {
      fac= 1./a1 ;
      g  = b1*fac;
      if (ABS(((g-gold)/g)) < EPS) { ok=1 ; n=ITMAX ; }
      gold=g;
    }
    ++n;
  }
  if (ok==0) {
    printf("ERROR- gcf: a too large(%f), ITMAX too small(%d)\n",a,ITMAX);
  }
  *gammcf = exp( (-1.*x) + (a*log(x)) - *gln ) * g ;
  return;
}

/* ----------------------------------------------------------------------
  Returns the incomplete gamma function Q(a,x) = 1 - P(a,x) .
    Input: a,x.
*/
/*@@*/
double gammq(double a, double x)
{
  double gamser,gammcf,gln,result;
/**/
  if ((x<0.)||(a<=0.)) {
    printf("ERROR- gammq: x<0 or a<=0:  x=%f  a=%f\n",x,a);
    exit(0);
/*
    return(0.);
*/
  }
/* Use the series representation and take its complement. */
  if (x<a+1.) {
    gser(a,x,&gamser,&gln);
    result = 1. - gamser;
  }
/* Use the continued fraction representation. */
  else {
    gcf(a,x,&gammcf,&gln);
    result = gammcf;
  }
  return(result);
}


/* ----------------------------------------------------------------------
   Given the degrees of freedom (nu) and the chi-squared statistic (chisq),
   returns the computed probability of agreement.
     Input: nu,chisq.
*/
/*@@*/
double chisq_test(double nu, double chisq)
{
  double prob;  
/**/
  prob = gammq(nu/2.,chisq/2.);
  return(prob);
}

/* ----------------------------------------------------------------------
  Return number of hits given an average count rate (ACR).  This uses a
  random number generator and the cumulative Poission Probability Function. 
  If the average count rate is greater than 100,000, then it will break it 
  up into groups of less then 100,000 to prevent overflow or numerical
  roundoff errors.      Input: ACR.
*/
/*@@*/
int Hits(double ACR)
{
/**/
/* Maximum hits per cycle is 1,000,000., but this should never be reached. */
  const double kmax = 1000000.;  
  double RandomNumber;    /* a random number between [0.0 , and 1.0) . */
  int ncycle;             /* number of cycles. */
  double rate;            /* rate for each cycle. */
  int total;              /* Total number of hits. */
  double rr,kk;
  int ii,k;
/**/
/* Break up accumulation of hits into cycles. */
  if ( ACR > 100000. ) {
    ncycle = 1 + (ACR/100000.);
    rr     = ncycle;
    rate   = ACR/rr;
  } else {
    ncycle = 1;
    rate   = ACR;
  }
/* Initialize total hits. */
  total=0;
/* Accumulate hits from each cycle. */
  for (ii=0; ii<ncycle; ++ii) {
/* 
   Compute cumulative Poission Probability Function for < kk hits given
   an average count rate of "rate".  Increase kk until probability is
   greater than "RandomNumber".
*/
    RandomNumber = drand48();   /* Value between 0. and 1., i.e. [0.0, 1.0) */
    kk = 1.;
    while (1) {
      if ( RandomNumber < gammq(kk,rate) ) break;
      if ( kk > kmax ) break;
      kk=kk+1.;
    }
    if (kk > kmax ) {
      fprintf(stderr,
 "ERROR: Hits: Over %f hits returned (should not happen).  Random number=%f \n",
 kmax,RandomNumber);
      return(-1);
    } 
    k = kk - 1.;           /* Subtract one and convert to integer. */
    total = total + k;     /* Accumulate. */
  }
  return(total);
}


/* ---------------------------------------------------------------------- */
/* Convert degrees to radians.  */
/*@@*/
double degtorad(double theta)
{
  const double pio180 = 0.017453293;   /* PI over 180 degrees. */
  return(theta*pio180);
}

/* ---------------------------------------------------------------------- */
/* Convert radians to degrees.  */
/*@@*/
double radtodeg(double theta)
{
  const double pio180 = 0.017453293;   /* PI over 180 degrees. */
  return(theta/pio180);
}


/* ----------------------------------------------------------------------
  Given a full-width-half-maximum (fwhm) for a gaussian distribution,
  Return a random position for radius and angle (theta) consistent with
  a normal distribution.   Input: fwhm.  Output: radius and theta (in radians).
*/
/*@@*/
void GaussianPosition(double fwhm, double *radius, double *theta)
{
/**/
  const double m4ln2 = -2.772588722;  /* -4 * (natural log of 2.) */
  const double twopi = +6.283185308;  /* 2.0 * pi */
  double volume,ma; 
/**/
/* Check for no FWHM. */
  if (fwhm < 1.e-30) { *radius = 0.;  *theta = 0.; }
/* Random radius. */
  volume = drand48();
  ma = m4ln2 / (fwhm*fwhm);
  *radius = sqrt( log(1.-volume) / ma );
/* Random Angle. */
  *theta = twopi*drand48();
  return;
}


/* ----------------------------------------------------------------------
  Returns a random number between a and b.  If a >= b, then
  randomizer is initialized.  Input: a,b.
  Call srand() before this to re-seed.
*/
/*@@*/
double doub_rand(double a, double b)
{
  double r;
  r = a + ((rand()/32767.0)*(b-a));
  r = MIN(MAX(r,a),b);
  return(r);
}



/* ----------------------------------------------------------------------
  Sort an array using "QuickSort" method.  
  Originally from NUMERICAL RECIPES (tab July 1988)
  Converted to C September 1999.
*/
/*@@*/
void cqcksrt(int nn, float arr[] )
{
/**/
const float fm = 7875.;
const float fa = 211.;
const float fc = 1663.;
const float fmi= 1.2698413e-4;
const int mm = 7;
const int nstack = 100;
int istack[100];
int jstack,ll,ir,j,i,iq,ii,jj,flag1;
float fx,aa;
/**/
/* Initialize. */
jstack=0; ll=1; ir=nn; fx=0.;
/* Big loop. */
while(1) {
if (ir-ll < mm) {
  for (j=ll+1; j<=ir; ++j) {
    aa=arr[j -1];
    flag1=0;
    for (i=j-1; i>=1; i=i-1) {
      if (arr[i -1] <= aa) { flag1=1; break; }
      arr[i+1 -1]=arr[i -1];
    }
    if (flag1 == 0) i=0;
    arr[i+1 -1]=aa;
  }
  if (jstack == 0) return;   /* Exit routine here. */
  ir= istack[jstack -1];
  ll= istack[jstack-1 -1];
  jstack = jstack-2;
} else {
  i=ll; j=ir;
  ii= ((fx*fa)+fc);
  jj= (fm);
  fx= ii%jj;
  iq= ll + ( ((ir-ll)+1) * (fx*fmi) );
  aa = arr[iq -1];
  arr[iq -1] = arr[ll -1];
  while (1) {
    while (j > 0) {
      if (aa >= arr[j -1]) break;
      j=j-1;
    }
    if (j <= i) {
      arr[i -1] = aa;
      break;
    }
    arr[i -1] = arr[j -1];
    i=i+1;
    while (i <= nn) {
      if (aa <= arr[i -1]) break;
      i=i+1;
    }
    if (j <= i) {
      arr[j -1]=aa;
      i=j;
      break;
    }
    arr[j -1]=arr[i -1];
    j=j-1;
  }
  jstack=jstack+2;
  if (jstack > nstack) printf("ERROR: qcksrt: NSTACK must be made larger.\n");
  if ((ir-i) >= (i-ll)) {
    istack[jstack -1]=ir;
    istack[jstack-1 -1]=i+1;
    ir=i-1;
  } else {
    istack[jstack -1]  = i-1;
    istack[jstack-1 -1]= ll;
    ll=i+1;
  }
}     /* if block */
}     /* while loop */
}


/* ----------------------------------------------------------------------
  THIS VERSION USES A double precision ARRAY.
  Sort an array using "QuickSort" method.  
  Originally from NUMERICAL RECIPES (tab July 1988)
  Converted to C September 1999.
*/
/*@@*/
void cqcksrt8(int nn, double arr[] )
{
/**/
const double fm = 7875.;
const double fa = 211.;
const double fc = 1663.;
const double fmi= 1.2698413e-4;
const int mm = 7;
const int nstack = 100;
int istack[100];
int jstack,ll,ir,j,i,iq,ii,jj,flag1;
double fx,aa;
/**/
/* Initialize. */
jstack=0; ll=1; ir=nn; fx=0.;
/* Big loop. */
while(1) {
if (ir-ll < mm) {
  for (j=ll+1; j<=ir; ++j) {
    aa=arr[j -1];
    flag1=0;
    for (i=j-1; i>=1; i=i-1) {
      if (arr[i -1] <= aa) { flag1=1; break; }
      arr[i+1 -1]=arr[i -1];
    }
    if (flag1 == 0) i=0;
    arr[i+1 -1]=aa;
  }
  if (jstack == 0) return;   /* Exit routine here. */
  ir= istack[jstack -1];
  ll= istack[jstack-1 -1];
  jstack = jstack-2;
} else {
  i=ll; j=ir;
  ii= ((fx*fa)+fc);
  jj= (fm);
  fx= ii%jj;
  iq= ll + ( ((ir-ll)+1) * (fx*fmi) );
  aa = arr[iq -1];
  arr[iq -1] = arr[ll -1];
  while (1) {
    while (j > 0) {
      if (aa >= arr[j -1]) break;
      j=j-1;
    }
    if (j <= i) {
      arr[i -1] = aa;
      break;
    }
    arr[i -1] = arr[j -1];
    i=i+1;
    while (i <= nn) {
      if (aa <= arr[i -1]) break;
      i=i+1;
    }
    if (j <= i) {
      arr[j -1]=aa;
      i=j;
      break;
    }
    arr[j -1]=arr[i -1];
    j=j-1;
  }
  jstack=jstack+2;
  if (jstack > nstack) printf("ERROR: qcksrt: NSTACK must be made larger.\n");
  if ((ir-i) >= (i-ll)) {
    istack[jstack -1]=ir;
    istack[jstack-1 -1]=i+1;
    ir=i-1;
  } else {
    istack[jstack -1]  = i-1;
    istack[jstack-1 -1]= ll;
    ll=i+1;
  }
}     /* if block */
}     /* while loop */
}


/* ----------------------------------------------------------------------
  Sort an array using "QuickSort" method.  
  This version sorts a key[] array.  The original arr[] is not changed.
  This version uses a float precision array.
  Originally from NUMERICAL RECIPES (tab July 1988), converted to C Sept. 1999.
  The values of key[] are array element positions within arr[].  WARNING: Be 
  sure that all values of key[] are within the array dimension bounds of arr[].
    Input: arr[], nkey, key[].    Output: key[] (sorted).
*/
/*@@*/
void cqcksrtkey4(float arr[], int nkey, int key[] )
{
/**/
const float fm = 7875.;
const float fa = 211.;
const float fc = 1663.;
const float fmi= 1.2698413e-4;
const int mm = 7;
const int nstack = 100;
int istack[100];
int jstack,ll,ir,j,i,iq,ii,jj,kk,flag1;
float fx;
/**/
/* Initialize. */
jstack=0; ll=1; ir=nkey; fx=0.;
/* Big loop. */
while(1) {
if (ir-ll < mm) {
  for (j=ll+1; j<=ir; ++j) {
    kk=key[j -1];
    flag1=0;
    for (i=j-1; i>=1; i=i-1) {
      if (arr[key[i -1]] <= arr[kk]) { flag1=1; break; }
      key[i+1 -1]=key[i -1];
    }
    if (flag1 == 0) i=0;
    key[i+1 -1]=kk;
  }
  if (jstack == 0) return;   /* Exit routine here. */
  ir= istack[jstack -1];
  ll= istack[jstack-1 -1];
  jstack = jstack-2;
} else {
  i=ll; j=ir;
  ii= ((fx*fa)+fc);
  jj= (fm);
  fx= ii%jj;
  iq= ll + ( ((ir-ll)+1) * (fx*fmi) );
  kk = key[iq -1];
  key[iq -1] = key[ll -1];
  while (1) {
    while (j > 0) {
      if (arr[kk] >= arr[key[j -1]]) break;
      j=j-1;
    }
    if (j <= i) {
      key[i -1] = kk;
      break;
    }
    key[i -1] = key[j -1];
    i=i+1;
    while (i <= nkey) {
      if (arr[kk] <= arr[key[i -1]]) break;
      i=i+1;
    }
    if (j <= i) {
      key[j -1]=kk;
      i=j;
      break;
    }
    key[j -1]=key[i -1];
    j=j-1;
  }
  jstack=jstack+2;
  if (jstack > nstack) printf("ERROR: qcksrt: NSTACK must be made larger.\n");
  if ((ir-i) >= (i-ll)) {
    istack[jstack -1]=ir;
    istack[jstack-1 -1]=i+1;
    ir=i-1;
  } else {
    istack[jstack -1]  = i-1;
    istack[jstack-1 -1]= ll;
    ll=i+1;
  }
}     /* if block */
}     /* while loop */
}



/* ----------------------------------------------------------------------
  Sort an array using "QuickSort" method.  
  This version sorts a key[] array.  The original arr[] is not changed.
  This version uses a double precision array.
  Originally from NUMERICAL RECIPES (tab July 1988), converted to C Sept. 1999.
  The values of key[] are array element positions within arr[].  WARNING: Be 
  sure that all values of key[] are within the array dimension bounds of arr[].
    Input: arr[], nkey, key[].    Output: key[] (sorted).
*/
/*@@*/
void cqcksrtkey8(double arr[], int nkey, int key[] )
{
/**/
const double fm = 7875.;
const double fa = 211.;
const double fc = 1663.;
const double fmi= 1.2698413e-4;
const int mm = 7;
const int nstack = 100;
int istack[100];
int jstack,ll,ir,j,i,iq,ii,jj,kk,flag1;
double fx;
/**/
/* Initialize. */
jstack=0; ll=1; ir=nkey; fx=0.;
/* Big loop. */
while(1) {
if (ir-ll < mm) {
  for (j=ll+1; j<=ir; ++j) {
    kk=key[j -1];
    flag1=0;
    for (i=j-1; i>=1; i=i-1) {
      if (arr[key[i -1]] <= arr[kk]) { flag1=1; break; }
      key[i+1 -1]=key[i -1];
    }
    if (flag1 == 0) i=0;
    key[i+1 -1]=kk;
  }
  if (jstack == 0) return;   /* Exit routine here. */
  ir= istack[jstack -1];
  ll= istack[jstack-1 -1];
  jstack = jstack-2;
} else {
  i=ll; j=ir;
  ii= ((fx*fa)+fc);
  jj= (fm);
  fx= ii%jj;
  iq= ll + ( ((ir-ll)+1) * (fx*fmi) );
  kk = key[iq -1];
  key[iq -1] = key[ll -1];
  while (1) {
    while (j > 0) {
      if (arr[kk] >= arr[key[j -1]]) break;
      j=j-1;
    }
    if (j <= i) {
      key[i -1] = kk;
      break;
    }
    key[i -1] = key[j -1];
    i=i+1;
    while (i <= nkey) {
      if (arr[kk] <= arr[key[i -1]]) break;
      i=i+1;
    }
    if (j <= i) {
      key[j -1]=kk;
      i=j;
      break;
    }
    key[j -1]=key[i -1];
    j=j-1;
  }
  jstack=jstack+2;
  if (jstack > nstack) printf("ERROR: qcksrt: NSTACK must be made larger.\n");
  if ((ir-i) >= (i-ll)) {
    istack[jstack -1]=ir;
    istack[jstack-1 -1]=i+1;
    ir=i-1;
  } else {
    istack[jstack -1]  = i-1;
    istack[jstack-1 -1]= ll;
    ll=i+1;
  }
}     /* if block */
}     /* while loop */
}


/* ----------------------------------------------------------------------
  Find median of an array of float (4 byte real) numbers.
  Input: arr[] (array of values which will be sorted).
         nn   (number of elements in the array).
  Returns the median value.
*/
/*@@*/
float cfind_median(int nn, float arr[] )
{
/**/
float median;
int kk;
/**/
/* Check. */
if (nn < 1) { return(0.); }
/* Sort the array. */
cqcksrt(nn,arr);
kk = (nn/2);
if ((nn%2) == 0) {
  median = ( arr[kk] + arr[(kk-1)] ) / 2.;   /* Even number of elements. */
} else {
  median = arr[kk];                          /* Odd number of elements. */
}
return(median);
}

/* ----------------------------------------------------------------------
  Find median of an array of float (4 byte real) numbers.
  THIS VERSION DOES NOT DESTROY THE ORIGINAL ARRAY.
  Input: nn    (number of elements in the array).
         arr[] (array of data values).
         scr[] (scratch array).
  Returns the median value.
*/
/*@@*/
float cfind_median_scratch(int nn, float arr[], float scr[] )
{
/**/
float median;
int ii,kk;
/**/
/* Check. */
if (nn < 1) { return(0.); }
/* Copy. */
for (ii=0; ii<nn; ++ii) { scr[ii] = arr[ii]; }
/* Sort the scratch array. */
cqcksrt(nn,scr);
kk = (nn/2);
if ((nn%2) == 0) {
  median = ( scr[kk] + scr[(kk-1)] ) / 2.;   /* Even number of elements. */
} else {
  median = scr[kk];                          /* Odd number of elements. */
}
return(median);
}

/* ----------------------------------------------------------------------
  Find median of an array of double (8 byte real) numbers.
  Input: arr[] (array of values which will be sorted).
         nn   (number of elements in the array).
  Returns the median value.
*/
/*@@*/
double cfind_median8(int nn, double arr[] )
{
/**/
double median;
int kk;
/**/
/* Check. */
if (nn < 1) { return(0.); }
/* Sort the array. */
cqcksrt8(nn,arr);
kk = (nn/2);
if ((nn%2) == 0) {
  median = ( arr[kk] + arr[(kk-1)] ) / 2.;   /* Even number of elements. */
} else {
  median = arr[kk];                          /* Odd number of elements. */
}
return(median);
}

/* ----------------------------------------------------------------------
  Find median of an array of double (8 byte real) numbers.
  THIS VERSION DOES NOT DESTROY THE ORIGINAL ARRAY.
  Input: nn    (number of elements in the array).
         arr[] (array of data values).
         scr[] (scratch array).
  Returns the median value.
*/
/*@@*/
double cfind_median_scratch8(int nn, double arr[], double scr[] )
{
/**/
float median;
int ii,kk;
/**/
/* Check. */
if (nn < 1) { return(0.); }
/* Copy. */
for (ii=0; ii<nn; ++ii) { scr[ii] = arr[ii]; }
/* Sort the scratch array. */
cqcksrt8(nn,scr);
kk = (nn/2);
if ((nn%2) == 0) {
  median = ( scr[kk] + scr[(kk-1)] ) / 2.;   /* Even number of elements. */
} else {
  median = scr[kk];                          /* Odd number of elements. */
}
return(median);
}




/* ----------------------------------------------------------------------
  Find median of an array of float (4 byte real) numbers.
  Input: arr[] (array of values which will NOT be sorted).
         nkey  (number of elements in the key array).
         key[] (key array containing array element positions within arr[]).
  Returns the median value of the key'ed subset of values.
*/
/*@@*/
float cfind_median_key(float arr[], int nkey, int key[] )
{
/**/
float median;
int kk;
/**/
/* Check. */
if (nkey < 1) { return(0.); }
/* Sort the key array. */
cqcksrtkey4(arr, nkey, key );
kk = (nkey/2);
if ((nkey%2) == 0) {
  median = ( arr[key[kk]] + arr[key[(kk-1)]] ) / 2.;   /* Even number. */
} else {
  median = arr[key[kk]];                               /* Odd number. */
}
return(median);
}



/* ----------------------------------------------------------------------
  Convert an ASCII file to a PostScript file using Courier fonts.
  "land" : Use landscape mode.  "pt=n" : Specify pointsize n (default 10).
  "bold" : Use Courier-Bold instead of Courier (warning: different aspect).
  "head" : Put header at top of file, i.e. filename and date.
  "left=n" : Shift left n points (n = 70 = one inch).
 
  ptsz char/line line/page   LANDSCAPE: char/line line/page'
   12 ..  79  .....  55  ................  105  ...  40'
   10 ..  92  .....  66  ................  126  ...  48'
    9 .. 107  .....  73  ................  142  ...  53'
    8 .. 117  .....  82  ................  160  ...  60'
    7 .. 131  .....  94  ................  183  ...  68'
    6 .. 170  ..... 110  ................  210  ...  80'
 
  If psfile is blank, send to standard output.
*/
/*@@*/
void cASCII_to_PS(char afile[], char psfile[], 
                 int ptsz, int left, int bold, int land,int head)
{
/**/
FILE *infu;
FILE *oufu;
int nlpp,hpos,lno,ii,kk;
char line[800], header[800];
char line1[800], line2[800];
float lss = 1.1;    /* Line skip scaling factor. */
/**/

/* Form header. */
strcpy(header,":: ");
strcat(header,afile);
strcat(header," ::");

/* Open ASCII file for reading. */
infu = fopen(afile,"r");

/* Open PostScript file for writing. */
oufu = fopen(psfile,"w");

/* Number of lines per page. */
if (land == 1) {
  nlpp = cint(530. / (ptsz * lss));
} else {
  nlpp = cint(730. / (ptsz * lss));
}

/* Write beginning stuff. */
fprintf(oufu,"%c!PS-Adobe-2.0\n",'%');
fprintf(oufu,"/ptsz %d def\n",ptsz);
fprintf(oufu,"/lskip ptsz %f mul def\n",lss);
fprintf(oufu,"/landscape{90 rotate 000 -800 translate}def\n");
fprintf(oufu,"/vinit 754 def\n");

/* Horizontal positioning */
if (ptsz < 10) {
  hpos = 20 + left;
} else {
  hpos = 30 + left;
}
fprintf(oufu,"/hpos %d def\n",hpos);

/* More stuff. */
fprintf(oufu,"/newline{/vpos vpos lskip sub def hpos vpos moveto}def\n");

/* Land scape? */
if (land == 1) {
  fprintf(oufu,"/newpage{/vpos vinit def hpos vpos moveto landscape newline}def\n");
} else {
  fprintf(oufu,"/newpage{/vpos vinit def hpos vpos moveto}def\n");
}

/* More stuff. */
fprintf(oufu,"/l{show newline}def\n");

/* Bold? */
if (bold == 1) {
  fprintf(oufu,"/Courier-Bold findfont\n");
} else {
  fprintf(oufu,"/Courier findfont\n");
}
fprintf(oufu,"ptsz scalefont\n");
fprintf(oufu,"setfont\n");
fprintf(oufu,"newpage\n");

/* start text */
lno=1;

/* Add header to top. */
if (head == 1) {
  fprintf(oufu,"( %s )l\n",header);
  ++lno;
}

/* Print lines */
while (fgetline(line,infu) == 1) {

/* Correct for special characters ), (, and \ .  */
  ii=0;
  while (ii <= clc(line)) {
    kk = strlen(line) - 1;
    if ((line[ii] == ')') || (line[ii] == '(') || (line[ii] == '\\')) {
      substrcpy(line,0,ii-1,line1,0);
      line1[ii] = '\0';
      substrcpy(line,ii,kk,line2,0);
      line2[1 + kk - ii] = '\0';
      strcpy(line,line1);
      strcat(line,"\\");
      strcat(line,line2);
      ++ii;
    }
    ++ii;
  }
  fprintf(oufu,"(%s)l\n",line);
  if (lno%nlpp == 0) {
    fprintf(oufu,"showpage\n");
    fprintf(oufu,"newpage\n");
    lno=1;
  } else {
    ++lno;
  }
}

/* Finish it off. */
fprintf(oufu,"showpage\n");

/* Close files. */
fclose(infu);
fclose(oufu);

return;
}



/* ----------------------------------------------------------------------
  Given a array of monotonically increasing values ( xx[] ) and a value
  ( xval ), return the "pixel" position within the array.  For example, if 
  "xval" exactly equals the third element array value, the pixel would be
  3.000, if it is exactly between the third and fourth element it will be
  3.500, and it will be an interpolated value at other positions.
  Returns "-9.0" if out-of-range.  Assumes there are at least two pixels.
*/
/*@@*/
double GetPixelPosition( float xx[], int nn, double xval )
{
/**/
int ii,i1;
double b1,b2,pp;
/**/
/* Find bracketting pixels. */
/* Less than first pixel. */
if (xval < xx[0] ) {
  i1 =-1;
  b1 = xx[0] - (xx[1] - xx[0]);
  b2 = xx[0];
  if (xval < b1) return(-1.);
} else {
if (xval > xx[nn-1] ) {
  i1 = nn-1;
  b1 = xx[nn-1];
  b2 = xx[nn-1] + (xx[nn-1] - xx[nn-2]);
  if (xval > b2) return(-1.);
} else {
/* Closest element to xval. */
  ii = cneari_bs4( xval, nn, xx );
  if (xval > xx[ii]) {
    i1 = ii;
    b1 = xx[ii];
    b2 = xx[ii+1];
  } else {
    i1 = ii-1;
    b1 = xx[ii-1];
    b2 = xx[ii];
  }
}}
/* Interpolate. */
pp = i1 + ( (xval - b1) / (b2 - b1) );
return( pp );
}


/* ----------------------------------------------------------------------
  Find average flux and it's error (per unit original bin) between the 
  element numbers pix1 and pix2 in array ff[] and ee[].  If any pixels
  within the range have ee[] < 0, then ignore that pixel.
  Pixels are weighted by fraction of pixel within the bounds (no reference
  to pixel-to-pixel error variations.)
  The "sumit" switch allows summing rather than averaging.

  Input:  ff[]       spectrum.                                     
          ee[]       error spectrum.                               
          pix1       first bound in terms of element array numbers.
          pix2       second bound in terms of element array numbers.
          sumit      0=average, 1=sum.

  Output: fval       flux value between bounds.                    
          eval       error value between bounds.                  
*/
/*@@*/
void FluxAverage(float ff[], float ee[], double pix1, double pix2,
                 int sumit, double *fval, double *eval )
{
/**/
double sum,esum,wsum,b1,b2;
int more,ii,i0,i1,i2;
/**/

/* Check bounds. */
if (pix1 >= pix2) {
  fprintf(stderr,"ERROR: FluxAverage: pix1>=pix2: pix1=%f pix2=%f\n",pix1,pix2);
  exit(0);
} 

/* Check for any bad pixels. */
more=1;
for (ii=cnint(pix1); ii<=cnint(pix2); ++ii) {
  if (ee[ii] <= 0.) more=0;
}
if (more == 0) {
  *fval = 0.;
  *eval =-1.;
  return;
}

/* Initialize sum and weight. */
sum = 0.;
esum= 0.;
wsum= 0.;

/* Whole pixels. */
i1 = cnint(pix1) + 1;
i2 = cnint(pix2) - 1;
for (ii=i1; ii<=i2; ++ii) {
  sum = sum + ff[ii];
  esum= esum+ (ee[ii] * ee[ii]);  /* Errors averaged in quadrature(variance).*/
  wsum= wsum+ 1.;
}

/* More to calculate. */
more = 1;

/* Left fractional pixel. */
b1 = pix1;
b2 = cnint(pix1) + 0.5;
if (b2 > pix2) {
  b2 = pix2;
  more = 0;
}
i0  = cnint(pix1);
sum = sum + (b2-b1)*ff[i0];
esum= esum+ (b2-b1)*(ee[i0] * ee[i0]);
wsum= wsum+ (b2-b1);

/* Right fractional pixel. */
if (more == 1) {
  b2  = pix2;
  b1  = cnint(pix2) - 0.5;
  i0  = cnint(pix2);
  sum = sum + (b2-b1)*ff[i0];
  esum= esum+ (b2-b1)*(ee[i0] * ee[i0]);
  wsum= wsum+ (b2-b1);
}

/* Average flux per unit original bin.                   */
/* Note: if pix2 > pix1, then wsum should always be > 0. */
/* Set weighting sum to 1 if sum requested.              */
if (sumit == 1) wsum=1.;
*fval = sum / wsum;
*eval = sqrt(esum) / wsum;

/* Special note. */
/*
  The derivation of the variance of  F = sum( Wi Ai ) / sum( Wi ) is
  var(F) = sum( Wi * Wi * var(Ai) ) / [ sum( Wi ) * sum( Wi ) ] , however,
  in cases of fractional pixels this underestimates the error since it neglects
  the correlation between adjacent bins.  We use the following equation:
  var(F) = sum( Wi * var(Ai) ) / [ sum( Wi ) * sum( Wi ) ]  instead, which is
  incorrect but probably is a better practical estimate of the true error. 
  Note that in the case of whole pixels the equations are the same.

  Imagine the case of a flat spectrum with the same error for each pixel.
  If we use the "correct" formula for the variance, the S/N will effectively
  increase depending on how the bin boundaries fall.  The "incorrect"
  formula used here, maintains the "correct" S/N for any new pixel size.  
  That is, the formula used in this code is designed to preserve the S/N such 
  that the next analyses will be correct if it assumes the pixels are 
  independent (which they are not, but in practice most analyses programs 
  will probably assume this).  Nevertheless, this rebinning may result in a 
  slight over-estimate of the final errors, which may need to be corrected 
  later in some programs (such as line-fitting with "VPFIT").
*/

return;
}


/* ----------------------------------------------------------------------
  Find sum of flux between two element number boundaries.
  Input:  ff[]       spectrum.                                     
          pix1       first bound in terms of element array numbers.
          pix2       second bound in terms of element array numbers.
  Output:  (Returns sum of flux between bounds).
*/
/*@@*/
double FluxSum( double ff[], double pix1, double pix2 )
{
/**/
double sum,b1,b2;
int more,ii,i0,i1,i2;
/**/
/* Check bounds. */
if (pix1 >= pix2) { return(0.); } 
/* Initialize sum and weight. */
sum = 0.;
/* Whole pixels. */
i1 = cnint(pix1) + 1;
i2 = cnint(pix2) - 1;
for (ii=i1; ii<=i2; ++ii) {
  sum = sum + ff[ii];
}
/* More to calculate. */
more = 1;
/* Left fractional pixel. */
b1 = pix1;
b2 = cnint(pix1) + 0.5;
if (b2 > pix2) {
  b2 = pix2;
  more = 0;
}
i0  = cnint(pix1);
sum = sum + (b2-b1)*ff[i0];
/* Right fractional pixel. */
if (more == 1) {
  b2  = pix2;
  b1  = cnint(pix2) - 0.5;
  i0  = cnint(pix2);
  sum = sum + (b2-b1)*ff[i0];
}
return( sum );
}


/* ----------------------------------------------------------------------
  Smooth an array of (double precision) numbers using a boxcar.
  Input:   radius : radius of boxcar.
           num    : number of points.
  In/Out:  arr    : Array to be smoothed.
  Scratch: scr    : Scratch array.
*/
/*@@*/
void SmoothArray8( int radius, int num, double arr[], double scr[] )
{
/**/
int ii,jj;
double sum,wsum;
/**/
/* Copy. */
for (ii=0; ii<num; ++ii) { scr[ii] = arr[ii]; }
/* Smooth. */
for (ii=0; ii<num; ++ii) {
  sum =0.;
  wsum=0.;
  for (jj=(ii-radius); jj<=(ii+radius); ++jj) {
    if ((jj>=0)&&(jj<num)) {
      sum = sum + scr[jj];
      wsum= wsum+ 1.;
    }
  }
  if (wsum > 0.) arr[ii] = sum / wsum;
}
return;
}
    

/* ----------------------------------------------------------------------
  Search SAO catalog for V<7 stars.
   Input: ra      : Center Right Ascension in decimal degrees.
          dec     : Center Declination in decimal degrees.
          radius  : Radius in decimal degrees.
          vmax    : Maximum V magnitude.
          outfile : Filename of output file (or "stdout").
*/
/*@@*/
void tab_search_sao( double ra, double dec, double radius, double vmax,
                     char outfile[] )
{
/**/
char saofile[100];
char line[500];
char st[2];
double V,Teff,sra,sdec;
FILE *infu;
FILE *outfu;
/**/
double const pi = 3.141592654;
double const pio180 = pi/180.;   /* PI over 180 degrees. */
/**/
/* SAO Catalog with stars V < 7.0 magnitude. */
strcpy(saofile,"/home/galex/catalogs/sao/saovl7.dat");
/* Open files. */
infu = fopen(saofile,"r");
if (infu == NULL) { 
  fprintf(stderr,"***ERROR: cannot open SAO file: %s .\n",saofile); 
  exit(0);
}
if (strcmp(outfile,"stdout") == 0) {
 outfu = stdout;
} else {
  outfu = fopen(outfile,"w");
  if (outfu == NULL) { 
    fprintf(stderr,"***ERROR: cannot open SAO output file: %s .\n",outfile); 
    exit(0);
  }
}
/* Read stars. */
while ( fgetline(line,infu) == 1 ) {
  sra =  cvalread(line, 184, 193) / pio180;   /* decimal degrees */
  sdec=  cvalread(line, 194, 204) / pio180;   /* decimal degrees */
  if ( cangsep(ra,dec,sra,sdec) < radius ) {
    V = cvalread(line, 81, 84);               /* V magnitude */
    if ( V < vmax ) {
      st[0] = line[84];                       /* Spectral type. */
      st[1] = line[85];
      Teff  = TypeToTemp(st);                 /* Estimate temperature. */
      fprintf(outfu,"%11.6f %11.6f %5.1f %6.0f  %c%c\n",
                                sra,sdec,V,Teff,st[0],st[1]);
    }
  }
}
fclose(infu);
if (strcmp(outfile,"stdout") != 0) { fclose(outfu); }
return;
}

/* ----------------------------------------------------------------------
   Take a combined ACT, USNOA, and SAO star list file and delete the 
   duplicate entries.
*/
/*@@*/
void tab_starweed( char infile[], char outfile[], double radius, 
                  double RAo, double DECo, int actonly, int showheader )
{
/**/
char line[100];
/**/
FILE *infu;
FILE *outfu;
/**/
int count,ii,j0,j1,kk,num,ptr,ok;
/**/
const int maxstar = 900000;
double diff;
/**/
double *ra;
double *dec;
char *lines;
int *id;
int *key;
/**/
/* Check. */
if ((radius < -900.)||(RAo < -900.)||(DECo < -900.)) {
  fprintf(stderr,"***ERROR: starweed: Need radius= , ra= , and dec= .\n");
  exit(0);
}
/* Allocate memory. */
if (!( ra = (double *)calloc(maxstar,sizeof(double)) )) {
  fprintf(stderr,"***ERROR: starweed: Out of memory for ra array.\n");
  exit(0);
}
if (!( dec = (double *)calloc(maxstar,sizeof(double)) )) {
  fprintf(stderr,"***ERROR: starweed: Out of memory for dec array.\n");
  exit(0);
}
if (!( lines = (char *)calloc((100*maxstar),sizeof(char)) )) {
  fprintf(stderr,"***ERROR: starweed: Out of memory for lines array.\n");
  exit(0);
}
if (!( id = (int *)calloc((maxstar),sizeof(int)) )) {
  fprintf(stderr,"***ERROR: starweed: Out of memory for id array.\n");
  exit(0);
}
if (!( key = (int *)calloc((maxstar),sizeof(int)) )) {
  fprintf(stderr,"***ERROR: starweed: Out of memory for key array.\n");
  exit(0);
}
/* Open input file. */
infu = fopen(infile,"r");
if (infu == NULL) {
  fprintf(stderr,"***ERROR: starweed: cannot open input file.\n"); 
  exit(0);
}
/* Initialize. */
num=0;
/* Read all lines. */
while( fgetline(line,infu) == 1) {
  ra[num] = GetLineValue(line,1);
  dec[num]= GetLineValue(line,2);
  id[num] = GetLineValue(line,5);
  ptr = num*100;
  substrcpy(line,0,98,lines,ptr);
  key[num]= num;
  ok=1;
/* Outside radius? */  
  if (cangsep(ra[num],dec[num],RAo,DECo) > radius) ok=0;
/* ACT only and not an ACT star? */  
  if ((actonly == 1)&&(id[num] > 199999)) ok=0;
/* Include. */
  if (ok == 1) ++num;
/* Check. */
  if (num > maxstar) {
    fprintf(stderr,"***ERROR: starweed: too many sources, num>%d .\n",num);
    exit(0);
  }
}
/* Close files. */
fclose(infu);
printf("Original number of sources within radius is %d .\n",num);
/* Sort key by DEC. */
cqcksrtkey8(dec,num,key);
/* Go through list. */
for (ii=1; ii<num; ++ii) {
  j0 = key[ii];
  kk  = 1;
  j1 = key[ii-kk];
/* Look backwards for close sources.  "dec[j0]" always larger because of sort. */
  while( (dec[j0]-dec[j1]) < 0.0016667 ) {
    if (id[j1] > -1) {
      diff = cangsep(ra[j0],dec[j0],ra[j1],dec[j1]);
      if (diff < 0.0016667) {
        if (id[j1] > id[j0]) {
/*        printf("## 1:deleting %d because of %d  %f\n",id[j1],id[j0],diff*3600.); */
          id[j1] = -1;
        } else {
/*        printf("## 0:deleting %d because of %d  %f\n",id[j0],id[j1],diff*3600.); */
          id[j0] = -1;
          break;
        }
      }
    }
    ++kk;
    if ((ii-kk) < 0) break;
    j1  = key[ii-kk];
  }
}
/* Open output file. */
outfu = fopen(outfile,"w");
if (outfu == NULL) {
  fprintf(stderr,"***ERROR: starweed: cannot open output file.\n"); 
  exit(0);
}
/* Write header if requested. */
if (showheader == 1) {
/*              155.2747690   29.7099940    5908.114  14.938   300061  20.119   7.747e-03   1.678e+00   */
fprintf(outfu,"| RA2000    | DEC2000    | Teff      | Vmag  | ID     |Mag2200| FUV       | NUV       | Radius   |\n");
fprintf(outfu,"| d         | d          | d         | d     | i      | d     | d         | d         | d        |\n");
}
/* Write all good sources. */
count=0;
for (ii=0; ii<num; ++ii) {
  kk=key[ii];
  if (id[kk] > -1) {
    ptr = kk*100;
    substrcpy(lines,ptr,ptr+98,line,0);
    line[99]='\0'; kk=clc(line); line[kk+1]='\0';
    fprintf(outfu,"%s\n",line); 
    ++count;
  }
}
printf("     New number of sources is %d (deleted %d or %5.2f%c).\n",
                            count,num-count,(100.*(num-count)/count),'\045');
/* Close output file. */
fclose(outfu);
/* Free memory. */
free(ra);      ra   = NULL;
free(dec);     dec  = NULL;
free(lines);   lines= NULL;
free(id);      id   = NULL;
free(key);     key  = NULL;
return;
}

/* ----------------------------------------------------------------------
  Return photons/second for a given band (based on arrays) for GALEX for 
  a star given the V magnitude and an effective temperature.
   (Uses Pegase Synthetic Spectra Results.)
*/
/*@@*/
double tab_starlist_GalexRateV( double V, double UserTeff, 
                        int num, double teff[], double ppstot[] )
{
/**/
double Teffclip,rate;
/**/
/* Find star temperature.  Clip at minimum and maximum limits. */
Teffclip = UserTeff;
if (Teffclip <   2885.) Teffclip =  2885.;
if (Teffclip > 179800.) Teffclip =179800.;
/* Interpolate to get flux and scale to given V magnitude. */
rate = yinterp(num, teff, ppstot, Teffclip) / pow(10.,(V/2.5));
return(rate);
}

