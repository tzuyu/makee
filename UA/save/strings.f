
C
C
C   THIS VERSION MODIFIED FOR LINUX OPERATING SYSTEMS.
C
C


C strings.f                      - for sun - tab - sep91, jun93
C Generalized the subroutines to handle any length string.
C Included WRITE80() for MS FORTRAN.
C
C      character leftjust(c)
C      subroutine newdate(s,mode)
C      integer*4 function revindex(c,s)
C      subroutine repstr(c,j)
C      subroutine allcaps(c)
C      subroutine upper_case(c)
C      subroutine lower_case(c)
C      subroutine strpos(c,fc,lc)
C      subroutine write80(c,j)
C      logical function getfile(filename,iun)
C      integer*4 function lastchar(s)
C      integer*4 function lc(s)
C      character rf5, rf7, rf9, rf9
C      character drf5, drf7, drf9, drf9
C

C------------------------------------------------------------------------
      character*(*) function leftjust(c)
      implicit none
      character*(*) c
      integer*4 fc,lc
      call strpos(c,fc,lc)
      leftjust = ' '
      leftjust = c(fc:lc)   
      return
      end  

C------------------------------------------------------------------------
C If mode=1 changes character date (03SEP92) to numeric date (9200903).
C If mode=2 changes numeric date (9200903) to character date (03SEP92).
C
      logical function newdate(s,mode)

      implicit none
      integer*4 i,mode
      character*(*) s
      character*3 month(12),m
      real*8 r8,fgetlinevalue
C
      data month / 'JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG',
     .             'SEP','OCT','NOV','DEC' /
C
      newdate = .false.
      if (len(s).ne.7) return
      if (mode.eq.1) then
        m=s(3:5)
        do i=1,12
          if (m.eq.month(i)) write(s(3:5),'(i3.3)') i
        enddo
        if (m.eq.s(3:5)) goto 980
        s = s(6:7)//s(3:5)//s(1:2)
      elseif (mode.eq.2) then
        r8 = fgetlinevalue(s(3:5),1)
        i  = max(1,min(12,(nint(r8))))
        s = s(6:7)//month(i)//s(1:2)
      endif
      newdate = .true.
      return
980   newdate = .false.
      return
      end


C------------------------------------------------------------------------
C Gets the next filename in unit#iun or returns .FALSE. if no more files.

      logical function getfile(filename,iun)

      implicit none
      character*(*) filename
      integer*4 iun
      filename = ' '
      read(iun,'(a)',end=5) filename
      getfile=.true.
      return
5     getfile=.false.
      return
      end

C----------------------------------------------------------------
C Finds first occurence of string S in C starting from "ptr".
C
      integer*4 function ptr_index(c,s,ptr)
      implicit none
      character*(*) c,s
      integer*4 ptr,i,lc
C
      i = index(c(ptr:lc(c)),s)
      if (i.eq.0) then
        ptr_index = 0
      else
        ptr_index = i + ptr - 1
      endif
      return
      end
 
C----------------------------------------------------------------
C Finds first occurence of string S in C from right side.  Like INDEX(),
C  but operates in reverse.
C
      integer*4 function revindex(c,s)
      implicit none
      character*(*) c
      character*(*) s
      integer*4 i,fc,lc,nc
 
      call strpos(s,fc,lc)
      nc=lc-fc
      i=len(c)
      do while(i.gt.nc)
        if (c(i-nc:i).eq.s(fc:lc)) then
          revindex=i-nc
          return
        endif
        i=i-1
      enddo
      revindex=0
      return
      end
 
 
C----------------------------------------------------------------
C Replaces c with a string of characters of ascii value j (blank=32)
C
      subroutine repstr(c,j)
      implicit none
      character*(*) c
      integer*4 j,i,k
      k=len(c)
      do i=1,k
        c(i:i)=char(j)
      enddo
      return
      end

C----------------------------------------------------------------
C Converts all letters in string to upper case.
C
      subroutine upper_case(c)
      implicit none
      character*(*) c
      integer*4 i,j,k
      k=len(c)
      do i=1,k
        j=ichar(c(i:i))
        if ((j.gt.96).and.(j.lt.123)) c(i:i)=char(j-32)
      enddo
      return
      end

C----------------------------------------------------------------
C Converts all letters in string to lower case.
C
      subroutine lower_case(c)
      implicit none
      character*(*) c
      integer*4 i,j,k
      k=len(c)
      do i=1,k
        j=ichar(c(i:i))
        if ((j.gt.64).and.(j.lt.91)) c(i:i)=char(j+32)
      enddo
      return
      end

C----------------------------------------------------------------
C Converts all letters in string to capital.
C
      subroutine allcaps(c)
      implicit none
      character*(*) c
      integer*4 i,j,k
      k=len(c)
      do i=1,k
        j=ichar(c(i:i))
        if ((j.gt.96).and.(j.lt.123)) c(i:i)=char(j-32)
      enddo
      return
      end

C----------------------------------------------------------------
C Returns the position of first and last non-blank character, does not
C  alter string.
      subroutine strpos(c,fc,lc)
      implicit none
      character*(*) c  ! string (input only)
      integer*4 fc,lc  ! position of first and last "normal" characters(output)
      integer*4 i,j,k
      k=len(c)
      i=1              ! find first non-blank character.
      j=ichar(c(i:i))
      do while( (i.lt.k) .and. ((j.lt.33).or.(j.gt.255)) ) ! not "normal" chars
        i=i+1
        j=ichar(c(i:i))
      enddo
      fc=i
      i=k              ! find last non-blank character.
      j=ichar(c(i:i))
      do while( (i.gt.1) .and. ((j.lt.33).or.(j.gt.255)) ) ! not "normal" chars
        i=i-1
        j=ichar(c(i:i))
      enddo
      lc=i
      if (fc.gt.lc) then    ! check for problems
        fc=1
        lc=1
      endif
      return
      end
 
 
C---------------------------------------------------------------------- write80
C writes out a string to unit j such that no trailing blanks will appear.
C  this is necessary in ms fortran which does not allow a<i> in format
C  statements.  writes strings up to 80 characters in length.
C
      subroutine write80(c,j)
      implicit none
      character*(*) c
      integer*4 j,f,l
 
      call strpos(c,f,l)
      if (l.le.1) then
        write(unit=j,fmt='(a1)') c(1:1)
      elseif (l.eq.2) then
        write(unit=j,fmt='(a2)') c(1:l)
      elseif (l.eq.3) then
        write(unit=j,fmt='(a3)') c(1:l)
      elseif (l.eq.4) then
        write(unit=j,fmt='(a4)') c(1:l)
      elseif (l.eq.5) then
        write(unit=j,fmt='(a5)') c(1:l)
      elseif (l.eq.6) then
        write(unit=j,fmt='(a6)') c(1:l)
      elseif (l.eq.7) then
        write(unit=j,fmt='(a7)') c(1:l)
      elseif (l.eq.8) then
        write(unit=j,fmt='(a8)') c(1:l)
      elseif (l.eq.9) then
        write(unit=j,fmt='(a9)') c(1:l)
      elseif (l.eq.10) then
        write(unit=j,fmt='(a10)') c(1:l)
      elseif (l.eq.11) then
        write(unit=j,fmt='(a11)') c(1:l)
      elseif (l.eq.12) then
        write(unit=j,fmt='(a12)') c(1:l)
      elseif (l.eq.13) then
        write(unit=j,fmt='(a13)') c(1:l)
      elseif (l.eq.14) then
        write(unit=j,fmt='(a14)') c(1:l)
      elseif (l.eq.15) then
        write(unit=j,fmt='(a15)') c(1:l)
      elseif (l.eq.16) then
        write(unit=j,fmt='(a16)') c(1:l)
      elseif (l.eq.17) then
        write(unit=j,fmt='(a17)') c(1:l)
      elseif (l.eq.18) then
        write(unit=j,fmt='(a18)') c(1:l)
      elseif (l.eq.19) then
        write(unit=j,fmt='(a19)') c(1:l)
      elseif (l.eq.20) then
        write(unit=j,fmt='(a20)') c(1:l)
      elseif (l.eq.21) then
        write(unit=j,fmt='(a21)') c(1:l)
      elseif (l.eq.22) then
        write(unit=j,fmt='(a22)') c(1:l)
      elseif (l.eq.23) then
        write(unit=j,fmt='(a23)') c(1:l)
      elseif (l.eq.24) then
        write(unit=j,fmt='(a24)') c(1:l)
      elseif (l.eq.25) then
        write(unit=j,fmt='(a25)') c(1:l)
      elseif (l.eq.26) then
        write(unit=j,fmt='(a26)') c(1:l)
      elseif (l.eq.27) then
        write(unit=j,fmt='(a27)') c(1:l)
      elseif (l.eq.28) then
        write(unit=j,fmt='(a28)') c(1:l)
      elseif (l.eq.29) then
        write(unit=j,fmt='(a29)') c(1:l)
      elseif (l.eq.30) then
        write(unit=j,fmt='(a30)') c(1:l)
      elseif (l.eq.31) then
        write(unit=j,fmt='(a31)') c(1:l)
      elseif (l.eq.32) then
        write(unit=j,fmt='(a32)') c(1:l)
      elseif (l.eq.33) then
        write(unit=j,fmt='(a33)') c(1:l)
      elseif (l.eq.34) then
        write(unit=j,fmt='(a34)') c(1:l)
      elseif (l.eq.35) then
        write(unit=j,fmt='(a35)') c(1:l)
      elseif (l.eq.36) then
        write(unit=j,fmt='(a36)') c(1:l)
      elseif (l.eq.37) then
        write(unit=j,fmt='(a37)') c(1:l)
      elseif (l.eq.38) then
        write(unit=j,fmt='(a38)') c(1:l)
      elseif (l.eq.39) then
        write(unit=j,fmt='(a39)') c(1:l)
      elseif (l.eq.40) then
        write(unit=j,fmt='(a40)') c(1:l)
      elseif (l.eq.41) then
        write(unit=j,fmt='(a41)') c(1:l)
      elseif (l.eq.42) then
        write(unit=j,fmt='(a42)') c(1:l)
      elseif (l.eq.43) then
        write(unit=j,fmt='(a43)') c(1:l)
      elseif (l.eq.44) then
        write(unit=j,fmt='(a44)') c(1:l)
      elseif (l.eq.45) then
        write(unit=j,fmt='(a45)') c(1:l)
      elseif (l.eq.46) then
        write(unit=j,fmt='(a46)') c(1:l)
      elseif (l.eq.47) then
        write(unit=j,fmt='(a47)') c(1:l)
      elseif (l.eq.48) then
        write(unit=j,fmt='(a48)') c(1:l)
      elseif (l.eq.49) then
        write(unit=j,fmt='(a49)') c(1:l)
      elseif (l.eq.50) then
        write(unit=j,fmt='(a50)') c(1:l)
      elseif (l.eq.51) then
        write(unit=j,fmt='(a51)') c(1:l)
      elseif (l.eq.52) then
        write(unit=j,fmt='(a52)') c(1:l)
      elseif (l.eq.53) then
        write(unit=j,fmt='(a53)') c(1:l)
      elseif (l.eq.54) then
        write(unit=j,fmt='(a54)') c(1:l)
      elseif (l.eq.55) then
        write(unit=j,fmt='(a55)') c(1:l)
      elseif (l.eq.56) then
        write(unit=j,fmt='(a56)') c(1:l)
      elseif (l.eq.57) then
        write(unit=j,fmt='(a57)') c(1:l)
      elseif (l.eq.58) then
        write(unit=j,fmt='(a58)') c(1:l)
      elseif (l.eq.59) then
        write(unit=j,fmt='(a59)') c(1:l)
      elseif (l.eq.60) then
        write(unit=j,fmt='(a60)') c(1:l)
      elseif (l.eq.61) then
        write(unit=j,fmt='(a61)') c(1:l)
      elseif (l.eq.62) then
        write(unit=j,fmt='(a62)') c(1:l)
      elseif (l.eq.63) then
        write(unit=j,fmt='(a63)') c(1:l)
      elseif (l.eq.64) then
        write(unit=j,fmt='(a64)') c(1:l)
      elseif (l.eq.65) then
        write(unit=j,fmt='(a65)') c(1:l)
      elseif (l.eq.66) then
        write(unit=j,fmt='(a66)') c(1:l)
      elseif (l.eq.67) then
        write(unit=j,fmt='(a67)') c(1:l)
      elseif (l.eq.68) then
        write(unit=j,fmt='(a68)') c(1:l)
      elseif (l.eq.69) then
        write(unit=j,fmt='(a69)') c(1:l)
      elseif (l.eq.70) then
        write(unit=j,fmt='(a70)') c(1:l)
      elseif (l.eq.71) then
        write(unit=j,fmt='(a71)') c(1:l)
      elseif (l.eq.72) then
        write(unit=j,fmt='(a72)') c(1:l)
      elseif (l.eq.73) then
        write(unit=j,fmt='(a73)') c(1:l)
      elseif (l.eq.74) then
        write(unit=j,fmt='(a74)') c(1:l)
      elseif (l.eq.75) then
        write(unit=j,fmt='(a75)') c(1:l)
      elseif (l.eq.76) then
        write(unit=j,fmt='(a76)') c(1:l)
      elseif (l.eq.77) then
        write(unit=j,fmt='(a77)') c(1:l)
      elseif (l.eq.78) then
        write(unit=j,fmt='(a78)') c(1:l)
      elseif (l.eq.79) then
        write(unit=j,fmt='(a79)') c(1:l)
      else
        write(unit=j,fmt='(a80)') c(1:80)
      endif
      return
      end

C----------------------------------------------------------------
C Returns the position of first "normal", non-blank character.
C On a blank string it returns the physical length of the string.
      integer*4 function firstchar(s)
      implicit none
      character*(*) s
      integer*4 i
      i=1
      do while( (i.lt.len(s)) .and. (ichar(s(i:i)).lt.33) )
        i=i+1
      enddo
      firstchar = i
      return
      end

C----------------------------------------------------------------
C SAME AS firstchar.
C Returns the position of first "normal", non-blank character.
C On a blank string it returns the physical length of the string.
      integer*4 function fc(s)
      implicit none
      character*(*) s
      integer*4 i
      i=1
      do while( (i.lt.len(s)) .and. (ichar(s(i:i)).lt.33) )
        i=i+1
      enddo
      fc = i
      return
      end

C----------------------------------------------------------------
C Returns the position of last "normal", non-blank character.
C On a blank string it returns 0.
      integer*4 function lastchar(s)
      implicit none
      character*(*) s
      integer*4 i
      i=len(s)
      do while( (i.ge.1) .and. (ichar(s(i:i)).lt.33) )
        i=i-1
      enddo
      lastchar = i
      return
      end

C----------------------------------------------------------------
C Calls lastchar().
      integer*4 function lc(s)
      implicit none
      character*(*) s
      integer*4 lastchar
      lc = lastchar(s)
      return
      end

C--------------------------------------------------------------------------- RF5
C Formats a real*4 number in a readable way, filling 5 spaces.

      character*5 function rf5(r)
      implicit none
      real*4 r
      character c*5

      if (r.eq.0.) then
        write(c,'(f5.2)',err=9) r
      else if (r.gt.0.) then   ! positive number
        if (r.gt.99999.) then
         c='*****'
        elseif (r.gt.999.) then
          write(c,'(i5)',err=9) nint(r)
        elseif (r.gt.99.) then
          write(c,'(f5.1)',err=9) r
        elseif (r.gt.9.) then
          write(c,'(f5.2)',err=9) r
        else
          write(c,'(f5.3)',err=9) r
        endif
      else                     ! negative number
        if (r.lt.-9999.) then
          c='*****'
        elseif (r.lt.-99.) then
          write(c,'(i5)',err=9) nint(r)
        elseif (r.lt.-9.) then
          write(c,'(f5.1)',err=9) r
        else
          write(c,'(f5.2)',err=9) r
        endif
      endif
      rf5 = c
      return
9     rf5 = '*****'
      return
      end


C--------------------------------------------------------------------------- RF7
C Formats a real*4 number in a readable way, filling 7 spaces.

      character*7 function rf7(r)
      implicit none
      real*4 r
      character c*7

      if (r.eq.0.) then
        write(c,'(f7.3)',err=9) r
      else if (r.gt.0.) then   ! positive number
        if (r.gt.999900) then
          write(c,'(1pe7.1)',err=9) r
        elseif (r.gt.99999.) then
          write(c,'(f7.0)',err=9) r
        elseif (r.gt.9999.) then
          write(c,'(f7.1)',err=9) r
        elseif (r.gt.999.) then
          write(c,'(f7.2)',err=9) r
        elseif (r.gt.9.) then
          write(c,'(f7.3)',err=9) r
        elseif (r.gt.0.0001) then
          write(c,'(f7.5)',err=9) r
        else
          write(c,'(1pe7.1)',err=9) r
        endif
      else                     ! negative number
        if (r.lt.-99990) then
          write(c,'(1pe7.0)',err=9) r
        elseif (r.lt.-999.) then
          write(c,'(f7.0)',err=9) r
        elseif (r.lt.-9.) then
          write(c,'(f7.2)',err=9) r
        elseif (r.lt.-0.001) then
          write(c,'(f7.4)',err=9) r
        else
          write(c,'(1pe7.0)',err=9) r
        endif
      endif
      rf7 = c
      return
9     rf7 = '*******'
      print *,'rf7: error'
      return
      end


C--------------------------------------------------------------------------- RF9
C Formats a real*4 number in a readable way, filling 9 spaces.
      character*9 function rf9(r)
      implicit none
      real*4 r
      character c*9
      if (r.eq.0.) then
        write(c,'(f9.5)',err=9) r
      else if (r.gt.0.) then   ! positive number
        if (r.gt.999900) then
          write(c,'(1pe9.3)',err=9) r
        elseif (r.gt.999.) then
          write(c,'(f9.2)',err=9) r
        elseif (r.gt.9.) then
          write(c,'(f9.5)',err=9) r
        elseif (r.gt.0.00001) then
          write(c,'(f9.7)',err=9) r
        else
          write(c,'(1pe9.3)',err=9) r
        endif
      else                     ! negative number
        if (r.lt.-999900) then
          write(c,'(1pe9.2)',err=9) r
        elseif (r.lt.-999.) then
          write(c,'(f9.1)',err=9) r
        elseif (r.lt.-9.) then
          write(c,'(f9.4)',err=9) r
        elseif (r.lt.-0.0001) then
          write(c,'(f9.6)',err=9) r
        else
          write(c,'(1pe9.2)',err=9) r
        endif
      endif
      rf9 = c
      return
9     rf9 = '*********'
      print *,'rf9: error'
      return
      end


C-------------------------------------------------------------------------- RF12
C Formats a real*4 number in a readable way, filling 12 spaces.
      character*12 function rf12(r)
      implicit none
      real*4 r
      character c*12
      if (r.eq.0.) then
        write(c,'(f12.5)',err=9) r
      else if (r.gt.0.) then   ! positive number
        if (r.gt.999900) then
          write(c,'(1pe12.6)',err=9) r
        elseif (r.gt.999.) then
          write(c,'(f12.5)',err=9) r
        elseif (r.gt.9.) then
          write(c,'(f12.8)',err=9) r
        elseif (r.gt.0.00001) then
          write(c,'(f12.10)',err=9) r
        else
          write(c,'(1pe12.6)',err=9) r
        endif
      else                     ! negative number
        if (r.lt.-999900) then
          write(c,'(1pe12.5)',err=9) r
        elseif (r.lt.-999.) then
          write(c,'(f12.4)',err=9) r
        elseif (r.lt.-9.) then
          write(c,'(f12.7)',err=9) r
        elseif (r.lt.-0.0001) then
          write(c,'(f12.9)',err=9) r
        else
          write(c,'(1pe12.5)',err=9) r
        endif
      endif
      rf12 = c
      return
9     rf12 = '************'
      print *,'rf12: error'
      return
      end


C------------------------------------------------------------------------- dRF5
C Formats a real*8 number in a readable way, filling 5 spaces.

      character*5 function drf5(r)
      implicit none
      real*8 r
      character c*5

      if (r.eq.0.) then
        write(c,'(f5.2)',err=9) r
      else if (r.gt.0.) then   ! positive number
        if (r.gt.99999.) then
         c='*****'
        elseif (r.gt.999.) then
          write(c,'(i5)',err=9) nint(r)
        elseif (r.gt.99.) then
          write(c,'(f5.1)',err=9) r
        elseif (r.gt.9.) then
          write(c,'(f5.2)',err=9) r
        else
          write(c,'(f5.3)',err=9) r
        endif
      else                     ! negative number
        if (r.lt.-9999.) then
          c='*****'
        elseif (r.lt.-99.) then
          write(c,'(i5)',err=9) nint(r)
        elseif (r.lt.-9.) then
          write(c,'(f5.1)',err=9) r
        else
          write(c,'(f5.2)',err=9) r
        endif
      endif
      drf5 = c
      return
9     drf5 = '*****'
      return
      end


C------------------------------------------------------------------------- dRF7
C Formats a real*8 number in a readable way, filling 7 spaces.

      character*7 function drf7(r)
      implicit none
      real*8 r
      character c*7

      if (r.eq.0.) then
        write(c,'(f7.3)',err=9) r
      else if (r.gt.0.) then   ! positive number
        if (r.gt.999900) then
          write(c,'(1pe7.1)',err=9) r
        elseif (r.gt.99999.) then
          write(c,'(f7.0)',err=9) r
        elseif (r.gt.9999.) then
          write(c,'(f7.1)',err=9) r
        elseif (r.gt.999.) then
          write(c,'(f7.2)',err=9) r
        elseif (r.gt.9.) then
          write(c,'(f7.3)',err=9) r
        elseif (r.gt.0.0001) then
          write(c,'(f7.5)',err=9) r
        else
          write(c,'(1pe7.1)',err=9) r
        endif
      else                     ! negative number
        if (r.lt.-99990) then
          write(c,'(1pe7.0)',err=9) r
        elseif (r.lt.-999.) then
          write(c,'(f7.0)',err=9) r
        elseif (r.lt.-9.) then
          write(c,'(f7.2)',err=9) r
        elseif (r.lt.-0.001) then
          write(c,'(f7.4)',err=9) r
        else
          write(c,'(1pe7.0)',err=9) r
        endif
      endif
      drf7 = c
      return
9     drf7 = '*******'
      print *,'drf7: error'
      return
      end


C------------------------------------------------------------------------ dRF9
C Formats a real*8 number in a readable way, filling 9 spaces.
      character*9 function drf9(r)
      implicit none
      real*8 r
      character c*9
      if (r.eq.0.) then
        write(c,'(f9.5)',err=9) r
      else if (r.gt.0.) then   ! positive number
        if (r.gt.999900) then
          write(c,'(1pe9.3)',err=9) r
        elseif (r.gt.999.) then
          write(c,'(f9.2)',err=9) r
        elseif (r.gt.9.) then
          write(c,'(f9.5)',err=9) r
        elseif (r.gt.0.00001) then
          write(c,'(f9.7)',err=9) r
        else
          write(c,'(1pe9.3)',err=9) r
        endif
      else                     ! negative number
        if (r.lt.-999900) then
          write(c,'(1pe9.2)',err=9) r
        elseif (r.lt.-999.) then
          write(c,'(f9.1)',err=9) r
        elseif (r.lt.-9.) then
          write(c,'(f9.4)',err=9) r
        elseif (r.lt.-0.0001) then
          write(c,'(f9.6)',err=9) r
        else
          write(c,'(1pe9.2)',err=9) r
        endif
      endif
      drf9 = c
      return
9     drf9 = '*********'
      print *,'drf9: error'
      return
      end


C----------------------------------------------------------------------- dRF12
C Formats a real*8 number in a readable way, filling 12 spaces.
      character*12 function drf12(r)
      implicit none
      real*8 r
      character c*12
      if (r.eq.0.) then
        write(c,'(f12.5)',err=9) r
      else if (r.gt.0.) then   ! positive number
        if (r.gt.999900) then
          write(c,'(1pe12.6)',err=9) r
        elseif (r.gt.999.) then
          write(c,'(f12.5)',err=9) r
        elseif (r.gt.9.) then
          write(c,'(f12.8)',err=9) r
        elseif (r.gt.0.00001) then
          write(c,'(f12.10)',err=9) r
        else
          write(c,'(1pe12.6)',err=9) r
        endif
      else                     ! negative number
        if (r.lt.-999900) then
          write(c,'(1pe12.5)',err=9) r
        elseif (r.lt.-999.) then
          write(c,'(f12.4)',err=9) r
        elseif (r.lt.-9.) then
          write(c,'(f12.7)',err=9) r
        elseif (r.lt.-0.0001) then
          write(c,'(f12.9)',err=9) r
        else
          write(c,'(1pe12.5)',err=9) r
        endif
      endif
      drf12 = c
      return
9     drf12 = '************'
      print *,'drf12: error'
      return
      end


C----------------------------------------------------------------------
C Sort an ASCII file 
C
      subroutine filesort_ascii(arg,narg,ok)
C
      implicit none
      integer*4 narg
      character*(*) arg(narg)
C
      integer*4 maxarr,n,i,numin,a1,a2,b1,b2,c1,c2,d1,d2,e1,e2,m1,m2
      integer*4 iun,oun,m,k,lastchar,nwrd,maxll
      parameter(maxarr=92000,maxll=501)
      character*501 arr(maxarr), dum, firstline
      character*3 c3
      character*40 s,wrd(9)
      logical inout,ascend_a,ascend_b,ascend_c,ascend_d,ascend_e,testad
      logical newdate,findarg,fl,ok

      real*8 fgetlinevalue
       
C Syntax.
      if (narg.lt.1) then
        print *,'Syntax:  fsort  -c#:# [-c#:#] [-c#:#] [-c#:#] [-c#:#]'
        print *,'                [-m#:#] [-fl] [infile [outfile]]'
        print *,' "c" must be either "a"(ascending) or "d"(descending)'
        print *,' This routine will sort up to five sublevels.'
        print *,'  -c#:#  = sort first by columns # to #.'
        print *,'  -c#:#  = then sort by columns # to #. ...'
        print *,'  -fl    = treat first line as special, no sorting.'
        print *,
     .  '  -m#:#  = change columns # to # from a char. date (05SEP92)'
        print *,'           to a numeric date (9200905) before sorting.'
        print *,'  Example:  fsort -a1:10 -d22:30 input.list > out.dat'
        print *,'  '
        print *,'Maximum number of lines:',maxarr
        print *,'  '
        ok=.false.
        return
      endif

C Initial.
      ok=.true.
      a1=0
      a2=0
      b1=0
      b2=0
      c1=0
      c2=0
      d1=0
      d2=0
      e1=0
      e2=0
      m1=0
      m2=0
      if (findarg(arg,narg,'-fl',':',wrd,nwrd)) then
        fl=.true.
      else
        fl=.false.
      endif
      if (findarg(arg,narg,'-m',':',wrd,nwrd)) then
        if (nwrd.ne.2) goto 910
        m1 = fgetlinevalue(wrd(1),1)
        m2 = fgetlinevalue(wrd(2),1)
        if (m2-m1.ne.6) goto 910
      endif
      numin = 1
      s=arg(1) 
      if (s(1:1).eq.'-') then
        if (.not.testad(s(2:2),ascend_a)) goto 910
        i=index(s,':') 
        if (i.lt.4) goto 910
        a1 = fgetlinevalue( s, 1 )
        a2 = fgetlinevalue( s, 2 )
        if ((a1.lt.1).or.(a1.gt.a2).or.(a2.gt.maxll)) goto 910
        numin = numin + 1
        s=arg(2)
        if (s(1:1).eq.'-') then
          if (.not.testad(s(2:2),ascend_b)) goto 910
          i=index(s,':') 
          if (i.lt.4) goto 910
          b1 = fgetlinevalue(s,1)
          b2 = fgetlinevalue(s,2)
          if ((b1.lt.1).or.(b1.gt.b2).or.(b2.gt.maxll)) goto 910
          numin = numin + 1
          s=arg(3)
          if (s(1:1).eq.'-') then
            if (.not.testad(s(2:2),ascend_c)) goto 910
            i=index(s,':') 
            if (i.lt.4) goto 910
            c1 = fgetlinevalue(s,1)
            c2 = fgetlinevalue(s,2)
            if ((c1.lt.1).or.(c1.gt.c2).or.(c2.gt.maxll)) goto 910
            numin = numin + 1
            s=arg(4)
            if (s(1:1).eq.'-') then
              if (.not.testad(s(2:2),ascend_d)) goto 910
              i=index(s,':') 
              if (i.lt.4) goto 910
              d1 = fgetlinevalue(s,1)
              d2 = fgetlinevalue(s,2)
              if ((d1.lt.1).or.(d1.gt.d2).or.(d2.gt.maxll)) goto 910
              numin = numin + 1
              s=arg(5)
              if (s(1:1).eq.'-') then
                if (.not.testad(s(2:2),ascend_e)) goto 910
                i=index(s,':') 
                if (i.lt.4) goto 910
                e1 = fgetlinevalue(s,1)
                e2 = fgetlinevalue(s,2)
                if ((e1.lt.1).or.(e1.gt.e2).or.(e2.gt.maxll)) goto 910
                numin = numin + 1
              endif
            endif
          endif
        endif
      endif

      if (numin.lt.2) goto 910

      if (.not.inout(arg,narg,numin,iun,oun)) goto 910
  
      if (fl) read(iun,'(a)') firstline
      
      n=0
      if (m1.gt.0) then       ! change date char-to-num
        do while(n.lt.maxarr)
          n=n+1
          arr(n) = ' '
          read(iun,'(a)',end=5) arr(n)
          c3 = arr(n)(m1+2:m1+4)
          arr(n)(maxll-2:maxll) = c3
          call upper_case(c3)
          arr(n)(m1+2:m1+4) = c3
          if (.not.newdate(arr(n)(m1:m2),1)) then
            dum=arr(n)
            goto 930
          endif
        enddo
      else
        do while(n.lt.maxarr)
          n=n+1
          arr(n) = ' '
          read(iun,'(a)',end=5) arr(n)
        enddo
      endif

5     if (n.ge.maxarr) goto 920
      n=n-1

C First sort by "a" columns
      if (ascend_a) then
        call qcksorta(arr,n,a1,a2)
      else
        call qcksortd(arr,n,a1,a2)
      endif

C Now sort by "b" columns within each group of
C   identical "a" columns
      if (b1.gt.0) then
        i=1
        do while(i.lt.n) 
          m=0
          dum = arr(i)
          do while((i.le.n).and.(arr(i)(a1:a2).eq.dum(a1:a2)))
            m=m+1
            i=i+1
          enddo
          if (m.gt.1) then
            k=i-m
            if (ascend_b) then
              call qcksorta(arr(k),m,b1,b2)
            else
              call qcksortd(arr(k),m,b1,b2)
            endif
          endif
        enddo
      endif

C Now sort by "c" columns within each group of
C   identical "a" and "b" columns
      if (c1.gt.0) then
        i=1
        do while(i.lt.n) 
          m=0
          dum = arr(i)
          do while((i.le.n).and.(arr(i)(a1:a2).eq.dum(a1:a2)).and.
     .                          (arr(i)(b1:b2).eq.dum(b1:b2)))
            m=m+1
            i=i+1
          enddo
          if (m.gt.1) then
            k=i-m
            if (ascend_c) then
              call qcksorta(arr(k),m,c1,c2)
            else
              call qcksortd(arr(k),m,c1,c2)
            endif
          endif
        enddo
      endif

C Now sort by "d" columns within each group of
C   identical "a" and "b" and "c" columns
      if (d1.gt.0) then
        i=1
        do while(i.lt.n) 
          m=0
          dum = arr(i)
          do while((i.le.n).and.(arr(i)(a1:a2).eq.dum(a1:a2)).and.
     .                          (arr(i)(b1:b2).eq.dum(b1:b2)).and.
     .                          (arr(i)(c1:c2).eq.dum(c1:c2)))
            m=m+1
            i=i+1
          enddo
          if (m.gt.1) then
            k=i-m
            if (ascend_d) then
              call qcksorta(arr(k),m,d1,d2)
            else
              call qcksortd(arr(k),m,d1,d2)
            endif
          endif
        enddo
      endif

C Now sort by "e" columns within each group of
C   identical "a" and "b" and "c" and "d" columns
      if (e1.gt.0) then
        i=1
        do while(i.lt.n) 
          m=0
          dum = arr(i)
          do while((i.le.n).and.(arr(i)(a1:a2).eq.dum(a1:a2)).and.
     .                          (arr(i)(b1:b2).eq.dum(b1:b2)).and.
     .                          (arr(i)(c1:c2).eq.dum(c1:c2)).and.
     .                          (arr(i)(d1:d2).eq.dum(d1:d2)))
            m=m+1
            i=i+1
          enddo
          if (m.gt.1) then
            k=i-m
            if (ascend_e) then
              call qcksorta(arr(k),m,e1,e2)
            else
              call qcksortd(arr(k),m,e1,e2)
            endif
          endif
        enddo
      endif

C Write out result.
      if (fl) write(oun,'(a)') firstline(1:lastchar(firstline))

      if (m1.gt.0) then      ! change date num-to-char
        do i=1,n
          if (.not.newdate(arr(i)(m1:m2),2)) then
            dum=arr(i)
            goto 930
          endif
          arr(i)(m1+2:m1+4) = arr(i)(maxll-2:maxll)
          arr(i)(maxll-2:maxll) = ' '
          write(oun,'(a)') arr(i)(1:lastchar(arr(i)))
        enddo
      else
        do i=1,n
          write(oun,'(a)') arr(i)(1:lastchar(arr(i)))
        enddo
      endif
   
      if (iun.ne.5) close(iun)
      if (oun.ne.6) close(oun)
      ok=.true.
      return

910   call printerr('Error with command line parameters.')
      ok=.false.
      return

920   continue
      write(dum,'(i7)') maxarr
      call printerr(
     . 'Too many lines in input file. Max: '//dum(1:7)//'.')
      if (iun.ne.5) close(iun)
      if (oun.ne.6) close(oun)
      ok=.false.
      return

930   call printerr('Error converting date "'//dum(m1:m2)//'" in line:')
      call printerr(dum)
      if (iun.ne.5) close(iun)
      if (oun.ne.6) close(oun)
      ok=.false.
      return

      end



C--------------------------------------------------------------
      logical function testad(c,ascend)
      character*1 c
      logical ascend
      testad = .true.
      if (c.eq.'a') then
        ascend = .true.
      elseif (c.eq.'d') then
        ascend = .false.
      else
        testad = .false.
      endif
      return
      end


C------------------------------------------------------------------------
C originated from NUMERICAL RECIPES...        TAB  April 1992
C
C This version sorts 501 character strings based on the characters in
C columns SC to EC, inclusive.  Ascending order.
C 
      SUBROUTINE QCKSORTA(ARR,N,SC,EC)
      IMPLICIT NONE
      INTEGER*4 N,M,NSTACK,SC,EC,I,J,JSTACK,L,IR,IQ
      REAL*4 FM,FA,FC,FMI,FX
      PARAMETER(M=7,NSTACK=50,FM=7875.,FA=211.,FC=1663.)
      PARAMETER(FMI=1.2698413E-4)
      INTEGER*4 ISTACK(NSTACK)
      character*501 arr(*),a

      JSTACK=0
      L=1
      IR=N
      FX=0.
10    IF(IR-L.LT.M)THEN
        DO 13 J=L+1,IR
          A=ARR(J)
          DO 11 I=J-1,1,-1
            IF(ARR(I)(SC:EC).LE.A(SC:EC))GO TO 12
            ARR(I+1)=ARR(I)
11        CONTINUE
          I=0
12        ARR(I+1)=A
13      CONTINUE
        IF(JSTACK.EQ.0)RETURN
        IR=ISTACK(JSTACK)
        L=ISTACK(JSTACK-1)
        JSTACK=JSTACK-2
      ELSE
        I=L
        J=IR
        FX=MOD(FX*FA+FC,FM)
        IQ=L+(IR-L+1)*(FX*FMI)
        A=ARR(IQ)
        ARR(IQ)=ARR(L)
20      CONTINUE
21        IF(J.GT.0)THEN
            IF(A(SC:EC).LT.ARR(J)(SC:EC))THEN
              J=J-1
              GO TO 21
            ENDIF
          ENDIF
          IF(J.LE.I)THEN
            ARR(I)=A
            GO TO 30
          ENDIF
          ARR(I)=ARR(J)
          I=I+1
22        IF(I.LE.N)THEN
            IF(A(SC:EC).GT.ARR(I)(SC:EC))THEN
              I=I+1
              GO TO 22
            ENDIF
          ENDIF
          IF(J.LE.I)THEN
            ARR(J)=A
            I=J
            GO TO 30
          ENDIF
          ARR(J)=ARR(I)
          J=J-1
        GO TO 20
30      JSTACK=JSTACK+2
        IF(JSTACK.GT.NSTACK)then
          PRINT *, 'NSTACK must be made larger.'
          READ(*,*)
        ENDIF
        IF(IR-I.GE.I-L)THEN
          ISTACK(JSTACK)=IR
          ISTACK(JSTACK-1)=I+1
          IR=I-1
        ELSE
          ISTACK(JSTACK)=I-1
          ISTACK(JSTACK-1)=L
          L=I+1
        ENDIF
      ENDIF
      GO TO 10
      END


C------------------------------------------------------------------------
C originated from NUMERICAL RECIPES...        TAB  April 1992
C
C This version sorts 501 character strings based on the characters in
C columns SC to EC, inclusive.  Descending order.
C 
      SUBROUTINE QCKSORTD(ARR,N,SC,EC)
      IMPLICIT NONE
      INTEGER*4 N,M,NSTACK,SC,EC,I,J,JSTACK,L,IR,IQ
      REAL*4 FM,FA,FC,FMI,FX
      PARAMETER(M=7,NSTACK=50,FM=7875.,FA=211.,FC=1663.)
      PARAMETER(FMI=1.2698413E-4)
      INTEGER*4 ISTACK(NSTACK)
      CHARACTER*501 A,ARR(*)

      JSTACK=0
      L=1
      IR=N
      FX=0.
10    IF(IR-L.LT.M)THEN
        DO 13 J=L+1,IR
          A=ARR(J)
          DO 11 I=J-1,1,-1
            IF(ARR(I)(SC:EC).GT.A(SC:EC))GO TO 12
            ARR(I+1)=ARR(I)
11        CONTINUE
          I=0
12        ARR(I+1)=A
13      CONTINUE
        IF(JSTACK.EQ.0)RETURN
        IR=ISTACK(JSTACK)
        L=ISTACK(JSTACK-1)
        JSTACK=JSTACK-2
      ELSE
        I=L
        J=IR
        FX=MOD(FX*FA+FC,FM)
        IQ=L+(IR-L+1)*(FX*FMI)
        A=ARR(IQ)
        ARR(IQ)=ARR(L)
20      CONTINUE
21        IF(J.GT.0)THEN
            IF(A(SC:EC).GE.ARR(J)(SC:EC))THEN
              J=J-1
              GO TO 21
            ENDIF
          ENDIF
          IF(J.LE.I)THEN
            ARR(I)=A
            GO TO 30
          ENDIF
          ARR(I)=ARR(J)
          I=I+1
22        IF(I.LE.N)THEN
            IF(A(SC:EC).LE.ARR(I)(SC:EC))THEN
              I=I+1
              GO TO 22
            ENDIF
          ENDIF
          IF(J.LE.I)THEN
            ARR(J)=A
            I=J
            GO TO 30
          ENDIF
          ARR(J)=ARR(I)
          J=J-1
        GO TO 20
30      JSTACK=JSTACK+2
        IF(JSTACK.GT.NSTACK)THEN
          PRINT *, 'NSTACK must be made larger.'
          READ(*,*)
        ENDIF
        IF(IR-I.GE.I-L)THEN
          ISTACK(JSTACK)=IR
          ISTACK(JSTACK-1)=I+1
          IR=I-1
        ELSE
          ISTACK(JSTACK)=I-1
          ISTACK(JSTACK-1)=L
          L=I+1
        ENDIF
      ENDIF
      GO TO 10
      END

C----------------------------------------------------------------------
C Change all the tildas to blanks in the string "s".
C
      subroutine tilda_to_blank(s)
C
      implicit none
      character*(*) s
      integer*4 i,lc
      do i=1,lc(s)
        if (s(i:i).eq.'~') s(i:i)=' '
      enddo
      return
      end

C----------------------------------------------------------------------
C Is string "a" greater alphabetically than string "b".
C Convert to lower case first.
C
      logical function greater_string(a,b)
C
      implicit none
      character*(*) a,b
      character*1000 aa,bb
      integer*4 i,lc
C
      greater_string = .false.
      aa = a
      bb = b
      call lower_case(aa)
      call lower_case(bb)
      i=1
      do while( (i.gt.0).and.(i.le.min(lc(aa),lc(bb))) )
        if (ichar(aa(i:i)).gt.ichar(bb(i:i))) then
          greater_string=.true.
          i = -1
        elseif (ichar(aa(i:i)).lt.ichar(bb(i:i))) then
          greater_string=.false.
          i = -1
        endif
        i=i+1
      enddo
C Perfect match?
      if (i.gt.0) greater_string= (lc(aa).gt.lc(bb))
      return
      end
      
C----------------------------------------------------------------------
C Does string "n1" match "n2"?
C
      logical function stringmatch(n1,n2)
C
      implicit none
      character*(*) n1,n2
      integer*4 lc
      stringmatch =(n1(1:lc(n1)).eq.n2(1:lc(n2)))
      return
      end




