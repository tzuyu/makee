/* cstuff.c             LINUX VERSION-- tab March 2000                  */  


#include "ctab.h"
#include "cphomax.h"
#include "cmisc.h"
#include "cufio.h"
#include "cmath-util.h"


/* FORTRAN callable C input-output-miscellaneous routines               */

/* ------------------------------------------------------------------- */
/* 
   Append a null at the end of the string at location given by "ii".
   Note that "ii" would be the last character in a FORTRAN routine.
   since c strings start at 0.
*/
void cappendnull_ ( char *wrd, int *ii )
{
wrd[*ii]='\0';
return;
}


/* ------------------------------------------------------------------- */
/* Start Text HTML output. */
void starthtml_ ()
{
printf("Content-type: text/html\n\n");
printf("   ");
return;
}

/* ------------------------------------------------------------------- */
/* Start Plain Text HTML output. */
void startplainhtml_ ()
{
printf("Content-type: text/plain\n\n");
printf("   ");
return;
}


/* ------------------------------------------------------------------- */
/* This converts an integer into a hexadecimal string. */
void int2hex_ (int *i, char *s)
{ 
sprintf(s,"%x",*i);
return;
}

/* ------------------------------------------------------------------- */
/* This converts a hexadecimal string into an integer. */
void hex2int_ (char *s, int *i)
{ 
sscanf(s,"%x",i);
return;
}


/* ------------------------------------------------------------------- */
/* This c routine returns the address of a real variable or real array */
void getrealaddress_ (float *a, int *loi)
{
*loi = (int)a;
return;
}

/* ------------------------------------------------------------------- */
void csys_ (char *string)
{
system(string);
return;
}

/* ------------------------------------------------------------------- */
void ftostderr_ (char *string)
{
fprintf(stderr,string);
return;
}

/* ------------------------------------------------------------------- */
void ftostdout_ (char *string)
{
fprintf(stdout,string);
return;
}

/* ---------------------------------------------------------------------
  FORTRAN's front end to timeget()...
*/
int fftimeget_ ()
{
int ii;
ii = timeget();
return(ii);
}

/* ---------------------------------------------------------------------
  FORTRAN's front end to GetLineValue()...
*/
double ffgetlinevalue_ (char *line, int *nn)
{
double rr;
rr = GetLineValue( line, *nn );
return(rr);
}

/* ---------------------------------------------------------------------
  FORTRAN's front end to GetLineValueQuiet()...
*/
double ffgetlinevaluequiet_ (char *line, int *nn)
{
double rr;
rr = GetLineValueQuiet( line, *nn );
return(rr);
}


/* ---------------------------------------------------------------------
  FORTRAN's front end to creadfitsheader()...
*/
int ffcreadfitsheader_ (char *fitsfile, char *header, int *headersize)
{
int ii;
ii = creadfitsheader( fitsfile, header, *headersize );
return(ii);
}


/* ---------------------------------------------------------------------
  FORTRAN's front end to creadfits()...
*/
int ffcreadfits_ (char *fitsfile, float *fd, int *maxpix, 
                                   char *header, int *headersize)
{
int ii;
ii = creadfits( fitsfile, fd, *maxpix, header, *headersize );
return(ii);
}


/* ---------------------------------------------------------------------
  FORTRAN's front end to cwritefits()...
*/
int ffcwritefits_ (char *fitsfile, float *fd, char *header, int *headersize)
{
int ii;
ii = cwritefits( fitsfile, fd, header, *headersize );
return(ii);
}


/* ---------------------------------------------------------------------
  FORTRAN's front end to camoeba_gate()...
*/
void ffamoebagate_ ( double (*funk)(double []),
                     int *npar, double par[], double parsl[],
                     double *ftol, double *resid, int *niter )
{
camoeba_gate( funk, *npar, par, parsl, *ftol, resid, niter );
return;
}


/* ------------------------------------------------------------------- */
/*
Input:   ibuf       : Output character buffer
*/
void stdinreadbyte_ (char *ibuf)
{
  *ibuf = getchar();
  return;
}



/* ------------------------------------------------------------------
   Calculate determinant of matrix via recursion.
*/
double determ( double a[20][20], int n )
{
/**/
int j;
double sum;
double cofac();
/**/
sum = 0.;
if (n<3) {
  sum = ( a[0][0] * a[1][1] ) - ( a[0][1] * a[1][0] );
} else {
  for (j=0; j<n; j=j+1) { sum = sum + ( a[0][j] * cofac(a,n,0,j) ); }
}
return(sum);
}


/* ------------------------------------------------------------------
   Calculate cofactor of the is,js element of the matrix a(n,n).
*/
double cofac( double a[20][20], int n, int is, int js )
{
/**/
int i,j;
double b[20][20],konst;
double determ();
/**/
/* First copy matrix a to matrix b so I can squeeze matrix b.        */
for (i=0; i<n; i=i+1) {
  for (j=0; j<n; j=j+1) {
     b[i][j] = a[i][j];
  }
}
/* Create constant.  */
if ((is+js) % 2 == 0) { konst = 1.; } else { konst = -1.; }
/* Now squeeze out row is and column js. */
if (is < n-1) {
  for (i=is+1; i<n; i=i+1) {
    for (j=0; j<n; j=j+1) {
      b[i-1][j] = b[i][j];
    }
  }
}
if (js < n-1) {
  for (j=js+1; j<n; j=j+1) {
    for (i=0; i<n; i=i+1) {
      b[i][j-1] = b[i][j];
    }
  }
}
return( konst * determ(b,n-1) );
}


/* --------------------------------------------------------------------- */
/* Front end for passing a 20 x 20 matrix and calling determ function.   */
void determgate_ (double aa[], int *nn, double *result)
{
/**/
int   i,j,k,nx,ny,num,n;
double a[20][20];
/**/
if (*nn <= 1) {
  *result = aa[0];
}
else {
  n = *nn;
  nx=20; ny=20; num=nx*ny;
  for (k=0; k<num; k=k+1) {
    i = k % nx;
    j = k / nx; 
    a[i][j] = aa[k];
  }
  *result = determ(a,n); 
}
return;
}



/* ------------------------------------------------------------------
  Converts the seconds since Jan. 1, 1970 into a UTC date string.
*/

void computeutcansitime_ (double *float_time, char *ansi_time)  
{
/**/
  time_t     int_time ;
  struct tm  utc ;
  int        i ; 
  char       str [30] ;
  char       tmp [10] ;
/**/

  int_time = (time_t) *float_time ;
  utc = *gmtime (&int_time) ;

  i = utc.tm_year + 1900 ;  

  sprintf (tmp, "%04d-", i) ;    
  strcpy (str, tmp) ;

  i = utc.tm_mon + 1 ;
  sprintf (tmp, "%02d-", i) ;  
  strcat (str, tmp) ;
  
  i = utc.tm_mday ; 
  sprintf (tmp, "%02d ", i) ;
  strcat (str, tmp) ;

  i = utc.tm_hour ;
  sprintf (tmp, "%02d:", i) ;
  strcat (str, tmp) ;

  i = utc.tm_min ;
  sprintf (tmp, "%02d:", i) ;
  strcat (str, tmp) ;

  i = utc.tm_sec ;
  sprintf (tmp, "%02d", i) ;
  strcat (str, tmp) ;
    
  strcpy (ansi_time, str) ;

return ; 
}

/* ------------------------------------------------------------------
   Converts the seconds since Jan. 1, 1970 into a local date string.
*/
void computelocalansitime_ (double *float_time, char *ansi_time)  
{
/**/
  time_t     int_time ;
  struct tm  utc ;
  int        i ; 
  char       str [30] ;
  char       tmp [10] ;
/**/

  int_time = (time_t) *float_time ;
  utc = *localtime (&int_time) ;

  i = utc.tm_year + 1900 ;  

  sprintf (tmp, "%04d-", i) ;    
  strcpy (str, tmp) ;

  i = utc.tm_mon + 1 ;
  sprintf (tmp, "%02d-", i) ;  
  strcat (str, tmp) ;
  
  i = utc.tm_mday ; 
  sprintf (tmp, "%02d ", i) ;
  strcat (str, tmp) ;

  i = utc.tm_hour ;
  sprintf (tmp, "%02d:", i) ;
  strcat (str, tmp) ;

  i = utc.tm_min ;
  sprintf (tmp, "%02d:", i) ;
  strcat (str, tmp) ;

  i = utc.tm_sec ;
  sprintf (tmp, "%02d", i) ;
  strcat (str, tmp) ;
    
  strcpy (ansi_time, str) ;

return ; 
}






/* --------------------------------------------------------------------- */
/* Galactic to Equatorial  ( FORTRAN front end for galeqd. )             */
/*                                                                       */
/* lng = longitude in degrees  (input)  */
/* lat = latitude in degrees   (input)  */
/* ra  = RA in degrees         (output) */
/* dec = Dec in degrees        (output) */
/*                                      */
void galtoequ_ (double *lng, double *lat, double *ra, double *dec)
/*                                      */
{
int pop_galeqd();
pop_galeqd(*lng,*lat,ra,dec);
return;
}


/* --------------------------------------------------------------------- */
/* Equatorial to Galactic ( FORTRAN front end for equgad. )              */
/*                                                                       */
/* ra  = RA in degrees         (input)   */
/* dec = Dec in degrees        (input)   */
/* lng = longitude in degrees  (output)  */
/* lat = latitude in degrees   (output)  */
/*                                       */
void equtogal_ (double *ra, double *dec, double *lng, double *lat)
/*                                       */
{
int pop_equgad();
pop_equgad(*ra,*dec,lng,lat);
return;
}

/* --------------------------------------------------------------------- */
/* Ecliptic to Equatorial ( FORTRAN front end for ecleqx. )              */
/*                                                                       */
/* lambda= ecliptic longitude in degrees (0 to 360) (input)    */
/* beta  = ecliptic latitude in degrees  (-90 to +90) (input)  */
/* ra    = RA in degrees         (output) */
/* dec   = Dec in degrees        (output) */
/*                                        */
void ecltoequ_ (double *lambda, double *beta, double *ra, double *dec)
/*                                       */
{
/**/
int iaus;
double eqx;
int pop_ecleqx();
/**/
iaus=2;            /* Use J2000 epoch. */
eqx =2000.;
pop_ecleqx(iaus,eqx,*lambda,*beta,ra,dec);
return;
}

/* --------------------------------------------------------------------- */
/* Equatorial to Ecliptic ( FORTRAN front end for equecx. )              */
/*                                                                       */
/* ra    = RA in degrees         (input) */
/* dec   = Dec in degrees        (input) */
/* lambda= ecliptic longitude in degrees (0 to 360) (output)    */
/* beta  = ecliptic latitude in degrees  (-90 to +90) (output)  */
/*                                        */
void equtoecl_ (double *ra, double *dec, double *lambda, double *beta)
/*                                       */
{
/**/
int iaus;
double eqx;
int pop_equecx();
/**/
iaus=2;            /* Use J2000 epoch. */
eqx =2000.;
pop_equecx(iaus,eqx,*ra,*dec,lambda,beta);
return;
}




/* ------------------------------------------------------------------- */
/* The following is from Tim Conrow's dir: /proj/wire/tim/pipe/pop/    */
/*                                                                     */

/* ------------------------------------------------------------------- */
double copysign(double,double);

/* ------------------------------------------------------------------- */
/* Galactic to Equatorial                                              */
/*                                                                     */
int pop_galeqd(double xl,double xb,double *rad,double *decd)
{
  /* galeqd,equgad,sglgal,galsgl subroutines moved from jcnvcd.f file to galeqd.f */
  /*   on 14sep92 - no changes to code, itself, made (code same as 17jan91) */
  /* galeqd (modifcation of galequ) requires double prec. args (22oct90) */
  /*ccc  entry equcor(xl,xb,rad,decd)   obsolete entry pt equcor. */
  /* 22mar85 for hepvax (changed darsin to asin;removed entry equcor) */
  /* 17mar82 updated so has more sensible name- galequ, but still */
  /*   retains alias for equcor. */
  /* 02oct80  jdb   29jan81 to input dtor,rtod to more places. 22apr81 */
  /* subroutine to compute earth equatorial coords from galactic coords */
  /* inputs: xl l in degrees, xb b in degrees. */
  /* b = galactic latitude (-90. to + 90. deg);l=galactic longitude(0-360) */
  /* outputs: rad ra in degrees, decd dec in degrees. */
  /*  x = cos(b)*cos(l) */
  /*  y = sin(l)*cos(b) */
  /*  z = sin(b) */
  /*  gal <==> equ equations from tom soifer.  see also: */
  /*   Classical Mechanics by Herbert Goldstein (Addison-Wesley Publ.Co., */
  /*   c.1950, 7th printing 1965) Section 4-4 on Eulerian Angles (p.107-109) */

  /* assumes gal north pole at 12h 49.0m, +27d24' (192.25d,+27.4d) */
  /*         gal (0, 0) at     17h 42.4m, -28d55' (265.36d,-28.91d) */
  /*   also: delta=lat=0 at gal (33d,0d) ; equ (282.25d,0d). */

  double xlr,xbr,cosb,cosl,sinl,x,y,z,sind,
       cosd,decr,rar,eq2,eq3,cosa,sina;

  static double a[4],b[4],c[4];
  static double cosph,sinph,cosps,sinps,costh,sinth;
  static double dtor, rtod;
  static double psi=-33.0, theta=62.6, phi=282.25;
  static int nthru=0;

  if(nthru == 0) {
               dtor = atan(1.0) / 45.0;
               rtod = 1.0 / dtor;
               /*         compute matrix for gal ==> equ conversion: */
               cosps = cos(psi*dtor);
               sinps = sin(psi*dtor);
               cosph = cos(phi*dtor);
               sinph = sin(phi*dtor);
               costh = cos(theta*dtor);
               sinth = sin(theta*dtor);
       
               /* |  b[1]  b[2]  b[3]  | */
               /* |  c[1]  c[2]  c[3]  | */
               /* |  a[1]  a[2]  a[3]  | */
       
               b[1] = cosps*cosph - costh*sinph*sinps;
               b[2] = -(sinps*cosph) - costh*sinph*cosps;
               b[3] = sinth*sinph;
       
               c[1] = cosps*sinph + costh*cosph*sinps;
               c[2] = -(sinps*sinph) + costh*cosph*cosps;
               c[3] = -(sinth*cosph);
       
               a[1] = sinth*sinps;
               a[2] = sinth*cosps;
               a[3] = costh;
       
               nthru = 1;
             }
                                                   
                                                   
  xlr = xl * dtor;
  xbr = xb * dtor;
  cosb = cos(xbr);
  cosl = cos(xlr);
  sinl = sin(xlr);
  z    = sin(xbr);
  x = cosb*cosl;
  y = sinl*cosb;
  sind = a[1]*x + a[2]*y + a[3]*z;
  if(fabs(sind)>=1.) {
                   sind = copysign(1.,sind);
                   decr =  asin(sind);
                   rar = 0.;
                 }
  else {
       eq2  = b[1]*x + b[2]*y + b[3]*z;
       eq3  = c[1]*x + c[2]*y + c[3]*z;
       decr =  asin(sind);
       cosd = sqrt(1.-sind*sind);
       cosa = eq2/cosd;
       sina = eq3/cosd;
       if(fabs(cosa)>1.) cosa = copysign(1.,cosa);
       if(fabs(sina)>1.) sina = copysign(1.,sina);
       rar = atan2(sina,cosa);
       }
  *rad  = rar * rtod;
  if(*rad < 0.0) *rad = 360.0 + *rad;
  *decd = decr * rtod;
  if(fabs(*decd) >= 90.0) {
                      *rad = 0.0;
                      if(*decd >  90.0) *decd =  90.0;
                      if(*decd < -90.0) *decd = -90.0;
                    }
  return(0);                                             
}

/* ------------------------------------------------------------------- */
/* Equatorial to Galactic                                              */
/*                                                                     */
int pop_equgad(double rad,double decd,double *xl,double *xb)
{
  /* equgad (modification of equgal) requires double prec. args (22oct90) */

  /* 22mar85 for hepvax (darsin changed to asin;entry galcor removed) */
  /*c    entry galcor(rad,decd,xl,xb) */
  /* 17mar82 updated for more sensible name- equgal, while retaining */
  /*     alias for old name of galcor. */
  /* 02oct80 jdb:subroutine to compute galactic coords from earth */
  /*  equatorial coords.29jan81 for more places in dtor,rtod.22apr81 */
  /*  gal <==>equ equations from tom soifer.  see also: */
  /*   Classical Mechanics by Herbert Goldstein (Addison-Wesley Publ.Co., */
  /*     c.1950, 7th printing 1965) Section 4-4 on Eulerian Angles (p.107-109) */
  /* assumes gal north pole at 12h 49.0m, +27d24' (192.25d,+27.4d) */
  /*         gal (0, 0) at     17h 42.4m, -28d55' (265.36d,-28.91d) */
  /*   also: delta=lat=0 at gal (33d,0d) ; equ (282.25d,0d). */

  /* inputs: rad ra in degrees, decd is dec in degrees. */
  /* b = galactic latitude (-90. to + 90. deg);l=galactic longitude(0-360) */

  /*  x = cos(alpha)*cos(delta) */
  /*  y = sin(alpha)*cos(delta) */
  /*  z = sin(delta) */
  /*  eq1 = sin(b) = -.8676*x - .1884*y + .4602*z */
  /*  eq2 = cos(b)*cos(l) = -.0669*x - .8728*y - .4835*z */
  /*  eq3 = cos(b)*sin(l) = +.4927*x - .4503*y + .7446*z */
  /*    b = arsin(sin(b)) */
  /*  cos(b) = sqrt(1.-sin(b)*sin(b)) */
  /*  cos(l) = eq2/cos(b) */
  /*  sin(l) = eq3/cos(b) */
  /*  l = atan2(sin(l)/cos(l): if(l<0) l = l+2*pi if radians. */

  double rar,decr,cosa,sina,cosd,x,y,z,cosb,
         sinb,eq2,eq3,xbr,xlr,cosl,sinl;

  static double a[4],b[4],c[4];
  static double cosph,sinph,cosps,sinps,costh,sinth;
  static double dtor, rtod;
  static double psi=-33.0, theta=62.6, phi=282.25;
  static int nthru=0;

  if(nthru == 0) {
               dtor = atan(1.0) / 45.0;
               rtod = 1.0 / dtor;
               /* compute matrix for equ ==> gal conversion: */
               cosps = cos(psi*dtor);
               sinps = sin(psi*dtor);
               cosph = cos(phi*dtor);
               sinph = sin(phi*dtor);
               costh = cos(theta*dtor);
               sinth = sin(theta*dtor);

               /* |  b[1]  b[2]  b[3]  | */
               /* |  c[1]  c[2]  c[3]  | */
               /* |  a[1]  a[2]  a[3]  | */

               b[1] = cosps*cosph - costh*sinph*sinps;
               b[2] = cosps*sinph + costh*cosph*sinps;
               b[3] = sinps*sinth;

               c[1] = -(sinps*cosph) - costh*sinph*cosps;
               c[2] = -(sinps*sinph) + costh*cosph*cosps;
               c[3] =  cosps*sinth;

               a[1] = sinth*sinph;
               a[2] = -(sinth*cosph);
               a[3] = costh;

               nthru = 1;
             }


  rar  = rad * dtor;
  decr = decd * dtor;
  cosa = cos(rar);
  sina = sin(rar);
  cosd = cos(decr);
  z    = sin(decr);
  x = cosa*cosd;
  y = sina*cosd;
  sinb = a[1]*x + a[2]*y + a[3]*z;
  if(fabs(sinb)>=1.) {
                   sinb = copysign(1.,sinb);
                   xbr =  asin(sinb);
                   xlr = 0.;
                 }
  else {
       eq2  = b[1]*x + b[2]*y + b[3]*z;
       eq3  = c[1]*x + c[2]*y + c[3]*z;
       xbr =  asin(sinb);
       cosb = sqrt(1.-sinb*sinb);
       cosl = eq2/cosb;
       sinl = eq3/cosb;
       if(fabs(cosl)>1.) cosl = copysign(1.,cosl);
       if(fabs(sinl)>1.) sinl = copysign(1.,sinl);
       xlr = atan2(sinl,cosl);
       }
  *xl  = xlr * rtod;
  if(*xl < 0.0) *xl = 360.0 + *xl;
  *xb = xbr * rtod;
  if(fabs(*xb) >= 90.0) {
                    *xl = 0.0;
                    if(*xb >  90.0) *xb =  90.0;
                    if(*xb < -90.0) *xb = -90.0;
                  }

  return(0);
}

/* ------------------------------------------------------------------- */
int pop_galsgl(double rad,double decd,double *xl,double *xb)
{
  /* galsgl (modification of equgad) to compute supergalactic coordinates */
  /*  from galactic coordinates.  j.bennett  30oct-05nov90 */

  /* inputs: rad =  gal lon (l) in degrees. double. */
  /*         decd = gal lat (b) in degrees. double. */
  /* returned: */
  /* xb = supergalactic latitude  (SGB)  (-90. to + 90. deg). double. */
  /* xl = supergalactic longitude (SGL)  (0-360. deg). double. */

  /*  Computed values of rotation used for the Eulerian angles: */
  /*   phi = 137.37 deg.;  theta = 83.68 deg.;  psi = 0. deg. */
  /*    Based on gal l,b = 137.37, 0 deg. at sg SGL,SGB = 0., 0. and */
  /*         SG North Pole (0,+90) at gal l,b = 47.37, +6.32 deg. */

  /* References: */
  /* de Vaucouleurs,G., A. de Vaucouleurs, and G.H.Corwin, */
  /*  Second Reference Catalog of Bright Galaxies, (1976), p.8 */
  /*  */
  /* Tully,B., Nearby Galaxies Catalog, (1988) p. 1,4-5 */
  /*   */
  /*    Note: de Vaucouleurs gives gal l,b 137.29, 0 for SGL,SGB= 0,0; */
  /*          this is not 90 degrees from Galactic North Pole.- used */
  /*          Tully's value of 137.37, 0 deg. */

  /*  gal <==> equ equations from tom soifer.  see also: */
  /*   Classical Mechanics by Herbert Goldstein (Addison-Wesley Publ.Co., */
  /*     c.1950, 7th printing 1965) Section 4-4 on Eulerian Angles  */
  /*     (p.107-109) */
  /*    note: such equations also appropriate for gal <==> sgl . */
  /*          See code below for def. of arrays a,b,c. */
  /*  x = cos(lon)*cos(lat) */
  /*  y = sin(lon)*cos(lat) */
  /*  z = sin(lat) */
  /*  eq1 = sin(sgb)          =   a[1]*x +  a[2]*y +  a[3]*z */
  /*  eq2 = cos(sgb)*cos(sgl) =   b[1]*x +  b[2]*y +  b[3]*z */
  /*  eq3 = cos(sgb)*sin(sgl) =   c[1]*x +  c[2]*y +  c[3]*z */
  /*  sgb = arsin(sin(sgb)) */
  /*  cos(sgb) = sqrt(1.-sin(sgb)*sin(sgb)) */
  /*  cos(sgl) = eq2/cos(sgb) */
  /*  sin(sgl) = eq3/cos(sgb) */
  /*  sgl = atan2(sin(sgl)/cos(sgl): if(sgl<0) sgl = sgl+2*pi if radians. */

  double rar,decr,cosa,sina,cosd,x,y,z,cosb,
         sinb,eq2,eq3,xbr,xlr,cosl,sinl;

  static double a[4],b[4],c[4];
  static double cosph,sinph,cosps,sinps,costh,sinth;
  static double dtor, rtod;
  static double psi=  0.0, theta=83.68, phi=137.37;
  static int nthru=0;

  if(nthru == 0) {
               dtor = atan(1.0) / 45.0;
               rtod = 1.0 / dtor;
               /* compute matrix for equ ==> gal conversion: */
               cosps = cos(psi*dtor);
               sinps = sin(psi*dtor);
               cosph = cos(phi*dtor);
               sinph = sin(phi*dtor);
               costh = cos(theta*dtor);
               sinth = sin(theta*dtor);

               /* |  b[1]  b[2]  b[3]  | */
               /* |  c[1]  c[2]  c[3]  | */
               /* |  a[1]  a[2]  a[3]  | */

               b[1] = cosps*cosph - costh*sinph*sinps;
               b[2] = cosps*sinph + costh*cosph*sinps;
               b[3] = sinps*sinth;

               c[1] = -(sinps*cosph) - costh*sinph*cosps;
               c[2] = -(sinps*sinph) + costh*cosph*cosps;
               c[3] =  cosps*sinth;

               a[1] = sinth*sinph;
               a[2] = -(sinth*cosph);
               a[3] = costh;

               nthru = 1;
             }


  rar  = rad * dtor;
  decr = decd * dtor;
  cosa = cos(rar);
  sina = sin(rar);
  cosd = cos(decr);
  z    = sin(decr);
  x = cosa*cosd;
  y = sina*cosd;
  sinb = a[1]*x + a[2]*y + a[3]*z;
  if(fabs(sinb)>=1.) {
                   sinb = copysign(1.,sinb);
                   xbr =  asin(sinb);
                   xlr = 0.;
                 }
  else {
       eq2  = b[1]*x + b[2]*y + b[3]*z;
       eq3  = c[1]*x + c[2]*y + c[3]*z;
       xbr =  asin(sinb);
       cosb = sqrt(1.-sinb*sinb);
       cosl = eq2/cosb;
       sinl = eq3/cosb;
       if(fabs(cosl)>1.) cosl = copysign(1.,cosl);
       if(fabs(sinl)>1.) sinl = copysign(1.,sinl);
       xlr = atan2(sinl,cosl);
       }
  *xl  = xlr * rtod;
  if(*xl < 0.0) *xl = 360.0 + *xl;
  *xb = xbr * rtod;
  if(fabs(*xb) >= 90.0) {
                    *xl = 0.0;
                    if(*xb >  90.0) *xb =  90.0;
                    if(*xb < -90.0) *xb = -90.0;
                  }

  return(0);
}

/* ------------------------------------------------------------------- */
int pop_sglgal(double xl,double xb,double *rad,double *decd)
{
  /* sglgal (modification of galeqd) to compute galactic coordinates */
  /*  from supergalactic coordinates.  j.bennett  30oct-05nov90 */

  /* inputs: */
  /* xb = supergalactic latitude  (SGB)  (-90. to + 90. deg). double. */
  /* xl = supergalactic longitude (SGL)  (0-360. deg). double. */
  /* returned: rad =  gal lon (l) in degrees. double. */
  /*           decd = gal lat (b) in degrees. double. */

  /*  Computed values of rotation used for the Eulerian angles: */
  /*   phi = 137.37 deg.;  theta = 83.68 deg.;  psi = 0. deg. */
  /*    Based on gal l,b = 137.37, 0 deg. at sg SGL,SGB = 0., 0. and */
  /*         SG North Pole (0,+90) at gal l,b = 47.37, +6.32 deg. */

  /* References: */
  /* de Vaucouleurs,G., A. de Vaucouleurs, and G.H.Corwin, */
  /*  Second Reference Catalog of Bright Galaxies, (1976), p.8 */
  /*  */
  /* Tully,B., Nearby Galaxies Catalog, (1988) p. 1,4-5 */
  /*   */
  /*    Note: de Vaucouleurs gives gal l,b 137.29, 0 for SGL,SGB= 0,0; */
  /*          this is not 90 degrees from Galactic North Pole.- used */
  /*          Tully's value of 137.37, 0 deg. */

  /*  gal <==> equ equations from tom soifer.  see also: */
  /*   Classical Mechanics by Herbert Goldstein (Addison-Wesley Publ.Co., */
  /*     c.1950, 7th printing 1965) Section 4-4 on Eulerian Angles  */
  /*     (p.107-109) */
  /*    note: such equations also appropriate for gal <==> sgl . */
  /*          See code below for def. of arrays a,b,c. */
  /*  x = cos(sgb)*cos(sgl) */
  /*  y = sin(sgl)*cos(sgb) */
  /*  z = sin(sgb) */
  /*  eq1 = sin(lat)          =   a[1]*x +  a[2]*y +  a[3]*z */
  /*  eq2 = cos(lat)*cos(lon) =   b[1]*x +  b[2]*y +  b[3]*z */
  /*  eq3 = cos(lat)*sin(lon) =   c[1]*x +  c[2]*y +  c[3]*z */
  /*  sgb = arsin(sin(lat)) */
  /*  cos(lat) = sqrt(1.-sin(lat)*sin(lat)) */
  /*  cos(lon) = eq2/cos(lat) */
  /*  sin(lon) = eq3/cos(lat) */
  /*  lon = atan2(sin(lon)/cos(lon): if(lon<0) lon = lon+2*pi if radians. */

  double xlr,xbr,cosb,cosl,sinl,x,y,z,sind,
         cosd,decr,rar,eq2,eq3,cosa,sina;

  static double a[4],b[4],c[4];
  static double cosph,sinph,cosps,sinps,costh,sinth;
  static double dtor, rtod;
  static double psi=  0.0, theta=83.68, phi=137.37;
  static int nthru=0;

  if(nthru == 0) {
               dtor = atan(1.0) / 45.0;
               rtod = 1.0 / dtor;

               /* compute matrix for gal ==> equ conversion: */
               cosps = cos(psi*dtor);
               sinps = sin(psi*dtor);
               cosph = cos(phi*dtor);
               sinph = sin(phi*dtor);
               costh = cos(theta*dtor);
               sinth = sin(theta*dtor);

               /* |  b[1]  b[2]  b[3]  | */
               /* |  c[1]  c[2]  c[3]  | */
               /* |  a[1]  a[2]  a[3]  | */

               b[1] = cosps*cosph - costh*sinph*sinps;
               b[2] = -(sinps*cosph) - costh*sinph*cosps;
               b[3] = sinth*sinph;

               c[1] = cosps*sinph + costh*cosph*sinps;
               c[2] = -(sinps*sinph) + costh*cosph*cosps;
               c[3] = -(sinth*cosph);

                 a[1] = sinth*sinps;
               a[2] = sinth*cosps;
               a[3] = costh;

               nthru = 1;
             }


  xlr = xl * dtor;
  xbr = xb * dtor;
  cosb = cos(xbr);
  cosl = cos(xlr);
  sinl = sin(xlr);
  z    = sin(xbr);
  x = cosb*cosl;
  y = sinl*cosb;
  sind = a[1]*x + a[2]*y + a[3]*z;
  if(fabs(sind)>=1.) {
                   sind = copysign(1.,sind);
                   decr =  asin(sind);
                   rar = 0.;
                 }
  else {
       eq2  = b[1]*x + b[2]*y + b[3]*z;
       eq3  = c[1]*x + c[2]*y + c[3]*z;
       decr =  asin(sind);
       cosd = sqrt(1.-sind*sind);
       cosa = eq2/cosd;
       sina = eq3/cosd;
       if(fabs(cosa)>1.) cosa = copysign(1.,cosa);
       if(fabs(sina)>1.) sina = copysign(1.,sina);
       rar = atan2(sina,cosa);
       }
  *rad  = rar * rtod;
  if(*rad < 0.0) *rad = 360.0 + *rad;
  *decd = decr * rtod;
  if(fabs(*decd) >= 90.0) {
                      *rad = 0.0;
                      if(*decd >  90.0) *decd =  90.0;
                      if(*decd < -90.0) *decd = -90.0;
                    }

  return(0);
}

/* ------------------------------------------------------------------- */
/* Direct copy from Judy Bennet's ECLEQX.F routine, translated to C.   */
/*  ecleqx.f  contains subs jgtobq,ecleqx,equecx                       */
/*    30jul92  j.bennett                                               */
/*                                                                     */
void jgtobq( int iaus, double eqx, double *ep)
{
/*  find value of epsilon (obliquity of ecliptic) for eqx */
/*   iaus is iau system : 1=Besselian; 2 (or anything but 1) = Julian. */
/*   eqx is equinox year (double precision) in terms of system designated */
/*        by iaus.  e.g. if B1950 wanted, then iau=1,eqx=1950.0d0; */
/*        if J2000 wanted, then iau=2,eqx=2000.0d0 */
/*   ep is epsilon in degrees (returned)  (double precision) */
/*  */
/*     iaus integer; all other arguments double precision. */
/*  if iaus = 2 (Julian): */
/*  ep   = epsilon = obliquity of the ecliptic of date (with respect to the */
/*         mean equator of date.(value from p.S26 of 1984 Astr.Alman.Supp) */
/*         epsilon = 84381.448" - 46.8150"*t - 0.00059"t*t + 0.001813"*t*t*t */
/*      where t is time measured in Julian centuries from epoch J2000.0 */
/*           note: the resulting ep when  t = -.5000021 (for B1950) is used */
/*                 differs by .015" of value usually */
/*                 used for B1950 of 23d26'44.84"(the value usually used */
/*                 for B1950). This discrepancy should cause no problems in */
/*                 in our precession conversion calculations. */
/*                 ep is used only in the E-term removal portion of conversion  */
/*                 at poles.  However, for real work, where the actually */
/*                 position is required, use the following formula for Besselian. */
/*  if iaus = 1 (Besselian): */
/*  ep   = epsilon = obliquity of the ecliptic (p.B18 of 1981-1983Astr.Alman.) */
/*         epsilon = 84404.84"  - 46.850"*t - 0.0033t*t + 0.00182"*t*t*t */
/*      where t is time measured in Besselian centuries from epoch B1950.0 */


  double t, t2, t3;

  if(iaus != 1) {
    t = (eqx - 2000.0) * 0.01;
    t2 = t*t;
    t3 = t*t2;
    
    *ep = (84381.448 - 46.8150*t - 0.00059*t2 + 0.001813*t3)/
      3600.0;
    
  }
  
  else {
    
    t = (eqx - 1950.0) * 0.01;
    t2 = t*t;
    t3 = t*t2;
    
    *ep = (84404.84  - 46.850*t  - 0.0033*t2  + 0.00182*t3)/
      3600.0;

  }

  return;
}


/* ------------------------------------------------------------------- */
/* Ecliptic to Equatorial                                              */
/*                                                                     */
int pop_ecleqx(int iaus, double eqx, double xlam, double beta, 
          double *rad, double *decd)
{
/*  version of ecleqd that adds args.iaus and  eqx so epsilon may be  */
/*        determined for that date.  Calls jgtobq. */
/*  ecliptic to  equatorial coordinate subroutine */

/*  equations from j.fowler (jan 1981) */

/*     iaus integer; all other arguments double precision. */
/*  inputs: */
/*   iaus is iau system : 1=Besselian; 2 (or anything but 1) = Julian. */
/*       iaus must apply to both input and output positions (ecleqx does */
/*       no Besselian to/from Julian conversions. */
/*   eqx is equinox year (double precision) in terms of system designated */
/*        by iaus.  e.g. if B1950 wanted, { iau=1,eqx=1950.0d0; */
/*        if J2000 wanted, { iau=2,eqx=2000.0d0 */
/*        Note: eqx applies to both input and output (ecleqx does no precession). */
/*   xlam=lambda=ecliptic longitude in decimal degrees (0.0d0 - 360.0d0 deg) */

/*   beta= ecliptic latitude in decimal degrees (-90.0d0 to +90.0d0 deg) */

/*   xl = sin(beta)              xe = xl*cos(e) - yl*sin(e) */
/*   yl = -cos(beta)*sin(lambda) ye = xl*sin(e) + yl*cos(e) */
/*   zl = cos(beta)*cos(lambda)  ze = zl */

/*   outputs: rad ra in decimal degrees; decd dec in decimal degrees. */

  double cosb,cosl,sinb,sinl,xl,yl,zl,xe,ye,ze,
         xlamr,betar,rar,decr,copysign();
  double e;
  static double leqx=-1.0, dtor, rtod, cose, sine;
  static int  nthru=0, laus=-99;

  if(nthru == 0) {
    dtor = atan(1.0) / 45.0;
    rtod = 1.0 / dtor;
    nthru = 1;
  }

  if(eqx != leqx || iaus != laus) {
    jgtobq(iaus,eqx,&e);
    e = e * dtor;
    cose = cos(e);
    sine = sin(e);
    leqx = eqx;
    laus = iaus;
  }

  xlamr = xlam*dtor;
  betar = beta*dtor;
  
  cosb = cos(betar);
  cosl = cos(xlamr);
  sinb = sin(betar);
  sinl = sin(xlamr);
  
  xl = sinb;
  yl = -(cosb*sinl);
  zl = cosb*cosl;

  xe = cose*xl - sine*yl;
  ye = sine*xl + cose*yl;
  ze = zl;

  rar = atan2(-ye,ze);
  *rad = rar * rtod;
  if(*rad < 0.0) *rad = 360.0 + *rad;

/*  try to catch pole on any machine (& return ra=0 {) */
  if(fabs(xe) > 1.0) {
    *decd = copysign(90.0,xe);
    *rad = 0.0;
  }
  else {
    decr =  asin(xe);
    *decd = decr * rtod;
    if(fabs(*decd) > 90.0) {
      *rad = 0.0;
      if(*decd >  90.0) *decd =  90.0;
      if(*decd > -90.0) *decd = -90.0;
    }
  }
  
  return(0);
}

/* ------------------------------------------------------------------- */
/* Equatorial to Ecliptic                                              */
/*                                                                     */
int pop_equecx(int iaus, double eqx, double rad, double decd, 
          double *xlam, double *beta)
{
/*  version of equecd that adds args. iaus and  eqx so epsilon may be */
/*         determined for that date.  Calls jgtobq. */
/*   equatorial to ecliptic conversion coordinate subroutine */

/*   equations from j.fowler (jan 1981) */

/*  inputs: */
/*   iaus is iau system : 1=Besselian; 2 (or anything but 1) = Julian. */
/*       iaus must apply to both input and output positions (equecx does */
/*       no Besselian to/from Julian conversions. */
/*   eqx is equinox year (double precision) in terms of system designated */
/*        by iaus.  e.g. if B1950 wanted, { iau=1,eqx=1950.0d0; */
/*        if J2000 wanted, { iau=2,eqx=2000.0d0 */
/*        Note: eqx applies to both input and output (equecx does no precession). */
/*   rad ra in deciaml degrees; decd dec in decimal degrees. */

/*  returned: xlam=lambda=ecliptic longitude in decimal deg.(0.0d0 - 360.0d0 deg) */

/*            beta= ecliptic latitude in decimal degrees (-90.0d0 to +90.0d0 deg) */

/*   xe = sin(dec)              xl = xe*cos(e) - ye*sin(e) */
/*   ye = -cos(dec)*sin(ra)     yl = xe*sin(e) + ye*cos(e) */
/*   ze = cos(dec)*cos(ra)      zl = ze */

  double cosd,cosr,sind,sinr,xl,yl,zl,xe,ye,ze,
         xlamr,betar,rar,decr,copysign();
  double e;

  static double leqx=-1.0, dtor, rtod, cose, sine;
  static int  nthru=0, laus=-99;

  if(nthru == 0) {
    dtor = atan(1.0) / 45.0;
    rtod = 1.0 / dtor;
    nthru = 1;
  }

  if(eqx != leqx || iaus != laus) {
    jgtobq(iaus,eqx,&e);
    e = e * dtor;
    cose = cos(e);
    sine = sin(e);
    leqx = eqx;
    laus = iaus;
  }

  rar = rad*dtor;
  decr = decd*dtor;

  cosd = cos(decr);
  cosr = cos(rar);
  sind = sin(decr);
  sinr = sin(rar);

  xe = sind;
  ye = -(cosd*sinr);
  ze = cosd*cosr;

  xl = cose*xe + sine*ye;
  yl = -sine*xe + cose*ye;
  zl = ze;

  if(fabs(xl) > 1.0) {
    *beta = copysign(90.0,xl);
    *xlam = 0.0;
    return(0);
  }
  else {
    betar =  asin(xl);
    xlamr = atan2(-yl,zl);
    *xlam = xlamr * rtod;
    if(*xlam < 0.0) *xlam = 360.0 + *xlam;
    *beta = betar * rtod;
    if(fabs(*beta) > 90.0) {
      if(*beta >  90.0) *beta =  90.0;
      if(*beta < -90.0) *beta = -90.0;
      *xlam = 0.0;
    }
  }

  return(0);
}
