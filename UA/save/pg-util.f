C pg-util.f  miscellaneous utilities to use with PGPLOT routines   tab June 92
C

C--------------------------------------------------------------------
C Marks the position of an emission or absorption line.
C
      subroutine PG_Mark(x,y1,y2)
      implicit none
      real*4 x,y1,y2,xx(2),yy(2)
      xx(1)=x
      xx(2)=x
      yy(1)=y1
      yy(2)=y2
      call PGLINE(2,xx,yy)
      return
      end

C--------------------------------------------------------------------
C Draws a line.
C
      subroutine PG_Line(x1,x2,y1,y2)
      implicit none
      real*4 x1,x2,y1,y2,xx(2),yy(2)
      xx(1)=x1
      xx(2)=x2
      yy(1)=y1
      yy(2)=y2
      call PGLINE(2,xx,yy)
      return
      end

C--------------------------------------------------------------------
C Draws a horizontal line.
C
      subroutine PG_HLine(x1,x2,y)
      implicit none
      real*4 x1,x2,y,xx(2),yy(2)
      xx(1)=x1
      xx(2)=x2
      yy(1)=y
      yy(2)=y
      call PGLINE(2,xx,yy)
      return
      end

C--------------------------------------------------------------------
C Draws a vertical line.
C
      subroutine PG_VLine(y1,y2,x)
      implicit none
      real*4 y1,y2,x,xx(2),yy(2)
      xx(1)=x
      xx(2)=x
      yy(1)=y1
      yy(2)=y2
      call PGLINE(2,xx,yy)
      return
      end

C--------------------------------------------------------------------
C Does PGLINE/PGBBUF in segments to avoid hanging the program.  It will not
C buffer more than 2000 segments at a time.  There may be a way to fix
C this problem by way of the system configuration.  Or it might be possible
C to take care of it within the PGPLOT utilities.
C
      subroutine PG_LineBuf(n,x,y)
      implicit none
      integer*4 n,nn,m,i,j
      real*4 x(n),y(n)
      if (n.lt.2001) then
        call PGBBUF
        call PGLINE(n,x,y)
        call PGEBUF
      else
        m = (n/2000) + 1
        do i=1,m
          call PGBBUF
          j = 1 + (i-1)*2000
          nn = min(2001,(n-j)+1)
          call PGLINE(nn,x(j),y(j))
          call PGEBUF
        enddo
      endif
      return
      end

C--------------------------------------------------------------------
C Does PGPT/PGBBUF in segments to avoid hanging the program.  It will not
C buffer more than 2000 segments at a time.  There may be a way to fix
C this problem by way of the system configuration.  Or it might be possible
C to take care of it within the PGPLOT utilities.
C "symbol" (input) is code number of the symbol to be drawn at each point:
C            -1, -2  : a single dot (diameter = current line width).
C            -3..-31 : a regular polygon with ABS(SYMBOL) edges
C             0.. 31 : standard marker symbols.
C            32..127 : ASCII characters (in current font).
C             > 127  :  a Hershey symbol number.
C
      subroutine PG_DotBuf(n,x,y,symbol)
      implicit none
      integer*4 n,nn,m,i,j,symbol
      real*4 x(n),y(n)
      if (n.lt.2001) then
        call PGBBUF
        call PGPT(n,x,y,symbol)
        call PGEBUF
      else
        m = (n/2000) + 1
        do i=1,m
          call PGBBUF
          j = 1 + (i-1)*2000
          nn = min(2001,(n-j)+1)
          call PGPT(nn,x(j),y(j),symbol)
          call PGEBUF
        enddo
      endif
      return
      end

C--------------------------------------------------------------------
C Does PGBIN/PGBBUF in segments to avoid hanging the program.  It will not
C buffer more than 1000 segments at a time.  There may be a way to fix
C this problem by way of the system configuration.  Or it might be possible
C to take care of it within the PGPLOT utilities.
C
      subroutine PG_BinBuf(n,x,y,center)
      implicit none
      integer*4 n,nn,m,i,j
      real*4 x(n),y(n)
      logical center
      if (n.lt.1001) then
        call PGBBUF
        call PGBIN(n,x,y,center)
        call PGEBUF
      else
        m = (n/1000) + 1
        do i=1,m
          call PGBBUF
          j = 1 + (i-1)*1000
          nn = min(1001,(n-j)+1)
          call PGBIN(nn,x(j),y(j),center)
          call PGEBUF
        enddo
      endif
      return
      end

C--------------------------------------------------------------------------
C A kinder, gentler entry into PGCURSE.
C The previous values of nx,ny (normalized x and y) from the last call
C are used, rather than the parameter values cx,cy.
C This keeps the cursor from jumping around.
C If you want the cursor to jump, use PGCURSE.
C
      subroutine PG_Cursor(cx,cy,key)
C
      implicit none
C
      real*4 cx,cy,nx,ny,x1,x2,y1,y2
      character*1 key
C
      common /PG_CursorBLK/ nx,ny
C
      call PGQWIN(x1,x2,y1,y2)
      cx = x1 + nx*(x2-x1)
      cy = y1 + ny*(y2-y1)
      call PGCURSE(cx,cy,key)
      nx = (cx-x1)/(x2-x1)
      ny = (cy-y1)/(y2-y1)
      return
      end

C---------------------------------------------------------------------------
C Draws error bars: n=number points, e=error, mark_size=half length of mark.
C hv = 'H' or 'V' for horizontal or vertical error bars.

      subroutine PG_ErrorBar(n,x,y,e,mark_size,hv)

      implicit none
      integer*4 n,i
      real*4 x(*),y(*),e(*),mark_size
      real*4 xx(6),yy(6) 
      character*1 hv 

      if (hv.eq.'V') then
        do i=1,n
          if (abs(e(i)).gt.0.) then
            xx(1) = x(i) + mark_size
            xx(2) = x(i) - mark_size
            xx(3) = x(i)
            xx(4) = x(i)
            xx(5) = x(i) + mark_size
            xx(6) = x(i) - mark_size
            yy(1) = y(i) - e(i)
            yy(2) = yy(1)
            yy(3) = yy(1)
            yy(4) = y(i) + e(i)
            yy(5) = yy(4)
            yy(6) = yy(4)
            call PGLINE(6,xx,yy)
          endif 
        enddo 
      else
        do i=1,n
          if (abs(e(i)).gt.0.) then
            xx(1) = x(i) - e(i)
            xx(2) = xx(1)
            xx(3) = xx(1)
            xx(4) = x(i) + e(i)
            xx(5) = xx(4)
            xx(6) = xx(4)
            yy(1) = y(i) + mark_size
            yy(2) = y(i) - mark_size
            yy(3) = y(i)
            yy(4) = y(i)
            yy(5) = y(i) + mark_size
            yy(6) = y(i) - mark_size
            call PGLINE(6,xx,yy)
          endif 
        enddo 
      endif  

      return
      end  

C---------------------------------------------------------------------------
C Draws error bars with different error ticks left and right of the mark.
C n=number points, yl=left error, yr=right error, mark_size=mark size in
C the horizontal direction.

      subroutine PG_Pt2Err(n,x,y,yl,yr,mark_size)

      implicit none
      integer*4 n,i
      real*4 x(*),y(*),yl(*),yr(*),mark_size
      real*4 xx(4),yy(4) 

      do i=1,n
        if (abs(yr(i)).gt.0.) then
          xx(1) = x(i) + mark_size 
          xx(2) = x(i)
          xx(3) = x(i)
          xx(4) = x(i) + mark_size
          yy(1) = y(i) - yr(i)
          yy(2) = y(i) - yr(i)
          yy(3) = y(i) + yr(i)
          yy(4) = y(i) + yr(i)
          call PGLINE(4,xx,yy)
        endif 
        if (abs(yl(i)).gt.0.) then
          xx(1) = x(i) - mark_size 
          xx(2) = x(i)
          xx(3) = x(i)
          xx(4) = x(i) - mark_size
          yy(1) = y(i) - yl(i)
          yy(2) = y(i) - yl(i)
          yy(3) = y(i) + yl(i)
          yy(4) = y(i) + yl(i)
          call PGLINE(4,xx,yy)
        endif 
      enddo 

      return
      end  


C------------------------------------------------------------------
C Histogram of data points (logarithmic in number).
C Expressed as a fraction of the total number of points.
C Mode: 1=normal, 0=do not make new box or window.
C Mode: 2=normal, but do plot lines in dotted lines.
C
      subroutine PG_BinHistLogFraction(y,n,xmin,xmax,xinc,xtick,nxtick,
     .                                 mode,uymin,uymax)
C
      implicit none
      integer*4 n,nb,i,nxtick,binnum
      real*4 y(*),xmin,xmax,xtick,uymin,uymax
      integer*4 max_nb,mode
      parameter(max_nb=9000)
      real*4 xinc,xb(max_nb),yb(max_nb),yr,ymin,ymax
C
C Initialize.
      nb = max(1,min(max_nb,nint((xmax-xmin)/xinc)+1))
      do i=1,nb
        xb(i)= xmin + ( (xinc*float(i-1)) + (xinc/2.) )
        yb(i)= 0.
      enddo
C Count number in bins.
      do i=1,n
        binnum = max(1,min(nb, nint(((y(i)-xmin)+(xinc/2.))/xinc) ))
        yb(binnum) = yb(binnum) + 1.
      enddo
C Fraction of the total number of points.
      do i=1,nb
        yb(i) = yb(i) / float(n)
      enddo
C Logarithmic in yb.
      do i=1,nb
        if (yb(i).lt.1.e-30) yb(i)=1.e-30
        yb(i) = log10(yb(i))
      enddo
C Make new window and box.
      if (mode.gt.0) then
        ymin = +1.e+30
        ymax = -1.e+30
        do i=1,nb
          if (yb(i).gt.-29.9) then
            if (yb(i).lt.ymin) ymin = yb(i)
            if (yb(i).gt.ymax) ymax = yb(i)
          endif
        enddo 
        yr = ymax-ymin
        ymin = ymin - (yr*0.1)
        ymax = ymax + (yr*0.1)
        if (abs(uymin).gt.0.) ymin = uymin
        if (abs(uymax).gt.0.) ymax = uymax
        call PGWINDOW(xmin,xmax,ymin,ymax)
        call PGBOX('BICNST',xtick,nxtick,'BLICNST',0.,0)
      endif
C Draw bins.
      call PGBBUF
      if (mode.eq.2) call PGSLS(4)
      call PGBIN(nb,xb,yb,.true.)
      if (mode.eq.2) call PGSLS(1)
      call PGEBUF
      return 
      end

C------------------------------------------------------------------
C Histogram of data points (logarithmic in number).
C Mode: 1=normal, 0=do not make new box or window.
C Mode: 2=normal, but do plot lines in dotted lines.
C
      subroutine PG_BinHistLog(y,n,xmin,xmax,xinc,xtick,nxtick,mode)
C
      implicit none
      integer*4 n,nb,i,nxtick,binnum
      real*4 y(*),xmin,xmax,xtick
      integer*4 max_nb,mode
      parameter(max_nb=9000)
      real*4 xinc,xb(max_nb),yb(max_nb),ymin,ymax
C Initialize.    
      nb = max(1,min(max_nb,nint((xmax-xmin)/xinc)+1))
      do i=1,nb  
        xb(i)= xmin + ( (xinc*float(i-1)) + (xinc/2.) )
        yb(i)= 0.
      enddo
C Count number in bins.
      do i=1,n   
        binnum = max(1,min(nb, nint(((y(i)-xmin)+(xinc/2.))/xinc) ))
        yb(binnum) = yb(binnum) + 1.
      enddo
C Logarithmic in yb.
      do i=1,nb
        if (yb(i).lt.1.) yb(i)=0.1
        yb(i) = log10(yb(i))
      enddo
C Make new window and box.
      if (mode.gt.0) then
        ymin = -0.1
        ymax = +0.1
        do i=1,nb
          if (yb(i).gt.ymax) ymax = yb(i)
        enddo
        ymax = ymax * 1.05
        call PGWINDOW(xmin,xmax,ymin,ymax)
        call PGBOX('BICNST',xtick,nxtick,'BLICNST',0.,0)
      endif       
C Draw bins.      
      call PGBBUF 
      if (mode.eq.2) call PGSLS(4)
      call PGBIN(nb,xb,yb,.true.)
      if (mode.eq.2) call PGSLS(1)
      call PGEBUF 
      return   
      end         
                 
C------------------------------------------------------------------
C Histogram of data points.
C Mode: 1=normal, 0=do not make new box or window.
C Mode: 2=normal, but do plot lines in dotted lines.
C
      subroutine PG_BinHist(y,n,xmin,xmax,xinc,xtick,nxtick,mode)
C
      implicit none
      integer*4 n,nb,i,nxtick,binnum
      real*4 y(*),xmin,xmax,xtick
      integer*4 max_nb,mode
      parameter(max_nb=9000)
      real*4 xinc,xb(max_nb),yb(max_nb),ymin,ymax
C Initialize.
      nb = max(1,min(max_nb,nint((xmax-xmin)/xinc)+1))
      do i=1,nb
        xb(i)= xmin + ( (xinc*float(i-1)) + (xinc/2.) )
        yb(i)= 0.
      enddo
C Count number in bins.
      do i=1,n
        binnum = max(1,min(nb, nint(((y(i)-xmin)+(xinc/2.))/xinc) ))
        yb(binnum) = yb(binnum) + 1.
      enddo
C Make new window and box.
      if (mode.gt.0) then
        ymin = 0.0 
        ymax = 1.0
        do i=1,nb
          if (yb(i).gt.ymax) ymax = yb(i)
        enddo 
        ymax = ymax * 1.05
        call PGWINDOW(xmin,xmax,ymin,ymax)
        call PGBOX('BICNST',xtick,nxtick,'BICNST',0.,0)
      endif
C Draw bins.
      call PGBBUF
      if (mode.eq.2) call PGSLS(4)
      call PGBIN(nb,xb,yb,.true.)
      if (mode.eq.2) call PGSLS(1)
      call PGEBUF
      return 
      end

C------------------------------------------------------------------
C Histogram of data points.  Fill in bin with hash marks.  Mode: 1=normal, 
C 0=do not make new box or window.  "spacing" is the spacing between lines in 
C the vertical direction in terms of a fraction of y range. "aspect_ratio" is 
C the aspect ratio ( y range / x range for 45 degree lines ).  Lines run 
C lower-left to upper-right (assuming xmin<xmax and ymin<ymax), unless 
C spacing<0 then lines run lower-right to upper-left.  Defaults: If spacing=0. 
C then spacing=0.02.  If aspect_ratio=0. then aspect_ratio=0.80 .
C
      subroutine PG_BinHistHash(y,n,xmin,xmax,xinc,xtick,nxtick,mode,
     .                            spacing,aspect_ratio)
C
      implicit none
      integer*4 n,nb,i,nxtick,binnum
      real*4 y(*),xmin,xmax,xtick,spacing,aspect_ratio
      integer*4 max_nb,mode
      parameter(max_nb=9000)
      real*4 xinc,xb(max_nb),yb(max_nb),ymin,ymax
C Initialize.
      nb = max(1,min(max_nb,nint((xmax-xmin)/xinc)+1))
      do i=1,nb
        xb(i)= xmin + ( (xinc*float(i-1)) + (xinc/2.) )
        yb(i)= 0.
      enddo
C Count number in bins.
      do i=1,n
        binnum = max(1,min(nb, nint(((y(i)-xmin)+(xinc/2.))/xinc) ))
        yb(binnum) = yb(binnum) + 1.
      enddo
C Make new window and box.
      if (mode.eq.1) then
        ymin = 0.0 
        ymax = 1.0
        do i=1,nb
          if (yb(i).gt.ymax) ymax = yb(i)
        enddo 
        ymax = ymax * 1.05
        call PGWINDOW(xmin,xmax,ymin,ymax)
        call PGBOX('BICNST',xtick,nxtick,'BICNST',0.,0)
      endif
C Draw bins.
      call PGBBUF
      call PG_BinHash(nb,xb,yb,spacing,aspect_ratio)
      call PGEBUF
      return 
      end


C------------------------------------------------------------------------
C Draws tick labels.
C
      subroutine PG_TLab(start,inc,number,foff,dp,r0,r1,side)
C
      implicit none

C     real*4      start      Value for first label in sequence.
C     real*4      inc        Increment for values.
C     integer*4   number     Number of values.
C     real*4      foff       Offset from side in fraction of the window
C                              in the horizontal (L,R) or vertical (T,B)
C                              direction.  If 0., foff = 0.013 (default).
C     integer*4   dp         Decimal places, (f20.2 for dp=2), if negative,
C                              then 1pe20.2 for dp=2 .
C     real*4      r0,r1      Linear transformation between labels and true
C                              coordinates: (label) = r0 + r1 * (coordinate),
C                              0.,0. implies 0.,1.  Note that start and inc
C                              refer to the true coordinates.
C     character*1 side       T(op), B(ottom), L(eft), or R(ight).

      real*4 start,inc,foff,r0,r1
      real*4 xmin,xmax,ymin,ymax,rx,ry,r,a,b,os
      integer*4 dp,number,i,d
      character c30*30, side*1

      a = r0
      b = r1  
      if ((a.eq.0.).and.(b.eq.0.)) b = 1.
      os= foff 
      if (os.eq.0.) os = 0.013 
      d = dp 
      d = min(22,max(-22,d)) 

      call PGQWIN(xmin,xmax,ymin,ymax)

      if (side.eq.'L') then
        rx = xmin - (xmax-xmin)*os
        do i=1,number
          ry = start + float(i-1)*inc
          r  = a + (b*ry)
          call PG_TLab_Write(c30,r,d)
          call PGPTXT(rx,ry,90.,0.5,c30) 
        enddo 

      elseif (side.eq.'R') then
        rx = xmax + (xmax-xmin)*os
        do i=1,number
          ry = start + float(i-1)*inc
          r  = a + (b*ry)
          call PG_TLab_Write(c30,r,d)
          call PGPTXT(rx,ry,90.,0.5,c30) 
        enddo 

      elseif (side.eq.'B') then
        ry = ymin - (ymax-ymin)*os
        do i=1,number
          rx = start + float(i-1)*inc
          r  = a + (b*rx)
          call PG_TLab_Write(c30,r,d)
          call PGPTXT(rx,ry,0.,0.5,c30) 
        enddo 

      elseif (side.eq.'T') then
        ry = ymax + (ymax-ymin)*os
        do i=1,number
          rx = start + float(i-1)*inc
          r  = a + (b*rx)
          call PG_TLab_Write(c30,r,d)
          call PGPTXT(rx,ry,0.,0.5,c30) 
        enddo 

      endif
 
      return
      end
      
C------------------------------------------------------------------   
      subroutine PG_TLab_Write(c30,r,d)
      implicit none
      character*30 c30,leftjust
      real*4 r
      integer*4 d,k
      if (d.eq.0) then
        write(c30,'(i30)') nint(r)
      elseif (d.gt.0) then 
        write(c30,'(f30.<d>)') r
      else
        k = abs(d) 
        write(c30,'(1pe30.<k>)') r
      endif 
      c30 = leftjust(c30) 
      if (d.lt.0) then
        k = index(c30,'E')
        c30 = c30(1:k-1)//'x10\\u'//c30(k+1:24) 
      endif
      return
      end 


C-------------------------------------------------------------
C Draw color map of FITS image.
C
      subroutine PG_FITS_Color(file,zero,span,cmap) 
C
      implicit none
      character*(*) file,cmap      ! FITS file and color map (or 'DEFAULT')
      real*4 zero,span             ! zero and span (bg=zero,fg=zero+span)
C
      integer*4 idim,jdim,mp,inhead
      parameter(mp=250000)
      real*4 a(mp),fg,bg
      integer*4 ia(mp),i1,i2,j1,j2
      character*57600 header 
      logical ok,clip 
C
      call readfits('test.fits',a,mp,header,ok)
      if (.not.ok) then
        print *,'Error reading FITS file: ',file
        return
      endif  
      idim = inhead('NAXIS1',header)
      jdim = inhead('NAXIS2',header) 
      bg = zero
      fg = zero+span
      i1 = 1
      j1 = 1  
      i2 = idim
      j2 = jdim

      clip = .true.
      call PG_Color(a,ia,idim,jdim,i1,i2,j1,j2,fg,bg,cmap,clip)

      return
      end  


C--------------------------------------------------------------
C PG_Color -- draw color image map of 2D data array.  Similar to PGGRAY but
C does colors.  Set PGPLOT_PS_COLOR to get color PostScript.
C
      subroutine PG_Color(a,ia,idim,jdim,i1,i2,j1,j2,fg,bg,cmap,clip)
C
      implicit none 
C
      integer*4 idim,jdim         ! physical size of data array     (input)
      integer*4 i1,i2,j1,j2       ! inclusive region to draw        (input)
      real*4 a(idim,jdim)         ! data array                      (input)
      real*4 fg,bg                ! foreground and background level (input)
      integer*4 ia(idim,jdim)     ! work space   (neither input nor output)
      character*(*) cmap          ! file containing color map data  (input)
      logical clip                ! Clip values above fg?           (input)

      integer*4 i,j,jj,ierr,nb,ncolor
      parameter(ncolor=64) 
      real*4 r,g,b,xmin,xmax,ymin,ymax,rr,gg,bb

C The color map file should contain 256 lines each of which contain three
C integers: red green blue , each of these values should be between 0 and
C 255 .  If ncolor < 256, the values will be averaged inside bins of size
C 256 / ncolor.  Element 1,1 is in the upper-left.

C Set up a color palette using ncolor indices from 16 to 15+ncolor.
      call PGSCR(0, 0.0, 0.3, 0.2)
      if (cmap.eq.'DEFAULT') then 
        do i=1,ncolor
          r = float(i-1)/float(ncolor-1)*0.8 + 0.2
          g = max(0.0, 2.0*float(i-1-ncolor/2)/float(ncolor-1))
          b = 0.2 + 0.4*float(ncolor-i)/float(ncolor)
          call PGSCR(i+15, r, g, b)
        enddo
      else
        open(31,file=cmap,status='old',iostat=ierr)
        if (ierr.ne.0) then
          print *,'Error opening : ',cmap  
          return
        endif  
        do i=1,ncolor
          r=0.
          g=0.  
          b=0. 
          nb = 256/ncolor
          do j=1,nb
            read(31,*) rr,gg,bb
            r = r + rr 
            g = g + gg
            b = b + bb  
          enddo 
          r = ( r / float(nb) ) / 256.
          g = ( g / float(nb) ) / 256. 
          b = ( b / float(nb) ) / 256. 
          call PGSCR(i+15,r,g,b)
        enddo
        close(31)   
      endif  

C Load image of color values.
      if (clip) then 
        do i=i1,i2
          do j=j1,j2
            jj = j1 + j2 - j
            ia(i,j) = nint( (min(1.,max(0., (a(i,jj)-bg)/(fg-bg) )))
     .                   * float(ncolor-1) ) + 16
          enddo
        enddo
      else
        do i=i1,i2
          do j=j1,j2
            jj = j1 + j2 - j
            r =  (max(0.,(a(i,jj)-bg)/(fg-bg)))
            r = r - float(int(r)) 
            ia(i,j) = nint( r * float(ncolor-1) ) + 16
          enddo
        enddo
      endif  

C Set up coordinate system- make sure pixels are square.
      xmin = float(i1)
      xmax = float(i2)
      ymin = float(j1)
      ymax = float(j2)
      call PGWNAD(xmin,xmax,ymax,ymin)

C use pgpixl to plot the image.
      call PGPIXL(ia,idim,jdim,i1,i2,j1,j2,xmin,xmax,ymin,ymax)

      call PGSCI(1)

      return 
      end


C------------------------------------------------------------------
C Like PGBIN except that the histogram is filled with hashings.
C See PG_Hash for definitions of spacing and aspect_ratio.
C The bins are assumed to be centered on the points, i.e. center=.TRUE.
C as defined in PGBIN.
C
      subroutine PG_BinHash(n,x,y,spacing,aspect_ratio)

      implicit none
      integer*4 n,i
      real*4 x(*),y(*),spacing,aspect_ratio
      real*4 x1,x2,y1,y2,xmin,xmax,ymin,ymax,small

      call PGQWIN(xmin,xmax,ymin,ymax)
      small = abs(ymax-ymin)*1.e-5

C Make a hashed box for each point.
      x2 = (x(1)+x(2))/2.
      x1 = x(1) - (x2-x(1))
      y1 = ymin
      y2 = y(1)
      if (abs(y1-y2).gt.small) then
        call PG_Hash(x1,x2,y1,y2,spacing,aspect_ratio)
      endif
      do i=2,n-1
        x1 = (x(i)+x(i-1))/2.
        x2 = (x(i)+x(i+1))/2.
        y1 = ymin
        y2 = y(i)
        if (abs(y1-y2).gt.small) then
          call PG_Hash(x1,x2,y1,y2,spacing,aspect_ratio)
        endif
      enddo
      x1 = (x(n)+x(n-1))/2.
      x2 = x(n) + (x(n)-x1)
      y1 = ymin
      y2 = y(n)
      if (abs(y1-y2).gt.small) then
        call PG_Hash(x1,x2,y1,y2,spacing,aspect_ratio)
      endif

      return
      end


C------------------------------------------------------------------
C Fills a square defined by x1,x2,y1,y2 with hashing.
C "spacing" is the spacing between lines in the vertical direction in
C terms of a fraction of y range.
C "aspect_ratio" is the aspect ratio ( y range / x range for 45 degree lines ).
C Lines run lower-left to upper-right (assuming xmin<xmax and ymin<ymax),
C unless spacing<0 then lines run lower-right to upper-left.
C
C If spacing      = 0.0  then  spacing      = 0.02
C If aspect_ratio = 0.0  then  aspect_ratio = 0.80
C

      subroutine PG_Hash(x1,x2,y1,y2,spacing,aspect_ratio)

      implicit none
      real*4 x1,x2,y1,y2,spacing,aspect_ratio
      real*4 xa,ya,xb,yb,ysp,xmin,xmax,ymin,ymax,r,yptr
      real*4 yinc,ar,xh(2,500),yh(2,500),yzero
      integer*4 i,j,nh
      logical norm

      yinc = spacing
      ar   = aspect_ratio
      if (abs(yinc).lt.0.001) yinc= 0.02
      if (abs(ar).lt.0.001)   ar  = 0.80

      call PGQWIN(xmin,xmax,ymin,ymax)

      r  = abs(y1-y2)/(ymax-ymin)
      if ( r.lt.abs(yinc) ) then
        print *,'Error- yinc too large for for height of box.'
        return
      endif

      xa = min(x1,x2)
      xb = max(x1,x2)
      ya = min(y1,y2)
      yb = max(y1,y2)
      ysp= abs(yinc)
      if (yinc.lt.0.) then
        norm = .false.
      else
        norm = .true.
      endif

C Convert box corners into square coordinate frame.
      xa =    (xa-xmin)/(xmax-xmin)
      xb =    (xb-xmin)/(xmax-xmin)
      ya = ar*(ya-ymin)/(ymax-ymin)
      yb = ar*(yb-ymin)/(ymax-ymin)

      IF (norm) THEN      ! lower-right to upper-left

C Create xh(..) and yh(..) arrays.
      nh=0
C Choose yptr such that one line of the family of lines would go through
C the origin.  This ensures that families from separate calls are "linked".
      yptr = ya - (xb-xa)
      yzero= yptr - xa
      yptr = yptr - ( yzero - ysp*float(int(yzero/ysp)) )
      do while (yptr.lt.yb) 
        nh = nh + 1
        xh(1,nh)=xa
        yh(1,nh)=yptr
        xh(2,nh)=xb
        yh(2,nh)=yptr+(xb-xa)
        yptr    = yptr + ysp
        if ((yh(2,nh).lt.ya).or.(yh(1,nh).gt.yb)) nh=nh-1     ! outside box
      enddo
C Clip lines to fit inside box.
      do i=1,nh
        if (yh(2,i).gt.yb) then
          xh(2,i) = xa + (yb-yh(1,i))  
          yh(2,i) = yb
        endif
        if (yh(1,i).lt.ya) then
          xh(1,i) = xa + (ya-yh(1,i))
          yh(1,i) = ya
        endif
      enddo

      ELSE              ! lower-left to upper-right

C Create xh(..) and yh(..) arrays.
      nh=0

C Choose yptr such that one line of the family of lines would go through
C the origin.  This ensures that families from separate calls are "linked".
      yptr = ya - (xb-xa)
      yzero= yptr + xb
      yptr = yptr - ( yzero - ysp*float(int(yzero/ysp)) )

      do while (yptr.lt.yb) 
        nh = nh + 1
        xh(1,nh)=xb
        yh(1,nh)=yptr
        xh(2,nh)=xa
        yh(2,nh)=yptr+(xb-xa)
        yptr    = yptr + ysp
        if ((yh(2,nh).lt.ya).or.(yh(1,nh).gt.yb)) nh=nh-1     ! outside box
      enddo
C Clip lines to fit inside box.
      do i=1,nh
        if (yh(2,i).gt.yb) then
          xh(2,i) = xb - (yb-yh(1,i))  
          yh(2,i) = yb
        endif
        if (yh(1,i).lt.ya) then
          xh(1,i) = xb - (ya-yh(1,i))
          yh(1,i) = ya
        endif
      enddo

      ENDIF

C     print *,'nh=',nh
C     do i=1,nh
C       print *,xh(1,i),yh(1,i),xh(2,i),yh(2,i)
C     enddo

C Convert hash lines back to user coordinate frame.
      do j=1,2
        do i=1,nh
          xh(j,i) = ( xh(j,i)*(xmax-xmin)    ) + xmin
          yh(j,i) = ( yh(j,i)*(ymax-ymin)/ar ) + ymin
        enddo
      enddo

C Plot lines.
      call PGBBUF
      do i=1,nh
        call PGLINE(2,xh(1,i),yh(1,i))
      enddo
      call PGEBUF

      return
      end
      

C-------------------------------------------------------------------

      subroutine PG_FigCap(disp,coord,cch,num,s)

      implicit none
      real*4 disp,coord,cch,sch,d
      integer*4 num,i,lastchar,j,n,k,ii
      character*(*) s
      character*100 ss

      call PGQCH(sch)
      call PGSCH(cch)
      n=0
      i=1
      do while(i.lt.lastchar(s))
        j=i+num-1
C count number of back slashes.
        k=0
        do ii=i,j
          if (s(ii:ii).eq.'\\') k=k+1
        enddo
        j=j+(2*k)
        do while(s(j:j).ne.' ')
          j=j-1
        enddo
        ss = ' '
        ss = s(i:j)
        d = disp + float(n)*sch
        print '(a)',ss(1:lastchar(ss))
        call PGMTEXT('B',d,coord,0.,ss)
        n=n+1
        i=j+1
      enddo
      call PGSCH(sch)

      return
      end


C-------------------------------------------------------------------------
C This version plots the residuals.
C Check residuals from polynomial fitting.
C Input:
C   np     = number of points in data arrays.               
C   x,y,w  = x and y values and weight for each data point.
C   order  = polynomial order, can range from 0 to 19, however spurious
C            numerical roundoff errors and excessive computation times
C            can be expected for orders larger than 4th or 5th.
C   coeff  = order+1 polynomial coeffecients ( c(0), c(1), c(2), ... )
C Output: 
C   high   = high residual
C   wrms   = weighted root-mean-square
C
      subroutine PG_poly_fit_residuals(np,x,y,w,order,coeff,high,wrms)

      implicit none
      integer*4 np,order,i
      real*4 x(*),y(*),w(*),coeff(*)
      real*4 d(9000),xmin,ymin,xmax,ymax
      real*4 high,wrms,xx,yy,diff,cpoly4,wsum

      high=0.
      wrms=0.
      wsum=0.
      do i=1,np
        xx = x(i)
        yy = cpoly4(xx,order,coeff)
        diff = abs(yy - y(i))
        d(i) = yy - y(i)
        if (diff.gt.high) high=diff
        wrms = wrms + (w(i)*diff*diff)
        wsum = wsum + w(i)
      enddo
      wrms = sqrt(wrms/wsum)

      call PGBEGIN(0,'/XDISP',1,1)
      call PGASK(.false.)
      call PGPAGE
      xmin = x(1)
      xmax = x(np)
      ymin = -1.*high*1.05
      ymax =  1.*high*1.05
      call PGSLW(1) 
      call PGSCF(2) 
      call PGSCH(1.2)
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGBOX('BCNST',0.,0,'BCNST',0.,0)
      call PGPT(np,x,d,1)
      call PGMTXT('B',2.5,0.5,0.5,'Pixel')
      call PGMTXT('L',2.0,0.5,0.5,'Residuals (Angstroms)')
      call PGEND

      return
      end


C--------------------------------------------------------------------
C Marks a list of QSO absorption lines.
C
      subroutine PG_AbsLineList(npix,x,y,za,userfile,priority)
      implicit none
      integer*4 npix           ! number of pixels in spectrum
      real*4 x(*),y(*)         ! spectrum
      real*4 za                ! absorption redshift
      character*(*) userfile   ! QSO abs. line data file, use ' ' for default
      integer*4 priority       ! minimum strength of lines: 1, 2, or 3

      real*4 xmin,xmax,ymin,ymax
      real*4 r1,r2,r3,wave(90),osc(90)
      integer*4 i1,i2,i3,i4,i5,i6,i7,n,i,lc
      character c8*8,c1*1,ion(90)*8,linefile*80,c40*40
      logical more

      call PGQWIN(xmin,xmax,ymin,ymax)
      xmin = xmin / (1.+za)
      xmax = xmax / (1.+za)

      if (userfile.eq.' ') then
        linefile = '/usr9/tom/har/qso_absorption'
      else
        linefile = userfile
      endif

      print '(2a)','QAL database: ',linefile(1:lc(linefile))

C Read lines.
      n=0
      open(37,file=linefile,status='old',iostat=i)
      if (i.ne.0) goto 9
      more=.true.
      do while(more)
        read(37,'(f11.4,i2,2x,a8,f8.6,i2,i4,i3,i2,f6.2,1x,i1,i1,a1)',
     .         err=8) r1,i1,c8,r2,i2,i3,i4,i5,r3,i6,i7,c1
        if (r1.lt.19000.) then
          if ((r1.gt.xmin).and.(r1.lt.xmax).and.(i6.le.priority)) then
            n=n+1 
            wave(n)= r1
            ion(n) = c8
            osc(n) = r2
          endif
        else
          more=.false.
        endif
      enddo
      close(37)
      wave(n+1)=0.
      ion(n+1) =' '
      osc(n+1) =0.

      print *,'Number of lines = ',n
      print *,'Select and draw lines. Select out doublets.'

C Select and draw lines. Select out doublets.
      do while(i.le.n)
        if ( (abs(wave(i)-wave(i+1)).lt.11.).and.
     .                    (ion(i).eq.ion(i+1)) ) then

          write(c40,'(a,a,i4,a,i4)') 
     .            ion(i)(1:lc(ion(i))),' \\gl\\gl\\fn',
     .            int(wave(i)),',',int(wave(i+1))

          call PG_AbsLine(npix,x,y,wave(i),wave(i+1),za,c40,1.0)
          i=i+1
        else
          c40=' '
          write(c40,'(a,a,i4)') ion(i)(1:lc(ion(i))),
     .           ' \\gl\\fn',int(wave(i))
          if (abs(wave(i)-wave(i-1)).lt.5.) then
            call PG_AbsLine(npix,x,y,wave(i),wave(i),za,c40,1.5)
          else
            call PG_AbsLine(npix,x,y,wave(i),wave(i),za,c40,1.0)
          endif
        endif
        i=i+1
      enddo

      return
8     close(37)
9     print *,'Error in xp_absorption_lines.'
      return
      end


C--------------------------------------------------------------------
C Marks the position of an absorption line.
C Used by PG_AbsLineList.
C
      subroutine PG_AbsLine(n,x,y,rw1,rw2,z,s,raise)
      implicit none
      real*4 x(*),y(*)
      real*4 w1,w2,rw1,rw2,z,high,raise
      character*(*) s
      integer*4 i,neari,n,i1,i2
      real*4 xmin,xmax,ymin,ymax,y1,y2,y3
      real*4 xx(4),yy(4),sep,ht

C Current window.
      call PGQWIN(xmin,xmax,ymin,ymax)
      sep = (ymax-ymin)*0.03
      ht  = (ymax-ymin)*0.10

C Convert rest wavelengths to observed.
      w1 = (1.+z)*rw1
      w2 = (1.+z)*rw2

C Find high point.
      i = neari(w1,n,x)
      i1= max(1,i-9)
      i = neari(w2,n,x)
      i2= min(n,i+9)
      high=0.
      do i=i1,i2
        if (y(i).gt.high) high=y(i)
      enddo
      
C Choose y bounds.
      y1 = high + sep
      y2 = min((y1+ymax)/2.,high+ht)
      y3 = y2 + sep

C Set arrays.
      xx(1)=w1
      xx(2)=w1
      xx(3)=w2
      xx(4)=w2
      yy(1)=y1*raise
      yy(2)=y2*raise
      yy(3)=y2*raise
      yy(4)=y1*raise

C Plot line and write string.
      call PGLINE(4,xx,yy)
      call PGPTXT(w2,y3*raise,90.,0.,s)

      return
      end

C----------------------------------------------------------------------
C Initiator for PG_SimpPlot.
C Use this to force certain plot ranges, use file = PS file or 'pgdisp',
C and then call 'go' and 'stop' with your plot data.
      subroutine PG_SimpPlot_Init(xmin,xmax,ymin,ymax,file)
      implicit none
      real*4 xmin,xmax,ymin,ymax
      real*4 x(2),y(2)
      character*(*) file
      x(1) = xmin
      x(2) = xmax
      y(1) = ymin
      y(2) = ymax
      call PG_SimpPlot(2,x,y,0,file)
      return
      end

C----------------------------------------------------------------------
C Very basic, simple, self-contained plotter.
C symbol = 0 (connect-the-dots), 1 (dot), 2 (plus), 3 (star), 4 (circle),
C 5 (x), 6 (square), 7 (triangle), 8 (circle and plus), etc...
C Use symbol = -9 for histogram mode.
C To open plot do: file = PostScript filename (or 'pgdisp' for screen).
C                                             (or 'vt100' for VT-100 w/RG).
C To plot data   : file = 'go'
C To close data  : file = 'stop'
C To do all three of the above with screen output : file = ' '.
C Ranges determined on first pass.

      subroutine PG_SimpPlot(n,x,y,symbol,file)

      implicit none
      integer*4 n,i,lc,symbol
      real*4 x(n),y(n)
      real*4 xmin,xmax,ymin,ymax
      character*(*) file
      character*80 c80

      if ((file.ne.'go').and.(file.ne.'stop')) then
        if (file(1:6).eq.'pgdisp') then
          call PGBEGIN(0,'/XDISP',1,1)
        elseif (file(1:5).eq.'vt100') then
          call PGBEGIN(0,'/RETRO',1,1)
        else
          c80 = file(1:lc(file))//'/PS'
          call PGBEGIN(0,c80,1,1)
        endif
        call PGASK(.false.)
        call PGPAGE
        xmin=+1.e+30
        xmax=-1.e+30
        ymin=+1.e+30
        ymax=-1.e+30
        do i=1,n
          if (x(i).lt.xmin) xmin=x(i)
          if (x(i).gt.xmax) xmax=x(i)
          if (y(i).lt.ymin) ymin=y(i)
          if (y(i).gt.ymax) ymax=y(i)
        enddo
        xmin = xmin - (xmax-xmin)*0.01
        xmax = xmax + (xmax-xmin)*0.01
        ymin = ymin - (ymax-ymin)*0.01
        ymax = ymax + (ymax-ymin)*0.01
C Avoid overflow problems.
        if (abs(xmin-xmax).lt.1.e-20) then
          xmin = xmin - 1.e-20
          xmax = xmax + 1.e-20
        endif
        if (abs(ymin-ymax).lt.1.e-20) then
          ymin = ymin - 1.e-20
          ymax = ymax + 1.e-20
        endif
        call PGSLW(1)
        call PGSCF(1)
        call PGSCH(1.0)
        call PGWINDOW(xmin,xmax,ymin,ymax)
        call PGBOX('BCNST',0.,0,'BCNST',0.,0)
      endif

      if ((file.eq.'go').or.(file.eq.' ')) then
        if (symbol.eq.0) then
          call PGLINE(n,x,y)
        elseif (symbol.eq.-9) then
          call PGBIN(n,x,y,.true.)
        else
          call PGPT(n,x,y,symbol)
        endif
      endif

      if ((file.eq.'stop').or.(file.eq.' ')) then
        call PGMTXT('B',2.5,0.5,0.5,'X')
        call PGMTXT('L',2.0,0.5,0.5,'Y')
        call PGEND
      endif

      return
      end  

C----------------------------------------------------------------------
      subroutine PG_DrawBox(x1,x2,y1,y2)
      implicit none
      real*4 x1,x2,y1,y2,xx(5),yy(5)
      xx(1)=x1
      xx(2)=x2
      xx(3)=x2
      xx(4)=x1
      xx(5)=x1
      yy(1)=y1
      yy(2)=y1
      yy(3)=y2
      yy(4)=y2
      yy(5)=y1
      call PGLINE(5,xx,yy)
      return
      end

C------------------------------------------------------------------------
C Read a real*8 number (r8) from plot window.
C ch   : (real*4) character height (suggested: 1.2).
C side : (character*1) either 'B' or 'T' (suggested: 'T').
C disp : (real*4) displacement of string from side (suggested: 0.5).
C coord: (real*4) location of input along viewport (suggested: 0.4).
C
      subroutine PG_ReadPlotReal(r8,ch,side,disp,coord)
C
      implicit none
      real*8 r8,valread8
      real*4 ch,disp,coord,xx,rr,cx,cy
      character side*1,s*20,ss*20,key*1
      integer*4 lc,i,ikey
C Save old character height.
      call PGQCH(rr)
      call PGSCH(ch)
C Erase previous string.
      call PGSCI(0)
      xx=coord+0.05
      do i=1,lc(s)
        call PGMTXT(side,disp,xx,0.,s(i:i))
        xx=xx+0.017
      enddo
      call PGSCI(1)
C Get new string.
      xx=coord
      call PGMTXT(side,disp,xx,0.,'>>>')
      ikey=0
      xx=coord+0.05
      s=' '
      do while (ikey.ne.13)
        call PGCURS(cx,cy,key)
        ikey=ichar(key)
C Allow only numeric characters and <cr>.
        if ((ikey.eq.13).or.(ikey.eq.44).or.(ikey.eq.47)) goto 3
        if ((ikey.lt.43).and.(ikey.ne.13).and.(ikey.ne.8)) goto 3
        if ((ikey.gt.57).and.(ikey.ne.69).and.(ikey.ne.101)
     .                  .and.(ikey.ne.127)) goto 3
C Backspace.
        if ((xx.gt.coord+0.05).and.((ikey.eq.127).or.(ikey.eq.8))) then
          call PGSCI(0)
          xx=xx-0.017
          i = lc(s)
          call PGMTXT(side,disp,xx,0.,s(i:i))
          call PGSCI(1)
          ss= s
          s = ' '
          s = ss(1:i-1)
          goto 3
        endif
C Numeric character (not a carraige return).
        call PGMTXT(side,disp,xx,0.,key)
        s=s(1:lc(s))//key
        xx=xx+0.017
3       continue
      enddo
      r8 = valread8(s)
      call PGSCH(rr)
      return
      end

