C This file is maxpix.inc 

C :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C ::::::::  CHANGE THESE LINES TO REDUCE MEMORY USAGE :::::::
C :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

C For 1x2 binning set size to >= 2303 x 1024 ~= 2360000.
C For 1x1 binning set size to >= 2303 x 2048 ~= 4720000.
C
C "makee" must read in 2 full RAW HIRES images at once.
C
C Maximum number of pixels in full RAW HIRES image.
      integer*4 maxpix
C
C     parameter(maxpix=4800000)  ! 2190x2190  (Keck HIRES)
                                 !                 (4 x 4.7 = 19 MB, real*4)
                                 !                 (2 x 19  = 38 MB)
C
C     parameter(maxpix=9184000)  ! 4100x2240 (Keck ESI,HIRES2,VLT,Subaru)
                                 !                  (9 x 4 = 36 MB)
                                 !                  (2 x 36= 72 MB)
C
      parameter(maxpix=24008001) ! For heywood.caltech.edu use only.
C
C

C Maximum number of pixels in a sub-image (scratch for PSE()).
      integer*4 maxpixsub
      parameter(maxpixsub=maxpix * 0.27)

C Maximum number of columns in an image.
      integer*4 maxcol
      parameter(maxcol=maxpix/1024)

C Maximum number of orders in an image.
      integer*4 maxord
      parameter(maxord=50)

C :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

