/* cmath-util.h    Include file for using cmath-util.c routines. */

extern double gaussian_sig( double x );

extern double gaussian_hwhm( double x );

extern double gaussval( double x, double FWHM );

extern void xyz2radec(double x, double y, double z, double *ra, double *dec);


extern void radec2xyz(double ra, double dec, double *x, double *y, double *z);

extern void radec2ypzp(double ra, double dec, double RM[3][3],
                       double *yp, double *zp );

extern void radec2prime(double ra, double dec, double RM[3][3],
                        double *rap, double *decp );

extern void xyz2prime(double x, double y, double z, double RM[3][3],
                      double *xp, double *yp, double *zp);

extern void prime2xyz(double xp, double yp, double zp, double RMinv[3][3],
                      double *x, double *y, double *z);

extern void MatMul3D(double aa[3][3], double bb[3][3], double cc[3][3] );

extern double MatDet2D(double aa[2][2]);

extern double MatSubDet3D(int col, int row, double aa[3][3]);

extern double MatDet3D(double aa[3][3]);

extern double MatCof3D(int col, int row, double aa[3][3]);

extern void MatInv3D(double mm[3][3], double mminv[3][3] );

extern void MatInv2D(double mm[2][2], double mminv[2][2] );

extern void SetRotationMatrix( double alpha, double theta, RMtype RM[] );

extern void SetImageRotationMatrix( double PA, RMtype RM[] );

extern void SetRotMat2D( double Angle, double RotMat[2][2], double RotMatInv[2][2] );

extern double camotry( double **pp, double y[],
                       double psum[], int ndim,
                       double (*funk)(double []), int ihi, double fac );

extern void camoeba( double **pp, double y[], int ndim,
                     double ftol, double (*funk)(double []), int *nfunk );

extern void camoeba_gate( double (*funk)(double []),
                          int npar, double par[], double parsl[],
                          double ftol, double *resid, int *niter );
