/* phomax.h                         tab Oct99  */

#define TESTSPEC 0       /* If "1" force a flat, square prob. dist. spec. */

#define MAXSIDE 3600              /* Maximum pixels on a side for an image. */

#define MAXPIX (3600*3600)+1000   /* Maximum pixels in an image. */
                                  /* 3000: 35 Megabytes for a float array. */
                                  /* 4000: 63 Megabytes for a float array. */

#define PHOMAX 1000000           /* One million photons (47 MB with phofp). */

#define PHOMAXINTERVAL 100000    /* Maximum photons within each time interval.*/

#define PhRecMAX 9000            /* Maximum sim. photon records per read. */

#define EAMAX 200                /* Maximum effective area points. */

#define SLMAX 90100              /* Maximum number of sources (objects). */

#define DXSLMAX 90100            /* Maximum number of                     */
                                 /*   Direct-image eXtracted Source List. */

#define PDMAXSP 70   /* Maximum number of (Probability Distribution) spectra. */
#define PDMAXPT 500  /* Maximum number of points in spectra. */

#define MCMAX 70     /* Maximum number of points in Mag. Conversion. */

#define TSMAX   90     /* Maximum number of template spectra. */
#define TSMAXPT 9000   /* Maximum number of points in a template spectrum. */


/* Full precision photon (48 bytes each) */
struct fullphoton {
  double x;
  double y;
  double z;
  double w;
  double t;
  double i;
};
typedef struct fullphoton phofp;   /* PHOton Full Precision */


/* Integer short-precision photon (16 bytes each) */
struct shortphoton {
  short int x;
  short int y;
  short int z;
  short int w;
  long  int t;
  long  int i;
};
typedef struct shortphoton phosp;   /* PHOton Short Precision */

/* Expanded photon list (23May00) -- 32 bytes */
struct EXPHOstructure {
  double     t;        /* Time tag; sec.s since 0Z Jan 1, 1970 ("UNIX time") */
  short int  x,y;      /* Pho. detector pos.*32767(+/-1 full width)(old u,v) */
  unsigned char  val;  /* Scaled photon "value" * 32767 */
  unsigned char    q;  /* Pulse height */
  unsigned short int flags;
  long  int  X,Y,Z;     /* Photon j2k position * 2147483648 */
  short int  thetax;    /* */
  short int  thetay;    /* */
};
typedef struct EXPHOstructure EXPHOtype;


/* miller photon list-- 32 bytes */
struct millerphoton {
  unsigned short int  u,v;   /* Photon detector position. */
  unsigned short int  ctl;   /* Control bits. */
  unsigned char      qval;   /* Photon Q "value".  */
  unsigned char        tt;   /* Time-Tag: difference, units of 20 microsec. */
};
typedef struct millerphoton phoml;   /* PHOton Miller List */


/* Binary FITS Photon List... 48 bytes. Full simulation file. May 2000. */
struct BFPLstructure {
  double        t;            /* time in seconds */
  short int     x;            /* detector x */
  short int     y;            /* detector y */
  unsigned char val;          /* value? */
  unsigned char q;            /* q */ 
  unsigned short int flags;   /* flags? */
  long int      X;            /* unit sphere X */ 
  long int      Y;            /* unit sphere Y */ 
  long int      Z;            /* unit sphere Z */ 
  short int     thetax;       /* theta x */
  short int     thetay;       /* theta y */
  short int     xfp;          /* x focal plane */
  short int     yfp;          /* y focal plane */
  long int      source;       /* source ID */
  short int     tdelay;       /* time delay? */
  unsigned short int lambda;  /* wavelength in Angstroms */
  int           spare;        /* spare? */
};
typedef struct BFPLstructure BFPLtype;


/* Abridged Binary FITS Photon List... 12 bytes.  May 2000. */
struct ABFPLstructure {
  short int       X;            /* unit sphere X */ 
  short int       Y;            /* unit sphere Y */ 
  short int       Z;            /* unit sphere Z */ 
  unsigned short int lambda;    /* wavelength in Angstroms. */
  long int       source;        /* source ID */
};
typedef struct ABFPLstructure ABFPLtype;


/* Rotation Matrices. */
struct RMstructure {
  double mat[3][3];        /* Sphere: Normal RA,Dec to prime system. */
  double inv[3][3];        /* Sphere: Prime system to normal RA,Dec. */
  double img[2][2];        /* Image: Normal x,y to "fixed-PA" x,y.  */
  double imginv[2][2];     /* Image: "Fixed-PA" x,y to normal x,y.  */
};
typedef struct RMstructure RMtype;


/*  Image Orientation Parameters 
    ----------------------------
    The transformation from ra,dec to column and row on an image is
    defined by the following.
    (1) Construct 2D rotation matrix "RM.img" from imgPA:
             RM.img[0][0] =-1.*sin( degtorad(imgPA) );
             RM.img[1][0] =-1.*cos( degtorad(imgPA) );
             RM.img[0][1] =    cos( degtorad(imgPA) );
             RM.img[1][1] =-1.*sin( degtorad(imgPA) );
    (2) Use RAo and DECo to set up 3D rotation matrix "RM.mat".
    (3) Use RM.mat to rotate RAc,DECc to primed system: xcp,ycp,zcp).
    (4) Use RM.mat to rotate ra,dec to primed system: xp,yp,zp.
    (5) Translate (offset): x = yp - ycp  and y = zp - zcp.
    (6) Use "RM.img" to rotate x and y.
    (7) The column and row are then:
           column = (x - xmin) / imgscale
           row    = (y - ymin) / imgscale
*/
struct IOPstructure {
  double RAo     ;  /* RA origin gnomonic projection, 3D rotat. matrix. */
  double DECo    ;  /* DEC origin gnomonic projection, 3D rotat. matrix.*/
  double RAc     ;  /* RA center of image in degrees. */
  double DECc    ;  /* DEC center of image in degrees. */
  double xmin    ;  /* x minimum: decimal deg. of 0,0 corner of image. */
  double ymin    ;  /* y minimum: decimal deg. of 0,0 corner of image. */
  double imgscale;  /* degrees per pixel on image. */
  double imgPA   ;  /* image Position Angle in degrees. */
};
typedef struct IOPstructure IOPtype;


/* Galex spectral Extraction Parameters. */
struct EPstructure {
  double grsdisp  ;  /* Grism dispersion, arcsecs per Angstr.(e.g. 0.24).*/
  double imgscale ;  /* Image scale in degrees per pixel. */
  double objwidth ;  /* Object width for extraction in arcseconds. */
  double bckwidth ;  /* Background width for extraction in arcseconds. */
  double objbcksep;  /* Separation betw. object and background in arcsecs*/
  double wavec    ;  /* Undeviated wavelength in Angstroms. */
  double wave1    ;  /* Blue end of wavelength range to extract. */
  double wave2    ;  /* Red end of wavelength range to extract. */
};
typedef struct EPstructure EPtype;


/* Galex Effective Areas */
struct EAstructure {
  int    num;      /* Number of points. */
  double wave;     /* wavelength in Angstroms.                 */
  double fuvim;    /* far ultraviolet imaging.                 */
  double nuvim;    /* near ultraviolet imaging.                */
  double fuvsp2;   /* far ultraviolet spectroscopy 2nd order.  */
  double fuvsp3;   /* far ultraviolet spectroscopy 3rd order.  */
  double nuvsp1;   /* near ultraviolet spectroscopy 1st order. */
  double nuvsp2;   /* near ultraviolet spectroscopy 2nd order. */
};
typedef struct EAstructure EAtype;


/* Galex Simulation Source List. */
struct SLstructure {
  int    num;     /* Number of sources (SL[0].num ).                */
  int    id;      /* Identifier for source.                         */
  int    it;      /* Temperature indices (-1 = not used)            */
  double ra;      /* Star RA in decimal degrees.                    */
  double dec;     /* Star DEC in decimal degrees.                   */
  double fuv;     /* Flux of source in photons per second.          */
  double nuv;     /* Flux of source in photons per second.          */
  double Teff;    /* Temperature of source.                         */
  int    Type;    /* Type of source.                                */
  double V;       /* V magnitude of source.                         */
  double Mag2200; /* Magnitude at 2200 Angstroms of source.         */
  double x;       /* X position of star within grid (degrees).      */
  double y;       /* Y position of star within grid (degrees).      */
  double f;       /* Flux in the current band of choice.            */
  double ycp;     /* Y-center primed frame (see simu_loadsubimage). */
  double zcp;     /* Z-center primed frame (see simu_loadsubimage). */
  double dist;    /* Magnitude distribution array.                  */
};
typedef struct SLstructure SLtype;

/* Galex Direct-image Extracted Source List. */
struct DXSLstructure {
  int    num;                /* Number of sources.                     */
  double ra[DXSLMAX] ;       /* Source RA in decimal degrees.          */
  double dec[DXSLMAX];       /* Star DEC in decimal degrees.           */
  double fuvim[DXSLMAX];     /* FUV flux of source in photons/second.  */
  double nuvim[DXSLMAX];     /* NUV flux of source in photons/second.  */
  double fuvimerr[DXSLMAX];  /* FUV flux error of source.              */
  double nuvimerr[DXSLMAX];  /* NUV flux error of source.              */
};
typedef struct DXSLstructure DXSLtype;

/* Galex spectral Probability Distributions for dwarf stars. */
struct PDstructure {
  int    numsp;          /* Number of spectra ( PD[0].numsp ) .       */
  int    numpt;          /* Number of points in each spectrum.        */
  double Teff;           /* Effective temperature for Dwarf star.     */
  double wave[PDMAXPT];  /* Wavelengths.                              */
  double prob[PDMAXPT];  /* Cumulative Probabilities. (BOTH ORDERS.)  */
  double ppsa[PDMAXPT];  /* Photons per second per Angstrom for V=0.  */
  double ppstot;         /* Total photons per second for V=0.         */
};
typedef struct PDstructure PDtype;


/* Galex Magnitude Conversion 5550 to 2200. */
struct MCstructure {
  int    num;       /* Number of points ( MC[0].num ). */
  double Teff;      /* Effective temperature. */
  double conv;      /* Conversion factor:  Mag2200 = Vmag + factor  */
};
typedef struct MCstructure MCtype;


/* Galex spectral templates. */
struct TSstructure {
  int numts;               /* Number of template spectra ( TS[0].numts ).     */
  double Teff;             /* Effective temperature.                          */
  double relflux[TSMAXPT]; /* Relative flux in template spectrum at a given   */
                           /* column number in the spectral image strip box.  */
};
typedef struct TSstructure TStype;


/* ACT Catalog.  8+8+(4*4) = 32 bytes */
struct ACTstructure {
  double ra;        /* Right Ascension in decimal degrees (J2000). */
  double dec;       /* Declination in decimal degrees (J2000). */
  float raPM;       /* RA proper motion in sec of time/year. */
  float decPM;      /* DEC proper motion in arcsec/year. */
  float VTmag;      /* V magnitude from Tycho. */
  float BmV;        /* Johnson B-V from Tycho. */
};
typedef struct ACTstructure ACTtype;


/* Offset computations (see phogrism()). */
struct OSstructure {
  float background; /* background level of current image. */
  int nbs;          /* number of bright stars. */
  double bsx[99];   /* bright star column. */
  double bsy[99];   /* bright star row. */
  double bsf[99];   /* bright star flux. */
  double col1;      /* First column offset for sum-box. */
  double col2;      /* Second column offset for sum-box. */
  double row1;      /* First row offset for sum-box. */
  double row2;      /* Second row offset for sum-box. */
  double Xoff;      /* Column offset. */
  double Yoff;      /* Row offset. */
  double Theta;     /* Angle offset. */
  double range;     /* Range for search. */
  double inc;       /* Search increment. */
};
typedef struct OSstructure OStype;
