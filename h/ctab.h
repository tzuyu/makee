/* ctab.h    General, standard, universal inclusions for all coding. */

/* Standard includes. */

#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* A few basic definition functions. */

#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp;

#define MIN(x,y) ((x) < (y) ? (x) : (y))

#define MAX(x,y) ((x) > (y) ? (x) : (y))

#define ABS(x) ((x) < 0 ? (-1*x) : (x))

/* 
  WARNING: with the ABS,MAX,MIN functions enclose your input inside
           extra parenthesis... e.g.  ABS((x - i))  NOT  ABS(x - i)... 
*/

/* Other functions that need to be defined external. */

extern double drand48(void);

/*
extern void srand48(int seed);
*/

extern double hypot(double x, double y);

