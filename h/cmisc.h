/* . . . . . . BEGIN . . . . . . . . . . . . */

extern FILE *fopen_read( char file[] );

extern FILE *fopen_write( char file[] );

extern void cGetMakeeHome( char wrd[] );

extern void hexbinarystring( char cc, char ss[] );

extern void show_binary16( unsigned short int ii, char bs[] );

extern void show_binary8( unsigned char ii, char bs[] );

extern void WriteString9(double value, char ss[]);

extern void WriteString7(double value, char ss[]);

extern void cDoBin4(int bin, int *np, float xx[], int mode);

extern int FileExist( char filename[] );

extern int timeget();

extern int ssny();

extern void timegetstring(char uline[]);

extern void getsystime( char uline[] );

extern void bswp2(char bb[]);

extern void bswp4(char bb[]);

extern void bswp8(char bb[]);

extern double SwapDouble( double value );

extern float SwapFloat( float value );

extern unsigned short int ByteSwap2us( unsigned short int ii );

extern short int ByteSwap2( short int ii );

extern long int ByteSwap4( long int ii );

extern double FindRADiff(double ra1, double ra2);

extern double FindRAMean(double ra1, double ra2);

extern double cpolyval(int nc, double co[], double xv);

extern int cneari(double xx, int narr, double arr[]);

extern int cneari4(float xx, int narr, float arr[]);

extern int cneari_bs(double xx, int narr, double arr[]);

extern int cneari_bs4(float xx, int narr, float arr[]);

extern void cpauseit();

extern void substrprt(char a[], int a1, int a2 );

extern void substrcpy( char a[], int a1, int a2, char b[], int b1 );

extern void substrcpy_terminate( char a[], int a1, int a2, char b[], int b1 );

extern int substrcmp(char a[], int a1, int a2, char b[], int b1 );

extern int substrcmp0(char a[], char b[], int n );

extern int cint( double rr );

extern int cnint( double rr );

extern void cupper_case(char a[]);

extern void clower_case(char a[]);

extern int cindex(char a[], char b[] );

extern int cindex2(char a[], char b[], int ptr );

extern int clc(char a[]);

extern int cfc(char a[]);

extern void cargcopy(char *argv[], int argc, int *narg, char arg[100][100]);

extern int cfindarg(char arg[100][100], int *narg, char parm[], char d, char wrd[] );

extern double cangsep(double ra1, double dec1, double ra2, double dec2);

extern double yinterp(int n, double x[], double y[], double xpt);

extern double TypeToTemp(char st[2]);

extern int numeral(char cc);

extern int numeric(char cc);

extern void cIPAChead(char ipachead[], char name[], int *p1, int *p2);

extern double cvalread(char userline[], int up1, int up2);

extern void FindNumericWord(char line[], int pp, int *sow, int *eow, int *ok);

extern double GetLineValue(char line[], int nn);

extern double GetLineValueQuiet(char line[], int nn);

extern double GLVQ(char line[], int nn);

extern double GLV(char line[], int nn);

extern int fgetline(char userline[], FILE *fu);

extern float readrealstdin();

extern double StarTempBmV(double userbv);

extern double BlackBodyAT(double a, double T, double x);

extern double IntegrateBlackBody(double lam1, double lam2, double Teff,
                          double flam0, double lam0);

extern double SolveBlackBodyA(double T, double xp[2], double yp[2]);

extern double gammln(double xx);

extern void gser(double a, double x, double *gamser, double *gln);

extern void gcf(double a, double x, double *gammcf, double *gln);

extern double gammq(double a, double x);

extern double chisq_test(double nu, double chisq);

extern int Hits(double ACR);

extern double degtorad(double theta);

extern double radtodeg(double theta);

extern void GaussianPosition(double fwhm, double *radius, double *theta);

extern double doub_rand(double a, double b);

extern void cqcksrt(int nn, float arr[] );

extern void cqcksrt8(int nn, double arr[] );

extern void cqcksrtkey4(float arr[], int nkey, int key[] );

extern void cqcksrtkey8(double arr[], int nkey, int key[] );

extern float cfind_median(int nn, float arr[] );

extern float cfind_median_scratch(int nn, float arr[], float scr[] );

extern double cfind_median8(int nn, double arr[] );

extern double cfind_median_scratch8(int nn, double arr[], double scr[] );

extern float cfind_median_key(float arr[], int nkey, int key[] );

extern void cASCII_to_PS(char afile[], char psfile[],
                 int ptsz, int left, int bold, int land,int head);

extern double GetPixelPosition( float xx[], int nn, double xval );

extern void FluxAverage(float ff[], float ee[], double pix1, double pix2,
                 int sumit, double *fval, double *eval );

extern double FluxSum( double ff[], double pix1, double pix2 );

extern void SmoothArray8( int radius, int num, double arr[], double scr[] );

/* . . . . . . END . . . . . . . . . . . . . */
