/* fstuff.h                                  tab  sep00 */

extern void f2cwriteradec_ ( double *ra, double *dec, char ss[], int *mode );

extern void  f2carccentroid_ ( float a[], int *sc, int *ec, int *sr, int *er,
                               int *row, double *pix, double *cent, 
                               double *peak, double *fwhm, int *iok );

extern void f2cautoidcorrelate_ ( int *na, float a[], float b[], 
                                int *cos1, int *cos2, float *xpeak, 
                                float *peak, float *fwhm, int *iok);

extern void f2cpolyfitgj_ ( int *npt, double x[], double y[], double wt[],
                            int *order, double ucoef[], double *xoff, 
                            int *forcexoff, int *ok);

extern void f2cpolyfitgjresiduals_ ( int *np, double x8[], double y8[], 
                                     double w8[], int *order, double coef[],
                                     double *xoff, double *high, 
                                     double *wrms, double *wppd );

extern void f2cpolyfitglls_ ( int *npt, double x[], double y[], double wt[],
                              int *order, double ucoef[], int *ok);

extern void f2cpolyfitgllsresiduals_ ( int *np, double x8[], double y8[], 
                                     double w8[], int *order, double coef[],
                                     double *high, double *wrms );

extern void f2cbiw_ ( float aa[], float bb[], char header[], char maskfile[],
                      int *maskmode, int *sc, int *ec, int *sr, int *er,
                      int *ok );

