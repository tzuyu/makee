/* cpg-util.h                        tab  Jan 2000 */

extern void cpg_PlotGaussian(float xoffset, float area, float sigma, int npp);

extern void cpg_BinHist(float y[], int n, float xmin, float xmax, float xinc, 
                        float xtick, int nxtick, int mode, float *fwhm);

extern void cpg_HLine(float x1, float x2, float y);

extern void cpg_DrawBox(float x1, float x2, float y1, float y2);

extern void cpg_drawline(float x1, float x2, float y1, float y2);

extern void cpg_pt1(float x1, float y1, int sym);

