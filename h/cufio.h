/* . . . . . . BEGIN . . . . . . . . . . . . */
extern int cgetpos(char card[], char header[], int headersize );

extern void cunheadset(char card[], char header[], int headersize);

extern void remove_FITS_card(char card[], char header[], int headersize);

extern int headroom(char card[], char header[], int headersize, int *ptr);

extern void SetupCardLocation(char card[], int cardloc, char header[],
                       int headersize );

extern void cfheadset(char card[], double value, char header[], int headersize );

extern void cfheadset2(char card[], double value, char header[], int headersize, 
                       char comment[], int mode );

extern void cinheadset(char card[], int value, char header[], int headersize );

extern void cinheadset2(char card[], int value, char header[], int headersize, char comment[] );

extern void ccheadset(char card[], char value[], char header[], int headersize );

extern void clheadset(char card[], char value, char header[], int headersize );

extern void clheadset2(char card[], char value, char header[], int headersize, char comment[] );

extern void cchead(char card[], char header[], int headersize, char value[] );

extern int clhead(char card[], char header[], int headersize);

extern double cfhead(char card[], char header[], int headersize );

extern int cinhead(char card[], char header[], int headersize );

extern int cnoteheadset( char card[], char usernote[], char header[], int headersize );

extern void cnotehead( char card[], char header[], int headersize, char note[] );

extern int crw_fitshead(char header[], int mode, FILE *ffu, int headersize );

extern int crw_fitsshort(float fd[], int mode, FILE *ffu, int npt,
                    double bzero, double bscale );

extern int crw_fitsfloat(float fd[], int mode, FILE *ffu, int npt );

extern void cAddFitsExt(char filename[]);

extern void cAddAscExt(char filename[]);

extern void cAddXyExt(char filename[]);

extern int cGetDimensions(char header[], int headersize,
                   int *sc, int *ec, int *sr, int *er, int *nc, int *nr);

extern void cMakeBasicHeader(char header[], int headersize, int numcol, int numrow );

extern int creadfitsheaderrawhires2(char fitsfile[], char header[], int headersize );

extern int creadfitsheader(char fitsfile[], char header[], int headersize );

extern int creadfits(char fitsfile[], float fd[], int maxpix,
                                  char header[], int headersize );

extern int cwritefits(char fitsfile[], float fd[], char header[], int headersize );

extern double cgetx(double ael, char header[], int headersize);

extern void ccshead(char card[], char shead[], int sheadsize, char value[] );

extern double cfshead(char card[], char shead[], int sheadsize );

extern void cfsheadset(char card[], double value, char shead[],int sheadsize, int mode);

extern int cwritexye(char xyfile[], int nn, float xx[], float yy[], float ee[], int errmode, char header[], int headersize );

extern int creadspfits(char file[], int *nn, float xx[], float aa[],
                int maxpix, char header[], int headersize );

extern int creadspxy(char xyfile[], int *nn, float xx[], float aa[],
                int maxpix, char header[], int headersize );

extern int creadspecfile( char file[], int maxpt, int binfac, int *np, float scr[],
              float xx[], float yy[], float zz[], float uu[], float vv[],
              int *nrow, char header[], int headersize,
              int yyrow, int zzrow, int uurow, int vvrow );

extern void chires2_readwrite( char rawfile[], int mode, int verbose, int quiet );

/* . . . . . . END . . . . . . . . . . . . . */
