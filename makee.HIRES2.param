C This file is makee.HIRES2.param  (MAuna Kea Echelle Extraction PARAMeters)
C
C          HIRES2 Version
C
C There should be no spaces before or after the "=" in the uncommented 
C statements below.
C
C:::::::::::::::::: Echelle Extraction PARAMETERS :::::::::::::::::::
C

C EETO_PolyOrder is the polynomial order to use when fitting the echelle
C orders.  Must be between 0 and 7.  This should be as small as reasonable.
C (Suggested value: 4).
C
   EETO_PolyOrder=4

C EEFO_ObjectFraction is how much of the total flux to enclose when determining
C the object width.  Must be between 0.1 and 0.99. (Suggested value: 0.98.)
C
   EEFO_ObjectFraction=0.98

C EEFO_PolyOffset is the polynomial order to use when fitting the object
C centroid offset (relative to bright star trace) as a function of order
C number.  Must be between 0 and 7.  This should be as small as reasonable
C (Suggested value: 3.)
C
   EEFO_PolyOffset=3

C EEFB_Compensate is subtracted from the edges of the background to adjust for
C the fact the initial estimate for the background limits (i.e. edge of the
C slit) tends to be a little wide. (It uses a 70% drop off criterion.)
C Increasing this number will give a smaller background region, which may be
C necessary if the flat does not line up with the object exposure.
C Decreasing this number will give a larger background.  You could make this
C number negative.  Must be between -9. and 9.  The units are real pixels.
C (Suggested value: 1.0.)
C
   EEFB_Compensate=1.0

C EEFB_OverlapCompensate is the same as EEFB_Compensate except that it is
C applied when the slit widths overlap.  Must be between -9. and 9.
C (Suggested value: 1.5.)
C
   EEFB_OverlapCompensate=1.5

C::::::::: SKY REGION :::::::::::::

C Define spacing (in pixels) between object and beginning of sky region.  Must
C be between -9. and 9. (Suggested value: 1.5.)
C If value is -8, then a binning dependant value is computed by the program.
C
   PSE_SkySpacer=1.0

C Define minimum amount of sky (in pixels) to use on either side of the object.
C This will cut into the profile of the object if necessary.  Must be between
C 0. and 9. (Suggested value: 0.5)
C
   PSE_SkyMin=0.5

C Define polynomial order to use when fitting sky background.  If -1 then the
C program will decide the order.  Currently, this will be 1 unless the ratio of
C sky areas on either side of the profile is more than 2:1, in which case the
C order will be 0.  Must be between -1 and 5. (Suggested value: -1 .)
C
   PSE_SkyOrder=-1

C Define whether to use only a single column when fitting the sky in each
C column.  If this false then up to two adjacent columns (with reduced
C weights) may be used when fitting the sky polynomial in a given column.
C This should (in principal) give a slightly better background subtraction.
C (Valid values are T or F, suggested value: F ).
C
   PSE_SingleColumnSkyFit=F

C Define rejection limit for radiation events in the sky region during the
C first CR rejection pass.  The units are sigmas, and the limit is compared
C against the computed value:
C        (deviation from expected value) / (one sigma error estimate).  
C Must be between 0.1 and 99. (Suggested value: 5.0 (5.5 for old HIRES))
C
   PSE_SkyRejLimOne=5.0

C Define rejection limit for satellite radiation events in the sky region
C which occurs during the first CR rejection pass.  These are corrupted
C pixels which are adjacent to large events.  A large event is defined as
C one which exceeds PSE_SkyRejLimOne by a factor of two.  Must be between
C 0.1 and 99. (Suggested value: 2.0.)
C
   PSE_SkyRejLimSat=2.0

C Define rejection limit for radiation events in the sky region during the
C second pixel rejection pass.  This pass is sensitive to both high pixels
C (CRs) and low pixels (holes).  Must be between 0.1 and 0.99.
C (Suggested value: 5.0 (4.0 for old HIRES))
C
   PSE_SkyRejLimTwo=5.0

C::::::::: OBJECT REGION :::::::::::::

C Define row increment to use when determining profile.  This is the separation
C between polynomials in real pixel (row) space.  Should be less than 0.5 .
C A smaller number improves the profile model, but increases CPU time.
C This is used both by PSE_ObjMask and PSE_ObjFlux.  Must be between 0.02 and
C 2.0. (Suggested value: 0.2.)
C
   PSEO_ProfInc=0.2

C Define +/- length around profile fit (row-direction) pixel position which
C determines which pixels are used during a profile polynomial fit.
C This is usually about one half of PSEO_ProfInc . Increasing this number means
C more pixels will be used in any given profile polynomial fit, but the pixel
C centers will be further from the position of the polynomial row offset. In
C general, one might want a larger value for faint objects and a smaller value
C for bright objects. Must be between 0.02 and 2.0. (Suggested value: 0.1.)
C
   PSEO_ProfIncSlop=0.1

C Define number of columns to use to find medians in the dispersion direction
C which are used to create sub-profiles used in the total profile fitting,
C during PSE_ObjMask.  This only affects the profile fitting during the CR
C rejection routines.  A larger number decreases the effect of cosmic rays.
C A smaller number yields a more accurate profile.  Must be between 5 and 50.
C (Suggested value: 12 .)
C
   PSEOM_ProfMdnBox=12

C Define polynomial order used when fitting to object profile during object
C cosmic ray masking (PSE_ObjMask) AND during flux calculations.  If an order
C of -1 is given, then the program guesses at the best order (from 0 to 5) to
C use based on the S/N of the data.  In this case, the order will (probably)
C change for different parts of the profile.  Must be between -1 and 7.
C (Suggested value: -1 .)
C
   PSE_ProOrder=-1

C Define rejection threshold for "deviant pixels" when fitting the polynomial
C during profile fitting within PSE_ObjMask.  This is not the same as the
C CR rejection threshold.  Must be between 0.1 and 99.  This is only used
C during the CR rejection routines.  (Suggested value: 4.0.)
C
   PSEOM_ProRejLim=4.0

C Define rejection threshold when rejecting CRs in the object region.  Must be
C between 0.1 and 99. (Suggested value: 5.0.)
C
   PSEOM_ObjRejLim=5.0

C Define rejection threshold which defines big CRs which need to be searched
C for affected adjacent pixels.  This is usually on the order of two or three
C times larger than ObjRejLim.  Must be between 0.1 and 99.
C (Suggested value: twice PSEOM_ObjRejLim ).
C
   PSEOM_ObjRejLimBig=10.0

C Define rejection threshold when rejecting satellite (adjacent pixel) CRs in
C the object region.  Adjacent pixels are inspected when the the "central"
C pixel exceeds 1 sigma by a factor of PSEOM_ObjRejLimBig.  Must be between
C 0.1 and 99.  (Suggested value: 2.2 ).
C
   PSEOM_ObjRejLimSat=2.2

C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
