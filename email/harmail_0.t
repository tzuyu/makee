
Due to the increasing number of groups (4) now using the HIRES Automated
Reduction software, I thought it would be a good idea to start an email
distribution list to announce changes, improvements, and bugs in the
programs.  This address will be available via an email alias (or "email
exploder") on cass157: HAR@cass157.ucsd.edu .

If you wish to added to the distribution list just reply to this email
message (no message body required).

Anyone with comments are welcome to use the distribution email address to
communicate with other users.  Although the list of addresses will not
appear in the email header, the list is not intended to be confidential, and
will be given to anyone upon request.

The HIRES reduction software will soon be available via anonymous ftp from
cass157 and should be relatively self-contained and easy to install.

Please forward this message to any interested persons.


-Tom Barlow
