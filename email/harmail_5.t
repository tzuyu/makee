                                               December 18, 1996
Hello Keck HIRES Data Reducers:

There is a new version of the HIRES Automated Reduction software (har.tar)
available via anonymous ftp from cass157.ucsd.edu under the hires
subdirectory.

There are no major bug fixes, but several auxilary programs have been
improved and expanded.  A "one-step" version of the extraction program is
included called "mkee".  This version is intended for use during an
observing run and is available at Keck.  "mkee" includes routines keckcards,
keckproc, and ee.

Some other changes have been made such as the inclusion of programs which
can be used in flux calibration and in scaling orders in order to create
large 1-dimensional spectra by adding together exposures of the same object.
See 0.har.user for a brief description.

Please note that the automatic wavelength scale calibration program "autoid"
will currently only work with the old "red" cross disperser in first order.
A new database of calibrated arc lamps will need to be created for the new
"UV" cross disperser.

-Tom Barlow
