
To HIRES data reducers:

The latest version of the HIRES Automated Reduction software is ready for
distribution.  The software is available by anonymous ftp from
cass157.ucsd.edu.  Go to the subdirectory "hires" and copy the har.tar.Z
(har == HIRES Automated Reductions).  This will untar into a
./har subdirectory.  Read the file 0.README first.

This version includes processing routines which can go from raw (as written
on the mountain) HIRES images to extracted spectra with a single command.  I
have added fixed-pattern bias image creation (if short darks are present)
within the program "keckproc".  keckproc will also group objects, stars, and
flats and create a script file containing all the necessary "ee" commands.

          .......

BUG NOTE:

In cases where the profile is shifted to one side of the slit (as defined by
the flat field) the default sky fit and subtraction can produce undesirable
results.  The symptom is a periodic pattern with a period roughly equal to
inverse slope of the spectrum, i.e. about 100 columns.  (In general, this
pattern can be caused by other problems and is usually a sign that something
has gone horribly wrong.)  To fix this, the default mode is now to set the
sky fit polynomial order to 0 (usually it is 1) if the ratio of sky regions
on either side of the profile is greater than 2.

          .......

The latest changes/additions:

(1) e-/DN and readout noise calculations, bias and flat field image creation.
    "keckproc" now writes the file "ee.script" which sets up all the ee
    commands for all observations.

(2) All ee (fixed) parameters are now in the file "keck.parm" and can be
    changed without relinking the program.

(3) Created "logsum" which summarizes the output from ee. 

(4) Created "atps" program which creates 132+ char/line PostScript output.

(5) Added default mode where bad regions are interpolated in "flux" spectra,
    and are set to -1.0 in variance spectra.

(6) Several other minor changes.

          .......

Probable future improvements:

(1) Handling different binning configurations (1x1, 4x1, etc.).

(2) Rejection of cosmic rays during flat field summation.

(3) Include sky fit error and possibly flat field error in the variance
    calculation for the flux.

(4) Routines for wavelength and flux calibration.

         -Tom
