
Important note to HIRES reducers:

The current HIRES CCD has a fixed pattern bias with a level of approximately
8 to 9 DN (counts, 1x2 binning) which increases to 15+ DN at the lowest column
numbers.  This offset can be seen in short dark frames and in long and short
exposures of objects (looking between the orders).  There is also a
column-to-column pattern with an amplitude of about 0.2 DN.  There is no
significant row-to-row pattern (to less than 0.1 DN).

It is important to subtract off this fixed pattern bias from all your images
(this is done automatically within "keckproc").  If you do not subtract off
this bias, the flat field division will be incorrect.  This will be most
significant in the wings of the PSF and in the sky-only regions.  The result
will be slightly more "noise" in your spectrum, caused by pixel-to-pixel
sensitivity fluctuations.

I would recommend that you take at least 10 short darks (1 second, shutter
closed, dewar cover on, hatch closed, lamps off).  These can be taken during
the day with an auto-repeat command.  You should take at least several so
that the cosmic rays can be easily excluded.

Comparison of bias patterns from short darks taken in May 1994 and September
1994 shows that the pattern has not changed significantly over this time.  I
have made available two bias images (for May94 and Sep94) in the cass157
anonymous ftp directory, for people who may not have appropriate short darks
from their run.

The dark current in addition to the fixed pattern bias is only about 1 DN
every hour (1x2 binning), so can probably be neglected.

-Tom
