From tab Fri Apr 21 15:36:48 1995
From: tab (Tom Barlow)
Date: Fri, 21 Apr 95 15:32:19 PDT
Subject: New HIRES Reduction Software


Hello Keck HIRES Data Reducers:

There is a new version of the HIRES Automated Reduction software
(har.tar) available via anonymous ftp from cass157.ucsd.edu under
the hires subdirectory.  Many additions and changes have been made,
but the most significant is the inclusion of automated wavelength
scale calibration programs.

Read the 0.README and 0.getting_started files once you have untared
the har.tar file.  You may also want to look at 0.modifications for
major and minor changes to the code.

There is now a version of ee on the mountain called mkee (Mauna Kea
Echelle Extraction.)  This program takes raw images and produces
wavelength calibrated extracted spectra, but skips steps like
calculating the gain and creating a new bias image.  Hopefully, it
should be available to all observers by simply typing mkee on
lanikai.keck.hawaii.edu .

-Tom Barlow

