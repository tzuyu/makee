Hello Keck HIRES Data Reducers:

A minor bug has been discovered in the HIRES Automated Reduction program
"ee".  The variance calculation during the optimal extraction was causing a
slight downward shift in the flux level.  This was due to the poisson error
being calculated for each pixel individually, which resulted in lower valued
pixels being overweighted.  The effect is noticeable in black absorption
lines where the S/N is low.  The bottom of the lines appear to be
significantly less than zero.

There is a new version of the HIRES Automated Reduction software (har.tar)
available via anonymous ftp from cass157.ucsd.edu under the hires
subdirectory.  In the new version the variance is now calculated using the
profile model, rather than using each pixel individually.  This does not have
a big effect on the resulting spectrum, but may be significant for some
applications.

Some other changes have been made such as the inclusion of (undocumented)
programs involved in flux calibration and scaling orders in order to create
large 1-dimensional spectra by adding together exposures of the same object.
See 0.har.user for a brief description.

-Tom Barlow
