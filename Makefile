#---------------------------------------------------------#
#   MAKEE Makefile                      tab 1997 -- 2000  #
#---------------------------------------------------------#
#
include Options

all:
	cd $(HERE)/UA ; make
	cd $(HERE)/Misc ; make
	cd $(HERE)/EE ; make

sun:
	/bin/rm -f Options
	/bin/ln -s OptionsSun Options
	/bin/rm -f SystemChoice.h
	/bin/ln -s SystemSun.h SystemChoice.h
	/bin/rm -f UA/cmisc.o
	cd $(HERE)/UA ; make
	cd $(HERE)/Misc ; make
	cd $(HERE)/EE ; make

linux:
	/bin/rm -f Options
	/bin/ln -s OptionsLinux Options
	/bin/rm -f SystemChoice.h
	/bin/ln -s SystemLinux.h SystemChoice.h
	/bin/rm -f UA/cmisc.o
	cd $(HERE)/UA ; make
	cd $(HERE)/Misc ; make
	cd $(HERE)/EE ; make

macosx:
	/bin/rm -f Options
	/bin/ln -s OptionsMacOSX Options
	/bin/rm -f SystemChoice.h
	/bin/ln -s SystemLinux.h SystemChoice.h
	/bin/rm -f UA/cmisc.o
	cd $(HERE)/UA ; make
	cd $(HERE)/Misc ; make
	cd $(HERE)/EE ; make

clean:
	cd $(HERE)/UA ; make clean
	cd $(HERE)/Misc ; make clean
	cd $(HERE)/EE ; make clean

