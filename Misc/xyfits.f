C xyfits.f   x-y file to FITS spectrum creator  tab Jun92,Aug93,Mar94,Nov94
C
      implicit none
      integer*4 narg,i,npix,lastchar,mp,order,lc,getpos
      parameter(mp=500000)
      real*8 y(mp),x(mp),p(mp),w(mp),e(mp)
      real*8 pixoff,waveoff,coeff(0:6),rms,high,r1,r2,r3
      real*4 valread,a(mp)
      character*80 arg(9),xyfile,ffile,wrd,efile,hfile
      character header*11520,c64*64,c64b*64
      logical findarg,ok,lfl,ef,hf,MKS,plotit,ps
      
C Intialize.
      do i=0,6
        coeff(i) = 0.d0
      enddo
      header=' '
      c64=' '

C Get command line parameters.
      call arguments(arg,narg,9)
      order=1
      efile=' '
      if (findarg(arg,narg,'ord=',':',wrd,i)) order = nint(valread(wrd))
      ef  = findarg(arg,narg,'ef=',':',wrd,i)
      if (ef) efile = wrd
      hf  = findarg(arg,narg,'hf=',':',wrd,i)
      if (hf) hfile = wrd
      lfl = findarg(arg,narg,'-lfl',':',wrd,i)
      MKS = findarg(arg,narg,'-MKS',':',wrd,i)
      plotit = findarg(arg,narg,'-plot',':',wrd,i)
      ps     = findarg(arg,narg,'-ps',':',wrd,i)

C Help.
      if (narg.ne.2) then
        print *,'Syntax: xyfits (x-y pair file) (FITS file) [options]'
        print *,
     .  '    Converts a file containing wavelength,flux pairs into a'
        print *,
     .  '    a FITS spectrum file.  One x-y pair per line, do NOT put'
        print *,'    the number of pixels on the first line.'
        print *,' Options:'
        print *,
     .  '  ord=n       : specifies the order (1-6) of the polynomial'
        print *,
     .  '                  wavelength scale, default=1 (linear scale).'
        print *,'  -lfl        : Convert to lamba*f(lambda).'
        print *,
     .  '  ef=filename : Assumes third column are flux errors, writes'
        print *,
     .  '                  out the error spectrum to "filename".'
        print *,
     .  '  hf=filename : Takes initial header cards from this file,'
        print *,'                  one card per line.'
        print *,'  -MKS        : Put wavelengths in meters.'
        print *,'  -plot       : Plot residuals.'
        print *,
     .  '  -ps         : Plot residuals in PostScript file "junk.ps".'
        call exit(0)
      endif
  
C Check polynomial order.
      if ((order.lt.1).or.(order.gt.6)) then
        print *,'ERROR- Order must be between 1 and 6, inclusive.'
        call exit(1)
      endif
      if (order.eq.1) print *,'Using linear wavelength scale.'  

C Get filenames, add ".fits" to FITS file is no extension specified.
      xyfile=arg(1)
      ffile =arg(2)
      if (index(ffile,'.').eq.0) then
        ffile=ffile(1:lastchar(ffile))//'.fits'
      endif
      if ((ef).and.(index(efile,'.').eq.0)) then
        efile=efile(1:lastchar(efile))//'.fits'
      endif

C Open and read xy pair file.
      npix=0
      open(1,file=xyfile,status='old',iostat=i)
      if (i.ne.0) goto 902
2     continue
      if (.not.ef) then
        read(1,*,end=5,err=902) r1,r2
        npix = npix + 1
      else
        read(1,*,end=5,err=902) r1,r2,r3
        npix = npix + 1
        e(npix)= r3
      endif
      x(npix)= r1
      y(npix)= r2 
      if (order.eq.1) then
        p(npix)= dfloat(npix)        ! starting column 1 for linear scales
      else
        p(npix)= dfloat(npix) - 1.   ! starting column 0 for polynomial scales
      endif
      w(npix)= 1.d0
      goto 2

5     continue
      close(1)

C Convert to lambda*f(lambda).
      if (lfl) then
        do i=1,npix
          y(i) = y(i)*x(i)
        enddo
      endif

C Convert Angstroms to meters.
      if (MKS) then
        do i=1,npix
          x(i) = x(i) / 1.d+10
        enddo
      endif

C Determine central pixel offset.
      if (order.eq.1) then
        pixoff = 0.d0
      else
        pixoff = dfloat(npix/2)
      endif

C Apply pixel offset.
      do i=1,npix
        p(i) = p(i) - pixoff
      enddo

      print *,p(1),p(npix)

C Determine forward polynomial wavelength scale.
      call poly_fit_glls(npix,p,x,w,order,coeff,ok) 
      if (.not.ok) goto 905
      call poly_fit_glls_residuals(npix,p,x,w,order,coeff,high,rms) 

C Plot residuals.
C     if (plotit) then
C       if (ps) then
C         call PGBEGIN(0,'junk.ps/PS',1,1)
C       else
C         call PGBEGIN(0,'/XDISP',1,1)
C       endif
C       call PGWINDOW(sngl(p(1)),sngl(p(npix)),
C    .                sngl(-1.*abs(high)),sngl(abs(high)))
C       call PGBOX('BNCST',0.,0,'BCSNT',0.,0)
C       do i=1,npix
C         xx(i)=sngl(p(i))
C         yy(i)=sngl( x(i) - polyval(order+1,coeff,p(i)) )
C       enddo
C       call PGPT(npix,xx,yy,-1)
C       call PG_HLine(sngl(p(1)),sngl(p(npix)),0.)
C       call PGSCH(1.5)
C       call PGMTEXT('B',1.9,0.5,0.5,'Pixels')
C       call PGMTEXT('L',1.5,0.5,0.5,'Residuals in Angstroms')
C       call PGEND
C     endif

C Echo results to user.
      print '(a,i3)','Order =',order
      c64b=' '
      write(c64b,'(a,f8.4,a,f8.4)')
     . 'Wavelength scale fit: high residual=',min(999.,max(-99.,high)),
     . '  RMS=',min(999.,max(-99.,rms))
      print '(a)',c64b

C Create FITS header.
      if (hf) then
        i=0
        open(1,file=hfile,status='old')
22      read(1,'(a)',end=55) wrd
        header(i+1:i+80) = wrd
        i=i+80
        goto 22 
55      close(1)
        call nice_header(header)
      else
        call make_basic_header(header,0,npix,0,1)
      endif
      if (getpos('OBJECT',header).eq.-1) then
        call cheadset('OBJECT',xyfile,header)
      endif
      call cheadset('XYFILE',xyfile,header)
      call inheadset('BITPIX',-32,header)
      call cheadset('DATATYPE','REAL*4',header)
      call unfit('CTYPE2',header)
      if (order.eq.1) then     ! make header and set cards for a LINEAR scale.
        call cheadset('CTYPE1','LAMBDA',header)
        call fheadset('CRVAL1',coeff(0)+coeff(1),header)
        call fheadset('CDELT1',coeff(1),header)
        call cheadset('FITERROR',c64b,header)
      else
        call fheadset('CRVAL1',0.d0,header)
        call fheadset('CDELT1',1.d0,header)
        call cheadset('CTYPE1','POLY_LAMBDA',header)
        call inheadset('LAMORD',order+1,header)
        write(c64,'(4(1pe15.7,1x))') pixoff,coeff(0),coeff(1),coeff(2)
        call cheadset('LPOLY0',c64,header)
        write(c64,'(4(1pe15.7,1x))') 
     .     coeff(3),coeff(4),coeff(5),coeff(6)
        call cheadset('LPOLY1',c64,header)
        call cheadset('FITERROR',c64b,header)
C Determine inverse polynomial wavelength scale.
        waveoff = coeff(0)
        do i=1,npix
          p(i) = p(i) + pixoff
          x(i) = x(i) - waveoff
        enddo
        do i=1,6
          coeff(i) = 0.d0
        enddo
        call poly_fit_glls(npix,x,p,w,order,coeff,ok) 
        if (.not.ok) goto 905
        call poly_fit_glls_residuals(npix,x,p,w,order,coeff,high,rms)
        c64b=' '
        write(c64b,'(a,f8.4,a,f8.4)')
     .   'Inverse polynomial: high residual=',min(999.,max(-99.,high)),
     .   '  RMS=',min(999.,max(-99.,rms))
        print '(a)',c64b
        write(c64,'(4(1pe15.7,1x))') waveoff,coeff(0),coeff(1),coeff(2)
        call cheadset('IPOLY0',c64,header)
        write(c64,'(4(1pe15.7,1x))')
     .   coeff(3),coeff(4),coeff(5),coeff(6)
        call cheadset('IPOLY1',c64,header)
        call cheadset('INVFIT',c64b,header)
      endif

C Write out FITS file.
      print '(a,a)','      Writing FITS file : ',ffile(1:lc(ffile))
      do i=1,npix
        a(i) = sngl(y(i))
      enddo
      call writefits(ffile,a,header,ok)
      if (.not.ok) then
        print *,'Error writing FITS file.'
        call exit(1)
      endif

C Write out FITS error file.
      if (ef) then
        print '(a,a)','Writing FITS error file : ',efile(1:lc(efile))
        do i=1,npix
          a(i)=sngl(e(i))
        enddo
        call writefits(efile,a,header,ok)
        if (.not.ok) then
          print *,'Error writing FITS error file.'
          call exit(1)
        endif
      endif

      stop
902   continue
      print *,'Error opening or reading : ',xyfile(1:lc(xyfile))
      stop
905   continue
      print *,'Error fitting polynomial.'
      stop
      end
