/* DNtoErg.c      tab Mar01 */

#include "ctab.h"
#include "cmisc.h"

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Convert DN to ergs/sec/cm^2/Ang ...
*/
int main(int argc, char *argv[])
{
/**/
char arg[900][100];
char infile[100];
char outfile[100];
char line[100];
char wrd[100];
/**/
const double c = 2.997925e+18 ;  /*  Ang/sec      */
const double h = 6.626176e-27 ;  /*  ergs*sec     */
double hc = h*c;
const double area = 500. * 500. * 3.141592654;   /* Area of 10 meter mirror */
/**/
double xx,yy,expos,disp,eperdn;
/**/
FILE *infu;
FILE *outfu;
/**/
int narg;
/**/

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Option */
expos = 1.;
if (cfindarg(arg,&narg,"expos=",':',wrd)==1) expos = GetLineValue(wrd,1);
disp = 1.;
if (cfindarg(arg,&narg,"disp=",':',wrd)==1) disp = GetLineValue(wrd,1);
eperdn=0.5;
if (cfindarg(arg,&narg,"eperdn=",':',wrd)==1) eperdn = GetLineValue(wrd,1);

/* Syntax. */
if ( narg != 2 ) {
  printf("Syntax: DNtoErg  (input .xy file)  (output .xy file)\n");
  printf("                    [ eperdn= ]  ( expos= )  ( disp= )\n");
  printf("\n");
  printf("  Convert Digital Number spectrum to ergs/sec/cm^2/Angstrom .\n");
  printf("  This assumes a 10 meter diameter mirror.\n");
  printf("  As detected on the CCD, not at the telescope aperture.\n");
  printf("  Divide by total throughput curve to get true ergs/s/cm^2/Ang.\n");
  printf("\n");
  printf("Options: eperdn= : electrons per digital number (def: 0.5 (ESI))\n");
  printf("         expos=  : exposure time in seconds (def: 1.0).\n");
  printf("         disp=   : dispersion in Ang/pixel (def: 1.0).\n");
  printf("\n");
  exit(0);
}

/* Files. */
strcpy(infile,arg[1]);
strcpy(outfile,arg[2]);

/* Echo */
printf("NOTE: exposure time is %f\n",expos);
printf("NOTE: dispersion in Angstroms per pixel is %f\n",disp);
printf("NOTE: electrons per digital number is %f\n",eperdn);

/* Open and read. */
infu = fopen(infile,"r");
outfu= fopen(outfile,"w");
while ( fgetline(line,infu) == 1 ) {
  xx = GLV(line,1);
  yy = GLV(line,2);
/* Convert. */
  yy = yy * eperdn * ( hc / xx ) / ( expos * area * disp );
/* Output. */
  fprintf(outfu,"%12.5e %12.5e\n",xx,yy);
}
fclose(infu);
fclose(outfu);

return(0);
}
