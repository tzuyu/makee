C qalplot.f
C
      implicit none
      integer*4 maxpt
      parameter(maxpt=201000)
      real*4 x(9000),y(9000),a(maxpt),yhi,ylo
      real*4 xmin,xmax,ymin,ymax,valread,mpc,dch
      real*4 uxmin,uxmax,uymin,uymax
      character*115200 header
      character*80 file,arg(99),wrd,linefile,wrds(9),psfile
      integer*4 naxis1,inhead,i,k,kk,skip,dcflag
      integer*4 narg,nh,nv,i1,i2,i3,i4,i5,i6,i7,n,maxl,nl,nlout,lc
      character c1*1,c8*8,c40*40
      integer*4 m
      parameter(m=900)
      character*8 ion(m)
      integer*4 key(m)
      real*4 arr(m),svpxmin,svpxmax,svpymin,svpymax
      real*8 owv(m),rwv(m),osc(m)
      real*8 fhead,crval1,cdelt1,za,swv,ewv,za1,r1,r2,r3,r8,getx
      real*8 w1,w2
      logical ok,vt100,findarg,all,norm,userx,usery,hist,land
      real*8 c,dv
      parameter(c=2.997925d+5)

      call arguments(arg,narg,99)
      all   = findarg(arg,narg,'-all',':',wrd,i)
      vt100 = findarg(arg,narg,'-vt100',':',wrd,i)
      land  = findarg(arg,narg,'-land',':',wrd,i)
      norm  = findarg(arg,narg,'-norm',':',wrd,i)
      hist  = findarg(arg,narg,'-hist',':',wrd,i)
      psfile= ' '
      if (findarg(arg,narg,'ps=',':',wrd,i)) psfile = wrd
      nh = 3
      if (findarg(arg,narg,'nh=',':',wrd,i)) nh = nint(valread(wrd))
      nv = 8
      if (findarg(arg,narg,'nv=',':',wrd,i)) nv = nint(valread(wrd))
      za = -9999.d0
      if (findarg(arg,narg,'z=',':',wrd,i)) za = dble(valread(wrd))
      dv = 200.d0
      if (findarg(arg,narg,'dv=',':',wrd,i)) dv = dble(valread(wrd))
      skip = 0
      if (findarg(arg,narg,'skip=',':',wrd,i)) skip = nint(valread(wrd))
      mpc = -1.e+31
      if (findarg(arg,narg,'mpc=',':',wrd,i)) mpc = valread(wrd)
      dch = 3.0
      if (findarg(arg,narg,'dch=',':',wrd,i)) dch = valread(wrd)
      svpxmin=0.10
      svpxmax=0.95
      svpymin=0.15
      svpymax=0.85
      if (findarg(arg,narg,'svp=',':',wrds,i)) then
        svpxmin = valread(wrds(1)) 
        svpxmax = valread(wrds(2)) 
        svpymin = valread(wrds(3)) 
        svpymax = valread(wrds(4)) 
      endif
      uxmin = 0.
      uxmax = 0.
      uymin = 0.
      uymax = 0.
      userx=.false.
      if (findarg(arg,narg,'ux=',':',wrds,i)) then
        uxmin = valread(wrds(1)) 
        uxmax = valread(wrds(2)) 
        userx = .true.
      endif
      usery=.false.
      if (findarg(arg,narg,'uy=',':',wrds,i)) then
        uymin = valread(wrds(1)) 
        uymax = valread(wrds(2)) 
        usery = .true.
      endif
      if ((narg.ne.2).or.(za.lt.-999.)) then
        print *,'Syntax:  qalplot (FITS file) (line file) z=redshift'
        print *,'            [dv=velocity]  [ps=(PostScript file)]'
        print *,'            [-land] [-vt100]  [-norm] [-hist]'
        print *,'            [nh=  nv= ]  [-all]  [skip= ]  [ mpc= ]'
        print *,'            [ dch= ]  [ svp=xmin:xmax:ymin:ymax ]'
        print *,'            [ux=xmin:xmax]   [uy=ymin:ymax]'
        print *,' Options:'
        print *,'     z=   : absorption redshift.'
        print *,'    dv=   : velocity radius.'
        print *,
     .  '  nh=,nv= : number of horizontal and vertical windows.'
        print *,'   -all   : plot all windows, not just first page.'
        print *,'   skip=n : skip first "n" windows.'
        print *,'   mpc=   : y minimum as percentage of high value.'
        print *,'   dch=   : default character height ( 3.0 ).'
        print *,
     .  '   -norm  : normalized intensity (draw line at y=1.0).'
        print *,'   -hist  : histogram mode.'
        print *,
     .  '   svp=   : PGSVP (set viewport in units of 0.0 to 1.0).'
        print *,'   ux=    : Set horizontal range all plots.'
        print *,'   uy=    : Set vertical for all plots.'
        print *,
     .'   ps=    : Write plot to PostScript file (instead of pgdisp).'
        print *,'   -land  : Use landscape for PostScript plot file.'
        print *,' '
        print *,
     . 'For "line file", copy $MAKEE_DIR/qso_absorption to your own'
        print *,
     . '  file.  Edit your file to include only the lines of interest.'
        print *,' '
        call exit(0)
      endif
      za1 = 1.d0 + za
      linefile = arg(2)
      file = arg(1)
      call AddFitsExt(file)
      call readfits(file,a,maxpt,header,ok)
      if (.not.ok) stop
      crval1 = fhead('CRVAL1',header)
      cdelt1 = fhead('CDELT1',header)
      naxis1 =inhead('NAXIS1',header)
      dcflag =max(0,inhead('DC-FLAG',header))
C
      if (all) then
        maxl=99999
      else
        maxl=nh*nv
      endif
C
      if (psfile.ne.' ') then
        print '(2a)','Writing: ',psfile(1:lc(psfile))
        if (land) then
          print *,'Landscape mode...'
          call PGBEGIN(0,psfile(1:lc(psfile))//'/PS',nh,nv)
        else
          print *,'Portrait mode...'
          call PGBEGIN(0,psfile(1:lc(psfile))//'/VPS',nh,nv)
        endif
      elseif (vt100) then
        call PGBEGIN(0,'/RETRO',nh,nv)
      else
        call PGBEGIN(0,'/XDISP',nh,nv)
      endif
      if ((psfile.eq.' ').and.(all)) then
        call PGASK(.true.)
      else
        call PGASK(.false.)
      endif
      call PGSLW(1) 
      call PGSCF(1) 
      call PGSCH(dch)

C Read line list.
C----+----1----+----2----+----3----+----4----+----5----+----6----+----7----+---
C12345678901--xx12345678----+---12----123--123456x1-1
C  1215.6701 4  H I     0.416200 4   1  1 0 12.00 10A
C  1548.2020 3  C IV    0.194000 3   1  6 3  8.65 10A
C  1550.7740 3  C IV    0.097000 4   1  6 3  8.65 10A
C  1854.7164 4  Al III  0.539000 3   1 13 2  6.49 10A
C----+----1----+----2----+----3----+----4----+----5----+----6----+----7----+---
C     swv = crval1
C     ewv = crval1 + ( dfloat(naxis1-1)*cdelt1 )
C Initialize then set starting and ending wavelength.
      swv = getx(-1.d0,header)
      swv = getx(1.d0,header)
      ewv = getx(dfloat(naxis1),header)
      nl = 0
      nlout = 0
      open(1,file=linefile,status='old')
1     continue
      read(1,'(f11.4,i2,2x,a8,f8.6,i2,i4,i3,i2,f6.2,
     .         1x,i1,i1,a1)',end=5,err=9)
     .         r1,i1,c8,r2,i2,i3,i4,i5,r3,i6,i7,c1
      r8 = r1*za1
      if ((r8.gt.swv).and.(r8.lt.ewv)) then
        nl=nl+1
        rwv(nl) = r1
        owv(nl) = r8
        osc(nl) = r2
        ion(nl) = c8
      else
        nlout=nlout+1
      endif
      goto 1
5     continue
      print *,'lines in, out, total =',nl,nlout,nl+nlout
      nl = min(nl,maxl)
      do i=1,nl
        key(i) = i
        arr(i) = sngl(rwv(i))
      enddo
      call qcksrtkey(nl,arr,key)

C Load arrays.
      do kk=1,nl
      if (kk.gt.skip) then
        k  = key(kk)
        w1 = owv(k) - ( owv(k) * dv / c )
        w2 = owv(k) + ( owv(k) * dv / c )
        if (dcflag.eq.1) then
          i1 = nint( ( ( log10(w1) - crval1 ) / cdelt1 ) + 1.d0 )
          i2 = nint( ( ( log10(w2) - crval1 ) / cdelt1 ) + 1.d0 )
        else
          i1 = nint((w1-crval1)/cdelt1) + 1
          i2 = nint((w2-crval1)/cdelt1) + 1
        endif
        n=0
        yhi = -1.e+30
        ylo = +1.e+30
        do i=i1,i2
          n=n+1
C         r8 = crval1 + (dfloat(i-1)*cdelt1)
          r8 = getx(dfloat(i),header)
          x(n) = sngl(c*(r8-owv(k))/owv(k))
          y(n) = a(i)
          if (y(n).gt.yhi) yhi=y(n)
          if (y(n).lt.ylo) ylo=y(n)
        enddo
        if (abs(yhi-ylo).lt.1.e-30) then
          yhi = yhi + 1.e-30
          yhi = yhi + abs(yhi)*0.001
        endif
        if (userx) then
          xmin=uxmin
          xmax=uxmax
        else
          xmin = x(1)
          xmax = x(n)
        endif
        if (usery) then
          ymin=uymin
          ymax=uymax
        else
          ymin = ylo - (yhi-ylo)*0.1
          ymax = yhi + (yhi-ylo)*0.1
        endif
        if (mpc.gt.-1.e+30) ymin = yhi*mpc 
        call PGPAGE
        call PGSVP(svpxmin,svpxmax,svpymin,svpymax)
        call PGWINDOW(xmin,xmax,ymin,ymax)
        call PGBOX('BCNST',0.,0,'BCNST',0.,0)
        if (hist) then
          call PGBIN(n,x,y,.true.)
        else
          call PGLINE(n,x,y)
        endif
        call PGSLS(4)
        call PG_Mark(0.,ymin,ymax)
        call PG_HLine(xmin,xmax,0.)
        if (norm) call PG_HLine(xmin,xmax,1.)
        call PGSLS(1)
        write(c40,'(a,f8.2,f6.3,f8.2)')
     .        ion(k)(1:lc(ion(k))),rwv(k),osc(k),owv(k)
        call PGMTXT('T',0.3,1.0,1.0,c40)
        if (mod(kk,nh*nv).eq.1) then
          write(c40,'(a,f7.5)') 'z=',za
          call PGMTXT('T',0.3,0.0,0.0,c40)
        endif
      endif
      enddo

      call PGEND
C
      stop
9     continue
      print *,'ERROR reading line list file.'
C
      stop
      end  

