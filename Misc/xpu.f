C xplot utilities...
C

C----------------------------------------------------------------------
C First find closest line in QAL LineList.
      integer*4 function nearLL(cx)
      implicit none
      real*4 cx
      include 'xplot.inc'
      integer*4 i,besti
      real*4 bestdev,dev
      bestdev = 9.e+30
      besti   = 1
      do i=1,ll_n
        dev = abs(cx-sngl(ll_cent(i)))
        if (dev.lt.bestdev) then
          besti = i
          bestdev = dev
        endif
      enddo
      nearLL = besti
      return
      end

C-------------------------------------------------------------------
C Auxilary options.

      subroutine xp_other_options(replot,ok)

      implicit none
      logical replot,ok
      include 'xplot.inc'
      integer*4 i,j,nearxi,ii
      real*4 cx,cy,rr
      character key*1
      real*8 r8,osc_out,gamma_out,wave_out
      character*8 ion_out

      replot= .false.
      ok    = .true.

1     continue
      print *,'Other options: C,W,R/r,S,T,E/e,A,B,^A,a,',
     .        'b,x,X,s,v,k,q;? for help.'

2     continue
      call PG_CURSOR(cx,cy,key)

      if ((key.eq.'?').or.(key.eq.'/')) then
        print *,'  C   = remove Continuum fit (set to zero.)'
        print *,'  R/r = enter filename/Read continuum sections file.'
        print *,'  L   = show the LAST set of Sections used.'
        print *,'  T   = aborT XPLOT without saving any modifications.'
        print *,'  E/e = set redshift/show Emission lines.'
        print *,
     .  '  A/a = set redshift/show Absorption lines (priority=1).'
        print *,
     .  '  B/b = set redshift/show Absorption lines (priority=1,2,3).'
        print *,'  <cr>= (^M) set multiple absorption redshifts.'
        print *,' ^A   = set redshift based on cursor position.'
        print *,'  x   = show atmospheric absorption lines.'
        print *,'  f   = Free fit of spectrum.'
        print *,'  m   = Median/rejection/average fit of spectrum.'
        print *,
     .  '  w   = Automatically find lines via equivalent widths.'
        print *,'  s   = set a section of continuum to a fixed value.'
        print *,'  g   = scale the data by a value.'
        print *,'  v   = show a voigt profile.'
        print *,
     .  '  k   = set and show velocity scale (km/s) (rest wavelength.)'
        print *,
     .  '  K   = set and show velocity scale (km/s) (observed wave.)'
        print *,
     .  ' ^K   = set and show velocity scale (km/s) (cursor position.)'
        print *,'  q   = quit: return to main menu.'
        goto 2

      elseif (key.eq.'q') then
        print *,'Returning to main menu.'
        return

      elseif ((key.eq.'k').or.(key.eq.char(11)).or.(key.eq.'K')) then
        if (key.eq.'k') then
          print '(2a)',
     .  'IN PLOT WINDOW:  Enter approximate rest wavelength.'
          print '(a) ', 
     .  '          (Enter 0 to turn off velocity scale.)'
          call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
          call xp_GrabLineData(r8,3,wave_out,osc_out,gamma_out,ion_out)
          print '(a,f9.3,f9.5)',ion_out,wave_out,osc_out
          r8 = wave_out * ( 1.d0 + dble(za) )
        elseif (key.eq.'K') then
          print '(2a)',
     .  'IN PLOT WINDOW:  Enter observed wavelength.'
          print '(a) ',
     .  '          (Enter 0 to turn off velocity scale.)'
          call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        else
          r8 = dble(cx)
        endif
        if (abs(r8).lt.1.d-30) then
          show_velocity = .false.
        else
          show_velocity = .true.
          lambda_zero_velocity = r8
        endif
        replot=.true.
        print *,'Returning to main menu.'
        return

      elseif (key.eq.'v') then
        call xp_InteractiveVoigt(cx)
        print *,'Returning to main menu.'
        return

      elseif (key.eq.'f') then
        call xp_FreeFit(replot)
        return

      elseif (key.eq.'m') then
        call xp_MedianFit(replot)
        return

      elseif (key.eq.'g') then
        print '(a)','Enter scaling value in plot window.'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        rr = sngl(r8)
        tabgscale = tabgscale * rr
        do ii=0,np
          y(ii) = y(ii) * rr
        enddo
        call ReBin()
        print '(a,f12.5,a,f12.5)',
     .        'NOTE: new: ',rr,' ... total tabgscale = ',tabgscale
        modified = .true.
        replot   = .true.
        return

      elseif (key.eq.'s') then
        call PG_CURSOR(cx,cy,key)
        i = nearxi(cx)
        call xp_mark_position(x(i),2)
        print *,'Choose second limit...'
        call PG_CURSOR(cx,cy,key)
        j = nearxi(cx)
        print '(a,$)',' Enter new value for continuum : '
        read(5,*,err=9) rr
        do ii=min(i,j),max(i,j)
          c(ii) = rr
        enddo
        modified = .true.
        replot=.true.
        return

      elseif (key.eq.'x') then
        call xp_ShowAtmAbsLines()
        return

      elseif (key.eq.'w') then
        call xp_FindQALs()
        replot=.false.
        return

      elseif (key.eq.'R') then
        call xp_read_sections(replot,.true.)
        return

      elseif (key.eq.'r') then
        call xp_read_sections(replot,.false.)
        return

      elseif (key.eq.'L') then
        do i=1,nsect
          call xp_sectline(i,15)
        enddo

      elseif (key.eq.'C') then
        do i=1,np
          c(i)=0.
        enddo
        contbuf= 0
        replot = .true.

      elseif ((key.eq.'E').or.(key.eq.'e')) then
        if (key.eq.'E') ze=0.
        call xp_emission_lines

      elseif (key.eq.char(13)) then    ! ctrl-M or <cr>
        print '(2a)','IN PLOT WINDOW: ',
     .               'Enter approximate QAL rest wavelength.'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        call xp_GrabLineData(r8,3,wave_out,osc_out,gamma_out,ion_out)
        print '(a,f9.3,f9.5)',ion_out,wave_out,osc_out
        print *,
     .  ' Mark positions of components with space bar, "q" to quit.'
13      continue
        call PG_CURSOR(cx,cy,key)
        if (key.ne.'q') then
          call xp_mark_position(cx,7)
          za_also_n = za_also_n + 1
          za_also(za_also_n) = sngl( dble(cx) / wave_out ) - 1.
          goto 13
        endif
        replot = .true.

      elseif (key.eq.char(1)) then    ! ctrl-A
        print '(2a)','IN PLOT WINDOW: ',
     .               'Enter approximate QAL rest wavelength.'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        call xp_GrabLineData(r8,3,wave_out,osc_out,gamma_out,ion_out)
        print '(a,f9.3,f9.5)',ion_out,wave_out,osc_out
        cy = sngl(wave_out)
        za = ( cx / cy ) - 1.0
        print '(a,f14.6,a,f9.3,a)',' Za =',za,'   (',cy,')'
        call xp_absorption_lines(1)
        abs_priority = 1
        if (za_also_n.gt.0) print *,' Deleting multiple Za systems...'
        za_also_n = 0

      elseif (key.eq.'A') then
        za=0.
        if (za_also_n.gt.0) print *,' Deleting multiple Za systems...'
        za_also_n = 0
        call xp_absorption_lines(1)
        abs_priority = 1
        if (abs(za+1.0).lt.1.e-30) abs_priority=0

      elseif (key.eq.'B') then
        za=0.
        if (za_also_n.gt.0) print *,' Deleting multiple Za systems...'
        za_also_n = 0
        call xp_absorption_lines(3)
        abs_priority = 3
        if (abs(za+1.0).lt.1.e-30) abs_priority=0

      elseif (key.eq.'a') then
        call xp_absorption_lines(1)
        abs_priority = 1
        if (abs(za+1.0).lt.1.e-30) abs_priority=0

      elseif (key.eq.'b') then
        call xp_absorption_lines(3)
        abs_priority = 3
        if (abs(za+1.0).lt.1.e-30) abs_priority=0

      elseif (key.eq.'T') then     ! abort, do not modify spectrum.
        if (modified) then
          print '(a)',  
     .  ' This will abort XPLOT and NOT save modifications,'
          print '(a,$)',
     .  ' you have made.  Do you wish to continue? (1/0) : '
          read(5,*,err=9) i
          if (i.eq.1) then
            print *,'Aborting XPLOT.'
            ok=.false.
            modified = .false.
            return
          else
            print *,'NOT aborting XPLOT.'
            ok=.true.
          endif
        else
          ok=.false.
          print *,'Aborting XPLOT.'
          return
        endif

      else
       print *,'Try again.'
       goto 1

      endif

      print *,'Returning to main menu.'
      return
9     print *,'Error in xp_other_options.'
      return
      end

C----------------------------------------------------------------------
C Interactively choose different parameters for Voigt profile.
C
      subroutine xp_InteractiveVoigt(obs_wave)
      implicit none
      real*4 obs_wave
      include 'xplot.inc'
      real*8 rest_wave,column_density,b_value
      real*8 gamma_value,f_value,redshift,r8
      real*8 osc_out,gamma_out,wave_out,fwhm
      real*4 cx,cy,x1,x2
      character key*1,ion_out*8
C
C Set bounds for voigt profile plot.
C We make this bigger than the current plot due to edge-smoothing effects.
      x1 = xmin - (abs(xmax-xmin)/10.)
      x2 = xmax + (abs(xmax-xmin)/10.)
C Ask rest wavelength and try to guess all the ion/line info from that.
      print '(a,$)',' Enter rest wavelength (Ang.) : '
      read(5,*,err=9) rest_wave
      call xp_GrabLineData(rest_wave,1,wave_out,osc_out,
     .                     gamma_out,ion_out)
C Ask user for other info if necessary.
      if (osc_out.gt.0.) rest_wave = wave_out
      if (osc_out.gt.0.)   print '(a,a   )',
     .  '         Ion name   =',ion_out
      if (osc_out.gt.0.)   print '(a,f9.3)',
     .  '    Rest wavelength =',rest_wave
      if (osc_out.gt.0.)   print '(a,f9.5)',
     .  'Oscillator strength =',osc_out
      if (gamma_out.gt.0.)print '(a,1pe9.2)',
     .  '        Gamma value =',gamma_out
C Set subroutine variables: redshift, gamma_value, and f_value.
      redshift = ( dble(obs_wave) / rest_wave ) - 1.d0
      print '(a,f9.4)',' Redshift=',redshift
      if (gamma_out.gt.0.) then
        gamma_value = gamma_out
      else
        print '(a,$)','            Enter gamma value : '
        read(5,*,err=9) gamma_value
      endif
      if (osc_out.gt.0.) then
        f_value = osc_out
      else
        print '(a,$)','                Enter f value : '
        read(5,*,err=9) f_value
      endif
C Set initial values.
      fwhm          = 0.d0
      column_density= 1.d+16
      b_value       =30.d0
C User can now key in parameters: column_density and b_value, 
1     continue
      print *,'Key options: (b)=set b value (km/s).'
      print *,'             (c)=set log of column density(log(cm^-2)).'
      print *,'             (d)=set column density (cm^-2).'
      print *,'             (f)=set FWHM for gaussian smoothing (LSF).'
      print *,'             (r)=redraw spectrum.'
      print *,'             (j)=make PS file: junk.ps .'
      print *,'             (q)=quit.'
      print *,'             (?)=help.'
      print *,' Obs.Wave     Z    RestWave    f      gamma     col',
     .        'umn  gua.fwhm log.col  b'
C
C Obs.Wave     Z    RestWave    f      gamma     column  gua.fwhm log.col  b
C 2163.5237 0.77970 1215.670  0.4162  6.27E+08  1.000E+16  10.00  16.00  40.00
C

2     continue
      if ((b_value.gt.0.).and.(column_density.gt.0.)) then
        call xp_draw_voigt(redshift,rest_wave,f_value,gamma_value,
     .                     column_density,b_value,fwhm,3,x1,x2)
      else
        print *,'Error: b_value,column_density=',b_value,column_density
        print *,'Error: Bad values.'
        goto 1
      endif
3     continue
      call PG_CURSOR(cx,cy,key)
      if (key.eq.'b') then
        print '(2a)','IN PLOT WINDOW: Enter b value in km/s.'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        call xp_draw_voigt(redshift,rest_wave,f_value,gamma_value,
     .                     column_density,b_value,fwhm,0,x1,x2)
        b_value = max(0.001d0,min(1000.d0,r8))
        goto 2
      elseif (key.eq.'c') then
        print '(2a)',
     .  'IN PLOT WINDOW: Enter log of column density (log(cm^-2)).'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        call xp_draw_voigt(redshift,rest_wave,f_value,gamma_value,
     .                     column_density,b_value,fwhm,0,x1,x2)
        column_density = max(1.d0,min(1.d+25,(10.d0 ** r8)))
        goto 2
      elseif (key.eq.'d') then
        print '(2a)','IN PLOT WINDOW: Enter column density ( cm^-2 ).'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        call xp_draw_voigt(redshift,rest_wave,f_value,gamma_value,
     .                     column_density,b_value,fwhm,0,x1,x2)
        column_density = max(1.d0,min(1.d+25,r8))
        goto 2
      elseif (key.eq.'f') then
        print '(2a)',
     .  'IN PLOT WINDOW: Enter gaussian smoothing FWHM (km/s).'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        call xp_draw_voigt(redshift,rest_wave,f_value,gamma_value,
     .                     column_density,b_value,fwhm,0,x1,x2)
        fwhm = max(0.d0,min(1000.d0,r8))
        goto 2
      elseif (key.eq.'r') then
        call xp_redraw_data()
        goto 2
      elseif (key.eq.'j') then

        call PGEND

        call PGBEGIN(0,'junk.ps'//PS_Mode,-1*nh,nv)
        print *,'PostScript plot in "junk.ps".'
        call PGASK(pg_ask)
        call PGVSTAND
        call PGPAGE
        call PGWINDOW(xmin,xmax,ymin,ymax)
        call xp_BetterBox
        call xp_redraw_data()
        call PGSLS(4)
        call PG_HLine(xmin,xmax,0.)
        call PGSLS(2)
        call xp_draw_voigt(redshift,rest_wave,f_value,gamma_value,
     .                     column_density,b_value,fwhm,1,x1,x2)
        call PGSLS(1)
        call PGEND

        call PGBEGIN(0,'/XDISP',-1*nh,nv)
        call PGASK(pg_ask)
        call PGVSTAND
        call PGPAGE
        call PGWINDOW(xmin,xmax,ymin,ymax)
        call xp_BetterBox
        call xp_redraw_data()
        call PGSLS(4)
        call PG_HLine(xmin,xmax,0.)
        call PGSLS(1)
        call xp_draw_voigt(redshift,rest_wave,f_value,gamma_value,
     .                     column_density,b_value,fwhm,3,x1,x2)

        goto 2
      elseif ((key.eq.'?').or.(key.eq.'/')) then
        goto 1
      elseif (key.eq.'q') then
        return
      else
        print *,'Unknown key command.'
        goto 1
      endif
9     print *,'Error reading input.'
      return
      end

C-------------------------------------------------------------------
C xv = x value of point to be added.
C Manages and organizes sections.
C Ensures that there are no overlapping sections
C Draws a section when it is completed.
C Keeps the list sorted.

      logical function xp_add_section_point(xv)

      implicit none
      include 'xplot.inc'
      real*4 xv
      integer*4 i,ii,jj,nearxi,k
      logical ok

C Array pixel number.
      k = nearxi(min(xmax,max(xmin,xv)))

C New point within old section?
      if (nsect.gt.0) then
        ok = .true.
        do i=1,nsect
          if ( (k.ge.sect(i,1)).and.(k.le.sect(i,2)) ) ok=.false.
        enddo
        if (.not.ok) then
          print *,'ERROR- Sections may not overlap.'
          xp_add_section_point = .false.
          return
        endif
      endif
      
C Add section point.
      nspt = nspt + 1
      ii = (nspt+1)/2
      jj = 2 - mod(nspt,2)
      sect(ii,jj) = k

C If a section is completed, update nsect, make sure bounds do
C not inclose another section, draw bracket, and sort sections.
      if (mod(nspt,2).eq.0) then
        nsect = nspt/2
        ii = sect(nsect,1)
        jj = sect(nsect,2)
        sect(nsect,1) = min(ii,jj)
        sect(nsect,2) = max(ii,jj)
        if (nsect.gt.1) then
          ok = .true.
          do i=1,nsect-1
           if ( (sect(i,1).ge.sect(nsect,1)).and.
     .          (sect(i,2).le.sect(nsect,2)) ) ok=.false.
          enddo
          if (.not.ok) then
            print *,'ERROR- Sections may not overlap.'
            xp_add_section_point = .false.
            nspt=nspt-1
            nsect=nspt/2
            return
          endif
        endif
        call xp_sectline(nsect,3)
        call xp_sort_sections
      endif

      xp_add_section_point = .true.

      return
      end


C-------------------------------------------------------------------
C Read in continuum segments from a file and then enter xp_sections.

      subroutine xp_read_sections(replot,ask)

      implicit none
      include 'xplot.inc'
      character*40 dfile,dum
      integer*4 i,ii,jj,nearxi,lc,imin,imax
      real*4 r1,r2
      logical replot,xp_add_section_point,ask

      call PGBBUF
C Ask user for data file filename.
      if ((dfile.eq.' ').or.(ask)) then
        print '(3a,$)',' Data file ( ',dfile(1:lc(dfile)),' ) : '
        dum=' '
        read(5,'(a)',err=9) dum
        if (dum.ne.' ') dfile = dum
      endif
C Open data file.
      open(37,file=dfile,status='old',iostat=i)
      if (i.ne.0) goto 9
C Initialize sections.
      nspt=0
      nsect=0
C Read redshift.
      read(37,*,err=8) ze
      ze = max(-0.5,min(9.9999,ze))
C Read sections.
      imin = nearxi(xmin)
      imax = nearxi(xmax)
      read(37,*,err=8) r1,r2 
      do while(r1.gt.0.)
        ii = nearxi(r1)
        jj = nearxi(r2)
        if ((jj.gt.ii).and.(jj.gt.imin).and.(ii.lt.imax)) then
          if (xp_add_section_point(r1)) then
            if (.not.xp_add_section_point(r2)) nspt=nspt-1
          endif
        endif
        read(37,*,err=8) r1,r2 
      enddo
      close(37)
      call PGEBUF
C Call xp_sections, indicate that sections have been set.
      r1 = -2.e+30
      call xp_sections(r1,replot)
      return
8     close(37)
9     print *,'Error with data file :'
      print '(a)',dfile
      call PGEBUF
      return
      end


C----------------------------------------------------------------------------
C Show QSO emission lines given redshift.

      subroutine xp_emission_lines

      implicit none
      include 'xplot.inc'
      integer*4 nearxi,i,lc
      real*4 restvac,obsvac,valread,r,rr,yv
      character line*80,c13*13,c8*8

      call PGBBUF
C Ask emission redshift if necessary.
      if (ze.eq.0.) then
        print '(a,$)','Enter emission redshift : '
        read(5,*,err=9) ze
        ze = max(-0.5,min(9.9999,ze))
      endif
C Open emission line data file and skip to first line: He II 256.
      open(37,file=qefile,status='old',iostat=i)
      if (i.ne.0) goto 9
      line=' '
      do while(line(6:10).ne.'He II')
        read(37,'(a)',err=8) line
      enddo
1     continue
      restvac = valread(line(28:36))
      obsvac  = restvac * ( 1. + ze )
      if ((obsvac.gt.xmin).and.(obsvac.lt.xmax)) then
        call xp_mark_position(obsvac,5)
        call PGQCH(rr)
        call PGSCH(0.85)
        c8 = line(5:12)
        c13= ' '
        write(c13,'(a,1x,i4)',err=8) c8(1:lc(c8)),nint(restvac)
        yv = y(nearxi(obsvac))
        if (yv.lt.ymin+(ymax-ymin)*0.7) then
          r = ymax - (ymax-ymin)*0.02
          call PGPTXT(obsvac,r,270.,0.,c13)
        else
          r = ymin + (ymax-ymin)*0.02
          call PGPTXT(obsvac,r,90.,0.,c13)
        endif
        call PGSCH(rr)
      endif
      read(37,'(a)',end=5,err=8) line
      if (line.eq.' ') goto 5
      goto 1
5     continue
      close(37)
      call PGEBUF
      return
8     close(37)
9     print *,'Error in xp_emission_lines.'
      call PGEBUF
      return
      end

C----------------------------------------------------------------------------
C Load QSO absorption line file.
C
      subroutine xp_load_qafile()
C
      implicit none
      include 'xplot.inc'
      real*8 r81,r82,r83,dev,bestdev
      integer*4 i1,i2,i3,i4,i5,i6,i7,i
      character c8*8,c1*1
      logical more

C Read lines.
      qal_n=0
      open(37,file=qafile,status='old',iostat=i)
      if (i.ne.0) goto 901
      more=.true.
      do while(more)
        read(37,'(f11.4,i2,2x,a8,f8.6,i2,i4,
     .            i3,i2,f6.2,1x,i1,i1,a1)',err=901)
     .            r81,i1,c8,r82,i2,i3,i4,i5,r83,i6,i7,c1
        if (r81.lt.19000.) then
          qal_n          = qal_n + 1
          if (qal_n.gt.maxqal) then
            print *,
     .   'ERROR: xp_load_qalfile: too many QSO absorption lines.'
            print *,
     .   'ERROR: xp_load_qalfile: Increase "maxqal" in xplott.inc'
            more=.false.
            qal_n          = qal_n - 1
          else
            qal_wav(qal_n) = r81
            qal_ion(qal_n) = c8
            qal_osc(qal_n) = r82
            qal_pri(qal_n) = i6
            qal_gam(qal_n) = -1.d0
          endif
        else
          more=.false.
        endif
      enddo
      close(37)
C Read gammas and insert into qal list.
      open(37,file=qgfile,status='old',iostat=i)
      if (i.ne.0) goto 902
3     continue
      read(37,'(7x,f8.3,f9.5,e10.2)',end=5,err=902) r81,r82,r83
C Find match.
      i1=1
      bestdev=abs(qal_wav(1)-r81)
      do i=2,qal_n
        dev = abs(qal_wav(i)-r81)
        if (dev.lt.bestdev) then
          i1 = i
          bestdev = dev
        endif
      enddo
C Error check.
      if (bestdev.gt.1.) print *,'ERROR- No match for gamma for:',r81
C Load data in list.
      qal_gam(i1) = r83
      goto 3
5     continue
      close(37)
      return
901   print *,'Error reading qal file.'
      return
902   print *,'Error reading gamma file.'
      return
      end

C----------------------------------------------------------------------------
C Show QSO absorption lines given redshift.

      subroutine xp_absorption_lines(priority)

      implicit none
      integer*4 priority
      include 'xplot.inc'
      real*4 r1,r2,rr,yv,ry,xpos
      integer*4 nearxi,i,lc,ii
      character c13*13, c2*2

      call PGBBUF
C Ask redshift if necessary.
      if (abs(za).lt.1.e-30) then
        print '(a,$)','Enter absorption redshift ( -1 for none ) : '
        read(5,*,err=9) za
        za = max(-9.9,min(99.9,za))
        if (abs(za+1.0).lt.1.e-30) return
      endif
C Draw lines.  Doublets are drawn with bracket.
      do i=1,qal_n
      if (qal_pri(i).le.priority) then
        r1 = sngl(qal_wav(i))*(1.+za)
        if ((r1.gt.xmin).and.(r1.lt.xmax)) then
          xpos = 0. 
C Doublet.
          if (abs(qal_wav(i)-qal_wav(i+1)).lt.20.) then
            if (qal_ion(i).eq.qal_ion(i+1)) then
              r2 = sngl(qal_wav(i+1))*(1.+za)
              if ((r2.gt.xmin).and.(r2.lt.xmax)) then
                call xp_double_mark(r1,r2,8)
                xpos = (r1+r2)/2.
              endif
            endif
          endif
C Single (always drawn.)
          call xp_mark_position(r1,8)
          xpos = r1
C Draw components to this line.
          do ii=1,za_also_n
            r1 = sngl(qal_wav(i))*(1.+za_also(ii))
            call xp_mark_position(r1,7)
            call PGSCI(7)
            write(c2,'(i2.2)',err=9) ii
            r2 = ymin + (ymax-ymin)*0.05
            call PGTEXT(r1,r2,c2)
            call PGSCI(color)
          enddo
C Write ion name and wavelength.
          call PGQCH(rr)
          call PGSCH(0.85)
          c13=' '
          write(c13,'(a,1x,i4)',err=9)
     .          qal_ion(i)(1:lc(qal_ion(i))),nint(qal_wav(i))
          yv = y(nearxi(xpos))
          if (yv.lt.ymin+(ymax-ymin)*0.7) then
            ry = ymax - (ymax-ymin)*0.02
            call PGPTXT(xpos,ry,270.,0.,c13)
          else
            ry = ymin + (ymax-ymin)*0.02
            call PGPTXT(xpos,ry,90.,0.,c13)
          endif
          call PGSCH(rr)
        endif
      endif
      enddo
      call PGEBUF
      return
9     print *,'Error in xp_absorption_lines.'
      call PGEBUF
      return
      end


C----------------------------------------------------------------------------
C Calculate y limits for minimum and maximum of data values.

      subroutine autoy(yfix,yfixzero)

      implicit none
      logical yfix,yfixzero
      real*4 yhigh,ylow
      integer*4 i

      include 'xplot.inc'

      if (yfix) then
        if (yfixzero) ymin = 0.
        return
      endif
      yhigh = -1.e+30
      ylow  = +1.e+30
      do i=1,np_bin
        if ((x_bin(i).gt.xmin).and.(x_bin(i).lt.xmax)) then
          if (y_bin(i).gt.yhigh) yhigh=y_bin(i)
          if (y_bin(i).lt.ylow)  ylow =y_bin(i)
        endif
      enddo
      if (ylow.gt.yhigh) then
        print *,'subroutine autoy: Error no points within x range.'
        return
      endif

C Increase limits slightly for aesthetics.
      if (yfixzero) then
        ymin = 0.
        ymax = 1.03 * yhigh
      else
        ymin = ylow - ( 0.03*(yhigh-ylow) )
        ymax = yhigh+ ( 0.03*(yhigh-ylow) )
      endif

      return
      end


C----------------------------------------------------------------------------
C Divide continuum.
C
      subroutine xp_contdiv(replot)

      implicit none
      include 'xplot.inc'
      integer*4 i
      real*4 r
      logical replot

      replot = .false.
      if (contbuf.eq.0) then
        print *,'Error- No continuum set.'
        return
      endif
      print *,
     .'This will alter both data and error spectrum buffers and will'
      print *,
     .'set any "zero continuum" segments to zero in the data & error.'
      print *,
     .'It will also change your current continuum.'
      print '(a,$)','Do you wish to continue? (1/0) : '
      read(5,*,err=9) i
      if (i.ne.1) return
      do i=1,np
        r = c(i)
        if (r.lt.1.e-30) then
          y(i) = 0.
          e(i) = 0.
          c(i) = 0.
        else
          y(i) = y(i) / r
          e(i) = e(i) / r
          c(i) = 1.
        endif
      enddo
      norm   = .true.
      replot = .true.
      return
9     print *,'Error in xp_contdiv.'
      return
      end

C----------------------------------------------------------------------------
C Calculations for centroid and equivalent width, between array elements
C s1 and s2, which must be at least 2 elements from array boundaries.
C The continuum must be set and < 0 bewteen s1 and s2.
C If mode=1, prompts for wavelength error.
C If mode=2, calculate column density.
C If ZeroPoint <> 0, then it is used in the following calculations.
C
      subroutine xp_centew(s1,s2,mode)

      implicit none
      include 'xplot.inc'
      integer*4 i,j,s1,s2,mode,k,lc,kk
      real*8 r,rr,r1,r2,xcent,xtopcent,xew,xewerr,zp
      real*8 xcenterr,lamerr,dx,xflux,xfluxerr,osc,restwave,wave1,wave2
      character*200 title
      logical emission
      real*4 xx(2),yy(2),median,cd,cde,ri,rie,ColDen,ColDenErr,rr4
      real*8 wave_out,osc_out,gamma_out,CogColDen(6),bv(6)
      character*8 ion_out
      character*80 basefile
C
C Set variables.
      basefile='/x1/tom/cog/lyman.out'
      title=' '
C Make sure there is a continuum.
      if (contbuf.eq.0) then
        print *,
     .  'Error- No continuum set. Use NORM keyword if normalized.'
        return
      else
        j=0
        do i=s1,s2
          if (c(i).lt.1.e-30) j=1
        enddo
        if (j.eq.1) then
          print *,'Error- Part of relevant continuum <= 0.'
          return
        endif
      endif
C
      print '(a,f11.4,a,f11.4)','X limits:',xo(s1),' ->',xo(s2)
      k=lc(title)+2
      write(title(k:),'(2f11.4)',err=9) x(s1),x(s2)

C Ask for lambda error.
      if (mode.eq.1) then
        print '(a,$)','What is the error in wavelengths : '
        read(5,*,err=9) lamerr
      endif

C Calculate centroid.
      r1 = 0.d0
      r2 = 0.d0
      do i=s1,s2
        r = abs(dble(y(i)-c(i)))
        r1= r1 + (x(i)*r)
        r2= r2 + r
      enddo
      xcent = r1 / r2
      print '(a,f11.4)',' Centroid (red line) = ',xcent
      k=lc(title)+2
      write(title(k:),'(f11.4)',err=9) xcent
      yy(1) = ymin
      yy(2) = ymax
      xx(1) = sngl(xcent)
      xx(2) = sngl(xcent)
      call PGSCI(2)                        ! red
      call PGLINE(2,xx,yy)
      call PGSCI(color)                    ! default
      if (mode.eq.1) then 
        r1 = 0.d0
        do i=s1,s2
          r1 = r1 + ( dble(e(i)*e(i))*(xo(i)-xcent)*(xo(i)-xcent) )
     .            + ( lamerr*lamerr*dble((y(i)-c(i))*(y(i)-c(i))) )
        enddo
        xcenterr = sqrt( r1 / (r2*r2) )
        print '(a,f10.4)',' Centroid error = ',xcenterr
        k=lc(title)+2
        write(title(k:),'(a,f10.4)',err=9) ,'+/-',xcenterr
      endif

C Calculate "above-median" centroid.
      do i=s1,s2
        a(i) = abs(y(i)-c(i))
      enddo
      call find_median(a(s1),1+s2-s1,median)
      r1 = 0.d0
      r2 = 0.d0
      do i=s1,s2
        r = abs(dble(y(i)-c(i)))
        if (r.gt.dble(median)) then
          r1 = r1 + (xo(i)*r)
          r2 = r2 + r
        endif
      enddo
      xtopcent = r1 / r2
      print '(a,f11.4)',
     .  ' "Top half median" centroid (green line) = ',xtopcent
      k=lc(title)+2
      write(title(k:),'(f11.4)',err=9) xtopcent
      yy(1) = ymin
      yy(2) = ymax
      xx(1) = sngl(xtopcent)
      xx(2) = sngl(xtopcent)
      call PGQLS(kk)
      call PGSLS(4)
      call PGSCI(3)                        ! green
      call PGLINE(2,xx,yy)
      call PGSCI(color)                    ! default
      call PGSLS(kk)

      if (abs(ZeroPoint).gt.1.e-30) then
        print *,' Using ZeroPoint of ',ZeroPoint
      endif

C Calculate equivalent width and error.
      xew = 0.d0
      xewerr = 0.d0
      do i=s1,s2
        r = dble(y(i)-ZeroPoint) / dble(c(i)-ZeroPoint)
        rr= dble(e(i)) / dble(c(i)-ZeroPoint)
        dx= (xo(i+1)-xo(i-1))/2.d0
        xew = xew + ( (1.d0 - r) * dx )
        xewerr = xewerr + ( rr*rr*dx*dx )
      enddo
      xewerr = sqrt(xewerr)
      if (errbuf.gt.0) then
        print '(a,f12.6,a,f10.6)',
     .  ' Equivalent Width =',xew,'  +/-',xewerr
        k=lc(title)+2
        write(title(k:),'(f12.6,a,f10.6)',err=9) xew,' +/-',xewerr
      else
        print '(a,f11.4)',' Equivalent Width =',xew
        k=lc(title)+2
        write(title(k:),'(f12.6)',err=9) xew
      endif

C Is this an emission line?
      if (xew.gt.0.) then
        emission = .false.
      else
        emission = .true.
      endif

C Calculate column density, if this is an absorption line.
      if ((mode.eq.2).and.(.not.emission)) then
C Find Column Density for resolved line.
        zp = ZeroPoint
        print '(a,$)',' Rest wavelength: '
        read(5,*,err=9) restwave
        call xp_GrabLineData(restwave,1,wave_out,
     .                       osc_out,gamma_out,ion_out)
        if (osc_out.gt.0.) then
          restwave= wave_out
          osc = osc_out
          print '(a,f9.3,f9.5)',ion_out,restwave,osc
        else
          print '(a,$)',' Oscillator Strg: '
          read(5,*,err=9) osc
        endif
        ColDen=0.
        ColDenErr=0.
        do i=s1,s2
          r = dble(y(i)) / dble(c(i))
          rr= dble(e(i)) / dble(c(i))
          ri = sngl(r)
          rie= sngl(rr)
          wave1 = (xo(i-1)+xo(i))/2.d0
          wave2 = (xo(i+1)+xo(i))/2.d0
          if (ri.gt.0.99999) then
            continue
          elseif (ri.lt.0.00001) then
            print *,
     .  'Error finding column density, flux goes below zero.'
            ColDen=-1.e+30
            goto 888
          elseif (rie.lt.0.) then
            print *,
     .  'Error finding column density, error less than zero.'
            ColDen=-1.e+30
            goto 888
          else
            call ColumnDensity(wave1,wave2,restwave,
     .                         ri,rie,osc,zp,cd,cde)
            ColDen=ColDen+cd
            ColDenErr=ColDenErr+(cde*cde)
          endif
        enddo
888     continue
        if (ColDenErr.gt.0.) then
          ColDenErr = sqrt(ColDenErr)
        else
          ColDenErr = 0.
        endif
        if (ColDen.gt.0.) then
          print '(2(a,1pe12.4))','        Column density=',ColDen,
     .                           '  +/-',ColDenErr
          print '(3(a,   f7.3))',
     .  ' Log of Column density=',log10(ColDen),
     .  '   +',log10(ColDen+ColDenErr)-log10(ColDen),
     .  '   -',log10(ColDen)-log10(ColDen-ColDenErr)
        else
          print *,' No column density calculated.'
        endif
C Find Column Density from Curve-Of-Growth.
        bv(1) =  5.d0
        bv(2) = 10.d0
        bv(3) = 15.d0
        bv(4) = 20.d0
        bv(5) = 30.d0
        bv(6) = 40.d0
        do i=1,6
          call CogColumn(basefile,xew,xcent,
     .                   restwave,osc,bv(i),CogColDen(i))
        enddo
        print *,
     .  'Estimate from E.W. and Curve-of-Growth (Lyman-alpha Gamma):'
        print '(a,6(f11.4))','        b = ',(bv(i),i=1,6)
        print '(a,6(f11.4))',' Log(C.D.)= ',(CogColDen(i),i=1,6)
      endif

C Calculate flux and error.
      if (emission) then
        xflux = 0.d0
        xfluxerr = 0.d0
        do i=s1,s2
          dx = (xo(i+1)-xo(i-1))/2.d0
          xflux = xflux + ( dble(y(i)-c(i))*dx )
          xfluxerr = xfluxerr + ( dble(e(i)*e(i))*dx*dx )
        enddo
        xfluxerr =  sqrt(xfluxerr)
        if (errbuf.gt.0) then
          print '(a,1pe14.5,a,1pe12.5)',
     .          ' Emission Line Flux =',xflux,'   +/-',xfluxerr
          k=lc(title)+2
          write(title(k:),'(1pe13.5,a,1pe12.5)',err=9) 
     .              xflux,' +/-',xfluxerr
        else
          print '(a,1pe14.5)',' Emission Line Flux =',xflux
          k=lc(title)+2
          write(title(k:),'(1pe13.5)',err=9) xflux
        endif
      endif  

      k=lc(title)
      print '(a)',title(1:k)
      call PGQCH(rr4)
      call PGSCH(0.85)
      call PGMTEXT('T',0.5,0.5,0.5,title(1:k))
      call PGSCH(rr4)

C Write output to equivalent width data file if requested.
      if (ewfile.ne.' ') then
        if (emission) then
          if (mode.eq.1) then
            write(34,'(a,f11.4,a,f8.4,1pe13.5,a,1pe12.5)')
     .         title(1:23),xcent,' +/-',xcenterr,xflux,' +/-',xfluxerr
          else
            write(34,'(a,f11.4,       1pe13.5,a,1pe12.5)')
     .         title(1:23),xcent,                xflux,' +/-',xfluxerr
          endif
        else
          if (mode.eq.1) then
            write(34,'(a,f11.4,a,f8.4,f10.5,a,f9.5)')
     .         title(1:23),xcent,' +/-',xcenterr,xew,' +/-',xewerr
          else
            write(34,'(a,f11.4,       f10.5,a,f9.5)')
     .         title(1:23),xcent,                xew,' +/-',xewerr
          endif
        endif
      endif

      return
9     print *,'Error in centew.'
      return
      end


C----------------------------------------------------------------------------
C Calculations for 2nd moment arc-line centroid.
C Input:
C     cx                 ! guess at center
C Output:
C     cw                 ! centroid output (wavelength or pixel)
C     cp                 ! centroid output (pixel)
C
      subroutine xp_arccent(cx,cw,cp)

      implicit none
      real*4 cx,cw,cp
      include 'xplot.inc'
      integer*4 nearxi,ni,i,ipeak
      integer*4 peakrad,baserad,centrad
      real*4 r,r1,r2,r3,ypeak,ybase,centx,centpix,xt(2),yt(2)
      logical emission
      character*7 c7a,c7b,c7c,rf7
      character*9 c9a,rf9

C First find the array element nearest the cursor
      ni=nearxi(cx)
      if ( (ni.lt.6) .or. (ni.gt.np-6) ) then
        print *,'Error- too close to edge!'
        return
      endif

C Set radii
      centrad = crad             ! default = 3
      peakrad = centrad + 1
      baserad = centrad + 2

C First decide whether the line is absorption or emission
6     r1 = 0.
      do i = ni-centrad, ni+centrad
        r1 = r1 + y(i)
      enddo
      r1 = r1 / float(2*centrad + 1)
      r2 = 0.
      do i = ni-baserad, ni+baserad
        r2 = r2 + y(i)
      enddo
      r2 = r2 / float(2*baserad + 1)
      if (r1.gt.r2) then
        emission = .true.
      else
        emission = .false.
      endif
      
C Find base line and "peak".
      ipeak = ni
      ypeak = y(ni)
      ybase = y(ni)
      if (emission) then
        do i = max(1,ni-peakrad), min(np,ni+peakrad)
          if (y(i).gt.ypeak) then
            ipeak = i
            ypeak = y(i)
          endif
        enddo
        do i = max(1,ipeak-baserad), min(np,ipeak+baserad)
          if (y(i).lt.ybase) ybase = y(i)
        enddo
      else
        do i = max(1,ni-peakrad), min(np,ni+peakrad)
          if (y(i).lt.ypeak) then
            ipeak = i
            ypeak = y(i)
          endif
        enddo
        do i = max(1,ipeak-baserad), min(np,ipeak+baserad)
          if (y(i).gt.ybase) ybase = y(i)
        enddo
      endif

C Now calculate 2nd moment centroid.
      r1 = 0.
      r2 = 0.
      r3 = 0.
      do i = max(1,ipeak-centrad), min(np,ipeak+centrad)
        r = max(0.,abs(y(i)-ybase))
        r = r*r
        r1= r1 + (r*x(i))
        r2= r2 + (r*float(i))
        r3= r3 + (r)
      enddo
      centx   = r1/r3
      centpix = r2/r3

C Try narrower radii if centroid is too far from peak.
      i = nearxi(centx)
      if ((abs(i-ipeak).gt.centrad-1).and.(peakrad.gt.1)) then
        peakrad = max(1,peakrad-1)
        centrad = max(1,centrad-1)
        print *,'Trying narrower search parameters...'
        goto 6
      endif

      c7a = rf7(ypeak)
      c7b = rf7(ybase)
      c9a = rf9(centx)
      c7c = rf7(centpix)

C Echo.
      print '(9(a))','Peak= ',c7a,'  Base= ',c7b,
     .        '  Centroid(2nd m.)= ',c9a,'  (pixel= ',c7c,' )'
      call PGBBUF
      call xp_mark_position(centx,2)
      i = max(1,ipeak-centrad)
       call xp_mark_position(x(i),4)
      i = min(np,ipeak+centrad)
      call xp_mark_position(x(i),4)
      xt(1) = xmin
      xt(2) = xmax
      yt(1) = ybase
      yt(2) = ybase
      call PGSCI(4)
      call PGLINE(2,xt,yt)
      call PGSCI(1)
      call PGEBUF

      cw = centx
      cp = centpix

      return
      end


C----------------------------------------------------------------------------
      integer*4 function nearxi(cx)
      implicit none
      real*4 cx,r,rmin
      integer*4 i,j
      include 'xplot.inc'
      j=1
      rmin=abs(x(1)-cx)
      do i=1,np
        r = abs(x(i)-cx)
        if (r.lt.rmin) then
          rmin = r
          j    = i
        endif
      enddo
      nearxi = j
      return
      end
C----------------------------------------------------------------------------
      integer*4 function nearxbini(cx)
      implicit none
      real*4 cx,r,rmin
      integer*4 i,j
      include 'xplot.inc'
      j=1
      rmin=abs(x_bin(1)-cx)
      do i=1,np_bin
        r = abs(x_bin(i)-cx)
        if (r.lt.rmin) then
          rmin = r
          j    = i
        endif
      enddo
      nearxbini = j
      return
      end

C----------------------------------------------------------------------------
C Does statistics on sections.
      subroutine xp_stats
      implicit none
      integer*4 i,num,totnum,ii,s1,s2
      real*4 r,yhigh,ylow,mean,rms,xp_ymedian,totmean
      include 'xplot.inc'
      if (nsect.lt.1) then
        print *,'Error- No sections set.'
        return
      endif
      totmean=0.
      totnum =0
      do ii=1,nsect
        s1=sect(ii,1)
        s2=sect(ii,2)
        mean = 0.
        num  = 0
        yhigh= y(s1)
        ylow = y(s1)
        do i=s1,s2
          r = y(i)
          num = num + 1
          mean= mean + r
          totnum = totnum + 1
          totmean= totmean + r
          if (r.gt.yhigh) yhigh = r
          if (r.lt.ylow)  ylow  = r
        enddo
        mean = mean / float(num)
        rms = 0.
        do i=s1,s2
          r = (y(i)-mean)
          rms= rms + (r*r)
        enddo
        rms = sqrt(rms/float(num))
        i = 0
        r = xp_ymedian(s1,s2)
C Section#12  Pixels:12345 -12345  (12345.12 -12345.12)  Number=12345
        print '(a,i2,a,i5,a,i5,a,f8.2,a,f8.2,a,i5)', ' Section#',ii,
     .   '  Pixels:',s1+i,' -',s2+i,
     .   '  (',x(s1),' -',x(s2),')  Number=',num
C   High=----+----1----  Low=----+----1----  Median=----+----1----
        print '(a,1pe14.6,a,1pe14.6,a,1pe14.6)',
     .   '   Mean=',mean,'  RMS=',rms,'  Median=',r
        print '(a,1pe14.6,a,1pe14.6)','   High=',yhigh,'  Low=',ylow
      enddo
      totmean=totmean/float(totnum)
      print '(a,i6,a,1pe14.6)',
     .  ' Total number=',totnum,'  Total mean=',totmean
      print *,' '
      return
      end


C----------------------------------------------------------------------------
C Returns median in common array y(s1..s2).  Does not change y(..) array.

      real*4 function xp_ymedian(s1,s2)
      implicit none
      integer*4 s1,s2,i,n
      include 'xplot.inc'
      real*4 r
      n=0 
      do i=s1,s2
        n=n+1
        a(n)=y(i)
      enddo
      call find_median(a,n,r)
      xp_ymedian = r
      return
      end

C----------------------------------------------------------------------------
C Draw little arrows near selected point.
C ic = color: 1=white, 2=red, 3=green, 4=blue,...
C
      subroutine xp_mark_position(cx,ic)
      implicit none
      real*4 cx,xx(1),yy(1),yv
      integer*4 ic,nearxi
      include 'xplot.inc'
      yv = y(nearxi(cx))
      xx(1) = cx
      yy(1) = yv + abs(ymax-ymin)*0.03
      call PGSCI(ic)
      call PGPOINT(1,xx,yy,31)
      yy(1) = yv - abs(ymax-ymin)*0.03
      call PGPOINT(1,xx,yy,30)
      call PGSCI(1)
      return
      end
C----------------------------------------------------------------------------
C Draw one little arrow below selected point, unless the point is near the
C bottom of the window, then draw an arrow above and below.
C ic = color: 1=white, 2=red, 3=green, 4=blue,...
C
      subroutine xp_single_mark(cx,ic)
      implicit none
      real*4 cx,xx(1),yy(1),yv
      integer*4 ic,nearxi
      include 'xplot.inc'
      yv = y(nearxi(cx))
      xx(1) = cx
      if ( (yv-ymin)/(ymax-ymin).lt.0.10) then
        yy(1) = yv + abs(ymax-ymin)*0.03
        call PGSCI(ic)
        call PGPOINT(1,xx,yy,31)
      endif
      yy(1) = yv - abs(ymax-ymin)*0.03
      call PGPOINT(1,xx,yy,30)
      call PGSCI(1)
      return
      end


C--------------------------------------------------------------------------
C Create a better looking PGPLOT box.
C
      subroutine xp_BetterBox
C
      implicit none
      include 'xplot.inc'
      real*4 xr,x41,x42
      real*8 v81,v82,x81,x82
      character*80 xstr,ystr
      xr = xmax - xmin
      ystr = 'BCNST'
      if (show_velocity) then
        xstr = 'BNST'
      else
        xstr = 'BCNST'
      endif
      if ((xr.gt.600).and.(xr.lt.1500)) then
        call PGBOX(xstr,100.,10,ystr,0.,0)
      elseif ((xr.gt.60).and.(xr.lt.170)) then
        call PGBOX(xstr,10. ,10,ystr,0.,0)
      elseif ((xr.gt.5).and.(xr.lt.20)) then
        call PGBOX(xstr,1.  ,10,ystr,0.,0)
      else
        call PGBOX(xstr,0.  , 0,ystr,0.,0)
      endif
      if (show_velocity) then
        x81=dble(xmin)
        x82=dble(xmax)
        v81=0.d0
        v82=0.d0
        call RelativeVelocity(lambda_zero_velocity,x81,v81)
        call RelativeVelocity(lambda_zero_velocity,x82,v82)
        x41=sngl(v81)
        x42=sngl(v82)
        call PGWINDOW(x41,x42,ymin,ymax)
        call PGBOX('CMST',0.,0,' ',0.,0)
        call PGWINDOW(xmin,xmax,ymin,ymax)
      endif
      return
      end

C--------------------------------------------------------------------------
C Sorts sections.

      subroutine xp_sort_sections

      implicit none
      include 'xplot.inc'
      integer*4 i,ia,ib
      real*4 r

      if (nsect.lt.1) return
      do i=1,nsect
        ia=sect(i,1)
        ib=sect(i,2)
        sect(i,1)=min(ia,ib)
        sect(i,2)=max(ia,ib)
      enddo
      do i=1,nsect
        a(i) = float(sect(i,1))
        b(i) = float(sect(i,2))
      enddo
      call find_median(a,nsect,r)
      call find_median(b,nsect,r)
      do i=1,nsect
        sect(i,1) = nint(a(i))
        sect(i,2) = nint(b(i))
      enddo
      return
      end


C--------------------------------------------------------------------------
C Allows user to enter multiple sub-sections, and then offers a few options
C to perform on those sections.

      subroutine xp_sections(cx,replot)

      implicit none
      include 'xplot.inc'
      integer*4 i,nearxi,last_nspt,last_nsect,last_fspt
      real*4 cx,cy
      character*1 key
      logical replot,xp_add_section_point
C
      common /XPSECTIONSBLK/ last_nspt,last_nsect,last_fspt
C
      replot = .false.

C Skip first part if some of the sections have already been set.
      if (cx.lt.-1.e+30) goto 2

C Initialize sections and set first mark.
      last_nspt = nspt
      last_nsect= nsect
      last_fspt = sect(1,1)
      nspt = 0
      nsect= 0
      if (xp_add_section_point(cx)) then
        call xp_mark_position(x(nearxi(cx)),3)
      endif

C                    ----+----1----+----2----+----3----+----4

2     print *,
     .  'Hit key:  <sp>, a, b, c, d, f, p, q, r, s, or "?" for help.'

4     continue
      call PG_CURSOR(cx,cy,key)

      if ((key.eq.'?').or.(key.eq.'/')) then
        print *,' <sp> = Select next section limit.'
        print *,'  "a" = Arbitrary (user) piecewise continuum.'
        print *,'  "b" = Boxcar smoothing of spectrum or continuum.'
        print *,'  "c" = Continuum/polynomial fitting.'
        print *,
     .  '  "d" = Delete the section enclosing the cursor position.'
        print *,
     .  '  "f" = Fit curves, including Gaussian, Power Law, etc.'
        print *,'  "p" = Piecewise continuum fitting.'
        print *,'  "q" = Quit sections, return to main menu.'
        print *,'  "r" = Recover previous sections.'
        print *,'  "s" = Statistics.'
        print *,
     .  'Hit key:  <sp>, b, c, d, f, p, q, r, s, or "?" for menu.'
        goto 4

      elseif (key.eq.' ') then
        if (xp_add_section_point(cx)) then
          call xp_mark_position(x(nearxi(cx)),3)
        endif
        goto 4

      elseif (key.eq.'r') then
        if (nspt.gt.1) then
          print *,'Sorry, cannot recover previous sections.'
        else
          call xp_mark_position(x(sect(1,1)),0)
          sect(1,1) = last_fspt
          nsect     = last_nsect
          nspt      = last_nspt
          do i=1,nsect
            call xp_sectline(i,3) 
          enddo
        endif
        goto 4

      elseif (key.eq.'d') then
        call xp_delete_section(cx)
        goto 4

      elseif (key.eq.'q') then
        goto 8

      elseif (key.eq.'s') then
        call xp_stats
 
      elseif (key.eq.'b') then
        call xp_boxcar(replot)

      elseif (key.eq.'c') then
        apt_n=0
        call xp_contpoly(replot)
        apt_n=0

      elseif (key.eq.'f') then
        call xp_fitfunk(replot)

      elseif (key.eq.'p') then
        call xp_piecewise(replot)

      elseif (key.eq.'a') then
        call xp_arbitrary(replot)

      else
        print *,'Unknown command.'
        goto 4

      endif

8     print *,'Returning to main menu.'
      return
9     print *,'Error in xp_sections.'
      return
      end


C------------------------------------------------------------------- 
C Allow user to remove a section they have decided not to set.

      subroutine xp_delete_section(cx)

      implicit none
      include 'xplot.inc'
      integer*4 i,ii,k,nearxi
      real*4 cx
      k=0
      i=nearxi(cx)
      do ii=1,nsect
        if ( (i.ge.sect(ii,1)).and.(i.le.sect(ii,2)) ) k=ii
      enddo
      if (k.eq.0) then
        print *,'No section was removed.'
        print *,'Place cursor within section to delete and hit "d".'
      else
        call xp_mark_position(x(sect(k,1)),0)
        call xp_mark_position(x(sect(k,2)),0)
        call xp_sectline(k,0)
        nsect=nsect-1
        nspt =nspt -2
        do ii=k,nsect
          sect(ii,1) = sect(ii+1,1)
          sect(ii,2) = sect(ii+1,2)
        enddo
      endif
      return
      end

C--------------------------------------------------------------------------
C This draws a line between the endpoints of a section.
C ic = color (usually 3=green)

      subroutine xp_sectline(n,ic)

      implicit none
      include 'xplot.inc'
      integer*4 n,ic
      real*4 x1,x2
      x1=x(min(sect(n,1),sect(n,2)))
      x2=x(max(sect(n,1),sect(n,2)))
      call xp_double_mark(x1,x2,ic)
      return
      end

C-------------------------------------------------------------------
C Draw a square-horseshoe shaped mark to indicate to positions.

      subroutine xp_double_mark(x1,x2,ic)

      implicit none
      include 'xplot.inc'
      real*4 x1,x2,xx(4),yy(4),high,low,ybar,r
      integer*4 i,nearxi,ia,ib,ic

      xx(1) = min(x1,x2)
      xx(2) = xx(1)
      xx(3) = max(x1,x2)
      xx(4) = xx(3)
      ia = nearxi(xx(1))
      ib = nearxi(xx(3))
      high=y(ia)
      low =y(ia)
      do i=ia,ib
        if (y(i).gt.high) high=y(i)
        if (y(i).lt.low)  low =y(i)
      enddo
      r = (ymax-ymin)*0.1
      if ((high+r).lt.ymax) then
        ybar = high + r
      elseif ((low-r).gt.ymin) then
        ybar = low - r
      elseif ((high+(r/2.)).lt.ymax) then
        ybar = high + (r/2.)
      elseif ((low-(r/2.)).gt.ymin) then
        ybar = low - (r/2.)
      else
        ybar = high + (r/4.)
      endif
      yy(1) = (y(ia)+ybar)/2.
      yy(2) = ybar
      yy(3) = ybar
      yy(4) = (y(ib)+ybar)/2.
      call PGSCI(ic)
      call PGLINE(4,xx,yy)
      call PGSCI(color)
      return
      end


C--------------------------------------------------------------------------
C This routine allows user to set an arbitrary continuum by connecting line
C segments.

      subroutine xp_arbitrary(replot)

      implicit none
      include 'xplot.inc'
      integer*4 i,j,nearxi,i1,i2,kk
      real*4 cx,cy,xpt,ypt,xx(2),yy(2)
      character key*1
      logical replot

C Clear temporary array.
      do i=1,np
        b(i)=0.
      enddo
      xpt = -1.
      ypt =  0.

C Have user connect line segments.
1     continue
      print '(a)',
     .  ' LMB = connect current cursor position to last point.'
      print '(a)',
     .  ' MMB = show where line would be, but do not set values.'
      print '(a)',
     .  ' RMB = move point without connecting to last point.'
      print '(a)',' "S" = substitute data spectrum.'
      print '(a)',' "s" = substitute continuum.'
      print '(a)',' "q" = quit.'

2     continue
      call PG_CURSOR(cx,cy,key)
      if (key.eq.'A') then         ! Left Mouse Button
        if (xpt.gt.-1.) then
          i=nearxi(xpt) 
          j=nearxi(cx) 
          i1 = min(i,j)
          i2 = max(i,j)
          if (abs(cx-xpt).gt.1.e-10) then
            do i=i1,i2
              b(i) = ypt + ( (x(i)-xpt) * ((cy-ypt)/(cx-xpt)) )
            enddo
          else
            b(i1) = cy
          endif
          call PGSCI(5)                    ! turquoise
          call PG_LineBuf((1+i2-i1),x(i1),b(i1))
          call PGSCI(color)                ! default
        endif
        xpt = cx
        ypt = cy
        call PGSCI(5)
        call PGPT(1,xpt,ypt,5)
        call PGSCI(color)
        goto 2

      elseif (key.eq.'D') then     ! Middle Mouse Button
        xx(1)=xpt
        yy(1)=ypt
        xx(2)=cx
        yy(2)=cy
        call PGSCI(15)
        call PGQLS(kk)
        call PGSLS(4)
        call PGLINE(2,xx,yy)
        call PGSLS(kk)
        call PGSCI(color)
        goto 2

      elseif (key.eq.'X') then     ! Right Mouse Button
        xpt = cx
        ypt = cy
        call PGSCI(5)
        call PGPT(1,xpt,ypt,5)
        call PGSCI(color)
        goto 2

      elseif ((key.eq.'s').or.(key.eq.'S')) then
        call xp_substitution(key)
        replot    = .true.
        print *,'ok.'

      elseif (key.ne.'q') then
        goto 1

      endif

      return
      end


C--------------------------------------------------------------------------
C This routine is used to fit a piecewise continuum to a spectrum with 
C several continuum segments set.

      subroutine xp_piecewise(replot)

      implicit none
      include 'xplot.inc'
      integer*4 i,k,ncoeff,ia,ib,ts,order
      parameter(ts=20)                    ! too small a region limit
      real*4 r4,xa,xb,ya,yb,valread,cx,cy
      real*8 coeff(0:99),polyval
      character dum*8, key*1
      logical xp_line_fit,replot

      if (nsect.lt.2) then
        print *,'You must have at least two continuum sections set.'
        return
      endif

C Clear temporary array.
      do i=1,np
        b(i)=0.
      enddo

C User can select polynomial to use within section.
      print '(a,$)',' Order of polynomial within sections (1) : '
      dum=' '
      read(5,'(a)',err=9) dum
      if (dum.eq.' ') then
        order=1
      else
        order = max(0,min(19,nint(valread(dum))))
      endif
      print '(a,i3,a)',' Using polynomial order',order,'.'

C Fit line to section k and then build continuum using end of previous section.
      k=1  
      DO WHILE(k.le.nsect)
      ia=sect(k,1)
      ib=sect(k,2)
      ncoeff = order + 1
      if (.not.xp_line_fit(ia,ib,coeff,ncoeff)) goto 9

C Fill in temporary array, connecting the ends of the fitted sections.
C Fill section with its fit.
      do i=sect(k,1),sect(k,2)
        b(i)= sngl(polyval(ncoeff,coeff,dble(x(i))))
      enddo
C Fill blue side of first region.
      if (k.eq.1) then
        do i=1,sect(k,1)
          b(i)= sngl(polyval(ncoeff,coeff,dble(x(i))))
        enddo
      endif
C Fill red side of last region.
      if (k.eq.nsect) then
        do i=sect(k,2),np
          b(i)= sngl(polyval(ncoeff,coeff,dble(x(i))))
        enddo
      endif
C Connect this section with the last section by the endpoint/midpoint.
C If a region is too small, use its midpoint.
      if (k.gt.1) then
        if ((sect(k-1,2)-sect(k-1,1)).gt.ts) then
          ia = sect(k-1,2)
        else
          ia = (sect(k-1,1)+sect(k-1,2))/2
        endif
        xa = x(ia)
        ya = b(ia)
        if ((sect(k,2)-sect(k,1)).gt.ts) then
          ib = sect(k,1)
        else
          ib = (sect(k,1)+sect(k,2))/2
        endif
        xb = x(ib)
        yb = sngl(polyval(ncoeff,coeff,dble(xb)))
        r4 = (ya-yb)/(xa-xb)
        do i=ia,ib
          b(i) = ( r4 * (x(i)-xb) ) + yb
        enddo
      endif

C Next segment.
      k=k+1
      ENDDO
C Plot the temporary array.
      call PGSCI(2)                    ! red
      call PG_LineBuf(np,x,b)
      call PGSCI(color)                ! default

C Ask user to substitute all or part of the continuum.
C              ----+----1----+----2----+----3----+----4
      print *,' s = Substitute continuum.'
      print *,' S = Substitute data spectrum.'
      print *,' q = Quit.'
      call PG_CURSOR(cx,cy,key)
      if ((key.eq.'s').or.(key.eq.'S')) then
        call xp_substitution(key)
        replot   = .true.
        print *,'ok.'
      endif

      return

9     print *,'Error in xp_piecewise.'
      return
      end


C--------------------------------------------------------------------
C Fit a straight line to a (QSO) continuum section.
C Adjusts, for Ly-alpha forest, error weighting, rejection of points,
C and very small sections.

      logical function xp_line_fit(ia,ib,coeff,ncoeff)

      implicit none
      include 'xplot.inc'
      integer*4 ia,ib,ncoeff,i,nfit,lyai,kk
      real*8 coeff(0:99),polyval,tiny8,wt,sum,wsum,mean,meanerr
      parameter(tiny8=1.d-50)
      logical ok,lyaforest,xp_line_fit_reject

      lyai=1
      if (ze.eq.0.) print *,'WARNING: No emission redshift set.'
      lyaforest = (x(ib).lt.(ze+1.)*1215.670)

C Lower bound for points, iteratively adjusted for Ly-alpha forest region,
C and for deviant point rejection.
      do i=ia,ib
        a(i)=0.
      enddo
C Load arrays.
3     continue
      wt=0.d0
      sum=0.d0
      wsum=0.d0
      nfit=0
      do i=ia,ib
        if ( (y(i).gt.a(i)).and.((errbuf.eq.0).or.(e(i).gt.0.)) ) then
          nfit=nfit+1 
          if (errbuf.gt.0) then
            wt = 1.d0 / ( dble(e(i)) * dble(e(i)) )
            w8(nfit)= wt
          else
            wt = 1.d0
            w8(nfit) = wt
          endif
          x8(nfit) = dble(x(i))
          y8(nfit) = dble(y(i))
          sum = sum + ( wt*y8(nfit) )
          wsum = wsum + wt
        endif 
      enddo
      if (wsum.gt.tiny8) then
        mean = sum/wsum
        meanerr = dsqrt(1.d0/wsum)
      else 
        mean = 0.
        meanerr = 0.
      endif
C Fit line unless too few points or points to noisy.
      coeff(0) = 0.d0
      coeff(1) = 0.d0
      if (nfit.le.0) then
        print *,'Warning: nfit<=0, using end points of region.'
        coeff(0) = (y(ia)+y(ib))/2.
        ncoeff=1
      elseif ( (nfit.lt.14).or.((errbuf.gt.0)
     .                     .and.(mean/meanerr.lt.30.)) ) then
        coeff(0) = mean
        ncoeff=1
      else
        call poly_fit_glls(nfit,x8,y8,w8,ncoeff-1,coeff,ok)
        if (.not.ok) then
          print *,'Fit failed.'
          goto 9
        endif

C::::::::::::::::::::::::::::::::::
C temporary plotting...
        do i=ia,ib
          b(i) = sngl(polyval(ncoeff,coeff,dble(x(i))))
        enddo
        call PGQLS(kk)
        call PGSLS(4)
        call PGSCI(3)
        call PGLINE(1+ib-ia,x(ia),b(ia))
        call PGSCI(color)
        call PGSLS(kk)
C::::::::::::::::::::::::::::::::::
        
      endif

C Special fitting procedure for lyman alpha forest region.
      if (lyaforest) then
        if ((lyai.lt.3).and.(nfit.gt.7.)) then
          do i=ia,ib
            a(i)= sngl(polyval(ncoeff,coeff,dble(x(i))))
          enddo
          lyai=lyai+1
          goto 3
        endif
      endif
      if (xp_line_fit_reject(ia,ib,coeff,ncoeff,nfit,lyaforest)) goto 3

      xp_line_fit = .true.
      return
9     continue
      xp_line_fit = .false.
      return
      end

C-------------------------------------------------------------------
C Reject points more than 4 sigma from line.
C Returns .true. if any points were rejected, i.e. a(i)=1.e+30.
C
      logical function xp_line_fit_reject(ia,ib,coeff,ncoeff,
     .                                    nfit,lyaforest)

      implicit none
      include 'xplot.inc'
      integer*4 ia,ib,ncoeff,nfit,n,imax,i
      real*4 r,dmax,rms4
      real*8 coeff(0:99),polyval,rms
      logical lyaforest
         
C Default.
      xp_line_fit_reject = .false.
C Count the number of points above 0.
      n=0 
      do i=ia,ib
        if (y(i).gt.0.) n=n+1
      enddo
C Too few points left to reject any more. 
      if ((nfit.lt.7).or.(n/nfit.gt.3)) return
C Find most deviant point not yet rejected.
      imax=0
      dmax=0.
      IF (errbuf.gt.0) THEN
      do i=ia,ib
        if ((y(i).gt.a(i)).and.(e(i).gt.0.)) then
          r = sngl(polyval(ncoeff,coeff,dble(x(i))))
          if ((.not.lyaforest).or.(r.lt.y(i))) then
          if (abs(r-y(i))/e(i).gt.dmax) then
            dmax=abs(r-y(i))/e(i)
            imax=i
          endif
          endif
        endif 
      enddo
      if (dmax.gt.4.) then
        a(imax)=1.e+30   
        print *,'Rejecting point at ',x(imax),'  dmax=',dmax
        xp_line_fit_reject = .true.
      endif
      ELSE
      rms=0.d0
      do i=ia,ib
        if (y(i).gt.a(i)) then
          r  = sngl(polyval(ncoeff,coeff,dble(x(i))))
          rms= rms + dble((r-y(i))*(r-y(i)))
          n  = n+1
        endif 
      enddo
      rms = dsqrt(rms/dfloat(n))
      rms4= sngl(rms)
      do i=ia,ib
        if (y(i).gt.a(i)) then
          r = sngl(polyval(ncoeff,coeff,dble(x(i))))
          if ((.not.lyaforest).or.(r.lt.y(i))) then
          if (abs(r-y(i))/rms4.gt.dmax) then
            dmax=abs(r-y(i))/rms4
            imax=i
          endif
          endif
        endif 
      enddo
      if (dmax.gt.4.) then
        a(imax)=1.e+30   
        print *,'Rejecting point at ',x(imax),'  dmax=',dmax
        xp_line_fit_reject = .true.
      endif
      ENDIF
      return
      end

C--------------------------------------------------------------------------
C This routine is used to fit a polynomial which will be used as a continuum
C in future operations, until XPLOT is exited.

      subroutine xp_contpoly(replot)

      implicit none
      include 'xplot.inc'
      integer*4 npoly,i,j,ii,ikey,order,nearxi,dig1,dig2,i1,i2
      real*8 r8,dist
      real*4 cx,cy,r4,x1
      character key*1,c5*5
      logical replot,xp_add_section_point,ok

C Zero continuum-for-fit points.
      do i=1,np
        cff(i)=0.
      enddo

C Key commands.
      npoly = 0
      print *,
     .  ' <sp>=add section, d=delete section, q=quit, f=fill zeros,'
      print *,' 00,01,02,03,04,...,98,99 = polynomial order,'
      print *,' s=substitute continuum,  S=substitute DATA.'
      print *,
     . ' x=add artificial point,  X=change weight of artificial point.'
      print *,' z/LMB=add weight=9 pt,   Z=delete artificial point.'
      print *,' c=add previous continuum section to include in fit.'
      print *,' r=redraw data and previous continuum.'
      print *,' C=show Coeffecients (toggle flag).'
      print *,' - = fit order with old polynomial fitting routine.'
      print *,' # = fit inverse poly. with old poly. fit. routine.'

2     continue
C Redraw artificial points.
      if (apt_n.gt.0) then
        call PGSCI(3)
        do i=1,apt_n
          cx = sngl(apt_x(i))
          cy = sngl(apt_y(i))
          if (apt_w(i).gt.0.) then
            call PGPT(1,cx,cy,3)
            call xp_write_c5(nint(apt_w(i)),c5)
            call PGTEXT(cx,cy,c5)
          endif
        enddo
        call PGSCI(color)
      endif
C Get cursor value.
      call PG_CURSOR(cx,cy,key)
      ikey = ichar(key)
C Fit polynomial with two-digit order, 48-57 is digit 0 through 9 inclusive.
      if ((ikey.ge.48).and.(ikey.le.57)) then
        dig1 = ikey - 48
        print *,'Hit second digit...'
        call PG_CURSOR(cx,cy,key)
        dig2 = ichar(key) - 48
        order= (dig1*10)+dig2
        if ((order.lt.0).or.(order.gt.99)) then
          print *,' Order must be between 0 and 99. Setting to 0.'
          order=0
        endif
        call xp_LoadFitDrawPoly(order,npoly,1,ok)
        if (.not.ok) goto 9
        goto 2

      elseif (ikey.eq.45) then      !  "-" = old poly fitter.
701     print '(a,$)','Enter order : '
        read(5,*,err=701) order
        if ((order.lt.0).or.(order.gt.19)) then
          print *,' Order must be between 0 and 19. Setting to 0.'
          order=0
        endif
        call xp_LoadFitDrawPoly(order,npoly,0,ok)
        if (.not.ok) goto 9
        goto 2

      elseif (key.eq.'#') then      !  "-" = old poly fitter.
702     print '(a,$)','Inv-old: Enter order : '
        read(5,*,err=702) order
        if ((order.lt.0).or.(order.gt.19)) then
          print *,' Order must be between 0 and 19. Setting to 0.'
          order=0
        endif
        call xp_InverseLoadFitDrawPoly(order,npoly,0,ok)
        if (.not.ok) goto 9
        goto 2

      elseif (key.eq.'r') then               ! redraw data and continuum.
        call xp_redraw_data()
        goto 2

      elseif (key.eq.'C') then               ! redraw data and continuum.
        if (show_coeff) then
          show_coeff = .false.
        else
          show_coeff = .true.
        endif
        print *,'Set show_coeff = ',show_coeff
        goto 2

      elseif (key.eq.'x') then               ! add artificial point
        print '(a,$)',' Enter weight for this point (# of pixels) : '
        read(5,*,err=9) r8
        apt_n       = apt_n + 1
        apt_x(apt_n)= dble(cx)
        apt_y(apt_n)= dble(cy)
        apt_w(apt_n)= dfloat(max(0,min(9000,nint(r8))))
        print '(a,i5,f9.3,1pe11.3,i5)',' New point: n,x,y,weight=',
     .          apt_n,apt_x(apt_n),apt_y(apt_n),nint(apt_w(apt_n))
        goto 2

      elseif (key.eq.'X') then               ! Change artificial point
        dist=9.d+30
        ii=0
        do i=1,apt_n
          r8 = abs((cx-sngl(apt_x(i)))/(xmax-xmin))
     .       + abs((cy-sngl(apt_y(i)))/(ymax-ymin))
          if (r8.lt.dist) then
            dist=r8
            ii=i
          endif
        enddo
        if (ii.gt.0) then
          print '(a,$)',' Change weight of point : '
          read(5,*,err=9) r8
C Erase old point weight.
          cx = sngl(apt_x(ii))
          cy = sngl(apt_y(ii))
          call xp_write_c5(nint(apt_w(ii)),c5)
          call PGSCI(0)
          call PGPT(1,cx,cy,3)
          call PGTEXT(cx,cy,c5)
C Draw new point weight.
          apt_w(ii) = dfloat(max(0,min(9000,nint(r8))))
          if (apt_w(ii).gt.0.) then
            call xp_write_c5(nint(apt_w(ii)),c5)
            call PGTEXT(cx,cy,c5)
            call PGSCI(3)
            call PGTEXT(cx,cy,c5)
          endif
          call PGSCI(color)
        else
          print *,'No point found.'
        endif
        goto 2

      elseif ((key.eq.'z').or.(key.eq.'A')) then  ! add weight=9 point
        r8 = 9.d0
        apt_n       = apt_n + 1
        apt_x(apt_n)= dble(cx)
        apt_y(apt_n)= dble(cy)
        apt_w(apt_n)= dfloat(max(0,min(9000,nint(r8))))
        print '(a,i5,f9.3,1pe11.3,i5)',' New point: n,x,y,weight=',
     .          apt_n,apt_x(apt_n),apt_y(apt_n),nint(apt_w(apt_n))
        goto 2

      elseif (key.eq.'Z') then               ! delete artificial point
        dist=9.d+30
        ii=0
        do i=1,apt_n
          r8 = abs((cx-sngl(apt_x(i)))/(xmax-xmin))
     .       + abs((cy-sngl(apt_y(i)))/(ymax-ymin))
          if (r8.lt.dist) then
            dist=r8
            ii=i
          endif
        enddo
        if (ii.gt.0) then
          r8=0.d0
C Erase old point weight.
          cx = sngl(apt_x(ii))
          cy = sngl(apt_y(ii))
          call xp_write_c5(nint(apt_w(ii)),c5)
          call PGSCI(0)
          call PGPT(1,cx,cy,3)
          call PGTEXT(cx,cy,c5)
C Draw new point weight.
          apt_w(ii)=max(0.d0,r8)
          if (apt_w(ii).gt.0.) then
            call xp_write_c5(nint(apt_w(ii)),c5)
            call PGTEXT(cx,cy,c5)
            call PGSCI(3)
            call PGTEXT(cx,cy,c5)
          endif
          call PGSCI(color)
        else
          print *,'No point found.'
        endif
        goto 2

      elseif (key.eq.'c') then               ! add previous continuum to fit.
        print *,'Hit space bar twice to define segment.'
        call PG_CURSOR(x1,cy,key)
        i = nearxi(x1)
        call xp_mark_position(x(i),2)
        print *,'Choose second limit...'
        call PG_CURSOR(cx,cy,key)
        j = nearxi(cx)
        call xp_mark_position(x(j),2)
        i1= min(i,j)
        i2= max(i,j)
        print '(a,$)',' Enter weight per point (0-99,0=delete) : '
        read(5,*,err=9) r4
        call PGBBUF
        call PGSCI(3)
        do i=i1,i2
          cff(i)=max(0.,min(99.,r4))
          if (cff(i).gt.0.) call PGPT(1,x(i),c(i),-1)
        enddo
        call PGSCI(color)
        call PGEBUF
        goto 2

      elseif (key.eq.' ') then               ! add another section point
        if (xp_add_section_point(cx)) then
          call xp_mark_position(x(nearxi(cx)),3)
        endif
        goto 2

      elseif (key.eq.'d') then               ! delete a section
        call xp_delete_section(cx)
        goto 2

      elseif (key.eq.'q') then
        return

      elseif ((key.eq.'s').or.(key.eq.'S')) then
        call xp_substitution(key)
        replot = .true.

      elseif (key.eq.'f') then               ! fill zeros
        print *,
     .  'Hit <sp> twice to define bounds where zeros will be filled.'
        call PG_CURSOR(cx,cy,key)
        i = nearxi(cx)
        call xp_mark_position(x(i),2)
        call PG_CURSOR(cx,cy,key)
        j = nearxi(cx)
        do ii=min(i,j),max(i,j)
          if (abs(y(ii)).lt.1.e-30) y(ii) = b(ii)
        enddo
        replot   = .true.
        modified = .true.
        call ReBin()

      else
        print *,'Try again.'
        goto 2

      endif

      return
9     print *,'Error in xp_contpoly.'
      return
      end

C----------------------------------------------------------------------
      subroutine xp_redraw_data()
      implicit none
      include 'xplot.inc'
      integer*4 i,nn,ii,nearxbini,nearxi
      i = nearxbini(xmin)
      nn= nearxbini(xmax) - i
      if (hist) then
        call PG_BinBuf( nn,x_bin(i),y_bin(i),.true.)
      else
        call PG_LineBuf(nn,x_bin(i),y_bin(i))
      endif
      if (contbuf.gt.0) then
        call PGSCI(4)                               ! blue
        if (norm) then
          call PG_HLine(xmin,xmax,1.)
        else
          call PG_LineBuf(nn,x_bin(i),c_bin(i))       ! over-plot continuum
        endif
        call PGBBUF
        call PGSCI(3)
        i = nearxi(xmin)
        nn= nearxi(xmax) - i
        do ii=i,i+nn
          if (cff(ii).gt.0.) call PGPT(1,x(ii),c(ii),-1)
        enddo
        call PGSCI(color)
        call PGEBUF
      endif
      return
      end

C---------------------------------------------------------------------- 
C mode 0 = old glls routine.  mode 1 = new gj routine.
C
      subroutine xp_LoadFitDrawPoly(order,npoly,mode,ok)
C
      implicit none
      integer*4 order,npoly,mode
C
      include 'xplot.inc'
C
      real*8 coeff(0:99),xoff,polyvaloff
      integer*4 i,ii,nfit,ncoeff
      logical ok
C
      if (npoly.ne.0) then                   ! Erase old plot?
        call PGSCI(0)                        ! black
        call PG_LineBuf(npoly,x,b)
        call PGSCI(color)                    ! default
      endif
      nfit=0                                 ! Load arrays...
      do ii=1,nsect
      do i=sect(ii,1),sect(ii,2)
        nfit = nfit + 1
        x8(nfit) = dble(x(i))
        y8(nfit) = dble(y(i))
        if (abs(y(i)).lt.1.e-30) then
          w8(nfit) = 0.d0
        else
          w8(nfit) = 1.d0
        endif
      enddo
      enddo
      do i=1,apt_n
        nfit=nfit+1
        x8(nfit) = apt_x(i)
        y8(nfit) = apt_y(i)
        w8(nfit) = apt_w(i)
      enddo
      do i=1,np
        if (cff(i).gt.0.) then
          nfit=nfit+1
          x8(nfit) = dble(x(i))
          y8(nfit) = dble(c(i))
          w8(nfit) = dble(cff(i))
        endif
      enddo
      do i=0,order
        coeff(i) = 0.d0
      enddo
      ncoeff = order + 1
C Fit polynomial.
      if (mode.eq.0) then
        print '(a,i2.2,a)',' Using poly_fit_glls (order=',order,')...'
        call poly_fit_glls(nfit,x8,y8,w8,order,coeff,ok)
        xoff=0.d0
      else
        print '(a,i2.2,a)',' Using poly_fit_gj (order=',order,')...'
        call poly_fit_gj(nfit,x8,y8,w8,order,coeff,xoff,0,ok)
      endif
      if (.not.ok) then
        print *,'ERROR- Fit failed.'
        return
      endif
C Fill arrays with polynomial, spanning entire spectrum.
      npoly = np
      do i=1,npoly
        b(i)= sngl(polyvaloff(ncoeff,coeff,dble(x(i)),xoff))
      enddo
C Echo coeffecients. 
      if (show_coeff) then
        print *,'order = ',order
        print *,'xoff  = ',xoff
        do i=0,order
          print '(a,i2.2,a,1pe20.12)','coef[',i,']=',coeff(i)
        enddo
      endif
C Finally plot the polynomial.
      call PGSCI(2)                        ! red
      call PG_LineBuf(npoly,x,b)
      call PGSCI(color)                    ! default
      return
      end


C---------------------------------------------------------------------- 
C    ...INVERSE FIT...
C
C mode 0 = old glls routine.  mode 1 = new gj routine.
C
      subroutine xp_InverseLoadFitDrawPoly(order,npoly,mode,ok)
C
      implicit none
      integer*4 order,npoly,mode
C
      include 'xplot.inc'
C
      real*8 coeff(0:99),xoff,polyvaloff,high,rms
      integer*4 i,ii,nfit,ncoeff
      logical ok
C
      if (npoly.ne.0) then                   ! Erase old plot?
        call PGSCI(0)                        ! black
        call PG_LineBuf(npoly,x,b)
        call PGSCI(color)                    ! default
      endif
      nfit=0                                 ! Load arrays...
      do ii=1,nsect
      do i=sect(ii,1),sect(ii,2)
        nfit = nfit + 1
        x8(nfit) = dble(x(i))
        y8(nfit) = dble(y(i))
        if (abs(y(i)).lt.1.e-30) then
          w8(nfit) = 0.d0
        else
          w8(nfit) = 1.d0
        endif
      enddo
      enddo
      do i=1,apt_n
        nfit=nfit+1
        x8(nfit) = apt_x(i)
        y8(nfit) = apt_y(i)
        w8(nfit) = apt_w(i)
      enddo
      do i=1,np
        if (cff(i).gt.0.) then
          nfit=nfit+1
          x8(nfit) = dble(x(i))
          y8(nfit) = dble(c(i))
          w8(nfit) = dble(cff(i))
        endif
      enddo
      do i=0,order
        coeff(i) = 0.d0
      enddo
      ncoeff = order + 1

C Fit inverse polynomial.
      print *,'Doing inverse polynomial fit...'
      if (mode.eq.0) then
        print '(a,i2.2,a)',' Using poly_fit_glls (order=',order,')...'
        call poly_fit_glls(nfit,y8,x8,w8,order,coeff,ok)
        xoff=0.d0
        call poly_fit_glls_residuals(nfit,y8,x8,w8,order,
     .                               coeff,high,rms) 
        print '(a,f9.5,a,f9.5))',
     .        'fit residuals: high=',high,'  rms=',rms
      else
        print '(a,i2.2,a)',' Using poly_fit_gj (order=',order,')...'
        call poly_fit_gj(nfit,y8,x8,w8,order,coeff,xoff,0,ok)
      endif
      if (.not.ok) then
        print *,'ERROR- Fit failed.'
        return
      endif

C Echo coeffecients. 
      if (show_coeff) then
        print *,'order = ',order
        print *,'xoff  = ',xoff
        do i=0,order
          print '(a,i2.2,a,1pe20.12)','coef[',i,']=',coeff(i)
        enddo
      endif

C Fill arrays with polynomial, spanning entire spectrum.
      print *,'WARNING: this may mess up continuum buffer.'
      npoly = nfit
      do i=1,npoly
        b(i) = sngl(y8(i))
        c(i) = sngl(polyvaloff(ncoeff,coeff,y8(i),xoff))
C       print '(2f11.3)',c(i),b(i)
      enddo

C Finally plot the polynomial.
      call PGSCI(2)                        ! red
      call PG_LineBuf(npoly,c,b)
      call PGSCI(color)                    ! default
C
      return
      end


C----------------------------------------------------------------------
      subroutine xp_write_c5(i,c5) 
      implicit none
      integer*4 i
      character c5*5
      if (i.lt.10.) then
        write(c5,'(i1,3x)') max(0,i)
      elseif (i.lt.100.) then
        write(c5,'(i2,2x)') i
      elseif (i.lt.1000.) then
        write(c5,'(i3,1x)') i
      else
        write(c5,'(i4)') min(9999,i)
      endif
      return
      end

C--------------------------------------------------------------------------
C This routine is used to fit various special functions such as a Gaussian,
C Lorentzian, and Power Law.
C
      subroutine xp_fitfunk(replot)
C
      implicit none
      include 'xplot.inc'
      integer*4 i,k,ii,nearxi,ia,ib,kk
      real*8 peak,hwhm,xcenter,r8,mln2,aa,bb,offset
      real*8 peak2,hwhm2,xcenter2
      parameter(mln2=-0.693147181d0)
      real*4 cx,cy,r1,r2
      character*1 key
      logical replot,curve,overplot
      logical fit_gauss, fit_gauss_fc, fit_dgauss_fc, fit_lorentz
      logical fit_bbody, fit_power, fit_bbody_usertemp
      logical xp_add_section_point

      curve = .false.
      ia = nearxi(xmin)
      ib = nearxi(xmax)

C Zero b array which is used to store current fit.
      do i=1,np
        b(i)=0.
      enddo

1     continue

C              ----+----1----+----2----+----3----+----4
      print *,'<sp>= add a section.'
      print *,' d = delete a section.'
      print *,' s = Substitute continuum.'
      print *,' S = Substitute data spectrum.'
      print *,' g = Gaussian: y = A exp(-ln(2)*((x-xc)/HWHM)^2).'
      print *,' G = Fixed center Gaussian.'
      print *,' l = Lorentzian: y = A / ( 1 + ((x-xc)/HWHM)^2 ).'
      print *,' t = Two Gaussians with fixed centers.'
      print *,' b = Black body: y = (A/x^5) / (exp(B/x)-1).'
      print *,' B = Black body (user specified temperature).'
      print *,' p = Power law: y = A*x^b .'
      print *,' q = Quit,  ? = Help.'
      
2     continue

C Get key stroke.
      call PG_CURSOR(cx,cy,key)
      if (key.eq.' ') then                   ! add another section point
        if (xp_add_section_point(cx)) then
          call xp_mark_position(x(nearxi(cx)),3)
        endif
        goto 2

      elseif (key.eq.'d') then               ! delete a section
        call xp_delete_section(cx)
        goto 2

      elseif (key.eq.'q') then
        return

      elseif (key.eq.'q') then
        goto 8

      elseif ((key.eq.'?').or.(key.eq.'/')) then
        goto 1

      elseif ((key.eq.'s').or.(key.eq.'S')) then
        if (.not.curve) then
          print *,'You need to fit a curve first.'
          goto 1
        endif
        call xp_substitution(key)
        replot = .true.
        print *,'ok.'
        goto 8

      endif

C Some curves require a normalized spectrum.
      if ( (key.eq.'t').or.(key.eq.'G').or.
     .                     (key.eq.'g').or.(key.eq.'l') ) then
        if (.not.norm) then
          print *,'Error- Must be a normalized spectrum.'
          print '(a,$)',' Is this a normalized spectrum? (1/0) : '
          read(5,*,err=1) i
          if (i.eq.1) then
            norm=.true.
          else
            goto 1
          endif
        endif
        offset = 1.d0
      else
        offset = 0.d0
      endif

C Load real*8 arrays.
      k=0
      do ii=1,nsect
        do i=sect(ii,1),sect(ii,2)
          if (abs(y(i)).gt.1.e-30) then
            k=k+1
            x8(k) = dble(x(i))
            y8(k) = dble(y(i)) - offset
            if (errbuf.gt.0.) then
              if (e(i).gt.1.e-30) then
                w8(k) = 1.d0/(dble(e(i))*dble(e(i)))
              else
                w8(k) = 0.d0
              endif
            else
              w8(k) = 1.d0
            endif
          endif
        enddo
      enddo

C Reset all possible parameters.
      aa      = 0.d0
      bb      = 0.d0
      peak    = 0.d0
      hwhm    = 0.d0
      xcenter = 0.d0
      peak2   = 0.d0
      hwhm2   = 0.d0
      xcenter2= 0.d0

C Erase the old curve- replace with dotted gray curve.
      if (curve) then
        ii = 1+ib-ia
        call PGSCI(0)                    ! black
        call PG_LineBuf(ii,x(ia),b(ia))
        call PGSCI(15)                   ! gray
        call PGQLS(kk)
        call PGSLS(4)                    ! dotted
        call PG_LineBuf(ii,x(ia),b(ia))
        call PGSLS(kk)
        call PGSCI(color)                ! default
      endif

C Execute curve fitting.
      if (key.eq.'g') then
        if (.not.fit_gauss(k,peak,hwhm,xcenter)) goto 9
        do i=1,np
          b(i) = 1.
          r8 = dabs((dble(x(i))-xcenter)/hwhm)
          if (r8.lt.15.) b(i) = 1. + sngl(peak*dexp(mln2*r8*r8))
        enddo
        overplot=.true. 

      elseif (key.eq.'G') then
        print '(a,$)',' Enter X center value : '
        read(5,*,err=9) xcenter
        if (.not.fit_gauss_fc(k,peak,hwhm,xcenter)) goto 9
        do i=1,np
          b(i) = 1.
          r8 = dabs((dble(x(i))-xcenter)/hwhm)
          if (r8.lt.15.) b(i) = 1. + sngl(peak*dexp(mln2*r8*r8))
        enddo
        overplot=.true. 

      elseif (key.eq.'t') then
        print '(a,$)',' Enter two X center values : '
        read(5,*,err=9) xcenter,xcenter2
        if (.not.fit_dgauss_fc(k,peak,hwhm,xcenter,
     .                         peak2,hwhm2,xcenter2)) goto 9
        do i=1,np
          r1 = 0.
          r8 = dabs((dble(x(i))-xcenter)/hwhm)
          if (r8.lt.15.) r1 = sngl(peak*dexp(mln2*r8*r8))
          r2 = 0.
          r8 = dabs((dble(x(i))-xcenter2)/hwhm2)
          if (r8.lt.15.) r2 = sngl(peak2*dexp(mln2*r8*r8))
          b(i) = 1. + r1 + r2
        enddo
        overplot=.true. 

      elseif (key.eq.'l') then
        if (.not.fit_lorentz(k,peak,hwhm,xcenter)) goto 9
        do i=1,np
          r8 = (dble(x(i))-xcenter)/hwhm
          b(i) = 1. + sngl(peak/(1.d0 + r8*r8))
        enddo
        overplot=.true.

      elseif (key.eq.'B') then
        if (.not.fit_bbody_usertemp(k,aa,bb)) goto 9
        do i=1,np
          r8 = (aa/dble(x(i))**5.d0)
          b(i) = r8 / ( dexp(bb/dble(x(i))) - 1.d0 )
        enddo
        overplot=.true.

      elseif (key.eq.'b') then
        if (.not.fit_bbody(k,aa,bb)) goto 9
        do i=1,np
          r8 = (aa/dble(x(i))**5.d0)
          b(i) = r8 / ( dexp(bb/dble(x(i))) - 1.d0 )
        enddo
        overplot=.true.

      elseif (key.eq.'p') then
        if (.not.fit_power(k,aa,bb)) goto 9
        do i=1,np
          b(i) = sngl(aa*(dble(x(i))**bb))
        enddo
        overplot=.true.

      else
        print *,'Try again.  "?" for help.'
        goto 2

      endif

C Overplot the new curve.
      if (overplot) then
        call PGSCI(2)                    ! red
        ii = 1+ib-ia
        call PG_LineBuf(ii,x(ia),b(ia))
        call PGSCI(color)                ! default
        curve = .true.
      endif

C Try another curve?
      goto 2

8     continue
      return
9     print *,'Fit failed- error in xp_fitfunk.'
      return
      end


C-------------------------------------------------------------------
      logical function figdispError(file)
      implicit none
      character*(*) file
      integer*4 type,dummy,gropen,ident,i
      type = 0
      dummy= 0
      i = gropen(type,dummy,file,ident) 
      if (i.eq.1) then
        figdispError = .false.
        call grclos
      else
        figdispError = .true.
        print *,
     .  '::::::::::::::::::: WARNING :::::::::::::::::::::::::::::'
        print *,
     .  '  The PGPLOT figdisp window was not properly opened.'
        print *,
     .  '  Start figdisp with the command "pgdisp".  If a window'
        print *,
     .  '  popped up, quit the window, and use "pgdisp" to start'
        print *,
     .  '  the window properly.'
        print *,
     .  ':::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
      endif
      return
      end


C--------------------------------------------------------------------------
C This routine smooths the spectrum or continuum with a boxcar smoothing
C function.

      subroutine xp_boxcar(replot)

      implicit none
      include 'xplot.inc'
      integer*4 i,ii,n,bc,j
      character key*1, dum*20
      real*4 cx,cy,valread
      logical replot

C User can select to smooth the continuum or the spectrum.
      print *,
     .  'Hit key: (c=continuum smooth, s=spectrum smooth, q=quit).'
      call PG_CURSOR(cx,cy,key)
      if (key.eq.'q') goto 8 
C User can select size of boxcar.
      print '(a,$)',
     .  ' Size of boxcar (in pixels) to use for smoothing (7) : '
      dum=' '
      read(5,'(a)',err=9) dum
      if (dum.eq.' ') then
        bc=3
      else
        bc = max(0,min(99,nint(valread(dum))))
        if (bc.lt.3) goto 9
        bc = bc/2
      endif
      print '(a,i3,a)',
     .  ' Using boxcar with a width of',2*bc+1,' pixels.'
C Do the smoothing, ignore pixels of zero value.
      if (key.eq.'c') then                 ! smooth continuum
        do ii=1,nsect
          do i=sect(ii,1),sect(ii,2)
            a(i)=0. 
            n   =0
            do j=max(1,min(np,i-bc)),max(1,min(np,i+bc))
              if (c(j).ne.0.) then
                n=n+1
                a(i)=a(i)+c(j)
              endif
            enddo
            a(i)=a(i)/float(n)
          enddo
        enddo
        do ii=1,nsect
          do i=sect(ii,1),sect(ii,2)
            c(i)=a(i)
          enddo
        enddo
        replot   = .true.
      elseif (key.eq.'s') then             ! smooth spectrum
        do ii=1,nsect
          do i=sect(ii,1),sect(ii,2)
            a(i)=0. 
            n   =0
            do j=max(1,min(np,i-bc)),max(1,min(np,i+bc))
              if (y(j).ne.0.) then
                n=n+1
                a(i)=a(i)+y(j)
              endif
            enddo
            a(i)=a(i)/float(n)
          enddo
        enddo
        do ii=1,nsect
          do i=sect(ii,1),sect(ii,2)
            y(i)=a(i)
          enddo
        enddo
        modified = .true.
        replot   = .true.
        call ReBin()
      endif
8     print *,'ok.'
      return
9     print *,'Error in xp_boxcar.'
      return
      end


C-----------------------------------------------------------------
C Substitutes either data (key='S') or continuum (key='s') with
C temporary b() array.

      subroutine xp_substitution(inkey)

      implicit none
      include 'xplot.inc'
      character inkey*1,key*1
      integer*4 ii,i,j,nearxi
      real*4 cx,cy

      if (inkey.eq.'s') then
        print *,
     .  'Hit <sp> twice to define region for CONTINUUM replacement,'
      else
        print *,
     .  'Hit <sp> twice to define region for DATA replacement,'
      endif
      print *,'or just "w" for the whole spectrum.'
      call PG_CURSOR(cx,cy,key)
      if (key.eq.'w') then
        i=1
        j=np
      else
        i = nearxi(cx)
        call xp_mark_position(x(i),2)
        call PG_CURSOR(cx,cy,key)
        j = nearxi(cx)
      endif
      call PGSCI(5)               ! Show section in turquoise.
      ii = 1+abs(i-j)
      call PG_LineBuf(ii,x(min(i,j)),b(min(i,j)))
      call PGSCI(color)
      if (inkey.eq.'s') then
        do ii=min(i,j),max(i,j)
          c(ii) = b(ii)
        enddo
        contbuf = 1
      else
        do ii=min(i,j),max(i,j)
          y(ii) = b(ii)
        enddo
        call ReBin()
      endif
      modified = .true.
      return
      end

C-----------------------------------------------------------------
C Take out a "kink" by fitting a polynomial on either side and replacing
C kink region with fit. "k" is the array element number in x(),y() of position
C of kink.
C
      subroutine xp_kinkfit(k,replot)

      implicit none
      include 'xplot.inc'
      integer*4 k,i,n
      integer*4 ncoeff,i1,i2
      character key*1
      real*4 cx,cy,s
      real*8 coeff(0:99),r8,polyval
      logical replot,ok
      integer*4 inner,outer,order

C Default.
      replot= .false.
      inner = 10
      outer = 40
      order = 4

1     continue

C Fill array for fit.
      n=0
      do i=max(1,k-outer),max(1,k-inner)
        n=n+1
        x8(n) = dble(x(i))
        y8(n) = dble(y(i))
        w8(n) = 1.d0
      enddo
      do i=min(np,k+inner),min(np,k+outer)
        n=n+1
        x8(n) = dble(x(i))
        y8(n) = dble(y(i))
        w8(n) = 1.d0
      enddo

C Fit order-th order polynomial.
      ncoeff = order + 1
      call poly_fit_glls(n,x8,y8,w8,order,coeff,ok)
      if (.not.ok) then
        print *,'Fit failed.'
        return
      endif

C Fill b array with polynomial.
      do i=1,np
        r8  = dble(x(i))
        b(i)= sngl(polyval(ncoeff,coeff,r8))
      enddo

C Plot the polynomial.
      call PGSCI(2)                        ! red
      call PG_LineBuf(np,x,b)
      call PGSCI(color)                    ! default

C Determine replacement region.
      s = y(k-2)-b(k-2) 
      if (abs(s).lt.1.e-30) then
        print *,'Error-- fit too close to kink.'
        return
      endif
      i1=k-3
      do while((i1.gt.max(1,k-outer)).and.((y(i1)-b(i1))/s.gt.0.))
        i1=i1-1
      enddo
      s = y(k+2)-b(k+2) 
      if (abs(s).lt.1.e-30) then
        print *,'Error-- fit too close to kink.'
        return
      endif
      i2=k+3
      do while((i2.lt.min(np,k+outer)).and.((y(i2)-b(i2))/s.gt.0.))
        i2=i2+1
      enddo
      
C Set b() to zero outside replacement region.
      do i=1,np
        if (i.lt.i1) then
          b(i)=0.
        elseif (i.gt.i2) then
          b(i)=0.
        endif
      enddo
 
C Plot the replacement polynomial.
      call PGSCI(5)                        ! turquoise
      call PG_LineBuf(np,x,b)
      call PGSCI(color)                    ! default

C Ok?
      print *,
     .'Is this ok? ("a"=abort,"u"=user fit range,"any other key"=yes)'
      call PG_CURSOR(cx,cy,key)
      if (key.eq.'a') then
        print *,'Aborting kink fit -- no replacement done.'
        return
      elseif (key.eq.'u') then
        print '(a,$)','Enter inner distance (10) : '
        read(5,*,err=900) inner
        if (inner.lt.0) goto 900
        print '(a,$)','Enter outer distance (40) : '
        read(5,*,err=900) outer
        if (outer.lt.inner+3) goto 900
        print '(a,$)','    Enter polynomial ( 4) : '
        read(5,*,err=900) order
        if ((order.lt.0).or.(order.gt.8)) goto 900
        goto 1
      endif

C Replace spectrum.
      do i=i1,i2
        y(i) = b(i)
      enddo
      replot = .true.
      return

900   print *,'Error in entered values.'
      return
      end

C----------------------------------------------------------------------
      subroutine xp_user_commands(userfile)
      implicit none
      character*80 userfile
      integer*4 lc,i,ci,lw,ls,if
      character side*2,text*80,com*80
      real*4 disp,coord,fjust,angle,size,x,y,x1,x2,y1,y2
      real*4 xleft,xright,ybot,ytop

      open(1,file=userfile,status='old',iostat=i)
      if (i.ne.0) then
        print '(2a)',' Error opening : ',userfile(1:lc(userfile))
        return
      endif
1     continue
      read(1,'(a)',end=5) com

      if (com(1:1).eq.'#') then
        continue

      elseif (com(1:5).eq.'PGSVP') then
        read(1,*,end=8) xleft
        read(1,*,end=8) xright
        read(1,*,end=8) ybot
        read(1,*,end=8) ytop
        call PGSVP(xleft,xright,ybot,ytop)

      elseif (com(1:7).eq.'PGMTEXT') then
        read(1,'(a)',end=8) side
        read(1,*,end=8) disp
        read(1,*,end=8) coord
        read(1,*,end=8) fjust
        read(1,'(a)',end=8) text
        call PGMTEXT(side,disp,coord,fjust,text)

      elseif (com(1:7).eq.'PG_MARK') then
        read(1,*,end=8) x
        read(1,*,end=8) y1
        read(1,*,end=8) y2
        call PG_MARK(x,y1,y2)

      elseif (com(1:8).eq.'PG_HLINE') then
        read(1,*,end=8) x1
        read(1,*,end=8) x2
        read(1,*,end=8) y
        call PG_HLINE(x1,x2,y)

      elseif (com(1:5).eq.'PGSCF') then
        read(1,*,end=8) if
        call PGSCF(if)

      elseif (com(1:5).eq.'PGSLS') then
        read(1,*,end=8) ls
        call PGSLS(ls)

      elseif (com(1:5).eq.'PGSLW') then
        read(1,*,end=8) lw
        call PGSLW(lw)

      elseif (com(1:5).eq.'PGSCI') then
        read(1,*,end=8) ci
        call PGSCI(ci)

      elseif (com(1:5).eq.'PGSCH') then
        read(1,*,end=8) size
        call PGSCH(size)

      elseif (com(1:6).eq.'PGPTXT') then
        read(1,*,end=8) x
        read(1,*,end=8) y
        read(1,*,end=8) angle
        read(1,*,end=8) fjust
        read(1,'(a)',end=8) text
        call PGPTXT(x,y,angle,fjust,text)

      else
        print '(2a)',' Error-- Unknown command : ',com(1:lc(com))
        return

      endif
      goto 1

5     continue
      close(1)
      return

8     continue
      print *,'Error reading command options for: ',com(1:lc(com))
      return

      end

C----------------------------------------------------------------------
      subroutine xp_LoadAtmAbsLines()
      implicit none
      include 'xplot.inc'
      real*4 r1,r2,r3,r4,r5
      integer*4 i,lc
      open(33,file=aafile,status='old',iostat=i)
      if (i.ne.0) goto 805
      naal=0
11    read(33,*,end=55) r1,r2,r3,r4,r5
      naal=naal+1
      aablu(naal)= r1
      aared(naal)= r2
      aarsi(naal)= r3
      goto 11
55    close(33)
C Avoid unnecessary overlap.
      do i=1,naal-1
        if (aared(i).gt.aablu(i+1)) then
          r5 = (aared(i)+aablu(i+1))/2.
          aared(i)  = r5-0.01
          aablu(i+1)= r5+0.01
        endif
      enddo
      return
805   continue
      print '(2a)',' Error opening or reading : ',aafile(1:lc(aafile))
      close(33)
      return
      end
C----------------------------------------------------------------------
      logical function xp_AtmAbsExist(bw,rw)
      implicit none
      real*4 bw,rw
      include 'xplot.inc'
      logical ok
      integer*4 i
      xp_AtmAbsExist = .false.
      if (naal.eq.0) call xp_LoadAtmAbsLines()
      if (naal.eq.0) return
      ok=.false.
      i=1
      do while ((i.le.naal).and.(.not.ok))
        if ((aablu(i).gt.bw).and.(aablu(i).lt.rw)) then
          ok=.true.
        elseif ((aared(i).gt.bw).and.(aared(i).lt.rw)) then
          ok=.true.
        elseif ((aablu(i).lt.bw).and.(aared(i).gt.rw)) then
          ok=.true.
        endif
        i=i+1
      enddo
      xp_AtmAbsExist = ok
      return
      end
C----------------------------------------------------------------------
      subroutine xp_ShowAtmAbsLines()
      implicit none
      include 'xplot.inc'
      integer*4 i,k
      real*4 r1,r2,y1,y2
      real*8 hvsf
      if (naal.eq.0) call xp_LoadAtmAbsLines()
      if (naal.eq.0) return
      call get_hvsf(header,hvsf)
      call PGQCI(k)
      call PGSCI(11)
      y1 = ymin + (0.1*(ymax-ymin))
      y2 = ymax - (0.1*(ymax-ymin))
      do i=1,naal
        r1 = aablu(i) * sngl(hvsf)
        r2 = aared(i) * sngl(hvsf)
        if ((r2.gt.xmin).and.(r1.lt.xmax)) then
          call PG_Line(r1,r2,y1,y1)
          call PG_Line(r1,r2,y2,y2)
          call PG_Line(r1,r1,y1,y2)
          call PG_Line(r2,r2,y1,y2)
          call PG_Line(r1,r2,y1,y2)
          call PG_Line(r2,r1,y1,y2)
        endif
      enddo
      call PGSCI(k)
      return
      end


C----------------------------------------------------------------------
C Read 2-D HIRES reduced spectra file.  Load xplot.inc common block variables.
C Load common variables: xo2d,y2d,yo2d,temphead (or e2d or c2d.)
C mode=0 : read main data file.
C mode=1 : read error file.
C mode=2 : read continuum file.
C
      subroutine xp_readfits2D(file,ok,mode)
C
      implicit none
      character*(*) file
      logical ok
      integer*4 mode
C
      include 'xplot.inc'
C
      integer*4 sc,ec,sr,er,nc,nr,i,j,k
      character c7*7,c8*8
      real*8 GetSpimWave,crval1,cdelt1,crpix1,fhead,r8
      logical card_exist
C
C Read file.
      call readfits(file,a,maxpt,temphead,ok)
      if (.not.ok) return
      call GetDimensions(temphead,sc,ec,sr,er,nc,nr)
      if (nr.lt.2) then
        print *,' Error-- data file is not 2-dimensional.'
        ok=.false.
        return
      endif
C Error spectrum.
      if (mode.eq.1) then
        do j=sr,er
          do i=sc,ec
            k = ((j-sr)*nc) + (1+i-sc)
            e2d(1+i-sc,j) = a(k)
          enddo
        enddo
C Continuum spectrum.
      elseif (mode.eq.2) then
        do j=sr,er
          do i=sc,ec
            k = ((j-sr)*nc) + (1+i-sc)
            c2d(1+i-sc,j) = a(k)
          enddo
        enddo
C Data spectrum... Copy a() into y2d and yo2d...
      elseif (mode.eq.0) then
        do j=sr,er
          do i=sc,ec
            k = ((j-sr)*nc) + (1+i-sc)
            y2d(1+i-sc,j) = a(k)
            yo2d(1+i-sc,j)= a(k)
          enddo
        enddo
      else
        print *,' Error with mode in xp_readfits2D.'
        call exit(1)
      endif
C
C Read x scale.
      IF (mode.eq.0) THEN
C
C Is there a polynomial wavelength scale?
      write(c7,'(a,i2.2)') 'WV_0_',sr
      write(c8,'(a,i2.2)') 'CRVL1_',sr
      if ( .not.card_exist(c7,temphead) .and.
     .     .not.card_exist(c8,temphead) ) then
        print *,'Loading pixel scale.'
C Load xo2d() array.
        crval1 = 1.
        crpix1 = 1.
        cdelt1 = 1.
        if (card_exist('CRVAL1',temphead)) then
          crval1 = fhead('CRVAL1',temphead)
        endif
        if (card_exist('CRPIX1',temphead)) then
          crpix1 = fhead('CRPIX1',temphead)
        endif
        if (card_exist('CDELT1',temphead)) then
          cdelt1 = fhead('CDELT1',temphead)
        endif
        do j=sr,er
          do i=sc,ec
            xo2d(1+i-sc,j)=(cdelt1*(dfloat(1+i-sc)-crpix1))+crval1 
          enddo
        enddo
      else
        print *,'Loading polynomial wavelength scale.'
C Initialize wavelength scale parameters.
        r8 = GetSpimWave(temphead,0.d0,0)
        if (r8.lt.0.) goto 901
C Load xo2d() array.
        do j=sr,er
          do i=sc,ec
            xo2d(1+i-sc,j)= GetSpimWave(temphead,dfloat(i),j)
            if (xo2d(1+i-sc,j).lt.0.) goto 901
          enddo
        enddo
      endif
C
      ENDIF
C
      return
901   print *,'Error reading 2D spectrum wavelength scale.'
      ok=.false.
      return
      end



C----------------------------------------------------------------------
C Read 2-D "also" spectrum file.  Load xplot.inc common block variables.
C Load common variables: x2d_also, y2d_also, and temphead.
C Increment "num_also".
C
      subroutine xp_readfits2D_also(file,ok)
C
      implicit none
      character*(*) file
      logical ok
C
      include 'xplot.inc'
C
      integer*4 sc,ec,sr,er,nc,nr,i,j,k
      character c7*7,c8*8
      real*8 GetSpimWave,crval1,cdelt1,crpix1,fhead,r8
      logical card_exist
C Read file.
      call readfits(file,a,maxpt,temphead,ok)
      if (.not.ok) return
      call GetDimensions(temphead,sc,ec,sr,er,nc,nr)
C Set number of points.
      n_also(num_also) = nc
      if (nr.lt.2) then
        print *,' Error-- data file is not 2-dimensional.'
        call exit(0)
      endif
C Data spectrum... Copy a() into y2d_also...
      do j=sr,er
        do i=sc,ec
          k = ((j-sr)*nc) + (1+i-sc)
          y2d_also(1+i-sc,j,num_also) = a(k)
        enddo
      enddo
C
C Read x scale.
C
C Is there a polynomial wavelength scale?
      write(c7,'(a,i2.2)') 'WV_0_',sr
      write(c8,'(a,i2.2)') 'CRVL1_',sr
C
C ... pixel scale ...
      if ( .not.card_exist(c7,temphead) .and.
     .     .not.card_exist(c8,temphead) ) then
C Load xo2d() array.
        crval1 = 1.
        crpix1 = 1.
        cdelt1 = 1.
        if (card_exist('CRVAL1',temphead)) then
          crval1 = fhead('CRVAL1',temphead)
        endif
        if (card_exist('CRPIX1',temphead)) then
          crpix1 = fhead('CRPIX1',temphead)
        endif
        if (card_exist('CDELT1',temphead)) then
          cdelt1 = fhead('CDELT1',temphead)
        endif
        do j=sr,er
          do i=sc,ec
            x2d_also(1+i-sc,j,num_also) = 
     .            (cdelt1 * (dfloat(1+i-sc)-crpix1)) + crval1 
          enddo
        enddo
C
C ... polynomial scale ...
      else
C Initialize wavelength scale parameters.
        r8 = GetSpimWave(temphead,0.d0,0)
        if (r8.lt.0.) goto 901
C Load xo2d() array.
        do j=sr,er
          do i=sc,ec
            r8 = GetSpimWave(temphead,dfloat(i),j)
            if (r8.lt.0.) goto 901
            x2d_also(1+i-sc,j,num_also) = r8
          enddo
        enddo
      endif
C
      return
901   print *,'Error reading 2D spectrum wavelength scale.'
      call exit(0)
      return
      end


C----------------------------------------------------------------------
C Set new row number for 2D spectra.
C
      subroutine xp_SetNewRow(row)
      implicit none
      integer*4 row
      include 'xplot.inc'
      integer*4 sc,ec,sr,er,nc,nr,i,j,ii,kk,numpoints
C Check.
      if (row2d.eq.0) then
        print *,'Error- This is not a 2D file.'
        return
      endif
C Get dimensions.
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
      numpoints = nc
C Check for 2D data.
      if (nr.lt.2) then
        print *,'Error- This is not a 2D file.'
        return
      endif
C Save y-data from previous row (which may have changed.)
      do i=1,numpoints
        y2d(i,row2d) = y(i)
        e2d(i,row2d) = e(i)
        c2d(i,row2d) = c(i)
      enddo
C Check and set new row number.
      j = row
      if (j.gt.er) then
        print *,' Highest row is',er
        j=er
      elseif (j.lt.sr) then
        print *,' Lowest row is',sr
        j=sr
      endif
      row2d = j
C Swap in appropriate arrays.
      do i=1,numpoints
        xo(i)= xo2d(i,row2d)
        x(i) = sngl(xo(i))
        y(i) = y2d(i,row2d)
        yo(i)= yo2d(i,row2d)
        e(i) = e2d(i,row2d)
        if (norm) then
          c(i) = 1.
        else
          c(i) = c2d(i,row2d)
        endif
      enddo
C Set x range.
      if ((x(1).lt.xmin).and.(x(numpoints).gt.xmax)) then
        print *,'Maintaining x range.'
      else
        xmin = x(1) -((x(numpoints)-x(1))*0.01)
        xmax = x(numpoints)+((x(numpoints)-x(1))*0.01)
      endif
C Set "also" buffers.
      if (num_also.gt.0) then
        do kk=1,num_also
          do ii=1,n_also(kk)
            x_also(ii,kk) = x2d_also(ii,row2d,kk)
            y_also(ii,kk) = y2d_also(ii,row2d,kk)
          enddo
        enddo
      endif
C
      return
      end

C----------------------------------------------------------------------
C Write 2-D HIRES reduced spectra file.  Load xplot.inc common block variables.
C Use common variables: xo2d,y2d,yo2d,temphead (or e2d or c2d.)
C mode=0 : write main data file.
C mode=1 : write error file.
C mode=2 : write continuum file.
C
      subroutine xp_writefits2D(file,ok,mode)
      implicit none
      character*(*) file
      logical ok
      integer*4 mode
      include 'xplot.inc'
      integer*4 sc,ec,sr,er,nc,nr,i,j,k,lc
C Save y-data from last viewed row (which may have changed.)
      call GetDimensions(temphead,sc,ec,sr,er,nc,nr)
      do i=1,nc
        y2d(i,row2d) = y(i)
        e2d(i,row2d) = e(i)
        c2d(i,row2d) = c(i)
      enddo
C Error spectrum.
      if (mode.eq.1) then
        do j=sr,er
          do i=sc,ec
            k = ((j-sr)*nc) + (1+i-sc)
            a(k) = e2d(1+i-sc,j)
          enddo
        enddo
C Continuum spectrum.
      elseif (mode.eq.2) then
        do j=sr,er
          do i=sc,ec
            k = ((j-sr)*nc) + (1+i-sc)
            a(k) = c2d(1+i-sc,j)
          enddo
        enddo
C Data spectrum.
      elseif (mode.eq.0) then
        do j=sr,er
          do i=sc,ec
            k = ((j-sr)*nc) + (1+i-sc)
            a(k) = y2d(1+i-sc,j)
          enddo
        enddo
      else
        print *,' Error with mode in xp_writefits2D.'
        call exit(1)
      endif
C Write file.
      call writefits(file,a,temphead,ok)
      if (.not.ok) then
        print '(2a)',' Error writing file: ',file(1:lc(file))
      endif
      return
      end

C----------------------------------------------------------------------
      subroutine ReBin()
      implicit none
      include 'xplot.inc'
      integer*4 i,ii
C (Re)load binned arrays.
      np_bin=np
      do i=1,np
        x_bin(i) = x(i)
        y_bin(i) = y(i)
        e_bin(i) = e(i)
        c_bin(i) = c(i)
      enddo
C Bin if necessary.
      if (bin.gt.1) then
        i=np
        call DoBin4(bin,i,x_bin,0)
        np_bin=i
        i=np
        call DoBin4(bin,i,y_bin,0)
        i=np
        if (errbuf.gt.0) call DoBin4(bin,i,e_bin,1)
        i=np
        if (contbuf.gt.0) call DoBin4(bin,i,c_bin,0)
      endif
C Re-bin other "also" arrays if necessary.
      do ii=1,num_also
        n_also_bin(ii)=n_also(ii)
        do i=1,n_also(ii)
          x_also_bin(i,ii) = x_also(i,ii)
          y_also_bin(i,ii) = y_also(i,ii)
        enddo
        if (bin.gt.1) then
          i=n_also(ii)
          call DoBin4(bin,i,x_also_bin(1,ii),0)
          n_also_bin(ii)=i
          i=n_also(ii)
          call DoBin4(bin,i,y_also_bin(1,ii),0)
        endif
      enddo
      return
      end

C----------------------------------------------------------------------
C Free-hand continuum "fitting"...
      subroutine xp_FreeFit(replot)
      implicit none
      logical replot
      include 'xplot.inc'
      integer*4 i,i1,i2,nearxi,nn
      real*4 cx,cy,cx0,cy0,MM,BB
      character key*1
      logical first
C
      common /XPFREEFITBLK/ cx0,cy0
C
C Initialize b array.
      i1 = nearxi(xmin)
      i2 = nearxi(xmax)
      do i=i1,i2
        b(i)=0.
      enddo
      first=.true.
      replot=.false.
C Start free hand drawing.
1     continue
      print *,'Hit key:  <sp>=segment point, "r"=restart segments,'
      print *,
     .  '"s"=substitute continuum, "S"=substitute data, "q"=quit.'
2     continue
      call PG_CURSOR(cx,cy,key)
      if (key.eq.'q') then
        print *,'Returning to main menu.'
        return
      elseif (key.eq.'r') then
        first=.true.
        goto 1
      elseif ((key.eq.'s').or.(key.eq.'S')) then
        call xp_substitution(key)
        replot=.true.
        print *,'Returning to main menu.'
        return
      elseif (key.eq.' ') then
        if (first) then
          print *,'Set first point.'
          cx0 = cx
          cy0 = cy
          first=.false.
          goto 2
        endif
C Load b array.
        MM = (cy0-cy)/(cx0-cx)
        BB = cy - (MM*cx)
        i1 = nearxi(cx0)
        i2 = nearxi(cx)
        do i=i1,i2
          b(i) = MM*x(i) + BB
        enddo
C Draw segment.
        call PGSCI(8)            ! orange
        nn = 1 + i2 - i1
        call PGLINE(nn,x(i1),b(i1))
        call PGSCI(color)
        cx0 = cx
        cy0 = cy
        goto 2
      else
        goto 1
      endif
      end

C----------------------------------------------------------------------
C Median, rejection, average fitting method.
C
      subroutine xp_MedianFit(replot)
C
      implicit none
      logical replot
      include 'xplot.inc'
      integer*4 i,j,i1,i2,nearxi,rad,k1,k2,k,nn
      integer*4 boxrad,ncheck,Def_ncheck
      real*4 cx,cy,cx2,rr,siglim,valread
      character key*1, s*20
C Initialize a and b array.
      i1 = nearxi(xmin)
      i2 = nearxi(xmax)
C b array stores the "fit".
      do i=i1,i2
        b(i)=0.
      enddo
C a array tells which points to include and reject.
      do i=i1,i2
        if ((errbuf.gt.0).and.(e(i).lt.0.)) then
          a(i)=-1.
        else
          a(i)=+1.
        endif
      enddo
      replot=.false.
C Set radius for median box.
      print '(a,$)',' Enter radius in pixels [50] : '
      read(5,'(a)') s
      if (s.eq.' ') then
        rad=30
      else
        rad=nint(valread(s))
      endif
      rad = max(1,min(199,rad))
      print *,' Radius=',rad
C Set smoothing box radius.
      print '(a,$)',' Smoothing box radius [20] : '
      read(5,'(a)') s
      if (s.eq.' ') then
        boxrad=10
      else
        boxrad=nint(valread(s))
      endif
      boxrad = max(0,min(199,boxrad))
      print *,' Smoothing radius=',boxrad
C Set sigma rejection limit.
      print '(a,$)',' Sigmas rejection limit [2.0] : '
      read(5,'(a)') s
      if (s.eq.' ') then
        siglim=2.5
      else
        siglim=valread(s)
      endif
      siglim = max(0.1,min(99.,siglim))
      print *,' Sigmas limit = ',siglim
      Def_ncheck=3
C Start free hand drawing.
1     continue
      print *,'Hit key:  <sp>=define bounds for median-fitting".'
      print *,
     .  '"s"=substitute continuum, "S"=substitute data, "q"=quit.'
2     continue
      call PG_CURSOR(cx,cy,key)
      if (key.eq.'q') then
        print *,'Returning to main menu.'
        return
      elseif ((key.eq.'s').or.(key.eq.'S')) then
        call xp_substitution(key)
        replot=.true.
        print *,'Returning to main menu.'
        return
      elseif (key.eq.' ') then
        call xp_mark_position(cx,2)
        print *,'...define second bound.'
        call PG_CURSOR(cx2,cy,key)
C Do we check for deviant pixels?
        if ((errbuf.gt.0).and.(siglim.gt.0.)) then
          ncheck=Def_ncheck
        else
          ncheck=0
        endif
3       continue
C Load b array.
        i = nearxi(cx)
        j = nearxi(cx2)
        i1= min(i,j)
        i2= max(i,j)
        do i=i1,i2
          k1 = min(np,max(1,i-rad))
          k2 = min(np,max(1,i+rad))
          nn=0
          do k=k1,k2
            if (a(k).gt.0.) then
              nn=nn+1
              arr(nn)=y(k)
            endif
          enddo
          if (nn.gt.1) then
            call find_median(arr,nn,rr)
          else
            rr=0.
          endif
          b(i)=rr
        enddo
C Smooth it?
        if (boxrad.gt.0) then
          do i=i1,i2
            k1 = min(i2,max(i1,i-boxrad))
            k2 = min(i2,max(i1,i+boxrad))
            rr = 0.
            nn = 0
            do k=k1,k2
              nn=nn+1
              rr=rr+b(k)
            enddo
            b(i) = rr / float(nn)
          enddo
        endif
C Reject deviant pixels.
        if (ncheck.gt.0) then
          do i=i1,i2 
            if (a(i).gt.0.) then
              if ((abs(b(i)-y(i))/e(i)).gt.siglim) then
                a(i)=-1.
                call PGPT(1,x(i),y(i),6)
              endif
            endif
          enddo
          ncheck=ncheck-1
          goto 3
        endif
C Draw b array.
        call PGSCI(8)            ! orange
        call PGLINE(1+i2-i1,x(i1),b(i1))
        call PGSCI(color)
        goto 1
      else
        goto 1
      endif
      end

C----------------------------------------------------------------------
C Experimental continuum fitting...
C
      subroutine xp_NewFit(replot,ok)
      implicit none
      logical replot,ok
      include 'xplot.inc'
      integer*4 i,j,i1,i2,boxsize,order,nearxi,n,nn,ii
      real*4 cx,cy,x1,xm,fraction,rr
      real*8 coef(0:99),polyval
      character key*1

C Define segment.
      print *,'Hit space bar twice to define segment.'
      call PG_CURSOR(x1,cy,key)
      i = nearxi(x1)
      call xp_mark_position(x(i),2)
      print *,'Choose second limit...'
      call PG_CURSOR(cx,cy,key)
      j = nearxi(cx)
      i1= min(i,j)
      i2= max(i,j)
      call xp_double_mark(x1,cx,5)

C Parameters.
12    continue
      print '(a,$)','Box size (pixels,4-40): '
      read(5,*,err=9) boxsize
      boxsize=max(4,min(40,boxsize))
      print '(a,$)','Fraction (0.0=low value,1.0=high value): '
      read(5,*,err=9) fraction
      fraction=max(0.,min(1.,fraction))
      print '(a,$)','Polynomial order (0-19): '
      read(5,*,err=9) order
      order=max(0,min(19,order))

C Load arrays for polynomial fit.
      n=0
      do i=i1,i2,boxsize
        nn=0
        xm=0.
        do ii=i,i+boxsize-1
          nn=nn+1
          a(nn) = y(ii)
          xm= xm+ x(ii)
        enddo
        call find_median(a,nn,rr)
        n=n+1
        x8(n)=xm/float(nn)
        y8(n)=dble(a(nint(float(nn)*fraction)))
        w8(n)=1.d0
      enddo
      do i=1,n
        a(i)=sngl(x8(i))
        b(i)=sngl(y8(i))
      enddo
      call PGSCI(4)
      call PGPT(n,a,b,4)
      if (n.le.order+1) then
        print *,'Error-- not enough points for given poly. order.'
        goto 16
      endif

C Fit polynomial.
14    continue
      call poly_fit_glls(n,x8,y8,w8,order,coef,ok)
      do i=1,np
        b(i)=0.
      enddo
      do i=i1,i2
        b(i)= sngl(polyval(order+1,coef,dble(x(i))))
      enddo
      call PGSCI(2)
      call PGLINE(1+i2-i1,x(i1),b(i1))
16    continue
      print '(a,$)',
     .  'New order (-1=quit; -2=subst contnm; -3=new params) : '
      read(5,*,err=9) i
      if (i.gt.-1) then
       order = i
       call PGSCI(0)
       call PGLINE(1+i2-i1,x(i1),b(i1))
       goto 14
      elseif (i.eq.-3) then
       goto 12
      elseif (i.eq.-2) then
       call xp_substitution('s')
      endif
      print *,'Returning to main menu.'
      call PGSCI(color)

      return
9     print *,'Error reading input.'
      return
      end

C----------------------------------------------------------------------
      subroutine xp_checkline(w1,w2,cx,prio)
      implicit none
      real*4 w1,w2,cx
      integer*4 prio
      include 'xplot.inc'
      real*4 r1
      r1 = za
      za = ( cx / w1 ) - 1.0
      print '(a,f14.6,a,f9.3,a)',' Za =',za,'   (',w1,')'
      call xp_absorption_lines(prio)
      za = ( cx / w2 ) - 1.0
      print '(a,f14.6,a,f9.3,a)',' Za =',za,'   (',w2,')'
      call xp_absorption_lines(prio)
      za = r1
      return
      end

C----------------------------------------------------------------------
C Draw a smoothed Voigt profile.  Generates one hundred points per pixel.
C If fwhm is zero, no smoothing is done.  Uses ZeroPoint if set. 
C Assumes normalized spectrum.
C Input: redshift       : redshift of absorption line.
C        rest_wave      : rest wavelength of absorption line (in Angstroms).
C        f_value        : oscillator strength of absorption line.
C        gamma_value    : gamma value of absorption line.
C        column_density : column density of absorption line (in cm^-2).
C        b_value        : b value of absorption line (in km/s).
C        fwhm           : FWHM of gaussian used to smooth profile (in km/s).
C        vcolor         : Color of plot line.
C        x1,x2          : Limits to profile plot in wavelengths.
C
      subroutine xp_draw_voigt(redshift,rest_wave,f_value,gamma_value,
     .                     column_density,b_value,fwhm,vcolor,x1,x2)
      implicit none
      real*8 redshift,rest_wave,f_value,gamma_value
      real*8 column_density,b_value,fwhm
      integer*4 vcolor
      real*4 x1,x2
      include 'xplot.inc'
      real*8 sol,wv0,lambda,tau
      parameter(sol=2.997925d+5)
      integer*4 nearxi,nn,i
C m is the maximum number of points in the voigt profile plot.
C sampdiv is the number of points in pixel or b value (whichever smaller.)
      integer*4 m
      real*8 sampdiv
      parameter(m=50000,sampdiv=10.d0)
      real*8 xx8(m),yy8(m),yys8(m),wave_fwhm,pixvel,sample,xblue,xred
      real*4 xx4(m),yys4(m),r1,r2,r3
      character c21*21
C
C If vcolor=0, erase last plot.
      if (vcolor.eq.0) goto 801
C Observed wavelength.
      wv0= rest_wave*(1.d0+redshift)
C Echo current parameters to user.
      r1 = sngl(fwhm)
      r2 = sngl(log10(column_density))
      r3 = sngl(b_value)
      write(c21,'(3f7.2)') r1,r2,r3
      print '(f10.4,f8.5,f9.3,f8.4,1pe10.2,1pe11.3,a)',
     .  wv0,redshift,rest_wave,f_value,gamma_value,column_density,c21
C Pixel width in km/s.
      i = nearxi(sngl(wv0))
      pixvel = dble(x(i+1)-x(i-1))/2.d0
      pixvel = sol*pixvel/wv0 
C Sampling size in angstroms.
      if (b_value.lt.pixvel) then
        sample = wv0*(b_value/sampdiv)/sol
      else
        sample = wv0*(pixvel/sampdiv)/sol
      endif
C Number of points on each side = m / 2.01 .
      xblue = wv0 - ( sample * (dfloat(m)/2.01d0) )
      xred  = wv0 + ( sample * (dfloat(m)/2.01d0) )
      xblue = max(xblue,x1) 
      xred  = min(xred ,x2)
      nn    = min(m,nint((xred-xblue)/sample))
C Load array.
      do i=1,nn
        lambda = xblue + (dfloat(i)*sample)
        call tau_voigt(lambda,redshift,rest_wave,
     .                 f_value,gamma_value,column_density,b_value,tau)
        xx8(i) = sngl(lambda)
        yy8(i) = sngl( exp( -1.d0 * tau ) )
        yy8(i) = yy8(i)*dble(1.-ZeroPoint) + dble( ZeroPoint )
      enddo
C Smooth array.
      if (fwhm.gt.0.) then
        wave_fwhm = wv0 * (fwhm/sol)
        print *,
     .  'Smooth with gaussian of width (in Angstroms): ',wave_fwhm
        call convolve_gaussian(yy8,xx8,yys8,nn,wv0,wave_fwhm)
      else
        print *,'Do not smooth with gaussian.'
        do i=1,nn
          yys8(i) = yy8(i)
        enddo
      endif
C Load 4 byte real array.
      do i=1,nn
        xx4(i) = sngl(xx8(i))
        yys4(i)= sngl(yys8(i))
      enddo
      call PGSCI(vcolor)
      call PGBBUF
      call PGLINE(nn,xx4,yys4)
      call PGEBUF
      call PGSCI(1)
      return
801   continue
      print *,'Erase last plot.'
      call PGSCI(vcolor)
      call PGBBUF
      call PGLINE(nn,xx4,yys4)
      call PGEBUF
      call PGSCI(1)
      return
901   continue
      print *,
     .  'Error in xp_draw_voigt-- number of points exceeds limit.'
      return
      end


C----------------------------------------------------------------------
C Grab info on a qso absorption line.
C Input:      guess : guess at rest wavelength
C          priority : limit search to lines with priority rank <= "priority"
C Output:  wave_out : rest wavelength
C           osc_out : oscillator strength
C         gamma_out : gamma value
C           ion_out : 8 character ion name
C
      subroutine xp_GrabLineData(guess,priority,
     .                        wave_out,osc_out,gamma_out,ion_out)
      implicit none
      real*8 guess,wave_out,osc_out,gamma_out
      integer*4 priority
      character*8 ion_out
      include 'xplot.inc'
      integer*4 i,ibest
      real*8 dev,bestdev
C Find best match to guess.
      bestdev=9.d+30
      ibest  = 1
      do i=1,qal_n
        if (qal_pri(i).le.priority) then
          dev = abs(qal_wav(i)-guess)
          if (dev.lt.bestdev) then
            ibest  = i
            bestdev= dev
          endif
        endif
      enddo
      wave_out = qal_wav(ibest)
      osc_out  = qal_osc(ibest)
      gamma_out= qal_gam(ibest)
      ion_out  = qal_ion(ibest)
      return
      end


C-----------------------------------------------------------------------
C Find QSO Absorption Lines...
C
      subroutine xp_FindQALs()
C
      implicit none
      include 'xplot.inc'
      integer*4 i,j,i1,i2,s1,s2,rad1,rad2,sl_n,bl_n,nearxi,besti,ii
      real*8 xew,xewerr,xcent,resint,sigs
      integer*4 m
      parameter(m=900)
      real*8 sl_sigs(m),best,siglim
      integer*4 sl_ae(m)
      character key*1
      real*4 cx,cy

C Defaults.
      rad1 = 1
      rad2 = 2
      siglim = 4.d0
C     print '(a,$)','Enter rad1 (1) , rad2 (2) , and siglim (5.0) = '
C     read(5,*) rad1,rad2,siglim
      print '(a,2i5,f12.5)',
     .  ' Rad1, rad2, and siglim = ',rad1,rad2,siglim
     
C Key stroke.
1     continue
      print *,'Hit key:  <sp>=segment point, "r"=restart segments,'
      print *,
     .  '"s"=substitute continuum, "S"=substitute data, "q"=quit.'
2     continue
      call PG_CURSOR(cx,cy,key)
      if (key.eq.'q') then
        print *,'Returning to main menu.'
        goto 800
      elseif (key.eq.' ') then
        call xp_mark_position(cx,2)
        i = nearxi(cx)
        print *,'Select second point...'
        call PG_CURSOR(cx,cy,key)
        j = nearxi(cx)
        i1 = min(i,j)
        i2 = max(i,j)
C Find significant equivalent widths.
        sl_n = 0
        bl_n = 0
        do i=i1,i2
C Set bounds.
          s1 = i-rad1
          s2 = i+rad1
          call xp_Find_EW(s1,s2,xew,xewerr,xcent,resint,sigs)
          if (sigs.gt.siglim) then
C Add to short list.
            sl_n = sl_n + 1
            sl_sigs(sl_n) = sigs
            sl_ae(sl_n)   = i
          else
            if (sl_n.gt.0) then
C Find best entry in short list.
              best=0.
              besti=1
              do ii=1,sl_n
                if (sl_sigs(ii).gt.best) then
                  besti=ii
                  best =sl_sigs(ii)
                endif
              enddo
C Record this in list.
              s1 = sl_ae(besti) - rad2
              s2 = sl_ae(besti) + rad2
              call xp_Find_EW(s1,s2,xew,xewerr,xcent,resint,sigs)
              bl_n = bl_n + 1
              bl_ew(bl_n)   = xew
              bl_ewerr(bl_n)= xewerr
              bl_cent(bl_n) = xcent
              bl_rw(bl_n)   = 0.d0
              bl_ri(bl_n)   = resint
              bl_crty(bl_n) = 1
C Show line on plot and report.
              call PGSCI(2)
              call PG_Mark(sngl(bl_cent(bl_n)),ymin,ymax)
              call PGSCI(1)
              print '(i5,f12.4,f7.3,2f9.5,f10.3)',
     .            bl_n,bl_cent(bl_n),bl_ri(bl_n),
     .            bl_ew(bl_n),bl_ewerr(bl_n),bl_rw(bl_n)
C Reset short list.
              sl_n = 0
            endif
          endif
        enddo
      else
        goto 1
      endif

800   continue
      call  xp_CleanLineList(bl_n)
      return
      end


C----------------------------------------------------------------------
C Take the new lines in bl_ arrays, add to ll_ arrays, sort the arrays,
C and eliminate duplicates.
C 
      subroutine xp_CleanLineList(bl_n)
      implicit none
      integer*4 bl_n
      include 'xplot.inc'
      integer*4 i,n,akey(maxarr)
      real*4 rr
C Add new lines to list.
      if (bl_n.gt.0) then
        do i=1,bl_n
          ll_n=ll_n+1
          ll_cent(ll_n) = bl_cent(i)
          ll_ew(ll_n)   = bl_ew(i)
          ll_ewerr(ll_n)= bl_ewerr(i)
          ll_rw(ll_n)   = bl_rw(i)
          ll_ri(ll_n)   = bl_ri(i)
          ll_crty(ll_n) = bl_crty(i)
        enddo
      endif
C Sort the list.
      do i=1,ll_n
        arr(i) = sngl(ll_cent(i))
        akey(i)= i
      enddo
      n=ll_n
      call qcksrtkey(n,arr,akey)
C Re-use bl_ array as a temporary array.
      do i=1,ll_n
        bl_cent(i) = ll_cent(akey(i))
        bl_ew(i)   = ll_ew(akey(i))
        bl_ewerr(i)= ll_ewerr(akey(i))
        bl_rw(i)   = ll_rw(akey(i))
        bl_ri(i)   = ll_ri(akey(i))
        bl_crty(i) = ll_crty(akey(i))
      enddo
C Mark duplicates.
      do i=1,ll_n-1
        if (abs(bl_cent(i)-bl_cent(i+1)).lt.0.1) then
          if (bl_rw(i+1).lt.1.) bl_rw(i+1)=bl_rw(i)
          bl_cent(i)=-1.
        endif
      enddo
C Eliminate duplicates.
      n = ll_n
      ll_n = 0
      do i=1,n
        if (bl_cent(i).gt.0.) then
          ll_n = ll_n + 1
          ll_cent(ll_n) = bl_cent(i)
          ll_ew(ll_n)   = bl_ew(i)
          ll_ewerr(ll_n)= bl_ewerr(i)
          ll_rw(ll_n)   = bl_rw(i)
          ll_ri(ll_n)   = bl_ri(i)
          ll_crty(ll_n) = bl_crty(i)
        endif
      enddo
C Sort all known redshifts.
      n=0
      do i=1,ll_n
        if (ll_rw(i).gt.0.) then
          n=n+1
          arr(n) = sngl(ll_cent(i)/ll_rw(i)) - 1.
        endif
      enddo
      call find_median(arr,n,rr)
C Condense duplicates and load redshift array.
      do i=1,n-1
        if (abs(arr(i)-arr(i+1)).lt.0.0002) arr(i)=-1.
      enddo
      ll_z_n=0
      do i=1,n
        if (arr(i).gt.0.) then
          ll_z_n=ll_z_n+1
          ll_z(ll_z_n)=dble(arr(i))
        endif
      enddo

      return
      end


C----------------------------------------------------------------------------
C Calculations for centroid and equivalent width, between array elements
C s1 and s2, which must be at least 2 elements from array boundaries.
C The continuum must be set and < 0 bewteen s1 and s2.
C If problems, xcent < 0 .  resint = residual intensity at bottom of line.
C
      subroutine xp_Find_EW(s1,s2,xew,xewerr,xcent,resint,sigs)

      implicit none
      include 'xplot.inc'
      integer*4 s1,s2,j,i
      real*8 xew,xewerr,xcent,sigs,resint
      real*8 r,rr,r1,r2,dx

C Make sure there is a continuum.
      if (contbuf.eq.0) then
        print *,
     .  'Error- No continuum set. Use NORM keyword if normalized.'
        return
      else
        j=0
        do i=s1,s2
          if (c(i).lt.1.e-30) j=1
        enddo
        if (j.eq.1) then
          print *,'Error- Part of relevant continuum <= 0.'
          return
        endif
      endif

C Calculate centroid and find lowest residual intensity.
      r1 = 0.d0
      r2 = 0.d0
      resint = 1.d+30
      do i=s1,s2
        r = abs(dble(y(i)-c(i)))
        r1= r1 + (x(i)*r)
        r2= r2 + r
        r = y(i)/c(i)
        if (r.lt.resint) resint=r
      enddo
      xcent = r1 / r2

C Calculate equivalent width and error.
      xew = 0.d0
      xewerr = 0.d0
      do i=s1,s2
        r = dble(y(i)) / dble(c(i))
        rr= dble(e(i)) / dble(c(i))
        dx= (xo(i+1)-xo(i-1))/2.d0
        xew = xew + ( (1.d0 - r) * dx )
        xewerr = xewerr + ( rr*rr*dx*dx )
      enddo
      xewerr = sqrt(xewerr)
      sigs = xew/xewerr

      return
      end

C-------------------------------------------------------------------
C-------------------------------------------------------------------
C-------------------------------------------------------------------

C :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
C
C fitfunk.F                 Fitting of various functions.
C
C root        description             formula                    parameters
C
C gauss      Single Gaussian     A e^(-ln(2)((x-xc)/HWHM)^2)     A,xc,HWHM
C
C lorentz    Single Lorentzian   A / (1+((x-xc)/HWHM)^2)         A,xc,HWHM
C
C gauss_fc   Single Gaussian     A e^(-ln(2)((x-x0)/HWHM)^2)     A,HWHM
C            fixed center   
C
C*dgauss     Double Gaussian    A1 e^(-ln(2)((x-xc1)/HWHM1)^2)   A1,xc1,HWHM1,
C                             + A2 e^(-ln(2)((x-xc2)/HWHM2)^2)   A2,xc2,HWHM2
C  
C*dgauss_sc  Double Gaussian    A1 e^(-ln(2)((x-xc)/HWHM1)^2)    A1,xc,HWHM1,
C            same center      + A2 e^(-ln(2)((x-xc)/HWHM2)^2)    A2,HWHM2
C 
C*dgauss_fc  Double Gaussian    A1 e^(-ln(2)((x-x0)/HWHM1)^2)    A1,HWHM1,
C            fixed center     + A2 e^(-ln(2)((x-x0)/HWHM2)^2)    A2,HWHM2
C 
C*lorgauss   Lorentzian plus    A1 / (1+((x-xc1)/HWHM1)^2)       A1,xc1,HWHM1,
C            Gaussian         + A2 e^(-ln(2)((x-xc2)/HWHM2)^2)   A2,xc2,HWHM2
C
C power      Power Law          A x^b                            A,b
C
C bbody      Black Body         ( A / x^5 ) / ( exp(B/x) - 1 )   A,B
C
C
C :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

C-------------------------------------------------------------------
C Make wild guesses at parameters for a Gaussian-like distribution.
      subroutine gausslike_autoguess(p,h,x0)
      implicit none
      real*8 p,h,x0,dum1,dum2
      include 'xplot.inc'    ! common block with x8, y8, and w8.
      integer*4 n,i,j,narr
      common /funkblk/ dum1,dum2,n
      real*4 r
C Initial guess.
      i = max(1,n/2)
      p = y8(i)
      x0= x8(i)
C Find group of five points with highest median.
      if (n.gt.5) then
        do i=3,n-2
          narr=0
          do j=i-2,i+2
            if (w8(j).gt.0.) then
              narr=narr+1
              arr(narr) = sngl(y8(j))
            endif
          enddo
          if (narr.gt.1) then
            call find_median(arr,narr,r)
            if (dble(r).gt.p) then
              p = dble(r)
              x0= x8(i)
            endif
          endif
        enddo
      endif
C Half of half width of region to be fitted.
      h = dabs(x8(1)-x8(n))/4.
      return
      end

C-------------------------------------------------------------------
C Make wild guesses at parameters for a Gaussian-like distribution.
C With a fixed center.
      subroutine gausslike_autoguess_fc(p,h,x0)
      implicit none
      real*8 p,h,x0,dum1,dum2
      include 'xplot.inc'      ! common block with x8, y8, and w8.
      integer*4 n,i,j,narr
      common /funkblk/ dum1,dum2,n
      real*4 r
C Find point closest to x0.
      j=max(1,n/2)
      dum1=1.d+60
      do i=1,n
        if ((w8(i).gt.0.).and.(dabs(x8(i)-x0).lt.dum1)) then
          dum1=x8(i)
          j=i
        endif
      enddo
C Initial guess and then find median of five points.
      p=y8(j)
      do i=max(1,j-2),min(n,j+2)
        narr=0
        if (w8(i).gt.0.) then
          narr=narr+1
          arr(narr)=sngl(y8(i))
        endif
        if (narr.gt.1) then
          call find_median(arr,narr,r)
          p=dble(r)
        endif
      enddo
C Half of half width of region to be fitted.
      h = dabs(x8(1)-x8(n))/4.
      return
      end


C--------------------------------------------------------------------------
C Fit a Double Gaussian with fixed centers of the form:
C  y = peak1 * exp(-ln(2)*((x-xcenter1)/hwhm1)**2.)  +
C      peak2 * exp(-ln(2)*((x-xcenter2)/hwhm2)**2.)
C Points and weights (x8,y8,w8) will be in a common block in an include file.
C If peak, hwhm, or xcenter are non-zero they are assumed to be guesses for the
C peak of the Gaussian, Half-Width-Half-Maximum, and center point.
C If any of these are zero, the function will make its own guesses.

      logical function fit_dgauss_fc(npoints,peak1,hwhm1,xcenter1,
     .                                       peak2,hwhm2,xcenter2)

      implicit none
      integer*4 npoints            ! number of points, x8(1..npts), etc.
      real*8 peak1,hwhm1,xcenter1  ! 1st Peak, HWHM, and center x value.
      real*8 peak2,hwhm2,xcenter2  ! 2nd Peak, HWHM, and center x value.
      include 'xplot.inc'         ! common block with x8, y8, and w8.
      logical amoeba_gate
      real*8 tiny,p1,h1,x01,p2,h2,x02
      parameter(tiny=1.d-50)
C For AMOEBA.
      real*8 fit_dgauss_fc_funk   ! External function to minimize   (input)
      EXTERNAL fit_dgauss_fc_funk
      integer*4 npar           ! Number of parameters            (input)
      parameter(npar=4)
      real*8 par(npar)         ! Parameters (p1,h1,p2,h2)      (input/output)
      real*8 parsl(npar)       ! Scale length for each parameter (input)
      real*8 resid             ! Residual (final value of FUNK)  (output)
C For use by fit_dgauss_fc_funk (function to be minimized).
      integer*4 n
      common /funkblk/ x01,x02,n

C Transfer parameters to variables.
      n  = npoints
      p1 = peak1
      p2 = peak2
      h1 = hwhm1
      h2 = hwhm2
      x01= xcenter1
      x02= xcenter2
C Auto-guess.
      call gausslike_autoguess_fc(p1,h1,x01)
      p1 =p1/1.5d0
      call gausslike_autoguess_fc(p2,h2,x02)
      p2 =p2/1.5d0
      print *,'Guesses: p1,p2,h1,h2 : ',p1,p2,h1,h2
C Use user guesses if requested.
      if (dabs(peak1).gt.tiny)    p1 = peak1
      if (dabs(hwhm1).gt.tiny)    h1 = hwhm1
      if (dabs(peak2).gt.tiny)    p2 = peak2
      if (dabs(hwhm2).gt.tiny)    h2 = hwhm2
C Load parameters.
      par(1) = p1*0.999d0
      par(2) = h1*1.001d0
      par(3) = p2*1.001d0
      par(4) = h2*0.999d0
C Load scale lengths.
      parsl(1) = 0.12d0*p1
      parsl(2) = 0.12d0*h1
      parsl(3) = 0.08d0*p2
      parsl(4) = 0.08d0*h2
C Fit Gaussian.
      fit_dgauss_fc =
     .   amoeba_gate(fit_dgauss_fc_funk,npar,par,parsl,resid)
      peak1     = par(1)
      hwhm1     = par(2)
      peak2     = par(3)
      hwhm2     = par(4)

      print '(a,3(1pe14.6))','1st Peak, FWHM, X-Center =',
     .           peak1,hwhm1*2.d0,xcenter1
      print '(a,3(1pe14.6))','2nd Peak, FWHM, X-Center =',
     .           peak2,hwhm2*2.d0,xcenter2

      return
      end

C-------------------------------------------------------------------
C Minimize the deviations from a Double Gaussian with fixed centers.

      real*8 function fit_dgauss_fc_funk(par)

      implicit none
      include 'xplot.inc'   ! common block with x8, y8, and w8.
      real*8 par(4)            ! p1,h1,p2,h2
      integer*4 i
      real*8 mln2,sum,r,yv,x01,x02
      parameter(mln2=-0.693147181d0)          !  -ln(2)
      integer*4 n
      common /funkblk/ x01,x02,n
      sum = 0.d0
      do i=1,n
        r  = (x8(i)-x01)/par(2)
        yv = par(1) * dexp(mln2*r*r)
        r  = (x8(i)-x02)/par(4)
        yv = yv + par(3) * dexp(mln2*r*r)
        r  = y8(i) - yv
        sum= sum + (r*r*w8(i))
      enddo
      if ( (par(1).lt.0.).or.(par(2).lt.0.).or.
     .     (par(3).lt.0.).or.(par(4).lt.0.) ) sum=sum*10.d0
      r = par(2)/par(4)
      if ((r.gt.5.).or.(r.lt.0.2)) sum = sum * 10.d0
      r = par(1)/par(3)
      if ((r.gt.50.).or.(r.lt.0.02)) sum = sum * 10.d0
      fit_dgauss_fc_funk = sum
      return
      end


C--------------------------------------------------------------------------
C Fit a Gaussian of the form:  y = peak * exp(-ln(2)*((x-xcenter)/hwhm)**2.).
C Points and weights (x8,y8,w8) will be in a common block in an include file.
C If peak, hwhm, or xcenter are non-zero they are assumed to be guesses for the
C peak of the Gaussian, Half-Width-Half-Maximum, and center point.
C If any of these are zero, the function will make its own guesses.

      logical function fit_gauss(npoints,peak,hwhm,xcenter)

      implicit none
      integer*4 npoints         ! number of points, x8(1..npts), etc.
      real*8 peak,hwhm,xcenter  ! Peak, HWHM, and center x value.
      include 'xplot.inc'       ! common block with x8, y8, and w8.
      logical amoeba_gate
      real*8 tiny,p,h,x0,dum1,dum2
      parameter(tiny=1.d-50)
C For AMOEBA.
      real*8 fit_gauss_funk   ! External function to minimize   (input)
      EXTERNAL fit_gauss_funk
      integer*4 npar          ! Number of parameters            (input)
      parameter(npar=3)
      real*8 par(npar)        ! Parameters (p,h,x0)             (input/output)
      real*8 parsl(npar)      ! Scale length for each parameter (input)
      real*8 resid            ! Residual (final value of FUNK)  (output)
C For use by fit_gauss_funk (function to be minimized).
      integer*4 n
      common /funkblk/ dum1,dum2,n

C Transfer parameters to variables.
      p = peak
      h = hwhm
      x0= xcenter
      n = npoints
C Auto-guess.
      call gausslike_autoguess(p,h,x0)
C Use user guesses if requested.
      if (dabs(peak).gt.tiny)    p = peak
      if (dabs(hwhm).gt.tiny)    h = hwhm
      if (dabs(xcenter).gt.tiny) x0= xcenter
C Load parameters.
      par(1) = p
      par(2) = h
      par(3) = x0
C Load scale lengths.
      parsl(1) = p*0.1d0
      parsl(2) = h*0.1d0
      parsl(3) = dabs(x8(1)-x8(n))/10.d0
C Fit Gaussian.
      fit_gauss = amoeba_gate(fit_gauss_funk,npar,par,parsl,resid)
      peak = par(1)
      hwhm = par(2)
      xcenter = par(3)
      print '(a,3(1pe13.6))',
     .   'Peak, FWHM, X-Center =',peak,hwhm*2.d0,xcenter

      return
      end

C-------------------------------------------------------------------
C Minimize the deviations from a Gaussian.

      real*8 function fit_gauss_funk(par)

      implicit none
      include 'xplot.inc'   ! common block with x8, y8, and w8.
      real*8 par(3)         ! p,h,x0
      integer*4 i
      real*8 mln2,sum,r,yv,dum1,dum2
      parameter(mln2=-0.693147181d0)          !  -ln(2)
      integer*4 n
      common /funkblk/ dum1,dum2,n
      sum = 0.d0
      do i=1,n
        r  = (x8(i)-par(3))/par(2)
        yv = par(1) * dexp(mln2*r*r)
        r  = y8(i)-yv
        sum = sum + (r*r*w8(i))
      enddo
      fit_gauss_funk = sum
      return
      end


C--------------------------------------------------------------------------
C Fit a Gaussian of the form:  y = peak * exp(-ln(2)*((x-xcenter)/hwhm)**2.).
C Points and weights (x8,y8,w8) will be in a common block in an include file.
C "xcenter" is assumed to be given and fixed, if peak or hwhm are non-zero
C they are assumed to be guesses for the peak of the Gaussian and
C Half-Width-Half-Maximum.  If either of these are zero, the function will
C make its own guesses.

      logical function fit_gauss_fc(npoints,peak,hwhm,xcenter)

      implicit none
      integer*4 npoints         ! number of points, x8(1..npts), etc.
      real*8 peak,hwhm,xcenter  ! Peak, HWHM, and given fixed center x value.
      include 'xplot.inc'       ! common block with x8, y8, and w8.
      logical amoeba_gate
      real*8 tiny,p,h,x0,dum1
      parameter(tiny=1.d-50)
C For AMOEBA.
      real*8 fit_gauss_fc_funk ! External function to minimize   (input)
      EXTERNAL fit_gauss_fc_funk
      integer*4 npar           ! Number of parameters            (input)
      parameter(npar=2)
      real*8 par(npar)         ! Parameters (p,h)                (input/output)
      real*8 parsl(npar)       ! Scale length for each parameter (input)
      real*8 resid             ! Residual (final value of FUNK)  (output)
C For use by fit_gauss_fc_funk (function to be minimized).
      integer*4 n
      common /funkblk/ dum1,x0,n

C Transfer parameters to variables.
      n = npoints
      p = peak
      h = hwhm
      x0 = xcenter
C Auto-guess.
      call gausslike_autoguess_fc(p,h,x0)
C Use user guesses if requested.
      if (dabs(peak).gt.tiny)  p = peak
      if (dabs(hwhm).gt.tiny)  h = hwhm
C Load parameters.
      par(1) = p
      par(2) = h
C Load scale lengths.
      parsl(1) = p*0.1d0
      parsl(2) = h*0.1d0
C Fit Gaussian.
      fit_gauss_fc = amoeba_gate(fit_gauss_fc_funk,npar,par,parsl,resid)
      peak = par(1)
      hwhm = par(2)
      print '(a,3(1pe13.6))',
     .      'Peak, FWHM, X-Center =',peak,hwhm*2.d0,xcenter

      return
      end

C-------------------------------------------------------------------
C Minimize the deviations from a Gaussian.
      real*8 function fit_gauss_fc_funk(par)
      implicit none
      include 'xplot.inc'   ! common block with x8, y8, and w8.
      real*8 par(2)         ! p,h
      integer*4 i
      real*8 mln2,sum,r,yv,dum1
      parameter(mln2=-0.693147181d0)          !  -ln(2)
      real*8 x0
      integer*4 n
      common /funkblk/ dum1,x0,n
      sum = 0.d0
      do i=1,n
        r  = (x8(i)-x0)/par(2)
        yv = par(1) * dexp(mln2*r*r)
        r  = y8(i)-yv
        sum = sum + (r*r*w8(i))
      enddo
      fit_gauss_fc_funk = sum
      return
      end

C--------------------------------------------------------------------------
C Fit a Lorentzian distribution of the form:
C     y = peak / ( 1 + ((x-xcenter)/hwhm)**2.) .
C
C Points and weights (x8,y8,w8) will be in a common block in an include file.
C If peak, hwhm, or xcenter are non-zero they are assumed to be guesses for the
C peak of the Lorentzian, Half-Width-Half-Maximum, and center point.
C If any of these are zero, the function will make its own guesses.

      logical function fit_lorentz(npoints,peak,hwhm,xcenter)

      implicit none
      integer*4 npoints         ! number of points, x8(1..npts), etc.
      real*8 peak,hwhm,xcenter  ! Peak, HWHM, and center x value.
      include 'xplot.inc'       ! common block with x8, y8, and w8.
      logical amoeba_gate
      real*8 tiny,p,h,x0,dum1,dum2
      parameter(tiny=1.d-50)
C For AMOEBA.
      real*8 fit_lorentz_funk   ! External function to minimize   (input)
      EXTERNAL fit_lorentz_funk
      integer*4 npar          ! Number of parameters            (input)
      parameter(npar=3)
      real*8 par(npar)        ! Parameters (p,h,x0)             (input/output)
      real*8 parsl(npar)      ! Scale length for each parameter (input)
      real*8 resid            ! Residual (final value of FUNK)  (output)
C For use by fit_lorentz_funk (function to be minimized).
      integer*4 n
      common /funkblk/ dum1,dum2,n

C Transfer parameters to variables.
      p = peak
      h = hwhm
      x0= xcenter
      n = npoints
C Auto-guess.
      call gausslike_autoguess(p,h,x0)
C Use user guesses if requested.
      if (dabs(peak).gt.tiny)    p = peak
      if (dabs(hwhm).gt.tiny)    h = hwhm
      if (dabs(xcenter).gt.tiny) x0= xcenter
C Load parameters.
      par(1) = p
      par(2) = h
      par(3) = x0
C Load scale lengths.
      parsl(1) = p*0.1d0
      parsl(2) = h*0.1d0
      parsl(3) = dabs(x8(1)-x8(n))/10.d0
C Fit Lorentz.
      fit_lorentz = amoeba_gate(fit_lorentz_funk,npar,par,parsl,resid)
      peak = par(1)
      hwhm = par(2)
      xcenter = par(3)
      print '(a,3(1pe13.6))',
     .      'Peak, FWHM, X-Center =',peak,hwhm*2.d0,xcenter

      return
      end
      
C-------------------------------------------------------------------
C Minimize the deviations from a Lorentzian.

      real*8 function fit_lorentz_funk(par)

      implicit none
      include 'xplot.inc'   ! common block with x8, y8, and w8.
      real*8 par(3)         ! p,h,x0
      integer*4 i
      real*8 mln2,sum,r,yv,dum1,dum2
      parameter(mln2=-0.693147181d0)          !  -ln(2)
      integer*4 n
      common /funkblk/ dum1,dum2,n
      sum = 0.d0
      do i=1,n
        r  = (x8(i)-par(3))/par(2)
        yv = par(1) / ( 1.d0 + r*r )
        r  = y8(i)-yv
        sum = sum + (r*r*w8(i))
      enddo
      fit_lorentz_funk = sum
      return
      end


C--------------------------------------------------------------------------
C Fit a Black Body distribution of the form:
C
C      y = ( A / x^5 )  / ( exp(B/x) - 1 )
C
C   where x is in wavelengths, A is 2*h*c*c, and B is h*c/(k*T) .
C
C Points and weights (x8,y8,w8) will be in a common block in an include file.
C If A and B are non-zero they are assumed to be guesses for these values.
C If either of these are zero, the function will make its own guesses.

      logical function fit_bbody_usertemp(npoints,AA,BB)

      implicit none
      integer*4 npoints         ! number of points, x8(1..npts), etc.
      real*8 AA,BB              ! parameters.
      include 'xplot.inc'       ! common block with x8, y8, and w8.
      real*8 tiny,dum1,dum2,AAp,BBp
      parameter(tiny=1.d-50)
C For AMOEBA.
      real*8 fit_bbody_funk   ! External function to minimize   (input)
      EXTERNAL fit_bbody_funk
      integer*4 npar          ! Number of parameters            (input)
      parameter(npar=2)
C For use by fit_bbody_funk (function to be minimized).
      integer*4 n
      common /funkblk/ dum1,dum2,n
      real*8 hc,kb,temp
      parameter(hc=4.1357d-15*2.9979d+18)   ! eV * Angstroms
      parameter(kb=8.6173d-5)       ! eV / degrees Kelvin (Boltzmann Constant)

C Transfer parameters to variables.
      AAp= AA
      BBp= BB
      n  = npoints
C Ask for temperature.
      print '(a,$)','Enter temperature (degrees Kelvin) : '
      read(5,*,err=99) temp
C Quick-guess.
      BBp= hc / ( temp * kb )
      call calc_bbody_a(AAp,BBp)
      AA = AAp
      BB = BBp
      print '(a,3(1pe13.6))','Fits:    A,B,temp =',AA,BB,(hc/(kb*BB))
      fit_bbody_usertemp = .true.
      return
99    print *,'Error in fit_bbody_usertemp...'
      fit_bbody_usertemp = .false.
      return
      end


C--------------------------------------------------------------------------
C Fit a Black Body distribution of the form:
C
C      y = ( A / x^5 )  / ( exp(B/x) - 1 )
C
C   where x is in wavelengths, A is 2*h*c*c, and B is h*c/(k*T) .
C
C Points and weights (x8,y8,w8) will be in a common block in an include file.
C If A and B are non-zero they are assumed to be guesses for these values.
C If either of these are zero, the function will make its own guesses.

      logical function fit_bbody(npoints,AA,BB)

      implicit none
      integer*4 npoints         ! number of points, x8(1..npts), etc.
      real*8 AA,BB              ! parameters.
      include 'xplot.inc'       ! common block with x8, y8, and w8.
      logical amoeba_gate
      real*8 tiny,dum1,dum2,AAp,BBp
      parameter(tiny=1.d-50)
C For AMOEBA.
      real*8 fit_bbody_funk   ! External function to minimize   (input)
      EXTERNAL fit_bbody_funk
      integer*4 npar          ! Number of parameters            (input)
      parameter(npar=2)
      real*8 par(npar)        ! Parameters (p,h,x0)             (input/output)
      real*8 parsl(npar)      ! Scale length for each parameter (input)
      real*8 resid            ! Residual (final value of FUNK)  (output)
C For use by fit_bbody_funk (function to be minimized).
      integer*4 n
      common /funkblk/ dum1,dum2,n
      real*8 hc,kb
      parameter(hc=4.1357d-15*2.9979d+18)   ! eV * Angstroms
      parameter(kb=8.6173d-5)       ! eV / degrees Kelvin (Boltzmann Constant)

C Transfer parameters to variables.
      AAp= AA
      BBp= BB
      n  = npoints
C Auto-guess.
      BBp = hc / ( 3.d+4 * kb )
      call calc_bbody_a(AAp,BBp)
      print '(a,3(1pe13.6))',
     .  'Guesses: A,B,temp =',AAp,BBp,(hc/(kb*BBp))
C Use user guesses if requested.
      if (dabs(AA).gt.tiny)  AAp = AA
      if (dabs(BB).gt.tiny)  BBp = BB
C Load parameters.
      par(1) = AAp
      par(2) = BBp
C Load scale lengths.
      parsl(1) = AAp*0.1d0
      parsl(2) = BBp*0.1d0
C Fit Black Body.
      fit_bbody = amoeba_gate(fit_bbody_funk,npar,par,parsl,resid)
      AA = par(1)
      BB = par(2)
      print '(a,3(1pe13.6))','Fits:    A,B,temp =',AA,BB,(hc/(kb*BB))

      return
      end

C----------------------------------------------------------------------
      subroutine calc_bbody_a(AAp,BBp)
C This subroutine calculates A for the black body fit (y=(A/x^5)/(exp(B/x)-1))
C for a given B:
C    A =  SUM { Wi * Yi * F(B,Xi) }   /  SUM { Wi * F(B,Xi)^2 }
C where F(B,Xi) = Xi^-5 / (exp(B/Xi)-1).
      implicit none
      include 'xplot.inc'       ! common block with x8, y8, and w8.
      real*8 AAp,BBp,dum1,dum2,f
      integer*4 n,i
      common /funkblk/ dum1,dum2,n
      dum1=0.d0
      dum2=0.d0
      do i=1,n
       f = ( x8(i)**(-5.d0) ) / ( dexp(BBp/x8(i)) - 1.d0 )
       dum1 = dum1 + w8(i)*y8(i)*f
       dum2 = dum2 + w8(i)*f*f
      enddo
      AAp = dum1 / dum2
      return
      end
      
C-------------------------------------------------------------------
C Minimize the deviations from a Black Body.

      real*8 function fit_bbody_funk(par)

      implicit none
      include 'xplot.inc'   ! common block with x8, y8, and w8.
      real*8 par(2)         ! A,B
      integer*4 i
      real*8 sum,r,yv,dum1,dum2
      integer*4 n
      common /funkblk/ dum1,dum2,n
      sum = 0.d0
      do i=1,n
        yv  = (par(1)/(x8(i)**5.d0))/(dexp(par(2)/x8(i))-1.d0)
        r  = y8(i)-yv
        sum = sum + (r*r*w8(i))
      enddo
      fit_bbody_funk = sum
      return
      end


C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

C:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


C--------------------------------------------------------------------------
C Fit a Power Law of the form:  y = A * ( x**b ),  A==aa, b==bb.
C Uses the trick of translating to:  ln(y) = ln(A) + b*ln(x), and then
C fitting to a straight line.
C Points and weights (x8,y8,w8) will be in a common block in an include file.

      logical function fit_power(n,aa,bb)

      implicit none
      integer*4 n            ! number of points, x8(1..npts), etc.
      real*8 aa,bb           ! A and b in the formula: y=Ax^b.
      include 'xplot.inc'    ! common block with x8, y8, and w8.
      integer*4 i
      real*8 c11,c12,c13,c22,c23,tiny
      parameter(tiny=1.d-50)

C Check for small values or negative values.
      do i=1,n
        if ( (w8(i).gt.tiny) .and. 
     .       ((x8(i).lt.tiny).or.(y8(i).lt.tiny)) ) then
          print *,'Error in fit_power: all values must be positive.'
          fit_power=.false.
          return
        endif
      enddo

C Translate into new frame.
      do i=1,n
        if (w8(i).gt.tiny) then
          w8(i) = y8(i)*y8(i)*w8(i)
          x8(i) = dlog(x8(i))
          y8(i) = dlog(y8(i))
        endif
      enddo
C Compute constants for fitting a straight line.
      c11=0.d0
      c12=0.d0
      c13=0.d0
      c22=0.d0
      c23=0.d0
      do i=1,n
        if (w8(i).gt.tiny) then
          c11 = c11 + w8(i)
          c12 = c12 + x8(i)*w8(i)
          c13 = c13 + y8(i)*w8(i)
          c22 = c22 + x8(i)*x8(i)*w8(i)
          c23 = c23 + y8(i)*w8(i)*x8(i)
        endif
      enddo
C Solve for aa and bb.
      bb = ( c13*c12 - c23*c11 ) / ( c12*c12 - c22*c11 )
      aa = ( c13 - c12*bb ) / c11
      aa = dexp(aa)
      print '(3(a,1pe11.4),a)',' y = A x^b :   A =',aa,'    b=',bb,
     .                         '   ( alpha=',-1.d0*(bb+2.d0),' )'
      fit_power=.true.
      return
      end

C----------------------------------------------------------------------
C Convolve a gaussian into an array of values.
C Input: a(),x(),npix,x0,fwhm.   Output: ao().
C
      subroutine convolve_gaussian(a,x,ao,npix,x0,fwhm)
      implicit none
      integer*4 npix,i,j
      real*8 a(npix),x(npix),ao(npix),x0,fwhm,gaussian_hwhm
      real*8 hwhm,sum,wsum,xx,r8
      hwhm = fwhm/2.d0
      print *,'Convolving... npix=',npix
      do i=1,npix
        sum =0.d0
        wsum=0.d0
        do j=1,npix 
          xx = (x(j)-x(i)) / hwhm
          if (abs(xx).lt.4.) then
            r8  = gaussian_hwhm(xx)
            sum = sum + ( a(j) * r8 )
            wsum= wsum+ r8
          endif
        enddo
        ao(i) = sum / wsum
      enddo
      print *,'...done.'
      return
      end
