C fsort.f         File Sorter            tab   April 1992
C
C Made it into a subroutine December 1998.
C

      implicit none
      character*80 arg(9)
      integer*4 narg
      logical ok

      call arguments(arg,narg,9)

      call filesort_ascii(arg,narg,ok)

      stop
      end
