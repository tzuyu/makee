/* catps.c                        tab  oct94/mar98/oct99  */


#include "ctab.h"
#include "cmisc.h"


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Convert an ASCII text file into a PostScript file.
*/
int main(int argc, char *argv[])
{
/**/
char arg[100][100];
char wrd[100];
int narg;
char afile[100],psfile[100];
/**/
int ptsz,left,bold,land,head;
/**/

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Get Options */
ptsz = 10;
if (cfindarg(arg,&narg,"pt=",       ':',wrd)==1) {ptsz =GetLineValue(wrd,1); }
ptsz = MAX(5,MIN(40,ptsz));
left = 0;
if (cfindarg(arg,&narg,"left=",     ':',wrd)==1) {left =GetLineValue(wrd,1); }
head = cfindarg(arg,&narg,"-header",'s',wrd);
bold = cfindarg(arg,&narg,"-bold",  's',wrd);
land = cfindarg(arg,&narg,"-land",  's',wrd);

/* Syntax. */
if ( narg < 2 ) {
  printf("Syntax: atps  (ASCII file)  (PostScript file)  [pt=n] [-land]\n");
  printf("                      [-bold]  [left=n]  [-header]\n");
  printf("\n");
  printf("   Simple ASCII to PostScript converter.  Output goes to\n");
  printf("   standard output unless an output file is specfied.\n");
  printf(" -header: Add header to top of file.\n");
  printf(" -land  : Use landscape mode.\n");
  printf("  pt=n  : Specify pointsize n (default 10).\n");
  printf(" -bold  : Use Courier-Bold instead of Courier (diff. aspect).\n");
  printf(" left=n : Bad n points on the left hand side (n = 70 = one inch).\n");
  printf("            (i.e shift text to the right)\n");
  printf("    ptsz char/line line/page   LANDSCAPE: char/line line/page\n");
  printf("     12 ..  79  .....  55  ................  105  ...  40\n");
  printf("     10 ..  92  .....  66  ................  126  ...  48\n");
  printf("      9 .. 107  .....  73  ................  142  ...  53\n");
  printf("      8 .. 117  .....  82  ................  160  ...  60\n");
  printf("      7 .. 131  .....  94  ................  183  ...  68\n");
  printf("      6 .. 170  ..... 110  ................  210  ...  80\n");
  printf("\n");
  exit(0);
}

/* Files */
strcpy(afile,arg[1]);
strcpy(psfile,arg[2]);

/* Call routine */
cASCII_to_PS(afile,psfile,ptsz,left,bold,land,head);

return(0);

}

