C column_edit.f                        tab April 1993
C
C Move or delete columns in a text file using standard input and output.
C (This can be used within "vi").
C
C
      implicit none
      integer*4 narg,sc,nc,k,i
      integer*4 lc
      character*40 arg(9),wrd
      character line*400, blank*400
      real*4 valread

      blank = ' '
      call arguments(arg,narg,9)
      wrd = arg(1)
      call allcaps(wrd)
      if ((index(wrd,'HELP').gt.0).or.(narg.eq.0)) then
       print *,'Syntax:  column_edit  sc nc'
       print *,'  '
       print *,'  Move or delete columns in a text file using standard'
       print *,'    input and output. This can be used within "vi".'
       print *,'  '
       print *,'  sc = starting column.'
       print *,'  nc = number of columns.'
       print *,'  '
       print '(2a)','  If nc > 0, nc blank columns are added',
     .              ' starting before sc.'
       print '(2a)','  If nc < 0, nc columns are deleted starting',
     .              ' with sc.'
       print '(2a)','  If nc = 0, delete all columns starting',
     .              ' with sc.'
       print *,'  '
       call exit(0)
      endif

      sc = nint(valread(arg(1)))
      nc = nint(valread(arg(2)))
      if (sc.lt.1) goto 990

1     read(5,'(a)',end=5) line
      k = max(1,lc(line))
      i = min(k,sc-1)
      if (nc.gt.0) then
        print '(a,a,a)',line(1:i),blank(1:nc),line(sc:k)
      elseif (nc.lt.0) then
        print '(a,a)',line(1:i),line(sc+abs(nc):k)
      else 
        print '(a)',line(1:i)
      endif 
      goto 1

5     continue 
      stop

C Non-damaging error exit...

990   continue
991   read(5,'(a)',end=995) line 
      print '(a)',line(1:lc(line))
      goto 991
995   continue 

      stop
      end

