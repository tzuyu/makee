C xplot.inc
C
      integer*4 maxpt,max_also,mpt2d,mrw2d,maxarr
      integer*4 maxqal,maxsys,maxsysline
      parameter(maxpt=300000,max_also=4)
      parameter(mpt2d=4100,mrw2d=42,maxarr=9000)
      parameter(maxqal=900,maxsys=90,maxsysline=900)
C
      character*115200 header,temphead,cheader,eheader
      character*80 psfile,contfile,errfile,datafile
      character*80 userfile,userfile0,device
      character*80 qefile,qafile,qgfile,aafile,ewfile,ll_file,sl_file
      character*8 qal_ion(maxqal),PS_Mode,Aux_Mode
      character*8 sl_ion(maxsys,maxsysline)
C
      real*8 x8(maxpt),y8(maxpt),w8(maxpt),xo(maxpt)
      real*8 xo2d(mpt2d,mrw2d)
      real*8 ll_cent(maxarr),ll_ew(maxarr)
      real*8 ll_ewerr(maxarr),ll_rw(maxarr)
      real*8 ll_ri(maxarr),ll_z(maxarr)
      real*8 sl_rw(maxsys,maxsysline),sl_za(maxsys)
      real*8 bl_cent(maxarr),bl_ew(maxarr)
      real*8 bl_ewerr(maxarr),bl_rw(maxarr)
      real*8 bl_ri(maxarr)
      real*8 qal_wav(maxqal),qal_osc(maxqal),qal_gam(maxqal)
      real*8 apt_x(maxarr),apt_y(maxarr),apt_w(maxarr)
      real*8 lambda_zero_velocity
C
      integer*4 np,np_bin,color,crad,bufnum,sect(90,2)
      integer*4 nsect,nspt,naal,nh,nv,npg,bin
      integer*4 errbuf,contbuf,num_also,n_also(max_also)
      integer*4 n_also_bin(max_also)
      integer*4 abs_priority,row2d,ll_n,qal_n,qal_pri(maxqal)
      integer*4 ll_z_n,ll_crty(maxarr),bl_crty(maxarr),apt_n,za_also_n
      integer*4 sl_color(maxsys),sl_numline(maxsys),sl_numsys
C
      real*4 arr(maxarr)
C
C current plot arrays.
      real*4 x(maxpt),y(maxpt),yo(maxpt),a(maxpt)
      real*4 b(maxpt),c(maxpt),e(maxpt)
C
C current plot bin arrays.
      real*4 x_bin(maxpt),y_bin(maxpt),e_bin(maxpt)
      real*4 c_bin(maxpt),cff(maxpt)
C
C _also arrays.
      real*4 x_also(maxpt,max_also),y_also(maxpt,max_also)
      real*4 x_also_bin(maxpt,max_also),y_also_bin(maxpt,max_also)
C
C 2D arrays.
      real*4 y2d_also(mpt2d,mrw2d,max_also)
      real*4 x2d_also(mpt2d,mrw2d,max_also)
      real*4 y2d(mpt2d,mrw2d),yo2d(mpt2d,mrw2d)
      real*4 e2d(mpt2d,mrw2d),c2d(mpt2d,mrw2d)
C
      real*4 xmin,xmax,ymin,ymax,mark,xctrd,ze,za,za_also(99)
      real*4 ZeroPoint,tabgscale
      real*4 xs,xe,ys,ye,ysa(99),yea(99)
      real*4 aablu(900),aared(900),aarsi(900),mfm,Xgap,XgapFraction
C
      logical hist,dots,dash,quit,modified,norm
      logical pg_ask,oo,ll_showZ,ll_showQ
      logical show_velocity,show_coeff

      common /XPLOTBLK/ header,temphead,cheader,eheader,
     .   psfile,contfile,errfile,datafile,userfile,userfile0,device,
     .   qefile,qafile,qgfile,aafile,ewfile,ll_file,sl_file,
     .   qal_ion,PS_Mode,Aux_Mode,sl_ion,
     .   x8,y8,w8,xo,xo2d,
     .   ll_cent,ll_ew,ll_ewerr,ll_rw,ll_ri,ll_z,sl_rw,sl_za,
     .   bl_cent,bl_ew,bl_ewerr,bl_rw,bl_ri,qal_wav,qal_osc,qal_gam,
     .   apt_x,apt_y,apt_w,lambda_zero_velocity,
     .   np,np_bin,color,crad,bufnum,sect,nsect,nspt,
     .   naal,nh,nv,npg,bin,
     .   errbuf,contbuf,num_also,n_also,n_also_bin,
     .   abs_priority,row2d,ll_n,qal_n,qal_pri,
     .   ll_z_n,ll_crty,bl_crty,apt_n,za_also_n,
     .   sl_color,sl_numline,sl_numsys,
     .   arr,x,y,yo,a,b,c,e,x_bin,y_bin,e_bin,c_bin,cff,
     .   x_also,y_also,x_also_bin,y_also_bin,
     .   y2d_also,x2d_also,
     .   y2d,yo2d,e2d,c2d,
     .   xmin,xmax,ymin,ymax,mark,xctrd,ze,za,za_also,
     .   ZeroPoint,tabgscale,
     .   xs,xe,ys,ye,ysa,yea,
     .   aablu,aared,aarsi,mfm,Xgap,XgapFraction,
     .   hist,dots,dash,quit,modified,norm,pg_ask,oo,
     .   ll_showZ,ll_showQ,show_velocity,show_coeff

C:::::::::::::::::::::::::
C  ............. Parameter and variable descriptions ...............
C  maxpt     : maximum number of points in spectrum.
C  max_also  : maximum number of additional spectra to overplot.
C  mpt2d     : maximum number of points in each row of 2 dimen. spectrum image.
C  mrw2d     : maximum number of rows in 2 dimensional spectrum image.
C  maxarr    : maximum number of points in miscellaneous arrays.
C  maxqal    : maximum number of qso absorption lines in database.
C  header    : FITS header for data spectrum.
C  temphead  : Scratch FITS header.
C  cheader   : FITS header for continuum spectrum.
C  eheader   : FITS headers for error spectrum.
C  psfile    : PostScript output filename.
C  contfile  : Continuum spectrum filename.
C  errfile   : Error spectrum filename.
C  datafile  : Data spectrum filename.
C  userfile  : file containing PGPLOT commands to be executed before plotting.
C  userfile0 : PGPLOT commands to be exec. only once before start of first plot.
C  device    :  PGPLOT device name.
C  qefile    : qso emission line database filename.
C  qafile    : qso absorption line database filename.
C  qgfile    : qso absorption line gamma values database filename.
C  aafile    : atmospheric absorption line database filename.
C  ewfile    : Equivalent widths output filename.
C  ll_file   : QAL line list filename.
C  x8,y8,w8  : real*8 scratch arrays usually used during polynomial fitting.
C  xo,xo2d   : original x values for 1d and 2d spectra.
C  ll_cent   : QAL line list centroid values.
C  ll_ew     : QAL line list equivalent width values.
C  ll_ewerr  : QAL line list equivalent width error values (1 sigma.)
C  ll_rw     : QAL line list rest wavelengths.
C  ll_ri     : QAL line list rest wavelengths.
C  ll_z      : QAL line list redshifts.
C  bl_cent   : temporary location when adding new QAL lines (centroid value.)
C  bl_ew     : temporary location when adding new QAL lines (equiv. width.)
C  bl_ewerr  : temporary location when adding new QAL lines (equiv. width err.)
C  bl_rw     : temporary location when adding new QAL lines (rest wavelength.)
C  bl_ri     : temporary location when adding new QAL lines (residual intensty.)
C  qal_ion   : qso absorption line database (ion names.)
C  qal_wav   : qso absorption line database (wavelength.)
C  qal_osc   : qso absorption line database (oscillator strength.)
C  qal_gam   : qso absorption line database (gamma value.)
C  qal_n     : qso absorption line database (number of lines.)
C  qal_pri   : qso absorption line database (priority number.)
C  apt_x     : artificial points added during polynomial fitting (x value.)
C  apt_y     : artificial points added during polynomial fitting (y value.)
C  apt_w     : artificial points added during polynomial fitting (weight value.)
C  np        : number points in primary data spectrum.
C  np_bin    : number points in primary data spectrum in binning mode.
C  color     : color of primary plotting.
C  crad      : radius used when centroiding arclamp lines.
C  bufnum    : =1 if data spectrum exists (leftover from Vista routine.)
C  sect      : boundaries of spectrum sections used when curve fitting, etc.
C  nsect     : number of sections set.
C  nspt      : number of section points (usually about twice nsect.)
C  naal      : number of atmospheric absorption lines.
C  nh        : number of plots on a page in horizontal direction.
C  nv        : number of plots on a page in vertical direction.
C  npg       : number of plot pages.
C  bin       : number of pixels per bin (in binning mode.)
C  PS_Mode   : either VPS, PS, VCPS, or CPS for portrait, landscape, color
C              portrait, or color landscape PostScript file.
C  Aux_Mode  : either "LID", or ...
C  Xgap      : Amount of space to leave at ends in X-axis-units.'
C  show_velocity : Show velocity scale on top of plot.
C  show_coeff : Show fit polynomial coeffecients during fitting.'
C  lambda_zero_velocity : The wavelength at zero velocity (when velocity scale)
C
C  parameter: maxqal     : Maximum number of QSO absorption lines.
C  parameter: maxsys     : Maximum nummer of (user) absorption systems.
C  parameter: maxsysline : Maximum number of (user) abs. sys. lines.
C
C  sl_file   : (absorption) Systems List filename.
C  sl_ion    : Systems List ion names for each line in each system.
C  sl_rw     : Systems List rest wavelength for each line in each system.
C  sl_za     : Systems List absorption redshift for each system.
C  sl_color  : Systems List color for plot display.
C  sl_numline: Systems List number of lines in a system.
C  sl_numsys : Systems List number of systems.
C
C  tabgscale : 'tab' 'g' scaling option, this is the accumulative scaling.
C
CCCCCCCCCCCCCCCCCCCCCCC
C  errbuf,contbuf,num_also,n_also,n_also_bin,
C  abs_priority,row2d,ll_n,
C  ll_z_n,ll_crty,bl_crty,apt_n,za_also_n,
C  arr,x,y,yo,a,b,c,e,x_bin,y_bin,e_bin,c_bin,cff,
C  x_also,y_also,x_also_bin,y_also_bin,
C  y2d,yo2d,e2d,c2d,
C  xmin,xmax,ymin,ymax,mark,xctrd,ze,za,za_also,ZeroPoint,
C  xs,xe,ys,ye,ysa,yea,
C  aablu,aared,aarsi,mfm,
C  .   hist,dots,quit,modified,norm,pg_ask,oo,ll_showZ,ll_showQ
CCCCCCCCCCCCCCCCCCCCCCC
