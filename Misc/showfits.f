C showfits.f
C
C
C Written in 1994 by Tom Barlow at the University of California, San Diego .
C     and revised in 1995 at the California Institute of Technology.  
C
C
      implicit none

      integer*4 mp
      parameter(mp=4000000)
      character*115200 header    ! FITS header.
      real*4 a(mp)              ! FITS data, 3000 x 3000
      real*4 b(mp)              ! scratch
      real*4 median,rms,maxval,minval,cx,cy,side,ampp,xq,yq,epoch,r1,r2
      real*4 mark_length,valread,bg,fg,x0,y0,gcx,gcy
      integer*4 rh,rm,dd,dm
      real*8 ura,udec,thesign,rs,ds,fhead,XPIX,YPIX
      real*8 ra1950,dec1950,ra2000,dec2000,tra,tdec,equinox
      character*80 arg(9),fits_file,wrd,chead,coord,s
      character*80 title,ps_file,root,addinfo,desig,id_01
      character poss*1, cc*1, c24*24
      integer*4 i,j,narg,inhead,scol,ecol,srow,erow
      integer*4 ncol,nrow,lc,ierr,i1,i2,j1,j2,k
      integer*4 level,getpos
      logical ok,findarg,ps,interactive,again,eposs,oposs,newcenter
      logical fieq,card,nomark,id_01flag
      logical dss,nohelp,green_mark,invert,dssold,firsttime

C     logical rw_fitshead,rw_fitsdata
C     integer*4 bitpix,npix,openc,closec,ifile
C     real*8 bscale,bzero

      green_mark = .false.
      nohelp   = .false.
      mark_length = 12.
      dss      = .false.
      dssold   = .false.
      side     = 0.
      ura      = 0.
      udec     = 0.
      poss     = ' '
      title    = ' '
      addinfo  = ' '
      id_01    = ' '
      newcenter= .false.
      fits_file= ' '
      ps_file  = ' '

      call arguments(arg,narg,9)
      ps  = findarg(arg,narg,'-ps',':',wrd,i)
      fieq= findarg(arg,narg,'-fi=',':',wrd,i)
      if (fieq) then
        ps_file = wrd
        ps      = .true.
      endif

      id_01flag = findarg(arg,narg,'-id',':',wrd,i)
      interactive= findarg(arg,narg,'-int',':',wrd,i)
      eposs = findarg(arg,narg,'-e',':',wrd,i)
      oposs = findarg(arg,narg,'-o',':',wrd,i)
      card  = findarg(arg,narg,'-card',':',wrd,i)
      nomark= findarg(arg,narg,'-nomark',':',wrd,i)
      invert= findarg(arg,narg,'-invert',':',wrd,i)
      level = 0
      if (findarg(arg,narg,'-faint',':',wrd,i)) level = -1
      if (findarg(arg,narg,'-bright',':',wrd,i)) level = 1
      if (findarg(arg,narg,'-side=',':',wrd,i)) then
        side = max(0.,valread(wrd))
      endif

      if ((narg.lt.1).and.(.not.eposs).and.(.not.oposs)) then
        print *,
     . 'Syntax: showfits  (FITS file rootname: *.fits)  [options]'
        print *,
     . '   Viewing and printing FITS images produced by "autoscan".'
        print *,'Options:'
        print *,
     . '  -int     : Interactive mode (zooming and marking object)'
        print *,'  -ps      : Create PostScript file (rootname.ps).'
        print *,'  -fi=name : Create PostScript file (name.ps).'
        print *,'  -side=n  : Set arcminutes on a side to n.'
        print *,'  -card    : Shrink printout to fit on card.'
        print *,
     . '  -nomark  : Do not show mark at (possible) object position.'
        print *,'  -id      : Show ID_01 header card on plot.'
        print *,'  -faint   : Optimize levels for very faint objects.'
        print *,'  -bright  : Optimize levels for bright objects.'
        print *,
     . '  -invert  : Invert black and white on PostScript picture.'
        stop
      endif

C Get root name.
      if (eposs.or.oposs) then
        if (oposs) poss = 'o'
        if (eposs) poss = 'e'
        open(3,file='object.info',status='old',iostat=ierr)
        if (ierr.ne.0) goto 903
        read(3,'(a)') wrd
        print *,'User coordinates: ',wrd(1:lc(wrd))
        call read_radec(ura,udec,wrd)
        close(3)
        call deci2hms(ura,rh,rm,rs,thesign)
        call deci2hms(udec,dd,dm,ds,thesign)
        if (thesign.gt.0.) then
          write(root,'(i2.2,i2.2,a,i2.2,i2.2,a)') rh,rm,'+',dd,dm,poss
        else
          write(root,'(i2.2,i2.2,a,i2.2,i2.2,a)') rh,rm,'-',dd,dm,poss
        endif
        print '(a,a)','Reading : ',root(1:lc(root))
      else
        root = arg(1)
      endif

      i = lc(root)
      j = index(root,'.fits')
      if (j.gt.0) then
        fits_file = root(1:i)
      else
        fits_file = root(1:i)//'.fits'
      endif
      if (ps_file.eq.' ') then
        j = index(fits_file,'.fits')
        ps_file   = fits_file(1:j)//'ps'
      endif

      call readfits(fits_file,a,mp,header,ok)
      if (.not.ok) stop

C Is this a Digital Sky Survey image?
      dss = (getpos('PLTGRADE',header).gt.0)

C Is this an old Digital Sky Survey image?
      dssold = (getpos('PLTVEC1',header).gt.0)

C Fix up header cards (must be done before image is row flipped.)
      if (dss) call fix_dss_header(header)

C Image dimensions.
      ncol = max(0,inhead('NAXIS1',header))
      nrow = max(0,inhead('NAXIS2',header))
      scol = max(0,inhead('CRVAL1',header))
      srow = max(0,inhead('CRVAL2',header))
      ecol = scol + ncol - 1
      erow = srow + nrow - 1
      x0   = float(scol)
      y0   = float(srow)
      print '(a)','  SCOL  ECOL  SROW  EROW  NCOL  NROW'
      print '(6(i6))',scol,ecol,srow,erow,ncol,nrow

C Flip rows.
      if (dss) call dss_rowflip(a,ncol,nrow)

C Look in header for information.
      ura = fhead('RA',header)
      udec= fhead('DEC',header)
      if (ura.gt.0.) then 
        call write_radec(ura,udec,coord,4)
      else
        coord = ' '
      endif
      epoch   = sngl(fhead('EPOCH',header))
      if (epoch.lt.0.) epoch = 1950.0
      addinfo = chead('ADDINFO',header)
      id_01   = chead('ID_01',header)
      wrd     = chead('POSS',header)
      call lower_case(wrd)
      call strpos(wrd,i,k)
      poss    = wrd(i:i)
      ampp    = sngl(fhead('ASPP',header)) / 60.
      if (ampp.lt.0.) ampp = 0.0473333
      xq      = max(-1.e+6,sngl(fhead('OBJCOL',header)))
      yq      = max(-1.e+6,sngl(fhead('OBJROW',header)))
      if (dss) yq = ( float(erow) - yq ) + float(srow)

C Set cursor near center of screen or at least near object.
      if (xq.gt.-1.e+5) then
        cx= ( 1. + xq - x0 ) * ampp
        cy= ( 1. + yq - y0 ) * ampp
      else
        r1= float(scol+ecol)/2. 
        r2= float(srow+erow)/2. 
        cx= ( 1. + r1 - x0 ) * ampp
        cy= ( 1. + r2 - y0 ) * ampp
      endif

C If side not given, set side to 15' with object found, 25' without.
      if (side.eq.0.) then
        if (xq.gt.-1.e+5) then
          side = min(15.,float(ncol)*ampp)
        else
          side = min(25.,float(ncol)*ampp)
        endif
      endif

C Set region of image to display (see PGGRAY).
      i1 = nint ( ( cx - 0.5*side ) / ampp )
      i2 = nint ( ( cx + 0.5*side ) / ampp )
      j1 = nint ( ( cy - 0.5*side ) / ampp )
      j2 = nint ( ( cy + 0.5*side ) / ampp )

C Create sideways page edge label designation...
      i =index(addinfo,'  ')
      if (addinfo(1:2).eq.'V=') i=0
      call deci2hms(ura,rh,rm,rs,thesign)
      call deci2hms(udec,dd,dm,ds,thesign)
      cc = poss
      call upper_case(cc)
      if (cc.eq.'X') cc='E'
      if (udec.ge.0.) then
        write(desig,'(i2.2,i2.2,a,i1,a,i2.2,i2.2,3x,a,3x,a)')
     .     rh,rm,'.',min(9,int(rs/6.d0)),'+',dd,dm,addinfo(1:i),cc
      else
        write(desig,'(i2.2,i2.2,a,i1,a,i2.2,i2.2,3x,a,3x,a)')
     .     rh,rm,'.',min(9,int(rs/6.d0)),'-',dd,dm,addinfo(1:i),cc
      endif

C Find background level.
      call find_median_rms(a,ncol,nrow,b,mp,median,rms,minval,maxval)
      print '(a,f10.3)',
     . 'Median of image   =',min(999999.,max(-99999.,median))
      print '(a,f10.3)',
     . 'RMS of background =',min(999999.,max(-99999.,rms))
      print '(a,f10.3)',
     . 'Minimum data value=',min(999999.,max(-99999.,minval))
      print '(a,f10.3)',
     . 'Maximum data value=',min(999999.,max(-99999.,maxval))

C First time for plot indicates we should set up green mark (see below).
      firsttime = .true.

C Initiate PGPLOT window.
1     if (ps) then
        wrd = ps_file(1:lc(ps_file))//'/VPS'
        call PGBEGIN(0,wrd,1,1)
        if (card) then
          call PGSVP(0.20,0.80,0.31,0.69)
        else
          call PGSVP(0.05,0.94,0.05,0.95)
        endif
        green_mark = .false.
      else
        call PGBEGIN(0,'/XWINDOW',1,1)
        call PGSVP(0.1,0.9,0.1,0.9)
      endif
      call PGASK(.false.)

C Display grayscale image in PGPLOT window.
2     continue
      call pg_showim(a,ncol,nrow,median,rms,minval,maxval,ps,level,
     .                       i1,i2,j1,j2,ampp,bg,fg,invert)

C Draw small tick marks near possible object position.
      if ((xq.gt.-1.e+5).and.(.not.nomark)) then
        call PGSLW(3)
        call mark_object(a,ncol,nrow,xq,yq,x0,y0,mark_length,ampp)
      endif

C If this is the first plot and this a DSS image 
C set up green mark at object position.
      if (firsttime.and.dss) then
        XPIX = dble((cx/ampp)+x0-1.0)
        YPIX = dble((cy/ampp)+y0-1.0)
        YPIX = ( dfloat(erow) - YPIX ) + dfloat(srow)
        tra = ura
        tdec= udec
        equinox = epoch 
        print *,'Using epoch=',equinox
        if (abs(equinox-2000.d0).gt.0.001) then
          call precess(equinox,2000.d0,tra,tdec)
        endif
        call write_radec(tra,tdec,wrd,5)
        print '(a,a)','Assuming J2000 = ',wrd(1:lc(wrd))
        call dss_radec_to_pix(header,tra,tdec,XPIX,YPIX)
        print '(a,2(f10.3))',' DSS Image pixel (x,y):',XPIX,YPIX
        XPIX = XPIX - 0.5d0
        YPIX = YPIX - 0.5d0
        print '(a,2(f10.3))','     Image pixel (x,y):',XPIX,YPIX
C Re-compensate for row flip to calculate cursor position and data value.
        YPIX = dfloat(erow) - ( YPIX - dfloat(srow) )
        r1 = (sngl(XPIX)-x0+1.0)*ampp
        r2 = (sngl(YPIX)-y0+1.0)*ampp
        i  = ((nint(YPIX)-nint(y0))*ncol) + (nint(XPIX)-nint(x0)) + 1
        print '(11x,a,f10.3)','Data value :',min(1.e+6,max(-1.e+5,a(i)))
        print '(a,2(f10.3))','    Cursor pixel (x,y):',r1,r2
        call PGSCH(6.0)
        call PGSCI(3)
        if (.not.ps) call PGPT(1,r1,r2,5)
        call PGSCH(1.0)
        call PGSCI(1)
        if (.not.ps) green_mark = .true.
        gcx = r1
        gcy = r2
        firsttime = .false.
      endif

C Draw green mark if "@" command was used.
      if (green_mark) then
        call PGSCH(6.0)
        call PGSCI(3)
        call PGPT(1,gcx,gcy,5)
        call PGSCH(1.0)
        call PGSCI(1)
      endif

C Draw labels around plot.
      call PGSLW(2)
      call PGSCF(2)
      call PGSCH(1.0)
      call PGMTEXT('B',2.7,0.5,0.5,'Arc-minutes')
      if (poss.eq.'o') then
        call PGMTEXT('B',2.7,1.0,1.0,'POSS-O')
      elseif (poss.eq.'e') then
        call PGMTEXT('B',2.7,1.0,1.0,'POSS-E')
      elseif (poss.eq.'j') then
        call PGMTEXT('B',2.7,1.0,1.0,'SERC-J (DSS)')
      elseif (poss.eq.'x') then
        call PGMTEXT('B',2.7,1.0,1.0,'POSS-E (DSS)')
      endif
      write(title,'(a,a,i4,a)') coord(1:lc(coord)),'  (',nint(epoch),')'
      if (ps) then
        call PGSCH(0.95)
        call PGMTEXT('T',1.0,0.5,0.5,'NORTH')
        call PGMTEXT('L',1.4,0.70,0.5,'EAST')
        call PGSCH(1.0)
        call PGMTEXT('T',3.3,0.5,0.5,addinfo)
        if (id_01flag) call PGMTEXT('B',5.0,0.5,0.5,id_01)
        call PGSCH(1.4)
        call PGMTEXT('T',4.7,0.5,0.5,title)
        call PGSCH(1.2)
        if (card) then
          call PGMTEXT('R',2.1,0.1,0.0,desig)
        else
          call PGMTEXT('R',1.95,0.6,0.0,desig)
        endif
        call PGSCH(1.0)
      else
        call PGMTEXT('T',-1.2,0.5,0.5,'North')
        call PGMTEXT('L',1.8,0.5,0.5,'East')
        call PGMTEXT('T',1.2,0.5,0.5,addinfo)
        if (id_01flag) call PGMTEXT('B',5.0,0.5,0.5,id_01)
        call PGSCH(1.2)
        call PGMTEXT('T',2.6,0.5,0.5,title)
        call PGMTEXT('R',2.1,0.4,0.0,desig)
        call PGSCH(1.0)
      endif

      IF ((interactive).and.(.not.ps)) THEN

C::::::::::::: START OF COMMAND TREE :::::::::::::::::::::

3     continue
      if (nohelp) then
        nohelp=.false.
      else
        call help_menu(dss)
      endif

4     continue
      call PGCURSE(cx,cy,cc)
      call allcaps(cc)

      if (cc.eq.'A') then
701     continue
        if (addinfo.eq.' ') then
          print '(a,$)',' Enter comment: '
        else
          print '(2a)',' Old comment: ',addinfo(1:lc(addinfo))
          print '(a,$)',' Enter new comment: '
        endif
        read(5,'(a)',err=701) addinfo
        call cheadset('ADDINFO',addinfo,header)
        print *,'Comment added to FITS header as "ADDINFO header card.'
        newcenter = .true.
        nohelp=.true.
        goto 2

      elseif (cc.eq.'N') then
        level = 0
        nohelp=.true.
        goto 2

      elseif (cc.eq.'B') then
        level = 1
        nohelp=.true.
        goto 2

      elseif (cc.eq.'F') then
        level = -1
        nohelp=.true.
        goto 2

      elseif (cc.eq.'L') then
        print '(2(a,f7.1))',
     . '      Median =',min(99999.,max(-9999.,median)),
     . '     RMS =',min(99999.,max(-9999.,rms))
        print '(2(a,i5))',
     . 'Current zero =',min(99999,max(-9999,nint(bg))),
     . '  span =',min(99999,max(-9999,nint(fg-bg)))
        level = -9
        print '(a,$)',' Enter zero and span : '
        read(5,*,err=907) bg,fg
        fg = bg + fg
        nohelp=.true.
        goto 2

      elseif (cc.eq.'X') then
        r1 = (1.+xq-x0) * ampp
        r2 = (1.+yq-y0) * ampp
        call PGSCH(6.0)
        call PGSCI(2)
        call PGPT(1,r1,r2,5)
        call PGSCH(1.0)
        call PGSCI(1)
        goto 4

      elseif (cc.eq.'S') then
        print '(a)','Center will be last cursor position.'
        print '(a,$)','Specify arc-minutes on a side : '
        read(5,*,err=907) r1
        side = max(0.1,min(300.,r1))
        i1 = nint ( ( cx - 0.5*side ) / ampp )
        i2 = nint ( ( cx + 0.5*side ) / ampp )
        j1 = nint ( ( cy - 0.5*side ) / ampp )
        j2 = nint ( ( cy + 0.5*side ) / ampp )
        nohelp=.true.
        goto 2

      elseif (cc.eq.'I') then
        side = 3.0
        i1 = nint ( ( cx - 0.5*side ) / ampp )
        i2 = nint ( ( cx + 0.5*side ) / ampp )
        j1 = nint ( ( cy - 0.5*side ) / ampp )
        j2 = nint ( ( cy + 0.5*side ) / ampp )
        nohelp=.true.
        goto 2

      elseif (cc.eq.'O') then
        i1 = 1
        i2 = ncol
        j1 = 1
        j2 = nrow
        side = float(ncol) * ampp
        nohelp=.true.
        goto 2

      elseif (cc.eq.'P') then
        xq = (cx/ampp)+x0-1.
        yq = (cy/ampp)+y0-1.
        call PGSLW(3)
        call mark_object(a,ncol,nrow,xq,yq,x0,y0,mark_length,ampp)
        newcenter = .true.
        side=15.
        i1 = nint ( ( cx - 0.5*side ) / ampp )
        i2 = nint ( ( cx + 0.5*side ) / ampp )
        j1 = nint ( ( cy - 0.5*side ) / ampp )
        j2 = nint ( ( cy + 0.5*side ) / ampp )
        call PGEND
        ps = .true.
        goto 1

      elseif (cc.eq.'M') then
        xq = (cx/ampp)+x0-1.
        yq = (cy/ampp)+y0-1.
        call PGSLW(3)
        call mark_object(a,ncol,nrow,xq,yq,x0,y0,mark_length,ampp)
        newcenter = .true.
        print '(a,$)',
     . 'Specify new arc-minutes on a side (0=do not redraw) : '
        read(5,*,err=907) r1
        if (r1.lt.0.0001) goto 4
        side = max(0.1,min(300.,r1))
        i1 = nint ( ( cx - 0.5*side ) / ampp )
        i2 = nint ( ( cx + 0.5*side ) / ampp )
        j1 = nint ( ( cy - 0.5*side ) / ampp )
        j2 = nint ( ( cy + 0.5*side ) / ampp )
        nohelp=.true.
        goto 2

      elseif (cc.eq.'C') then
        print *,'Centroiding on object...'
        xq = (cx/ampp)+x0-1.
        yq = (cy/ampp)+y0-1.
        call quick_centroid(a,scol,ecol,srow,erow,xq,yq)
        call PGSLW(3)
        call mark_object(a,ncol,nrow,xq,yq,x0,y0,mark_length,ampp)
        newcenter = .true.
        print '(a,$)',
     . 'Specify new arc-minutes on a side (0=do not redraw) : '
        read(5,*,err=907) r1
        if (r1.lt.0.0001) goto 4
        side = max(0.1,min(300.,r1))
        i1 = nint ( ( cx - 0.5*side ) / ampp )
        i2 = nint ( ( cx + 0.5*side ) / ampp )
        j1 = nint ( ( cy - 0.5*side ) / ampp )
        j2 = nint ( ( cy + 0.5*side ) / ampp )
        nohelp=.true.
        goto 2

      elseif (cc.eq.'Z') then
        print *,'Centroiding on object...'
        xq = (cx/ampp)+x0-1.
        yq = (cy/ampp)+y0-1.
        call quick_centroid(a,scol,ecol,srow,erow,xq,yq)
        call PGSLW(3)
        call mark_object(a,ncol,nrow,xq,yq,x0,y0,mark_length,ampp)
        newcenter = .true.
        side=15.0
        i1 = nint ( ( cx - 0.5*side ) / ampp )
        i2 = nint ( ( cx + 0.5*side ) / ampp )
        j1 = nint ( ( cy - 0.5*side ) / ampp )
        j2 = nint ( ( cy + 0.5*side ) / ampp )
        call PGEND
        ps = .true.
        goto 1

      elseif (cc.eq.'R') then
        print '(a,f6.2)','Old mark length =',mark_length
        print '(a,$)',   'New mark length : '
        read(5,*,err=907) r1
        mark_length = min(99.,max(1.,r1))
        print '(a,f6.2)','New mark length =',mark_length
        print '(a,$)',
     . 'Specify new arc-minutes on a side (0=do not redraw) : '
        read(5,*,err=907) r1
        if (r1.lt.0.0001) goto 4
        side = max(0.1,min(300.,r1))
        i1 = nint ( ( cx - 0.5*side ) / ampp )
        i2 = nint ( ( cx + 0.5*side ) / ampp )
        j1 = nint ( ( cy - 0.5*side ) / ampp )
        j2 = nint ( ( cy + 0.5*side ) / ampp )
        nohelp=.true.
        goto 2

      elseif (cc.eq.'D') then
        print '(a,a)','Old coordinates = ',coord(1:lc(coord))
        print '(a,$)','New coordinates : '
        read(5,'(a)',err=907) wrd
        call read_radec(ura,udec,wrd)
        call write_radec(ura,udec,coord,4)
        s = ' '
        call write_radec(ura,udec,s,5)
        call cheadset('OBJECT',s(1:lc(s)),header)
        print '(a,a)','New coordinates = ',s(1:lc(s))
        wrd = ' '
        wrd = s(1:2)//':'//s(4:5)//':'//s(7:11)
        call cheadset('RA',wrd(1:11),header)
        wrd = ' '
        wrd = s(14:16)//':'//s(18:19)//':'//s(21:24)
        call cheadset('DEC',wrd(1:11),header)
        newcenter = .true.
        nohelp=.true.
        print *,
     . 'You should now exit (do not abort) and rerun showfits.'
        goto 2

      elseif (cc.eq.'G') then
        if (green_mark) then
          green_mark = .false.
        else
          green_mark = .true.
        endif
        print *,' '
        nohelp=.true.
        goto 2

      elseif ((cc.eq.'@').or.(cc.eq.'2')) then
        if (dss) then
          XPIX = dble((cx/ampp)+x0-1.0)
          YPIX = dble((cy/ampp)+y0-1.0)
          YPIX = ( dfloat(erow) - YPIX ) + dfloat(srow)
          wrd=' '
          print '(a,$)','Enter RA (HH MM SS) and DEC (DD MM SS) : '
          read(5,'(a)',err=907) wrd
          call read_radec(tra,tdec,wrd)
          print '(a,$)','Enter EQUINOX (YYYY) : '
          read(5,*,err=907) equinox
          if (abs(equinox-2000.d0).gt.0.001) then
            call precess(equinox,2000.d0,tra,tdec)
          endif
          call write_radec(tra,tdec,wrd,5)
          print '(a,a)','Assuming J2000 = ',wrd(1:lc(wrd))
          call dss_radec_to_pix(header,tra,tdec,XPIX,YPIX)
          print '(a,2(f10.3))',' DSS Image pixel (x,y):',XPIX,YPIX
          XPIX = XPIX - 0.5d0
          YPIX = YPIX - 0.5d0
          print '(a,2(f10.3))','     Image pixel (x,y):',XPIX,YPIX
C Re-compensate for row flip to calculate cursor position and data value.
          YPIX = dfloat(erow) - ( YPIX - dfloat(srow) )
          r1 = (sngl(XPIX)-x0+1.0)*ampp
          r2 = (sngl(YPIX)-y0+1.0)*ampp
          i  = ((nint(YPIX)-nint(y0))*ncol) + (nint(XPIX)-nint(x0)) + 1
          print '(11x,a,f10.3)',
     . 'Data value :',min(1.e+6,max(-1.e+5,a(i)))
          print '(a,2(f10.3))','    Cursor pixel (x,y):',r1,r2
          call PGSCH(6.0)
          call PGSCI(3)
          call PGPT(1,r1,r2,5)
          call PGSCH(1.0)
          call PGSCI(1)
          green_mark = .true.
          gcx = r1
          gcy = r2
        else
          print *,'Must be a DSS image.'
        endif
        print *,' '
        goto 4

      elseif ((cc.eq.'+').or.(cc.eq.'=')) then
        XPIX = dble((cx/ampp)+x0-1.0)
        YPIX = dble((cy/ampp)+y0-1.0)
        i  = ((nint(YPIX)-nint(y0))*ncol) + (nint(XPIX)-nint(x0)) + 1
        print '(11x,a,f10.3)','Data value :',min(1.e+6,max(-1.e+5,a(i)))
        print '(a,2(f10.3))','    Cursor pixel (x,y):',cx,cy 
        if (.not.dss) then
          print '(a,2(f10.3))','     Image pixel (x,y):',XPIX,YPIX
        endif
        if (dss) then
C Compensate for row flip for DSS images only.
          YPIX = ( dfloat(erow) - YPIX ) + dfloat(srow)
          print '(a,2(f10.3))','     Image pixel (x,y):',XPIX,YPIX
C Compensate for DSS convention.
          XPIX = XPIX + 0.5d0
          YPIX = YPIX + 0.5d0
          print '(a,2(f10.3))',' DSS Image pixel (x,y):',XPIX,YPIX
          call dss_pix_to_radec(header,XPIX,YPIX,ra2000,dec2000) 
          ra1950 =ra2000
          dec1950=dec2000
          call precess(2000.d0,1950.d0,ra1950,dec1950)
          call write_radec(ra2000,dec2000,c24,5)
          print '(a,a)',' J2000 : ',c24
          call write_radec(ra1950,dec1950,c24,5)
          print '(a,a)','  1950 : ',c24
        endif
        print *,' '
        goto 4

      elseif ((cc.eq.'*').or.(cc.eq.'8')) then
        print *,'Centroiding...'
        r1 = (cx/ampp)+x0-1.
        r2 = (cy/ampp)+y0-1.
        call quick_centroid(a,scol,ecol,srow,erow,r1,r2)
        cx = (1.+r1-x0)*ampp
        cy = (1.+r2-y0)*ampp
        call PGSCH(6.0)
        call PGSCI(2)
        call PGPT(1,cx,cy,5)
        call PGSCH(1.0)
        call PGSCI(1)
        XPIX = dble((cx/ampp)+x0-1.0)
        YPIX = dble((cy/ampp)+y0-1.0)
        i  = ((nint(YPIX)-nint(y0))*ncol) + (nint(XPIX)-nint(x0)) + 1
        print '(11x,a,f10.3)','Data value :',min(1.e+6,max(-1.e+5,a(i)))
        print '(a,2(f10.3))','    Cursor pixel (x,y):',cx,cy 
        if (.not.dss) then
          print '(a,2(f10.3))','     Image pixel (x,y):',XPIX,YPIX
        endif
        if (dss) then
C Compensate for row flip for DSS images only.
          YPIX = ( dfloat(erow) - YPIX ) + dfloat(srow)
          print '(a,2(f10.3))','     Image pixel (x,y):',XPIX,YPIX
C Compensate for DSS convention.
          XPIX = XPIX + 0.5d0
          YPIX = YPIX + 0.5d0
          print '(a,2(f10.3))',' DSS Image pixel (x,y):',XPIX,YPIX
          call dss_pix_to_radec(header,XPIX,YPIX,ra2000,dec2000) 
          ra1950 =ra2000
          dec1950=dec2000
          call precess(2000.d0,1950.d0,ra1950,dec1950)
          call write_radec(ra2000,dec2000,c24,5)
          print '(a,a)',' J2000 : ',c24
          call write_radec(ra1950,dec1950,c24,5)
          print '(a,a)','  1950 : ',c24
        endif
        print *,' '
        goto 4

      elseif ((cc.ne.' ').and.(ichar(cc).ne.27)) then
        goto 3

      endif

C::::::::::::: END OF COMMAND TREE :::::::::::::::::::::::::::::::

      ENDIF

      again = .false.
      if (.not.ps) then
        print '(a,$)','Do you want a PostScript file (y/n/a) ? '
        read(5,'(a)') wrd
        call allcaps(wrd)
        if (index(wrd,'Y').gt.0) again = .true.
        if (index(wrd,'A').gt.0) goto 990
      endif

      call PGEND

      if (again) then
        ps = .true.
        goto 1
      endif

      if (ps) then
        print *,'PostScript file: '//ps_file(1:lc(ps_file))
      endif
      if ((ps).and.(.not.fieq)) then
        print '(a,$)','Print PostScript file (y/n) ? '
        read(5,'(a)',err=907) wrd
        call allcaps(wrd)
        if (index(wrd,'Y').gt.0) call system('lpr '//ps_file)
      endif

C Add object position to header if new center was selected.
C Write new image to disk, make adjustments to DSS image.
      if (newcenter) then

        if (dss) then
          yq = ( float(erow) - yq ) + float(srow)
          call dss_rowflip(a,ncol,nrow)
        endif
        print *,'Adding new object position to header...'
        call fheadset('OBJCOL',dble(xq),header) 
        call fheadset('OBJROW',dble(yq),header) 

        call writefits(fits_file,a,header,ok)

      endif

      stop
903   print *,'Error- could not open or read object.info file.'
      close(3)
      stop
907   print *,'Error reading input.'
      goto 990
990   print *,'Aborting...'
      stop
992   print *,'Error reading info file.'
      stop
      end

C-------------------------------------------------------------------

      subroutine pg_showim(a,ncol,nrow,median,rms,minval,maxval,
     .                     ps,level,i1,i2,j1,j2,ampp,bg,fg,invert)

      implicit none
      integer*4 ncol,nrow,i1,i2,j1,j2,level
      real*4 a(ncol,nrow),median,rms,minval,maxval,ampp
      real*4 xmin,xmax,ymin,ymax,tr(6),bg,fg,r
      logical ps,invert

      tr(1)=0.
      tr(2)=ampp
      tr(3)=0.
      tr(4)=0.
      tr(5)=0.
      tr(6)=ampp

      xmin = float(i1)*ampp
      xmax = float(i2)*ampp
      ymin = float(j1)*ampp
      ymax = float(j2)*ampp

      call PGPAGE
      call PGSLW(3)
      call PGSCH(0.7)
      call PGSCF(1)
      call PGWNAD(xmin,xmax,ymax,ymin)           ! invert y-axis
      r = abs(xmax-xmin)
      if (r.gt.10.) then
        call PGBOX('BCNSTI',5.,5,'BCNSTI',5.,5)
      else
        call PGBOX('BCNSTI',1.,6,'BCNSTI',1.,6)
      endif
      call PGSCH(1.0)

      if (level.eq.-1) then
        bg = max(minval*0.5,median - (4.*rms))
        fg = min(maxval*1.5,median + (12.*rms))
      elseif (level.eq.1) then
        bg = max(minval*0.5,median - (10.*rms))
        fg = min(maxval*1.5,median + (34.*rms))
      elseif (level.ne.-9) then
        if (ps) then
          bg = max(minval*0.5,median - (6.*rms))
          fg = min(maxval*1.5,median + (23.*rms))
        else
          bg = max(0.,median - (10.*rms))
          fg = min(maxval,median + (23.*rms))
        endif
      endif

      i2 = max(i1+3,i2)
      j2 = max(j1+3,j2)
      i1 = max(1,i1)
      i2 = min(ncol,i2)
      j1 = max(1,j1)
      j2 = min(nrow,j2)

      if (.not.invert) then
C This is the normal convention.
        call PGGRAY(a,ncol,nrow,i1,i2,j1,j2,fg,bg,tr)
      else
C This is the "normal inverted" PostScript.
        call PGGRAY(a,ncol,nrow,i1,i2,j1,j2,bg,fg,tr)
      endif

      return
      end


C---------------------------------------------------------------------
C Draw finding chart type marks on image at angles with minimal flux.
C Assume scale on graph in arc-minutes: graph position = pixel# * ampp.

      subroutine mark_object(a,ncol,nrow,uxq,uyq,x0,y0,radius,ampp)

      implicit none
      integer*4 ncol,nrow
      real*4 a(ncol,nrow)
      real*4 uxq,uyq          ! position of object in pixels
      real*4 x0,y0            ! starting column, starting row
      real*4 radius           ! radius of marks in pixels
      real*4 ampp             ! arc-minutes per pixel

      real*4 flux(36),deg(36),tan_angle,d,x,y,r,d1,d2,degmin,fmin
      real*4 xx(2),yy(2),sum,r1,r2,xq,yq
      real*8 DEGTORAD
      integer*4 i,j,n,x1,x2,y1,y2,imin,num
      logical between

C Find position as if image started at pixel 1,1.
      xq = 1. + uxq - x0
      yq = 1. + uyq - y0

C Region used for determining position of marks.
      x1 = max(1,min(ncol,nint( xq - radius - 1.)))
      x2 = max(1,min(ncol,nint( xq + radius + 1.)))
      y1 = max(1,min(nrow,nint( yq - radius - 1.)))
      y2 = max(1,min(nrow,nint( yq + radius + 1.)))

C Calculate mean fluxes at various angles.
      do n=1,36
        d1=(float(n-1)*10.) - 15.
        d2=(float(n)*10.) + 15.
        num=0
        sum=0.
        do i=x1,x2
          x=float(i)
          do j=y1,y2
            y=float(j)
            r=sqrt((xq-x)*(xq-x)+(yq-y)*(yq-y))
            d=tan_angle(x-xq,y-yq)
            if ((r.le.radius).and.(between(d,d1,d2))) then
              sum = sum + a(i,j)
              num = num + 1
            endif
          enddo
        enddo
        flux(n) = sum / float(max(1,num))
        deg(n)  = float(n)*10. - 5
      enddo
      n=36

C Bubble sort flux.
      do i=1,n-1
        fmin=flux(i)
        imin=i
        do j=i,n
          if (flux(j).lt.fmin) then
            fmin=flux(j)
            imin=j
          endif
        enddo
        flux(imin) = flux(i)
        flux(i)    = fmin
        degmin     = deg(imin)
        deg(imin)  = deg(i)
        deg(i)     = degmin
      enddo

C Choose best angles.
      j=0
      i=1
      do while(j.eq.0)
        i=i+1
        d = abs(deg(i)-deg(1))
        if (d.gt.180.) d=360.-d
        if (d.gt.70.) j=i
      enddo

C Draw marks.
      r1= 0.30 * (4.733e-2/ampp)
      r2= 0.70 * (4.733e-2/ampp)

      d = sngl( DEGTORAD( dble( deg(1) ) ) )
      xx(1) = ( xq + ( r1 * radius * cos(d) ) ) * ampp
      yy(1) = ( yq + ( r1 * radius * sin(d) ) ) * ampp
      xx(2) = ( xq + ( r2 * radius * cos(d) ) ) * ampp
      yy(2) = ( yq + ( r2 * radius * sin(d) ) ) * ampp
      call PGLINE(2,xx,yy)

      d = sngl( DEGTORAD( dble( deg(j) ) ) )
      xx(1) = ( xq + ( r1 * radius * cos(d) ) ) * ampp
      yy(1) = ( yq + ( r1 * radius * sin(d) ) ) * ampp
      xx(2) = ( xq + ( r2 * radius * cos(d) ) ) * ampp
      yy(2) = ( yq + ( r2 * radius * sin(d) ) ) * ampp
      call PGLINE(2,xx,yy)

      return
      end

C---------------------------------------------------------------
      real*4 function tan_angle(x,y)
      implicit none
      real*4 x,y,d
      real*8 DEGTORAD
      d = DEGTORAD( dble( abs(y/max(0.000001,abs(x))) ) )
      d = atan( d )
      if ((y.lt.0.).and.(x.gt.0.)) d = 360. - d
      if ((x.lt.0.).and.(y.gt.0.)) d = 180. - d
      if ((x.lt.0.).and.(y.lt.0.)) d = 180. + d
      tan_angle = d
      return
      end

C-------------------------------------------------------------------

      subroutine quick_centroid(a,scol,ecol,srow,erow,xq,yq)

      implicit none
      integer*4 scol,ecol,srow,erow,i,j,n
      real*4 a(scol:ecol,srow:erow)
      real*4 xq,yq,b(2500),median,x,y,f,w,xc,yc
      integer*4 x1,x2,y1,y2,iter

      iter = 0
1     continue
      x1=max(scol,min(ecol,nint(xq-23.)))
      x2=max(scol,min(ecol,nint(xq+23.)))
      y1=max(srow,min(erow,nint(yq-23.)))
      y2=max(srow,min(erow,nint(yq+23.)))
      n=0
      do i=x1,x2
        do j=y1,y2
          n=n+1
          b(n) = a(i,j)
        enddo
      enddo
      call find_median(b,n,median)
      median = b(max(1,n/10))
      x1=max(scol,min(ecol,nint(xq-3.)))
      x2=max(scol,min(ecol,nint(xq+3.)))
      y1=max(srow,min(erow,nint(yq-3.)))
      y2=max(srow,min(erow,nint(yq+3.)))
      x=0.
      y=0.
      f=0.
      do i=x1,x2
        do j=y1,y2
          w = max(0.,a(i,j)-median)
          f = f + w
          x = x + float(i)*w
          y = y + float(j)*w
        enddo
      enddo
      xc = x / f
      yc = y / f
      if ((((xc-xq)*(xc-xq))+((yc-yq)*(yc-yq))).gt.1.e-6) then
        xq = xc
        yq = yc
        iter=iter+1
        if (iter.lt.10) goto 1
      endif
      xq = xc
      yq = yc
   
      return
      end

C------------------------------------------------------------------
C Is angle d between angles d1 and d2?
C Assume that the differences between all angles are acute.

      logical function between(d,d1,d2)

      implicit none
      real*4 d,d1,d2,t,t1,t2,sep,sep1,sep2
      t = d
      t1= d1
      t2= d2
      if (t.lt.0.)  t =t+360.
      if (t1.lt.0.) t1=t1+360.
      if (t2.lt.0.) t2=t2+360.
      if (t.gt.360.)  t =t-360.
      if (t1.gt.360.) t1=t1-360.
      if (t2.gt.360.) t2=t2-360.
      sep = abs(t1-t2)
      if (sep.gt.180.) sep = 360. - sep
      sep1 = abs(t-t1)
      if (sep1.gt.180.) sep1 = 360. - sep1
      sep2 = abs(t-t2)
      if (sep2.gt.180.) sep2 = 360. - sep2
      between = ((sep1.le.sep).and.(sep2.le.sep))
      return
      end


C---------------------------------------------------------------------
C Fix up DSS header to include stuff autoscan would have put in.
      subroutine fix_dss_header(header)
      implicit none
      character*(*) header
      character*80 card1,card2,card3,chead
      character s*24
      integer*4 i,lc,inhead,ierr,getpos
      real*8 ra,dec,ura,udec,r,fhead

      print *,'Adding cards to DSS FITS header.'

C Set CRVALs.
      i = inhead('CNPIX1',header)
      call inheadset('CRVAL1',i,header)
      i = inhead('CNPIX2',header)
      call inheadset('CRVAL2',i,header)
C Set 1950.0 RA and DEC.
      card1 = chead('OBJCTRA',header)
      card2 = chead('OBJCTDEC',header)
      card3 = card1(1:lc(card1))//' '//card2(1:lc(card2))
      call read_radec(ra,dec,card3)
      call precess(2000.d0,1950.d0,ra,dec)
      call write_radec(ra,dec,s,5)
      card1 = ' '
      card1 = s(1:2)//':'//s(4:5)//':'//s(7:11)
      if (getpos('RA',header).le.0) then
        call cheadset('RA',card1(1:11),header)
      endif
      card2 = ' '
      card2 = s(14:16)//':'//s(18:19)//':'//s(21:24)
      if (getpos('DEC',header).le.0) then
        call cheadset('DEC',card2(1:11),header)
      endif
      r = fhead('EPOCH',header)
      if (getpos('PLTEPOCH',header).le.0) then
        call fheadset('PLTEPOCH',r,header)
      endif
      call inheadset('EPOCH',1950,header) 
C Look for object.info file.
      if (getpos('ADDINFO',header).le.0) then
        call cheadset('ADDINFO',' ',header)
      endif
      if (getpos('ID_01',header).le.0) then
        call cheadset('ID_01',' ',header)
      endif
      open(3,file='object.info',status='old',iostat=ierr)
      if (ierr.eq.0) then
        read(3,'(a)',end=5) card1
        call read_radec(ura,udec,card1)
        call as_ra_dec(ra,dec,ura,udec,r)
        if (r.lt.10.) then
          read(3,'(a)',end=5) card1
          i=lc(card1)
          call cheadset('ADDINFO',card1(1:i),header)
          read(3,'(a)',end=5) card2
          i=max(5,lc(card2))
          call cheadset('ID_01',card1(5:i),header)
        endif
5       close(3)
      endif
C Miscellaneous.
      card1 = chead('TELESCOP',header)
      if ((index(card1,'lomar').gt.0).or.
     .    (index(card1,'LOMAR').gt.0)) then
        call cheadset('POSS','x',header)
      else
        call cheadset('POSS','j',header)
      endif
      call find_dss_aspp(header,r)
      call fheadset('ASPP',r,header)
      print '(a,f9.4)','Arcseconds per pixel =',r
      return
      end

C-----------------------------------------------------------------------------
C Calculate the arcseconds per pixel (ASPP) for a DSS image.
C This must be done BEFORE image is row flipped.
C
      subroutine find_dss_aspp(header,aspp)
      implicit none
      character*(*) header
      integer*4 scol,srow,ncol,nrow,inhead
      real*8 aspp,ra1,dec1,ra2,dec2,XPIX,YPIX,r
      scol = inhead('CRVAL1',header)
      srow = inhead('CRVAL2',header)
      ncol = inhead('NAXIS1',header)
      nrow = inhead('NAXIS2',header)
      XPIX = dfloat(scol + ncol/2)
      YPIX = dfloat(srow + nrow/2)
      call dss_pix_to_radec(header,XPIX,YPIX,ra1,dec1) 
      XPIX = XPIX + 10.d0
      YPIX = YPIX + 10.d0
      call dss_pix_to_radec(header,XPIX,YPIX,ra2,dec2) 
      call as_ra_dec(ra1,dec1,ra2,dec2,r)
      aspp = (r*60.d0) / dsqrt(200.d0)
      return
      end

      
C-------------------------------------------------------------------------
      subroutine help_menu(dss)
      implicit none
      logical dss
      print *,' '
      print *,'Hit key for option:'
      print *,'   "i" = zoom in (5 arcminutes on a side)'
      print *,'   "o" = zoom out (whole image)'
      print *,'   "s" = set arcminutes on a side (zoom image)'
      print *,'   "m" = mark and set object position'
      print *,
     . '   "p" = mark, set object then quit (with 15 arcminute field)'
      print *,'   "x" = draw an X at the current object position'
      print *,'   "c" = centroid, mark, and set object position'
      print *,
     . '   "z" = centroid, mark, set, then quit (with 15 arcmin field)'
      print *,'   "f" = adjust levels for faint objects'
      print *,'   "b" = adjust levels for bright objects'
      print *,'   "n" = adjust levels for normal objects'
      print *,'   "l" = adjust levels manually'
      print *,'   "r" = change mark radius'
      print *,'   "d" = change RA and DEC in FITS header'
      print *,
     . '   "g" = show/erase green X (green X appears on screen only)'
      print *,'   "+" or "=" = cursor and pixel information'
      print *,
     . '   "*" or "8" = centroid, then give cursor, pixel information'
      print *,
     . '   "@" or "2" = find pixel position from coordinates (DSS only)'
      print *,'   "a" = Add comments to title on plot.'
      print *,'   "?" or "/" or "h" = help menu'
      print *,'   "ESC" or "<sp>" = quit'
      if (dss) then
        print *,
     . 'For Digital Sky Survey images, "+" and "*" yield coordinates.'
      endif
      print *,' '
      return
      end


C-----------------------------------------------------------------------------
C Given the RA (decimal hours) and DEC (decimal degrees) in J2000 coordinates,
C find the XPIX, YPIX position.  User supplies header, coordinates, and an
C initial guess at the position (center of the image is ok).
C The X,Y position is defined such that CNPIX1,CNPIX2 are the coordinates for
C the lower-left corner of the lower-left pixel, where the first pixel is
C displayed in the lower-left (as in IRAF). Note that Vista displays this
C pixel in the upper-left.
C
      subroutine dss_radec_to_pix(header,raJ2000,decJ2000,XPIX,YPIX)

      implicit none
      character*(*) header            ! FITS Header (input)
      real*8 raJ2000,decJ2000         ! Decimal J2000 RA,DEC (input)
      real*8 XPIX,YPIX                ! X,Y pixel position (input/output)
      real*8 aspp,xx,yy,cdc,angsep,ra,dec
      integer*4 iter
      real*8 dd,DEGTORAD

      dd  = DEGTORAD( decJ2000 )
      cdc = 15.d0 * dcos( dd )
      call find_dss_aspp(header,aspp)
      xx=XPIX
      yy=YPIX
      iter = 0
      angsep = 9.d0
      do while((angsep.gt.0.001).and.(iter.lt.10))
        call dss_pix_to_radec(header,xx,yy,ra,dec)
        xx = xx - ( (raJ2000-ra)*cdc*3600.d0/aspp )
        yy = yy + ( (decJ2000-dec)*3600.d0/aspp )
        call as_ra_dec(ra,dec,raJ2000,decJ2000,angsep)
        angsep = angsep * 60.d0
        iter = iter + 1
      enddo
      if (iter.gt.10) print *,'WARNING: Exceeded iteration limit.'
      XPIX = xx
      YPIX = yy
      return
      end



C-----------------------------------------------------------------------------
C Given the X,Y position, generate the J2000 coordinates in decimal (RA) hours
C decimal (DEC) degrees.
C The X,Y position is defined such that CNPIX1,CNPIX2 are the coordinates for
C the lower-left corner of the lower-left pixel, where the first pixel is
C displayed in the lower-left (as in IRAF). Note that Vista displays this
C pixel in the upper-left.
C
      subroutine dss_pix_to_radec(header,XPIX,YPIX,raJ2000,decJ2000)

      implicit none
      character*(*) header            ! FITS Header (input)
      real*8 XPIX,YPIX                ! X,Y pixel position (input)
      real*8 raJ2000,decJ2000         ! Decimal J2000 RA,DEC (output)

      character chead*80,c80*80
      real*8 x,y,fhead,xc,yc,px,py
      real*8 a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13
      real*8 b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13
      real*8 hour,deg,min,sec,x2y2
      real*8 alpha,delta,xi,eta,deltaC,alphaC,pi,twopi
      parameter(   pi=3.141592654d0)
      parameter(twopi=6.283185308d0)

C Get parameters from header.
      xc = fhead('PPO3',header)
      yc = fhead('PPO6',header)
      px = fhead('XPIXELSZ',header)
      py = fhead('YPIXELSZ',header)

      a1 = fhead('AMDX1',header)
      a2 = fhead('AMDX2',header)
      a3 = fhead('AMDX3',header)
      a4 = fhead('AMDX4',header)
      a5 = fhead('AMDX5',header)
      a6 = fhead('AMDX6',header)
      a7 = fhead('AMDX7',header)
      a8 = fhead('AMDX8',header)
      a9 = fhead('AMDX9',header)
      a10= fhead('AMDX10',header)
      a11= fhead('AMDX11',header)
      a12= fhead('AMDX12',header)
      a13= fhead('AMDX13',header)

      b1 = fhead('AMDY1',header)
      b2 = fhead('AMDY2',header)
      b3 = fhead('AMDY3',header)
      b4 = fhead('AMDY4',header)
      b5 = fhead('AMDY5',header)
      b6 = fhead('AMDY6',header)
      b7 = fhead('AMDY7',header)
      b8 = fhead('AMDY8',header)
      b9 = fhead('AMDY9',header)
      b10= fhead('AMDY10',header)
      b11= fhead('AMDY11',header)
      b12= fhead('AMDY12',header)
      b13= fhead('AMDY13',header)

      hour= fhead('PLTRAH',header)
      min = fhead('PLTRAM',header)
      sec = fhead('PLTRAS',header)
      alphaC = pi * (hour+((min+(sec/60.d0))/60.d0)) / 12.d0
      if (alphaC.lt.0.0d0) alphaC = alphaC + twopi
      if (alphaC.gt.twopi) alphaC = alphaC - twopi

      deg = abs(fhead('PLTDECD',header))
      min = abs(fhead('PLTDECM',header))
      sec = abs(fhead('PLTDECS',header))
      deltaC = pi * (deg+((min+(sec/60.d0))/60.d0)) / 180.d0
      c80 = chead('PLTDECSN',header)
      if (index(c80,'-').gt.0) deltaC = -1.d0 * deltaC

C Compute values.
      x = (xc - px*XPIX) / 1000.d0
      y = (py*YPIX - yc) / 1000.d0

      x2y2 = x*x + y*y

      xi = a1*x + a2*y + a3 + a4*x*x + a5*x*y + a6*y*y + a7*x2y2
     .   + a8*x*x*x + a9*x*x*y + a10*x*y*y + a11*y*y*y + a12*x*x2y2
     .   + a13*x*x2y2*x2y2

      eta= b1*y + b2*x + b3 + b4*y*y + b5*x*y + b6*x*x + b7*x2y2
     .   + b8*y*y*y + b9*x*y*y + b10*x*x*y + b11*x*x*x + b12*y*x2y2
     .   + b13*y*x2y2*x2y2

      xi = pi * ( xi / (3600.d0 * 180.d0) )

      eta= pi * ( eta/ (3600.d0 * 180.d0) )

      alpha= atan( (xi/cos(deltaC)) / (1.d0-eta*tan(deltaC)) ) + alphaC

      delta= atan( ((eta+tan(deltaC))*cos(alpha-alphaC))
     .             / (1.d0-eta*tan(deltaC)) )

      if (alpha.lt.0.0d0) alpha = alphaC + twopi
      if (alpha.gt.twopi) alpha = alphaC - twopi

      raJ2000 = 12.d0 * alpha / pi

      decJ2000= 180.d0 * delta / pi

      return
      end


C------------------------------------------------------------------------
      subroutine dss_rowflip(a,ncol,nrow)
      implicit none
      integer*4 ncol,nrow,i,j,jj,n
      real*4 a(ncol,nrow),r
      n = nrow/2
      do j=1,n
        jj = (nrow-j)+1            !   ( erow - j ) + srow
        do i=1,ncol
          r       = a(i,j)
          a(i,j)  = a(i,jj)
          a(i,jj) = r
        enddo
      enddo
      return
      end


C----------------------------------------------------------------------
      subroutine find_median_rms(a,nc,nr,b,mp,median,rms,minval,maxval)
      implicit none
      integer*4 nc,nr,n,k,mp,i
      real*4 a(*)
      real*4 median,rms,b(mp),minval,maxval
      n=0
      do i=1,(nc*nr),4
        n   = n + 1
        b(n)= a(i)
      enddo
      call find_median(b,n,median)
      minval = +1.e+10
      maxval = -1.e+10
      rms= 0.
      k  = 0
      do i=1,(nc*nr)
        if (a(i).lt.minval) minval=a(i)
        if (a(i).gt.maxval) maxval=a(i)
        if (a(i).lt.median) then
          rms= rms + (median-a(i))*(median-a(i))
          k  = k + 1
        endif
      enddo
      if (k.lt.1) then
        rms = 1.
      else
        rms = sqrt( rms/float(k) )
      endif
      return
      end
