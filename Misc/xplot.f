C xplot                         tab   Jul92 / May95 / Oct95 / Apr97
C
C Interactive plotting of a FITS spectrum using PGPLOT utilities.
C
      implicit none
C
      include 'xplot.inc'
C
      integer*4 i1,i2,i3,i4,i5,i6,k,ii,pglw,j
      integer*4 narg,sc,ec,sr,er,nc,nr,i,lc,k1,k2,inhead
      character*80 arg(99),file,wrd,wrd2
      real*8 getx,fhead,r8
      real*4 r4,valread
      logical ok,findarg,portrait,colorps
      real*8 r81,r82,r83,r84,r85

      character*20 pgcolors(20)
      data pgcolors /
     . 'white', 'red', 'green', 'blue', 'turquoise', 'purple',
     . 'yellow', 'orange', 'lime green', 'light green', 'light blue',
     . 'dark purple', 'light red', 'dark gray', 'light gray', 'white',
     . 'white', 'white', 'white', 'white' /

C System specific files.
      call GetMakeeHome(wrd)
      i=lc(wrd)
      qafile = wrd(1:i)//'/qso_absorption'
      qefile = wrd(1:i)//'/qso_emission'
      qgfile = wrd(1:i)//'/qso_gammas'
      aafile = wrd(1:i)//'/atmabs.dat'

C Initialize parameters and look for keywords
      tabgscale = 1.0
      quit    = .false.
      hist    = .false.
      dots    = .false.
      dash    = .false.
      PS_Mode = '/PS'
      pglw    = 1
      color   = 1
      bufnum  = 0
      errbuf  = 0
      contbuf = 0
      mark    = -2.e+20
      xctrd   = -2.e+20
      ze      = 0.
      za      = 0.
      crad    = 3
      num_also= 0
      norm    = .false.
      ll_file = ' '
      sl_file = ' '
      datafile= ' '
      errfile = ' '
      contfile= ' '
      userfile= ' '
      userfile0= ' '
      ewfile   = ' '
      abs_priority = 0
      xs      = -1.e+31
      xe      = -1.e+31
      ys      = -1.e+31
      ye      = -1.e+31
      mfm     = -1.e+31
      psfile  = ' '
      device  = ' '
      nh      = 1
      nv      = 1
      npg     = 1
      bin     = 1
      row2d   = 0
      naal    = 0
      ll_n    = 0
      ll_z_n  = 0
      ll_showZ  = .true.
      ll_showQ  = .true.
      sl_numsys = 0
      za_also_n = 0
      ZeroPoint = 0.
      Aux_Mode  = 'qal'
      show_velocity = .false.
      show_coeff    = .false.
      XgapFraction  = 0.02
      lambda_zero_velocity = 0.d0
      r4 = 0.

C Get arguments.
      call arguments(arg,narg,99)

C Look for multiple y dimension commands.
      do i=1,99
        ysa(i) = -1.e+31
        yea(i) = -1.e+31
      enddo
      k=0
      do i=1,narg
        if (arg(i)(1:2).eq.'y=') then
          k1=index(arg(i),',')
          arg(i)(k1:k1)=' '
          k2=index(arg(i),',')
          arg(i)(k2:k2)=' '
          k = max(1,min(99,nint(valread(arg(i)(3:k1-1)))))
          ysa(k) = valread(arg(i)(k1+1:k2-1))
          yea(k) = valread(arg(i)(k2+1:lc(arg(i))))
        endif
      enddo
C Delete all "y=" which may be found by "findarg()".
      do while(findarg(arg,narg,'y=',':',wrd,i))
      enddo

C Other arguments.
      if (findarg(arg,narg,'xgf=' ,':',wrd,i))
     .   XgapFraction=valread(wrd)
      if (findarg(arg,narg,'aux=' ,':',wrd,i)) Aux_Mode=wrd(1:8)
      call lower_case(Aux_Mode)
      if (findarg(arg,narg,'qaf=' ,':',wrd,i)) qafile=wrd
      if (findarg(arg,narg,'ll=' ,':',wrd,i))  ll_file=wrd
      if (findarg(arg,narg,'sl=' ,':',wrd,i))  sl_file=wrd
      if (findarg(arg,narg,'uf=' ,':',wrd,i))  userfile=wrd
      if (findarg(arg,narg,'uf0=' ,':',wrd,i)) userfile0=wrd
      if (findarg(arg,narg,'ewf=' ,':',wrd,i)) ewfile=wrd
      if (findarg(arg,narg,'ef=' ,':',wrd,i))  errfile=wrd
      if (findarg(arg,narg,'cf=' ,':',wrd,i))  contfile=wrd
      if (findarg(arg,narg,'-ps' ,':',wrd,i))  psfile='junk.ps'
      if (findarg(arg,narg,'ps=' ,':',wrd,i))  psfile=wrd
      if (findarg(arg,narg,'device=' ,':',wrd,i)) device=wrd
      if (findarg(arg,narg,'xs=' ,':',wrd,i))  xs = valread(wrd)
      if (findarg(arg,narg,'xe=' ,':',wrd,i))  xe = valread(wrd)
      if (findarg(arg,narg,'min=',':',wrd,i))  ys = valread(wrd)
      if (findarg(arg,narg,'max=',':',wrd,i))  ye = valread(wrd)
      if (findarg(arg,narg,'mfm=',':',wrd,i))  mfm= valread(wrd)
      if (findarg(arg,narg,'nh=' ,':',wrd,i))  nh = nint(valread(wrd))
      if (findarg(arg,narg,'nv=' ,':',wrd,i))  nv = nint(valread(wrd))
      if (findarg(arg,narg,'npg=',':',wrd,i))  npg= nint(valread(wrd))
      if (findarg(arg,narg,'bin=',':',wrd,i))  bin= nint(valread(wrd))
      if (findarg(arg,narg,'row=',':',wrd,i))
     .    row2d = nint(valread(wrd))
      if (findarg(arg,narg,'color=',':',wrd,i))
     .   color= nint(valread(wrd))
      if (findarg(arg,narg,'pglw=',':',wrd,i))
     .    pglw= nint(valread(wrd))
      pglw=min(201,max(1,pglw))
      norm= findarg(arg,narg,'-norm',':',wrd,i)
      quit= findarg(arg,narg,'-quit',':',wrd,i)
      hist= findarg(arg,narg,'-hist',':',wrd,i)
      dash= findarg(arg,narg,'-dash',':',wrd,i)
      dots= findarg(arg,narg,'-dots',':',wrd,i)
      oo  = findarg(arg,narg,'-oo',':',wrd,i)
      portrait = findarg(arg,narg,'-portrait',':',wrd,i)
      colorps  = findarg(arg,narg,'-colorps',':',wrd,i)
      if ((.not.portrait).and.(.not.colorps)) PS_Mode='/PS'
      if ((     portrait).and.(.not.colorps)) PS_Mode='/VPS'
      if ((.not.portrait).and.(     colorps)) PS_Mode='/CPS'
      if ((     portrait).and.(     colorps)) PS_Mode='/VCPS'

C Echo syntax.
      if (narg.lt.1) then
        print *,
     .  'Syntax: xplot (FITS file)  [row= (specify for 2-D spectra)]'
        print *,
     .  '          [ef=(error FITS file)]  [cf=(continuum FITS file)]'
        print *,
     .  '          [ (2nd FITS file)  [ (3rd file) ] ... up to 4 ]'
        print *,
     .  '          [ xs= ] [ xe= ] [ min= ] [ max= ]  [ mfm= ]'
        print *,
     .  '          [ nh= ] [ nv= ] [ npg= ] [ bin= ] [ -norm ]'
        print *,
     .  '          [-quit] [-oo] [-hist] [-dots] [-dash] [ color= ]'
        print *,
     .  '          [ y=plot#,ymin,ymax [ y= ... ] ]  [pglw=]'
        print *,
     .  '          [ -ps | ps=file | device= ] [-portrait] [-colorps]'
        print *,
     .  '          [ uf=(user command file) ]'
        print *,
     .  '          [ uf0=(user command file for initial commands) ]'
        print *,
     .  '          [ ewf=(output filename for equivalent width data)]'
        print *,
     . '          [ ll=(line list file)]  [ qaf=(qso abs. line file) ]'
        print *,
     .  '          [ aux=(qal,hlm,...) {Auxilary mode key commands} ]'
        print *,'          [ xgf= ]  [sl=(systems list file)]'  
        print *,' Options:'
        print *,
     .  '   nh,nv,npg : # horizontal plots, # vertical, # of pages.'
        print *,
     .  '   mfm=      : set minimum as a fraction of maximum value.'
        print *,
     .  '   -oo       : show object only on title line.'
        print *,
     .  '   pglw=     : Set PGPLOT line width (1 to 201).'
        print *,
     .  '   -portrait : Use protrait (not landscape) for PostScript.'
        print *,
     .  '   -colorps  : Make color PostScript file (note that you'
        print *,
     .  '               will always get color PostScript if the'
        print *,
     .  '               environment variable PGPLOT_PS_COLOR is set.)'
        print *,
     .  '   xgf=      : XgapFraction (default=0.02)'
        print *,' '
        call exit(0)
      endif

C Load QAL database.
      call xp_load_qafile()

C Load line list file.
      if (ll_file.ne.' ') then
C Get the version of the line list file.
        open(35,file=ll_file,status='old')
        read(35,'(a)',err=901) wrd
        close(35)
        ll_n=0
        if (index(wrd,'Version').eq.0) then
          open(35,file=ll_file,status='old')
135       read(35,*,end=535,err=901) i1,r81,r82,r83,r84,r85
          ll_n = ll_n + 1
          ll_cent(ll_n) = r81
          ll_ri(ll_n)   = r82
          ll_ew(ll_n)   = r83
          ll_ewerr(ll_n)= r84
          ll_rw(ll_n)   = r85
          ll_crty(ll_n) = 1
          goto 135
535       close(35)
        elseif (index(wrd,'Version 2').gt.0) then
          open(35,file=ll_file,status='old')
          read(35,'(a)',err=901) wrd
          print '(2a)',' Line list file ',wrd(1:lc(wrd))
136       read(35,*,end=536,err=901) i1,r81,r82,r83,r84,r85,i2
          ll_n = ll_n + 1
          ll_cent(ll_n) = r81
          ll_ri(ll_n)   = r82
          ll_ew(ll_n)   = r83
          ll_ewerr(ll_n)= r84
          ll_rw(ll_n)   = r85
          ll_crty(ll_n) = i2
          goto 136
536       close(35)
        else
          print '(2a)',
     .  ' Error-- Unknown line list version: ',wrd(1:lc(wrd))
          call exit(1)
        endif
        call xp_CleanLineList(0)
      endif

C Load absorptions systems list file.
      if (sl_file.ne.' ') then
        open(37,file=sl_file,status='old')
        read(37,'(a)',err=911) wrd
        if (lc(wrd).gt.2) goto 911
137     continue
        sl_numsys = sl_numsys + 1  
        if (sl_numsys.gt.maxsys) then
          print *,'ERROR: xplot: too many absorption line systems.'
          print *,'ERROR: xplot: increase "maxsys" in "xplot.inc".'
          call exit(0)
        endif
        sl_color(sl_numsys) = nint(valread(wrd))
        read(37,*,err=911) sl_za(sl_numsys)
        sl_numline(sl_numsys) = 0
237     continue
        read(37,'(a)',end=537,err=911) wrd
        if (lc(wrd).lt.3) goto 137
        sl_numline(sl_numsys) = sl_numline(sl_numsys) + 1
        k = sl_numline(sl_numsys)
        if (k.gt.maxsysline) then
         print *,'ERROR: xplot: too many absorption lines.'
         print *,'ERROR: xplot: increase "maxsysline" in "xplot.inc".'
         call exit(0)
        endif
        read(wrd,'(f9.3,1x,a)',err=911)
     .        sl_rw(sl_numsys,k),sl_ion(sl_numsys,k)
        goto 237
537     close(37)
      endif

C Open equivalent width data file.
      if (ewfile.ne.' ') then
        open(34,file=ewfile,status='unknown')
111     read(34,'(a)',end=555) wrd
        goto 111
555     continue
      endif

C Clear 2D arrays.
      do i=1,mpt2d
        do ii=1,mrw2d
          xo2d(i,ii)=0.d0
          y2d(i,ii) =0.
          yo2d(i,ii)=0.
          e2d(i,ii) =0.
          c2d(i,ii) =0.
        enddo
      enddo
C Check.
      if ((nh.ne.1).or.(nv.ne.1).or.(npg.ne.1)) then
        if ((.not.quit).and.(psfile.eq.' ')) then
          print *,
     .  'Error: nh= and nv= must be 1 unless -quit,-ps/ps= is used.'
          call exit(1)
        endif
      endif
C Construct spectrum FITS filename.
      datafile=arg(1)
      call AddFitsExt(datafile)
C If more than one row, assume a 2-D spectrum file.
      if (row2d.eq.0) then
        call readfitsheader(datafile,header,ok)
        if (inhead('NAXIS2',header).gt.1) row2d=1
      endif
C Read in FITS file.
C ....... 2D ......
      if (row2d.gt.0) then
        print '(2a)',
     .  ' Reading 2-D spectrum: ',datafile(1:lc(datafile))
        file=datafile
        call xp_readfits2D(file,ok,0)
        header=temphead
        if (inhead('NAXIS1',header).gt.mpt2d) then
          print *,
     .  'Error two many columns in 2d spectrum. mpt2d=',mpt2d
          call exit(1)
        endif
        if (inhead('NAXIS2',header).gt.mrw2d) then
          print *,'Error two many rows in 2d spectrum. mrw2d=',mrw2d
          call exit(1)
        endif
C ....... 1D ......
      else
        print '(2a)',' Reading: ',datafile(1:lc(datafile))
        call readfits(datafile,y,maxpt,header,ok)
        if (.not.ok) then
          print '(2a)',' Error reading file: ',datafile(1:lc(datafile))
          call exit(1)
        endif
        if (inhead('NAXIS1',header).gt.maxpt) then
          print *,'Error number of points exceeds maxpt=',maxpt
          call exit(1)
        endif
      endif
      if (.not.ok) then
        print *,'Error reading data FITS file.'
        call exit(1)
      endif
      call GetDimensions(header,sc,ec,sr,er,nc,nr)
C Defaults for continuum and error header.
      eheader = header
      cheader = header
C Spectrum.
      if ((nr.ne.1).and.(row2d.eq.0)) then
        print *,'Error-- image must be spectrum.'
        call exit(1)
      endif
C Data image exists.
      bufnum = 1
C
C ...Load X and Y arrays...
C ........ 2D .........
      if (row2d.gt.0) then
C Swap in appropriate arrays.
        do i=1,nc
          xo(i)= xo2d(i,row2d)
          x(i) = sngl(xo(i))
          y(i) = y2d(i,row2d)
          yo(i)= yo2d(i,row2d)
        enddo
C ........ 1D .........
      else
C Initialize x value function.
        r8 = getx(-1.d0,header)
        if (r8.lt.-1.e+29) then
          print *,'Error getting wavelength scale.'
          call exit(1)
        endif
        do i=1,nc
          xo(i)= getx(dfloat(i),header)
          if (xo(i).lt.-1.e+7) then
            print *,'X value too negative ( < -1.e+7).'
            call exit(1)
          endif
C Single precision x value.
          x(i) = sngl(xo(i)) 
C Save original data.
          yo(i)= y(i)
        enddo
      endif
C Number of points equals number of columns.
      np = nc

C Get redshift for QSO if available.
      r8 = fhead('REDSHIFT',header)
      if (r8.gt.0.) ze = max(-0.5,min(9.9999,sngl(r8)))
C Get absorption redshift for QSO if available.
      r8 = fhead('ZABS',header)
      if (r8.gt.0.) za = max(-0.5,min(9.9999,sngl(r8)))

C Read other (also) FITS files.
      num_also=0
      do i=2,min(narg,max_also+1)
        file=arg(i)
        call AddFitsExt(file)
        num_also=num_also+1
C ....... 2D ......
        if (row2d.gt.0) then
          print '(2a)',' Also reading 2-D spec: ',file(1:lc(file))
          call xp_readfits2D_also(file,ok)
          if (inhead('NAXIS1',temphead).gt.mpt2d) then
            print *,'Error two many columns. mpt2d=',mpt2d
            call exit(1)
          endif
          if (inhead('NAXIS2',header).gt.mrw2d) then
            print *,'Error two many rows. mrw2d=',mrw2d
            call exit(1)
          endif

C ....... 1D ......
        else
          call readfits(file,y_also(1,num_also),maxpt,temphead,ok)
          print '(2a)',' Also reading: ',file(1:lc(file))
          if (.not.ok) then
            print '(2a)',' Error reading : ',file(1:lc(file))
            call exit(1)
          endif
          if (inhead('NAXIS1',temphead).gt.maxpt) then
            print '(2a)',' Error reading : ',file(1:lc(file))
            print *,'Error number of points exceeds maxpt=',maxpt
            call exit(1)
          endif
          print '(i3,5a)',num_also,' = ',pgcolors(min(20,1+num_also)),
     .        '  ( ',file(1:lc(file)),' )'
          r8=getx(-1.d0,temphead)
          if (r8.lt.-1.e+29) then
            print *,
     .  'Error getting wavelength scale for "also" file: ',r8
            call exit(1)
          endif
          call GetDimensions(temphead,i1,i2,i3,i4,i5,i6)
          k=0
          do ii=i1,i2
            k=k+1
            x_also(k,num_also)=sngl(getx(dfloat(k),temphead))+r4
          enddo
          n_also(num_also) = k
        endif
      enddo

C Load error buffer in common block array e(..).
      if (errfile.ne.' ') then
        call AddFitsExt(errfile)
        if (row2d.gt.0) then
          print '(2a)',' Reading 2-D HIRES error spectrum: ',
     .                 errfile(1:lc(errfile))
          file=errfile
          call xp_readfits2D(file,ok,1)
          eheader=temphead
          do i=1,np
            e(i) = e2d(i,row2d)
          enddo
        else
          print '(2a)',' Reading error file: ',errfile(1:lc(errfile))
          call readfits(errfile,e,maxpt,eheader,ok)
        endif
        if (.not.ok) then
          print *,'Error reading error FITS file.'
          call exit(1)
        endif
        call GetDimensions(eheader,i1,i2,i3,i4,i5,i6)
        if (i5.ne.nc) then
          print *,
     .  'ERROR- data and error spectra have different dimensions.'
          call exit(1)
        endif
        errbuf=1
      else
        do i=1,np
          e(i) = 0.
        enddo
      endif

C Load continuum buffer in common block array c(..).
      if (contfile.ne.' ') then
        call AddFitsExt(contfile)
        if (row2d.gt.0) then
          print '(2a)',' Reading 2-D HIRES continuum spectrum: ',
     .                 contfile(1:lc(contfile))
          file=contfile
          call xp_readfits2D(file,ok,2)
          cheader=temphead
          do i=1,np
            c(i) = c2d(i,row2d)
          enddo
        else
          print '(2a)',
     .  ' Reading continuum file: ',contfile(1:lc(contfile))
          call readfits(contfile,c,maxpt,cheader,ok)
        endif
        if (.not.ok) then
          print *,'Error reading error FITS file.'
          call exit(1)
        endif
        call GetDimensions(cheader,i1,i2,i3,i4,i5,i6)
        if (i5.ne.nc) then
          print *,
     .  'ERROR- data and continuum spectra have different dimensions.'
          call exit(1)
        endif
        contbuf=1
      else
C Set continuum to 1.0 if normalized spectrum or initialize to zero.
        if (norm) then
          r4 = 1.
          contbuf=1
        else
          r4 = 0.
        endif
        do i=1,np
          c(i) = r4
        enddo
      endif

C Initialize window.
      if (psfile.ne.' ') then
        wrd = psfile(1:lc(psfile))//PS_Mode(1:lc(PS_Mode))
        call PGBEGIN(0,wrd,-1*nh,nv)
        print '(2a)',
     .  ' PostScript plot in file : ',psfile(1:lc(psfile))
      elseif (device.ne.' ') then
        call PGBEGIN(0,device,-1*nh,nv)
        print '(2a)',' Output device : ',device(1:lc(device))
      else
        call GetMakeeHome(wrd)
        wrd2 = wrd(1:lc(wrd))//'/00.pgplot_device'
        if (access(wrd2,'r').eq.0) then
          open(1,file=wrd2,status='old')
          read(1,'(a)') device
          close(1)
        else
          device = '/XDISP'
        endif
        print '(2a)','Device=',device(1:lc(device))
        call PGBEGIN(0,device,-1*nh,nv)
      endif
      pg_ask = ((nh.gt.1).or.(nv.gt.1).or.(npg.gt.1))
      call PGASK(pg_ask)
      call PGSLS(1)
      call PGSCI(1)
      call PGSLW(pglw)
      call PGSCH(1.2)

C Set up and clear window.
      call PGVSTAND

C Do user specified commands prior to plotting.
      if (userfile0.ne.' ') call xp_user_commands(userfile0)

C Shall we quit after plotting?
      if (psfile.ne.' ') quit=.true.
      if ( (device.ne.' ').and.
     .     (device(1:6).ne.'/XDISP').and.
     .     (device(1:3).ne.'/XW').and.
     .     (device(1:6).ne.'/RETRO') ) quit=.true.

C Set row number in 2D array.
      j = row2d
      if (j.gt.0) call xp_SetNewRow(j)

C . . . . . . . . . . . . . . . . . . . .
C . . . . . . . . . . . . . . . . . . . .

C Go into interactive plotting mode.
      call xp_interact()

C Close PGDISP connection.
      call PGEND

C Close equivalent width data file.
      if (ewfile.ne.' ') close(34)

C If data has been changed write it out...
      if (modified) then
101     print '(a,$)',
     .  ' Data modified.  Do you want to overwrite? (1/0) : '
        read(5,*,err=101) i
        if ((i.ne.1).and.(i.ne.0)) goto 101
        if (i.eq.0) then
          print '(a,$)',
     .  ' Are you sure you do not want to save? (1/0) : '
          read(5,*,err=101) j
          if (j.ne.1) goto 101
        endif
        if (i.eq.1) then
          if (row2d.gt.0) then
            print '(2a)',' Writing : ',datafile(1:lc(datafile))
            temphead=header
            file=datafile
            call xp_writefits2D(file,ok,0)
            if (errbuf.gt.0) then
              print '(2a)',' Writing : ',errfile(1:lc(errfile))
              temphead=eheader
              file=errfile
              call xp_writefits2D(file,ok,1)
            endif
            if (contbuf.gt.0) then
              if (contfile.eq.' ') then
                contfile='con_'//datafile(1:lc(datafile))
              endif
              print '(3a,$)',
     .   ' Enter filename for continuum [ ',
     .   contfile(1:lc(contfile)),' ] : '
              read(5,'(a)') file
              if (file.eq.' ') file=contfile
              contfile=file
              call AddFitsExt(contfile)
              print '(2a)',' Writing : ',contfile(1:lc(contfile))
              temphead=cheader
              file=contfile
              call xp_writefits2D(file,ok,2)
            endif
          else
            print '(2a)',' Writing : ',datafile(1:lc(datafile))
            call writefits(datafile,y,header,ok)
            if (errbuf.gt.0) then
              print '(2a)',' Writing : ',errfile(1:lc(errfile))
              call writefits(errfile,e,eheader,ok)
            endif
            if (contbuf.gt.0) then
              if (contfile.eq.' ')
     .    contfile='con_'//datafile(1:lc(datafile))
              print '(3a,$)',
     .    ' Enter filename for continuum [ ',
     .    contfile(1:lc(contfile)),' ] : '
              read(5,'(a)') file
              if (file.eq.' ') file=contfile
              contfile=file
              call AddFitsExt(contfile)
              print '(2a)',' Writing : ',contfile(1:lc(contfile))
              call writefits(contfile,c,cheader,ok)
            endif
          endif
        endif
      endif

C Write out line list?
      if ((ll_n.gt.0).and.(psfile.eq.' ')) then
        if (ll_file.eq.' ') ll_file='junk.list'
        print '(3a,$)',
     .  ' Filename for QAL line list [ ',ll_file(1:lc(ll_file)),' ]: '
        read(5,'(a)') wrd
        if (wrd.ne.' ') ll_file = wrd
        print '(2a)',
     .  ' Writing line list file : ',ll_file(1:lc(ll_file))
        open(35,file=ll_file,status='unknown')
        write(35,'(a)') 'Version 2 (Sep96)'
        do i=1,ll_n
          write(35,'(i5,f12.4,f7.3,2f9.5,f10.3,i3)')
     .       i,ll_cent(i),ll_ri(i),
     .       ll_ew(i),ll_ewerr(i),ll_rw(i),ll_crty(i)
        enddo
        close(35)
      endif

      stop
901   print *,'Error opening line list file.'
      stop
911   print *,'Error reading systems list file.'
      stop
      end

C-----------------------------------------------------------------------------
C Main interactive routine.

      subroutine xp_interact()

      implicit none
      include 'xplot.inc'
      real*4 r,r1,r2,r3,r4,r5,xp_ymedian,cw,cp,cx,cy
      real*4 xminset(99),xmaxset(99)
      real*4 valread,x0,x1,x2
      real*8 r8,fhead,r81,r82
      integer*4 obsnum,integ,inhead,i,ii,j,nn,nearxi,lc,plot_count
      character*80 object,c80,chead,wrd,data_info
      character*7 rf7,c7a,c7b
      character*9 rf9,c9a,c9b
      character cs*200,key*1,c1*1,c2*2,c3*3,c6*6
      logical replot,yfix,yfixzero,ok,doing_PS,mulplot,xp_AtmAbsExist
      integer*4 i1,i2,kk,nearxbini

C Initial.
      mulplot = .false.
      data_info = ' '

C Short message for interactive users.
      if (.not.quit) print *,' Hit "?" or "/" for main menu...'

C Set Xgap space at either end in X-value units.
      Xgap = XgapFraction * abs(x(1)-x(np))

C Fix the y axis limits?
      yfix = .false.
      if (abs(ymin).lt.1.e-30) then
        yfixzero = .true.
      else
        yfixzero = .false.
      endif

C Has the spectrum been modified- i.e. do we need to reload it.?
      modified = .false.

C Not doing a PS plot.
      doing_PS = .false.

C Get header cards and form plot title.
      obsnum = max(0,inhead('OBSNUM',header))
      r8 = fhead('EXPOSURE',header)
      integ  = nint(max(-1.d0,min(99999.d0,r8)))
      object = chead('OBJECT',header)
      cs = ' '
      if (ze.eq.0.) then
        write(cs,'(2a,i4,i6,a)',err=9)
     .    object(1:lc(object(1:50))),
     .    '  ( Obs#',obsnum,integ,'s )'
      else
        write(cs,'(2a,i4,i6,a,f7.4,a)',err=9)
     .    object(1:lc(object(1:50))),
     .    '  ( Obs#',obsnum,integ,'s  z\\de\\u=',ze,' )'
      endif
      if (oo) cs=object

C Initial x limits.
      xmin = x(1) - Xgap
      xmax = x(np)+ Xgap
      if (xs.gt.-1.e+30) xmin=xs
      if (xe.gt.-1.e+30) xmax=xe
      mulplot = ((nh.gt.1).or.(nv.gt.1).or.(npg.gt.1))

C For multiple plots.
      if (mulplot) then
        plot_count=0
        do i=1,nh*nv*npg
          r1 = xmin + float(i-1)*(xmax-xmin)/float(nh*nv*npg)
          r2 = xmin + float(i)  *(xmax-xmin)/float(nh*nv*npg)
          xminset(i) = r1
          xmaxset(i) = r2
          if (i.ne.1)         xminset(i) = r1 - (r2-r1)*0.04
          if (i.ne.nh*nv*npg) xmaxset(i) = r2 + (r2-r1)*0.04
        enddo
      else
        plot_count=1
      endif

C . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
C . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 

C Update binned arrays and calculate new y limits.
1     continue
      call ReBin()
      if (mulplot) then
        plot_count = plot_count + 1
        xmin = xminset(plot_count)
        xmax = xmaxset(plot_count)
      endif
C Check x range.  Set minimum range to incompass a few pixels.
      if (xmin.gt.x(np)) xmin=x(max(1,np-4))
      if (xmax.lt.x(1))  xmax=x(min(4,np  ))
      i1 = nearxi(xmin)
      i2 = nearxi(xmax)
      if (abs(i1-i2).lt.4) then
        i  = (i1+i2)/2
        r1 = abs( x(min(np,max(1,i-2))) - x(min(np,max(1,i+2))) )
        r2 = (xmin+xmax)/2.
        xmin = r2 - r1
        xmax = r2 + r1
      endif
C Determine best y range.
      call autoy(yfix,yfixzero)
      if (mfm.gt.-1.e+30) ymin = ymax*mfm
      if (ys .gt.-1.e+30) ymin = ys
      if (ye .gt.-1.e+30) ymax = ye
      if (mulplot) then
        if (ysa(plot_count).gt.-1.e+30) ymin = ysa(plot_count)
        if (yea(plot_count).gt.-1.e+30) ymax = yea(plot_count)
      endif

C Clear screen (refresh).
2     continue
      call PGPAGE

C Plot the spectrum. 
3     continue
      call PGBBUF

C Avoid overflows.
      r1 = abs(ymax-ymin) 
      r2 = (ymin+ymax)/2.
      if (r2.lt.1.e-20) then
        ymin = ymin - 1.e-20
        ymax = ymax + 1.e-20
      else
        if (r1/r2.lt.1.e-5) then
          ymin = ymin - (r2)*1.e-5
          ymax = ymax + (r2)*1.e-5
        endif
      endif
C Set window size.
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call PGSCI(1)                         ! Write labels in white.
      call xp_BetterBox                     ! Does a PGBOX
      call PGEBUF
      call PGSCI(color)
      i = nearxbini(xmin)
      nn= nearxbini(xmax) - i
      if (dots) then
        call PG_DotBuf(nn,x_bin(i),y_bin(i),-1)
      elseif (hist) then
        call PG_BinBuf(nn,x_bin(i),y_bin(i),.true.)
      else
        call PG_LineBuf(nn,x_bin(i),y_bin(i))
      endif
      call PGQLS(kk)
      call PGSLS(4)
      call PG_HLine(xmin,xmax,0.)
      call PGSLS(kk)
C Over-plot continuum spectrum.
      if (contbuf.gt.0) then
        call PGSCI(4)                               ! blue
        if (norm) then
          call PG_HLine(xmin,xmax,1.)
        else
          call PG_LineBuf(nn,x_bin(i),c_bin(i))       ! over-plot continuum
        endif
      endif
C Over-plot zero-point.
      if (abs(ZeroPoint).gt.1.e-30) then
        call PGSCI(5)
        call PG_HLine(xmin,xmax,ZeroPoint)
      endif
C Over-plot error spectrum.
      if (errbuf.gt.0) then
        call PGSCI(8)                       ! brown
        if (hist) then
          call PG_BinBuf(nn,x_bin(i),e_bin(i),.true.)
        else
          call PG_LineBuf(nn,x_bin(i),e_bin(i))
        endif
      endif
C Over-plot other spectra.
      if (num_also.gt.0) then
        j = color
        do ii=1,num_also
          j=j+1
          if (dash) then
            call PGSLS(ii+1)
          else
            call PGSCI(j)
          endif
          if (dots) then
             call PG_DotBuf(n_also_bin(ii),
     .                      x_also_bin(1,ii),y_also_bin(1,ii),-1)
          elseif (hist) then
            call PG_BinBuf(n_also_bin(ii),
     .                     x_also_bin(1,ii),y_also_bin(1,ii),.true.) 
          else
            call PG_LineBuf(n_also_bin(ii),
     .                      x_also_bin(1,ii),y_also_bin(1,ii)) 
          endif
        enddo
      endif
      call PGSLS(1)        ! Solid lines.
      call PGSCI(1)        ! Back to white.
      call PGMTEXT('B',3.,0.5,0.5,cs)
      if (mulplot) then
        write(c3,'(i3)') plot_count
        call PGMTEXT('T',0.8,0.05,0.,c3)
      endif

C Mark QALs.
      if ((ll_n.gt.0).and.(ll_showQ)) then
        call PGQCH(r)
        if (psfile.eq.' ') then
          call PGSCH(0.75)
        else
          call PGSCH(0.95)
        endif
        call PGBBUF
        do i=1,ll_n
          if (mod(i,40).eq.0) then
            call PGEBUF
            call PGBBUF
          endif
          r1= sngl(ll_cent(i))
          if ((r1.gt.xmin).and.(r1.lt.xmax)) then
            if (psfile.eq.' ') then
              call xp_single_mark(r1,3)
            else
              call xp_single_mark(r1,1)
            endif
            j = nearxi(r1)
            if (psfile.eq.' ') call PGSCI(3)
            r2= y(j) - ((ymax-ymin)*0.06)
            if (i.lt.100) then
              write(c3,'(i2.2,1x)') i
            else
              write(c3,'(i3.3)') i
            endif
            call PGTEXT(r1,r2,c3)
            if ((ll_rw(i).gt.0.).or.(ll_crty(i).lt.0)) then
              r2= y(j) - ((ymax-ymin)*0.10)
              if (ll_crty(i).eq.1) c2='  '
              if (ll_crty(i).eq.2) c2='? '
              if (ll_crty(i).eq.3) c2='??'
              if (ll_crty(i).gt.0) then
                write(c6,'(i4.4,a)') int(ll_rw(i)),c2
              elseif (ll_crty(i).eq.-2) then
                c6='Atm   '
              else
                c6='???   '
              endif
              if (psfile.eq.' ') call PGSCI(2)
              call PGTEXT(r1,r2,c6)
            endif
            if ((ll_rw(i).gt.0.).and.(ll_showZ)) then
              r3 = sngl(ll_cent(i)/ll_rw(i)) - 1.
              write(c6,'(f6.4)') r3
              r2 = y(j) - ((ymax-ymin)*0.14)
              if (psfile.eq.' ') call PGSCI(5)
              call PGTEXT(r1,r2,c6)
            endif
          endif
        enddo
        call PGEBUF
        call PGSCH(r)
        call PGSCI(color)
      endif

C Mark systems list of qso absorption lines (with various colors).
      if (sl_numsys.gt.0) then
        call PGQCH(r)
        call PGSCH(0.85)
        do i=1,sl_numsys
          do j=1,sl_numline(i)
            r1 = sngl( ( 1.d0 + sl_za(i) ) * sl_rw(i,j) )
            write(c80,'(a,f8.2)')
     .   sl_ion(i,j)(1:lc(sl_ion(i,j))),sl_rw(i,j)
            call PGSCI(sl_color(i))
            call PG_VLine(ymax,(ymax-(ymax-ymin)/2.),r1)
            call PGPTXT(r1,(ymin+0.10*(ymax-ymin)),90.,0.,c80)
          enddo
        enddo
        call PGSCH(r)
        call PGSCI(1)
      endif

C For MARK= keyword.
      if (mark.gt.-1.e+10) then   
        r = mark
        call xp_mark_position(r,2)
      endif

C For ARCCENT= keyword.
      if (xctrd.gt.-1.e+10) then
        call xp_arccent(xctrd,cw,cp)
        xctrd = cw
      endif

C Mark predicted positions of absorption lines.
      if (abs_priority.gt.0) then
        i = abs_priority
        call xp_absorption_lines(i)
        write(c80,'(a,f8.5,a,i2)',err=9)
     .   'z=',za,'   priority=',abs_priority
        call PGMTEXT('B',3.,0.,0.,c80)
      endif

C Show 2-D spectrum row number.
      if (row2d.gt.0) then
        r1=xmin 
        r2=xmax 
        if (xp_AtmAbsExist(r1,r2)) then
          write(c80,'(a,i3,a)',err=9) 'Row=',row2d,'  Atm.Abs.'
        else
          write(c80,'(a,i3)',err=9) 'Row=',row2d
        endif
        call PGMTEXT('T',0.2,1.0,1.0,c80)
      endif

C Show binning factor.
      if (bin.gt.1) then
        write(c80,'(a,i3)',err=9) 'Bin=',bin
        call PGMTEXT('B',3.,1.0,1.0,c80)
      endif

C Do next plot in a series of multiple plots.
      if (plot_count.lt.nh*nv*npg) goto 1

C Do user specified commands.
      if (userfile.ne.' ') call xp_user_commands(userfile)

C Non-interactive.
      if (quit) return

C Did we just do a PostScript Plot?  If so, re-initialize.
      if (doing_PS) then
        call PGEND
        call PGBEGIN(0,'/XDISP',-1*nh,nv)
        call PGASK(pg_ask)
        call PGVSTAND
        doing_PS = .false.
        goto 1
      endif

C Main interactive loop point.
5     continue

C Get key stroke.
      call PG_CURSOR(cx,cy,key)

      if ((key.eq.'?').or.(key.eq.'/')) then         ! help
        call xp_main_help

      elseif (key.eq.'b') then     ! New binning factor.
        print '(2a)','IN PLOT WINDOW: ',
     .               'Enter new binning value (1 to 128.)'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        i=nint(r8)
        bin=max(1,min(128,i))
        goto 1

      elseif ((key.eq.'-').or.(key.eq.'_')) then  ! decrease row for 2D data.
        j=row2d-1
        call xp_SetNewRow(j)
        goto 1

      elseif ((key.eq.'+').or.(key.eq.'=')) then  ! increase row for 2D data.
        j=row2d+1
        call xp_SetNewRow(j)
        goto 1

      elseif (key.eq.'5') then     ! Set new row for 2D data.
        print *,
     .  'Hit 2 digits in plot screen to select new row number...'
        call PG_CURSOR(cx,cy,key)
        i1=ichar(key)-48
        j=row2d
        if ((i1.ge.0).and.(i1.le.9)) then
          call PG_CURSOR(cx,cy,key)
          i2=ichar(key)-48
          if ((i2.ge.0).and.(i2.le.9)) j=(i1*10)+i2
        endif
        call xp_SetNewRow(j)
        goto 1

      elseif (key.eq.'J') then     ! PostScript Plot
        call PGEND
        wrd = 'junk.ps'//PS_Mode(1:lc(PS_Mode))
        call PGBEGIN(0,wrd,-1*nh,nv)
        print *,'PostScript plot in "junk.ps".'
        call PGASK(pg_ask)
        call PGVSTAND
        doing_PS = .true.
        goto 1

      elseif (key.eq.'u') then     ! update (recalculate y limits and replot)
        goto 1

      elseif (key.eq.char(13)) then     ! data information:  <cr>
        i = nearxi(cx)
C----+----1----+----2----+----3----+----4----+----5----+----6----+----7----+---
C x,y: 1234567, 123456789  cursor: 1234567, 123456789  array:12345  pixel:12345
C x,y: 3456.34, 40.238923  cursor: 3457.23, 59.123023  array:  534  pixel:  544
        c7a = rf7(x(i))
        c9a = rf9(y(i))
        c7b = rf7(cx)
        c9b = rf9(cy)
        call PGSCI(0)
        call PGMTEXT('T',0.5,0.5,0.5,data_info)
        call PGSCI(1)
        write(data_info,'(9(a),i5,a,i5)',err=9)
     .   ' x,y: ',c7a,', ',c9a,'  cursor: ',
     .    c7b,', ',c9b,'  array:',i,'  pixel:',i
        print *,data_info(1:78)
        call PGMTEXT('T',0.5,0.5,0.5,data_info)
        goto 5

      elseif (key.eq.' ') then     ! section marking
        call xp_sections(cx,replot)
        if (replot) goto 1
        goto 5

      elseif (key.eq.'k') then     ! kink fix by fit/replace
        i = nearxi(cx)
        call xp_kinkfit(i,replot)
        if (replot) goto 1
        goto 5

      elseif (key.eq.'m') then     ! zero out a section of the spectrum
        i = nearxi(cx)
        call xp_mark_position(x(i),2)
        print *,'Choose second limit...'
        call PG_CURSOR(cx,cy,key)
        j = nearxi(cx)
        do ii=min(i,j),max(i,j)
          y(ii) = 0.
        enddo
        modified = .true.
        goto 1

      elseif (key.eq.'M') then     ! set a section to -1 ...
        i = nearxi(cx)
        call xp_mark_position(x(i),2)
        print *,'Choose second limit...'
        call PG_CURSOR(cx,cy,key)
        j = nearxi(cx)
        do ii=min(i,j),max(i,j)
          y(ii) = -1.
        enddo
        modified = .true.
        goto 1

      elseif (key.eq.'S') then     ! replace spectrum with continuum
        print *,
     . 'Hit "<sp>" twice to define segment of spectrum to replace with'
        print *,
     . 'continuum or hit "w" for whole spectrum or "a" to abort.'
        print *,
     . 'If "w", then data spectrum becomes an "also" spectrum.'
        call PG_CURSOR(cx,cy,key)
        if (key.eq.'w') then
          num_also = min(max_also,num_also+1)
          n_also(num_also) = np
          do ii=1,np
            x_also(ii,num_also) = x(ii)
            y_also(ii,num_also) = y(ii)
          enddo
          do ii=1,np
            y(ii)=c(ii)
            c(ii)=0.
          enddo
          modified= .true.
          contbuf = 0
          goto 1
        elseif (key.eq.' ') then
          i = max(2,min(np-1,nearxi(cx)))
          call xp_mark_position(x(i),2)
          call PG_CURSOR(cx,cy,key)
          if (key.eq.' ') then
            j = max(2,min(np-1,nearxi(cx)))
            call xp_mark_position(x(j),2)
            do ii=min(i,j),max(i,j)
              y(ii)=c(ii)
            enddo
            modified = .true.
            goto 1
          else
            print *,'Aborted continuum substitution.'
          endif
        else
          print *,'Aborted continuum substitution.'
        endif
        goto 5

      elseif (key.eq.'r') then     ! recover a section of the spectrum
        call xp_mark_position(cx,2)
        i = nearxi(cx)
        print *,'Choose second limit...'
        call PG_CURSOR(cx,cy,key)
        j=nearxi(cx)
        do ii=min(i,j),max(i,j)
          y(ii) = yo(ii)
        enddo
        goto 1

      elseif (key.eq.'e') then     ! change a segment of spec. to a constant
        i = nearxi(cx)
        call xp_mark_position(x(i),2)
        print *,'Choose second limit...'
        call PG_CURSOR(cx,cy,key)
        j = nearxi(cx)
        print '(a,$)',' Enter new data value (-99=abort) : '
        read(5,*,err=9) r
        if (r.ne.-99.) then
          do ii=min(i,j),max(i,j)
            y(ii) = r
          enddo
          modified = .true.
          goto 1
        endif
        goto 5

      elseif (key.eq.'H') then     ! dots mode
        if (dots) then
          dots = .false.
        else
          dots = .true.
        endif
        goto 2

      elseif (key.eq.'h') then     ! histogram mode
        if (hist) then
          hist = .false.
        else
          hist = .true.
        endif
        goto 2

      elseif (key.eq.'f') then     ! fix y limits
        if (yfix) then
          yfix = .false.
        else
          yfix = .true.
        endif
        print *,'yfix = ',yfix
        goto 5

      elseif (key.eq.'F') then     ! fix y zero
        if (yfixzero) then
          yfixzero = .false.
        else
          yfixzero = .true.
        endif
        print *,'yfixzero = ',yfixzero
        goto 1

      elseif (key.eq.'Q') then     ! C IV?
        call xp_checkline(1548.202,1550.774,cx,1)
        goto 5

      elseif (key.eq.'W') then     ! Mg II?
        call xp_checkline(2796.352,2803.531,cx,1)
        goto 5

      elseif (key.eq.'E') then     ! D I?
        call xp_checkline(1215.3394,1215.6701,cx,1)
        goto 5

      elseif (key.eq.'Z') then     ! Other line?
        print *,'   a = O VI  1031.927  1037.616'
        print *,'   b = N V   1238.821  1242.804'
        print *,'   c = Si IV 1393.755  1402.770'
        print *,'   d = C IV  1548.202  1550.774'
        print *,'   e = AlIII 1854.716  1862.790'
        print *,'   f = Mg II 2796.352  2803.531'
        print *,'   g = Ca II 3934.777  3969.591'
        print *,'   h = Na I  5891.583  5897.558'
        print *,'                  Hit key...'
        call PG_CURSOR(cx,cy,key)
        if (key.eq.'a') then
          call xp_checkline(1031.927,1037.616,cx,1)
        elseif (key.eq.'b') then
          call xp_checkline(1238.821,1242.804,cx,1)
        elseif (key.eq.'c') then
          call xp_checkline(1393.755,1402.770,cx,1)
        elseif (key.eq.'d') then
          call xp_checkline(1548.202,1550.774,cx,1)
        elseif (key.eq.'e') then
          call xp_checkline(1854.716,1862.790,cx,1)
        elseif (key.eq.'f') then
          call xp_checkline(2796.352,2803.531,cx,1)
        elseif (key.eq.'g') then
          call xp_checkline(3934.777,3969.591,cx,2)
        elseif (key.eq.'h') then
          call xp_checkline(5891.583,5897.558,cx,2)
        else
          print *,'Unknown key.'
        endif
        goto 5

      elseif (key.eq.'@') then     ! SPECIAL
        r1=cx
        call xp_mark_position(r1,2)
        print *,'Choose second position...'
        call PG_CURSOR(cx,cy,key)
        r2=cx
        call xp_mark_position(r2,2)
        i1 = nearxi(r1)
        i2 = nearxi(r2)
C Find high and low and residual intensity.
        j=0
        do i=i1,i2
          j=j+1
          arr(j)=y(i)
        enddo
        call find_median(arr,j,r3)
        r3 = arr(1)/arr(j)
C Centroid.
        r4=0.
        r5=0.
        do i=i1,i2
          r4 = r4 + x(i)*(-1.*(y(i)-arr(j)))
          r5 = r5 + (-1.*(y(i)-arr(j)))
        enddo
        r4 = r4/r5
C EQW.
        r5=0.
        do i=i1,i2
          r5 = r5 + ( ( 1. - (y(i)/arr(j)) ) * (x(i+1)-x(i)) )
        enddo
        write(6,'(2f10.3,f7.3,f10.3,f9.3)') r1,r2,r3,r4,r5
        write(9,'(2f10.3,f7.3,f10.3,f9.3)') r1,r2,r3,r4,r5
        goto 5

      elseif (key.eq.'y') then     ! zoom out on y axis
        if (yfixzero) then
          ymax=ymax*1.5
        else
          r = (ymax-ymin)/2.
          ymin = ymin - r
          ymax = ymax + r
        endif
        goto 2

      elseif (key.eq.'w') then     ! whole plot
        xmin = x(1) - Xgap
        xmax = x(np)+ Xgap
        goto 1

      elseif (key.eq.'l') then     ! set limits
        print '(a,$)','xmin,xmax and ymin,ymax (0,0=old) : '
        read(5,*,err=9) r1,r2,r3,r4
        if ((r1.ne.0.).or.(r2.ne.0.)) then
          xmin=r1
          xmax=r2
        endif
        if ((r3.ne.0.).or.(r4.ne.0.)) then
          ymin=r3
          ymax=r4
        else
          goto 1
        endif
        goto 2

      elseif (key.eq.'i') then     ! zoom in
        r  = (xmax-xmin)/4.
        xmin = max(x(1) -Xgap,min(x(max(1,np-3)),cx-r))
        xmax = min(x(np)+Xgap,max(x(min(np,3  )),cx+r))
        goto 1

      elseif (key.eq.'I') then     ! BIG zoom in
        r  = (xmax-xmin)/12.
        xmin = max(x(1) -Xgap,min(x(max(1,np-3)),cx-r))
        xmax = min(x(np)+Xgap,max(x(min(np,3  )),cx+r))
        goto 1

      elseif (key.eq.'o') then     ! zoom out
        r = xmax-xmin
        xmin = max(x(1) -Xgap,min(x(max(1,np-3)),cx-r))
        xmax = min(x(np)+Xgap,max(x(min(np,3  )),cx+r))
        goto 1

      elseif (key.eq.'O') then     ! BIG zoom out
        r = (xmax-xmin)*3.
        xmin = max(x(1) -Xgap,min(x(max(1,np-3)),cx-r))
        xmax = min(x(np)+Xgap,max(x(min(np,3  )),cx+r))
        goto 1

      elseif (key.eq.'p') then     ! pan
        r = (xmax-xmin)/2.
        xmin = max(x(1) -Xgap,min(x(max(1,np-3)),cx-r))
        xmax = min(x(np)+Xgap,max(x(min(np,3  )),cx+r))
        goto 1

      elseif (key.eq.'[') then     ! go left
        r = xmax-xmin
        xmin = max(x(1)-Xgap,xmin-(r*0.9))
        xmax = xmin + r
        goto 1

      elseif (key.eq.']') then     ! go right
        r = xmax-xmin
        xmax = min(x(np)+Xgap,xmax+(r*0.9))
        xmin = xmax - r
        goto 1

      elseif (key.eq.'{') then     ! go left a lot
        r = xmax-xmin
        xmin = max(x(1)-Xgap,xmin-(r*2.0))
        xmax = xmin + r
        goto 1

      elseif (key.eq.'}') then     ! go right a lot
        r = xmax-xmin
        xmax = min(x(np)+Xgap,xmax+(r*2.0))
        xmin = xmax - r
        goto 1

      elseif (key.eq.'A') then     ! Left Mouse Button (select x limits)
        r1=max(x(1)-Xgap,min(x(np)+Xgap,cx))
        print *,' Select second x limit...'
        call PG_CURSOR(cx,cy,key)
        r2=max(x(1)-Xgap,min(x(np)+Xgap,cx))
        xmin=min(r1,r2)
        xmax=max(r1,r2)
        goto 1

      elseif (key.eq.'D') then     ! Middle Mouse Button (select both limits)
        r1=max(x(1)-Xgap,min(x(np)+Xgap,cx))
        r3=cy
        print *,' Select second limit...'
        call PG_CURSOR(cx,cy,key)
        r2=max(x(1)-Xgap,min(x(np)+Xgap,cx))
        xmin=min(r1,r2)
        xmax=max(r1,r2)
        ymin=min(r3,cy)
        ymax=max(r3,cy)
        goto 2

      elseif (key.eq.'X') then     ! Right Mouse Button (select y limits)
        r=cy
        print *,' Select second y limit...'
        call PG_CURSOR(cx,cy,key)
        ymin=min(r,cy)
        ymax=max(r,cy)
        goto 2

      elseif (key.eq.'#') then     ! arc-line centroid
        call xp_arccent(cx,cw,cp)
        goto 5

      elseif (key.eq.'%') then     ! set zero-point
        print '(a,1pe10.3,a,$)',' Enter zero-point [',cy,']: '
        read(5,'(a)',err=9) c80
        if (c80.eq.' ') then
          ZeroPoint = cy
        else
          ZeroPoint = valread(c80)
        endif
        print '(a,1pe10.3)',' New zero-point :',ZeroPoint
        goto 3

      elseif (key.eq.'*') then     ! multiply spectrum.
        print '(2a)','IN PLOT WINDOW: Enter scale factor.'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        print *,'Multiplying data array by:',r8
        do i=1,np
          y(i) = sngl( dble(y(i)) * r8 )
        enddo
        if (errbuf.gt.0) then
          print *,'Multiplying error array by:',r8
          do i=1,np
            e(i) = sngl( dble(e(i)) * r8 )
          enddo
        endif
        if (contbuf.gt.0) then
          print *,'Multiplying continuum array by:',r8
          do i=1,np
            c(i) = sngl( dble(c(i)) * r8 )
          enddo
        endif
        modified=.true.
        goto 1

      elseif (key.eq.'^') then     ! shift wavelengths.
        print '(2a)',
     .  'IN PLOT WINDOW: Enter shift in wavelength units.'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        print *,'Shifting x values by:',r8
        print *,
     .  'WARNING: This will not be saved when files are written.'
        do i=1,np
          x(i) = sngl(dble(x(i))+r8)
        enddo
        goto 1
    
C Centroid, equivalent width, column density.
      elseif ((key.eq.'C').or.(key.eq.'c').or.(key.eq.'&')) then
        print *,
     .  'Hit "<sp>" twice to define bounds for centroid and E.W.'
        call PG_CURSOR(cx,cy,c1)
        i = max(2,min(np-1,nearxi(cx)))
        call xp_mark_position(x(i),2)
        call PG_CURSOR(cx,cy,c1)
        j = max(2,min(np-1,nearxi(cx)))
        call xp_mark_position(x(j),2)
        if (key.eq.'C') then
          call xp_centew(min(i,j),max(i,j),1)
        elseif (key.eq.'&') then
          call xp_centew(min(i,j),max(i,j),2)
        else
          call xp_centew(min(i,j),max(i,j),0)
        endif
        goto 5
    
C Centroid, equivalent width, column density.
      elseif ((key.eq.'C').or.(key.eq.'c').or.(key.eq.'&')) then
        print *,
     .  'Hit "<sp>" twice to define bounds for centroid and E.W.'
        call PG_CURSOR(cx,cy,c1)
        i = max(2,min(np-1,nearxi(cx)))
        call xp_mark_position(x(i),2)
        call PG_CURSOR(cx,cy,c1)
        j = max(2,min(np-1,nearxi(cx)))
        call xp_mark_position(x(j),2)
        if (key.eq.'C') then
          call xp_centew(min(i,j),max(i,j),1)
        elseif (key.eq.'&') then
          call xp_centew(min(i,j),max(i,j),2)
        else
          call xp_centew(min(i,j),max(i,j),0)
        endif
        goto 5

      elseif (key.eq.'d') then     ! divide continuum
        call xp_contdiv(replot)
        if (replot) then
          modified = .true.
          yfix = .false.
          print *,'yfix = ',yfix
          goto 1
        endif
        goto 5
    
      elseif (key.eq.'z') then     ! zap a pixel
        i = nearxi(cx)
        y(i) = xp_ymedian(max(1,i-4),min(np,i+4))
        modified = .true.
        call ReBin()
        goto 2
    
      elseif ((key.eq.'~').or.(key.eq.'`').or.(key.eq.char(9))) then
        call xp_other_options(replot,ok)
        if (.not.ok) return
        if (replot) goto 1
        goto 5

      elseif (key.eq.char(27)) then     ! ESC- quit
        return
    
      endif

C Auxilary key options: keys a,g,n,t,v,x,... (q),(s),...

C QAL line identifications.
      if (Aux_Mode(1:3).eq.'qal') then

        if (key.eq.'g') then
          call xp_QAL_linelist_options(cx,cy,' ')
          goto 3
  
        elseif (key.eq.'a') then
          call xp_QAL_linelist_options(cx,cy,'a')
          goto 3
  
        elseif (key.eq.'x') then
          call xp_QAL_linelist_options(cx,cy,'c')
          goto 3
  
        elseif (key.eq.'v') then
          call xp_QAL_linelist_options(cx,cy,'d')
          goto 3

        elseif (key.eq.'t') then     ! Set QAL line.
          call xp_QAL_setview(cx,replot)
          if (replot) then
            goto 1
          else
            goto 5
          endif

        elseif (key.eq.'n') then
          x0=cx 
          print '(a,f7.3,4x,a,f8.1)',
     .  ' New z=',(x0/1215.67)-1.0,'Wave=',x0
          call PGSCI(2)
          r81=0.d0
          call RelativeVelocity(dble(x0),r81,230.d0)
          call PG_VLine(ymin,ymax,sngl(r81))
          r82=0.d0
          call RelativeVelocity(dble(x0),r82,-230.d0)
          call PG_VLine(ymin,ymax,sngl(r82))
          call PGSCI(1)
          print *,
     .  'Hit <space> or <q> to quit or click twice for bounds.'
          call PG_CURSOR(cx,cy,key)
          if (key.eq.'q') goto 5
          if (key.eq.' ') then
            x1 = sngl(min(r81,r82))
            x2 = sngl(max(r81,r82))
          else
            x1=cx
            call PGSLS(4)
            call PG_VLine(ymin,ymax,x1)
            call PG_CURSOR(cx,cy,key)
            x2=cx
            call PG_VLine(ymin,ymax,x2)
            call PGSLS(1)
          endif
          call PGQWIN(r1,r2,r3,r4)
          call PGEND
          call xp_mark_lyman(x0,x1,x2)
          call PGBEGIN(0,'/XDISP',-1*nh,nv)
          xmin = r1
          xmax = r2
          call PGASK(pg_ask)
          call PGVSTAND
          goto 1

        endif

      endif


      print *,'Unknown key command... use "?" for menu.'
      goto 5
     
9     print *,'Error in xp_interact.'
      goto 5

      end


C------------------------------------------------------------------------
C
      subroutine xp_main_help
      implicit none
      include 'xplot.inc'
C
C                    ----+----1----+----2----+----3----+----4
      print *,
     .'..............................................................'
      print '(a,a)','#=arc-line centroid,  c/C/&=centroid a',
     .              'nd equivalent width++,  <cr>=show data,'
C
      print '(a,a)','f=(un)yfix,  F=(un)yfixzero,  y=y zoom, ',
     .              ' e=edit data,   ~/`/TAB = other options'
C
      print '(a,a)','d=divide continuum,  h=(no)histogram, ',
     .              'm=zero section,  r=recover section,'
C
C                    ----+----1----+----2----+----3----+----4
      print '(a,a)','i/I=zoom in,  o/O=zoom-out,  p=pan,  [/{',
     .              '=go left,  ]/}=go right, l=enter limits,'
C
      print '(a,a)','LMB=x limits, MMB=both limits, RMB=y lim',
     .              'its,  w=show whole spectrum, H=(no)dots,'
C
C                    ----+----1----+----2----+----3----+----4
      print '(a,a)','<sp>=section marking (statistics, curve ',
     .              'fitting, set continuum,...),'
C
      print '(a,a)','S=substitute spectrum w/continuum,  u=up',
     .              'date(refresh),  z=zap,  ESC=exit XPLOT.'
C
      print '(a,a)','k=fit/replace a kink in the spectrum.',
     .              '  Q=C IV?  W=Mg II?  E=D I?  Z=other?'
C
C                    ----+----1----+----2----+----3----+----4
      print '(a,a)','J=Make PostScript plot of current plot.',
     .              '  5=New row.  +/-=change row.  b=rebin.'
C
      print '(a,a)','@=write wavelengths.  ^=shift in x.  ',
     .              '%=set zero point.  *=multiply spectrum.'
C
C QAL line identifications.
      if (Aux_Mode(1:3).eq.'qal') then
        print '(a,a)','g = QAL line list option table ;  ',
     .                't = Set/show QAL ;  a = QAL option "a";'
        print '(a,a)','x = QAL option "c" ;  v = QAL option "d" . ',
     .                'n = Ly-alpha+ '
      endif

      return
      end

C----------------------------------------------------------------------
C Set QAL wavelength or view other line or return to original.
C
      subroutine xp_QAL_setview(cx,replot)
      implicit none
      real*4 cx
      logical replot
      include 'xplot.inc'
      real*8 r8,wave_out,osc_out,gamma_out,zpo,owv
      character*8 ion_out
      real*4 r1,r2
      character*80 c80
C
      common /XPQALSETVIEWBLK/ zpo
C
      print '(2a)','IN PLOT WINDOW: ',
     .      'Enter QAL wavelength (-wave to set, 0=return view).'
      call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
      if (r8.lt.0.) then
        r8=abs(r8)
        call xp_GrabLineData(r8,3,wave_out,osc_out,gamma_out,ion_out)
        zpo = dble(cx)/wave_out 
        owv = wave_out
        print '(a,f9.3,2x,2a,f9.3,a,f8.5)',
     .        ' Set: ',wave_out,ion_out,' at',owv,'  Za=',zpo-1.d0
        za  = sngl(zpo-1.d0)
        abs_priority=3
        call xp_absorption_lines(3)
        write(c80,'(a,f8.5,a,i2)',err=9)
     .   'z=',za,'   priority=',abs_priority
        call PGMTEXT('B',3.,0.,0.,c80)
        lambda_zero_velocity = zpo*owv
        replot=.true.
      elseif (abs(r8).lt.1.d-30) then
        r2 = sngl(zpo*owv)
        r1 = ( r2*(xmax-xmin)/((xmin+xmax)/2.) )/2.
        xmin = r2 - r1
        xmax = r2 + r1
        replot=.true.
        lambda_zero_velocity = zpo*owv
      else
        call xp_GrabLineData(r8,3,wave_out,osc_out,gamma_out,ion_out)
        r2 = sngl(zpo*wave_out)
        print '(a,f9.3,2x,2a,f9.3)',
     .  'View: ',wave_out,ion_out,' at',r2 
        if ((r2.lt.x(1)).or.(r2.gt.x(np))) goto 99
        r1 = ( r2*(xmax-xmin)/((xmin+xmax)/2.) )/2.
        xmin = r2 - r1
        xmax = r2 + r1
        replot=.true.
        lambda_zero_velocity = zpo*wave_out
      endif
      show_velocity=.true.
      return
9     print *,'Error reading value in xp_QAL_setview.'
      return
99    print '(a,f10.3,a,f10.3)',
     . 'Error-- Line is out of range for this spectrum:',
     .  x(1),' to',x(np)
      return
      end

C----------------------------------------------------------------------
C Extra options.
C
      subroutine xp_QAL_linelist_options(cx,cy,inkey)
C
      implicit none
      real*4 cx,cy
      character inkey*1
      include 'xplot.inc'
      character key*1
      integer*4 i,besti,s1,s2,bl_n,nearxi,nearLL,k
      real*8 xew,xewerr,xcent,sigs,r8,resint,the_rest_wave
      real*8 wave_out,osc_out,gamma_out
      character*8 ion_out
C
      common /XPQALLINELISTOPTIONSBLK/ the_rest_wave
C
C Menu.
      print *,'d=delete line, a=add line, c=centroid and add line,'
      print *,
     .'w=add rest wavelength to line, u=set uncertainty of line.'
      print *,'t=try to id line based on known redshifts.'
      print *,
     .'O=toggle display of redshifts, o=toggle display of lines. '
      print *,
     .'z=print redshift list.  g=add the same rest wavelength as last'
      if (inkey.eq.' ') then
        call PG_CURSOR(cx,cy,key)
      else
        key=inkey
      endif
      if (key.eq.'o') then
        if (ll_showQ) then
          ll_showQ=.false.
        else
          ll_showQ=.true.
        endif
      elseif (key.eq.'O') then
        if (ll_showZ) then
          ll_showZ=.false.
        else
          ll_showZ=.true.
        endif
      elseif (key.eq.'z') then
        print *,'Known redshifts:'
        do i=1,ll_z_n
          print '(i4,f7.4)',i,ll_z(i)
        enddo
      elseif (key.eq.'t') then
C Try to guess line from known redshifts.
        r8=dble(cx)
        do i=1,ll_z_n
          do k=1,qal_n
            if (abs((qal_wav(k)*(ll_z(i)+1.d0))-r8).lt.0.20) then
              print '(f9.2,a,f9.2, 2x,a,f9.3,f8.5, a,f9.2)',
     .        r8,' : ',qal_wav(k)*(ll_z(i)+1.d0),
     .        qal_ion(k)(1:8),qal_wav(k),ll_z(i),
     .        '   ...C IV 1548 at ',1548.20d0*(1.d0+ll_z(i))
            endif
          enddo
        enddo
      elseif (key.eq.'u') then
C Uncertainty.
        besti = nearLL(cx)
        print '(2a)','IN PLOT WINDOW: ',
     .     'Enter uncertainty level (1=ok,2=?,3=??,-1=unknown,-2=atm).'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        i = nint(r8)
        ll_crty(besti) = max(-2,min(3,i))
        if (ll_crty(besti).eq.0) ll_crty(besti)=1
        if (ll_crty(besti).lt.0) ll_rw(besti)=0.
      elseif (key.eq.'d') then
C Delete line.
        besti = nearLL(cx)
        do i=besti,ll_n-1
          ll_cent(i) = ll_cent(i+1)
          ll_ew(i)   = ll_ew(i+1)
          ll_ewerr(i)= ll_ewerr(i+1)
          ll_rw(i)   = ll_rw(i+1)
          ll_ri(i)   = ll_ri(i+1)
          ll_crty(i) = ll_crty(i+1)
        enddo
        ll_n=ll_n-1
      elseif ((key.eq.'a').or.(key.eq.'c')) then
C Find info on this line.
        i = nearxi(cx)
        s1= i - 2
        s2= i + 2
        call xp_Find_EW(s1,s2,xew,xewerr,xcent,resint,sigs)
C Add line.
        bl_n = 1
        if (key.eq.'a') bl_cent(1) = dble(cx)
        if (key.eq.'c') bl_cent(1) = xcent
        bl_ew(1)   = xew
        bl_ewerr(1)= xewerr
        bl_rw(1)   = 0.d0
        bl_ri(1)   = resint
        bl_crty(1) = 1
        print '(a,f10.4,4f10.5)',
     .        ' New line: ',bl_cent(1),bl_ew(1),bl_ewerr(1),
     .        bl_ew(1)/bl_ewerr(1),bl_rw(1)
        call xp_CleanLineList(bl_n)
      elseif (key.eq.'w') then
C Add rest wavelength to linelist database.
        besti = nearLL(cx)
        print '(2a)','IN PLOT WINDOW: ',
     .     'Enter value close to rest wave.(0=none,-1=unknown,-2=atm).'
        call PG_ReadPlotReal(r8,1.2,'T',0.5,0.4)
        r8 = max(-2.,r8)
        the_rest_wave = r8
        if (nint(r8).eq.-1) then
          wave_out = 0.d0
          ll_crty(besti) = -1
        elseif (nint(r8).eq.-2) then
          wave_out = 0.d0
          ll_crty(besti) = -2
        elseif (nint(r8).eq.0) then
          wave_out = 0.d0
          ll_crty(besti) = 1
        else
          call xp_GrabLineData(r8,3,wave_out,osc_out,gamma_out,ion_out)
          print '(f9.3,2x,a)',wave_out,ion_out
          ll_crty(besti) = 1
        endif
        ll_rw(besti) = wave_out
      elseif (key.eq.'g') then
C Add same rest wavelength to linelist database as last entry.
        besti = nearLL(cx)
        r8 = the_rest_wave
        if (r8.lt.0.) then
          wave_out = 0.d0
        else
          call xp_GrabLineData(r8,3,wave_out,osc_out,gamma_out,ion_out)
          print '(f9.3,2x,a)',wave_out,ion_out
        endif
        ll_rw(besti) = wave_out
      else
        print *,'Command option not found-- return to main menu.'
      endif
      return
9     print *,'Error reading input.'
      return
      end

C----------------------------------------------------------------------
C Given cursor position of Lyman-alpha, show lyman-alpha, C IV, Lyman-Beta,
C and O VI.
C
      subroutine xp_mark_lyman(x0,x1,x2)
      implicit none
      real*4 x0,x1,x2
      include 'xplot.inc'
      real*4 rv,vel_range,xx0,xx1,xx2,zpo0,zpo1,zpo2,size,cx,cy,valread
      character key*1
      real*8 lam1,lam2,velb,velr
      integer*4 lyalf,lybet,ovi1,ovi2,civ
      parameter(vel_range=900.)

C Redshift plus one.
      zpo0 = x0 / 1215.67
      zpo1 = x1 / 1215.67
      zpo2 = x2 / 1215.67
      call PGBEGIN(0,'/XDISP',2,2)
      call PGQCH(size)
      call PGSCH(2.0)

C Lyman-alpha.
      xx0 = zpo0 * 1215.67
      rv = vel_range
      call get_rv_range(xx0,rv,xmin,xmax)
      call xp_plot_ion()
      call PGSCI(3)
      call PG_VLine(ymin,ymax,xx0)
      call PGSCI(2)
      xx1 = zpo1 * 1215.67
      xx2 = zpo2 * 1215.67
      call PG_VLine(ymin,ymax,xx1)
      call PG_VLine(ymin,ymax,xx2)
      call PGSCI(1)
      call PGMTEXT('T',0.5,0.1,0.0,'Ly\\ga')

C Lyman-beta.
      xx0 = zpo0 * 1025.7223
      call get_rv_range(xx0,rv,xmin,xmax)
      call xp_plot_ion()
      call PGSCI(3)
      call PG_VLine(ymin,ymax,xx0)
      call PGSCI(2)
      xx1 = zpo1 * 1025.7223
      xx2 = zpo2 * 1025.7223
      call PG_VLine(ymin,ymax,xx1)
      call PG_VLine(ymin,ymax,xx2)
      call PGSCI(1)
      call PGMTEXT('T',0.5,0.1,0.0,'Ly\\gb')

C C IV.
      xx0 = zpo0 * 1549.48
      rv = 1.8 * vel_range
      call get_rv_range(xx0,rv,xmin,xmax)
      call xp_plot_ion()
      call PGSCI(3)
      xx0 = zpo0 * 1548.195
      call PG_VLine(ymin,ymax,xx0)
      xx0 = zpo0 * 1550.770
      call PG_VLine(ymin,ymax,xx0)
      call PGSCI(2)
      xx1 = zpo1 * 1548.195
      xx2 = zpo2 * 1550.770
      call PG_VLine(ymin,ymax,xx1)
      call PG_VLine(ymin,ymax,xx2)
      call PGSCI(1)
      call PGMTEXT('T',0.5,0.1,0.0,'C IV')

C O VI.
      xx0 = zpo0 * 1034.7714
      rv = 2. * vel_range
      call get_rv_range(xx0,rv,xmin,xmax)
      call xp_plot_ion()
      call PGSCI(3)
      xx0 = zpo0 * 1031.9261
      call PG_VLine(ymin,ymax,xx0)
      xx0 = zpo0 * 1037.6167
      call PG_VLine(ymin,ymax,xx0)
      call PGSCI(2)
      xx1 = zpo1 * 1031.9261
      xx2 = zpo2 * 1031.9261
      call PG_VLine(ymin,ymax,xx1)
      call PG_VLine(ymin,ymax,xx2)
      xx1 = zpo1 * 1037.6167
      xx2 = zpo2 * 1037.6167
      call PG_VLine(ymin,ymax,xx1)
      call PG_VLine(ymin,ymax,xx2)
      call PGSCI(1)
      call PGMTEXT('T',0.5,0.1,0.0,'O VI')

C Ask quality of each line.
      print *,'Hit 1,2,3 or 4 for Lyman-alpha (5="1" for all lines).'
      call PG_CURSOR(cx,cy,key)
      lyalf = nint(valread(key)) 
      if (lyalf.eq.5) then
        lyalf=1
        lybet=1
        civ  =1
        ovi1 =1
        ovi2 =1
        goto 7
      endif

      print *,'Hit 1,2,3 or 4 for Lyman-beta.'
      call PG_CURSOR(cx,cy,key)
      lybet = nint(valread(key)) 

      print *,'Hit 1,2,3 or 4 for C IV.'
      call PG_CURSOR(cx,cy,key)
      civ   = nint(valread(key)) 

      print *,'Hit 1,2,3 or 4 for O VI blue.'
      call PG_CURSOR(cx,cy,key)
      ovi1  = nint(valread(key)) 

      print *,'Hit 1,2,3 or 4 for O VI red.'
      call PG_CURSOR(cx,cy,key)
      ovi2  = nint(valread(key)) 

7     continue

C Velocities.
      lam1= dble(x0)
      lam2= dble(x1)
      velb= 0.d0
      call RelativeVelocity(lam1,lam2,velb)
      lam2= dble(x2)
      velr= 0.d0
      call RelativeVelocity(lam1,lam2,velr)

C Print out to file.
      write(6,'(f9.5,2f8.2,1x,5(i2),2x,a20)') 
     .   zpo0-1.,velb,velr,lybet,ovi1,ovi2,lyalf,civ,datafile(1:20)
      write(25,'(f9.5,2f8.2,5(i2),2x,a20)') 
     .   zpo0-1.,velb,velr,lybet,ovi1,ovi2,lyalf,civ,datafile(1:20)

      call PGSCH(size)
      call PGEND

      return
      end

C----------------------------------------------------------------------
      subroutine get_rv_range(xx,rv,x1,x2)
      implicit none
      real*4 xx,rv,x1,x2
      real*8 lam1,lam2,vel
      vel = -1.d0 * dble(rv)
      lam1= dble(xx)
      lam2= 0.d0
      call RelativeVelocity(lam1,lam2,vel)
      x1  = sngl(lam2)
      lam2= 0.d0
      vel = dble(rv)
      call RelativeVelocity(lam1,lam2,vel)
      x2  = sngl(lam2)
      return
      end

C----------------------------------------------------------------------
C Plot ion.
      subroutine xp_plot_ion()
      implicit none
      include 'xplot.inc'
      integer*4 i1,i2,nearxi
      ymin=0.0
      ymax=1.2
      call PGPAGE
      call PGWINDOW(xmin,xmax,ymin,ymax)
      call xp_BetterBox
      i1 = nearxi(xmin)
      i2 = nearxi(xmax)
      call PGLINE(1+i2-i1,x(i1),y(i1))
      call PGSCI(4)
      call PG_HLine(xmin,xmax,1.0)
      call PGSCI(1)
      return
      end
