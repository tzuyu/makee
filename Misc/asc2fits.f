C asc2fits.f     ASCII style FITS file to IEEE FITS file   tab  Oct 92
C
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C
C
      implicit none

      character*115200 header         ! FITS header, (720 cards max).
      real*4 a(4000000)              ! FITS data, (2000 x 2000).

      character*80 card, arg(2), infile, outfile
      integer*4 i,narg,n1,n2,inhead,maxchars,ierr,lastchar,npix
      logical ok

      call arguments(arg,narg,2)
      if (narg.lt.1) then
        print *,
     . 'Syntax: asc2fits (root ASCII FITS filename, i.e. root.asc )'
        print *,'  '
        print *,
     . '  Converts an ASCII style FITS image file to IEEE FITS file.'
        print *,
     . '  Assumes input file has ".asc" extension, the result'
        print *,
     . '  has ".fits" extension.  Maximum image size is 4,000,000'
        print *,'  pixels (2000 by 2000).'
        print *,'  '
        print *,
     . '  Beginning of ASCII file should look similar to a FITS'
        print *,
     . '  header with one card per line.  The data should follow'
        print *,
     . '  the "END" card as a free-format ASCII number, one number'
        print *,
     . '  per line, row 1 then row 2 then row 3, etc.'
        print *,'  '
        stop
      endif

      infile = arg(1)
      outfile= ' '
      i=index(infile,'.asc')
      if (i.eq.0) then
        outfile= infile(1:lastchar(infile))//'.fits'
        infile = infile(1:lastchar(infile))//'.asc'
      else
        outfile= infile(1:i)//'fits'
      endif
        
      open(1,file=infile,status='old',iostat=ierr)
      if (ierr.ne.0) then
        print *,'Error opening : ',infile
        stop
      endif

      maxchars = len(header) - 300
      header = ' '
      i = 1
      card = ' '
      do while ((card(1:4).ne.'END ').and.(i.lt.maxchars))
        read(1,'(a)',end=7,err=9) card
        header(i:i+79) = card
        i = i + 80
      enddo
      if (i.ge.maxchars) goto 7

      n1 = inhead('NAXIS1',header)
      n2 = inhead('NAXIS2',header)
      if (n1.lt.1) then
        print *,'Error- NAXIS1 < 1 .'
        goto 99
      endif
      if (n2.lt.1) then
        print *,'Error- NAXIS2 < 1 .'
        goto 99
      endif
      npix = n1 * n2
      print *,
     . 'Output: ',outfile(1:lastchar(outfile)),'  with',npix,' pixels.'

      do i=1,npix
        read(1,*,end=8,err=9) a(i)
      enddo
      close(1)

      call writefits(outfile,a,header,ok)
      if (.not.ok) then
        print *,'Error writing: ',outfile(1:lastchar(outfile))
      endif

      stop
7     print *,'Error- No END header card found.'
      stop
8     print *,'Error- end of file found before end of data.'
      stop
9     print *,'Error reading : ',infile
99    stop
      end
