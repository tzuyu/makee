/* clcalc.c       Command Line Calculator          tab  Jun92/Oct99  */

#include "ctab.h"
#include "cmisc.h"

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
int main(int argc, char *argv[])
{
/**/
char arg[100][100];
int narg;
char wrd[100];
/**/
char op;
char c9a[100];
char c9b[100];
double rr,result;
int d2,d8,ee,ptr;
/**/

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Defaults. */
d2 = 0;
d8 = 0;
op = ' ';
rr = 0.;

/* Get options. */
d2 = cfindarg(arg,&narg,"-d2",'s',wrd);
d8 = cfindarg(arg,&narg,"-d8",'s',wrd);
ee = cfindarg(arg,&narg,"-ee",'s',wrd);

/* Syntax. */
if ( narg < 3 ) {
  printf("Syntax: clcalc  (number) (operator: + - x / ^ ) (number)\n");
  printf("                   [ (op) (num) ]  [ (op) (num) ] ...\n");
  printf("                   [ -d2 | -d8 | -ee ]\n");
  printf("\n");
  printf("  Command Line Calculator.\n");
  printf("  Enter a series of operations and numbers.\n");
  printf("\n");
  printf("  Options: -d2 : two decimal places.\n");
  printf("           -d8 : eight decimal places.\n");
  printf("           -ee : extended exponential.\n");
  printf("\n");
  exit(0);
}

if ( (narg%2) == 0 ) {
  fprintf(stderr,"ERROR: Must have odd number of parameters.\n");
  exit(-1);
}

/* Load command line arguments. */
result= GetLineValue(arg[1],1);

/* Select operator. */

ptr=1;
while (narg > ptr) {
  op = arg[ptr+1][0]; 
  rr = GetLineValue(arg[ptr+2],1);
  switch (op) {
    case '+': result = result + rr; break;
    case '-': result = result - rr; break;
    case 'x': result = result * rr; break;
    case '/': result = result / rr; break;
    case '^': result = pow(result,rr); break;
    default: printf("ERROR: Unknown operator: %c\n",op); exit(-1); break;
  }
  ptr = ptr + 2;
  if (narg > ptr) {
    if (d2 == 1) {
      printf("...sub:%3d: (%c%10.2f) %10.2f\n",(ptr-1)/2,op,rr,result);
    } else { 
      if (d8 == 1) {
        printf("...sub:%3d: (%c%16.8f) %16.8f\n",(ptr-1)/2,op,rr,result);
      } else {
        if (ee == 1) {
          printf("...sub:%3d: (%c%22.14e) %22.14e\n",(ptr-1)/2,op,rr,result);
        } else {
          WriteString9(rr,c9a);
          WriteString9(result,c9b);
          printf("...sub:%3d: (%c %s) %s\n",(ptr-1)/2,op,c9a,c9b);
        }
      }
    }
  }
}

/* Echo result. */
if (d2 == 1) {
  printf("          : (%c%10.2f) %10.2f  (result)\n",op,rr,result);
} else {
  if (d8 == 1) {
    printf("          : (%c%16.8f) %16.8f  (result)\n",op,rr,result);
  } else {
    if (ee == 1) {
    printf("          : (%c%22.14e) %22.14e  (result)\n",op,rr,result);
    } else {
    WriteString9(rr,c9a);
    WriteString9(result,c9b);
    printf("          : (%c %s) %s  (result)\n",op,c9a,c9b);
    }
  }
}

return(0);
}
