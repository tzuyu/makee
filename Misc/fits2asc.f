C fits2asc.f     IEEE FITS file to ASCII style FITS file   tab  Oct 92
C

      implicit none

      character*17280 header         ! FITS header, 6 x 2880 (216 cards)
      integer*4 maxpt
      parameter(maxpt = 656000)
      real*4 a(maxpt)                ! FITS data, 820 x 800

      character*80 card,arg(9),infile,outfile,wrd
      integer*4 i,narg,ierr,lastchar,npix,sc,ec,sr,er,nc,nr,j,pixno
      logical ok,findarg,nohead,rows

      call arguments(arg,narg,9)
      nohead = findarg(arg,narg,'-nohead',':',wrd,i)
      rows   = findarg(arg,narg,'-rows',':',wrd,i)
      if (narg.ne.1) then
        print *,'Syntax: fits2asc (IEEE FITS file) [-nohead] [-rows]'
        print *,'  Converts an IEEE FITS file to ASCII style FITS file.'
        print *,'  Assumes input file has ".fits" extension, the result'
        print *,'  has ".asc" extension.'
        print *,'   -nohead : Do no write FITS header.'
        print *,'   -rows   : Write out data with rows as the fastest'
        print *,'             varying indice.'
        stop
      endif

      infile = arg(1)
      outfile= ' '
      i=index(infile,'.fits')
      if (i.eq.0) then
        outfile= infile(1:lastchar(infile))//'.asc'
        infile = infile(1:lastchar(infile))//'.fits'
      else
        outfile= infile(1:i)//'asc'
      endif
        
      call readfits(infile,a,maxpt,header,ok)
      if (.not.ok) print *,'Error reading: ',infile(1:lastchar(infile))

      open(1,file=outfile,status='unknown',iostat=ierr)
      if (ierr.ne.0) then
        print *,'Error opening for output : ',outfile
        stop
      endif

      i = 1
      card = ' '
      do while (card(1:4).ne.'END ')
        card = header(i:i+79)
        if (.not.nohead) write(1,'(a)',err=9) card(1:lastchar(card))
        i = i + 80
      enddo

      call GetDimensions(header,sc,ec,sr,er,nc,nr)
      npix = nc*nr

      print *,
     . 'Output: ',outfile(1:lastchar(outfile)),'  with',npix,' pixels.'

      if (rows) then
        do i=1,nc
          do j=1,nr
            pixno = i + ((j-1)*nc)
            write(1,'(1pe15.8)',err=9) a(pixno)
          enddo
        enddo
      else
        do i=1,npix
          write(1,'(1pe15.8)',err=9) a(i)
        enddo
      endif

      close(1)

      stop
9     print *,'Error writing : ',outfile
99    stop
      end
