C Smooth a linear wavelength or pixel scale FITS spectrum.
C
      implicit none
      integer*4 maxpix
      parameter(maxpix=201000)
      real*4 a(maxpix)
      real*8 a8(maxpix),x8(maxpix),ao8(maxpix)
      character*115200 header
      character*80 infile,outfile,arg(9),wrd
      real*8 fwhm,valread8
      integer*4 npix,narg,i
      logical ok,findarg

      call arguments(arg,narg,9)
      fwhm=0.d0
      if (findarg(arg,narg,'fwhm=',':',wrd,i)) fwhm = valread8(wrd)
      if (narg.ne.2) then
        print *,
     .   'Syntax:  smoothfits (input FITS file) (output FITS file)'
        print *,
     .   '               (fwhm=r)'
        print *,
     .   '    Smooths a spectrum with a gaussian.  Full-Width-Half-'
        print *,
     .   '    Maximum is in x units (usually angstroms).'
        call exit(0)
      endif
      if (abs(fwhm).lt.1.e-15) then
        print *,'Must specify non-zero fwhm.'
        call exit(1)
      endif

      infile = arg(1)
      outfile= arg(2)
      call AddFitsExt(infile)
      call AddFitsExt(outfile)
      call readfitsxy8(infile,x8,a8,maxpix,npix)
      call readfits(infile,a,maxpix,header,ok)

      if (fwhm.lt.0.) then
        call var_convolve_gaussian(a8,x8,ao8,npix)
      else
        call convolve_gaussian(a8,x8,ao8,npix,fwhm)
      endif

      do i=1,npix
        a(i) = sngl(ao8(i))
      enddo

      call writefits(outfile,a,header,ok)

      stop
      end


C----------------------------------------------------------------------
C Convolve a gaussian (with variable fwhm) into an array of values.
C Input: a(),x(),npix.   Output: ao().
C
      subroutine var_convolve_gaussian(a,x,ao,npix)
      implicit none
      integer*4 npix,i,j
      real*8 a(npix),x(npix),ao(npix),gaussian_hwhm,hwhm,sum,wsum,xx,r8

      do i=1,npix
C
        if (mod(i,500).eq.0) print '(i9,f10.3)',i,x(i)
C
        hwhm = ( 2.15d0 + ((x(i)-5800.d0)*(9.375d-4)) ) / 2.d0
        sum =0.d0
        wsum=0.d0
        do j=1,npix 
          xx = (x(j)-x(i)) / hwhm
          if (abs(xx).lt.5.) then
            r8  = gaussian_hwhm(xx)
            sum = sum + ( a(j) * r8 )
            wsum= wsum+ r8
          endif
        enddo
        ao(i) = sum / wsum
      enddo

      return
      end

C----------------------------------------------------------------------
C Convolve a gaussian into an array of values.
C Input: a(),x(),npix,fwhm.   Output: ao().
C
      subroutine convolve_gaussian(a,x,ao,npix,fwhm)
      implicit none
      integer*4 npix,i,j
      real*8 a(npix),x(npix),ao(npix),fwhm,gaussian_hwhm,hwhm
      real*8 sum,wsum,xx,r8
      hwhm = fwhm/2.d0
      print *,'Convolving... npix=',npix

      do i=1,npix
        sum =0.d0
        wsum=0.d0
        do j=1,npix 
          xx = (x(j)-x(i)) / hwhm
          if (abs(xx).lt.4.) then
            r8  = gaussian_hwhm(xx)
            sum = sum + ( a(j) * r8 )
            wsum= wsum+ r8
          endif
        enddo
        ao(i) = sum / wsum
      enddo
      print *,'...done.'
      return
      end

