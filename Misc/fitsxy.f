C fitsxy.f     FITS spectrum x-y file creator   tab   June 1992

      implicit none
      integer*4 narg,i,npix,lastchar,ierr,oun
      character*80 arg(9),ffile,wrd
      integer*4 maxpt
      parameter(maxpt=301000)
      real*8 y(maxpt),x(maxpt),xs,xe,valread8
      logical findarg
       
      call arguments(arg,narg,9)
      xs = -1.d+30
      xe = +1.d+30
      if (findarg(arg,narg,'xs=',':',wrd,i)) xs = valread8(wrd)
      if (findarg(arg,narg,'xe=',':',wrd,i)) xe = valread8(wrd)
      if ((narg.lt.1).or.(narg.gt.2)) then
        print *,'Syntax: fitsxy (FITS file) [output file]  [xs=] [xe=]'
        call exit(0)
      endif

      ffile=' '
      ffile=arg(1)
      if (index(ffile,'.').eq.0) then
        ffile=ffile(1:lastchar(ffile))//'.fits'
      endif

      if (narg.gt.1) then
        open(2,file=arg(2),status='unknown',iostat=ierr)
        if (ierr.ne.0) then
          call printerr('Error opening : '//arg(2))
          stop
        endif
        oun=2
      else
        oun=6
      endif

      call readfitsxy8(ffile,x,y,maxpt,npix)

      if (npix.lt.0) goto 990
      do i=1,npix
        if ((x(i).gt.xs).and.(x(i).lt.xe)) then
          write(oun,'(f10.4,1pe14.6)') x(i),y(i)
        endif
      enddo 

      if (oun.ne.6) close(oun)

      call EXIT(0)
      stop

990   call EXIT(1)
      stop
      end
