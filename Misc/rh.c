/* rh.c                  tab  oct99 */

#include "ctab.h"
#include "cmisc.h"
#include "cufio.h"

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Read and print out a FITS header.
*/
int main(int argc, char *argv[])
{
/**/
const int headersize = 115200;
char header[115200];
/**/
char arg[900][100];
char fitsfile[100];
char wrd[100];
char object[100];
int ii,kk,narg,ptr,brief,iarg;
int sc,ec,sr,er,nc,nr;
/**/

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Option. */
brief = 0;
if (cfindarg(arg,&narg,"-b",'s',wrd)==1) { brief = 1; }


/* Syntax. */
if ( narg < 1 ) {
  printf("Syntax: rh [-b] (FITS file(s))\n");
  printf("\n");
  printf("  Option:  -b : brief one line listing.\n");
  printf("\n");
  exit(0);
}

/* Head of columns. */
if (brief == 1) printf(" Object Name        SC    EC    SR    ER    NC    NR  Filename\n");

/* Each FITS file. */
for (iarg=1; iarg<=narg; ++iarg) {

/* Read FITS file. */
  strcpy(fitsfile,arg[iarg]);
  cAddFitsExt(fitsfile);
  if (brief == 0) printf("Reading: %s\n",fitsfile);
  if ( creadfitsheader(fitsfile,header,headersize) == 0) {
    fprintf(stderr,"ERROR: rh: Cannot read file %s .\n",fitsfile);
    exit(0);
  }

/* Show whole header. */
  if (brief == 0) {

    fprintf(stdout,"...............................................\n");
    ptr=0;
    while (1) {
     substrcpy(header,ptr,ptr+79,wrd,0);
     wrd[80]='\0';
     fprintf(stdout,"%s\n",wrd);
     if (substrcmp(wrd,0,7,"END     ",0) == 0) break;
     ptr = ptr + 80;
     if (ptr > headersize-80) {
       fprintf(stderr,"ERROR: Did not find END card.\n");
       exit(0);
     }
    }
    fprintf(stdout,"...............................................\n");

/* Show brief one line header. */
  } else {
    cchead("OBJECT",header,headersize,object);
    kk = clc(object);
    for (ii=kk+1; ii<16; ++ii) { object[ii]=' '; }
    object[16] = '\0';
    ii = cGetDimensions(header,headersize,&sc,&ec,&sr,&er,&nc,&nr);
    printf("%s %5d %5d %5d %5d %5d %5d  %s\n",object,sc,ec,sr,er,nc,nr,fitsfile);

  }

}

return(0);
}
