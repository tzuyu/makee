C astro.f   
C Program to do various astrophysical calculations...
C T. A. Barlow  CASS/UCSD
C revised: Dec87, Feb88, Aug88, May89, Nov89
C revised for Sun: Mar92, Aug92
C
C These subroutines/functions use double-precision (real*8) for all the real
C   variables.  You should always use real*8 variables when calling them.
C
C Link with ua...
C
C          See ASTRO_LIB.FOR for subroutines and functions.

      implicit none
      integer*4 option
      real*8 z(0:20),h(0:20),q(0:20)

      h(1) =0.0
      q(1) =-9.0
      goto 20
10    print *,'   '
      print *,
     .'   Enter "0" for variables which are to be calculated or are to'
      print *,
     .'   remain the same.  Enter "-1" for to enter an array of values.'
      print *,'   '
      print *,'  (1)- Relative velocity given redshift(s)'
      print *,'  (2)- Relative velocity given wavelength(s)'
      print *,'  (3)- Vacuum-to-Air / Air-to-Vacuum conversions'
      print *,'  (4)- Precession'
      print *,'  (5)- Angular separation of two (RA,DEC) positions'
      print *,'  (6)- Cluster calculations'
      write(6,6010) h(1),q(1)
6010  format(3x,'(7)- Reset Ho and qo  (',F6.2,',',F6.3,')')
      print *,'  (8)- Luminosity, observed flux, proper distance(cm)'
      print *,'  (9)- Spatial distances, angles, proper distance(Mpc)'
      print *,' (10)- Look-back time, age of universe'
      print *,' (11)- Equatorial to galactic or ecliptic coordinates'
      print *,' (12)- Sky values: PA, air mass, refrac., julian day,...'
      print *,
     .' (13)- Tables of PA, air mass,... as func. of HA and DEC'
      print *,
     .' (14)- Relative transmission vs. hour angle table for an object.'
      print *,' (15)- Locate RA-DEC position from plate measurements.'
      print *,' (16)- Sun and moon information.'
      print *,
     . ' (17)- Decimal RA and DEC to/from hour,min,sec and deg,min,sec.'
      print *,' (18)- Sun daytime tables.'

20    write(6,6020)
6020  format(/,30X,'(0=option list, -1=quit) option >> ',$)
      read(5,'(i3)',err=20) option

      if (option.eq.0) goto 10
      if (option.eq.1) call RELVEL(z)
      if (option.eq.2) call RELVELW
      if (option.eq.3) call VACAIR
      if (option.eq.4) call PRECESS_IT
      if (option.eq.5) call ANGSEP
      if (option.eq.6) call CLUSTER
      if (option.eq.7) call RESETHQ(h,q)
      if (option.eq.8) call LUMFLUX(z,h,q)
      if (option.eq.9) call SPATIAL(z,h,q)
      if (option.eq.10) call LOOKBACK(z,h,q)
      if (option.eq.11) call GALACTIC
      if (option.eq.12) call DISPSKY
      if (option.eq.13) call TABLES
      if (option.eq.14) call RELTRANS
      if (option.eq.15) call LOCATE
      if (option.eq.16) call sun_moon_tables
      if (option.eq.17) call Decimal_To_Hours
      if (option.eq.18) call sun_daytime_tables    ! added -tab 07jul2008

      if (option.ne.-1) goto 20
      
      END
