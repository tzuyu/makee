/* cpgim.c                      tab  Sep99 Nov99 Mar00 */


/* This is the makee version of pgim (written for GALEX sources).  */

#include "ctab.h"
#include "cmisc.h"
#include "cufio.h"
#include "cpg-util.h"
#include "cpgplot.h"

/* #define MAXPIX (3000*3000)+1000 */  /* Maximum pixels in an image. */

#define MAXPIX 9150000            /* Maximum pixels in an image. */
                                  /* 3000: 35 Megabytes for a float array. */
                                  /* 4000: 63 Megabytes for a float array. */


/* : : : :  : : : :  : : : :  : : : :  : : : :  : : : :  : : : :  : : : : */
/* Global variables (see PGPLOT:PGIM for details).     */

int idim[9],jdim[9];    /* Dimensions of image data for each plane.      */
float bg[9],fg[9];      /* Background, Foreground levels for each plane. */
float tr[6];            /* Transfer array.                               */
char device[100];       /* Display device.                               */
char colorfile[100];    /* Full path color map filename.                 */
char colorroot[100];    /* Root name of color map filename.              */
char rootname[9][100];  /* Root name for .fits, .box, etc. (each plane). */
char fitsfile[9][100];  /* Filename of FITS image file (each plane).     */

int headersize = 115200; /* Size of FITS header.                     */
char header[115200];     /* FITS header.                             */

int NumPlane = 1;       /* Total number of image planes.            */
int plane    = 0;       /* Current plane to display.                */

float xmin,xmax,ymin,ymax;  /* Current display range. */

char top1[100],top2[100];   /* Data values printed on screen. */

/* : : : :  : : : :  : : : :  : : : :  : : : :  : : : :  : : : :  : : : : */



/* ---------------------------------------------------------------------
*/
void FindKeyTest( char cc )
{
if ( cc == '\000' ) printf(":::000\n");
if ( cc == '\001' ) printf(":::001\n");
if ( cc == '\002' ) printf(":::002\n");
if ( cc == '\003' ) printf(":::003\n");
if ( cc == '\004' ) printf(":::004\n");
if ( cc == '\005' ) printf(":::005\n");
if ( cc == '\006' ) printf(":::006\n");
if ( cc == '\007' ) printf(":::007\n");
if ( cc == '\010' ) printf(":::010\n");
if ( cc == '\011' ) printf(":::011\n");
if ( cc == '\012' ) printf(":::012\n");
if ( cc == '\013' ) printf(":::013\n");
if ( cc == '\014' ) printf(":::014\n");
if ( cc == '\015' ) printf(":::015\n");
if ( cc == '\016' ) printf(":::016\n");
if ( cc == '\017' ) printf(":::017\n");
if ( cc == '\020' ) printf(":::020\n");
if ( cc == '\021' ) printf(":::021\n");
if ( cc == '\022' ) printf(":::022\n");
if ( cc == '\023' ) printf(":::023\n");
if ( cc == '\024' ) printf(":::024\n");
if ( cc == '\025' ) printf(":::025\n");
if ( cc == '\026' ) printf(":::026\n");
if ( cc == '\027' ) printf(":::027\n");
if ( cc == '\030' ) printf(":::030\n");
if ( cc == '\031' ) printf(":::031\n");
if ( cc == '\032' ) printf(":::032\n");
if ( cc == '\033' ) printf(":::033\n");
if ( cc == '\034' ) printf(":::034\n");
if ( cc == '\035' ) printf(":::035\n");
if ( cc == '\036' ) printf(":::036\n");
if ( cc == '\037' ) printf(":::037\n");
if ( cc == '\040' ) printf(":::040\n");
if ( cc == '\041' ) printf(":::041\n");
if ( cc == '\042' ) printf(":::042\n");
if ( cc == '\043' ) printf(":::043\n");
if ( cc == '\044' ) printf(":::044\n");
if ( cc == '\045' ) printf(":::045\n");
if ( cc == '\046' ) printf(":::046\n");
if ( cc == '\047' ) printf(":::047\n");
if ( cc == '\050' ) printf(":::050\n");
if ( cc == '\051' ) printf(":::051\n");
if ( cc == '\052' ) printf(":::052\n");
if ( cc == '\053' ) printf(":::053\n");
if ( cc == '\054' ) printf(":::054\n");
if ( cc == '\055' ) printf(":::055\n");
if ( cc == '\056' ) printf(":::056\n");
if ( cc == '\057' ) printf(":::057\n");
if ( cc == '\060' ) printf(":::060\n");
if ( cc == '\061' ) printf(":::061\n");
if ( cc == '\062' ) printf(":::062\n");
if ( cc == '\063' ) printf(":::063\n");
if ( cc == '\064' ) printf(":::064\n");
if ( cc == '\065' ) printf(":::065\n");
if ( cc == '\066' ) printf(":::066\n");
if ( cc == '\067' ) printf(":::067\n");
if ( cc == '\070' ) printf(":::070\n");
if ( cc == '\071' ) printf(":::071\n");
if ( cc == '\072' ) printf(":::072\n");
if ( cc == '\073' ) printf(":::073\n");
if ( cc == '\074' ) printf(":::074\n");
if ( cc == '\075' ) printf(":::075\n");
if ( cc == '\076' ) printf(":::076\n");
if ( cc == '\077' ) printf(":::077\n");
if ( cc == '\100' ) printf(":::100\n");
if ( cc == '\101' ) printf(":::101\n");
if ( cc == '\102' ) printf(":::102\n");
if ( cc == '\103' ) printf(":::103\n");
if ( cc == '\104' ) printf(":::104\n");
if ( cc == '\105' ) printf(":::105\n");
if ( cc == '\106' ) printf(":::106\n");
if ( cc == '\107' ) printf(":::107\n");
if ( cc == '\110' ) printf(":::110\n");
if ( cc == '\111' ) printf(":::111\n");
if ( cc == '\112' ) printf(":::112\n");
if ( cc == '\113' ) printf(":::113\n");
if ( cc == '\114' ) printf(":::114\n");
if ( cc == '\115' ) printf(":::115\n");
if ( cc == '\116' ) printf(":::116\n");
if ( cc == '\117' ) printf(":::117\n");
if ( cc == '\120' ) printf(":::120\n");
if ( cc == '\121' ) printf(":::121\n");
if ( cc == '\122' ) printf(":::122\n");
if ( cc == '\123' ) printf(":::123\n");
if ( cc == '\124' ) printf(":::124\n");
if ( cc == '\125' ) printf(":::125\n");
if ( cc == '\126' ) printf(":::126\n");
if ( cc == '\127' ) printf(":::127\n");
if ( cc == '\130' ) printf(":::130\n");
if ( cc == '\131' ) printf(":::131\n");
if ( cc == '\132' ) printf(":::132\n");
if ( cc == '\133' ) printf(":::133\n");
if ( cc == '\134' ) printf(":::134\n");
if ( cc == '\135' ) printf(":::135\n");
if ( cc == '\136' ) printf(":::136\n");
if ( cc == '\137' ) printf(":::137\n");
if ( cc == '\140' ) printf(":::140\n");
if ( cc == '\141' ) printf(":::141\n");
if ( cc == '\142' ) printf(":::142\n");
if ( cc == '\143' ) printf(":::143\n");
if ( cc == '\144' ) printf(":::144\n");
if ( cc == '\145' ) printf(":::145\n");
if ( cc == '\146' ) printf(":::146\n");
if ( cc == '\147' ) printf(":::147\n");
if ( cc == '\150' ) printf(":::150\n");
if ( cc == '\151' ) printf(":::151\n");
if ( cc == '\152' ) printf(":::152\n");
if ( cc == '\153' ) printf(":::153\n");
if ( cc == '\154' ) printf(":::154\n");
if ( cc == '\155' ) printf(":::155\n");
if ( cc == '\156' ) printf(":::156\n");
if ( cc == '\157' ) printf(":::157\n");
if ( cc == '\160' ) printf(":::160\n");
if ( cc == '\161' ) printf(":::161\n");
if ( cc == '\162' ) printf(":::162\n");
if ( cc == '\163' ) printf(":::163\n");
if ( cc == '\164' ) printf(":::164\n");
if ( cc == '\165' ) printf(":::165\n");
if ( cc == '\166' ) printf(":::166\n");
if ( cc == '\167' ) printf(":::167\n");
if ( cc == '\170' ) printf(":::170\n");
if ( cc == '\171' ) printf(":::171\n");
if ( cc == '\172' ) printf(":::172\n");
if ( cc == '\173' ) printf(":::173\n");
if ( cc == '\174' ) printf(":::174\n");
if ( cc == '\175' ) printf(":::175\n");
if ( cc == '\176' ) printf(":::176\n");
if ( cc == '\177' ) printf(":::177\n");  /* backspace or delete */
if ( cc == '\200' ) printf(":::200\n");
if ( cc == '\201' ) printf(":::201\n");
if ( cc == '\202' ) printf(":::202\n");
if ( cc == '\203' ) printf(":::203\n");
if ( cc == '\204' ) printf(":::204\n");
if ( cc == '\205' ) printf(":::205\n");
if ( cc == '\206' ) printf(":::206\n");
if ( cc == '\207' ) printf(":::207\n");
if ( cc == '\210' ) printf(":::210\n");
if ( cc == '\211' ) printf(":::211\n");
if ( cc == '\212' ) printf(":::212\n");
if ( cc == '\213' ) printf(":::213\n");
if ( cc == '\214' ) printf(":::214\n");
if ( cc == '\215' ) printf(":::215\n");
if ( cc == '\216' ) printf(":::216\n");
if ( cc == '\217' ) printf(":::217\n");
if ( cc == '\220' ) printf(":::220\n");
if ( cc == '\221' ) printf(":::221\n");
if ( cc == '\222' ) printf(":::222\n");
if ( cc == '\223' ) printf(":::223\n");
if ( cc == '\224' ) printf(":::224\n");
if ( cc == '\225' ) printf(":::225\n");
if ( cc == '\226' ) printf(":::226\n");
if ( cc == '\227' ) printf(":::227\n");
if ( cc == '\230' ) printf(":::230\n");
if ( cc == '\231' ) printf(":::231\n");
if ( cc == '\232' ) printf(":::232\n");
if ( cc == '\233' ) printf(":::233\n");
if ( cc == '\234' ) printf(":::234\n");
if ( cc == '\235' ) printf(":::235\n");
if ( cc == '\236' ) printf(":::236\n");
if ( cc == '\237' ) printf(":::237\n");
if ( cc == '\240' ) printf(":::240\n");
if ( cc == '\241' ) printf(":::241\n");
if ( cc == '\242' ) printf(":::242\n");
if ( cc == '\243' ) printf(":::243\n");
if ( cc == '\244' ) printf(":::244\n");
if ( cc == '\245' ) printf(":::245\n");
if ( cc == '\246' ) printf(":::246\n");
if ( cc == '\247' ) printf(":::247\n");
if ( cc == '\250' ) printf(":::250\n");
if ( cc == '\251' ) printf(":::251\n");
if ( cc == '\252' ) printf(":::252\n");
if ( cc == '\253' ) printf(":::253\n");
if ( cc == '\254' ) printf(":::254\n");
if ( cc == '\255' ) printf(":::255\n");
if ( cc == '\256' ) printf(":::256\n");
if ( cc == '\257' ) printf(":::257\n");
if ( cc == '\260' ) printf(":::260\n");
if ( cc == '\261' ) printf(":::261\n");
if ( cc == '\262' ) printf(":::262\n");
if ( cc == '\263' ) printf(":::263\n");
if ( cc == '\264' ) printf(":::264\n");
if ( cc == '\265' ) printf(":::265\n");
if ( cc == '\266' ) printf(":::266\n");
if ( cc == '\267' ) printf(":::267\n");
if ( cc == '\270' ) printf(":::270\n");
if ( cc == '\271' ) printf(":::271\n");
if ( cc == '\272' ) printf(":::272\n");
if ( cc == '\273' ) printf(":::273\n");
if ( cc == '\274' ) printf(":::274\n");
if ( cc == '\275' ) printf(":::275\n");
if ( cc == '\276' ) printf(":::276\n");
if ( cc == '\277' ) printf(":::277\n");
return;
}


/* ----------------------------------------------------------------------
  Get a character string from screen window prompting with "line".
*/
void cpg_WindowString( char line[], char answer[] )
{
/**/
char wrd[100];
char prompt[100];
char cc;
char s[2];
float ptr,cx,cy;
int ii;
/**/
/* Clear. */
strcpy(answer,"");
/* Show entry of value in plot window. */
strcpy(prompt,line);
cpgmtxt("T",1.05,0.5,0.0,prompt);
strcpy(wrd,"");
ii  =0;
ptr =0.50 + (clc(prompt) * 0.022);
s[1]='\0';
while (1) {
  cpgcurs( &cx, &cy, &cc );
  if (cc == '\177') {              /* BackSpace */
    if (ii > 0) {
      s[0]=wrd[ii-1];
      cpgsci(0);
      cpgmtxt("T",1.05,ptr,0.0,s);
      cpgsci(1);
      ptr=ptr-0.022;
      --ii;
    }
  } else {
    ptr=ptr+0.022;
    s[0]=cc;
    cpgmtxt("T",1.05,ptr,0.0,s);
    if (cc == '\015') break;
    wrd[ii] = cc;
    ++ii;
  }
}
wrd[ii]='\0';
/* Load value. */
strcpy(answer,wrd);
/* Erase value. */
cpgsci(0);
cpgmtxt("T",1.05,0.5,0.0,prompt);
ptr = 0.50 + (clc(prompt) * 0.022);
for (ii=0; ii<=clc(wrd); ++ii) {
  s[0] = wrd[ii];
  ptr=ptr+0.022;
  cpgmtxt("T",1.05,ptr,0.0,s);
}
ptr=ptr+0.022;
s[0] = '\015';
cpgmtxt("T",1.05,ptr,0.0,s);
cpgsci(1);
return;
}


/* ----------------------------------------------------------------------
  Set the scale factor number.  Returns new scale factor.
*/
int cpg_setscalefactor( char cc )
{
/**/
int sf;
/**/
sf = -1;
if ( cc == '0') sf = 0;
if ( cc == '1') sf = 1;
if ( cc == '2') sf = 2;
if ( cc == '3') sf = 3;
if ( cc == '4') sf = 4;
if ( cc == '5') sf = 5;
if ( cc == '6') sf = 6;
if ( cc == '7') sf = 7;
if ( cc == '8') sf = 8;
if ( cc == '9') sf = 9;
return(sf);
}


/* ----------------------------------------------------------------------
  Set the color map using: cpgscr(ci,cr,cg,cb) .
*/
void cpg_setcolormap( char colorfile[] )
{
/**/
int ii;
float gray,red,green,blue;
char line[100];
FILE *infu;
/**/

/* First set the color index range (64 colors) */
cpgscir(16,79);

/* Default is a gray color map. */
for (ii=16; ii<80; ++ii) {
  gray = MAX(0.,MIN(1., (ii-16.)/63. ));
  cpgscr(ii,gray,gray,gray);
}

if (strcmp(colorfile,"gray") != 0) {

/* Open color map file. */
  infu = fopen( colorfile, "r" ) ;
  if (infu == NULL) {
    fprintf(stderr,
            "ERROR: cpg_setcolormap: Cannot open color file %s\n",colorfile);
    return;
  }
  ii=16;
  while ( fgetline(line,infu) == 1 ) {
    red  = GetLineValue(line,1) / 255.;
    green= GetLineValue(line,2) / 255.;
    blue = GetLineValue(line,3) / 255.;
    cpgscr(ii,red,green,blue);
    ++ii;
    if (ii > 79) break;
  }

}
return;
}


/* --------------------------------------------------------------------
  Display the image using PGPLOT and PGIM.
  Input:  image : the 2D/3D image data.
          mode  :  0 = do not open new device.
                   1 = open new device (first time).
                  -1 = close old device then open new device.
*/
void cpg_showimage( float *image, int mode )
{
/**/
char title[100];
int ii,planepixno;
float xi1,xi2,yj1,yj2;
float xrange,yrange,xcenter,ycenter;
int i1,i2,j1,j2;
/**/
/*
FILE *infu;
float xxx[1],yyy[1];
char line[60];
*/
/**/

/* Check. */
if ((plane >= NumPlane)||(plane < 0)) {
  fprintf(stderr,"ERROR: bad plane number: %d   NumPlane=%d.\n",plane,NumPlane);
  exit(0);
}

/* If not square, increase smaller range. */
xrange = xmax - xmin;
yrange = ymax - ymin;
if (xrange < yrange) {
  xcenter = (xmin + xmax)/2.;
  xmin = xcenter - (yrange/2.);
  xmax = xcenter + (yrange/2.);
} else {
if (yrange < xrange) {
  ycenter = (ymin + ymax)/2.;
  ymin = ycenter - (xrange/2.);
  ymax = ycenter + (xrange/2.);
}}

/* Show only relevant part of image. */
i1 = xmin - 2.;
i2 = xmax + 2.;
j1 = ymin - 2.;
j2 = ymax + 2.;
i1 = MAX(1,MIN(idim[plane],i1));
i2 = MAX(1,MIN(idim[plane],i2));
j1 = MAX(1,MIN(jdim[plane],j1));
j2 = MAX(1,MIN(jdim[plane],j2));

/* Close device. */
if (mode == -1) cpgend();

/* Open device */
if (ABS((mode)) == 1) cpgbeg(0,device,1,1);

/* Set color, line width, and character height. */
cpgsci(1);
cpgslw(1);
cpgsch(1.0);

/* Set color map. */
cpg_setcolormap( colorfile );

/* New page. */
cpgpage();

/* No asking. */
cpgask(0);

/* Square window. */
cpgwnad(xmin,xmax,ymin,ymax);

/* Box with inverted tick marks. */
cpgbox("BICNST",0.,0,"BICNST",0.,0);

/* Draw image on device. */
ii = MAXPIX;
planepixno = plane * (ii/NumPlane);
cpgimag( &image[ planepixno ], idim[plane], jdim[plane], i1, i2, j1, j2, bg[plane], fg[plane], tr );

/* Draw bounds around image. Note: arrays start at index "0". */
xi1 = i1-1.5; xi2 = i2-0.5;
yj1 = j1-1.5; yj2 = j2-0.5;
cpgsls(4);
cpg_DrawBox(xi1,xi2,yj1,yj2);
cpgsls(1);

/* Label. */
cpgmtxt("B",2.4,0.5,0.5,"Column");
cpgmtxt("L",2.4,0.5,0.5,"Row");

/* Construct right info label. */
sprintf(title,"[%1d]  %s  bg=%f  fg=%f  cf=%s",plane,rootname[plane],bg[plane],fg[plane],colorroot);
cpgmtxt("B",3.6,0.5,0.5,title);


/* ### */
/*
cpgbbuf();
infu = fopen("fort.44","r");
cpgsci(3);
ii=0;
while( fgetline(line,infu) == 1) {
  if ((ii%4)==0) {
  xxx[0] = GLV(line,1);
  yyy[0] = GLV(line,2);
  cpgpt(1,xxx,yyy,1);
  }
  ++ii;
}
fclose(infu);
cpgebuf();

cpgbbuf();
infu = fopen("fort.43","r");
cpgsci(2);
ii=0;
while( fgetline(line,infu) == 1) {
  if ((ii%4)==0) {
  xxx[0] = GLV(line,1);
  yyy[0] = GLV(line,2);
  cpgpt(1,xxx,yyy,1);
  }
  ++ii;
}
fclose(infu);
cpgebuf();

cpgbbuf();
infu = fopen("fort.45","r");
cpgsci(6);
ii=0;
while( fgetline(line,infu) == 1) {
  if ((ii%4)==0) {
  xxx[0] = GLV(line,1);
  yyy[0] = GLV(line,2);
  cpgpt(1,xxx,yyy,1);
  }
  ++ii;
}
fclose(infu);
cpgebuf();
*/
/* ### */


cpgsci(1);
return;
}


/* ----------------------------------------------------------------------
 Zoom in or out of image, by changing xmin,xmax,ymin,ymax.
   Input: cx,cy : cursor position.
          mode  : -n=zoom in "n" times, +n=zoom out "n" times.
                   0=pan.
*/
void cpg_zoom( float cx, float cy, int mode )
{
/**/
float xcenter,ycenter,xrange,yrange,rad;
int ii;
/**/
/* New center. */
xcenter  = cx;
ycenter  = cy;
/* Ranges. */
xrange = xmax - xmin;
yrange = ymax - ymin;
/* Radius. */
rad = 1.;
/* Zoom radius. */
if (mode < 0) {
  for (ii=0; ii < ABS(mode); ++ii) { rad = rad / 2.; }   /* in */
} else {
if (mode > 0) {
  for (ii=0; ii < ABS(mode); ++ii) { rad = rad * 2.; }   /* out */
}}
/* New ranges. */
xrange = (xrange/2.) * rad;
yrange = (yrange/2.) * rad;
xmin = xcenter - xrange;
xmax = xcenter + xrange;
ymin = ycenter - yrange;
ymax = ycenter + yrange;
return;
}


/* ----------------------------------------------------------------------
  Show a row or a column on a seperate device plot.
*/
void cpg_RowColumnPlot( float *image, int nc, float *ocx, float *ocy,
                        float *lx1, float *lx2, float *ly1, float *ly2 )
{
/**/
float cx,cy,cx1,cx2,cy1,cy2;
float xx[1010],yy[1010];
float rr,xmin,xmax,ymin,ymax,MM,BB;
int nn,type,pixno,ii,max_pix;
char cc,cc0;
char device2[100];
/**/

/* Device. */
strcpy(device2,"/XDISP");

/* Echo note. */
printf("Click left mouse button twice to select range.\n");

/* Choices. */
cc0 = 'o';
/*
printf("Choose (click left mouse button twice to select range):\n");
printf("  o = one dimensional plot (pgdisp).\n");
printf("  a = column average (pgdisp).\n");
printf("  b = row average row plot (pgdisp).\n");
printf("  m = column median row plot (pgdisp).\n");
printf("  n = row median plot (pgdisp).\n");
printf("  j = produce postscript plot.\n");
/x Get key stroke. x/
cpgcurs( ocx, ocy, &cc0 );
*/

/* Range. */
cpgcurs( ocx, ocy, &cc );
cx1 = *ocx; cy1 = *ocy;
cpgcurs( ocx, ocy, &cc );
cx2 = *ocx; cy2 = *ocy;

/* More column or row? */
if (ABS((cx2 - cx1)) > ABS((cy2 - cy1))) {
  type = 0;
  if (cx1 > cx2) {
    rr = cx2; cx2 = cx1; cx1 = rr;
    rr = cy2; cy2 = cy1; cy1 = rr;
  }
  MM = (cy2 - cy1) / (cx2 - cx1);
  BB = cy2 - (MM * cx2);
} else {
  type = 1;
  if (cy1 > cy2) {
    rr = cx2; cx2 = cx1; cx1 = rr;
    rr = cy2; cy2 = cy1; cy1 = rr;
  }
  MM = (cx2 - cx1) / (cy2 - cy1);
  BB = cx2 - (MM * cy2);
}
/* One-dimensional plot. */
max_pix = MAXPIX;
nn=0;
xmin=0.; xmax=0.;
if (cc0 == 'o') {
  if (type == 0) {
    rr = (cx2 - cx1) / 1000.;
    xmin = cx1 - (rr * 50.);
    xmax = cx2 + (rr * 50.);
    for (cx=cx1; cx<=cx2; cx=cx+rr) {
      cy = (MM * cx) + BB;
      xx[nn] = cx;
      pixno = ( plane * (max_pix / NumPlane) ) + cnint(cx) + (cnint(cy) * nc);
      yy[nn] = image[pixno];
      ++nn;
    }
  } else {
    rr = (cy2 - cy1) / 1000.;
    xmin = cy1 - (rr * 50.);
    xmax = cy2 + (rr * 50.);
    for (cy=cy1; cy<=cy2; cy=cy+rr) {
      cx = (MM * cy) + BB;
      xx[nn] = cy;
      pixno = ( plane * (max_pix / NumPlane) ) + cnint(cx) + (cnint(cy) * nc);
      yy[nn] = image[pixno];
      ++nn;
    }
  }
} else {
  printf("### not available.\n");
  *lx1 = 0.; *lx2 = 0.; *ly1 = 0.; *ly2 = 0.;
  return;
}

/* y range. */
ymin=+1.e+30; ymax=-1.e+30;
for (ii=0; ii<nn; ++ii) {
  if (yy[ii] < ymin) ymin = yy[ii];
  if (yy[ii] > ymax) ymax = yy[ii];
}
rr = ymax - ymin;  rr = MAX((rr),(1.e-30));
ymin = ymin - (rr * 0.03);
ymax = ymax + (rr * 0.03);

/* Close image display device. */
cpgend();

/* Plot it. */
cpgbeg(0,device2,1,1);                /* Open device. */
cpgpage();                            /* New page. */
cpgask(0);                            /* No asking. */
cpgswin(xmin,xmax,ymin,ymax);         /* Set window. */
cpgbox("BCNST",0.,0,"BCNST",0.,0);    /* Box with inverted tick marks. */
cpgline(nn,xx,yy);                    /* Draw cut. */
if (type == 0) {
  cpgmtxt("B",2.4,0.5,0.5,"Column Number");
} else {
  cpgmtxt("B",2.4,0.5,0.5,"Row Number");
}
cpgmtxt("L",2.4,0.5,0.5,"Image Value");
cpgend();                             /*  Close device2. */

/* Save points. */
*lx1 = cx1; *lx2 = cx2; *ly1 = cy1; *ly2 = cy2;

return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Draw a color image from a FITS image.
*/
int main(int argc, char *argv[])
{
/**/
char arg[100][100];
char wrd[100];
char giffile[100];
char psfile[100];
float cx,cy;
float lx1,lx2,ly1,ly2;
float xytemp,xrange,yrange,userbg,userfg;
char cc;
double r1,r2;
/**/
int ii,narg,numpix,sc,ec,sr,er,nc,nr,pixno,sf,quit,interactive;
int planepixno,verbose;
/**/
float *image;
/**/

/* Initialize. */
strcpy(top1,"");
strcpy(top2,"");

/* Copy arguments. */
cargcopy(argv, argc, &narg, arg);

/* Options. */

strcpy(giffile,"none");
if (cfindarg(arg,&narg,"gif=",  ':',wrd)==1) { strcpy(giffile,wrd); }

strcpy(psfile,"none");
if (cfindarg(arg,&narg,"ps=",  ':',wrd)==1) { strcpy(psfile,wrd); }

userbg=-2.e+30;
if (cfindarg(arg,&narg,"bg=",  ':',wrd)==1) { userbg= GetLineValue(wrd,1); }

userfg=-2.e+30;
if (cfindarg(arg,&narg,"fg=",  ':',wrd)==1) { userfg= GetLineValue(wrd,1); }

verbose=0;
if (cfindarg(arg,&narg,"-verbose",'s',wrd)==1) { verbose = 1; }
if (cfindarg(arg,&narg,"-v"      ,'s',wrd)==1) { verbose = 1; }

quit=0;
if (cfindarg(arg,&narg,"-quit",'s',wrd)==1) { quit = 1; }
if (cfindarg(arg,&narg,"-q"   ,'s',wrd)==1) { quit = 1; }

strcpy(colorfile,"wrmb");
if (cfindarg(arg,&narg,"colorfile=",':',wrd)==1) { strcpy(colorfile,wrd); }
if (cfindarg(arg,&narg,"cf=",       ':',wrd)==1) { strcpy(colorfile,wrd); }

/* Syntax. */
if ( narg < 1 ) {
  printf("Syntax: cpgim  (root filename) [2nd file [3rd file [4th file] ] ]\n");
  printf("     [bg=]  [fg=]  [cf=]  [ps=file] [gif=file]  \n");
  printf("   [-v[erbose]]  [-q[uit]]\n");
  printf("\n");  
  printf("    bg=   : Background level (default is  0.1%c pixel.)\n",'%');
  printf("    fg=   : Foreground level (default is 99.9%c pixel.)\n",'%');
  printf("    cf=   : Color file to use (default is grayscale)\n");
  printf("              (wrmb,bw,ibw,prism,temper,contour,rain,star,wrb)\n");
  printf("    ps=   : PostScript filename.\n");
  printf("    gif=  : GIF image filename.\n");
  printf(" -verbose : Screen output verbosity.\n");
  printf("   -quit  : Quit immediately (non-interactive).\n");
  printf("\n");  
  printf("  Draw a color image of a FITS image.\n");
  printf("  Assumes root.fits, root.box, etc.\n");
  printf("\n");  
  exit(0);
}

/* Allocate memory. */
if (!( image = (float *)calloc(MAXPIX,sizeof(int)) )) {
  fprintf(stderr,"ERROR: cpgim: Out of memory for image array.\n");
  exit(0);
}

/* Echo. */
if (verbose == 1) {
  printf("colorfile= %s\n",colorfile);
  printf("psfile   = %s\n",psfile);
  printf("giffile  = %s\n",giffile);
}

/* Save colorfile root name. */
strcpy(colorroot,colorfile);

/* Prepend directory name. */
if (strcmp(colorfile,"gray") != 0) {
  strcat(colorfile,".col.64");
  cGetMakeeHome(wrd);
  strcat(wrd,"/colormaps/");
  strcat(wrd,colorfile);
  strcpy(colorfile,wrd);
}

/* Number of planes. */
NumPlane = narg;

printf("Loading %d image planes...\n",NumPlane);

/* Load each plane: 0,1,2 ... */
for (plane=0; plane<NumPlane; ++plane) {

/* Form root name. */
  strcpy(rootname[plane],arg[plane+1]);
  ii = cindex(rootname[plane],".fits");    /* truncate .fits extension */
  if (ii > 0) rootname[plane][ii] = '\0';

/* FITS file. */
  strcpy(fitsfile[plane],rootname[plane]);
  cAddFitsExt(fitsfile[plane]);

/* Read FITS image. */
  ii = MAXPIX;
  planepixno = plane * (ii/NumPlane);
  printf("Reading %s as plane %d ...\n",fitsfile[plane],plane);
  if ( creadfits(fitsfile[plane],&image[planepixno],ii,header,headersize) == 0) {
    fprintf(stderr,"ERROR: cpgim:Cannot read FITS file: %s.\n",fitsfile[plane]);
    exit(0);
  }
  numpix = cGetDimensions(header,headersize,&sc,&ec,&sr,&er,&nc,&nr);
  printf("Number of pixels: %d  ( ncol=%d  nrow=%d )\n",numpix,nc,nr);
  ii = MAXPIX;
  if (numpix > (ii/NumPlane)) {
    fprintf(stderr,"ERROR: too many pixels: %d .  MAXPIX=%d  NumPlane=%d  MAXPIX/NumPlane=%d .\n",numpix,MAXPIX,NumPlane,ii/NumPlane);
    exit(0);
  }

/* Set dimensions. */
  idim[plane] = nc;
  jdim[plane] = nr;

/* Get pixel range. */
  if ((userbg < -1.e+30)||(userfg < -1.e+30)) {
    cqcksrt(numpix,&image[planepixno]);
    printf("\n");
    printf("Percentiles:\n");
    ii=planepixno+(0.0001*(numpix-1)); printf(" 0.01:%13.5e\n",image[ii]);
    ii=planepixno+(0.001 *(numpix-1)); printf("  0.1:%13.5e\n",image[ii]);
    bg[plane] = image[ii]; 
    ii=planepixno+(0.010 *(numpix-1)); printf("  1.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.100 *(numpix-1)); printf(" 10.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.200 *(numpix-1)); printf(" 20.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.300 *(numpix-1)); printf(" 30.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.400 *(numpix-1)); printf(" 40.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.500 *(numpix-1)); printf(" 50.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.600 *(numpix-1)); printf(" 60.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.700 *(numpix-1)); printf(" 70.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.800 *(numpix-1)); printf(" 80.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.900 *(numpix-1)); printf(" 90.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.950 *(numpix-1)); printf(" 95.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.990 *(numpix-1)); printf(" 99.0:%13.5e\n",image[ii]);
    ii=planepixno+(0.999 *(numpix-1)); printf(" 99.9:%13.5e\n",image[ii]);
    fg[plane] = image[ii]; 
    ii=planepixno+(0.9999*(numpix-1)); printf("99.99:%13.5e\n",image[ii]);
    printf("\n");
  /* Re-read FITS image (since data has now been sorted.) */
    ii = MAXPIX;
    creadfits(fitsfile[plane],&image[planepixno],ii,header,headersize);
  } else {
    bg[plane] = userbg;   /* User values. */
    fg[plane] = userfg;
  }

/* Echo. */
  printf("Plane=%d  Background=%f  Foreground=%f\n",plane,bg[plane],fg[plane]);

}

/* Initial plane is 0. */
plane = 0;

/* Set transfer array:
X = tr[0] + tr[1]*I + tr[2]*J
Y = tr[3] + tr[4]*I + tr[5]*J
*/
tr[0]=-1.;   /* Offset of -1 so first pixel will be called "0". */
tr[1]= 1.;
tr[2]= 0.;
tr[3]=-1.;
tr[4]= 0.;
tr[5]= 1.;

/* Initial x and y range. */
xmin = -1.;
xmax = idim[plane] + 1.;
ymin = -1.;
ymax = jdim[plane] + 1.;

/* Set initial device name. */
if (strcmp(psfile,"none") != 0) {
  printf("Writing to: %s\n",psfile);
  strcat(psfile,"/CPS");
  strcpy(device,psfile);
  interactive=0;
} else { 
if (strcmp(giffile,"none") != 0) {
  printf("Writing to: %s\n",giffile);
  strcat(giffile,"/GIF");
  strcpy(device,giffile);
  interactive=0;
} else {
  strcpy(device,"/XWINDOW");
  interactive=1;
}}

/* Show the image. */
cpg_showimage( image, 1 );

/* Quit now. */
if (quit) interactive=0;

/* Query loop. */
while ( interactive ) {

/* Get key stroke. */
  cpgcurs( &cx, &cy, &cc );

/* Get scale factor, if not -1, get another key. */
  sf = cpg_setscalefactor( cc );
  if ( sf != -1 ) cpgcurs( &cx, &cy, &cc );

/* Query tree. */

/* Quit. */
  if (( cc == 'q' )||( cc == '\033' )) {
    printf("Quitting...\n");
    interactive=0;

/* Show data value. */
  } else {
  if ( cc == 'd' ) {
/* Erase old value. */
    cpgsci(0);
    cpgmtxt("T",1.05,0.0,0.0,top1);
    cpgmtxt("T",2.25,0.0,0.0,top2);
    cpgsci(1);
    ii = MAXPIX;
    pixno = ( plane * (ii/NumPlane) ) + cnint(cx) + ( cnint(cy)*nc );
    sprintf(top1,"%8.2f %8.2f %9.2f",cx,cy,image[pixno]);
    cpgmtxt("T",1.05,0.0,0.0,top1);
    printf("..........................\n");
    printf("Cursor column, row, data value: %s\n",top1);

/* Increase plane number. */
  } else {
  if ((cc == '=')||(cc == '+')) {
    ++plane;
    if (plane >= NumPlane) plane=0;
    cpg_showimage( image, 0 );

/* Decrease plane number. */
  } else {
  if ((cc == '-')||(cc == '_')) {
    --plane;
    if (plane < 0) plane=NumPlane-1;
    cpg_showimage( image, 0 );

/* Set levels. */
  } else {
  if ( cc == 'l' ) {
    printf("Enter new bg and fg in window.\n");
    cpg_WindowString("bg  fg >>",wrd);
    r1 = GetLineValue(wrd,1);
    r2 = GetLineValue(wrd,2);
    if ((r1 < -1.e+20)||(r2 < -1.e+20)) {
      fprintf(stderr,"ERROR: cpgim: reading bg and fg values.\n");
    } else {
    if (ABS((r1-r2)) < 1.e-30) {
      fprintf(stderr,"ERROR: cpgim: bg and fg values too close together.\n");
      fprintf(stderr,"ERROR: cpgim:  ... bg=%f  fg=%f\n",r1,r2);
    } else {
      bg[plane] = r1; fg[plane] = r2;
    }}
    cpg_showimage( image, 0 );

/* Move to a given position. */
  } else {
  if ( cc == 'm' ) {
    printf("Enter new center column and row in window.\n");
    cpg_WindowString("column row >>",wrd);
    r1 = GetLineValue(wrd,1);
    r2 = GetLineValue(wrd,2);
    xrange = xmax - xmin;
    yrange = ymax - ymin;
    xmin = r1 - (xrange/2.);
    xmax = r1 + (xrange/2.);
    ymin = r2 - (yrange/2.);
    ymax = r2 + (yrange/2.);
    cx = r1;
    cy = r2;
    cpg_showimage( image, 0 );

/* Restore-- display whole image. */
  } else {
  if ((cc == 'r')||(cc == 'w')) {
    if (verbose == 1) printf("Restoring whole image display.\n");
    xmin=-1.; xmax=idim[plane]+1.;
    ymin=-1.; ymax=jdim[plane]+1.;
    cpg_showimage( image, 0 );

/* Move up (increasing rows). */
  } else {
  if ( cc == '}' ) {
    yrange = 0.9 * (ymax - ymin);
    for (ii=0; ii<MAX(1,sf); ++ii) ymin = ymin + yrange;
    for (ii=0; ii<MAX(1,sf); ++ii) ymax = ymax + yrange;
    cy = (ymin+ymax)/2.;
    cpg_showimage( image, 0 );

/* Move down (decreasing rows). */
  } else {
  if ( cc == '{' ) {
    yrange = 0.9 * (ymax - ymin);
    for (ii=0; ii<MAX(1,sf); ++ii) ymin = ymin - yrange;
    for (ii=0; ii<MAX(1,sf); ++ii) ymax = ymax - yrange;
    cy = (ymin+ymax)/2.;
    cpg_showimage( image, 0 );

/* Move left (decreasing columns). */
  } else {
  if ( cc == '[' ) {
    xrange = 0.9 * (xmax - xmin);
    for (ii=0; ii<MAX(1,sf); ++ii) xmin = xmin - xrange;
    for (ii=0; ii<MAX(1,sf); ++ii) xmax = xmax - xrange;
    cx = (xmin+xmax)/2.;
    cpg_showimage( image, 0 );

/* Move right (increasing columns). */
  } else {
  if ( cc == ']' ) {
    xrange = 0.9 * (xmax - xmin);
    for (ii=0; ii<MAX(1,sf); ++ii) xmin = xmin + xrange;
    for (ii=0; ii<MAX(1,sf); ++ii) xmax = xmax + xrange;
    cx = (xmin+xmax)/2.;
    cpg_showimage( image, 0 );

/* Zoom in. */
  } else {
  if ((cc == 'A')||(cc == 'i')) {
    if (verbose == 1) printf("Zooming in  %d time(s).\n",MAX(1,sf));
    cpg_zoom( cx, cy, -1 * MAX(1,sf) );
    cpg_showimage( image, 0 );

/* Zoom out. */
  } else {
  if ((cc == 'X')||(cc == 'o')) {
    if (verbose == 1) printf("Zooming out %d time(s).\n",MAX(1,sf));
    cpg_zoom( cx, cy, +1 * MAX(1,sf) );
    cpg_showimage( image, 0 );

/* New box. */
  } else {
  if ( cc == 'b' ) {
    xmin = cx; ymin = cy;
    printf("Move cursor and hit key again to define square box.\n");
    cpgcurs( &cx, &cy, &cc );
    xmax = cx; ymax = cy;
    if (xmin > xmax) { xytemp=xmin; xmin=xmax; xmax=xytemp; }
    if (ymin > ymax) { xytemp=ymin; ymin=ymax; ymax=xytemp; }
    cpg_showimage( image, 0 );

/* Pan. */
  } else {
  if ((cc == 'D')||(cc == 'p')) {
    cpg_zoom( cx, cy, 0 );
    cpg_showimage( image, 0 );

/* Plot a cut through image. */
  } else {
  if ( cc == '.' ) {
    cpg_RowColumnPlot( image, nc, &cx, &cy, &lx1, &lx2, &ly1, &ly2 );
    strcpy(device,"/XWINDOW");
    cpg_showimage( image, 1 );
    cpg_drawline(lx1,lx2,ly1,ly2);

/* PostScript file. */
  } else {
  if ( cc == 'f' ) {
    cpg_WindowString("PS file:",psfile);
    printf("Writing to: %s ...\n",psfile);
    strcat(psfile,"/CPS");
    strcpy(device,psfile);
    cpg_showimage( image, -1 );
    strcpy(device,"/XWINDOW");
    cpg_showimage( image, -1 );
    printf("...done.\n");

/* GIF file. */
  } else {
  if ( cc == 'g' ) {
    printf("Enter name of new GIF image file: ");
    fgetline(giffile,stdin);
    printf("Writing to: %s\n",giffile);
    strcat(giffile,"/GIF");
    strcpy(device,giffile);
    cpg_showimage( image, -1 );
    strcpy(device,"/XWINDOW");
    cpg_showimage( image, -1 );

/* Change color map. */
  } else {
  if ( cc == 'c' ) {
    if (sf < 0) {
      printf("Choices:0.gray 1.wrmb 2.wrb 3.bw 4.ibw 5.temper 6.rain 7.contour 8.prism 9.star\n");
      printf("Enter number of new color map: ");
      fgetline(wrd,stdin);
      sf = GetLineValue(wrd,1);
    }
    strcpy(colorfile,"gray");
    if (sf == 1) strcpy(colorfile,"wrmb");
    if (sf == 2) strcpy(colorfile,"wrb");
    if (sf == 3) strcpy(colorfile,"bw");
    if (sf == 4) strcpy(colorfile,"ibw");
    if (sf == 5) strcpy(colorfile,"temper");
    if (sf == 6) strcpy(colorfile,"rain");
    if (sf == 7) strcpy(colorfile,"contour");
    if (sf == 8) strcpy(colorfile,"prism");
    if (sf == 9) strcpy(colorfile,"star");
    strcpy(colorroot,colorfile);   /* Save colorfile root name. */
/* Prepend directory name. */
    if (strcmp(colorfile,"gray") != 0) {
      strcat(colorfile,".col.64");
      cGetMakeeHome(wrd);
      strcat(wrd,"/colormaps/");
      strcat(wrd,colorfile);
      strcpy(colorfile,wrd);
    }
    cpg_showimage( image, 0 );

/* Help. */
  } else {
  if (( cc == '?' )||( cc == '/')) {
    printf("Valid key strokes:\n");
    printf("   d : show cursor position and Data value.\n");
    printf("   c : enter new Color map.\n");
    printf("  #c : load Color map number '#'.\n");
    printf("   l : enter new background and foreground Level.\n");
    printf(" r,w : Restore Whole image display.\n");
    printf("   i : zoom in (Left Mouse Button).\n");
    printf("   o : zoom out (Right Mouse Button).\n");
    printf("   p : pan (Middle Mouse Button).\n");
    printf(" 0-9 : scale factor (preceeds and modifies actual key command.)\n");
    printf("   [ : move image left (decreasing columns).\n");
    printf("   ] : move image right (increasing columns).\n");
    printf("   { : move image down (decreasing rows).\n");
    printf("   } : move image up (increasing rows).\n");
    printf("   m : move to a given location.\n");
    printf("   b : specify new Box with cursor.\n");
    printf("   f : write postscript File.\n");
    printf("   g : write Gif file.\n");
    printf(" +,= : Increase plane number.\n");
    printf(" -,_ : Decrease plane number.\n");
    printf("  .  : Plot a slice through the image in 'pgdisp' window.\n");
    printf(" ESC or q : Quit.\n");

/* Unknown key command. */
  } else {
    printf(" No function for key '%c'.\n",cc);
    printf("............. Octal number for this key....\n");
    FindKeyTest( cc );
    printf("...........................................\n");

  }}}}}}}}}}}}}}}}}}}}

}

/* Close device. */
cpgend();

return(0);
}
