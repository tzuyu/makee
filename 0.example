This is ./makee/0.example                       tab-- 28 August 1998

A 3000s exposure of the quasar 0841+129 (V about 18.5) yielded the 
following results:

Setup: echangl=+0.543  xdangl=+1.577  decker=C2  filter=og530
       binning = 1 col by 2 rows
       e-/DN = 2.45 (inverse gain)
       readout noise = 6.00 e-
       Red cross disperser in first order.

The total PSF on the HIRES focal plane was about 0.9 arcseconds FWHM.

Using makee the results at 6600 angstroms were:

The object width extracted was about 6.8 binned pixels.
The rescaled flat field level was 3.40 (divide Flat-*.fits by the
  rescaling value stated at run-time.)
The flux level (flat field corrected) (see Flux-*.fits) was 43.0.
The object level in counts (see DN-*.fits) was 146.2 DN.
The sky level (flat field corrected) (see Sky-*.fits) was 0.93.
The sky level in DN was 3.16 (taking Sky-*.fits and multiplying
  by the rescaled flat field.)

Predicted S/N per pixel in final spectrum:

146.2 x 2.45 = 358.2 e- (object minus sky)
6.8 x 3.16 x 2.45 = 52.6 e- (sky)

total noise = sqrt( 358.2 + 52.6 + (6.8 x 6.0 x 6.0 ) )
            = sqrt( 358.2 + 52.6 + 244.8 )
            = 25.6 e-

total signal-to-noise = 358.2 / 25.8 = 14.0  (per spectrum pixel)

The actual extracted spectrum yielded a S/N of 14.1 using "makee mode=3"
(consistent with our prediction).  Using optimal extraction ("makee mode=1")
the S/N was 15.3, or about a 9% improvement in the S/N.

....

Notice that the readout noise is a significant factor in the total noise.
The readout noise pixel can be decreased by increasing the binning during
readout.  To very roughly simulate a binning of 1x4 or 2x2 we decreased the
readout noise parameter to makee by sqrt(2) to 4.26 electrons.  This test
extraction yielded a S/N about 8% higher than a readout noise of 6.0
electrons.  However, with 2x2 binning the spectral resolution would be
degraded by undersampling.   With 1x4 the profile fitting may be compromised
(especially for optimal extraction), i.e. the actual 1x4 S/N improvement may
be somewhat less than 8%.  (The decrease in this improvement would be
greater in good seeing and less in bad seeing.)

Note that these values only apply for our example.  The optimal extraction
improvement will be greater for lower object/sky ratios (up to a theoretical
maximum of about 30%) and less for higher object/sky ratios (down to 0% for
bright stars).  The contribution of readout-noise (and thus the improvement
with rebinning) will depend on the object + sky level and the exposure time.
